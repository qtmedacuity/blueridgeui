/**
 * @file   StaticChartController.h
 *
 * \brief This file declares the Static Chart Controller class.
 *
 * Description:
 * Provides a controller to manage static data to a WaveChartWidget.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/
#pragma once

#include "presentation/WaveChartController.h"

class StaticChartController : public WaveChartController
{
    Q_OBJECT
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(double samplesPerSecond READ samplesPerSecond WRITE setSamplesPerSecond NOTIFY samplesPerSecondChanged)
    Q_PROPERTY(double rangeInSeconds READ rangeInSeconds WRITE setRangeInSeconds NOTIFY rangeInSecondsChanged)

    static const QString PressureTag;

   public:
    explicit StaticChartController(const QString& resource, const QString& sourceFile = QString(),
                                   QObject* parent = nullptr);
    ~StaticChartController() override;

    Q_INVOKABLE void refresh();

   signals:
    void sourceChanged(QString);
    void samplesPerSecondChanged(unsigned long);
    void rangeInSecondsChanged(double);

   public slots:
    QString source() const;
    void setSource(const QString& sourceFile);

    double samplesPerSecond() const;
    void setSamplesPerSecond(unsigned long samples);

    double rangeInSeconds() const;
    void setRangeInSeconds(double seconds);

   private:
    explicit StaticChartController(QObject* = nullptr) = delete;
    StaticChartController(StaticChartController const&) = delete;
    StaticChartController& operator=(StaticChartController const&) = delete;

    void readDataFile(const QString& sourceFile);
    void updateSensorData(unsigned long sequence, double pressure);

   private:
    QString m_sourceFile;
    unsigned long m_samplesPerSecond;
    double m_rangeInSeconds;
    unsigned long m_totalSampleRange;
};
