#pragma once
/**
 * @file SystemSettingsController.h
 *
 * \brief This file declares the controller of system settings.
 *
 * Description:
 * Provides access to commands and data for system settings management.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/
#include <QObject>

class ISystemSettingsProvider;

class SystemSettingsController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool rfState READ rfState WRITE setRFState NOTIFY rfStateChanged)
    Q_PROPERTY(QString defaultLanguage READ defaultLanguage NOTIFY configurationChanged)
    Q_PROPERTY(unsigned int paMeanMinimum READ paMeanMinimum NOTIFY configurationChanged)
    Q_PROPERTY(unsigned int paMeanMaximum READ paMeanMaximum NOTIFY configurationChanged)
    Q_PROPERTY(int searchPressureMinimum READ searchPressureMinimum NOTIFY configurationChanged)
    Q_PROPERTY(int searchPressureMaximum READ searchPressureMaximum NOTIFY configurationChanged)
    Q_PROPERTY(int searchPressureStart READ searchPressureStart NOTIFY configurationChanged)
    Q_PROPERTY(unsigned int systolicMinimum READ systolicMinimum NOTIFY configurationChanged)
    Q_PROPERTY(unsigned int systolicMaximum READ systolicMaximum NOTIFY configurationChanged)
    Q_PROPERTY(unsigned int diastolicMinimum READ diastolicMinimum NOTIFY configurationChanged)
    Q_PROPERTY(unsigned int diastolicMaximum READ diastolicMaximum NOTIFY configurationChanged)
    Q_PROPERTY(unsigned int referenceMeanMinimum READ referenceMeanMinimum NOTIFY configurationChanged)
    Q_PROPERTY(unsigned int referenceMeanMaximum READ referenceMeanMaximum NOTIFY configurationChanged)

   public:
    explicit SystemSettingsController(ISystemSettingsProvider& settingsProvider, QObject* parent = nullptr);
    ~SystemSettingsController() override;

    Q_INVOKABLE void enableSensorDataStream();
    Q_INVOKABLE void disableSensorDataStream();

   signals:
    void rfStateChanged(bool);
    void configurationChanged();

   public slots:
    bool rfState() const;
    void setRFState(bool state);

    QString defaultLanguage() const;
    unsigned int paMeanMinimum() const;
    unsigned int paMeanMaximum() const;
    int searchPressureMinimum() const;
    int searchPressureMaximum() const;
    int searchPressureStart() const;
    unsigned int systolicMinimum() const;
    unsigned int systolicMaximum() const;
    unsigned int diastolicMinimum() const;
    unsigned int diastolicMaximum() const;
    unsigned int referenceMeanMinimum() const;
    unsigned int referenceMeanMaximum() const;

   private slots:
    void enableSensorData();
    void disableSensorData();
    void rfStateUpdated();
    void configurationUpdated();

   private:
    void handleRFStateUpdate();
    void handleConfigurationUpdate();

   private:
    ISystemSettingsProvider& m_settingsProvider;
    unsigned long m_rfStateCbId;
    unsigned long m_sensorErrorCbId;
    unsigned long m_configCbId;
};
