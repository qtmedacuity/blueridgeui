#pragma once

/**
 * @file   SensorCalibration.h
 *
 * \brief This file contains the declaration of the Sensor Calibration class.
 *
 * Description:
 * Provides methods for data for sensor calibration.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>

class SensorCalibrationData;

class SensorCalibration : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString date READ date WRITE setDate NOTIFY calibrationChanged)
    Q_PROPERTY(QString time READ time WRITE setTime NOTIFY calibrationChanged)
    Q_PROPERTY(int systolic READ systolic WRITE setSystolic NOTIFY calibrationChanged)
    Q_PROPERTY(int diastolic READ diastolic WRITE setDiastolic NOTIFY calibrationChanged)
    Q_PROPERTY(int mean READ mean WRITE setMean NOTIFY calibrationChanged)
    Q_PROPERTY(int reference READ reference WRITE setReference NOTIFY calibrationChanged)
    Q_PROPERTY(int heartRate READ heartRate WRITE setHeartRate NOTIFY calibrationChanged)
    Q_PROPERTY(int sensorStrength READ sensorStrength WRITE setSensorStrength NOTIFY calibrationChanged)
    Q_PROPERTY(QString waveFormFile READ waveFormFile WRITE setWaveFormFile NOTIFY calibrationChanged)

   public:
    explicit SensorCalibration(QObject *parent = nullptr);
    explicit SensorCalibration(SensorCalibrationData *sensorCalibrationData, QObject *parent = nullptr);

    QString date() const;
    void setDate(QString date);
    QString time() const;
    void setTime(QString time);
    int systolic() const;
    void setSystolic(int systolic);
    int diastolic() const;
    void setDiastolic(int diastolic);
    int mean() const;
    void setMean(int mean);
    int heartRate() const;
    void setHeartRate(int heartRate);
    int reference() const;
    void setReference(int reference);
    int sensorStrength() const;
    void setSensorStrength(int sensorStrength);
    QString waveFormFile() const;
    void setWaveFormFile(QString waveFormFile);

   signals:
    void calibrationChanged() const;

   private:
    QString m_date;
    QString m_time;
    int m_systolic;
    int m_diastolic;
    int m_mean;
    int m_reference;
    int m_heartRate;
    int m_sensorStrength;
    QString m_waveFormFile;
};
