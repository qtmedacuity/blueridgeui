#pragma once

/**
 * @file   DateTimeController.h
 *
 * \brief This file contains the declaration of the date and time
 * controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the date and
 * time.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>

class QTimer;

class DateTimeController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString dateNow READ dateNow NOTIFY dateChanged)
    Q_PROPERTY(QString timeNow READ timeNow NOTIFY timeChanged)

   public:
    DateTimeController(std::string const& dateFormat, std::string const& timeFormat);

   signals:
    void dateChanged(QString currentDate);
    void timeChanged(QString currentTime);

   public slots:
    QString dateNow() const { return m_date; }
    QString timeNow() const { return m_time; }

   private slots:
    void update();

   private:
    DateTimeController(QObject* parent = nullptr) = delete;
    DateTimeController(DateTimeController const&) = delete;
    DateTimeController& operator=(DateTimeController const&) = delete;

   private:
    QString m_date;
    std::string m_dateFormat;
    QString m_time;
    std::string m_timeFormat;
    QTimer* m_timer;
};
