#pragma once

/**
 * @file   CellularStrengthController.h
 *
 * \brief This file contains the declaration of the Cellular Strength Controller class.
 *
 * Description:
 * Provides the UI with Cellular Status and update signals.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/SignalStrengthController.h"

class ICellularStrengthProvider;

class CellularStrengthController : public SignalStrengthController
{
    Q_OBJECT
    Q_PROPERTY(QString carrier READ carrier NOTIFY carrierChanged)

   public:
    CellularStrengthController(ICellularStrengthProvider& provider, StatusValueList const& barvalues = {});
    ~CellularStrengthController() override;

   signals:
    void cellularDataUpdated();
    void carrierChanged(QString cellCarrier);

   public slots:
    virtual QString carrier() const { return m_carrier; }
    virtual void setCarrier(QString const& cellCarrier);
    virtual void setStrength(int signal) override;

   private slots:
    void setCellStatus();

   private:
    CellularStrengthController(QObject* parent = nullptr) = delete;
    CellularStrengthController(CellularStrengthController const&) = delete;
    CellularStrengthController& operator=(CellularStrengthController const&) = delete;

    void handleCellularData();

   private:
    ICellularStrengthProvider& m_provider;
    unsigned long m_cbId;
    QString m_carrier;
};
