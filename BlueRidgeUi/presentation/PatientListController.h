#pragma once

/**
 * @file   PatientListController.h
 *
 * \brief This file contains the declaration of the patient list controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the list of
 * patients.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>
#include <QSortFilterProxyModel>
#include "presentation/PatientListModel.h"

class IPatientListProvider;

class PatientListController : public QSortFilterProxyModel
{
    Q_OBJECT
   public:
    PatientListController(IPatientListProvider& provider, QObject* parent = nullptr);
    ~PatientListController() override;

    Q_PROPERTY(QString lastUpdated READ lastUpdated NOTIFY lastUpdatedChanged)

    Q_INVOKABLE void sortByColumn(int column, bool ascending);
    Q_INVOKABLE void setFilterString(QString filter);

    Q_INVOKABLE QVariant get(int index);
    Q_INVOKABLE void getAllPatients();
    Q_INVOKABLE void getImplantPatients();
    Q_INVOKABLE void getWaitingImplantPatients();
    Q_INVOKABLE void refresh();
    Q_INVOKABLE void uploadToMerlin() {}  // TODO: Implement
    Q_INVOKABLE void exportToUsb() {}     // TODO: Implement
    Q_INVOKABLE int selectAllPatients(bool selected);
    Q_INVOKABLE bool havePendingUploads();

    QString lastUpdated() const;

   signals:
    void lastUpdatedChanged(QString lastUpdated);
    void refreshComplete();
    void refreshUpdated();
    void patientListUpdated();

   private slots:
    void handleUpdatedPatientList();
    void handleRefreshedPatientList();

   private:
    void setLastUpdated();
    void handleRefreshResponse();
    void handlePatientListResponse();

   private:
    IPatientListProvider& m_provider;
    unsigned long m_patientCbId;
    unsigned long m_refreshCbId;
    PatientListModel* m_patientListModel = nullptr;
    QString m_lastUpdated;
    QList<PatientInfo*> m_selectedPatients;
    bool m_havePendingUploads;
};
