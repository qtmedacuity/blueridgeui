#pragma once

/**
 * @file   CardiacOutput.h
 *
 * \brief This file contains the declaration of the Cardiac Output class.
 *
 * Description:
 * Provides methods and data for Cardiac Output.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>

class CardiacOutputData;
class Recording;

class CardiacOutput : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString date READ date WRITE setDate NOTIFY cardiacOutputChanged)
    Q_PROPERTY(QString time READ time WRITE setTime NOTIFY cardiacOutputChanged)
    Q_PROPERTY(float cardiacOutput READ cardiacOutput WRITE setCardiacOutput NOTIFY cardiacOutputChanged)
    Q_PROPERTY(int heartRate READ heartRate WRITE setHeartRate NOTIFY cardiacOutputChanged)
    Q_PROPERTY(int sensorStrength READ sensorStrength WRITE setSensorStrength NOTIFY cardiacOutputChanged)
    Q_PROPERTY(QString waveFormFile READ waveFormFile WRITE setWaveFormFile NOTIFY cardiacOutputChanged)

   public:
    explicit CardiacOutput(QObject *parent = nullptr);
    explicit CardiacOutput(CardiacOutputData *cardiacOutputData, QObject *parent = nullptr);
    explicit CardiacOutput(Recording *recording, float coValue);

    QString date() const;
    void setDate(QString date);
    QString time() const;
    void setTime(QString time);
    float cardiacOutput() const;
    void setCardiacOutput(float output);
    int heartRate() const;
    void setHeartRate(int heartRate);
    int sensorStrength() const;
    void setSensorStrength(int sensorStrength);
    QString waveFormFile() const;
    void setWaveFormFile(QString waveFormFile);

    CardiacOutputData toCardiacOutputData() const;

   signals:
    void cardiacOutputChanged() const;

   private:
    QString m_date;
    QString m_time;
    float m_cardiacOutput;
    int m_heartRate;
    int m_sensorStrength;
    QString m_waveFormFile;
};
