#pragma once

/**
 * @file   RhcValues.h
 *
 * \brief This file contains the declaration of the Rhc Values class.
 *
 * Description:
 * Provides methods and data for Rhc Values.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>

class RhcData;
class RhcValues : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString date READ date WRITE setDate NOTIFY rhcValuesChanged)
    Q_PROPERTY(QString time READ time WRITE setTime NOTIFY rhcValuesChanged)
    Q_PROPERTY(int raValue READ raValue WRITE setRaValue NOTIFY rhcValuesChanged)
    Q_PROPERTY(int rvSystolic READ rvSystolic WRITE setRvSystolic NOTIFY rhcValuesChanged)
    Q_PROPERTY(int rvDiastolic READ rvDiastolic WRITE setRvDiastolic NOTIFY rhcValuesChanged)
    Q_PROPERTY(int pcwpValue READ pcwpValue WRITE setPcwpValue NOTIFY rhcValuesChanged)
    Q_PROPERTY(int paSystolic READ paSystolic WRITE setPaSystolic NOTIFY rhcValuesChanged)
    Q_PROPERTY(int paDiastolic READ paDiastolic WRITE setPaDiastolic NOTIFY rhcValuesChanged)
    Q_PROPERTY(int paMean READ paMean WRITE setPaMean NOTIFY rhcValuesChanged)
    Q_PROPERTY(int heartRate READ heartRate WRITE setHeartRate NOTIFY rhcValuesChanged)
    Q_PROPERTY(int sensorStrength READ sensorStrength WRITE setSensorStrength NOTIFY rhcValuesChanged)
    Q_PROPERTY(QString waveFormFile READ waveFormFile WRITE setWaveFormFile NOTIFY rhcValuesChanged)

   public:
    explicit RhcValues(QObject* parent = nullptr);
    explicit RhcValues(RhcData const* rhcData, QObject* parent = nullptr);

    QString date() const;
    void setDate(QString date);
    QString time() const;
    void setTime(QString time);
    int raValue() const;
    void setRaValue(int value);
    int rvSystolic() const;
    void setRvSystolic(int systolic);
    int rvDiastolic() const;
    void setRvDiastolic(int diastolic);
    int pcwpValue() const;
    void setPcwpValue(int value);
    int paSystolic() const;
    void setPaSystolic(int systolic);
    int paDiastolic() const;
    void setPaDiastolic(int diastolic);
    int paMean() const;
    void setPaMean(int mean);
    int heartRate() const;
    void setHeartRate(int heartRate);
    int sensorStrength() const;
    void setSensorStrength(int sensorStrength);
    QString waveFormFile() const;
    void setWaveFormFile(QString waveFormFile);

    void toRhcData(RhcData* rhcData) const;
    void fromRhcData(RhcData const* rhcData);

   signals:
    void rhcValuesChanged();

   private:
    QString m_date;
    QString m_time;
    int m_raValue;
    int m_rvSystolic;
    int m_rvDiastolic;
    int m_pcwpValue;
    int m_paSystolic;
    int m_paDiastolic;
    int m_paMean;
    int m_heartRate;
    int m_sensorStrength;
    QString m_waveFormFile;
};
