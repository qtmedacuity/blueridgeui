#pragma once

/**
 * @file   WifiStrengthController.h
 *
 * \brief This file contains the declaration of the Wifi Strength Controller class.
 *
 * Description:
 * Provides the UI with Wifi Status and update signals.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/SignalStrengthController.h"

class IWifiStrengthProvider;

class WifiStrengthController : public SignalStrengthController
{
    Q_OBJECT
    Q_PROPERTY(QString networkName READ networkName NOTIFY networkNameChanged)

   public:
    WifiStrengthController(IWifiStrengthProvider& provider, StatusValueList const& barvalues = {});
    ~WifiStrengthController() override;

   signals:
    void wifiDataUpdated();
    void networkNameChanged(QString network);

   public slots:
    virtual QString networkName() const { return m_networkName; }
    virtual void setNetworkName(QString const& network);
    virtual void setStrength(int signal) override;

   private slots:
    void setWifiStatus();

   private:
    WifiStrengthController(QObject* = nullptr) = delete;
    WifiStrengthController(WifiStrengthController const&) = delete;
    WifiStrengthController& operator=(WifiStrengthController const&) = delete;

    void handleWifiData();

   private:
    IWifiStrengthProvider& m_provider;
    unsigned long m_cbId;
    QString m_networkName;
};
