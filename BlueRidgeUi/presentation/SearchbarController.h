#pragma once

/**
 * @file   SearchbarController.h
 *
 * \brief This file contains the declaration of the Search Bar Controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the pressure search bar.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>

class ISearchbarProvider;

class SearchbarController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int searchValue READ searchValue WRITE setSearchValue NOTIFY searchValueChanged)
    Q_PROPERTY(bool autoSearching READ autoSearching NOTIFY autoSearchingChanged)
    Q_PROPERTY(bool userEnabled READ userEnabled WRITE setUserEnabled NOTIFY userEnabledChanged)

   public:
    explicit SearchbarController(ISearchbarProvider& searchProvider, QObject* parent = nullptr);
    virtual ~SearchbarController();

    Q_INVOKABLE void startAutoSearching();

   signals:
    void searchValueChanged(int);
    void autoSearchingChanged(bool);
    void userEnabledChanged(bool);

   public slots:
    int searchValue() const;
    void setSearchValue(int value);
    bool autoSearching() const;
    bool userEnabled() const;
    void setUserEnabled(bool enabled);

   private:
    SearchbarController(QObject* = nullptr) = delete;
    SearchbarController(SearchbarController const&) = delete;
    SearchbarController& operator=(SearchbarController const&) = delete;

    void handleSearchValueUpdated();
    void handleAutoSearchingUpdated();

   private:
    ISearchbarProvider& m_searchProvider;
    unsigned long m_searchCbId;
    unsigned long m_autoCbId;
    int m_searchValue;
    bool m_userEnabled;
};
