#pragma once

/**
 * @file   PatientController.h
 *
 * \brief This file declares the Patient Controller class.
 *
 * Description:
 * Provides the UI with controls and data to interact with a patient.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>
#include <QVariant>

class IUsbProvider;
class IPatientProvider;
class ITransactionProvider;
class PatientInfo;

class PatientController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(PatientInfo* currentPatient READ currentPatient WRITE setCurrentPatient)

   public:
    PatientController(IPatientProvider& patientProvider, IUsbProvider& usbProvider,
                      ITransactionProvider& transactionProvider, QObject* parent = nullptr);
    ~PatientController() override;

    PatientInfo* currentPatient();
    void setCurrentPatient(PatientInfo* patient);

    Q_INVOKABLE int getPatientId();
    Q_INVOKABLE void getUsbData();
    Q_INVOKABLE QVariant getPatientBarInfo();
    Q_INVOKABLE void createTransaction(QString eventType);
    Q_INVOKABLE int getTransactionId();
    Q_INVOKABLE void abandonTransaction();
    Q_INVOKABLE void setSensorPosition(QString position);
    Q_INVOKABLE void requestListOfImplantDoctors();
    Q_INVOKABLE void requestListOfClinicians();
    Q_INVOKABLE void requestListOfClinics();
    Q_INVOKABLE void addNewPatient(PatientInfo* patient);

    Q_INVOKABLE QList<QString> getListOfImplantDoctors();
    Q_INVOKABLE QList<QString> getListOfClinicians();
    Q_INVOKABLE QList<QString> getListOfClinics();

   signals:
    void usbDataUpdated();
    void usbData(QString serialNumber);
    void transactionComplete();
    void transactionCommitFinished();
    void patientDataSaved();
    void patientSavedFinished();
    void newPatientSavedFinished();
    void listDataReady();
    void listOfImplantDoctorsReady();
    void listOfCliniciansReady();
    void listOfClinicsReady();

   private slots:
    void handleUsbReady();
    void handleTransactionIdReady();
    void handleTransactionCommitReady();
    void handlePatientSaveComplete();
    void handleListDataReady();

   private:
    PatientController(QObject* = nullptr) = delete;
    PatientController(PatientController const&) = delete;
    PatientController& operator=(PatientController const&) = delete;

    void savePatient();

    void handleTransactionResponse();
    void handleTransactionCommitResponse();
    void handlePatientSavedResponse();
    void handleUsbUpdatedResponse();
    void handleListDataResponse();

   private:
    IPatientProvider& m_patientProvider;
    IUsbProvider& m_usbProvider;
    ITransactionProvider& m_transactionProvider;
    unsigned long m_transactionCbId;
    unsigned long m_patientCbId;
    unsigned long m_usbCbId;
    unsigned long m_listDataCbId;
    unsigned long m_transactionCommitCbId;
    QString m_serialNumber;
    QString m_calCode;
    QString m_baseline;
    PatientInfo* m_currentPatient;
    int m_transactionId;
    QList<QString> m_implantDoctorList;
    QList<QString> m_clinicians;
    QList<QString> m_clinics;
    QList<QString> m_listDataRequests;
    bool m_savingNewPatient;

    void handleImplantDoctors(std::vector<std::string> dataList);
    void handleClinicians(std::vector<std::string> dataList);
    void handleClinics(std::vector<std::string> dataList);
};
