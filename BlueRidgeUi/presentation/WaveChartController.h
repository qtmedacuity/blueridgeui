/**
 * @file   WaveChartController.h
 *
 * \brief This file declares the Wave Chart Controller base class.
 *
 * Description:
 * Provides a base class for chart controllers.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#pragma once

#include "widgets/WaveChart/WaveAxis.h"

#include <QObject>

#include <algorithm>
#include <functional>
#include <mutex>
#include <vector>

namespace QtCharts
{
class QXYSeries;
}

class WaveChartWidget;
class WaveChartController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool zoomInVerticalEnabled READ zoomInVerticalEnabled NOTIFY zoomInVerticalEnabledChanged)
    Q_PROPERTY(bool zoomOutVerticalEnabled READ zoomOutVerticalEnabled NOTIFY zoomOutVerticalEnabledChanged)
    Q_PROPERTY(bool zoomInHorizontalEnabled READ zoomInHorizontalEnabled NOTIFY zoomInHorizontalEnabledChanged)
    Q_PROPERTY(bool zoomOutHorizontalEnabled READ zoomOutHorizontalEnabled NOTIFY zoomOutHorizontalEnabledChanged)
    Q_PROPERTY(double verticalOffset READ verticalOffset WRITE setVerticalOffset NOTIFY verticalOffsetChanged)
    Q_PROPERTY(double horizontalOffset READ horizontalOffset WRITE setHorizontalOffset NOTIFY horizontalOffsetChanged)

   protected:
    typedef QPair<QVector<QPointF>, QtCharts::QXYSeries*> DataArrays;
    typedef QList<DataArrays> DataArraysList;

   public:
    typedef std::vector<WaveAxis::PointList> PointsLists;

   private:
    struct AxisAttributes {
        double hardMax;
        double hardMin;
        double upperViewPort;
        double lowerViewPort;
        double delta;
        AxisAttributes() = delete;
        explicit AxisAttributes(const WaveAxis::PointList& pointList, const unsigned int numTicks = 10);
        template <class T>
        T clamp(T val, T min, T max)
        {
            return val > min ? val < max ? val : max : min;
        }
        double adjustedLowerViewport(double scrollValue)
        {
            return clamp(lowerViewPort + scrollValue, hardMin, hardMax - delta);
        }
        double adjustedUpperViewport(double scrollValue) { return adjustedLowerViewport(scrollValue) + delta; }
    };
    typedef std::vector<AxisAttributes> AxisAttributesLists;
    typedef std::vector<std::tuple<AxisAttributes, WaveAxis::PointList>> PointSets;

    struct AxisConfiguration {
        unsigned int tickCount;
        unsigned int defaultIndex;
        unsigned int index;
        bool zoomInEnabled;
        bool zoomOutEnabled;
        double scrollBase;
        PointSets pointSets;
        std::function<void(bool)> zoomInSignal;
        std::function<void(bool)> zoomOutSignal;
        WaveAxis* component;
        AxisConfiguration()
            : tickCount(0),
              defaultIndex(0),
              index(0),
              zoomInEnabled(false),
              zoomOutEnabled(false),
              scrollBase(0.0),
              pointSets(),
              zoomInSignal(nullptr),
              zoomOutSignal(nullptr),
              component(nullptr)
        {
        }
        AxisAttributes& attributes(unsigned int idx) { return std::get<0>(pointSets.at(idx)); }
        WaveAxis::PointList& pointList(unsigned int idx) { return std::get<1>(pointSets.at(idx)); }
    };

   public:
    explicit WaveChartController(const QString& resource, QObject* parent = nullptr);
    ~WaveChartController() override;

    Q_INVOKABLE virtual void registerSeries(QtCharts::QXYSeries* series);
    Q_INVOKABLE virtual void unregisterSeries(QtCharts::QXYSeries* series);

    Q_INVOKABLE void registerComponent(WaveChartWidget* widget);
    Q_INVOKABLE void unregisterComponent(WaveChartWidget* widget);
    Q_INVOKABLE void zoomInHorizontal();
    Q_INVOKABLE void zoomOutHorizontal();
    Q_INVOKABLE void zoomInVertical();
    Q_INVOKABLE void zoomOutVertical();
    Q_INVOKABLE void scrollHorizontal(double scrollPercent, bool active);
    Q_INVOKABLE void scrollVertical(double scrollPercent, bool active);

   signals:
    void seriesRegistered(QString);
    void seriesUnegistered(QString);
    void zoomInHorizontalEnabledChanged(bool enabled);
    void zoomOutHorizontalEnabledChanged(bool enabled);
    void zoomInVerticalEnabledChanged(bool enabled);
    void zoomOutVerticalEnabledChanged(bool enabled);
    void verticalOffsetChanged(double offset);
    void horizontalOffsetChanged(double offset);

   public slots:
    bool zoomInHorizontalEnabled() const;
    bool zoomOutHorizontalEnabled() const;
    bool zoomInVerticalEnabled() const;
    bool zoomOutVerticalEnabled() const;
    double verticalOffset() const;
    void setVerticalOffset(double offset);
    double horizontalOffset() const;
    void setHorizontalOffset(double offset);

   private:
    explicit WaveChartController(QObject* = nullptr) = delete;
    WaveChartController(WaveChartController const&) = delete;
    WaveChartController& operator=(WaveChartController const&) = delete;

    void fromJSON(const std::string& configString);
    void configureComponent(AxisConfiguration& axis, double scrollValue);
    void updateComponent(AxisConfiguration& axis, double scrollValue);
    void zoomInComponent(AxisConfiguration& axis);
    void zoomOutComponent(AxisConfiguration& axis);

   protected:
    AxisConfiguration m_xAxisConfig;
    AxisConfiguration m_yAxisConfig;
    double m_verticalOffset;
    double m_horizontalOffset;
    WaveChartWidget* m_widget;
    std::mutex m_mutex;
    DataArraysList m_dataArraysList;
};
