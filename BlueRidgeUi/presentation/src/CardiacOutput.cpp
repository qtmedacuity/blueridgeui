/**
 * @file   CardiacOutput.cpp
 *
 * \brief This file contains the definition of the Cardiac Output class.
 *
 * Description:
 * Provides methods and data for Cardiac Output.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "CardiacOutput.h"
#include "presentation/Recording.h"
#include "providers/CardiacOutputData.h"

CardiacOutput::CardiacOutput(QObject *parent)
    : QObject(parent),
      m_date(""),
      m_time(""),
      m_cardiacOutput(0.0),
      m_heartRate(0),
      m_sensorStrength(0),
      m_waveFormFile("")
{
}

CardiacOutput::CardiacOutput(CardiacOutputData *coData, QObject *parent) : QObject(parent)
{
    m_date = QString().fromStdString(coData->date);
    m_time = QString().fromStdString(coData->time);
    m_cardiacOutput = coData->cardiacOutput;
    m_heartRate = coData->heartRate;
    m_sensorStrength = coData->signalStrength;
    m_waveFormFile = QString().fromStdString(coData->waveformFile);
}

CardiacOutput::CardiacOutput(Recording *recording, float coValue)
{
    m_date = recording->recordingDate();
    m_time = recording->recordingTime();
    m_heartRate = recording->heartRate();
    m_sensorStrength = recording->sensorStrength();
    m_waveFormFile = recording->waveFormFile();
    m_cardiacOutput = coValue;
}

CardiacOutputData CardiacOutput::toCardiacOutputData() const
{
    CardiacOutputData coData;
    coData.date = m_date.toStdString();
    coData.time = m_time.toStdString();
    coData.heartRate = static_cast<int>(m_heartRate);
    coData.signalStrength = static_cast<int>(m_sensorStrength);
    coData.cardiacOutput = static_cast<float>(m_cardiacOutput);
    coData.waveformFile = m_waveFormFile.toStdString();
    return coData;
}

QString CardiacOutput::date() const { return m_date; }
void CardiacOutput::setDate(QString date)
{
    if (m_date != date) {
        m_date = date;
        emit cardiacOutputChanged();
    }
}

QString CardiacOutput::time() const { return m_time; }
void CardiacOutput::setTime(QString time)
{
    if (m_time != time) {
        m_time = time;
        emit cardiacOutputChanged();
    }
}

float CardiacOutput::cardiacOutput() const { return m_cardiacOutput; }
void CardiacOutput::setCardiacOutput(float output)
{
    if (m_cardiacOutput != output) {
        m_cardiacOutput = output;
        emit cardiacOutputChanged();
    }
}

int CardiacOutput::heartRate() const { return m_heartRate; }
void CardiacOutput::setHeartRate(int heartRate)
{
    if (m_heartRate != heartRate) {
        m_heartRate = heartRate;
        emit cardiacOutputChanged();
    }
}

int CardiacOutput::sensorStrength() const { return m_sensorStrength; }
void CardiacOutput::setSensorStrength(int sensorStrength)
{
    if (m_sensorStrength != sensorStrength) {
        m_sensorStrength = sensorStrength;
        emit cardiacOutputChanged();
    }
}

QString CardiacOutput::waveFormFile() const { return m_waveFormFile; }
void CardiacOutput::setWaveFormFile(QString waveFormFile)
{
    if (m_waveFormFile != waveFormFile) {
        m_waveFormFile = waveFormFile;
        emit cardiacOutputChanged();
    }
}
