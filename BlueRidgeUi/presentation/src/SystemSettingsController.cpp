/**
 * @file SystemSettingsController.cpp
 *
 * \brief This file defines the controller of system settings.
 *
 * Description:
 * Provides access to commands and data for system settings management.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/
#include "presentation/SystemSettingsController.h"
#include "providers/DataSocketError.h"
#include "providers/ISensorDataProvider.h"
#include "providers/ISystemSettingsProvider.h"

#include <limits>

SystemSettingsController::SystemSettingsController(ISystemSettingsProvider& settingsProvider, QObject* parent)
    : QObject(parent),
      m_settingsProvider(settingsProvider),
      m_rfStateCbId(std::numeric_limits<unsigned long>().max()),
      m_sensorErrorCbId(std::numeric_limits<unsigned long>().max()),
      m_configCbId(std::numeric_limits<unsigned long>().max())
{
    QObject::connect(this, &SystemSettingsController::rfStateChanged, [&]() {
        if (rfState()) {
            m_settingsProvider.enableSensorDataSource();
        } else {
            m_settingsProvider.disableSensorDataSource();
        }
    });

    m_configCbId = m_settingsProvider.registerConfigurationCallback([&]() { handleConfigurationUpdate(); });
    m_rfStateCbId = m_settingsProvider.registerRFStateCallback([&]() { handleRFStateUpdate(); });
    m_sensorErrorCbId = m_settingsProvider.registerSensorDataSocketErrorCallback([&](DataSocketError err) {
        if (DataSocketError::ReadFailed == err) {
            m_settingsProvider.requestRFState();
        } else if (DataSocketError::ConnectionFailed == err || DataSocketError::Unknown == err) {
            m_settingsProvider.disconnectSensorDataSocket();
            m_settingsProvider.connectSensorDataSocket();
        }
    });

    m_settingsProvider.requestConfiguration();
    m_settingsProvider.requestRFState();
}

SystemSettingsController::~SystemSettingsController()
{
    m_settingsProvider.unregisterSensorDataSocketErrorCallback(m_sensorErrorCbId);
    m_settingsProvider.unregisterRFStateCallback(m_rfStateCbId);
    m_settingsProvider.unregisterConfigurationCallback(m_configCbId);
}

void SystemSettingsController::enableSensorDataStream()
{
    QMetaObject::invokeMethod(this, "enableSensorData", Qt::QueuedConnection);
}

void SystemSettingsController::disableSensorDataStream()
{
    QMetaObject::invokeMethod(this, "disableSensorData", Qt::QueuedConnection);
}

bool SystemSettingsController::rfState() const { return m_settingsProvider.rfState(); }

void SystemSettingsController::setRFState(bool state) { m_settingsProvider.setRFState(state); }

QString SystemSettingsController::defaultLanguage() const
{
    return QString().fromStdString(m_settingsProvider.configuration()._language);
}

unsigned int SystemSettingsController::paMeanMinimum() const { return m_settingsProvider.configuration()._paMean._min; }

unsigned int SystemSettingsController::paMeanMaximum() const { return m_settingsProvider.configuration()._paMean._max; }

int SystemSettingsController::searchPressureMinimum() const
{
    return m_settingsProvider.configuration()._searchPressure._min;
}

int SystemSettingsController::searchPressureMaximum() const
{
    return m_settingsProvider.configuration()._searchPressure._max;
}

int SystemSettingsController::searchPressureStart() const
{
    return m_settingsProvider.configuration()._searchPressure._default;
}

unsigned int SystemSettingsController::systolicMinimum() const
{
    return m_settingsProvider.configuration()._systolic._min;
}

unsigned int SystemSettingsController::systolicMaximum() const
{
    return m_settingsProvider.configuration()._systolic._max;
}

unsigned int SystemSettingsController::diastolicMinimum() const
{
    return m_settingsProvider.configuration()._diastolic._min;
}

unsigned int SystemSettingsController::diastolicMaximum() const
{
    return m_settingsProvider.configuration()._diastolic._max;
}

unsigned int SystemSettingsController::referenceMeanMinimum() const
{
    return m_settingsProvider.configuration()._referenceMean._min;
}

unsigned int SystemSettingsController::referenceMeanMaximum() const
{
    return m_settingsProvider.configuration()._referenceMean._max;
}

void SystemSettingsController::enableSensorData() { m_settingsProvider.enableSensorDataStream(); }

void SystemSettingsController::disableSensorData() { m_settingsProvider.disableSensorDataStream(); }

void SystemSettingsController::rfStateUpdated() { emit rfStateChanged(rfState()); }

void SystemSettingsController::configurationUpdated() { emit configurationChanged(); }

void SystemSettingsController::handleRFStateUpdate()
{
    QMetaObject::invokeMethod(this, "rfStateUpdated", Qt::QueuedConnection);
}

void SystemSettingsController::handleConfigurationUpdate()
{
    QMetaObject::invokeMethod(this, "configurationUpdated", Qt::QueuedConnection);
}
