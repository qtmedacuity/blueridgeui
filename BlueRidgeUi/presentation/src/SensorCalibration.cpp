/**
 * @file   SensorCalibration.cpp
 *
 * \brief This file contains the definition of the Sensor Calibration class.
 *
 * Description:
 * Provides methods and data for sensor calibration.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "SensorCalibration.h"
#include "providers/SensorCalibrationData.h"

SensorCalibration::SensorCalibration(QObject *parent)
    : QObject(parent),
      m_date(""),
      m_time(""),
      m_systolic(0),
      m_diastolic(0),
      m_mean(0),
      m_reference(0),
      m_heartRate(0),
      m_sensorStrength(0),
      m_waveFormFile("")
{
}

SensorCalibration::SensorCalibration(SensorCalibrationData *sensorCalibrationData, QObject *parent) : QObject(parent)
{
    m_date = QString().fromStdString(sensorCalibrationData->date);
    m_time = QString().fromStdString(sensorCalibrationData->time);
    m_systolic = sensorCalibrationData->systolic;
    m_diastolic = sensorCalibrationData->diastolic;
    m_mean = sensorCalibrationData->mean;
    m_reference = sensorCalibrationData->reference;
    m_heartRate = sensorCalibrationData->heartRate;
    m_sensorStrength = sensorCalibrationData->signalStrength;
    m_waveFormFile = QString().fromStdString(sensorCalibrationData->waveformFile);
}

QString SensorCalibration::date() const { return m_date; }
void SensorCalibration::setDate(QString date)
{
    if (m_date != date) {
        m_date = date;
        emit calibrationChanged();
    }
}

QString SensorCalibration::time() const { return m_time; }
void SensorCalibration::setTime(QString time)
{
    if (m_time != time) {
        m_time = time;
        emit calibrationChanged();
    }
}

int SensorCalibration::systolic() const { return m_systolic; }
void SensorCalibration::setSystolic(int systolic)
{
    if (m_systolic != systolic) {
        m_systolic = systolic;
        emit calibrationChanged();
    }
}

int SensorCalibration::diastolic() const { return m_diastolic; }
void SensorCalibration::setDiastolic(int diastolic)
{
    if (m_diastolic != diastolic) {
        m_diastolic = diastolic;
        emit calibrationChanged();
    }
}

int SensorCalibration::mean() const { return m_mean; }
void SensorCalibration::setMean(int mean)
{
    if (m_mean != mean) {
        m_mean = mean;
        emit calibrationChanged();
    }
}

int SensorCalibration::heartRate() const { return m_heartRate; }
void SensorCalibration::setHeartRate(int heartRate)
{
    if (m_heartRate != heartRate) {
        m_heartRate = heartRate;
        emit calibrationChanged();
    }
}

int SensorCalibration::reference() const { return m_reference; }
void SensorCalibration::setReference(int reference)
{
    if (m_reference != reference) {
        m_reference = reference;
        emit calibrationChanged();
    }
}

int SensorCalibration::sensorStrength() const { return m_sensorStrength; }
void SensorCalibration::setSensorStrength(int sensorStrength)
{
    if (m_sensorStrength != sensorStrength) {
        m_sensorStrength = sensorStrength;
        emit calibrationChanged();
    }
}

QString SensorCalibration::waveFormFile() const { return m_waveFormFile; }
void SensorCalibration::setWaveFormFile(QString waveFormFile)
{
    if (m_waveFormFile != waveFormFile) {
        m_waveFormFile = waveFormFile;
        emit calibrationChanged();
    }
}
