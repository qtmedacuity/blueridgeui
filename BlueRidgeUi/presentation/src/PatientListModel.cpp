/**
 * @file   PatientListModel.cpp
 *
 * \brief This file contains the definition of the patient list model class.
 *
 * Description:
 * Provides data, methods and signals needed by the patient list view UI control.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/PatientListModel.h"
#include <QDate>
#include <QDebug>
#include "presentation/PatientInfo.h"
#include "providers/PatientListProvider.h"

PatientListModel::PatientListModel(QObject *parent) : QAbstractListModel(parent) {}

PatientListModel::~PatientListModel()
{
    for (int ii(0); ii < m_patients.size(); ++ii) {
        if (nullptr != m_patients.at(ii)) {
            delete m_patients[ii];
            m_patients[ii] = nullptr;
        }
    }
    m_patients.clear();
}

int PatientListModel::rowCount(const QModelIndex &) const { return m_patients.count(); }

QVariant PatientListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() >= 0 && index.row() < rowCount()) {
        switch (role) {
            case FullNameRole:
                return m_patients.at(index.row())->patientName();
            case DobRole:
                return m_patients.at(index.row())->dob();
            case PatientIdRole:
                return m_patients.at(index.row())->patientId();
            case SerialNumberRole:
                return m_patients.at(index.row())->serialNumber();
            case ImplantDateRole:
                return m_patients.at(index.row())->implantDate();
            case SelectedRole:
                return m_patients.at(index.row())->selected();
            case UploadPendingRole:
                return m_patients.at(index.row())->uploadPending();
            default:
                return QVariant();
        }
    }
    return QVariant();
}

bool PatientListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == SelectedRole) {
        m_patients.at(index.row())->setSelected(value.toBool());
    }
    return true;
}

QHash<int, QByteArray> PatientListModel::roleNames() const
{
    static const QHash<int, QByteArray> roles{{FullNameRole, "fullName"},       {DobRole, "dob"},
                                              {PatientIdRole, "patientId"},     {SerialNumberRole, "serialNumber"},
                                              {ImplantDateRole, "implantDate"}, {UploadPendingRole, "uploadPending"},
                                              {SelectedRole, "selected"}};
    return roles;
}

PatientInfo *PatientListModel::get(int index)
{
    if (index >= 0 && index < m_patients.size()) {
        PatientInfo *foundPatient = m_patients.value(index);
        return foundPatient;
    }
    return nullptr;
}

static int m_sortColumn = 0;

bool compareDateStrings(QString dateStr1, QString dateStr2, bool lessThan)
{
    if ((dateStr1.isNull() || dateStr1.isEmpty()) && (dateStr2.isNull() || dateStr2.isEmpty())) {
        return false;
    }
    if ((dateStr1.isNull() || dateStr1.isEmpty()) && !dateStr2.isEmpty()) {
        return (lessThan ? true : false);
    }
    if ((dateStr2.isNull() || dateStr2.isEmpty()) && !dateStr1.isEmpty()) {
        return (lessThan ? false : true);
    }
    QDate date1 = QDate::fromString(dateStr1, "d MMM yyyy");
    QDate date2 = QDate::fromString(dateStr2, "d MMM yyyy");
    if (lessThan) {
        return date1 < date2;
    } else {
        return date1 > date2;
    }
}

bool lessThan(const PatientInfo *pat1, const PatientInfo *pat2)
{
    switch (m_sortColumn) {
        case 0:
            return pat1->patientName() < pat2->patientName();
        case 1:
            return compareDateStrings(pat1->dob(), pat2->dob(), true);
        case 2:
            return pat1->patientId() < pat2->patientId();
        case 3:
            return pat1->serialNumber() < pat2->serialNumber();
        case 4:
            return compareDateStrings(pat1->implantDate(), pat2->implantDate(), true);
        default:
            return false;
    }
}

bool greaterThan(const PatientInfo *pat1, const PatientInfo *pat2)
{
    switch (m_sortColumn) {
        case 0:
            return pat1->patientName() > pat2->patientName();
        case 1:
            return compareDateStrings(pat1->dob(), pat2->dob(), false);
        case 2:
            return pat1->patientId() > pat2->patientId();
        case 3:
            return pat1->serialNumber() > pat2->serialNumber();
        case 4:
            return compareDateStrings(pat1->implantDate(), pat2->implantDate(), false);
        default:
            return false;
    }
}

void PatientListModel::sort(int column, Qt::SortOrder order)
{
    m_sortColumn = column;
    if (order == Qt::DescendingOrder) {
        std::sort(m_patients.begin(), m_patients.end(), greaterThan);
    } else {
        std::sort(m_patients.begin(), m_patients.end(), lessThan);
    }
}

QList<PatientInfo *> PatientListModel::getSelectedPatients()
{
    QList<PatientInfo *> patients;
    foreach (PatientInfo *pat, m_patients) {
        if (pat->selected()) {
            patients.append(pat);
        }
    }
    return patients;
}

int PatientListModel::selectAllPatients(bool selected)
{
    foreach (PatientInfo *pat, m_patients) {
        pat->setSelected(selected);
    }
    return selected ? m_patients.size() : 0;
}

void PatientListModel::setPatientList(QList<PatientInfo *> patients)
{
    this->m_patients.clear();
    this->m_patients = patients;
    this->sort(0, Qt::AscendingOrder);
    this->endResetModel();
}
