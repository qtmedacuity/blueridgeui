/**
 * @file   CellularStrengthController.cpp
 *
 * \brief This file contains the definition of the Cellular Strength Controller class.
 *
 * Description:
 * Provides the UI with Cellular Status and update signals.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/CellularStrengthController.h"
#include "providers/ICellularStrengthProvider.h"

#include <iostream>

CellularStrengthController::CellularStrengthController(ICellularStrengthProvider& provider,
                                                       StatusValueList const& barvalues)
    : SignalStrengthController(barvalues), m_provider(provider), m_carrier("")
{
    QObject::connect(this, &CellularStrengthController::cellularDataUpdated, this,
                     &CellularStrengthController::setCellStatus);
    m_cbId = m_provider.registerCellularStrengthCallback([&] { handleCellularData(); });
}

CellularStrengthController::~CellularStrengthController() { m_provider.unregisterCellularStrengthCallback(m_cbId); }

void CellularStrengthController::setCarrier(QString const& cellCarrier)
{
    QString carrier(cellCarrier);
    if (!m_provider.isSet()) {
        carrier = tr("Not Initialized");
    } else if (!m_provider.isEnabled()) {
        carrier = tr("Disabled");
    } else if (!m_provider.isConnected()) {
        carrier = tr("No Carrier");
    }

    if (carrier != m_carrier) {
        m_carrier = carrier;
        emit carrierChanged(m_carrier);
    }
}

void CellularStrengthController::setStrength(int signal)
{
    if (m_provider.isSet() && m_provider.isEnabled() && m_provider.isConnected()) {
        SignalStrengthController::setStrength(signal);
    } else {
        SignalStrengthController::setStrength(-1.0);
    }
}

void CellularStrengthController::setCellStatus()
{
    setCarrier(QString().fromStdString(m_provider.getCarrier()));
    setStrength(m_provider.getStrength());
}

void CellularStrengthController::handleCellularData()
{
    QMetaObject::invokeMethod(this, "cellularDataUpdated", Qt::QueuedConnection);
}
