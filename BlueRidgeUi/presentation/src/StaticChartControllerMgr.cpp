#include "presentation/StaticChartControllerMgr.h"
#include "presentation/StaticChartController.h"

StaticChartControllerMgr::StaticChartControllerMgr(const QString& resource, QObject* parent)
    : QObject(parent), m_resource(resource)
{
}

StaticChartControllerMgr::~StaticChartControllerMgr() {}

StaticChartController* StaticChartControllerMgr::controller(const QString& source)
{
    ControllerMap::iterator iter(m_controllers.find(source));
    if (iter == m_controllers.end()) {
        iter = m_controllers.insert(source, new StaticChartController(m_resource, source, this));
    }

    return iter.value();
}
