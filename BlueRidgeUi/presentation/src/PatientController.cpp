/**
 * @file   PatientController.cpp
 *
 * \brief This file defines the Patient Controller class.
 *
 * Description:
 * Provides the UI with controls and data to interact with a patient.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/PatientController.h"
#include "presentation/PatientInfo.h"
#include "providers/IPatientProvider.h"
#include "providers/ITransactionProvider.h"
#include "providers/IUsbProvider.h"

#include <QJsonDocument>
#include <QJsonObject>

PatientController::PatientController(IPatientProvider& patientProvider, IUsbProvider& usbProvider,
                                     ITransactionProvider& transactionProvider, QObject* parent)
    : QObject(parent),
      m_patientProvider(patientProvider),
      m_usbProvider(usbProvider),
      m_transactionProvider(transactionProvider),
      m_transactionCbId(static_cast<unsigned long>(-1)),
      m_patientCbId(static_cast<unsigned long>(-1)),
      m_usbCbId(static_cast<unsigned long>(-1)),
      m_currentPatient(nullptr),
      m_transactionId(0),
      m_savingNewPatient(false)
{
    QObject::connect(this, &PatientController::transactionComplete, this, &PatientController::handleTransactionIdReady);
    QObject::connect(this, &PatientController::patientDataSaved, this, &PatientController::handlePatientSaveComplete);
    QObject::connect(this, &PatientController::usbDataUpdated, this, &PatientController::handleUsbReady);
    QObject::connect(this, &PatientController::listDataReady, this, &PatientController::handleListDataReady);
    QObject::connect(this, &PatientController::transactionCommitFinished, this,
                     &PatientController::handleTransactionCommitReady);

    m_transactionCbId = m_transactionProvider.registerTransactionReadyCallback([&] { handleTransactionResponse(); });
    m_transactionCommitCbId =
        m_transactionProvider.registerCommitTransactionCallback([&] { handleTransactionCommitResponse(); });
    m_patientCbId = m_patientProvider.registerPatientSavedCallback([&] { handlePatientSavedResponse(); });
    m_usbCbId = m_usbProvider.registerUsbCallback([&] { handleUsbUpdatedResponse(); });
    m_listDataCbId = m_patientProvider.registerListDataCallback([&] { handleListDataResponse(); });
}

PatientController::~PatientController()
{
    m_usbProvider.unregisterUsbCallback(m_usbCbId);
    m_patientProvider.unregisterPatientSavedCallback(m_patientCbId);
    m_patientProvider.unregisterListDataCallback(m_listDataCbId);
    m_transactionProvider.unregisterTransactionReadyCallback(m_transactionCbId);
    m_transactionProvider.unregisterCommitTransactionCallback(m_transactionCommitCbId);
}

PatientInfo* PatientController::currentPatient() { return m_currentPatient; }

void PatientController::setCurrentPatient(PatientInfo* patient) { m_currentPatient = patient; }

int PatientController::getPatientId() { return m_currentPatient->patientId(); }

void PatientController::getUsbData() { m_usbProvider.requestUsbStatus(); }

QList<QString> PatientController::getListOfImplantDoctors() { return m_implantDoctorList; }

QList<QString> PatientController::getListOfClinicians() { return m_clinicians; }

QList<QString> PatientController::getListOfClinics() { return m_clinics; }

QVariant PatientController::getPatientBarInfo()
{
    QJsonObject patientBarInfo;
    patientBarInfo.insert("patientName", m_currentPatient ? m_currentPatient->patientName() : "");
    patientBarInfo.insert("dob", m_currentPatient ? m_currentPatient->dob() : "");
    QString patientSerialNumber = m_currentPatient ? m_currentPatient->serialNumber() : "";
    patientBarInfo.insert("serialNumber", patientSerialNumber.isNull() || patientSerialNumber.isEmpty()
                                              ? m_serialNumber
                                              : patientSerialNumber);
    QJsonDocument doc(patientBarInfo);
    return QVariant::fromValue(doc.toJson());
}

void PatientController::createTransaction(QString eventType)
{
    if (nullptr != m_currentPatient) {
        m_transactionProvider.requestTransactionCreation(m_currentPatient->patientId(), eventType.toStdString());
    }
}

int PatientController::getTransactionId() { return m_transactionId; }

void PatientController::abandonTransaction() { m_transactionProvider.abandonTransaction(m_transactionId); }

void PatientController::setSensorPosition(QString position)
{
    if (nullptr != m_currentPatient) {
        m_currentPatient->setImplantLocation(position);
        savePatient();
    }
}

void PatientController::requestListOfImplantDoctors()
{
    m_implantDoctorList.clear();
    m_listDataRequests.append("ImplantDoctors");
    m_patientProvider.requestListData("ImplantDoctors");
}

void PatientController::requestListOfClinicians()
{
    m_clinicians.clear();
    m_listDataRequests.append("Clinicians");
    m_patientProvider.requestListData("Clinicians");
}

void PatientController::requestListOfClinics()
{
    m_clinics.clear();
    m_listDataRequests.append("Clinics");
    m_patientProvider.requestListData("Clinics");
}

void PatientController::addNewPatient(PatientInfo* patient)
{
    m_currentPatient = patient;
    m_currentPatient->setPatientName(patient->lastName() + ", " + patient->firstName() + " " +
                                     patient->middleInitial());
    m_savingNewPatient = true;
    // saving a new patient requires that a transaction is created.
    createTransaction("");
}

void PatientController::handleUsbReady()
{
    m_serialNumber = QString().fromStdString(m_usbProvider.getSerialNumber());
    m_calCode = QString().fromStdString(m_usbProvider.getCalCode());
    m_baseline = QString().fromStdString(m_usbProvider.getBaseline());
    emit usbData(m_serialNumber);
}

void PatientController::handleTransactionIdReady()
{
    m_transactionId = m_transactionProvider.getTransactionId();
    // if created transaction to save new patient, call save patient
    if (m_savingNewPatient && m_currentPatient != nullptr) {
        savePatient();
    }
}

void PatientController::handleTransactionCommitReady()
{
    // if committed due to saving a new patient, reset flag and emit signal.
    if (m_savingNewPatient) {
        m_savingNewPatient = false;
        emit newPatientSavedFinished();
    }
}

void PatientController::handlePatientSaveComplete()
{
    if (nullptr != m_currentPatient && m_currentPatient->patientId() == 0) {
        m_currentPatient->setPatientId(m_patientProvider.getPatientId());
    }
    // if saving a new patient, then need to commit the transaction, otherwise
    // emit the patient saved signal
    if (m_savingNewPatient && m_currentPatient != nullptr) {
        m_transactionProvider.commitTransaction(m_transactionId, m_currentPatient->patientId());
    } else {
        emit patientSavedFinished();
    }
}

void PatientController::handleListDataReady()
{
    for (QString type : m_listDataRequests) {
        std::vector<std::string> dataList = m_patientProvider.getListData(type.toStdString());
        if (dataList.size() > 0) {
            if (type == "ImplantDoctors") {
                handleImplantDoctors(dataList);
            } else if (type == "Clinicians") {
                handleClinicians(dataList);
            } else {
                handleClinics(dataList);
            }
        }
    }
}

void PatientController::handleImplantDoctors(std::vector<std::string> dataList)
{
    for (std::string value : dataList) {
        m_implantDoctorList.append(QString::fromStdString(value));
    }
    m_listDataRequests.removeOne("ImplantDoctors");
    emit listOfImplantDoctorsReady();
}

void PatientController::handleClinicians(std::vector<std::string> dataList)
{
    for (std::string value : dataList) {
        m_clinicians.append(QString::fromStdString(value));
    }
    m_listDataRequests.removeOne("Clinicians");
    emit listOfCliniciansReady();
}

void PatientController::handleClinics(std::vector<std::string> dataList)
{
    for (std::string value : dataList) {
        m_clinics.append(QString::fromStdString(value));
    }
    m_listDataRequests.removeOne("Clinics");
    emit listOfClinicsReady();
}

void PatientController::savePatient()
{
    if (nullptr == m_currentPatient) return;

    if (m_currentPatient->calCode().isNull() || m_currentPatient->calCode().isEmpty()) {
        // set the usb data into the patient
        m_currentPatient->setCalCode(m_calCode);
        m_currentPatient->setBaselineCode(m_baseline);
    }
    m_patientProvider.savePatient(m_transactionId, m_currentPatient->toPatientData());
}

void PatientController::handleTransactionResponse()
{
    QMetaObject::invokeMethod(this, "transactionComplete", Qt::QueuedConnection);
}

void PatientController::handleTransactionCommitResponse()
{
    QMetaObject::invokeMethod(this, "transactionCommitFinished", Qt::QueuedConnection);
}

void PatientController::handlePatientSavedResponse()
{
    QMetaObject::invokeMethod(this, "patientDataSaved", Qt::QueuedConnection);
}

void PatientController::handleUsbUpdatedResponse()
{
    QMetaObject::invokeMethod(this, "usbDataUpdated", Qt::QueuedConnection);
}

void PatientController::handleListDataResponse()
{
    QMetaObject::invokeMethod(this, "listDataReady", Qt::QueuedConnection);
}
