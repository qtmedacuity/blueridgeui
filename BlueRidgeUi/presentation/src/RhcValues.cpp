/**
 * @file   RhcValues.cpp
 *
 * \brief This file contains the definition of the Rhc Values class.
 *
 * Description:
 * Provides methods and data for Rhc Values.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "RhcValues.h"
#include "RhcData.h"

RhcValues::RhcValues(QObject* parent)
    : QObject(parent),
      m_date(""),
      m_time(""),
      m_raValue(0),
      m_rvSystolic(0),
      m_rvDiastolic(0),
      m_pcwpValue(0),
      m_paSystolic(0),
      m_paDiastolic(0),
      m_paMean(0),
      m_heartRate(0),
      m_sensorStrength(0),
      m_waveFormFile("")
{
}

RhcValues::RhcValues(RhcData const* rhcData, QObject* parent) : QObject(parent)
{
    if (nullptr != rhcData) {
        m_date = QString().fromStdString(rhcData->date);
        m_time = QString().fromStdString(rhcData->time);
        m_raValue = rhcData->ra;
        m_rvSystolic = rhcData->rvSystolic;
        m_rvDiastolic = rhcData->rvDiastoic;
        m_pcwpValue = rhcData->pcwp;
        m_paSystolic = rhcData->paSystolic;
        m_paDiastolic = rhcData->paDiastolic;
        m_paMean = rhcData->paMean;
        m_heartRate = rhcData->heartRate;
        m_sensorStrength = rhcData->signalStrength;
        m_waveFormFile = QString().fromStdString(rhcData->waveformFile);
    }
}

QString RhcValues::date() const { return m_date; }
void RhcValues::setDate(QString date)
{
    if (m_date != date) {
        m_date = date;
        emit rhcValuesChanged();
    }
}

QString RhcValues::time() const { return m_time; }
void RhcValues::setTime(QString time)
{
    if (m_time != time) {
        m_time = time;
        emit rhcValuesChanged();
    }
}

int RhcValues::raValue() const { return m_raValue; }
void RhcValues::setRaValue(int value)
{
    if (m_raValue != value) {
        m_raValue = value;
        emit rhcValuesChanged();
    }
}

int RhcValues::rvSystolic() const { return m_rvSystolic; }
void RhcValues::setRvSystolic(int systolic)
{
    if (m_rvSystolic != systolic) {
        m_rvSystolic = systolic;
        emit rhcValuesChanged();
    }
}

int RhcValues::rvDiastolic() const { return m_rvDiastolic; }
void RhcValues::setRvDiastolic(int diastolic)
{
    if (m_rvDiastolic != diastolic) {
        m_rvDiastolic = diastolic;
        emit rhcValuesChanged();
    }
}

int RhcValues::pcwpValue() const { return m_pcwpValue; }
void RhcValues::setPcwpValue(int value)
{
    if (m_pcwpValue != value) {
        m_pcwpValue = value;
        emit rhcValuesChanged();
    }
}

int RhcValues::paSystolic() const { return m_paSystolic; }
void RhcValues::setPaSystolic(int systolic)
{
    if (m_paSystolic != systolic) {
        m_paSystolic = systolic;
        emit rhcValuesChanged();
    }
}

int RhcValues::paDiastolic() const { return m_paDiastolic; }
void RhcValues::setPaDiastolic(int diastolic)
{
    if (m_paDiastolic != diastolic) {
        m_paDiastolic = diastolic;
        emit rhcValuesChanged();
    }
}

int RhcValues::paMean() const { return m_paMean; }
void RhcValues::setPaMean(int mean)
{
    if (m_paMean != mean) {
        m_paMean = mean;
        emit rhcValuesChanged();
    }
}

int RhcValues::heartRate() const { return m_heartRate; }
void RhcValues::setHeartRate(int heartRate)
{
    if (m_heartRate != heartRate) {
        m_heartRate = heartRate;
        emit rhcValuesChanged();
    }
}

int RhcValues::sensorStrength() const { return m_sensorStrength; }
void RhcValues::setSensorStrength(int sensorStrength)
{
    if (m_sensorStrength != sensorStrength) {
        m_sensorStrength = sensorStrength;
        emit rhcValuesChanged();
    }
}

QString RhcValues::waveFormFile() const { return m_waveFormFile; }
void RhcValues::setWaveFormFile(QString waveFormFile)
{
    if (m_waveFormFile != waveFormFile) {
        m_waveFormFile = waveFormFile;
        emit rhcValuesChanged();
    }
}

void RhcValues::toRhcData(RhcData* rhcData) const
{
    if (nullptr != rhcData) {
        rhcData->date = m_date.toStdString();
        rhcData->time = m_time.toStdString();
        rhcData->ra = m_raValue;
        rhcData->rvSystolic = m_rvSystolic;
        rhcData->rvDiastoic = m_rvDiastolic;
        rhcData->pcwp = m_pcwpValue;
        rhcData->paSystolic = m_paSystolic;
        rhcData->paDiastolic = m_paDiastolic;
        rhcData->paMean = m_paMean;
        rhcData->heartRate = m_heartRate;
        rhcData->signalStrength = m_sensorStrength;
        rhcData->waveformFile = m_waveFormFile.toStdString();
    }
}

void RhcValues::fromRhcData(RhcData const* rhcData)
{
    if (nullptr != rhcData) {
        bool changed(false);

        changed |= m_date != QString().fromStdString(rhcData->date);
        m_date = QString().fromStdString(rhcData->date);

        changed |= m_time != QString().fromStdString(rhcData->time);
        m_time = QString().fromStdString(rhcData->time);

        changed |= m_raValue != rhcData->ra;
        m_raValue = rhcData->ra;

        changed |= m_rvSystolic != rhcData->rvSystolic;
        m_rvSystolic = rhcData->rvSystolic;

        changed |= m_rvDiastolic != rhcData->rvDiastoic;
        m_rvDiastolic = rhcData->rvDiastoic;

        changed |= m_pcwpValue != rhcData->pcwp;
        m_pcwpValue = rhcData->pcwp;

        changed |= m_paSystolic != rhcData->paSystolic;
        m_paSystolic = rhcData->paSystolic;

        changed |= m_paDiastolic != rhcData->paDiastolic;
        m_paDiastolic = rhcData->paDiastolic;

        changed |= m_paMean != rhcData->paMean;
        m_paMean = rhcData->paMean;

        changed |= m_heartRate != rhcData->heartRate;
        m_heartRate = rhcData->heartRate;

        changed |= m_sensorStrength != rhcData->signalStrength;
        m_sensorStrength = rhcData->signalStrength;

        changed |= m_waveFormFile != QString().fromStdString(rhcData->waveformFile);
        m_waveFormFile = QString().fromStdString(rhcData->waveformFile);

        if (changed) {
            emit rhcValuesChanged();
        }
    }
}
