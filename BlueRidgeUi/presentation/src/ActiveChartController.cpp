/**
 * @file   ActiveChartController.cpp
 *
 * \brief This file defines the Active Chart Controller class.
 *
 * Description:
 * Provides a controller to manage streaming data to a WaveChartWidget.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/ActiveChartController.h"
#include "providers/ISensorDataProvider.h"

#include <QDebug>
#include <QtCharts/QXYSeries>

#include <iostream>
#include <limits>

ActiveChartController::ActiveChartController(ISensorDataProvider& provider, const QString& resource, QObject* parent)
    : WaveChartController(resource, parent),
      m_provider(provider),
      m_rateCbId(std::numeric_limits<unsigned long>::max()),
      m_dataCbId(std::numeric_limits<unsigned long>::max()),
      m_samplesPerSecond(0),
      m_rangeInSeconds(10.0),
      m_totalSampleRange(0),
      m_gapInSeconds(0.3),
      m_arraysIndex(0),
      m_lastRange(0.0),
      m_lastSequence(0),
      m_adjustSequence(0),
      m_reset(true)
{
    m_rateCbId =
        m_provider.registerDataRateCallback([&](unsigned int dataRateHz) { handleSensorDataRate(dataRateHz); });
    m_dataCbId = m_provider.registerSensorDataPointCallback(
        [&](unsigned long sequence, double pressure) { handleSensorData(sequence, pressure); });
}

ActiveChartController::~ActiveChartController()
{
    m_provider.unregisterSensorDataPointCallback(m_dataCbId);
    m_provider.unregisterDataRateCallback(m_rateCbId);
}

void ActiveChartController::refresh() { QMetaObject::invokeMethod(this, "clearSensorData", Qt::QueuedConnection); }

double ActiveChartController::rangeInSeconds() const { return m_rangeInSeconds; }

void ActiveChartController::setRangeInSeconds(double seconds)
{
    if (std::abs(seconds - m_rangeInSeconds) < std::numeric_limits<double>::epsilon()) return;

    m_rangeInSeconds = seconds;
    m_totalSampleRange = static_cast<unsigned long>(m_samplesPerSecond * m_rangeInSeconds);
    emit rangeInSecondsChanged(m_rangeInSeconds);
}

double ActiveChartController::gapInSeconds() const { return m_gapInSeconds; }

void ActiveChartController::setGapInSeconds(double seconds)
{
    if (std::abs(seconds - m_gapInSeconds) < std::numeric_limits<double>::epsilon()) return;

    m_gapInSeconds = seconds;
    emit gapInSecondsChanged(m_gapInSeconds);
}

void ActiveChartController::clearSensorData()
{
    for (DataArrays& dataArrays : m_dataArraysList) {
        dataArrays.first.clear();
        dataArrays.second->replace(dataArrays.first);
    }
    m_reset = true;
}

void ActiveChartController::updateSensorData(unsigned long sequence, double pressure)
{
    if (m_dataArraysList.empty()) return;

    if (sequence < m_lastSequence) {
        m_reset = true;
    }

    m_lastSequence = sequence;
    if (m_reset) {
        m_adjustSequence = sequence;
        m_reset = false;
    }

    unsigned long index((sequence - m_adjustSequence) % static_cast<unsigned long>(m_totalSampleRange));
    double range((static_cast<double>(index) / static_cast<double>(m_samplesPerSecond)) + m_horizontalOffset);

    DataArrays* primary(&m_dataArraysList[m_arraysIndex]);
    if (m_lastRange > range) {
        m_arraysIndex = ++m_arraysIndex >= m_dataArraysList.count() ? 0 : m_arraysIndex;
        primary = &m_dataArraysList[m_arraysIndex];
    }
    m_lastRange = range;

    primary->first.append(QPointF(range, pressure + m_verticalOffset));

    for (DataArrays& dataArrays : m_dataArraysList) {
        if (&dataArrays != primary) {
            while (!dataArrays.first.empty() && dataArrays.first.first().x() < range + m_gapInSeconds) {
                dataArrays.first.remove(0);
            }
        } else {
            while (!dataArrays.first.empty() &&
                   dataArrays.first.first().x() < range - m_rangeInSeconds + m_gapInSeconds) {
                dataArrays.first.remove(0);
            }
        }

        dataArrays.second->replace(dataArrays.first);
    }
}

void ActiveChartController::updateSensorDataRate(unsigned long dataRateHz)
{
    m_samplesPerSecond = dataRateHz;
    m_totalSampleRange = static_cast<unsigned long>(m_samplesPerSecond * m_rangeInSeconds);
}

void ActiveChartController::handleSensorData(unsigned long sequence, double pressure)
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (0 == m_dataArraysList.count()) return;
    }

    QMetaObject::invokeMethod(this, "updateSensorData", Qt::QueuedConnection, Q_ARG(unsigned long, sequence),
                              Q_ARG(double, pressure));
}

void ActiveChartController::handleSensorDataRate(unsigned int dataRateHz)
{
    unsigned long dataRate(static_cast<unsigned long>(dataRateHz));
    QMetaObject::invokeMethod(this, "updateSensorDataRate", Qt::QueuedConnection, Q_ARG(unsigned long, dataRate));
}
