/**
 * @file SearchbarController.cpp
 *
 * \brief This file contains the definition of the Search Bar Controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the pressure search bar.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/SearchbarController.h"
#include "providers/ISearchbarProvider.h"

SearchbarController::SearchbarController(ISearchbarProvider& searchProvider, QObject* parent)
    : QObject(parent),
      m_searchProvider(searchProvider),
      m_searchCbId(static_cast<unsigned long>(-1)),
      m_autoCbId(static_cast<unsigned long>(-1)),
      m_searchValue(0),
      m_userEnabled(false)
{
    m_searchCbId = m_searchProvider.registerSearchPressureCallback([&] { handleSearchValueUpdated(); });
    m_autoCbId = m_searchProvider.registerAutoSearchingCallback([&] { handleAutoSearchingUpdated(); });
}

SearchbarController::~SearchbarController()
{
    m_searchProvider.unregisterAutoSearchingCallback(m_autoCbId);
    m_searchProvider.unregisterSearchPressureCallback(m_searchCbId);
}

void SearchbarController::startAutoSearching() { m_searchProvider.setAutoSearching(); }

int SearchbarController::searchValue() const { return static_cast<int>(m_searchProvider.searchPressure()); }

void SearchbarController::setSearchValue(int value)
{
    m_searchProvider.setSearchPressure(static_cast<unsigned int>(value > 0 ? value : 0));
}

bool SearchbarController::autoSearching() const { return m_searchProvider.autoSearching(); }

bool SearchbarController::userEnabled() const { return m_userEnabled; }

void SearchbarController::setUserEnabled(bool enabled)
{
    if (enabled != m_userEnabled) {
        m_userEnabled = enabled;
        emit userEnabledChanged(m_userEnabled);
    }
}

void SearchbarController::handleSearchValueUpdated()
{
    QMetaObject::invokeMethod(this, "searchValueChanged", Qt::QueuedConnection,
                              Q_ARG(int, static_cast<int>(m_searchProvider.searchPressure())));
}

void SearchbarController::handleAutoSearchingUpdated()
{
    QMetaObject::invokeMethod(this, "autoSearchingChanged", Qt::QueuedConnection,
                              Q_ARG(bool, m_searchProvider.autoSearching()));
}
