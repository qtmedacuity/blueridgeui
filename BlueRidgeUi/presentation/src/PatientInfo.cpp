/**
 * @file   PatientInfo.cpp
 *
 * \brief This file contains the definition of the patient info class.
 *
 * Description:
 * Provides methods for data of a single patient.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/PatientInfo.h"

PatientInfo::PatientInfo(QObject *parent) : QObject(parent) {}

PatientInfo::PatientInfo(PatientData const &patient, QObject *parent) : QObject(parent)
{
    m_patientId = static_cast<int>(patient.patientId);
    m_firstName = QString().fromStdString(patient.firstName);
    m_middleInitial = QString().fromStdString(patient.middleInitial);
    m_lastName = QString().fromStdString(patient.lastName);
    m_patientName = QString().fromStdString(patient.patientName);
    m_dob = QString().fromStdString(patient.dob);
    m_implantDate = QString().fromStdString(patient.implantDate);
    m_phone = QString().fromStdString(patient.phone);
    m_clinician = QString().fromStdString(patient.clinician);
    m_clinic = QString().fromStdString(patient.clinic);
    m_serialNumber = QString().fromStdString(patient.serialNumber);
    m_implantDoctor = QString().fromStdString(patient.implantDoctor);
    m_implantLocation = QString().fromStdString(patient.implantLocation);
    m_uploadPending = patient.uploadPending;
}

PatientData PatientInfo::toPatientData() const
{
    PatientData data;
    data.patientId = static_cast<unsigned int>(m_patientId);
    data.firstName = m_firstName.toStdString();
    data.middleInitial = m_middleInitial.toStdString();
    data.lastName = m_lastName.toStdString();
    data.dob = m_dob.toStdString();
    data.implantDate = m_implantDate.toStdString();
    data.phone = m_phone.toStdString();
    data.clinic = m_clinic.toStdString();
    data.clinician = m_clinician.toStdString();
    data.implantDoctor = m_implantDoctor.toStdString();
    data.serialNumber = m_serialNumber.toStdString();
    data.implantLocation = m_implantLocation.toStdString();
    data.calCode = m_calCode.toStdString();
    data.baselineCode = m_baselineCode.toStdString();
    data.patientName = m_patientName.toStdString();
    data.uploadPending = m_uploadPending;

    return data;
}

int PatientInfo::patientId() const { return m_patientId; }

void PatientInfo::setPatientId(int patientId)
{
    if (patientId != m_patientId) {
        m_patientId = patientId;
        emit patientInfoChanged();
    }
}

QString PatientInfo::firstName() const { return m_firstName; }

void PatientInfo::setFirstName(QString firstName)
{
    if (firstName != m_firstName) {
        m_firstName = firstName;
        emit patientInfoChanged();
    }
}

QString PatientInfo::middleInitial() const { return m_middleInitial; }

void PatientInfo::setMiddleInitial(QString middleInitial)
{
    if (middleInitial != m_middleInitial) {
        m_middleInitial = middleInitial;
        emit patientInfoChanged();
    }
}

QString PatientInfo::lastName() const { return m_lastName; }

void PatientInfo::setLastName(QString lastName)
{
    if (lastName != m_lastName) {
        m_lastName = lastName;
        emit patientInfoChanged();
    }
}

QString PatientInfo::patientName() const { return m_patientName; }

void PatientInfo::setPatientName(QString name)
{
    if (name != m_patientName) {
        m_patientName = name;
        emit patientInfoChanged();
    }
}

QString PatientInfo::dob() const { return m_dob; }

void PatientInfo::setDob(QString dob)
{
    if (dob != m_dob) {
        m_dob = dob;
        emit patientInfoChanged();
    }
}

QString PatientInfo::implantDate() const { return m_implantDate; }

void PatientInfo::setImplantDate(QString implantDate)
{
    if (m_implantDate != implantDate) {
        m_implantDate = implantDate;
        emit patientInfoChanged();
    }
}

QString PatientInfo::phone() const { return m_phone; }

void PatientInfo::setPhone(QString phone)
{
    if (m_phone != phone) {
        m_phone = phone;
        emit patientInfoChanged();
    }
}

QString PatientInfo::clinician() const { return m_clinician; }

void PatientInfo::setClinician(QString clinician)
{
    if (clinician != m_clinician) {
        m_clinician = clinician;
        emit patientInfoChanged();
    }
}

QString PatientInfo::clinic() const { return m_clinic; }

void PatientInfo::setClinic(QString clinic)
{
    if (m_clinic != clinic) {
        m_clinic = clinic;
        emit patientInfoChanged();
    }
}

QString PatientInfo::serialNumber() const { return m_serialNumber; }

void PatientInfo::setSerialNumber(QString serialNumber)
{
    if (serialNumber != m_serialNumber) {
        m_serialNumber = serialNumber;
        emit patientInfoChanged();
    }
}

QString PatientInfo::implantDoctor() const { return m_implantDoctor; }

void PatientInfo::setImplantDoctor(QString implantDoctor)
{
    if (implantDoctor != m_implantDoctor) {
        m_implantDoctor = implantDoctor;
        emit patientInfoChanged();
    }
}

QString PatientInfo::implantLocation() const { return m_implantLocation; }

void PatientInfo::setImplantLocation(QString implantLocation)
{
    if (implantLocation != m_implantLocation) {
        m_implantLocation = implantLocation;
        emit patientInfoChanged();
    }
}

QString PatientInfo::calCode() const { return m_calCode; }

void PatientInfo::setCalCode(QString calCode)
{
    if (m_calCode != calCode) {
        m_calCode = calCode;
    }
}

QString PatientInfo::baselineCode() const { return m_baselineCode; }

void PatientInfo::setBaselineCode(QString baseline)
{
    if (m_baselineCode != baseline) {
        m_baselineCode = baseline;
    }
}

bool PatientInfo::uploadPending() const { return m_uploadPending; }

void PatientInfo::setUploadPending(bool uploadPending)
{
    if (uploadPending != m_uploadPending) {
        m_uploadPending = uploadPending;
        emit uploadPendingChanged();
    }
}

bool PatientInfo::selected() const { return m_selected; }

void PatientInfo::setSelected(bool selected)
{
    if (selected != m_selected) {
        m_selected = selected;
        emit selectedChanged();
    }
}
