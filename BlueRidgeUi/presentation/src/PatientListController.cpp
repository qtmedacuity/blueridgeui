/**
 * @file   PatientListController.cpp
 *
 * \brief This file contains the definition of the patient list controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the list of
 * patients.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/PatientListController.h"
#include <QDateTime>
#include <QtDebug>
#include <iostream>
#include "presentation/PatientInfo.h"
#include "providers/IPatientListProvider.h"

PatientListController::PatientListController(IPatientListProvider& provider, QObject* parent)
    : QSortFilterProxyModel(parent), m_provider(provider), m_havePendingUploads(false)
{
    connect(this, &PatientListController::patientListUpdated, this, &PatientListController::handleUpdatedPatientList);
    connect(this, &PatientListController::refreshUpdated, this, &PatientListController::handleRefreshedPatientList);
    m_patientCbId = m_provider.registerPatientListCallback([&] { handlePatientListResponse(); });
    m_refreshCbId = m_provider.registerRefreshCallback([&] { handleRefreshResponse(); });
}

PatientListController::~PatientListController()
{
    m_provider.unregisterRefreshCallback(m_refreshCbId);
    m_provider.unregisterPatientListCallback(m_patientCbId);
}

void PatientListController::sortByColumn(int column, bool ascending)
{
    sourceModel()->sort(column, (ascending ? Qt::AscendingOrder : Qt::DescendingOrder));
    this->invalidate();  // this is needed to refresh the screen
}

void PatientListController::setFilterString(QString filter)
{
    this->setFilterCaseSensitivity(Qt::CaseInsensitive);
    this->setFilterFixedString(filter);
}

void PatientListController::getAllPatients()
{
    if (m_patientListModel) {
        delete m_patientListModel;
    }
    m_havePendingUploads = false;
    m_patientListModel = new PatientListModel();
    this->setSourceModel(m_patientListModel);
    this->setFilterFixedString("");
    m_provider.requestAllPatients();
}

void PatientListController::getImplantPatients()
{
    if (m_patientListModel) {
        delete m_patientListModel;
    }
    m_havePendingUploads = false;
    m_patientListModel = new PatientListModel();
    this->setSourceModel(m_patientListModel);
    this->setFilterFixedString("");
    m_provider.requestPatientsWithImplant();
}

void PatientListController::getWaitingImplantPatients()
{
    if (m_patientListModel) {
        delete m_patientListModel;
    }
    m_havePendingUploads = false;
    m_patientListModel = new PatientListModel();
    this->setSourceModel(m_patientListModel);
    this->setFilterFixedString("");
    m_provider.requestPatientsWaitingForImplant();
}

QVariant PatientListController::get(int i)
{
    QModelIndex sourceIndex = mapToSource(index(i, 0));
    PatientInfo* patient = m_patientListModel->get(sourceIndex.row());
    return QVariant::fromValue(patient);
}

void PatientListController::refresh() { m_provider.requestRefresh(); }

int PatientListController::selectAllPatients(bool selected)
{
    int numSelected = m_patientListModel->selectAllPatients(selected);
    this->invalidate();  // this is needed to refresh the screen
    return numSelected;
}

bool PatientListController::havePendingUploads() { return m_havePendingUploads; }

QString PatientListController::lastUpdated() const { return m_lastUpdated; }

void PatientListController::handleUpdatedPatientList()
{
    QList<PatientInfo*> patientInfos;
    for (PatientData& data : m_provider.patientList()) {
        patientInfos.push_back(new PatientInfo(data));
        if (!m_havePendingUploads && data.uploadPending) {
            m_havePendingUploads = true;
        }
    }

    m_patientListModel->setPatientList(patientInfos);
    m_lastUpdated =
        QString().fromStdString(m_provider.refreshDate()) + " " + QString().fromStdString(m_provider.refreshTime());
    emit lastUpdatedChanged(m_lastUpdated);
}

void PatientListController::handleRefreshedPatientList() { emit refreshComplete(); }

void PatientListController::handleRefreshResponse()
{
    QMetaObject::invokeMethod(this, "refreshUpdated", Qt::QueuedConnection);
}

void PatientListController::handlePatientListResponse()
{
    QMetaObject::invokeMethod(this, "patientListUpdated", Qt::QueuedConnection);
}
