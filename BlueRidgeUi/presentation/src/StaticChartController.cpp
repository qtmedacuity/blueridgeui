/**
 * @file   StaticChartController.cpp
 *
 * \brief This file defines the Static Chart Controller class.
 *
 * Description:
 * Provides a controller to manage static data to a WaveChartWidget.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/StaticChartController.h"

#include <QDebug>
#include <QFile>
#include <QXmlStreamReader>
#include <QtCharts/QXYSeries>

const QString StaticChartController::PressureTag("RawPressure");

StaticChartController::StaticChartController(const QString& resource, const QString& sourceFile, QObject* parent)
    : WaveChartController(resource, parent),
      m_sourceFile(),
      m_samplesPerSecond(125),
      m_rangeInSeconds(10.25),
      m_totalSampleRange(0)
{
    QObject::connect(this, &StaticChartController::sourceChanged,
                     [&](QString sourceFile) { readDataFile(sourceFile); });
    QObject::connect(this, &StaticChartController::seriesRegistered, [&](QString) { refresh(); });
    QObject::connect(this, &StaticChartController::verticalOffsetChanged, [&](double) { refresh(); });
    QObject::connect(this, &StaticChartController::horizontalOffsetChanged, [&](double) { refresh(); });

    setSource(sourceFile);
}

StaticChartController::~StaticChartController() {}

void StaticChartController::refresh() { readDataFile(m_sourceFile); }

QString StaticChartController::source() const { return m_sourceFile; }

void StaticChartController::setSource(const QString& sourceFile)
{
    if (0 == sourceFile.size()) return;

    if (sourceFile != m_sourceFile) {
        m_sourceFile = sourceFile;
        emit sourceChanged(m_sourceFile);
    }
}

double StaticChartController::samplesPerSecond() const { return m_samplesPerSecond; }

void StaticChartController::setSamplesPerSecond(unsigned long samples)
{
    if (samples != m_samplesPerSecond) {
        m_samplesPerSecond = samples;
        emit samplesPerSecondChanged(m_samplesPerSecond);
    }
}

double StaticChartController::rangeInSeconds() const { return m_rangeInSeconds; }

void StaticChartController::setRangeInSeconds(double seconds)
{
    if (std::abs(seconds - m_rangeInSeconds) < std::numeric_limits<double>::epsilon()) return;

    m_rangeInSeconds = seconds;
    emit rangeInSecondsChanged(m_rangeInSeconds);
}

void StaticChartController::readDataFile(const QString& sourceFile)
{
    QFile xmlFile(sourceFile);
    if (!xmlFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Error while loading file:" << sourceFile;
        return;
    }

    if (m_dataArraysList.empty()) return;
    for (DataArrays& dataArray : m_dataArraysList) {
        dataArray.first.clear();
    }

    QXmlStreamReader xml(&xmlFile);
    xml.readNextStartElement();
    while (!xml.atEnd()) {
        if (xml.name() == PressureTag && xml.isStartElement()) {
            bool converted(false);
            unsigned long sequence(0);
            double tempValue(0.0);
            double value(0.0);
            QStringList tokens = xml.readElementText().split(',');
            for (QString token : tokens) {
                token.remove(QRegExp("[\\n\\s+]"));
                tempValue = token.toDouble(&converted);
                if (converted) {
                    value = tempValue;
                }
                updateSensorData(sequence++, value);
            }
            break;
        }
        xml.readNextStartElement();
    }
}

void StaticChartController::updateSensorData(unsigned long sequence, double pressure)
{
    if (m_dataArraysList.empty()) return;

    double range((static_cast<double>(sequence) / static_cast<double>(m_samplesPerSecond)) + m_horizontalOffset);
    for (DataArrays& dataArray : m_dataArraysList) {
        dataArray.first.append(QPointF(range, pressure + m_verticalOffset));
        dataArray.second->replace(dataArray.first);
    }
}
