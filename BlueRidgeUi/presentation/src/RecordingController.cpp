/**
 * @file   RecordingController.cpp
 *
 * \brief This file defines the Recording Controller class.
 *
 * Description:
 * Provides the UI with controls and data to interact with a recording.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/RecordingController.h"
#include "presentation/CardiacOutput.h"
#include "presentation/Recording.h"
#include "presentation/RhcValues.h"
#include "providers/CardiacOutputData.h"
#include "providers/IRecordingProvider.h"
#include "providers/ITransactionProvider.h"
#include "providers/PaMeanData.h"
#include "providers/RhcData.h"

#include <QDate>
#include <QDebug>
#include <QFile>
#include <QTime>
#include <QXmlStreamReader>

#include <QJsonDocument>
#include <QJsonObject>
#include <vector>

RecordingController::RecordingController(ITransactionProvider& transactionProvider,
                                         IRecordingProvider& recordingProvider, QObject* parent)
    : QObject(parent),
      m_recordingProvider(recordingProvider),
      m_transactionProvider(transactionProvider),
      m_transactionCbId(static_cast<unsigned long>(-1)),
      m_recordReadyCbId(static_cast<unsigned long>(-1)),
      m_recordStatusCbId(static_cast<unsigned long>(-1)),
      m_takeReadingCbId(static_cast<unsigned long>(-1)),
      m_saveRecordingCbId(static_cast<unsigned long>(-1)),
      m_deleteRecordingCbId(static_cast<unsigned long>(-1)),
      m_setCardiacOutputCbId(static_cast<unsigned long>(-1)),
      m_paMean(0),
      m_paMeanOffset(0),
      m_offsetSystolic(0),
      m_offsetDiastolic(0),
      m_offsetMean(0),
      m_recordingPressures(false),
      m_newRecording(nullptr)
{
    m_recordReadyCbId = m_recordingProvider.registerReadyToRecordCallback([&] { handleReadyToRecordResponse(); });
    m_recordStatusCbId = m_recordingProvider.registerRecordingStatusCallback([&] { handleRecordingStatusResponse(); });
    m_takeReadingCbId = m_recordingProvider.registerTakeReadingCallback([&] { handleTakeReadingResponse(); });
    m_setPaMeanCbId = m_recordingProvider.registerSetPaMeanCallback([&] { handleSetPaMeanResponse(); });
    m_saveRecordingCbId = m_recordingProvider.registerSaveRecordingCallback([&] { handleSaveRecordingResponse(); });
    m_deleteRecordingCbId =
        m_recordingProvider.registerDeleteRecordingCallback([&] { handleDeleteRecordingResponse(); });
    m_setCardiacOutputCbId =
        m_recordingProvider.registerSetCardiacOutputCallback([&] { handleSetCardiacOutputResponse(); });
    m_setRhcValuesCbId = m_recordingProvider.registerSetRhcValuesCallback([&] { handleSetRhcValuesResponse(); });
}

RecordingController::~RecordingController()
{
    m_transactionProvider.unregisterTransactionReadyCallback(m_transactionCbId);
    m_recordingProvider.unregisterRecordingStatusCallback(m_recordStatusCbId);
    m_recordingProvider.unregisterReadyToRecordCallback(m_recordReadyCbId);
    m_recordingProvider.unregisterTakeReadingCallback(m_takeReadingCbId);
    m_recordingProvider.unregisterSetPaMeanCallback(m_setPaMeanCbId);
    m_recordingProvider.unregisterSaveRecordingCallback(m_saveRecordingCbId);
    m_recordingProvider.unregisterSetCardiacOutputCallback(m_setCardiacOutputCbId);
    m_recordingProvider.unregisterSetRhcValuesCallback(m_setRhcValuesCbId);
}

void RecordingController::getRecordingStatus(QString command)
{
    m_recordingProvider.requestRecordingStatus(command.toStdString());
}

void RecordingController::takeReading()
{
    int transactionId = m_transactionProvider.getTransactionId();
    if (transactionId != 0) {
        m_recordingProvider.takeReading(transactionId);
    }
}

void RecordingController::savePaMean()
{
    qDebug() << "saving pa mean: " << m_paMean;
    if (m_paMean >= 0 && m_paMean <= 150) {
        m_recordingProvider.setPaMean(m_transactionProvider.getTransactionId(), m_paMean);
    }
}

void RecordingController::recordPressures()
{
    m_recordingPressures = true;
    this->takeReading();
}

void RecordingController::saveRecording(Recording* recording)
{
    m_recordingProvider.saveRecording(m_transactionProvider.getTransactionId(), recording->toRecordingData());
}

void RecordingController::deleteRecording(int recordingId)
{
    m_recordingProvider.deleteRecording(m_transactionProvider.getTransactionId(), recordingId);
}

void RecordingController::saveCardiacOutput(float cardiacOutput)
{
    CardiacOutput coData(m_newRecording, cardiacOutput);
    m_recordingProvider.setCardiacOutput(m_transactionProvider.getTransactionId(), coData.toCardiacOutputData());
}

void RecordingController::saveRhcValues(RhcValues* rhcValues)
{
    RhcData rhcData;
    rhcValues->toRhcData(&rhcData);
    m_recordingProvider.setRhcValues(m_transactionProvider.getTransactionId(), rhcData);
}

QVariant RecordingController::getNewRecording() { return QVariant::fromValue(m_newRecording); }

void RecordingController::handleReadyToRecord()
{
    if (m_recordingProvider.isReadyToRecord()) {
        emit readyToRecord();
    }
}

void RecordingController::takeReadingReady()
{
    // create new recording object
    createRecording();
    if (m_recordingPressures) {
        m_recordingPressures = false;
        emit recordingPressuresFinished();
    } else {
        emit takeReadingFinished();
    }
}

int getSignalStrenghAverage(QString strengthValString)
{
    QStringList valPieces = strengthValString.split(",");
    std::vector<int> valueList;
    for (QString piece : valPieces) {
        piece.remove(QRegExp("[\\n\\s+]"));
        valueList.push_back(piece.toInt());
    }
    int sum = 0;
    int size = valueList.size();
    for (int i = 0; i < size; i++) {
        sum += valueList[i];
    }

    return sum / size;
}

void RecordingController::createRecording()
{
    m_newRecording = new Recording();
    m_newRecording->setRecordingId(m_recordingProvider.getRecordingId());
    QString dateNow = QDate::currentDate().toString("dd MMM yyyy");
    QString timeNow = QTime::currentTime().toString("hh:mm:ss A");
    m_newRecording->setRecordingDate(dateNow);
    m_newRecording->setRecordingTime(timeNow);
    // default position
    m_newRecording->setPosition("Horizontal");

    // get data from waveform file.
    std::string waveformFile = m_recordingProvider.getWaveFormFileName();
    m_newRecording->setWaveFormFile(QString().fromStdString(waveformFile));

    QFile xmlFile(waveformFile.c_str());
    if (!xmlFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Error while loading file:" << m_newRecording->waveFormFile();
        return;
    }

    QXmlStreamReader xml(&xmlFile);
    xml.readNextStartElement();
    while (!xml.atEnd()) {
        if (xml.name() == "AvgSystolic" && xml.isStartElement()) {
            m_newRecording->setSystolic(xml.readElementText().toInt());
        }
        if (xml.name() == "AvgDiastolic" && xml.isStartElement()) {
            m_newRecording->setDiastolic(xml.readElementText().toInt());
        }
        if (xml.name() == "AvgMean" && xml.isStartElement()) {
            m_newRecording->setMean(xml.readElementText().toInt());
        }
        if (xml.name() == "AvgHR" && xml.isStartElement()) {
            m_newRecording->setHeartRate(xml.readElementText().toInt());
        }
        if (xml.name() == "SignalStrength" && xml.isStartElement()) {
            QString values = xml.readElementText();
            int avgSignalStrength = getSignalStrenghAverage(values);
            m_newRecording->setSensorStrength(avgSignalStrength);
        }
        xml.readNextStartElement();
    }
}

int RecordingController::paMean() const { return m_paMean; }

void RecordingController::setPaMean(int value)
{
    m_paMean = value;
    emit paMeanChanged();
}

int RecordingController::paMeanOffset() const { return m_paMeanOffset; }

int RecordingController::offsetSystolic() const { return m_offsetSystolic; }

int RecordingController::offsetDiastolic() const { return m_offsetDiastolic; }

int RecordingController::offsetMean() const { return m_offsetMean; }

void RecordingController::handleSetPaMeanReady()
{
    PaMeanData paMeanData = m_recordingProvider.getPaMeanData();
    m_paMeanOffset = paMeanData.offset;
    m_offsetSystolic = paMeanData.systolic;
    m_offsetDiastolic = paMeanData.diastolic;
    m_offsetMean = paMeanData.mean;
    emit paMeanChanged();
    emit paMeanSaveComplete();
}

void RecordingController::handleSaveRecording() { emit recordingSaveComplete(); }

void RecordingController::handleDeleteRecording() { emit recordingDeleteComplete(); }

void RecordingController::handleSetCardiacOutputReady() { emit setCardiacOutputComplete(); }

void RecordingController::handleSetRhcValuesReady() { emit setRhcValuesComplete(); }

void RecordingController::handleRecordingStatusResponse()
{
    QMetaObject::invokeMethod(this, "handleReadyToRecord", Qt::QueuedConnection);
}

void RecordingController::handleReadyToRecordResponse()
{
    QMetaObject::invokeMethod(this, "handleReadyToRecord", Qt::QueuedConnection);
}

void RecordingController::handleTakeReadingResponse()
{
    QMetaObject::invokeMethod(this, "takeReadingReady", Qt::QueuedConnection);
}

void RecordingController::handleSetPaMeanResponse()
{
    QMetaObject::invokeMethod(this, "handleSetPaMeanReady", Qt::QueuedConnection);
}

void RecordingController::handleSaveRecordingResponse()
{
    QMetaObject::invokeMethod(this, "handleSaveRecording", Qt::QueuedConnection);
}

void RecordingController::handleDeleteRecordingResponse()
{
    QMetaObject::invokeMethod(this, "handleDeleteRecording", Qt::QueuedConnection);
}

void RecordingController::handleSetCardiacOutputResponse()
{
    QMetaObject::invokeMethod(this, "handleSetCardiacOutputReady", Qt::QueuedConnection);
}

void RecordingController::handleSetRhcValuesResponse()
{
    QMetaObject::invokeMethod(this, "handleSetRhcValuesReady", Qt::QueuedConnection);
}
