/**
 * @file   RecordingListModel.cpp
 *
 * \brief This file defines the Recording List Model class.
 *
 * Description:
 * Provides the UI with controls and data to interact with a recording list.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/RecordingListModel.h"
#include "presentation/Recording.h"
#include "providers/RecordingProvider.h"
#include "providers/TransactionProvider.h"

#include <QQmlEngine>

RecordingListModel::RecordingListModel(IRecordingProvider &recordingProvider, ITransactionProvider &transactionProvider,
                                       QObject *parent)
    : QAbstractListModel(parent), m_recordingProvider(recordingProvider), m_transactionProvider(transactionProvider)
{
    m_saveRecordingCbId = m_recordingProvider.registerSaveRecordingCallback([&] { handleSaveRecordingResponse(); });
    m_deleteRecordingCbId =
        m_recordingProvider.registerDeleteRecordingCallback([&] { handleDeleteRecordingResponse(); });
    m_transactionDetailsCbId =
        m_transactionProvider.registerGetTransactionDetailsCallback([&] { handleTransactionDetailsResponse(); });
}

RecordingListModel::~RecordingListModel()
{
    m_recordingList.clear();
    m_recordingProvider.unregisterSaveRecordingCallback(m_saveRecordingCbId);
    m_recordingProvider.unregisterDeleteRecordingCallback(m_deleteRecordingCbId);
    m_transactionProvider.unregisterGetTransactionDetailsCallback(m_transactionDetailsCbId);
}

Recording *RecordingListModel::get(int i)
{
    if (i >= 0 && i < m_recordingList.size()) {
        Recording *recording = m_recordingList.at(i);
        return recording;
    }
    return nullptr;
}

void RecordingListModel::clearRecordings() { m_recordingList.clear(); }

int RecordingListModel::rowCount(const QModelIndex &parent) const { return m_recordingList.size(); }

QVariant RecordingListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= 0 && index.row() < rowCount()) {
        switch (role) {
            case RecordingId:
                return m_recordingList.at(index.row())->recordingId();
            case RecordingTime:
                return m_recordingList.at(index.row())->recordingTime();
            case Systolic:
                return m_recordingList.at(index.row())->systolic();
            case Diastolic:
                return m_recordingList.at(index.row())->diastolic();
            case Mean:
                return m_recordingList.at(index.row())->mean();
            case HeartRate:
                return m_recordingList.at(index.row())->heartRate();
            case ReferenceSystolic:
                return m_recordingList.at(index.row())->referenceSystolic();
            case ReferenceDiastolic:
                return m_recordingList.at(index.row())->referenceDiastolic();
            case ReferenceMean:
                return m_recordingList.at(index.row())->referenceMean();
            case SensorStrength:
                return m_recordingList.at(index.row())->sensorStrength();
            case Position:
                return m_recordingList.at(index.row())->position();
            case WaveFormFile:
                return m_recordingList.at(index.row())->waveFormFile();
            case HaveNotes:
                QString notes = m_recordingList.at(index.row())->notes();
                return !(notes.isNull() || notes.isEmpty());
        }
    }

    return QVariant();
}

QHash<int, QByteArray> RecordingListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[RecordingId] = "recordingId";
    roles[RecordingTime] = "recordingTime";
    roles[Systolic] = "systolic";
    roles[Diastolic] = "diastolic";
    roles[Mean] = "mean";
    roles[HeartRate] = "heartRate";
    roles[ReferenceSystolic] = "referenceSystolic";
    roles[ReferenceDiastolic] = "referenceDiastolic";
    roles[ReferenceMean] = "referenceMean";
    roles[SensorStrength] = "sensorStrength";
    roles[Position] = "position";
    roles[HaveNotes] = "haveNotes";
    roles[WaveFormFile] = "waveFormFile";
    return roles;
}

void RecordingListModel::append(Recording *recording)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());

    // This is very important to prevent items to be garbage collected in JS!!!
    QQmlEngine::setObjectOwnership(recording, QQmlEngine::CppOwnership);

    m_recordingList.append(recording);

    endInsertRows();
}

void RecordingListModel::remove(int index)
{
    if (index >= 0 && index < m_recordingList.size()) {
        beginRemoveRows(QModelIndex(), index, index);
        m_recordingList.removeAt(index);
        endRemoveRows();
    }
}

void RecordingListModel::handleSaveRecording()
{
    Recording *recording = new Recording(m_recordingProvider.getSavedRecording());
    // check if recording already exists
    for (int i = 0; i < m_recordingList.size(); i++) {
        if (m_recordingList.at(i)->recordingId() == recording->recordingId()) {
            m_recordingList[i] = recording;
            dataChanged(index(i), index(i));
            return;
        }
    }
    // if got here, then recording not found so append to list
    append(recording);
}

void RecordingListModel::handleDeleteRecording()
{
    // find recording by id and remove it
    int recordingId = m_recordingProvider.getDeletedRecordingId();
    for (int i = 0; i < m_recordingList.size(); i++) {
        if (m_recordingList.at(i)->recordingId() == recordingId) {
            remove(i);
            break;
        }
    }
}

void RecordingListModel::handleTransactionDetails()
{
    m_recordingList.clear();
    std::list<RecordingData> recordings = m_transactionProvider.getEventData().recordings;
    std::list<RecordingData>::iterator iter;
    for (iter = recordings.begin(); iter != recordings.end(); iter++) {
        RecordingData rec = *iter;
        Recording *recording = new Recording(rec);
        m_recordingList.append(recording);
    }
    endResetModel();
}

void RecordingListModel::handleSaveRecordingResponse()
{
    QMetaObject::invokeMethod(this, "handleSaveRecording", Qt::QueuedConnection);
}

void RecordingListModel::handleDeleteRecordingResponse()
{
    QMetaObject::invokeMethod(this, "handleDeleteRecording", Qt::QueuedConnection);
}

void RecordingListModel::handleTransactionDetailsResponse()
{
    QMetaObject::invokeMethod(this, "handleTransactionDetails", Qt::QueuedConnection);
}
