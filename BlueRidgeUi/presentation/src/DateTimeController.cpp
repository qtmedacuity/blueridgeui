/**
 * @file   DateTimeController.cpp
 *
 * \brief This file contains the definition of the date and time
 * controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the date and
 * time.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/DateTimeController.h"
#include <QDate>
#include <QTime>
#include <QTimer>

DateTimeController::DateTimeController(std::string const& dateFormat, std::string const& timeFormat)
    : m_dateFormat(dateFormat), m_timeFormat(timeFormat), m_timer(new QTimer(this))
{
    connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));
    m_timer->start(1000);
}

void DateTimeController::update()
{
    QString dateNow = QDate::currentDate().toString(m_dateFormat.c_str());
    if (dateNow != m_date) {
        m_date = dateNow;
        emit dateChanged(m_date);
    }
    QString timeNow = QTime::currentTime().toString(m_timeFormat.c_str());
    if (timeNow != m_time) {
        m_time = timeNow;
        emit timeChanged(m_date);
    }
}
