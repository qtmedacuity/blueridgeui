/**
 * @file   WifiStrengthController.cpp
 *
 * \brief This file contains the definition of the Wifi Strength Controller class.
 *
 * Description:
 * Provides the UI with Wifi Status and update signals.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/WifiStrengthController.h"
#include "providers/IWifiStrengthProvider.h"

WifiStrengthController::WifiStrengthController(IWifiStrengthProvider& provider, StatusValueList const& barvalues)
    : SignalStrengthController(barvalues), m_provider(provider), m_networkName("")
{
    QObject::connect(this, &WifiStrengthController::wifiDataUpdated, this, &WifiStrengthController::setWifiStatus);
    m_cbId = m_provider.registerWifiStrengthCallback([&] { handleWifiData(); });
}

WifiStrengthController::~WifiStrengthController() { m_provider.unregisterWifiStrengthCallback(m_cbId); }

void WifiStrengthController::setNetworkName(QString const& network)
{
    QString name(network);
    if (!m_provider.isSet()) {
        name = tr("Invalid");
    } else if (!m_provider.isEnabled()) {
        name = tr("Disabled");
    } else if (!m_provider.isConnected()) {
        name = tr("No Connection");
    }

    if (name != m_networkName) {
        m_networkName = name;
        emit networkNameChanged(m_networkName);
    }
}

void WifiStrengthController::setStrength(int signal)
{
    if (m_provider.isSet() && m_provider.isEnabled() && m_provider.isConnected()) {
        SignalStrengthController::setStrength(signal);
    } else if (!(m_provider.isSet() && m_provider.isConnected())) {
        SignalStrengthController::setStrength(-1.0);
    } else {
        SignalStrengthController::setStrength(-2.0);
    }
}

void WifiStrengthController::setWifiStatus()
{
    setNetworkName(QString().fromStdString(m_provider.getNetworkName()));
    setStrength(m_provider.getStrength());
}

void WifiStrengthController::handleWifiData()
{
    QMetaObject::invokeMethod(this, "wifiDataUpdated", Qt::QueuedConnection);
}
