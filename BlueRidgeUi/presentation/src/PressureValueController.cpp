/**
 * @file   PressureValueController.cpp
 *
 * \brief This file defines the Pressure Value Controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the pressure values
 * (systolic/diastolic, mean and heart rate).
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/PressureValueController.h"
#include "providers/ISensorDataProvider.h"

#include <QDebug>

PressureValueController::PressureValueController(ISensorDataProvider& sensorProvider, QObject* parent)
    : QObject(parent),
      sensorProvider(sensorProvider),
      m_meanValueCbId(static_cast<unsigned long>(-1)),
      m_systolic(0),
      m_diastolic(0),
      m_mean(0),
      m_heartRate(0)
{
    m_meanValueCbId =
        sensorProvider.registerMeanValueCallback([&](unsigned int value) { handleMeanValue(static_cast<int>(value)); });
    m_systolicPressuresCbId =
        sensorProvider.registerSystolicValuesCallback([&](unsigned int systolic, unsigned int diastolic) {
            handleSystolicValues(static_cast<int>(systolic), static_cast<int>(diastolic));
        });
    m_heartRateCbId =
        sensorProvider.registerHeartRateCallback([&](unsigned int value) { handleHeartRate(static_cast<int>(value)); });

    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(updatePressures()));
}

PressureValueController::~PressureValueController()
{
    sensorProvider.unregisterMeanValueCallback(m_meanValueCbId);
    sensorProvider.unregisterSystolicValuesCallback(m_systolicPressuresCbId);
    sensorProvider.unregisterHeartRateCallback(m_heartRateCbId);
}

void PressureValueController::getPressureValues(QString command)
{
    if (command == "Start") {
        m_timer->start(1000);
    } else {
        m_timer->stop();
    }
}

void PressureValueController::updatePressures()
{
    // emit changed signals
    emit meanChanged();
    emit systolicChanged();
    emit diastolicChanged();
    emit heartRateChanged();
}

void PressureValueController::updateMean(int value) { m_mean = value; }

void PressureValueController::updateSystolicPressures(int systolic, int diasotlic)
{
    m_systolic = systolic;
    m_diastolic = diasotlic;
}

void PressureValueController::updateHeartRate(int value) { m_heartRate = value; }

void PressureValueController::handleMeanValue(int value)
{
    QMetaObject::invokeMethod(this, "updateMean", Qt::QueuedConnection, Q_ARG(int, value));
}

void PressureValueController::handleSystolicValues(int systolic, int diastolic)
{
    QMetaObject::invokeMethod(this, "updateSystolicPressures", Qt::QueuedConnection, Q_ARG(int, systolic),
                              Q_ARG(int, diastolic));
}

void PressureValueController::handleHeartRate(int value)
{
    QMetaObject::invokeMethod(this, "updateHeartRate", Qt::QueuedConnection, Q_ARG(int, value));
}

int PressureValueController::systolic() const { return m_systolic; }

int PressureValueController::diastolic() const { return m_diastolic; }

int PressureValueController::mean() const { return m_mean; }

int PressureValueController::heartRate() const { return m_heartRate; }
