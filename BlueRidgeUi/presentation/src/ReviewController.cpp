/**
 * @file   ReviewController.cpp
 *
 * \brief This file defines the Review Controller class.
 *
 * Description:
 * Provides the UI with controls and data to interact with review data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "ReviewController.h"
#include "presentation/CardiacOutput.h"
#include "presentation/PatientInfo.h"
#include "presentation/Recording.h"
#include "presentation/RhcValues.h"
#include "presentation/SensorCalibration.h"
#include "providers/EventData.h"
#include "providers/RecordingData.h"
#include "providers/TransactionProvider.h"

#include <QDebug>

ReviewController::ReviewController(ITransactionProvider& provider, QObject* parent)
    : QObject(parent),
      m_transactionProvider(provider),
      m_needToUpload(false),
      m_transactionId(0),
      m_patientId(0),
      m_sensorCalibration(nullptr),
      m_cardiacOutput(nullptr),
      m_rhcValues(nullptr)
{
    m_transactionCommitCbId =
        m_transactionProvider.registerCommitTransactionCallback([&] { handleTransactionCommitResponse(); });

    m_transactionDetailsCbId =
        m_transactionProvider.registerGetTransactionDetailsCallback([&] { handleTransactionDetailsResponse(); });

    m_uploadToMerlinCbId =
        m_transactionProvider.registerUploadToMerlinCallback([&] { handleUploadToMerlinResponse(); });
}

ReviewController::~ReviewController()
{
    m_transactionProvider.unregisterCommitTransactionCallback(m_transactionCommitCbId);
    m_transactionProvider.unregisterGetTransactionDetailsCallback(m_transactionDetailsCbId);
    m_transactionProvider.unregisterUploadToMerlinCallback(m_uploadToMerlinCbId);
    m_sensorCalibration = nullptr;
    m_cardiacOutput = nullptr;
    m_rhcValues = nullptr;
}

void ReviewController::getReviewData(int transactionId, int patientId)
{
    qDebug() << "ReviewController::getReviewData, transactionId " << transactionId << " patient id: " << patientId;
    m_sensorCalibration = nullptr;
    m_cardiacOutput = nullptr;
    m_rhcValues = nullptr;
    m_transactionProvider.requestTransactionDetails(transactionId, patientId);
}

SensorCalibration* ReviewController::getSensorCalibration() { return m_sensorCalibration; }

CardiacOutput* ReviewController::getCardiacOutput() { return m_cardiacOutput; }

RhcValues* ReviewController::getRhcValues() { return m_rhcValues; }

PatientInfo* ReviewController::getPatientInfo() { return m_patientInfo; }

QString ReviewController::getEventDateTime() { return m_eventDateTime; }

void ReviewController::commitTransaction(int transactionId, int patientId)
{
    m_transactionProvider.commitTransaction(transactionId, patientId);
}

void ReviewController::commitAndUpload(int transactionId, int patientId)
{
    m_needToUpload = true;
    m_transactionId = transactionId;
    m_patientId = patientId;
    // commit first
    m_transactionProvider.commitTransaction(transactionId, patientId);
}

void ReviewController::handleTransactionCommit()
{
    if (m_needToUpload) {
        // commit and upload
        m_transactionProvider.uploadToMerlin(m_transactionId, m_patientId);
    } else {
        emit transactionCommitted();
    }
}
void ReviewController::handleTransactionDetails()
{
    EventData eventData = m_transactionProvider.getEventData();
    m_eventDateTime = QString().fromStdString(eventData.date) + " " + QString().fromStdString(eventData.time);

    if (eventData.sensorCalibration != nullptr) {
        m_sensorCalibration = new SensorCalibration(eventData.sensorCalibration);
    }
    if (eventData.cardiacOutput != nullptr) {
        m_cardiacOutput = new CardiacOutput(eventData.cardiacOutput);
    }
    if (eventData.rhc != nullptr) {
        m_rhcValues = new RhcValues(eventData.rhc);
    }

    m_patientInfo = new PatientInfo(m_transactionProvider.getPatientData());

    emit reviewDataReady();
}

void ReviewController::handleUploadToMerlin()
{
    m_needToUpload = false;
    m_transactionId = 0;
    m_patientId = 0;
    emit uploadCompleted();
}

void ReviewController::handleTransactionCommitResponse()
{
    QMetaObject::invokeMethod(this, "handleTransactionCommit", Qt::QueuedConnection);
}

void ReviewController::handleTransactionDetailsResponse()
{
    QMetaObject::invokeMethod(this, "handleTransactionDetails", Qt::QueuedConnection);
}

void ReviewController::handleUploadToMerlinResponse()
{
    QMetaObject::invokeMethod(this, "handleUploadToMerlin", Qt::QueuedConnection);
}
