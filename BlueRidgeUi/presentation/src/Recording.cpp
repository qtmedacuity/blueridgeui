/**
 * @file   Recording.cpp
 *
 * \brief This file contains the definition of the Recording class.
 *
 * Description:
 * Provides methods for data of a single recording.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "Recording.h"

Recording::Recording(QObject *parent)
    : QObject(parent),
      m_recordingId(0),
      m_uploadPending(true),
      m_recordingDate(""),
      m_recordingTime(""),
      m_systolic(0),
      m_diastolic(0),
      m_mean(0),
      m_heartRate(0),
      m_referenceSystolic(0),
      m_referenceDiastolic(0),
      m_referenceMean(0),
      m_sensorStrength(0),
      m_position(""),
      m_notes(""),
      m_waveFormFile("")
{
}

Recording::Recording(RecordingData const &recording, QObject *parent) : QObject(parent)
{
    m_recordingId = static_cast<int>(recording.recordingId);
    m_uploadPending = recording.uploadPending;
    m_recordingDate = QString().fromStdString(recording.recordingDate);
    m_recordingTime = QString().fromStdString(recording.recordingTime);
    m_systolic = static_cast<int>(recording.systolic);
    m_diastolic = static_cast<int>(recording.diastolic);
    m_mean = static_cast<int>(recording.mean);
    m_heartRate = static_cast<int>(recording.heartRate);
    m_sensorStrength = static_cast<int>(recording.signalStrength);
    m_referenceSystolic = static_cast<int>(recording.refSystolic);
    m_referenceDiastolic = static_cast<int>(recording.refDiastolic);
    m_referenceMean = static_cast<int>(recording.refMean);
    m_position = QString().fromStdString(recording.patientPosition);
    m_notes = QString().fromStdString(recording.notes);
    m_waveFormFile = QString().fromStdString(recording.waveformFile);
}

RecordingData Recording::toRecordingData() const
{
    RecordingData recording;
    recording.recordingId = static_cast<int>(m_recordingId);
    recording.uploadPending = m_uploadPending;
    recording.recordingDate = m_recordingDate.toStdString();
    recording.recordingTime = m_recordingTime.toStdString();
    recording.systolic = static_cast<int>(m_systolic);
    recording.diastolic = static_cast<int>(m_diastolic);
    recording.mean = static_cast<int>(m_mean);
    recording.heartRate = static_cast<int>(m_heartRate);
    recording.signalStrength = static_cast<int>(m_sensorStrength);
    recording.refSystolic = static_cast<int>(m_referenceSystolic);
    recording.refDiastolic = static_cast<int>(m_referenceDiastolic);
    recording.refMean = static_cast<int>(m_referenceMean);
    recording.notes = m_notes.toStdString();
    recording.patientPosition = m_position.toStdString();
    recording.waveformFile = m_waveFormFile.toStdString();

    return recording;
}

int Recording::recordingId() const { return m_recordingId; }

void Recording::setRecordingId(int recordingId)
{
    if (m_recordingId != recordingId) {
        m_recordingId = recordingId;
        emit recordingChanged();
    }
}

bool Recording::uploadPending() const { return m_uploadPending; }

void Recording::setUploadPending(bool uploadPending)
{
    if (m_uploadPending != uploadPending) {
        m_uploadPending = uploadPending;
        emit recordingChanged();
    }
}

QString Recording::recordingDate() const { return m_recordingDate; }

void Recording::setRecordingDate(QString recordingDate)
{
    if (m_recordingDate != recordingDate) {
        m_recordingDate = recordingDate;
        emit recordingChanged();
    }
}

QString Recording::recordingTime() const { return m_recordingTime; }

void Recording::setRecordingTime(QString recordingTime)
{
    if (m_recordingTime != recordingTime) {
        m_recordingTime = recordingTime;
        emit recordingChanged();
    }
}

int Recording::systolic() const { return m_systolic; }

void Recording::setSystolic(int systolic)
{
    if (m_systolic != systolic) {
        m_systolic = systolic;
        emit recordingChanged();
    }
}

int Recording::diastolic() const { return m_diastolic; }

void Recording::setDiastolic(int diastolic)
{
    if (m_diastolic != diastolic) {
        m_diastolic = diastolic;
        emit recordingChanged();
    }
}

int Recording::mean() const { return m_mean; }

void Recording::setMean(int mean)
{
    if (m_mean != mean) {
        m_mean = mean;
        emit recordingChanged();
    }
}

int Recording::heartRate() const { return m_heartRate; }

void Recording::setHeartRate(int heartRate)
{
    if (m_heartRate != heartRate) {
        m_heartRate = heartRate;
        emit recordingChanged();
    }
}

int Recording::referenceSystolic() const { return m_referenceSystolic; }

void Recording::setReferenceSystolic(int referenceSystolic)
{
    if (m_referenceSystolic != referenceSystolic) {
        m_referenceSystolic = referenceSystolic;
        emit recordingChanged();
    }
}

int Recording::referenceDiastolic() const { return m_referenceDiastolic; }

void Recording::setReferenceDiastolic(int referenceDiastolic)
{
    if (m_referenceDiastolic != referenceDiastolic) {
        m_referenceDiastolic = referenceDiastolic;
        emit recordingChanged();
    }
}

int Recording::referenceMean() const { return m_referenceMean; }

void Recording::setReferenceMean(int referenceMean)
{
    if (m_referenceMean != referenceMean) {
        m_referenceMean = referenceMean;
        emit recordingChanged();
    }
}

int Recording::sensorStrength() const { return m_sensorStrength; }

void Recording::setSensorStrength(int sensorStrength)
{
    if (m_sensorStrength != sensorStrength) {
        m_sensorStrength = sensorStrength;
        emit recordingChanged();
    }
}

QString Recording::position() const { return m_position; }

void Recording::setPosition(QString position)
{
    if (m_position != position) {
        m_position = position;
        emit recordingChanged();
    }
}

QString Recording::notes() const { return m_notes; }

void Recording::setNotes(QString notes)
{
    if (m_notes != notes) {
        m_notes = notes;
        emit recordingChanged();
    }
}

QString Recording::waveFormFile() const { return m_waveFormFile; }

void Recording::setWaveFormFile(QString waveFormFile)
{
    if (m_waveFormFile != waveFormFile) {
        m_waveFormFile = waveFormFile;
        emit recordingChanged();
    }
}
