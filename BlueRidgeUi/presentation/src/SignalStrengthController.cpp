/**
 * @file   SignalStrengthController.cpp
 *
 * \brief This file contains the definition of the signal strength controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the signal strength
 * indicator.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/SignalStrengthController.h"

SignalStrengthController::SignalStrengthController(StatusValueList const& statusValueList)
    : m_statusValueList(statusValueList), m_strength(0.0), m_status("")
{
    std::sort(std::begin(m_statusValueList), std::end(m_statusValueList),
              [](StatusValue const& a, StatusValue const& b) { return std::get<1>(a) >= std::get<1>(b); });
}

QString SignalStrengthController::getStatusAt(int strength) const
{
    for (StatusValue const& sv : m_statusValueList) {
        if (std::get<1>(sv) <= strength) {
            return QString().fromStdString(std::get<0>(sv));
        }
    }

    return QString();
}

int SignalStrengthController::strength() const { return m_strength; }

void SignalStrengthController::setStrength(int value)
{
    int strength = value < 0 ? 0 : value > 100 ? 100 : value;
    if (strength != m_strength) {
        m_strength = strength;
        emit strengthChanged(m_strength);
    }

    setStatus(getStatusAt(value));
}

QString SignalStrengthController::status() const { return m_status; }

void SignalStrengthController::setStatus(QString const& value)
{
    if (value != m_status) {
        m_status = value;
        emit statusChanged(m_status);
    }
}
