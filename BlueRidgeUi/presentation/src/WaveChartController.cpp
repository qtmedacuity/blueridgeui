/**
 * @file   WaveChartController.cpp
 *
 * \brief This file defines the Wave Chart Controller base class.
 *
 * Description:
 * Provides a base class for chart controllers.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/
#include "presentation/WaveChartController.h"
#include "widgets/WaveChart/WaveAxis.h"
#include "widgets/WaveChart/WaveAxisPoint.h"
#include "widgets/WaveChart/WaveChartWidget.h"

#include <QDebug>
#include <QFile>
#include <QtCharts/QXYSeries>

#include <json/json.h>

#include <algorithm>
#include <exception>
#include <iomanip>
#include <limits>
#include <memory>
#include <sstream>
#include <string>

WaveChartController::WaveChartController(const QString& resource, QObject* parent)
    : QObject(parent), m_verticalOffset(0.0), m_horizontalOffset(0.0), m_widget(nullptr)
{
    QFile axesDataFile(resource);
    if (!axesDataFile.open(QIODevice::ReadOnly))
        throw std::runtime_error(
            QString("WaveChartController could not open %0").arg(axesDataFile.fileName()).toStdString());

    fromJSON(QByteArray(axesDataFile.readAll()).toStdString());
    axesDataFile.close();

    m_xAxisConfig.zoomInSignal = [&](bool enabled) { emit zoomInHorizontalEnabledChanged(enabled); };
    m_xAxisConfig.zoomOutSignal = [&](bool enabled) { emit zoomOutHorizontalEnabledChanged(enabled); };
    m_yAxisConfig.zoomInSignal = [&](bool enabled) { emit zoomInVerticalEnabledChanged(enabled); };
    m_yAxisConfig.zoomOutSignal = [&](bool enabled) { emit zoomOutVerticalEnabledChanged(enabled); };
}

WaveChartController::~WaveChartController()
{
    unregisterComponent(m_widget);
    std::lock_guard<std::mutex> lock(m_mutex);
    m_dataArraysList.clear();
}

void WaveChartController::registerSeries(QtCharts::QXYSeries* series)
{
    if (nullptr == series) return;

    std::lock_guard<std::mutex> lock(m_mutex);
    for (DataArrays& dataArrays : m_dataArraysList) {
        if (series == dataArrays.second) {
            qWarning() << "Series already registered: " << series->name();
            return;
        }
    }

    m_dataArraysList.append(qMakePair(QVector<QPointF>(), series));
    emit seriesRegistered(series->name());
}

void WaveChartController::unregisterSeries(QtCharts::QXYSeries* series)
{
    if (nullptr == series) return;

    bool unregistered(false);
    std::lock_guard<std::mutex> lock(m_mutex);
    for (DataArrays& dataArrays : m_dataArraysList) {
        if (series == dataArrays.second) {
            unregistered = m_dataArraysList.removeOne(dataArrays);
            break;
        }
    }

    if (unregistered) {
        emit seriesUnegistered(series->name());
    }
}

void WaveChartController::registerComponent(WaveChartWidget* widget)
{
    if (widget == m_widget) return;

    m_widget = widget;
    if (nullptr == m_widget) {
        m_xAxisConfig.component = nullptr;
        m_yAxisConfig.component = nullptr;
        return;
    }

    m_xAxisConfig.index = m_xAxisConfig.defaultIndex;
    m_xAxisConfig.component = m_widget->xAxis();
    configureComponent(m_xAxisConfig, 0.0);

    m_yAxisConfig.index = m_yAxisConfig.defaultIndex;
    m_yAxisConfig.component = m_widget->yAxis();
    configureComponent(m_yAxisConfig, 0.0);
}

void WaveChartController::unregisterComponent(WaveChartWidget* widget)
{
    if (widget == m_widget) {
        m_widget = nullptr;
        m_xAxisConfig.component = nullptr;
        m_yAxisConfig.component = nullptr;
    }
}

void WaveChartController::zoomInHorizontal()
{
    if (nullptr == m_xAxisConfig.component) return;

    zoomInComponent(m_xAxisConfig);
}

void WaveChartController::zoomOutHorizontal()
{
    if (nullptr == m_xAxisConfig.component) return;

    zoomOutComponent(m_xAxisConfig);
}

void WaveChartController::zoomInVertical()
{
    if (nullptr == m_yAxisConfig.component) return;

    zoomInComponent(m_yAxisConfig);
}

void WaveChartController::zoomOutVertical()
{
    if (nullptr == m_yAxisConfig.component) return;

    zoomOutComponent(m_yAxisConfig);
}

void WaveChartController::scrollHorizontal(double scrollPercent, bool active)
{
    if (nullptr == m_xAxisConfig.component) return;

    AxisAttributes& attribs(m_xAxisConfig.attributes(m_xAxisConfig.index));
    double scrollValue(-((attribs.delta * scrollPercent) / 100.0));

    if (!active) {
        m_xAxisConfig.scrollBase += scrollValue;
        return;
    }

    updateComponent(m_xAxisConfig, scrollValue);
}

void WaveChartController::scrollVertical(double scrollPercent, bool active)
{
    if (nullptr == m_yAxisConfig.component) return;

    AxisAttributes& attribs(m_yAxisConfig.attributes(m_yAxisConfig.index));
    double scrollValue((attribs.delta * scrollPercent) / 100.0);

    if (!active) {
        m_yAxisConfig.scrollBase += scrollValue;
        return;
    }

    updateComponent(m_yAxisConfig, scrollValue);
}

bool WaveChartController::zoomInHorizontalEnabled() const { return m_xAxisConfig.zoomInEnabled; }

bool WaveChartController::zoomOutHorizontalEnabled() const { return m_xAxisConfig.zoomOutEnabled; }

bool WaveChartController::zoomInVerticalEnabled() const { return m_yAxisConfig.zoomInEnabled; }

bool WaveChartController::zoomOutVerticalEnabled() const { return m_yAxisConfig.zoomOutEnabled; }

double WaveChartController::verticalOffset() const { return m_verticalOffset; }

void WaveChartController::setVerticalOffset(double offset)
{
    if (std::abs(offset - m_verticalOffset) < std::numeric_limits<double>::epsilon()) return;

    m_verticalOffset = offset;
    emit verticalOffsetChanged(m_verticalOffset);
}

double WaveChartController::horizontalOffset() const { return m_horizontalOffset; }

void WaveChartController::setHorizontalOffset(double offset)
{
    if (std::abs(offset - m_horizontalOffset) < std::numeric_limits<double>::epsilon()) return;

    m_horizontalOffset = offset;
    emit horizontalOffsetChanged(m_horizontalOffset);
}

void WaveChartController::fromJSON(const std::string& configString)
{
    Json::Value configRoot;
    Json::CharReaderBuilder builder;
    std::shared_ptr<Json::CharReader> jsonReader(builder.newCharReader());
    std::string errorStr;
    bool success =
        jsonReader->parse(configString.c_str(), configString.c_str() + configString.length(), &configRoot, &errorStr);
    if (!(success && configRoot.isObject()))
        throw std::runtime_error(std::string("WaveChartController configuratrion file failed to parse: ") + errorStr +
                                 " [" + configString + "]");

    auto extractJson = [&](std::string const& tag, Json::Value* root) -> Json::Value* {
        Json::Value* value = &(*root)[tag.c_str()];
        if (value->isNull())
            throw std::runtime_error(std::string("WaveChartController configuration failed to parse ") + tag);
        return value;
    };
    auto extractObject = [&](std::string const& tag, Json::Value* root) -> Json::Value* {
        Json::Value* value = extractJson(tag, root);
        if (!value->isObject())
            throw std::runtime_error(std::string("WaveChartController configuration failed to parse object ") + tag);
        return value;
    };
    auto extractArray = [&](std::string const& tag, Json::Value* root) -> Json::Value* {
        Json::Value* value = extractJson(tag, root);
        if (!value->isArray())
            throw std::runtime_error(std::string("WaveChartController configuration failed to parse array ") + tag);
        return value;
    };
    auto extractInteger = [&](std::string const& tag, Json::Value* root) -> int {
        Json::Value* value = extractJson(tag, root);
        if (!value->isInt())
            throw std::runtime_error(std::string("WaveChartController configuration failed to parse integer ") + tag);
        return value->asInt();
    };
    auto extractUnsignedInteger = [&](std::string const& tag, Json::Value* root) -> unsigned int {
        Json::Value* value = extractJson(tag, root);
        if (!value->isUInt())
            throw std::runtime_error(
                std::string("WaveChartController configuration failed to parse unsigned integer ") + tag);
        return value->asUInt();
    };
    auto extractDouble = [&](std::string const& tag, Json::Value* root) -> double {
        Json::Value* value = extractJson(tag, root);
        if (!value->isDouble())
            throw std::runtime_error(std::string("WaveChartController configuration failed to parse double ") + tag);
        return value->asDouble();
    };
    auto extractString = [&](std::string const& tag, Json::Value* root) -> std::string {
        Json::Value* value = extractJson(tag, root);
        if (!value->isString())
            throw std::runtime_error(std::string("WaveChartController configuration failed to parse string ") + tag);
        return value->asString();
    };
    auto extractPoint = [&](Json::Value* root) -> WaveAxisPoint {
        if (!root->isObject())
            throw std::runtime_error(std::string("WaveChartController configuration failed to parse point"));

        return WaveAxisPoint(extractDouble("value", root), extractString("label", root),
                             extractInteger("ticklength", root), extractInteger("tickwidth", root),
                             extractInteger("grid", root));
    };
    auto extractPointList = [&](Json::Value* root) -> WaveAxis::PointList {
        if (!root->isArray())
            throw std::runtime_error(std::string("WaveChartController configuration failed to parse point list"));

        unsigned long maxChars(0);
        WaveAxis::PointList pointList;
        for (unsigned int idx(0); idx < root->size(); ++idx) {
            pointList.push_back(extractPoint(&(*root)[idx]));
            std::string label(pointList.back().label());
            label.erase(label.begin(), std::find_if_not(label.begin(), label.end(),
                                                        [](unsigned char c) -> bool { return std::isspace(c); }));
            label.erase(
                find_if_not(label.rbegin(), label.rend(), [](unsigned char c) -> bool { return isspace(c); }).base(),
                label.end());
            label.erase(std::remove(label.begin(), label.end(), '\t'), label.end());
            label.erase(std::remove(label.begin(), label.end(), '\n'), label.end());
            maxChars = std::max(maxChars, label.size());
            pointList.back().setLabel(label);
        }
        if (pointList.empty())
            throw std::runtime_error(std::string("WaveChartController configuration failed on empty point list"));

        std::sort(std::begin(pointList), std::end(pointList),
                  [](WaveAxisPoint const& a, WaveAxisPoint const& b) -> bool { return a < b; });
        for (WaveAxisPoint& point : pointList) {
            if (maxChars > point.label().size()) {
                std::stringstream ss;
                ss << std::setw(static_cast<int>(maxChars)) << point.label();
                point.setLabel(ss.str());
            }
        }
        return pointList;
    };
    auto extractListOfPointList = [&](Json::Value* root) -> WaveChartController::PointsLists {
        if (!root->isArray())
            throw std::runtime_error(
                std::string("WaveChartController configuration failed to parse list of point lists"));

        WaveChartController::PointsLists pointsList;
        for (unsigned int idx(0); idx < root->size(); ++idx) {
            pointsList.push_back(extractPointList(&(*root)[idx]));
        }
        if (pointsList.empty())
            throw std::runtime_error(
                std::string("WaveChartController configuration failed on empty list of point lists"));

        return pointsList;
    };
    auto extractPointSets = [&](std::string const& tag, Json::Value* root,
                                unsigned int tickCount) -> WaveChartController::PointSets {
        Json::Value* array = extractArray(tag, root);
        WaveChartController::PointsLists pointsLists(extractListOfPointList(array));
        WaveChartController::PointSets pointSets;
        for (WaveAxis::PointList const& pointList : pointsLists) {
            pointSets.push_back(
                std::tuple<AxisAttributes, WaveAxis::PointList>(AxisAttributes(pointList, tickCount), pointList));
        }
        return pointSets;
    };

    Json::Value* xAxis(extractObject("XAxis", &configRoot));
    m_xAxisConfig.tickCount = extractUnsignedInteger("tickCount", xAxis);
    m_xAxisConfig.pointSets = extractPointSets("points", xAxis, m_xAxisConfig.tickCount);
    m_xAxisConfig.defaultIndex = extractUnsignedInteger("defaultPointSet", xAxis);
    if (m_xAxisConfig.defaultIndex >= m_xAxisConfig.pointSets.size())
        m_xAxisConfig.defaultIndex = static_cast<unsigned int>(m_xAxisConfig.pointSets.size()) - 1;

    m_xAxisConfig.index = m_xAxisConfig.defaultIndex;
    m_xAxisConfig.zoomInEnabled = static_cast<bool>(0 < m_xAxisConfig.index);
    m_xAxisConfig.zoomOutEnabled = static_cast<bool>(m_xAxisConfig.pointSets.size() - 1 > m_xAxisConfig.index);

    Json::Value* yAxis(extractObject("YAxis", &configRoot));
    m_yAxisConfig.tickCount = extractUnsignedInteger("tickCount", yAxis);
    m_yAxisConfig.pointSets = extractPointSets("points", yAxis, m_yAxisConfig.tickCount);
    m_yAxisConfig.defaultIndex = extractUnsignedInteger("defaultPointSet", yAxis);
    if (m_yAxisConfig.defaultIndex >= m_yAxisConfig.pointSets.size())
        m_yAxisConfig.defaultIndex = static_cast<unsigned int>(m_yAxisConfig.pointSets.size()) - 1;

    m_yAxisConfig.index = m_yAxisConfig.defaultIndex;
    m_yAxisConfig.zoomInEnabled = static_cast<bool>(0 < m_yAxisConfig.index);
    m_yAxisConfig.zoomOutEnabled = static_cast<bool>(m_yAxisConfig.pointSets.size() - 1 > m_yAxisConfig.index);
}

void WaveChartController::configureComponent(AxisConfiguration& axis, double scrollValue)
{
    axis.scrollBase = scrollValue;
    axis.component->setPoints(axis.pointList(axis.index));
    axis.component->setDisplayMax(axis.attributes(axis.index).adjustedUpperViewport(axis.scrollBase));
    axis.component->setDisplayMin(axis.attributes(axis.index).adjustedLowerViewport(axis.scrollBase));
}

void WaveChartController::updateComponent(AxisConfiguration& axis, double scrollValue)
{
    AxisAttributes& attribs(axis.attributes(axis.index));
    axis.component->setDisplayMin(attribs.adjustedLowerViewport(scrollValue + axis.scrollBase));
    axis.component->setDisplayMax(attribs.adjustedUpperViewport(scrollValue + axis.scrollBase));
}

void WaveChartController::zoomInComponent(AxisConfiguration& axis)
{
    if (0 < axis.index) {
        --axis.index;
        configureComponent(axis, axis.scrollBase);
    }
    if (!axis.zoomOutEnabled) {
        axis.zoomOutEnabled = true;
        axis.zoomOutSignal(axis.zoomOutEnabled);
    }
    bool zoomInEnable(0 < axis.index);
    if (zoomInEnable != axis.zoomInEnabled) {
        axis.zoomInEnabled = zoomInEnable;
        axis.zoomInSignal(axis.zoomInEnabled);
    }
}

void WaveChartController::zoomOutComponent(WaveChartController::AxisConfiguration& axis)
{
    if (axis.pointSets.size() - 1 > axis.index) {
        ++axis.index;
        configureComponent(axis, axis.scrollBase);
    }
    if (!axis.zoomInEnabled) {
        axis.zoomInEnabled = true;
        axis.zoomInSignal(axis.zoomInEnabled);
    }
    bool zoomOutEnable(axis.pointSets.size() - 1 > axis.index);
    if (zoomOutEnable != axis.zoomOutEnabled) {
        axis.zoomOutEnabled = zoomOutEnable;
        axis.zoomOutSignal(axis.zoomOutEnabled);
    }
}

WaveChartController::AxisAttributes::AxisAttributes(const WaveAxis::PointList& pointList, const unsigned int numTicks)
    : hardMax(std::numeric_limits<double>::min()),
      hardMin(std::numeric_limits<double>::max()),
      upperViewPort(hardMax),
      lowerViewPort(hardMin),
      delta(0.0)
{
    bool minSet(false);
    bool upperSet(false);
    bool lowerSet(false);
    unsigned int count(0);
    for (WaveAxisPoint const& point : pointList) {
        hardMax = point.value();
        if (!minSet) {
            hardMin = hardMax;
            minSet = true;
        }
        if (!lowerSet && hardMax == 0.0) {
            lowerViewPort = hardMax;
            lowerSet = true;
        }
        if (lowerSet && !upperSet && count++ >= numTicks) {
            upperViewPort = hardMax;
            upperSet = true;
        }
    }
    if (!lowerSet) {
        lowerViewPort = hardMin;
    }
    if (!upperSet) {
        count = 0;
        for (WaveAxisPoint const& point : pointList) {
            double value(point.value());
            if (value >= lowerViewPort && numTicks >= count++) {
                upperViewPort = value;
            }
        }
    }
    delta = upperViewPort - lowerViewPort;
}
