/**
 * @file   PASignalStrengthController.cpp
 *
 * \brief This file contains the definition of the PA Pressure Signal Strength Controller class.
 *
 * Description:
 * Provides the UI with PA Pressure Signal Status and update signals.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "presentation/PASignalStrengthController.h"
#include "providers/ISensorDataProvider.h"

#include <limits>

PASignalStrengthController::PASignalStrengthController(StatusValueList const& barvalues, QString const& noPulseTag,
                                                       ISensorDataProvider& sensorProvider)
    : SignalStrengthController(barvalues),
      m_noPulseTag(noPulseTag),
      m_sensorProvider(sensorProvider),
      m_pulsatility(false),
      m_pulsatilityOverride(false),
      m_sensorCB(std::numeric_limits<unsigned long>::max()),
      m_searchCB(std::numeric_limits<unsigned long>::max())
{
    QObject::connect(this, &PASignalStrengthController::pulsatilityChanged,
                     [&](bool) { m_sensorProvider.sendSignalStrength(); });
    QObject::connect(this, &PASignalStrengthController::pulsatilityOverrideChanged,
                     [&](bool) { m_sensorProvider.sendSignalStrength(); });

    m_sensorCB = m_sensorProvider.registerSignalStrengthCallback(
        [&](unsigned int value) { handleSignalStrength(static_cast<int>(value)); });
    m_sensorCB = m_sensorProvider.registerPulsatilityCallback([&](bool value) { handlePulsatility(value); });

    m_sensorProvider.sendPulsatility();
    m_sensorProvider.sendSignalStrength();
}

PASignalStrengthController::~PASignalStrengthController()
{
    m_sensorProvider.unregisterSignalStrengthCallback(m_sensorCB);
}

QString PASignalStrengthController::getStatusAt(int strength) const
{
    if (m_pulsatility || m_pulsatilityOverride) return SignalStrengthController::getStatusAt(strength);

    return m_noPulseTag;
}

bool PASignalStrengthController::pulsatility() const { return m_pulsatility; }

bool PASignalStrengthController::pulsatilityOverride() const { return m_pulsatilityOverride; }

void PASignalStrengthController::setPulsatilityOverride(bool override)
{
    if (override != m_pulsatilityOverride) {
        m_pulsatilityOverride = override;
        emit pulsatilityOverrideChanged(m_pulsatilityOverride);
    }
}

void PASignalStrengthController::updatePulsatility(bool pulse)
{
    if (pulse != m_pulsatility) {
        m_pulsatility = pulse;
        emit pulsatilityChanged(m_pulsatility);
    }
}

void PASignalStrengthController::handleSignalStrength(int value)
{
    QMetaObject::invokeMethod(this, "setStrength", Qt::QueuedConnection, Q_ARG(int, value));
}

void PASignalStrengthController::handlePulsatility(bool value)
{
    QMetaObject::invokeMethod(this, "updatePulsatility", Qt::QueuedConnection, Q_ARG(bool, value));
}
