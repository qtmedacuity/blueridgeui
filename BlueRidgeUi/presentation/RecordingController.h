﻿#pragma once

/**
 * @file   RecordingController.h
 *
 * \brief This file declares the Recording Controller class.
 *
 * Description:
 * Provides the UI with controls and data to interact with a recording.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>
#include <QVariant>

class IRecordingProvider;
class ITransactionProvider;
class Recording;
class RhcValues;

class RecordingController : public QObject
{
    Q_OBJECT

   public:
    RecordingController(ITransactionProvider& transactionProvider, IRecordingProvider& recordingProvider,
                        QObject* parent = nullptr);
    ~RecordingController() override;

    Q_PROPERTY(int paMean READ paMean WRITE setPaMean NOTIFY paMeanChanged)

    Q_INVOKABLE void getRecordingStatus(QString command);
    Q_INVOKABLE void takeReading();
    Q_INVOKABLE void savePaMean();
    Q_INVOKABLE void recordPressures();
    Q_INVOKABLE QVariant getNewRecording();
    Q_INVOKABLE void saveRecording(Recording* recording);
    Q_INVOKABLE void deleteRecording(int recordingId);
    Q_INVOKABLE void saveCardiacOutput(float cardiacOutput);
    Q_INVOKABLE void saveRhcValues(RhcValues* rhcValues);

   signals:
    void readyToRecord();
    void takeReadingFinished();
    void paMeanChanged();
    void paMeanSaveComplete();
    void recordingPressuresFinished();
    void recordingSaveComplete();
    void recordingDeleteComplete();
    void setCardiacOutputComplete();
    void setRhcValuesComplete();

   public slots:
    int paMean() const;
    void setPaMean(int value);
    int paMeanOffset() const;
    int offsetSystolic() const;
    int offsetDiastolic() const;
    int offsetMean() const;

   private slots:
    void handleReadyToRecord();
    void takeReadingReady();
    void handleSetPaMeanReady();
    void handleSaveRecording();
    void handleDeleteRecording();
    void handleSetCardiacOutputReady();
    void handleSetRhcValuesReady();

   private:
    RecordingController(QObject* = nullptr) = delete;
    RecordingController(RecordingController const&) = delete;
    RecordingController& operator=(RecordingController const&) = delete;

    void handleTransactionResponse();
    void handleReadyToRecordResponse();
    void handleRecordingStatusResponse();
    void handleTakeReadingResponse();
    void handleSetPaMeanResponse();
    void handleSaveRecordingResponse();
    void handleDeleteRecordingResponse();
    void handleSetCardiacOutputResponse();
    void handleSetRhcValuesResponse();

    void createRecording();

   private:
    IRecordingProvider& m_recordingProvider;
    ITransactionProvider& m_transactionProvider;
    unsigned long m_transactionCbId;
    unsigned long m_recordReadyCbId;
    unsigned long m_recordStatusCbId;
    unsigned long m_takeReadingCbId;
    unsigned long m_setPaMeanCbId;
    unsigned long m_saveRecordingCbId;
    unsigned long m_deleteRecordingCbId;
    unsigned long m_setCardiacOutputCbId;
    unsigned long m_setRhcValuesCbId;
    int m_paMean;
    int m_paMeanOffset;
    int m_offsetSystolic;
    int m_offsetDiastolic;
    int m_offsetMean;
    bool m_recordingPressures;
    Recording* m_newRecording;
};
