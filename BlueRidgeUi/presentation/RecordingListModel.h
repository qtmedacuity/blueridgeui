#pragma once

/**
 * @file   RecordingListModel.h
 *
 * \brief This file declares the Recording List Model class.
 *
 * Description:
 * Provides the UI with controls and data to interact with a recording list.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QAbstractListModel>

class Recording;
class IRecordingProvider;
class ITransactionProvider;

class RecordingListModel : public QAbstractListModel
{
    Q_OBJECT

   public:
    Q_INVOKABLE Recording *get(int index);
    Q_INVOKABLE void clearRecordings();

    enum RecordingRole {
        RecordingId = Qt::UserRole + 1,
        RecordingTime,
        Systolic,
        Diastolic,
        Mean,
        HeartRate,
        ReferenceSystolic,
        ReferenceDiastolic,
        ReferenceMean,
        SensorStrength,
        Position,
        HaveNotes,
        WaveFormFile
    };
    Q_ENUM(RecordingRole);

    explicit RecordingListModel(IRecordingProvider &recordingProvider, ITransactionProvider &transactionProvider,
                                QObject *parent = nullptr);
    ~RecordingListModel() override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;
    void append(Recording *recording);
    void remove(int index);

   private slots:
    void handleSaveRecording();
    void handleDeleteRecording();
    void handleTransactionDetails();

   private:
    void handleSaveRecordingResponse();
    void handleDeleteRecordingResponse();
    void handleTransactionDetailsResponse();

    IRecordingProvider &m_recordingProvider;
    ITransactionProvider &m_transactionProvider;
    unsigned long m_saveRecordingCbId;
    unsigned long m_deleteRecordingCbId;
    unsigned long m_transactionDetailsCbId;
    QList<Recording *> m_recordingList;
};
