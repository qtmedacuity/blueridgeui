#pragma

/**
 * @file   Recording.h
 *
 * \brief This file contains the declaration of the Recording class.
 *
 * Description:
 * Provides methods for data of a single recording.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>

#include "providers/RecordingData.h"

class Recording : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int recordingId READ recordingId WRITE setRecordingId NOTIFY recordingChanged)
    Q_PROPERTY(bool uploadPending READ uploadPending WRITE setUploadPending NOTIFY recordingChanged)
    Q_PROPERTY(QString recordingDate READ recordingDate WRITE setRecordingDate NOTIFY recordingChanged)
    Q_PROPERTY(QString recordingTime READ recordingTime WRITE setRecordingTime NOTIFY recordingChanged)
    Q_PROPERTY(int systolic READ systolic WRITE setSystolic NOTIFY recordingChanged)
    Q_PROPERTY(int diastolic READ diastolic WRITE setDiastolic NOTIFY recordingChanged)
    Q_PROPERTY(int mean READ mean WRITE setMean NOTIFY recordingChanged)
    Q_PROPERTY(int heartRate READ heartRate WRITE setHeartRate NOTIFY recordingChanged)
    Q_PROPERTY(int referenceSystolic READ referenceSystolic WRITE setReferenceSystolic NOTIFY recordingChanged)
    Q_PROPERTY(int referenceDiastolic READ referenceDiastolic WRITE setReferenceDiastolic NOTIFY recordingChanged)
    Q_PROPERTY(int referenceMean READ referenceMean WRITE setReferenceMean NOTIFY recordingChanged)
    Q_PROPERTY(int sensorStrength READ sensorStrength WRITE setSensorStrength NOTIFY recordingChanged)
    Q_PROPERTY(QString position READ position WRITE setPosition NOTIFY recordingChanged)
    Q_PROPERTY(QString notes READ notes WRITE setNotes NOTIFY recordingChanged)
    Q_PROPERTY(QString waveFormFile READ waveFormFile WRITE setWaveFormFile NOTIFY recordingChanged)

   public:
    explicit Recording(QObject *parent = nullptr);
    explicit Recording(RecordingData const &recording, QObject *parent = nullptr);

    RecordingData toRecordingData() const;

    int recordingId() const;
    void setRecordingId(int recordingId);
    bool uploadPending() const;
    void setUploadPending(bool uploadPending);
    QString recordingDate() const;
    void setRecordingDate(QString recordingDate);
    QString recordingTime() const;
    void setRecordingTime(QString recordingTime);
    int systolic() const;
    void setSystolic(int systolic);
    int diastolic() const;
    void setDiastolic(int diastolic);
    int mean() const;
    void setMean(int mean);
    int heartRate() const;
    void setHeartRate(int heartRate);
    int referenceSystolic() const;
    void setReferenceSystolic(int referenceSystolic);
    int referenceDiastolic() const;
    void setReferenceDiastolic(int referenceDiastolic);
    int referenceMean() const;
    void setReferenceMean(int referenceMean);
    int sensorStrength() const;
    void setSensorStrength(int sensorStrength);
    QString position() const;
    void setPosition(QString position);
    QString notes() const;
    void setNotes(QString notes);
    QString waveFormFile() const;
    void setWaveFormFile(QString waveFormFile);

   signals:
    void recordingChanged() const;

   private:
    int m_recordingId;
    bool m_uploadPending;
    QString m_recordingDate;
    QString m_recordingTime;
    int m_systolic;
    int m_diastolic;
    int m_mean;
    int m_heartRate;
    int m_referenceSystolic;
    int m_referenceDiastolic;
    int m_referenceMean;
    int m_sensorStrength;
    QString m_position;
    QString m_notes;
    QString m_waveFormFile;
};
