#pragma once

/**
 * @file   PatientInfo.h
 *
 * \brief This file contains the declaration of the patient class.
 *
 * Description:
 * Provides methods for data of a single patient.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>

#include "providers/PatientData.h"

class PatientInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int patientId READ patientId WRITE setPatientId NOTIFY patientInfoChanged)
    Q_PROPERTY(QString firstName READ firstName WRITE setFirstName NOTIFY patientInfoChanged)
    Q_PROPERTY(QString middleInitial READ middleInitial WRITE setMiddleInitial NOTIFY patientInfoChanged)
    Q_PROPERTY(QString lastName READ lastName WRITE setLastName NOTIFY patientInfoChanged)
    Q_PROPERTY(QString patientName READ patientName WRITE setPatientName NOTIFY patientInfoChanged)
    Q_PROPERTY(QString dob READ dob WRITE setDob NOTIFY patientInfoChanged)
    Q_PROPERTY(QString implantDate READ implantDate WRITE setImplantDate NOTIFY patientInfoChanged)
    Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY patientInfoChanged)
    Q_PROPERTY(QString clinician READ clinician WRITE setClinician NOTIFY patientInfoChanged)
    Q_PROPERTY(QString clinic READ clinic WRITE setClinic NOTIFY patientInfoChanged)
    Q_PROPERTY(QString serialNumber READ serialNumber WRITE setSerialNumber NOTIFY patientInfoChanged)
    Q_PROPERTY(QString implantDoctor READ implantDoctor WRITE setImplantDoctor NOTIFY patientInfoChanged)
    Q_PROPERTY(QString implantLocation READ implantLocation WRITE setImplantLocation NOTIFY patientInfoChanged)
    Q_PROPERTY(bool uploadPending READ uploadPending WRITE setUploadPending NOTIFY uploadPendingChanged)
    Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectedChanged)

   public:
    explicit PatientInfo(QObject *parent = nullptr);
    explicit PatientInfo(PatientData const &patient, QObject *parent = nullptr);

    PatientData toPatientData() const;

    int patientId() const;
    void setPatientId(int patientId);
    QString firstName() const;
    void setFirstName(QString firstName);
    QString middleInitial() const;
    void setMiddleInitial(QString middleInitial);
    QString lastName() const;
    void setLastName(QString lastName);
    QString patientName() const;
    void setPatientName(QString name);
    QString dob() const;
    void setDob(QString dob);
    QString implantDate() const;
    void setImplantDate(QString implantDate);
    QString phone() const;
    void setPhone(QString phone);
    QString clinician() const;
    void setClinician(QString clinician);
    QString clinic() const;
    void setClinic(QString clinic);
    QString serialNumber() const;
    void setSerialNumber(QString serialNumber);
    QString implantDoctor() const;
    void setImplantDoctor(QString implantDoctor);
    QString implantLocation() const;
    void setImplantLocation(QString implantLocation);
    QString calCode() const;
    void setCalCode(QString calCode);
    QString baselineCode() const;
    void setBaselineCode(QString baseline);
    bool uploadPending() const;
    void setUploadPending(bool uploadPending);
    bool selected() const;
    void setSelected(bool selected);

   signals:
    void patientInfoChanged() const;
    void uploadPendingChanged() const;
    void selectedChanged() const;

   private:
    int m_patientId;
    QString m_firstName;
    QString m_middleInitial;
    QString m_lastName;
    QString m_patientName;
    QString m_dob;
    QString m_implantDate;
    QString m_phone;
    QString m_clinician;
    QString m_clinic;
    QString m_serialNumber;
    QString m_implantDoctor;
    QString m_implantLocation;
    QString m_calCode;
    QString m_baselineCode;
    bool m_uploadPending = false;
    bool m_selected = false;
};
