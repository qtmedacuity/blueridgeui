#pragma once

/**
 * @file   ReviewController.h
 *
 * \brief This file declares the Review Controller class.
 *
 * Description:
 * Provides the UI with controls and data to interact with review data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>

class ITransactionProvider;
class Recording;
class SensorCalibration;
class CardiacOutput;
class RhcValues;
class PatientInfo;

class ReviewController : public QObject
{
    Q_OBJECT

   public:
    explicit ReviewController(ITransactionProvider& provider, QObject* parent = nullptr);
    ~ReviewController() override;

    Q_INVOKABLE void getReviewData(int transactionId, int patientId);
    Q_INVOKABLE SensorCalibration* getSensorCalibration();
    Q_INVOKABLE CardiacOutput* getCardiacOutput();
    Q_INVOKABLE RhcValues* getRhcValues();
    Q_INVOKABLE PatientInfo* getPatientInfo();
    Q_INVOKABLE QString getEventDateTime();
    Q_INVOKABLE void commitTransaction(int transactionId, int patientId);
    Q_INVOKABLE void commitAndUpload(int transactionId, int patientId);

   signals:
    void reviewDataReady();
    void transactionCommitted();
    void uploadCompleted();

   private slots:
    void handleTransactionCommit();
    void handleTransactionDetails();
    void handleUploadToMerlin();

   private:
    void handleTransactionCommitResponse();
    void handleTransactionDetailsResponse();
    void handleUploadToMerlinResponse();

   private:
    ITransactionProvider& m_transactionProvider;
    unsigned long m_transactionCommitCbId;
    unsigned long m_transactionDetailsCbId;
    unsigned long m_uploadToMerlinCbId;
    bool m_needToUpload;
    int m_transactionId;
    int m_patientId;

    SensorCalibration* m_sensorCalibration;
    CardiacOutput* m_cardiacOutput;
    RhcValues* m_rhcValues;
    PatientInfo* m_patientInfo;
    QString m_eventDateTime;
};
