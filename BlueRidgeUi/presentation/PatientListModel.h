#pragma once

/**
 * @file   PatientListModel.h
 *
 * \brief This file contains the declaration of the patient list model class.
 *
 * Description:
 * Provides data, methods and signals needed by the patient list view UI control.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QAbstractListModel>
#include <QObject>

class PatientListProvider;
class PatientInfo;

class PatientListModel : public QAbstractListModel
{
    Q_OBJECT
   public:
    enum PatientRole {
        FullNameRole,
        DobRole,
        PatientIdRole,
        SerialNumberRole,
        ImplantDateRole,
        UploadPendingRole,
        SelectedRole
    };
    Q_ENUM(PatientRole)

    explicit PatientListModel(QObject *parent = nullptr);
    ~PatientListModel() override;

    int rowCount(const QModelIndex & = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    QHash<int, QByteArray> roleNames() const override;

    void sort(int column, Qt::SortOrder order) override;

    PatientInfo *get(int index);
    QList<PatientInfo *> getSelectedPatients();
    int selectAllPatients(bool selected);

    void setPatientList(QList<PatientInfo *> patients);

   private:
    QList<PatientInfo *> m_patients;
};
