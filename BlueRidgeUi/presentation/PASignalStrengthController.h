#pragma once

/**
 * @file   PASignalStrengthController.h
 *
 * \brief This file contains the declaration of the PA Pressure Signal Strength Controller class.
 *
 * Description:
 * Provides the UI with PA Pressure Signal Status and update signals.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <presentation/SignalStrengthController.h>

class ISensorDataProvider;

class PASignalStrengthController : public SignalStrengthController
{
    Q_OBJECT
    Q_PROPERTY(bool pulsatility READ pulsatility NOTIFY pulsatilityChanged)
    Q_PROPERTY(bool pulsatilityOverride READ pulsatilityOverride WRITE setPulsatilityOverride NOTIFY
                   pulsatilityOverrideChanged)

   public:
    PASignalStrengthController(StatusValueList const& barvalues, QString const& noPulseTag,
                               ISensorDataProvider& sensorProvider);
    ~PASignalStrengthController() override;

    Q_INVOKABLE QString getStatusAt(int strength) const override;

   signals:
    void pulsatilityChanged(bool);
    void pulsatilityOverrideChanged(bool);

   public slots:
    bool pulsatility() const;
    bool pulsatilityOverride() const;
    void setPulsatilityOverride(bool override);

   private slots:
    void updatePulsatility(bool pulse);

   private:
    void handleSignalStrength(int value);
    void handlePulsatility(bool value);

   private:
    QString const m_noPulseTag;
    ISensorDataProvider& m_sensorProvider;
    bool m_pulsatility;
    bool m_pulsatilityOverride;
    unsigned long m_sensorCB;
    unsigned long m_searchCB;
};
