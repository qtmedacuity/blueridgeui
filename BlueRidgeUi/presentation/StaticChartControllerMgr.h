#pragma once

#include <QMap>
#include <QObject>
#include <QString>

class StaticChartController;
class StaticChartControllerMgr : public QObject
{
    Q_OBJECT

    typedef QMap<QString, StaticChartController*> ControllerMap;

   public:
    explicit StaticChartControllerMgr(const QString& resource, QObject* parent = nullptr);
    ~StaticChartControllerMgr() override;

    Q_INVOKABLE StaticChartController* controller(const QString& source);

   private:
    QString m_resource;
    ControllerMap m_controllers;
};
