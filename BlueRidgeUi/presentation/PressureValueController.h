#pragma once

/**
 * @file   PressureValueController.h
 *
 * \brief This file contains the declaration of the Pressure Value Controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the pressure values
 * (systolic/diastolic, mean and heart rate).
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QObject>

#include <QTimer>

class ISensorDataProvider;

class PressureValueController : public QObject
{
    Q_OBJECT

   public:
    explicit PressureValueController(ISensorDataProvider& sensorProvider, QObject* parent = nullptr);
    virtual ~PressureValueController();

    Q_PROPERTY(int systolic READ systolic NOTIFY systolicChanged)
    Q_PROPERTY(int diastolic READ diastolic NOTIFY diastolicChanged)
    Q_PROPERTY(int mean READ mean NOTIFY meanChanged)
    Q_PROPERTY(int heartRate READ heartRate NOTIFY heartRateChanged)

    Q_INVOKABLE void getPressureValues(QString command);

   signals:
    void systolicChanged();
    void diastolicChanged();
    void meanChanged();
    void heartRateChanged();

   public slots:
    int systolic() const;
    int diastolic() const;
    int mean() const;
    int heartRate() const;

   private slots:
    void updateMean(int value);
    void updateSystolicPressures(int systolic, int diastolic);
    void updateHeartRate(int value);
    void updatePressures();

   private:
    PressureValueController(PressureValueController const&) = delete;
    PressureValueController& operator=(PressureValueController const&) = delete;

    void handleMeanValue(int value);
    void handleSystolicValues(int systolic, int diastolic);
    void handleHeartRate(int value);

    ISensorDataProvider& sensorProvider;
    unsigned long m_meanValueCbId;
    unsigned long m_systolicPressuresCbId;
    unsigned long m_heartRateCbId;
    int m_systolic;
    int m_diastolic;
    int m_mean;
    int m_heartRate;

    QTimer* m_timer;
};
