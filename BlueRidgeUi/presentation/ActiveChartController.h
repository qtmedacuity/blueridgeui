/**
 * @file   ActiveChartController.h
 *
 * \brief This file declares the Active Chart Controller class.
 *
 * Description:
 * Provides a controller to manage streaming data to a WaveChartWidget.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#pragma once

#include "presentation/WaveChartController.h"

class ISensorDataProvider;

class ActiveChartController : public WaveChartController
{
    Q_OBJECT
    Q_PROPERTY(double rangeInSeconds READ rangeInSeconds WRITE setRangeInSeconds NOTIFY rangeInSecondsChanged)
    Q_PROPERTY(double gapInSeconds READ gapInSeconds WRITE setGapInSeconds NOTIFY gapInSecondsChanged)

   public:
    explicit ActiveChartController(ISensorDataProvider& provider, const QString& resource, QObject* parent = nullptr);
    ~ActiveChartController() override;

    Q_INVOKABLE void refresh();

   signals:
    void rangeInSecondsChanged(double);
    void gapInSecondsChanged(double);

   public slots:
    double rangeInSeconds() const;
    void setRangeInSeconds(double seconds);

    double gapInSeconds() const;
    void setGapInSeconds(double seconds);

   private slots:
    void clearSensorData();
    void updateSensorData(unsigned long sequence, double pressure);
    void updateSensorDataRate(unsigned long dataRateHz);

   private:
    explicit ActiveChartController(QObject* = nullptr) = delete;
    ActiveChartController(ActiveChartController const&) = delete;
    ActiveChartController& operator=(ActiveChartController const&) = delete;

    void handleSensorData(unsigned long sequence, double pressure);
    void handleSensorDataRate(unsigned int dataRateHz);

   private:
    ISensorDataProvider& m_provider;
    unsigned long m_rateCbId;
    unsigned long m_dataCbId;
    unsigned long m_samplesPerSecond;
    double m_rangeInSeconds;
    unsigned long m_totalSampleRange;
    double m_gapInSeconds;
    int m_arraysIndex;
    double m_lastRange;
    unsigned long m_lastSequence;
    unsigned long m_adjustSequence;
    bool m_reset;
};
