#pragma once

/**
 * @file   SignalStrengthController.h
 *
 * \brief This file contains the declaration of the signal strength controller class.
 *
 * Description:
 * Provides data, methods and signals needed by the UI to display the signal strength
 * indicator.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <tuple>
#include <vector>

#include <QObject>

class SignalStrengthController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int strength READ strength WRITE setStrength NOTIFY strengthChanged)
    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)

   public:
    typedef std::tuple<std::string, int> StatusValue;
    typedef std::vector<StatusValue> StatusValueList;

    SignalStrengthController(StatusValueList const& statusvalues = {});

    Q_INVOKABLE virtual QString getStatusAt(int strength) const;

   signals:
    void strengthChanged(int signal);
    void statusChanged(QString status);

   public slots:
    virtual int strength() const;
    virtual void setStrength(int value);
    virtual QString status() const;
    virtual void setStatus(QString const& value);

   private:
    SignalStrengthController(QObject* parent = nullptr) = delete;
    SignalStrengthController(SignalStrengthController const&) = delete;
    SignalStrengthController& operator=(SignalStrengthController const&) = delete;

   protected:
    StatusValueList m_statusValueList;
    int m_strength;
    QString m_status;
};
