#pragma once

/**
 * @file ICellularStrengthProvider.h
 *
 * \brief This file declares the interface of a Cellular Strength Provider class.
 *
 * Description:
 * Provides cellular strength, status and carrier.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>
#include <string>

class ICellularStrengthProvider
{
   public:
    virtual ~ICellularStrengthProvider();
    virtual unsigned long registerCellularStrengthCallback(std::function<void()> func) = 0;
    virtual void unregisterCellularStrengthCallback(unsigned long funcId) = 0;
    virtual int getStrength() = 0;
    virtual std::string getCarrier() = 0;
    virtual bool isConnected() = 0;
    virtual bool isEnabled() = 0;
    virtual bool isSet() = 0;
};
