#pragma once

/**
 * @file ISensorDataProvider.h
 *
 * \brief This file declares the interface of a Sensor Data Provider class.
 *
 * Description:
 * Provides commands and access to streaming sensor data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>

class ISensorDataProvider
{
   public:
    typedef std::function<void(unsigned int)> UIntCallback;
    typedef std::function<void(unsigned long, double)> ULongDoubleCallback;
    typedef std::function<void(unsigned int, unsigned int)> UIntUIntCallback;
    typedef std::function<void(bool)> BoolCallback;

    virtual ~ISensorDataProvider();
    virtual unsigned long registerDataRateCallback(UIntCallback func) = 0;
    virtual void unregisterDataRateCallback(unsigned long funcId) = 0;
    virtual unsigned long registerSensorDataPointCallback(ULongDoubleCallback func) = 0;
    virtual void unregisterSensorDataPointCallback(unsigned long funcId) = 0;
    virtual unsigned long registerSignalStrengthCallback(UIntCallback func) = 0;
    virtual void unregisterSignalStrengthCallback(unsigned long funcId) = 0;
    virtual unsigned long registerMeanValueCallback(UIntCallback func) = 0;
    virtual void unregisterMeanValueCallback(unsigned long funcId) = 0;
    virtual unsigned long registerSystolicValuesCallback(UIntUIntCallback func) = 0;
    virtual void unregisterSystolicValuesCallback(unsigned long funcId) = 0;
    virtual unsigned long registerHeartRateCallback(UIntCallback func) = 0;
    virtual void unregisterHeartRateCallback(unsigned long funcId) = 0;
    virtual unsigned long registerPulsatilityCallback(BoolCallback func) = 0;
    virtual void unregisterPulsatilityCallback(unsigned long funcId) = 0;

    virtual void sendSignalStrength() = 0;
    virtual void sendMean() = 0;
    virtual void sendSystolicDiastolic() = 0;
    virtual void sendHeartRate() = 0;
    virtual void sendPulsatility() = 0;
};
