#pragma once

/**
 * @file IMessageParser.h
 *
 * \brief This file declares the interface of a Message Parser class.
 *
 * Description:
 * Provides command and response packet handling to and from the socket.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/DataSocketError.h"

#include <functional>
#include <memory>
#include <string>

class IMessageParser
{
   public:
    typedef std::shared_ptr<const std::string> Payload;
    typedef std::function<void(unsigned int, Payload)> Callback;
    typedef std::function<void(DataSocketError)> ErrorCallback;

    virtual ~IMessageParser();
    virtual unsigned long registerCallback(Callback func) = 0;
    virtual void unregisterCallback(unsigned long funcId) = 0;
    virtual unsigned long registerErrorCallback(ErrorCallback func) = 0;
    virtual void unregisterErrorCallback(unsigned long funcId) = 0;
    virtual bool sendCommand(unsigned int cmd) = 0;
    virtual bool sendCommand(unsigned int cmd, std::string const& payload) = 0;
    virtual bool sendCommand(unsigned int cmd, Payload payload) = 0;
};
