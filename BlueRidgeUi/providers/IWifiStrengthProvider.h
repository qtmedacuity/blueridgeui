#pragma once

/**
 * @file IWifiStrengthProvider.h
 *
 * \brief This file declares the interface of a Wifi Strength Provider class.
 *
 * Description:
 * Provides wifi strength, status and network.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>
#include <string>

class IWifiStrengthProvider
{
   public:
    virtual ~IWifiStrengthProvider();
    virtual unsigned long registerWifiStrengthCallback(std::function<void()> func) = 0;
    virtual void unregisterWifiStrengthCallback(unsigned long funcId) = 0;
    virtual int getStrength() = 0;
    virtual std::string getNetworkName() = 0;
    virtual bool isConnected() = 0;
    virtual bool isEnabled() = 0;
    virtual bool isSet() = 0;
};
