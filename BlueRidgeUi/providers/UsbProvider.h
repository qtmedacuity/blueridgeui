#pragma once

/**
 * @file   UsbProvider.h
 *
 * \brief This file declares the Usb Provider class.
 *
 * Description:
 * Provides usb device controls and information.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/IUsbProvider.h"
#include "providers/MessageDataProvider.h"

#include <cmem_usbStatus.h>

#include <mutex>

class UsbProvider : public IUsbProvider, public MessageDataProvider
{
   public:
    UsbProvider(IMessageParser& parser);

    unsigned long registerUsbCallback(std::function<void()> func) override;
    void unregisterUsbCallback(unsigned long funcId) override;
    void requestUsbStatus() override;
    bool isPresent() override;
    bool isValid() override;
    std::string getSerialNumber() override;
    std::string getCalCode() override;
    std::string getBaseline() override;

   private:
    UsbProvider() = delete;
    UsbProvider(UsbProvider const&) = delete;
    UsbProvider& operator=(UsbProvider const&) = delete;

    void handleMessage(unsigned int cmd, std::string const& payload) override;

   private:
    cmem_usbStatus m_usbStatus;
    std::mutex m_dataMutex;
};
