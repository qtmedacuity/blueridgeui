#pragma once
/**
 * @file   PatientProvider.h
 *
 * \brief This file declares the Patient Provider class.
 *
 * Description:
 * Provides controls and information of a patient.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/IPatientProvider.h"
#include "providers/MessageDataProvider.h"

#include <cmem_patient.h>

#include <map>
#include <mutex>

class PatientProvider : public IPatientProvider, public MessageDataProvider
{
    typedef std::vector<std::string> ListData;
    typedef std::map<std::string, ListData> ListDataMap;

   public:
    PatientProvider(IMessageParser& parser);

    unsigned long registerPatientSavedCallback(std::function<void()> func) override;
    void unregisterPatientSavedCallback(unsigned long funcId) override;
    unsigned long registerListDataCallback(std::function<void()> func) override;
    void unregisterListDataCallback(unsigned long funcId) override;

    void savePatient(int transactionId, PatientData const& patient) override;
    void requestListData(std::string type) override;

    int getPatientId() const override;
    std::vector<std::string> getListData(std::string type) const override;

   private:
    PatientProvider() = delete;
    PatientProvider(PatientProvider const&) = delete;
    PatientProvider& operator=(PatientProvider const&) = delete;

    void handleMessage(unsigned int cmd, std::string const& payload) override;

   private:
    int m_patientId;
    ListDataMap m_listDataMap;
    mutable std::mutex m_dataMutex;
};
