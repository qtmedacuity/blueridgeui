#pragma once

/**
 * @file IActiveObject.h
 *
 * \brief This file declares the interface of an Active Object class.
 *
 * Description:
 * Provides methods to serialize function calls on a separate thread.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>

class IActiveObject
{
   public:
    typedef std::function<void()> Message;

    virtual ~IActiveObject();
    virtual void send(Message msg) = 0;
};
