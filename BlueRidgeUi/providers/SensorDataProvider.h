#pragma once

/**
 * @file   SensorDataProvider.h
 *
 * \brief This file declares the Sensor Data Provider class.
 *
 * Description:
 * Provides commands and access to streaming sensor data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/ActiveObject.h"
#include "providers/ISensorDataParser.h"
#include "providers/ISensorDataProvider.h"

#include "cardiomems_types.h"

#include <mutex>
#include <vector>

class SensorDataProvider : public ISensorDataProvider
{
   public:
    typedef std::vector<ULongDoubleCallback> ULongDoubleCallbackList;
    typedef std::vector<UIntCallback> UIntCallbackList;
    typedef std::vector<UIntUIntCallback> UIntUIntCallbackList;
    typedef std::vector<BoolCallback> BoolCallbackList;

    SensorDataProvider(ISensorDataParser& parser);
    ~SensorDataProvider() override;

    unsigned long registerDataRateCallback(UIntCallback func) override;
    void unregisterDataRateCallback(unsigned long funcId) override;
    unsigned long registerSensorDataPointCallback(ULongDoubleCallback func) override;
    void unregisterSensorDataPointCallback(unsigned long funcId) override;
    unsigned long registerSignalStrengthCallback(UIntCallback func) override;
    void unregisterSignalStrengthCallback(unsigned long funcId) override;
    unsigned long registerMeanValueCallback(UIntCallback func) override;
    void unregisterMeanValueCallback(unsigned long funcId) override;
    unsigned long registerSystolicValuesCallback(UIntUIntCallback func) override;
    void unregisterSystolicValuesCallback(unsigned long funcId) override;
    unsigned long registerHeartRateCallback(UIntCallback func) override;
    void unregisterHeartRateCallback(unsigned long funcId) override;
    unsigned long registerPulsatilityCallback(BoolCallback func) override;
    void unregisterPulsatilityCallback(unsigned long funcId) override;

    void sendSignalStrength() override;
    void sendMean() override;
    void sendSystolicDiastolic() override;
    void sendHeartRate() override;
    void sendPulsatility() override;

   private:
    SensorDataProvider() = delete;
    SensorDataProvider(SensorDataProvider const&) = delete;
    SensorDataProvider& operator=(SensorDataProvider const&) = delete;

    void handleSensorData(ISensorDataParser::DataListPtr sensorReadings);

    template <typename CBList, typename Func>
    unsigned long registerCallback(CBList& cblist, Func func)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        unsigned long funcId(cblist.size());
        cblist.push_back(func);
        return funcId;
    }

    template <typename CBList>
    void unregisterCallback(CBList& cblist, unsigned long funcId)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (cblist.size() > funcId) {
            cblist.at(funcId) = nullptr;
        }
    }

   private:
    ISensorDataParser& m_parser;
    unsigned long m_cbId;
    unsigned long m_sequenceNumber;
    unsigned int m_signalStrength;
    unsigned int m_meanValue;
    unsigned int m_systolicPressure;
    unsigned int m_diastolicPressure;
    unsigned int m_heartRate;
    bool m_pulsatility;
    ULongDoubleCallbackList m_sensorDataPointCallbacks;
    UIntCallbackList m_signalStrengthCallbacks;
    UIntCallbackList m_meanValueCallbacks;
    UIntUIntCallbackList m_systolicValuesCallbacks;
    UIntCallbackList m_heartRateCallbacks;
    BoolCallbackList m_pulsatilityCallbacks;
    mutable std::mutex m_mutex;
    ActiveObject m_activeObj;
};
