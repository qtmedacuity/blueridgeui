#pragma once

/**
 * @file SearchbarProvider.cpp
 *
 * \brief This file declares the Searchbar Provider class.
 *
 * Description:
 * Provides access to commands and data for the pressure search bar.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/ISearchbarProvider.h"
#include "providers/MessageDataProvider.h"

#include <map>
#include <mutex>
#include <string>

class IMessageParser;

class SearchbarProvider : public ISearchbarProvider, public MessageDataProvider
{
    typedef std::map<unsigned int, std::function<void(unsigned int, const std::string&)>> HandlerMap;

   public:
    SearchbarProvider(IMessageParser& parser);
    unsigned long registerSearchPressureCallback(ISearchbarProvider::Callback func) override;
    void unregisterSearchPressureCallback(unsigned long funcId) override;
    unsigned long registerAutoSearchingCallback(ISearchbarProvider::Callback func) override;
    void unregisterAutoSearchingCallback(unsigned long funcId) override;
    unsigned int searchPressure() const override;
    void setSearchPressure(unsigned int value) override;
    bool autoSearching() const override;
    void setAutoSearching() override;

   private:
    void handleMessage(unsigned int cmd, std::string const& payload) override;
    void handleSearchPressure(unsigned int cmd, const std::string& payload);
    void handleAutoSearch(unsigned int cmd, const std::string& payload);

   private:
    mutable std::mutex m_mutex;
    HandlerMap m_handlers;
    unsigned int m_searchPresssure;
    bool m_autoSearching;
    ActiveObject m_handlerActObj;
};
