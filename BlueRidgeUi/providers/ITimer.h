#pragma once

/**
 * @file ITimer.h
 *
 * \brief This file declares the interface of a Timer class.
 *
 * Description:
 * Provides a user defined function call at regular intervals based on a millisecond timer.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <chrono>
#include <functional>

class ITimer
{
   public:
    typedef std::function<void()> Callback;
    virtual ~ITimer();
    virtual void start(std::chrono::microseconds const& timeout, Callback func) = 0;
    virtual void start(unsigned int timeoutHz, Callback func) = 0;
    virtual void start(std::chrono::microseconds const& timeout) = 0;
    virtual void start(unsigned int timeoutHz) = 0;
    virtual void stop() = 0;
};
