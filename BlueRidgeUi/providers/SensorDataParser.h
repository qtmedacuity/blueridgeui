#pragma once

/**
 * @file SensorDataParser.h
 *
 * \brief This file declares the Sensor Data Parser class.
 *
 * Description:
 * Parses sensor data from the data socket.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <chrono>
#include <mutex>
#include <vector>

#include "providers/ActiveObject.h"
#include "providers/ISensorDataParser.h"

class IDataSocket;
class ITimer;

class SensorDataParser : public ISensorDataParser
{
    typedef std::chrono::system_clock Clock;
    typedef std::chrono::time_point<Clock> TimePoint;

    typedef std::vector<DataCallback> DataCallbackList;
    typedef std::vector<DataRateCallback> DataRateCallbackList;
    typedef std::vector<DataSocketErrorCallback> ErrorCallbackList;

   public:
    explicit SensorDataParser(ITimer& timer, IDataSocket& socket,
                              std::chrono::milliseconds socketReadTimeout = std::chrono::milliseconds(5000));
    ~SensorDataParser() override;

    unsigned long registerDataCallback(DataCallback func) override;
    void unregisterDataCallback(unsigned long funcId) override;
    unsigned long registerDataRateCallback(DataRateCallback func) override;
    void unregisterDataRateCallback(unsigned long funcId) override;
    unsigned long registerErrorCallback(DataSocketErrorCallback func) override;
    void unregisterErrorCallback(unsigned long funcId) override;
    void start(unsigned int readRateHz, unsigned int flushRateHz) override;
    void restart() override;
    void stop() override;
    void streaming(bool enabled) override;
    void reading(bool enabled) override;

   private:
    SensorDataParser() = delete;
    SensorDataParser(SensorDataParser&) = delete;
    SensorDataParser& operator=(SensorDataParser&) = delete;

    DataListPtr extractBuffers();
    void setBuffer(DataReading_t const& buffer);
    bool active() const;
    void setActive(bool enabled);
    bool streamingEnabled() const;
    void setStreamingEnabled(bool enabled);

    void flushData();
    void readBytes();
    void signalError(DataSocketError err);

   private:
    ITimer& m_timer;
    IDataSocket& m_socket;
    const std::chrono::milliseconds m_socketReadTimeout;
    mutable std::mutex m_cbMutex;
    mutable std::mutex m_dataMutex;
    unsigned int m_readRateHz;
    unsigned int m_flushRateHz;
    bool m_active;
    bool m_streaming;
    bool m_rfenabled;
    TimePoint m_readTimeout;
    DataCallbackList m_dataCallbacks;
    DataRateCallbackList m_dataRateCallbacks;
    ErrorCallbackList m_errorCallbacks;
    DataListPtr m_sensorReadings;
    ActiveObject m_flushActObj;
    ActiveObject m_readActObj;
    ActiveObject m_errorActObj;
};
