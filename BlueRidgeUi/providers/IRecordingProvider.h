#pragma once

/**
 * @file IRecordingProvider.h
 *
 * \brief This file declares the interface of a Recording Provider class.
 *
 * Description:
 * Provides recording commands and information.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/RecordingData.h"

#include <functional>

class PaMeanData;
class CardiacOutputData;
class RhcData;

class IRecordingProvider
{
   public:
    virtual ~IRecordingProvider();
    virtual unsigned long registerRecordingStatusCallback(std::function<void()> func) = 0;
    virtual void unregisterRecordingStatusCallback(unsigned long funcId) = 0;
    virtual unsigned long registerReadyToRecordCallback(std::function<void()> func) = 0;
    virtual void unregisterReadyToRecordCallback(unsigned long funcId) = 0;
    virtual unsigned long registerTakeReadingCallback(std::function<void()> func) = 0;
    virtual void unregisterTakeReadingCallback(unsigned long funcId) = 0;
    virtual unsigned long registerGetPaMeanCallback(std::function<void()> func) = 0;
    virtual void unregisterGetPaMeanCallback(unsigned long funcId) = 0;
    virtual unsigned long registerSetPaMeanCallback(std::function<void()> func) = 0;
    virtual void unregisterSetPaMeanCallback(unsigned long funcId) = 0;
    virtual unsigned long registerSaveRecordingCallback(std::function<void()> func) = 0;
    virtual void unregisterSaveRecordingCallback(unsigned long funcId) = 0;
    virtual unsigned long registerDeleteRecordingCallback(std::function<void()> func) = 0;
    virtual void unregisterDeleteRecordingCallback(unsigned long funcId) = 0;
    virtual unsigned long registerSetCardiacOutputCallback(std::function<void()> func) = 0;
    virtual void unregisterSetCardiacOutputCallback(unsigned long funcId) = 0;
    virtual unsigned long registerSetRhcValuesCallback(std::function<void()> func) = 0;
    virtual void unregisterSetRhcValuesCallback(unsigned long funcId) = 0;

    virtual void requestRecordingStatus(std::string command) = 0;
    virtual void takeReading(int transactionId) = 0;
    virtual void requestPaMean() = 0;
    virtual void setPaMean(int transactionId, int paMean) = 0;
    virtual void saveRecording(int transactionId, RecordingData const& recordingData) = 0;
    virtual void deleteRecording(int transactionId, int recordingId) = 0;
    virtual void setCardiacOutput(int transactionId, CardiacOutputData coData) = 0;
    virtual void setRhcValues(int transactionId, RhcData const& rhcData) = 0;

    virtual bool isReadyToRecord() const = 0;
    virtual int getRecordingId() const = 0;
    virtual std::string getWaveFormFileName() const = 0;
    virtual int getPaMean() const = 0;
    virtual PaMeanData getPaMeanData() const = 0;
    virtual RecordingData getSavedRecording() const = 0;
    virtual int getDeletedRecordingId() const = 0;
};
