#pragma once

/**
 * @file ISensorDataParser.h
 *
 * \brief This file declares the interface of a Sensor Data Parser class.
 *
 * Description:
 * Parses sensor data from the data socket.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/DataSocketError.h"

#include <cardiomems_types.h>
#include <functional>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

class ISensorDataParser
{
   public:
    typedef std::vector<DataReading_t> DataList;
    typedef std::shared_ptr<DataList> DataListPtr;
    typedef std::function<void(DataListPtr)> DataCallback;
    typedef std::function<void(unsigned int)> DataRateCallback;

    virtual ~ISensorDataParser();
    virtual unsigned long registerDataCallback(DataCallback func) = 0;
    virtual void unregisterDataCallback(unsigned long funcId) = 0;
    virtual unsigned long registerDataRateCallback(DataRateCallback func) = 0;
    virtual void unregisterDataRateCallback(unsigned long funcId) = 0;
    virtual unsigned long registerErrorCallback(DataSocketErrorCallback func) = 0;
    virtual void unregisterErrorCallback(unsigned long funcId) = 0;
    virtual void start(unsigned int readRateHz, unsigned int flushRateHz) = 0;
    virtual void restart() = 0;
    virtual void stop() = 0;
    virtual void streaming(bool enabled) = 0;
    virtual void reading(bool enabled) = 0;
};
