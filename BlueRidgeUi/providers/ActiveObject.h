#pragma once

/**
 * @file ActiveObject.h
 *
 * \brief This file declares the Active Object class.
 *
 * Description:
 * Provides methods to serialize function calls on a separate thread.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <atomic>
#include <functional>
#include <memory>
#include <thread>

#include "providers/IActiveObject.h"
#include "providers/ThreadSafeQueue.h"

class ActiveObject : public IActiveObject
{
   public:
    ActiveObject();
    ~ActiveObject() override;
    void send(Message msg) override;

   private:
    ActiveObject(ActiveObject const&) = delete;
    ActiveObject& operator=(ActiveObject const&) = delete;

    void run();

   private:
    std::atomic<bool> m_done;
    ThreadSafeQueue<Message> m_queue;
    std::unique_ptr<std::thread> m_thread;
};
