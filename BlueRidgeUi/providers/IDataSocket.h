#pragma once

/**
 * @file IDataSocket.h
 *
 * \brief This file declares the interface of a Data Socket class.
 *
 * Description:
 * Provides read and write access to a data socket.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <cstdio>
#include <string>

class IDataSocket
{
   public:
    virtual ~IDataSocket();

    virtual int connection(const char* serverFile, int msecTimeout, int& err, std::string& errstr) = 0;
    virtual int disconnect() = 0;
    virtual int reconnect(int& err, std::string& errstr) = 0;

    virtual ssize_t readbytes(void* buffer, size_t count, int& err, std::string& errstr) = 0;
    virtual ssize_t writebytes(void const* buffer, size_t count, int& err, std::string& errstr) = 0;
};
