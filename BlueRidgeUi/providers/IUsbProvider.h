#pragma once

/**
 * @file IUsbProvider.h
 *
 * \brief This file declares the interface of a Usb Provider class.
 *
 * Description:
 * Provides usb device controls and information.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>
#include <string>

class IUsbProvider
{
   public:
    virtual ~IUsbProvider();
    virtual unsigned long registerUsbCallback(std::function<void()> func) = 0;
    virtual void unregisterUsbCallback(unsigned long funcId) = 0;
    virtual void requestUsbStatus() = 0;
    virtual bool isPresent() = 0;
    virtual bool isValid() = 0;
    virtual std::string getSerialNumber() = 0;
    virtual std::string getCalCode() = 0;
    virtual std::string getBaseline() = 0;
};
