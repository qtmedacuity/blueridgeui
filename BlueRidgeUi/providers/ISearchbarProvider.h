#pragma once

/**
 * @file ISearchbarProvider.h
 *
 * \brief This file declares the interface of a Searchbar Provider class.
 *
 * Description:
 * Provides access to commands and data for the pressure search bar.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>

class ISearchbarProvider
{
   public:
    typedef std::function<void()> Callback;

    virtual ~ISearchbarProvider();
    virtual unsigned long registerSearchPressureCallback(Callback func) = 0;
    virtual void unregisterSearchPressureCallback(unsigned long funcId) = 0;
    virtual unsigned long registerAutoSearchingCallback(Callback func) = 0;
    virtual void unregisterAutoSearchingCallback(unsigned long funcId) = 0;
    virtual unsigned int searchPressure() const = 0;
    virtual void setSearchPressure(unsigned int value) = 0;
    virtual bool autoSearching() const = 0;
    virtual void setAutoSearching() = 0;
};
