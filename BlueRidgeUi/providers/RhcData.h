#pragma once

/**
 * @file   RhcData.h
 *
 * \brief This file declares the Rhc Data class.
 *
 * Description:
 * Provides a simple data object for storing rhc data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <string>

class cmem_rhc;
struct RhcData {
    std::string date;
    std::string time;
    int ra;
    int rvSystolic;
    int rvDiastoic;
    int paSystolic;
    int paDiastolic;
    int paMean;
    int pcwp;
    int heartRate;
    int signalStrength;
    std::string notes;
    std::string waveformFile;
    std::string position;

    RhcData();
    explicit RhcData(cmem_rhc& rhc);
    void toCmemRhc(cmem_rhc& rhc) const;
};
