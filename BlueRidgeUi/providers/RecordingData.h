#pragma once

/**
 * @file   RecordingData.h
 *
 * \brief This file declares the Recording Data class.
 *
 * Description:
 * Provides a simple data object for storing data from a recording.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "cmem_recording.h"

struct RecordingData {
    int recordingId;
    bool uploadPending;
    std::string recordingDate;
    std::string recordingTime;
    int systolic;
    int diastolic;
    int mean;
    int heartRate;
    int signalStrength;
    int refSystolic;
    int refDiastolic;
    int refMean;
    std::string notes;
    std::string patientPosition;
    std::string waveformFile;

    RecordingData();
    RecordingData(cmem_recording& recording);
};
