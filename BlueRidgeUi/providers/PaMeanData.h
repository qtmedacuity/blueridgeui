#pragma once

/**
 * @file   PaMeanData.h
 *
 * \brief This file declares the Pa Mean Data class.
 *
 * Description:
 * Provides a simple data object for storing data received after saving the PA mean.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "json/json.h"

struct PaMeanData {
    int paMean;
    int offset;
    int systolic;
    int diastolic;
    int mean;

    PaMeanData();
    explicit PaMeanData(const Json::Value *values);

    void add_field(const char *key, const std::string &stringValue);
};
