#pragma once
/**
 * @file   RecordingProvider.h
 *
 * \brief This file declares the Recording Provider class.
 *
 * Description:
 * Provides controls and information of a recording.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/IRecordingProvider.h"
#include "providers/MessageDataProvider.h"
#include "providers/PaMeanData.h"

#include <mutex>

class RecordingProvider : public IRecordingProvider, public MessageDataProvider
{
   public:
    RecordingProvider(IMessageParser& parser);

    unsigned long registerRecordingStatusCallback(std::function<void()> func) override;
    void unregisterRecordingStatusCallback(unsigned long funcId) override;
    unsigned long registerReadyToRecordCallback(std::function<void()> func) override;
    void unregisterReadyToRecordCallback(unsigned long funcId) override;
    unsigned long registerTakeReadingCallback(std::function<void()> func) override;
    void unregisterTakeReadingCallback(unsigned long funcId) override;
    unsigned long registerGetPaMeanCallback(std::function<void()> func) override;
    void unregisterGetPaMeanCallback(unsigned long funcId) override;
    unsigned long registerSetPaMeanCallback(std::function<void()> func) override;
    void unregisterSetPaMeanCallback(unsigned long funcId) override;
    unsigned long registerSaveRecordingCallback(std::function<void()> func) override;
    void unregisterSaveRecordingCallback(unsigned long funcId) override;
    unsigned long registerDeleteRecordingCallback(std::function<void()> func) override;
    void unregisterDeleteRecordingCallback(unsigned long funcId) override;
    unsigned long registerSetCardiacOutputCallback(std::function<void()> func) override;
    void unregisterSetCardiacOutputCallback(unsigned long funcId) override;
    unsigned long registerSetRhcValuesCallback(std::function<void()> func) override;
    void unregisterSetRhcValuesCallback(unsigned long funcId) override;

    void requestRecordingStatus(std::string command) override;
    void takeReading(int transactionId) override;
    void requestPaMean() override;
    void setPaMean(int transactionId, int paMean) override;
    void saveRecording(int transactionId, RecordingData const& recordingData) override;
    void deleteRecording(int transactionId, int recordingId) override;
    void setCardiacOutput(int transactionId, CardiacOutputData coData) override;
    void setRhcValues(int transactionId, RhcData const& rhcData) override;

    bool isReadyToRecord() const override;
    int getRecordingId() const override;
    std::string getWaveFormFileName() const override;
    int getPaMean() const override;
    PaMeanData getPaMeanData() const override;
    RecordingData getSavedRecording() const override;
    int getDeletedRecordingId() const override;

   private:
    RecordingProvider() = delete;
    RecordingProvider(RecordingProvider const&) = delete;
    RecordingProvider& operator=(RecordingProvider const&) = delete;

    void handleMessage(unsigned int cmd, std::string const& payload) override;

   private:
    bool m_readyToRecord;
    int m_recordingId;
    std::string m_waveFormFileName;
    int m_paMean;
    PaMeanData m_paMeanData;
    RecordingData m_savedRecording;
    int m_deletedRecordingId;
    mutable std::mutex m_dataMutex;
};
