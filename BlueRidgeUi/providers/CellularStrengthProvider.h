#pragma once

/**
 * @file   CellularStrengthProvider.h
 *
 * \brief This file declares the Cellular Strength Provider class.
 *
 * Description:
 * Provides cellular strength, status and carrier.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/ICellularStrengthProvider.h"
#include "providers/MessageDataProvider.h"

#include <cmem_cellStatus.h>

#include <mutex>

class ITimer;

class CellularStrengthProvider : public ICellularStrengthProvider, public MessageDataProvider
{
   public:
    CellularStrengthProvider(ITimer& timer, IMessageParser& parser, unsigned int refreshRateHz = 1);
    ~CellularStrengthProvider() override;

    unsigned long registerCellularStrengthCallback(std::function<void()> func) override;
    void unregisterCellularStrengthCallback(unsigned long funcId) override;
    int getStrength() override;
    std::string getCarrier() override;
    bool isConnected() override;
    bool isEnabled() override;
    bool isSet() override;

   private:
    CellularStrengthProvider() = delete;
    CellularStrengthProvider(CellularStrengthProvider const&) = delete;
    CellularStrengthProvider& operator=(CellularStrengthProvider const&) = delete;

    void handleMessage(unsigned int cmd, std::string const& payload) override;

   private:
    ITimer& m_timer;
    cmem_cellStatus m_cellStatus;
    std::mutex m_dataMutex;
};
