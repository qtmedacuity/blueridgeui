#pragma once

/**
 * @file SystemSettingsProvider.h
 *
 * \brief This file declares the system settings class.
 *
 * Description:
 * Provides access to commands and data for system settings management.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/ISystemSettingsProvider.h"
#include "providers/MessageDataProvider.h"

#include <map>
#include <mutex>
#include <string>

class IMessageParser;
class ISensorDataParser;

class SystemSettingsProvider : public ISystemSettingsProvider, public MessageDataProvider
{
    typedef std::map<unsigned int, std::function<void(std::string const&)>> HandlerMap;

   public:
    SystemSettingsProvider(IMessageParser& parser, ISensorDataParser& sensorDataParser);

    unsigned long registerRFStateCallback(SettingsChangedCallback func) override;
    void unregisterRFStateCallback(unsigned long funcId) override;
    unsigned long registerSensorDataSocketErrorCallback(DataSocketErrorCallback func) override;
    void unregisterSensorDataSocketErrorCallback(unsigned long funcId) override;
    unsigned long registerConfigurationCallback(SettingsChangedCallback func) override;
    void unregisterConfigurationCallback(unsigned long funcId) override;

    void requestRFState() override;
    bool rfState() const override;
    void setRFState(bool state) override;

    void requestConfiguration() override;
    Configuration const& configuration() const override;

    void connectSensorDataSocket() override;
    void disconnectSensorDataSocket() override;
    void enableSensorDataSource() override;
    void disableSensorDataSource() override;
    void enableSensorDataStream() override;
    void disableSensorDataStream() override;

   private:
    void handleMessage(unsigned int cmd, std::string const& payload) override;
    void handleRFState(std::string const& payload);
    void handleConfiguration(std::string const& payload);

   private:
    ISensorDataParser& m_sensorDataParser;
    mutable std::mutex m_mutex;
    HandlerMap m_handlers;
    bool m_rfState;
    Configuration m_configuration;
    ActiveObject m_handlerActObj;
};
