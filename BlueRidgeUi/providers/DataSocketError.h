#pragma once

/**
 * @file DataSocketError.h
 *
 * \brief This file declares the enumeration interface of Data Socket errors.
 *
 * Description:
 * Provides error enumeration for data sockets.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>
#include <string>
#include <vector>

enum class DataSocketError : unsigned short {
    NoError = 0,
    ReadFailed,
    WriteFailed,
    ConnectionFailed,
    UserFailed,
    Unknown
};

static const std::vector<std::string> DataSocketErrStr = {"NoError",          "ReadFailed", "WriteFailed",
                                                          "ConnectionFailed", "UserFailed", "Unknown"};

typedef std::function<void(DataSocketError)> DataSocketErrorCallback;
