#pragma once

/**
 * @file ISystemSettingsProvider.h
 *
 * \brief This file declares the interface of a system settings class.
 *
 * Description:
 * Provides access to commands and data for system settings management.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/Configuration.h"
#include "providers/DataSocketError.h"

class ISystemSettingsProvider
{
   public:
    typedef std::function<void()> SettingsChangedCallback;

    virtual ~ISystemSettingsProvider();
    virtual unsigned long registerRFStateCallback(SettingsChangedCallback func) = 0;
    virtual void unregisterRFStateCallback(unsigned long funcId) = 0;
    virtual unsigned long registerSensorDataSocketErrorCallback(DataSocketErrorCallback func) = 0;
    virtual void unregisterSensorDataSocketErrorCallback(unsigned long funcId) = 0;
    virtual unsigned long registerConfigurationCallback(SettingsChangedCallback func) = 0;
    virtual void unregisterConfigurationCallback(unsigned long funcId) = 0;

    virtual void requestRFState() = 0;
    virtual bool rfState() const = 0;
    virtual void setRFState(bool) = 0;

    virtual void requestConfiguration() = 0;
    virtual Configuration const& configuration() const = 0;

    virtual void connectSensorDataSocket() = 0;
    virtual void disconnectSensorDataSocket() = 0;
    virtual void enableSensorDataSource() = 0;
    virtual void disableSensorDataSource() = 0;
    virtual void enableSensorDataStream() = 0;
    virtual void disableSensorDataStream() = 0;
};
