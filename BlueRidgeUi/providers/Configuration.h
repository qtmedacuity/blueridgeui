#pragma once

/**
 * @file Configuration.h
 *
 * \brief This file declares the Configuration class.
 *
 * Description:
 * Provides a data structure for storing system configuration.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <string>
#include <utility>

template <typename T>
struct ConfigMinMax {
    T _min;
    T _max;

    ConfigMinMax() : _min(0), _max(0) {}
    ConfigMinMax(T mn, T mx) : _min(mn), _max(mx) {}
    ConfigMinMax(const ConfigMinMax& other) : _min(other._min), _max(other._max) {}
    ConfigMinMax(ConfigMinMax&& other) : ConfigMinMax() { swap(*this, other); }
    ConfigMinMax& operator=(ConfigMinMax rhs) { swap(*this, rhs); return *this; }
    friend void swap(ConfigMinMax& p1, ConfigMinMax& p2) { using std::swap; swap(p1._min, p2._min); swap(p1._max, p2._max); }
};

template <typename T>
struct ConfigMinMaxDefault : public ConfigMinMax<T> {
    T _default;

    ConfigMinMaxDefault() : ConfigMinMax<T>(0, 0), _default(0) {}
    ConfigMinMaxDefault(T mn, T mx, T def) : ConfigMinMax<T>(mn, mx), _default(def) {}
    ConfigMinMaxDefault(const ConfigMinMaxDefault& other) : ConfigMinMax<T>(other), _default(other._default) {}
    ConfigMinMaxDefault(ConfigMinMaxDefault&& other) : ConfigMinMaxDefault<T>() { swap(*this, other); }
    ConfigMinMaxDefault& operator=(ConfigMinMaxDefault rhs) { swap(*this, rhs); return *this; }
    friend void swap(ConfigMinMaxDefault& p1, ConfigMinMaxDefault& p2) { using std::swap; swap(p1._min, p2._min); swap(p1._max, p2._max); swap(p1._default, p2._default); }
};

typedef ConfigMinMax<unsigned int> ConfigPAMean;
typedef ConfigMinMaxDefault<int> ConfigSearchPressure;
typedef ConfigMinMax<unsigned int> ConfigSystolic;
typedef ConfigMinMax<unsigned int> ConfigDiastolic;
typedef ConfigMinMax<unsigned int> ConfigReferenceMean;

class Configuration
{
   public:
    explicit Configuration();
    explicit Configuration(std::string const& language, ConfigPAMean const& paMean,
                           ConfigSearchPressure const& searchPressure, ConfigSystolic const& systolic,
                           ConfigDiastolic const& diastolic, ConfigReferenceMean const& referenceMean, bool hasDate,
                           bool hasTime);
    Configuration(const Configuration& other);
    Configuration(Configuration&& other);
    Configuration& operator=(Configuration other);
    friend void swap(Configuration& p1, Configuration& p2);

   public:
    std::string _language;
    ConfigPAMean _paMean;
    ConfigSearchPressure _searchPressure;
    ConfigSystolic _systolic;
    ConfigDiastolic _diastolic;
    ConfigReferenceMean _referenceMean;
    bool _hasDate;
    bool _hasTime;
};
