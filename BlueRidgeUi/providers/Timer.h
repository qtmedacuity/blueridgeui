#pragma once

/**
 * @file Timer.h
 *
 * \brief This file declares the Timer class.
 *
 * Description:
 * Provides a user defined function call at regular intervals based on a millisecond timer.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/ActiveObject.h"
#include "providers/ITimer.h"

#include <atomic>
#include <condition_variable>
#include <mutex>

class Timer : public ITimer
{
   public:
    Timer();
    ~Timer() override;

    void start(std::chrono::microseconds const& timeout, Callback func) override;
    void start(unsigned int timeoutHz, Callback func) override;
    void start(std::chrono::microseconds const& timeout) override;
    void start(unsigned int timeoutHz) override;
    void stop() override;

   private:
    bool m_finished;
    Callback m_func;
    std::chrono::microseconds m_timeout;
    std::condition_variable m_condition;
    mutable std::mutex m_mutex;
    ActiveObject m_activeObj;
};
