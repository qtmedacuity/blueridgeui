#pragma once

/**
 * @file IMessageDataProvider.h
 *
 * \brief This file declares the interface of a Message Data Provider class.
 *
 * Description:
 * Provides a handler for managing response data messages.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>
#include <string>

class IMessageDataProvider
{
   public:
    typedef std::function<void()> Callback;
    virtual ~IMessageDataProvider();
    virtual unsigned long registerCallback(unsigned int cmdId, Callback func) = 0;
    virtual void unregisterCallback(unsigned int cmdId, unsigned long funcId) = 0;

   protected:
    virtual void handleMessage(unsigned int cmdId, std::string const& payload) = 0;
};
