#pragma once

/**
 * @file   PatientListProvider.h
 *
 * \brief This file declares the Patient List Provider class.
 *
 * Description:
 * Provides commands and data for patient list information.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/IPatientListProvider.h"
#include "providers/MessageDataProvider.h"

#include <cardiomems_types.h>
#include <cmem_patient_list.h>

#include <mutex>

class PatientInfo;

class PatientListProvider : public IPatientListProvider, public MessageDataProvider
{
    enum PatientSearchType { waitingImplant, implanted, all };

   public:
    PatientListProvider(IMessageParser& parser);

    unsigned long registerPatientListCallback(std::function<void()> func) override;
    void unregisterPatientListCallback(unsigned long funcId) override;
    unsigned long registerRefreshCallback(std::function<void()> func) override;
    void unregisterRefreshCallback(unsigned long funcId) override;
    void requestPatientsWaitingForImplant() override;
    void requestPatientsWithImplant() override;
    void requestAllPatients() override;
    void requestRefresh() override;
    std::string refreshDate() override;
    std::string refreshTime() override;
    std::vector<PatientData> patientList() override;

   private:
    PatientListProvider() = delete;
    PatientListProvider(PatientListProvider const&) = delete;
    PatientListProvider& operator=(PatientListProvider const&) = delete;

    void requestPatientList(eRecordType searchType);
    void handleMessage(unsigned int cmd, std::string const& payload) override;

   private:
    cmem_patient_list m_patientList;
    std::mutex m_dataMutex;
};
