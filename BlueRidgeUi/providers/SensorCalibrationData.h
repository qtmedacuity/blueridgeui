#pragma once

/**
 * @file   SensorCalibrationData.h
 *
 * \brief This file declares the Sensor Calibration Data class.
 *
 * Description:
 * Provides a simple data object for storing data from a sensor calibration.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "cmem_calibration.h"

struct SensorCalibrationData {
    std::string date;
    std::string time;
    int systolic;
    int diastolic;
    int mean;
    int reference;
    int heartRate;
    int signalStrength;
    std::string notes;
    std::string waveformFile;

    SensorCalibrationData();
    explicit SensorCalibrationData(cmem_calibration& calibration);
};
