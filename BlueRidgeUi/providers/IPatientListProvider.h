#pragma once

/**
 * @file IPatientListProvider.h
 *
 * \brief This file declares the interface of a Patient List Provider class.
 *
 * Description:
 * Provides commands and data for patient list data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>
#include <string>
#include <vector>

#include "providers/PatientData.h"

class IPatientListProvider
{
   public:
    virtual ~IPatientListProvider();
    virtual unsigned long registerPatientListCallback(std::function<void()> func) = 0;
    virtual void unregisterPatientListCallback(unsigned long funcId) = 0;
    virtual unsigned long registerRefreshCallback(std::function<void()> func) = 0;
    virtual void unregisterRefreshCallback(unsigned long funcId) = 0;

    virtual void requestPatientsWaitingForImplant() = 0;
    virtual void requestPatientsWithImplant() = 0;
    virtual void requestAllPatients() = 0;
    virtual void requestRefresh() = 0;

    virtual std::string refreshDate() = 0;
    virtual std::string refreshTime() = 0;
    virtual std::vector<PatientData> patientList() = 0;
};
