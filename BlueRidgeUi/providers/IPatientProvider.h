#pragma once

/**
 * @file IPatientProvider.h
 *
 * \brief This file declares the interface of a Patient Provider class.
 *
 * Description:
 * Provides patient commands and information.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>
#include <vector>

class PatientData;

class IPatientProvider
{
   public:
    virtual ~IPatientProvider();
    virtual unsigned long registerPatientSavedCallback(std::function<void()> func) = 0;
    virtual void unregisterPatientSavedCallback(unsigned long funcId) = 0;
    virtual unsigned long registerListDataCallback(std::function<void()> func) = 0;
    virtual void unregisterListDataCallback(unsigned long funcId) = 0;

    virtual void savePatient(int transactionId, PatientData const& patient) = 0;
    virtual void requestListData(std::string type) = 0;

    virtual int getPatientId() const = 0;
    virtual std::vector<std::string> getListData(std::string type) const = 0;
};
