#pragma once

/**
 * @file MessageDataProvider.h
 *
 * \brief This file declares the Message Data Provider class.
 *
 * Description:
 * Provides a handler for managing response data messages.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/ActiveObject.h"
#include "providers/IMessageDataProvider.h"

#include <map>
#include <memory>
#include <mutex>
#include <vector>

class IMessageParser;

class MessageDataProvider : public IMessageDataProvider
{
    typedef std::vector<Callback> CallbackList;
    typedef std::map<unsigned int, CallbackList> CallbacksMap;

   public:
    MessageDataProvider(IMessageParser& parser, std::vector<unsigned int> const& validCommandIds = {});
    ~MessageDataProvider() override;

    unsigned long registerCallback(unsigned int cmdId, Callback func) override;
    void unregisterCallback(unsigned int cmdId, unsigned long funcId) override;

   protected:
    void receiveMessage(unsigned int cmdId, std::shared_ptr<const std::string> payload);
    void signalUpdate(unsigned int cmdId);

   private:
    MessageDataProvider() = delete;
    MessageDataProvider(MessageDataProvider const&) = delete;
    MessageDataProvider& operator=(MessageDataProvider const&) = delete;

   protected:
    IMessageParser& m_parser;
    unsigned long m_cbId;
    std::vector<unsigned int> m_validCommandIds;
    mutable std::mutex m_cbMutex;
    CallbacksMap m_callbacks;
    ActiveObject m_activeObj;
};
