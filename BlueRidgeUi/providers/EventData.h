#pragma once

/**
 * @file   EventData.h
 *
 * \brief This file declares the Event Data class.
 *
 * Description:
 * Provides a simple data object for storing event data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "cmem_event.h"
#include "providers/CardiacOutputData.h"
#include "providers/PatientData.h"
#include "providers/RecordingData.h"
#include "providers/RhcData.h"
#include "providers/SensorCalibrationData.h"

struct EventData {
    int patientId;
    int eventId;
    std::string eventType;
    std::string date;
    std::string time;
    int paMean;
    std::string clinic;
    std::string clinician;

    SensorCalibrationData *sensorCalibration;
    CardiacOutputData *cardiacOutput;
    RhcData *rhc;
    std::list<RecordingData> recordings;

    EventData();
    EventData(cmem_event &event);
};
