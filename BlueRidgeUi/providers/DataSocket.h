#pragma once

/**
 * @file DataSocket.h
 *
 * \brief This file declares the Data Socket class.
 *
 * Description:
 * Provides read/write access to a data socket.
 * This class leaves thread protected access to users of this object.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/IDataSocket.h"

#include <poll.h>

class DataSocket : public IDataSocket
{
    static const int defaultTimeout;

   public:
    explicit DataSocket();
    explicit DataSocket(const char* serverFile);
    explicit DataSocket(const char* serverFile, int msecTimeout);
    ~DataSocket() override;

    int connection(const char* serverFile, int msecTimeout, int& err, std::string& errstr) override;
    int disconnect() override;
    int reconnect(int& err, std::string& errstr) override;

    ssize_t readbytes(void* buffer, size_t count, int& err, std::string& errstr) override;
    ssize_t writebytes(void const* buffer, size_t count, int& err, std::string& errstr) override;

   private:
    DataSocket(DataSocket&) = delete;
    DataSocket& operator=(DataSocket&) = delete;

    std::string errorString(const char* file, const char* func, int line, int err = 0);

   private:
    std::string m_serverFile;
    int m_msecTimeout;
    struct pollfd m_pollfds[1];
    int m_socket;
};
