#pragma once

/**
 * @file   PatientData.h
 *
 * \brief This file declares the Patient Data class.
 *
 * Description:
 * Provides a simple data object for storing data from a single patient.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <string>

#include <cmem_patient.h>

struct PatientData {
    unsigned int patientId;
    std::string patientKey;
    std::string firstName;
    std::string middleInitial;
    std::string lastName;
    std::string dob;
    std::string implantDate;
    std::string phone;
    std::string clinic;
    std::string clinician;
    std::string implantDoctor;
    std::string serialNumber;
    std::string implantLocation;
    std::string calCode;
    std::string baselineCode;
    std::string patientName;
    bool uploadPending;

    PatientData();
    PatientData(cmem_patient& patient);
    cmem_patient toCMemPatient() const;
};
