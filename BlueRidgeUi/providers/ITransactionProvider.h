#pragma once

/**
 * @file ITransactionProvider.h
 *
 * \brief This file declares the interface of a Transaction Provider class.
 *
 * Description:
 * Provides transaction commands and information.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <functional>

class EventData;
class PatientData;
class ITransactionProvider
{
   public:
    virtual ~ITransactionProvider();
    virtual unsigned long registerTransactionReadyCallback(std::function<void()> func) = 0;
    virtual void unregisterTransactionReadyCallback(unsigned long funcId) = 0;
    virtual unsigned long registerTransactionAbandonedCallback(std::function<void()> func) = 0;
    virtual void unregisterTransactionAbandonedCallback(unsigned long funcId) = 0;
    virtual unsigned long registerCommitTransactionCallback(std::function<void()> func) = 0;
    virtual void unregisterCommitTransactionCallback(unsigned long funcId) = 0;
    virtual unsigned long registerGetTransactionDetailsCallback(std::function<void()> func) = 0;
    virtual void unregisterGetTransactionDetailsCallback(unsigned long funcId) = 0;
    virtual unsigned long registerUploadToMerlinCallback(std::function<void()> func) = 0;
    virtual void unregisterUploadToMerlinCallback(unsigned long funcId) = 0;

    virtual void requestTransactionCreation(int patientId, std::string eventType) = 0;
    virtual void abandonTransaction(int transactionId) = 0;
    virtual void commitTransaction(int transactionId, int patientId) = 0;
    virtual void requestTransactionDetails(int transactionId, int patientId) = 0;
    virtual void uploadToMerlin(int transactionId, int patientId) = 0;

    virtual int getTransactionId() const = 0;
    virtual bool haveValidTransactionId() const = 0;
    virtual EventData getEventData() const = 0;
    virtual PatientData getPatientData() const = 0;
};
