#pragma once

/**
 * @file   CardiacOutputData.h
 *
 * \brief This file declares the Cardiac Output Data class.
 *
 * Description:
 * Provides a simple data object for storing data of a cardiac output.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "cmem_cardiacOutput.h"

struct CardiacOutputData {
    std::string date;
    std::string time;
    float cardiacOutput;
    int heartRate;
    int signalStrength;
    std::string notes;
    std::string waveformFile;

    CardiacOutputData();
    explicit CardiacOutputData(cmem_cardiacOutput& cardiacOutput);
    cmem_cardiacOutput toCmemCardiacOutput();
};
