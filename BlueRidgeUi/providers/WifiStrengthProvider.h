#pragma once

/**
 * @file   WifiStrengthProvider.h
 *
 * \brief This file declares the Wifi Strength Provider class.
 *
 * Description:
 * Provides wifi strength, status and network.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/IWifiStrengthProvider.h"
#include "providers/MessageDataProvider.h"

#include <cmem_wifiStatus.h>

#include <mutex>

class ITimer;

class WifiStrengthProvider : public IWifiStrengthProvider, public MessageDataProvider
{
   public:
    WifiStrengthProvider(ITimer& timer, IMessageParser& parser, unsigned int refreshRateHz = 1);
    ~WifiStrengthProvider() override;

    unsigned long registerWifiStrengthCallback(std::function<void()> func) override;
    void unregisterWifiStrengthCallback(unsigned long funcId) override;
    int getStrength() override;
    std::string getNetworkName() override;
    bool isConnected() override;
    bool isEnabled() override;
    bool isSet() override;

   private:
    WifiStrengthProvider() = delete;
    WifiStrengthProvider(WifiStrengthProvider const&) = delete;
    WifiStrengthProvider& operator=(WifiStrengthProvider const&) = delete;

    void handleMessage(unsigned int cmd, std::string const& payload) override;

   private:
    ITimer& m_timer;
    cmem_wifiStatus m_wifiStatus;
    std::mutex m_dataMutex;
};
