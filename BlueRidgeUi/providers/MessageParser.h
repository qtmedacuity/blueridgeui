#pragma once

/**
 * @file MessageParser.h
 *
 * \brief This file declares the Message Parser class.
 *
 * Description:
 * Provides command and response packet handling to and from the socket.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/ActiveObject.h"
#include "providers/IMessageParser.h"

#include <atomic>
#include <mutex>
#include <vector>

class IDataSocket;

class MessageParser : public IMessageParser
{
    static const unsigned int writeRetries;
    static const unsigned int readRetries;
    static const unsigned int socketConnectTimeout;

    typedef std::vector<Callback> CallbackList;
    typedef std::vector<ErrorCallback> ErrorCallbackList;

   public:
    MessageParser(IDataSocket& dataSock);
    ~MessageParser() override;

    unsigned long registerCallback(Callback func) override;
    void unregisterCallback(unsigned long funcId) override;
    unsigned long registerErrorCallback(ErrorCallback func) override;
    void unregisterErrorCallback(unsigned long funcId) override;
    bool sendCommand(unsigned int cmd) override;
    bool sendCommand(unsigned int cmd, std::string const& payload) override;
    bool sendCommand(unsigned int cmd, Payload payload) override;

   private:
    MessageParser() = delete;
    MessageParser(MessageParser&) = delete;
    MessageParser& operator=(MessageParser&) = delete;

    bool readResponse();
    ssize_t readbytes(void* buffer, size_t count);
    ssize_t writebytes(void const* buffer, size_t count);
    void signalError(DataSocketError err);

   private:
    IDataSocket& m_dataSock;
    unsigned int m_writeTries;
    unsigned int m_readTries;
    std::atomic<bool> m_active;
    std::mutex m_mutex;
    CallbackList m_callbacks;
    ErrorCallbackList m_errorCallbacks;
    ActiveObject m_activeObj;
    ActiveObject m_activeErr;
};
