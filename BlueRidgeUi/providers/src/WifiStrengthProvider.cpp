/**
 * @file   WifiStrengthProvider.cpp
 *
 * \brief This file defines the Wifi Strength Provider class.
 *
 * Description:
 * Provides wifi strength, status and network.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/WifiStrengthProvider.h"
#include "providers/IMessageParser.h"
#include "providers/ITimer.h"

#include <cmem_command.h>

#include <chrono>

WifiStrengthProvider::WifiStrengthProvider(ITimer& timer, IMessageParser& parser, unsigned int refreshRateHz)
    : MessageDataProvider(parser, std::vector<unsigned int>({
                                      eGET_WIFI_STATUS,
                                  })),
      m_timer(timer)

{
    m_timer.start(refreshRateHz, [&] { m_parser.sendCommand(eGET_WIFI_STATUS); });
}

WifiStrengthProvider::~WifiStrengthProvider() { m_timer.stop(); }

unsigned long WifiStrengthProvider::registerWifiStrengthCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eGET_WIFI_STATUS, func);
}

void WifiStrengthProvider::unregisterWifiStrengthCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_WIFI_STATUS, funcId);
}

int WifiStrengthProvider::getStrength()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_wifiStatus.getSignalStrength();
}

std::string WifiStrengthProvider::getNetworkName()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    std::string network;
    m_wifiStatus.getNetworkName(network);
    return network;
}

bool WifiStrengthProvider::isConnected()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_wifiStatus.isWiFiUp();
}

bool WifiStrengthProvider::isEnabled()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_wifiStatus.isEnabled();
}

bool WifiStrengthProvider::isSet()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_wifiStatus.isSet();
}

void WifiStrengthProvider::handleMessage(unsigned int cmd, std::string const& payload)
{
    if (eGET_WIFI_STATUS == cmd) {
        {
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_wifiStatus = cmem_wifiStatus(payload);
        }
        signalUpdate(eGET_WIFI_STATUS);
    }
}
