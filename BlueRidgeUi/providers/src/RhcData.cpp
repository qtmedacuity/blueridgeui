
/**
 * @file   RhcData.cpp
 *
 * \brief This file defines the Rhc Data class.
 *
 * Description:
 * Provides a simple data object for storing rhc data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "RhcData.h"
#include <cmem_rhc.h>

RhcData::RhcData()
    : ra(0),
      rvSystolic(0),
      rvDiastoic(0),
      paSystolic(0),
      paDiastolic(0),
      paMean(0),
      pcwp(0),
      heartRate(0),
      signalStrength(0)
{
}

RhcData::RhcData(cmem_rhc& rhc)
{
    // TODO: Bug QM-187
    //       cmem_rhc contains several unused private member variables with matching public
    //       accessors, but without matching mutators.  The constructor of cmem_rhc leaves
    //       these unsused variables in an undefined state.  Since these variables cannot be
    //       initialized or modified, they are not copied to the RhcData structure.
    //       Unused cmem_rhc members: mRecordingId, mDate, mTime, mAverageHeartRate, nNotes,
    //                                mAverageSignalStrength, mPosition, and mWaveformData.

    ra = rhc.getRA();
    rvSystolic = rhc.getRVSystolic();
    rvDiastoic = rhc.getRVDiastolic();
    paSystolic = rhc.getPASystolic();
    paDiastolic = rhc.getPADiastolic();
    paMean = rhc.getPAMean();
    pcwp = rhc.getPCWP();
}

void RhcData::toCmemRhc(cmem_rhc& rhc) const
{
    cmem_rhc cmemRhc;
    cmemRhc.add_field("PAMean", std::to_string(paMean).c_str());
    cmemRhc.add_field("PADiastolic", std::to_string(paDiastolic).c_str());
    cmemRhc.add_field("PASystolic", std::to_string(paSystolic).c_str());
    cmemRhc.add_field("PCWP", std::to_string(pcwp).c_str());
    cmemRhc.add_field("RVDiastolic", std::to_string(rvDiastoic).c_str());
    cmemRhc.add_field("RVSystolic", std::to_string(rvSystolic).c_str());
    cmemRhc.add_field("RA", std::to_string(ra).c_str());
    rhc = cmemRhc;
}
