/**
 * @file   RecordingData.cpp
 *
 * \brief This file defines the Recording Data class.
 *
 * Description:
 * Provides a simple data object for storing data from a single recording.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "RecordingData.h"
#include <string.h>

RecordingData::RecordingData()
    : recordingId(0),
      uploadPending(false),
      systolic(0),
      diastolic(0),
      mean(0),
      heartRate(0),
      signalStrength(0),
      refSystolic(0),
      refDiastolic(0),
      refMean(0)
{
}

RecordingData::RecordingData(cmem_recording &recording)
{
    recordingId = recording.getRecordingId();
    uploadPending = recording.uploadPending();
    systolic = recording.getSystolic();
    diastolic = recording.getDiastolic();
    mean = recording.getMean();
    heartRate = recording.getAvgHeartRate();
    signalStrength = recording.getAvgSignalStrength();
    refSystolic = recording.getRefSystolic();
    refDiastolic = recording.getRefDiastolic();
    refMean = recording.getRefMean();
    notes = recording.getNotes();
    waveformFile = recording.getWaveform();
    recordingDate = recording.getDate().fmt_date();
    recordingTime = recording.getTime().fmt_time();

    switch (recording.getPosition()) {
        case e30degrees:
            patientPosition = "30_Degrees";
            break;
        case e45degrees:
            patientPosition = "45_Degrees";
            break;
        case e90degrees:
            patientPosition = "90_Degrees";
            break;
        case eHorizontal:
        default:
            patientPosition = "Horizontal";
    }
}
