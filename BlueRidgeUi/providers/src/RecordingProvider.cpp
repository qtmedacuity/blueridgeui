/**
 * @file   RecordingProvider.cpp
 *
 * \brief This file defines the Recording Provider class.
 *
 * Description:
 * Provides controls and information of a recording.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/RecordingProvider.h"
#include "providers/CardiacOutputData.h"
#include "providers/IMessageParser.h"
#include "providers/RhcData.h"

#include <cmem_command.h>
#include <cmem_rhc.h>
#include <json/json.h>

#include <iostream>

#define MAX_SAMPLES 1250

RecordingProvider::RecordingProvider(IMessageParser& parser)
    : MessageDataProvider(parser,
                          std::vector<unsigned int>({eGET_RECORDING_STATUS, eASYNC_READY_TO_RECORD, eTAKE_READING,
                                                     eGET_PA_CATHETER_MEAN, eSET_PA_CATHETER_MEAN, eSAVE_READING,
                                                     eDELETE_READING, eSET_CO_DATA, eSET_RHC_DATA})),
      m_readyToRecord(false),
      m_recordingId(0),
      m_waveFormFileName(""),
      m_paMean(0)
{
}

unsigned long RecordingProvider::registerRecordingStatusCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eGET_RECORDING_STATUS, func);
}

void RecordingProvider::unregisterRecordingStatusCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_RECORDING_STATUS, funcId);
}

unsigned long RecordingProvider::registerReadyToRecordCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eASYNC_READY_TO_RECORD, func);
}

void RecordingProvider::unregisterReadyToRecordCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eASYNC_READY_TO_RECORD, funcId);
}

unsigned long RecordingProvider::registerTakeReadingCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eTAKE_READING, func);
}

void RecordingProvider::unregisterTakeReadingCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eTAKE_READING, funcId);
}

unsigned long RecordingProvider::registerGetPaMeanCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eGET_PA_CATHETER_MEAN, func);
}

void RecordingProvider::unregisterGetPaMeanCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_PA_CATHETER_MEAN, funcId);
}

unsigned long RecordingProvider::registerSetPaMeanCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eSET_PA_CATHETER_MEAN, func);
}

void RecordingProvider::unregisterSetPaMeanCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eSET_PA_CATHETER_MEAN, funcId);
}

unsigned long RecordingProvider::registerSaveRecordingCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eSAVE_READING, func);
}

void RecordingProvider::unregisterSaveRecordingCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eSAVE_READING, funcId);
}

unsigned long RecordingProvider::registerDeleteRecordingCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eDELETE_READING, func);
}

void RecordingProvider::unregisterDeleteRecordingCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eDELETE_READING, funcId);
}

unsigned long RecordingProvider::registerSetCardiacOutputCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eSET_CO_DATA, func);
}

void RecordingProvider::unregisterSetCardiacOutputCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eSET_CO_DATA, funcId);
}

unsigned long RecordingProvider::registerSetRhcValuesCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eSET_RHC_DATA, func);
}

void RecordingProvider::unregisterSetRhcValuesCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eSET_RHC_DATA, funcId);
}

void RecordingProvider::requestRecordingStatus(std::string command)
{
    Json::Value payload;
    payload["Command"] = command;
    m_parser.sendCommand(eGET_RECORDING_STATUS, payload.toStyledString());
}

void RecordingProvider::takeReading(int transactionId)
{
    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    m_parser.sendCommand(eTAKE_READING, payload.toStyledString());
}

void RecordingProvider::requestPaMean() { m_parser.sendCommand(eGET_PA_CATHETER_MEAN); }

void RecordingProvider::setPaMean(int transactionId, int paMean)
{
    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    payload["PAMean"] = std::to_string(paMean);
    m_parser.sendCommand(eSET_PA_CATHETER_MEAN, payload.toStyledString());
}

void RecordingProvider::saveRecording(int transactionId, RecordingData const& recordingData)
{
    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    payload["RecordingId"] = std::to_string(recordingData.recordingId);
    payload["Systolic"] = std::to_string(recordingData.systolic);
    payload["Diastolic"] = std::to_string(recordingData.diastolic);
    payload["Mean"] = std::to_string(recordingData.mean);
    if (recordingData.refSystolic > 0) {
        payload["ReferenceSystolic"] = std::to_string(recordingData.refSystolic);
    }
    if (recordingData.refDiastolic > 0) {
        payload["ReferenceDiastolic"] = std::to_string(recordingData.refDiastolic);
    }
    if (recordingData.refMean > 0) {
        payload["ReferenceMean"] = std::to_string(recordingData.refMean);
    }
    payload["AverageHeartRate"] = std::to_string(recordingData.heartRate);
    payload["AverageSignalStrength"] = std::to_string(recordingData.signalStrength);
    if (recordingData.notes.length() > 0) {
        payload["Notes"] = recordingData.notes;
    }
    payload["PatientPosition"] = recordingData.patientPosition;
    m_parser.sendCommand(eSAVE_READING, payload.toStyledString());
}

void RecordingProvider::deleteRecording(int transactionId, int recordingId)
{
    m_deletedRecordingId = recordingId;
    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    payload["RecordingId"] = std::to_string(recordingId);
    m_parser.sendCommand(eDELETE_READING, payload.toStyledString());
}

void RecordingProvider::setCardiacOutput(int transactionId, CardiacOutputData coData)
{
    std::ostringstream ostr;
    cmem_cardiacOutput cmemCo = coData.toCmemCardiacOutput();
    ostr << "{\n\t\"TransactionId\" : \"" << transactionId << "\",";
    ostr << cmemCo.jsonify();
    ostr << "\n}";
    std::cout << "setCardiacOutput payload: " << ostr.str() << std::endl;
    m_parser.sendCommand(eSET_CO_DATA, ostr.str());
}

void RecordingProvider::setRhcValues(int transactionId, const RhcData& rhcData)
{
    std::ostringstream ostr;
    cmem_rhc cmemRhc;
    rhcData.toCmemRhc(cmemRhc);
    ostr << "{\n\t\"TransactionId\" : \"" << transactionId << "\",";
    cmemRhc.jsonify(ostr);
    ostr << "\n}";
    std::cout << "setRhcValues payload: " << ostr.str() << std::endl;
    m_parser.sendCommand(eSET_RHC_DATA, ostr.str());
}

bool RecordingProvider::isReadyToRecord() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_readyToRecord;
}

int RecordingProvider::getRecordingId() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_recordingId;
}

std::string RecordingProvider::getWaveFormFileName() const { return m_waveFormFileName; }

int RecordingProvider::getPaMean() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_paMean;
}

PaMeanData RecordingProvider::getPaMeanData() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_paMeanData;
}

RecordingData RecordingProvider::getSavedRecording() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_savedRecording;
}

int RecordingProvider::getDeletedRecordingId() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_deletedRecordingId;
}

void RecordingProvider::handleMessage(unsigned int cmd, std::string const& payload)
{
    Json::Value jsonRoot;
    Json::CharReaderBuilder builder;
    std::shared_ptr<Json::CharReader> jsonReader(builder.newCharReader());

    if (1 != jsonReader->parse(payload.c_str(), payload.c_str() + payload.length(), &jsonRoot, nullptr)) return;

    bool sendUpdate = true;
    if (eGET_RECORDING_STATUS == cmd) {
        Json::Value* jsonValue = &jsonRoot["ReadyToRecord"];
        if (!jsonValue->isNull()) {
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_readyToRecord = strncasecmp("True", jsonValue->asCString(), sizeof("True")) == 0;
        } else {
            // Recording status with command stop will not return "ReadyToRecord" so don't
            // update the m_readyToRecord flag or send any update.
            sendUpdate = false;
        }
    } else if (eASYNC_READY_TO_RECORD == cmd) {
        Json::Value* jsonValue = &jsonRoot["ReadyToRecord"];
        std::lock_guard<std::mutex> lock(m_dataMutex);
        m_readyToRecord = strncasecmp("True", jsonValue->asCString(), sizeof("True")) == 0;
    } else if (eTAKE_READING == cmd) {
        Json::Value* jsonRecordingId = &jsonRoot["RecordingId"];
        Json::Value* jsonWaveform = &jsonRoot["WaveformData"];
        std::lock_guard<std::mutex> lock(m_dataMutex);
        m_recordingId = std::atoi(jsonRecordingId->asCString());
        m_waveFormFileName = jsonWaveform->asCString();
    } else if (eGET_PA_CATHETER_MEAN == cmd) {
        Json::Value* jsonValue = &jsonRoot["PAMean"];
        std::lock_guard<std::mutex> lock(m_dataMutex);
        m_paMean = std::atoi(jsonValue->asCString());
    } else if (eSET_PA_CATHETER_MEAN == cmd) {
        std::lock_guard<std::mutex> lock(m_dataMutex);
        m_paMeanData = PaMeanData(&jsonRoot);
    } else if (eSAVE_READING == cmd) {
        std::cout << "Save reading completed" << payload << std::endl;
        std::lock_guard<std::mutex> lock(m_dataMutex);
        cmem_recording recording = cmem_recording(&jsonRoot["Recordings"][0]);
        m_savedRecording = RecordingData(recording);
    } else if (eDELETE_READING == cmd) {
        m_recordingId = 0;
        m_waveFormFileName = "";
    }

    if (sendUpdate) {
        signalUpdate(cmd);
    }
}
