/**
 * @file ActiveObject.cpp
 *
 * \brief This file defines the Active Object class.
 *
 * Description:
 * Provides methods to serialize function calls on a separate thread.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/ActiveObject.h"

ActiveObject::ActiveObject() : m_done(false)
{
    m_thread = std::unique_ptr<std::thread>(new std::thread([=] { this->run(); }));
}

ActiveObject::~ActiveObject()
{
    send([&] { m_done = true; });
    m_thread->join();
}

void ActiveObject::send(Message msg) { m_queue.push(msg); }

void ActiveObject::run()
{
    while (!m_done) {
        Message msg = m_queue.pop();
        msg();
    }
}
