/**
 * @file Timer.cpp
 *
 * \brief This file defines the Timer class.
 *
 * Description:
 * Provides a user defined function call at regular intervals based on a millisecond timer.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/Timer.h"

typedef std::chrono::high_resolution_clock Clock;
typedef Clock::time_point TimePoint;

Timer::Timer() : m_finished(true), m_func(nullptr), m_timeout(0) {}

Timer::~Timer() { stop(); }

void Timer::start(std::chrono::microseconds const& timeout, Callback func)
{
    stop();

    if (nullptr == func || 0 >= timeout.count()) return;

    m_activeObj.send([&, timeout, func]() mutable -> void {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_timeout = timeout;
        m_func = func;
        m_finished = false;
        while (!m_finished) {
            m_condition.wait_for(lock, m_timeout, [&] { return m_finished; });
            if (!m_finished) {
                m_func();
            }
        }
    });
}

void Timer::start(unsigned int timeoutHz, ITimer::Callback func)
{
    std::chrono::microseconds flushRate(static_cast<unsigned int>((1.0 / static_cast<double>(timeoutHz)) * 1000000.0));
    Timer::start(flushRate, func);
}

void Timer::start(std::chrono::microseconds const& timeout) { Timer::start(timeout, m_func); }

void Timer::start(unsigned int timeoutHz) { Timer::start(timeoutHz, m_func); }

void Timer::stop()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_finished = true;
    m_condition.notify_one();
}
