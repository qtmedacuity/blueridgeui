/**
 * @file   SensorDataProvider.cpp
 *
 * \brief This file defines the Sensor Data Provider class.
 *
 * Description:
 * Provides commands and access to streaming sensor data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/SensorDataProvider.h"

#include <limits>

SensorDataProvider::SensorDataProvider(ISensorDataParser& parser)
    : m_parser(parser),
      m_cbId(std::numeric_limits<unsigned long>::max()),
      m_sequenceNumber(std::numeric_limits<unsigned int>::max()),
      m_signalStrength(0),
      m_meanValue(0),
      m_systolicPressure(0),
      m_diastolicPressure(0),
      m_heartRate(0),
      m_pulsatility(false)
{
    m_cbId = m_parser.registerDataCallback(
        [&](ISensorDataParser::DataListPtr sensorReadings) { handleSensorData(sensorReadings); });
}

SensorDataProvider::~SensorDataProvider() { m_parser.unregisterDataCallback(m_cbId); }

unsigned long SensorDataProvider::registerDataRateCallback(UIntCallback func)
{
    return m_parser.registerDataRateCallback(func);
}

void SensorDataProvider::unregisterDataRateCallback(unsigned long funcId)
{
    m_parser.unregisterDataRateCallback(funcId);
}

unsigned long SensorDataProvider::registerSensorDataPointCallback(ULongDoubleCallback func)
{
    return registerCallback(m_sensorDataPointCallbacks, func);
}

void SensorDataProvider::unregisterSensorDataPointCallback(unsigned long funcId)
{
    unregisterCallback(m_sensorDataPointCallbacks, funcId);
}
unsigned long SensorDataProvider::registerSignalStrengthCallback(UIntCallback func)
{
    return registerCallback(m_signalStrengthCallbacks, func);
}
void SensorDataProvider::unregisterSignalStrengthCallback(unsigned long funcId)
{
    unregisterCallback(m_signalStrengthCallbacks, funcId);
}
unsigned long SensorDataProvider::registerMeanValueCallback(UIntCallback func)
{
    return registerCallback(m_meanValueCallbacks, func);
}
void SensorDataProvider::unregisterMeanValueCallback(unsigned long funcId)
{
    unregisterCallback(m_meanValueCallbacks, funcId);
}
unsigned long SensorDataProvider::registerSystolicValuesCallback(UIntUIntCallback func)
{
    return registerCallback(m_systolicValuesCallbacks, func);
}
void SensorDataProvider::unregisterSystolicValuesCallback(unsigned long funcId)
{
    unregisterCallback(m_systolicValuesCallbacks, funcId);
}
unsigned long SensorDataProvider::registerHeartRateCallback(UIntCallback func)
{
    return registerCallback(m_heartRateCallbacks, func);
}
void SensorDataProvider::unregisterHeartRateCallback(unsigned long funcId)
{
    unregisterCallback(m_heartRateCallbacks, funcId);
}

unsigned long SensorDataProvider::registerPulsatilityCallback(ISensorDataProvider::BoolCallback func)
{
    return registerCallback(m_pulsatilityCallbacks, func);
}

void SensorDataProvider::unregisterPulsatilityCallback(unsigned long funcId)
{
    unregisterCallback(m_pulsatilityCallbacks, funcId);
}

void SensorDataProvider::sendSignalStrength()
{
    m_activeObj.send([&]() {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (UIntCallback func : m_signalStrengthCallbacks) {
            if (nullptr != func) {
                func(m_signalStrength);
            }
        }
    });
}

void SensorDataProvider::sendMean()
{
    m_activeObj.send([&]() {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (UIntCallback func : m_meanValueCallbacks) {
            if (nullptr != func) {
                func(m_meanValue);
            }
        }
    });
}

void SensorDataProvider::sendSystolicDiastolic()
{
    m_activeObj.send([&]() {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (UIntUIntCallback func : m_systolicValuesCallbacks) {
            if (nullptr != func) {
                func(m_systolicPressure, m_diastolicPressure);
            }
        }
    });
}

void SensorDataProvider::sendHeartRate()
{
    m_activeObj.send([&]() {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (UIntCallback func : m_heartRateCallbacks) {
            if (nullptr != func) {
                func(m_heartRate);
            }
        }
    });
}

void SensorDataProvider::sendPulsatility()
{
    m_activeObj.send([&]() {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (BoolCallback func : m_pulsatilityCallbacks) {
            if (nullptr != func) {
                func(m_pulsatility);
            }
        }
    });
}

void SensorDataProvider::handleSensorData(ISensorDataParser::DataListPtr sensorReadings)
{
    m_activeObj.send([&, sensorReadings]() {
        for (DataReading_t const& datapoint : *sensorReadings.get()) {
            if (static_cast<unsigned long>(datapoint.sequenceNumber) != m_sequenceNumber) {
                m_sequenceNumber = static_cast<unsigned long>(datapoint.sequenceNumber);
                std::lock_guard<std::mutex> lock(m_mutex);
                for (ULongDoubleCallback func : m_sensorDataPointCallbacks) {
                    if (nullptr != func) {
                        func(m_sequenceNumber, static_cast<double>(datapoint.pressure));
                    }
                }
            }
        }
    });

    m_activeObj.send([&, sensorReadings]() {
        unsigned int signalStrength(static_cast<unsigned int>(sensorReadings.get()->back().signalStrength));
        if (signalStrength != m_signalStrength) {
            m_signalStrength = signalStrength;
            std::lock_guard<std::mutex> lock(m_mutex);
            for (UIntCallback func : m_signalStrengthCallbacks) {
                if (nullptr != func) {
                    func(m_signalStrength);
                }
            }
        }
    });

    m_activeObj.send([&, sensorReadings]() {
        unsigned int meanValue(static_cast<unsigned int>(sensorReadings.get()->back().CM_Mean));
        if (meanValue != m_meanValue) {
            m_meanValue = meanValue;
            std::lock_guard<std::mutex> lock(m_mutex);
            for (UIntCallback func : m_meanValueCallbacks) {
                if (nullptr != func) {
                    func(m_meanValue);
                }
            }
        }
    });

    m_activeObj.send([&, sensorReadings]() {
        unsigned int systolicPressure(static_cast<unsigned int>(sensorReadings.get()->back().CM_Systolic));
        unsigned int diastolicPressure(static_cast<unsigned int>(sensorReadings.get()->back().CM_Diastolic));
        if (systolicPressure != m_systolicPressure || diastolicPressure != m_diastolicPressure) {
            m_systolicPressure = systolicPressure;
            m_diastolicPressure = diastolicPressure;
            std::lock_guard<std::mutex> lock(m_mutex);
            for (UIntUIntCallback func : m_systolicValuesCallbacks) {
                if (nullptr != func) {
                    func(m_systolicPressure, m_diastolicPressure);
                }
            }
        }
    });

    m_activeObj.send([&, sensorReadings]() {
        unsigned int heartRate(static_cast<unsigned int>(sensorReadings.get()->back().heartRate));
        if (heartRate != m_heartRate) {
            m_heartRate = heartRate;
            std::lock_guard<std::mutex> lock(m_mutex);
            for (UIntCallback func : m_heartRateCallbacks) {
                if (nullptr != func) {
                    func(m_heartRate);
                }
            }
        }
    });

    m_activeObj.send([&, sensorReadings]() {
        bool pulsatility(static_cast<bool>(sensorReadings.get()->back().CM_Pulsatility));
        if (pulsatility != m_pulsatility) {
            m_pulsatility = pulsatility;
            std::lock_guard<std::mutex> lock(m_mutex);
            for (BoolCallback func : m_pulsatilityCallbacks) {
                if (nullptr != func) {
                    func(m_pulsatility);
                }
            }
        }
    });
}
