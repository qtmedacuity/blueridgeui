/**
 * @file DataSocket.cpp
 *
 * \brief This file defines the Data Socket class.
 *
 * Description:
 * Provides read access to a data socket.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/DataSocket.h"

#include <errno.h>
#include <poll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdexcept>

const int DataSocket::defaultTimeout(1000);

DataSocket::DataSocket() : m_serverFile(""), m_msecTimeout(defaultTimeout), m_pollfds(), m_socket(-1) {}

DataSocket::DataSocket(char const* serverFile)
    : m_serverFile(serverFile), m_msecTimeout(defaultTimeout), m_pollfds(), m_socket(-1)
{
    int err(0);
    std::string errStr;
    connection(serverFile, m_msecTimeout, err, errStr);
}

DataSocket::DataSocket(char const* serverFile, int msecTimeout)
    : m_serverFile(serverFile), m_msecTimeout(msecTimeout), m_pollfds(), m_socket(-1)
{
    int err(0);
    std::string errStr;
    connection(serverFile, msecTimeout, err, errStr);
}

DataSocket::~DataSocket() { disconnect(); }

int DataSocket::connection(const char* serverFile, int msecTimeout, int& err, std::string& errstr)
{
    if (nullptr == serverFile || 0 > msecTimeout) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = EINVAL);
        return -1;
    }

    m_serverFile = serverFile;
    m_msecTimeout = msecTimeout;

    disconnect();

    m_socket = socket(AF_LOCAL, SOCK_STREAM, 0);
    if (0 > m_socket) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = errno);
        return -1;
    }

    struct sockaddr_un server_addr;
    memset(&server_addr, 0, sizeof(struct sockaddr_un));
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, serverFile);

    if (0 > connect(m_socket, reinterpret_cast<struct sockaddr*>(&server_addr), sizeof(struct sockaddr_un))) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = errno);
        return disconnect();
    }

    return m_socket;
}

int DataSocket::disconnect()
{
    if (0 <= m_socket) {
        close(m_socket);
        m_socket = -1;
    }
    return m_socket;
}

int DataSocket::reconnect(int& err, std::string& errstr)
{
    disconnect();
    return connection(m_serverFile.c_str(), m_msecTimeout, err, errstr);
}

ssize_t DataSocket::readbytes(void* buffer, size_t count, int& err, std::string& errstr)
{
    if (nullptr == buffer || 0 == count) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = EINVAL);
        return -1;
    }

    if (0 > m_socket) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = ENOTCONN);
        return -1;
    }

    m_pollfds[0].fd = m_socket;
    m_pollfds[0].events = POLLIN;

    if (0 > poll(m_pollfds, 1, m_msecTimeout)) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = errno);
        return -1;
    }

    if (((m_pollfds[0].revents & POLLHUP) == POLLHUP) || ((m_pollfds[0].revents & POLLERR) == POLLERR) ||
        ((m_pollfds[0].revents & POLLNVAL) == POLLNVAL)) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = ENOTCONN);
        return -1;
    }

    ssize_t numBytes(0);
    if ((m_pollfds[0].revents & POLLIN) == POLLIN) {
        numBytes = read(m_socket, buffer, count);
        if (0 >= numBytes) {
            errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = errno);
        }
    } else {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = EAGAIN);
    }

    return numBytes;
}

ssize_t DataSocket::writebytes(const void* buffer, size_t count, int& err, std::string& errstr)
{
    if (nullptr == buffer || 0 == count) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = EINVAL);
        return -1;
    }

    if (0 > m_socket) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = ENOTCONN);
        return -1;
    }

    m_pollfds[0].fd = m_socket;
    m_pollfds[0].events = POLLOUT;

    if (0 > poll(m_pollfds, 1, m_msecTimeout)) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = errno);
        return -1;
    }

    if (((m_pollfds[0].revents & POLLHUP) == POLLHUP) || ((m_pollfds[0].revents & POLLERR) == POLLERR) ||
        ((m_pollfds[0].revents & POLLNVAL) == POLLNVAL)) {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = ENOTCONN);
        return -1;
    }

    ssize_t numBytes(0);
    if ((m_pollfds[0].revents & POLLOUT) == POLLOUT) {
        numBytes = write(m_socket, buffer, count);
        if (0 >= numBytes) {
            errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = errno);
        }
    } else {
        errstr = errorString(__FILE__, __FUNCTION__, __LINE__, err = EAGAIN);
    }

    return numBytes;
}

std::string DataSocket::errorString(const char* file, const char* func, int line, int err)
{
    char errbuff[1024] = {};
    if (err) {
        snprintf(errbuff, sizeof(errbuff), "%s:%s:%d  %s:%d", file, func, line, strerror(err), err);
    } else {
        snprintf(errbuff, sizeof(errbuff), "%s:%s:%d", file, func, line);
    }
    return std::string(errbuff);
}
