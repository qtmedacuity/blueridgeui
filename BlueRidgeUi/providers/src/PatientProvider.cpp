/**
 * @file   PatientProvider.cpp
 *
 * \brief This file defines the Patient Provider class.
 *
 * Description:
 * Provides controls and information of a patient.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/PatientProvider.h"
#include "providers/IMessageParser.h"
#include "providers/PatientData.h"

#include <cmem_command.h>
#include <json/json.h>

#include <iostream>
#include <vector>

PatientProvider::PatientProvider(IMessageParser& parser)
    : MessageDataProvider(parser, std::vector<unsigned int>({eSET_PATIENT, eGET_DROPDOWN_LIST_DATA}))
{
}

unsigned long PatientProvider::registerPatientSavedCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eSET_PATIENT, func);
}

void PatientProvider::unregisterPatientSavedCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eSET_PATIENT, funcId);
}

unsigned long PatientProvider::registerListDataCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eGET_DROPDOWN_LIST_DATA, func);
}

void PatientProvider::unregisterListDataCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_DROPDOWN_LIST_DATA, funcId);
}

void PatientProvider::savePatient(int transactionId, PatientData const& patient)
{
    cmem_patient convertedPatient = patient.toCMemPatient();
    std::string convertedString;
    convertedPatient.updatePatientCmd(static_cast<std::uint32_t>(transactionId), convertedString);

    m_parser.sendCommand(eSET_PATIENT, convertedString);
}

int PatientProvider::getPatientId() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_patientId;
}

void PatientProvider::requestListData(std::string type)
{
    Json::Value payload;
    payload["Names"] = type;
    m_parser.sendCommand(eGET_DROPDOWN_LIST_DATA, payload.toStyledString());
}

std::vector<std::string> PatientProvider::getListData(std::string type) const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    std::vector<std::string> dataItems = m_listDataMap.at(type);
    return dataItems;
}

void PatientProvider::handleMessage(unsigned int cmd, std::string const& payload)
{
    Json::Value jsonRoot;
    Json::CharReaderBuilder builder;
    std::shared_ptr<Json::CharReader> jsonReader(builder.newCharReader());

    if (1 != jsonReader->parse(payload.c_str(), payload.c_str() + payload.length(), &jsonRoot, nullptr)) return;

    if (eSET_PATIENT == cmd) {
        Json::Value* jsonValue = &jsonRoot["PatientId"];
        if (!jsonValue->isNull()) {
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_patientId = std::atoi(jsonValue->asCString());
        } else {
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_patientId = 0;
        }
    } else if (eGET_DROPDOWN_LIST_DATA == cmd) {
        Json::Value* jsonValue = &jsonRoot["Type"];
        std::string type = jsonValue->asString();
        Json::Value* jsonList = &jsonRoot["Names"];
        if (!jsonList->isNull() && jsonList->isArray()) {
            ListData dataItems;
            // for each element in this subarray
            for (uint i = 0; i < jsonList->size(); i++) {
                const Json::Value* data = &((*jsonList)[i]["Name"]);
                dataItems.push_back(data->asString());
            }
            // add to map
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_listDataMap.insert(std::pair<std::string, ListData>(type, dataItems));
        }
    }
    signalUpdate(cmd);
}
