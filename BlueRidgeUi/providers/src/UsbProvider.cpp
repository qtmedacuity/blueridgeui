/**
 * @file   UsbProvider.cpp
 *
 * \brief This file defines the Usb Provider class.
 *
 * Description:
 * Provides usb device controls and information.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/UsbProvider.h"
#include "providers/IMessageParser.h"

#include <cmem_command.h>

UsbProvider::UsbProvider(IMessageParser& parser)
    : MessageDataProvider(parser, std::vector<unsigned int>({
                                      eGET_USB_STATUS,
                                  }))
{
}

unsigned long UsbProvider::registerUsbCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eGET_USB_STATUS, func);
}

void UsbProvider::unregisterUsbCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_USB_STATUS, funcId);
}

void UsbProvider::requestUsbStatus() { m_parser.sendCommand(eGET_USB_STATUS); }

bool UsbProvider::isPresent()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_usbStatus.isPreset();
}

bool UsbProvider::isValid()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_usbStatus.isValid();
}

std::string UsbProvider::getSerialNumber()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    std::string serialNumber;
    m_usbStatus.getSerialNumber(serialNumber);
    return serialNumber;
}

std::string UsbProvider::getCalCode()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    std::string calCode;
    m_usbStatus.getCalCode(calCode);
    return calCode;
}

std::string UsbProvider::getBaseline()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    std::string baseline;
    m_usbStatus.getBaseline(baseline);
    return baseline;
}

void UsbProvider::handleMessage(unsigned int cmd, std::string const& payload)
{
    if (eGET_USB_STATUS == cmd) {
        {
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_usbStatus = cmem_usbStatus(payload);
        }
        signalUpdate(eGET_USB_STATUS);
    }
}
