/**
 * @file   CardiacOutputData.cpp
 *
 * \brief This file defines the Sensor Cardiac Output Data class.
 *
 * Description:
 * Provides a simple data object for storing data of a cardiac output.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "CardiacOutputData.h"

CardiacOutputData::CardiacOutputData() : cardiacOutput(0.0), heartRate(0), signalStrength(0) {}

CardiacOutputData::CardiacOutputData(cmem_cardiacOutput& co)
{
    date = std::string(co.getDate().fmt_date());
    time = std::string(co.getTime().fmt_time());
    cardiacOutput = co.getCO();
    heartRate = co.getHeartRate();
    signalStrength = co.avgSignalStrength();
    notes = std::string(co.getNotes());
    waveformFile = std::string(co.waveformPath());
}

cmem_cardiacOutput CardiacOutputData::toCmemCardiacOutput()
{
    cmem_cardiacOutput cmemCo;
    cmemCo.add_field("WaveformData", waveformFile.c_str());
    cmemCo.add_field("CardiacOutput", std::to_string(cardiacOutput).c_str());
    cmemCo.add_field("HeartRate", std::to_string(heartRate).c_str());
    cmemCo.add_field("SignalStrength", std::to_string(signalStrength).c_str());
    cmemCo.add_field("Date", date.c_str());
    cmemCo.add_field("Time", time.c_str());
    return cmemCo;
}
