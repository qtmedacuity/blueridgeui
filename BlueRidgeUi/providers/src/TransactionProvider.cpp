/**
 * @file   TransactionProvider.cpp
 *
 * \brief This file defines the Transaction Provider class.
 *
 * Description:
 * Provides controls and information of a transaction.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/TransactionProvider.h"
#include "providers/IMessageParser.h"
#include "providers/PatientData.h"

#include <cmem_command.h>
#include <json/json.h>

#include <iostream>

TransactionProvider::TransactionProvider(IMessageParser& parser)
    : MessageDataProvider(parser,
                          std::vector<unsigned int>({eCREATE_TRANSACTION_ID, eABANDON_TRANSACTION, eCOMMIT_TRANSACTION,
                                                     eUPLOAD_TO_MERLIN, eGET_TRANSACTION_DETAILS})),
      m_transactionId(0)
{
}

unsigned long TransactionProvider::registerTransactionReadyCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eCREATE_TRANSACTION_ID, func);
}

void TransactionProvider::unregisterTransactionReadyCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eCREATE_TRANSACTION_ID, funcId);
}

unsigned long TransactionProvider::registerTransactionAbandonedCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eABANDON_TRANSACTION, func);
}

void TransactionProvider::unregisterTransactionAbandonedCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eABANDON_TRANSACTION, funcId);
}

unsigned long TransactionProvider::registerCommitTransactionCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eCOMMIT_TRANSACTION, func);
}

void TransactionProvider::unregisterCommitTransactionCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eCOMMIT_TRANSACTION, funcId);
}

unsigned long TransactionProvider::registerGetTransactionDetailsCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eGET_TRANSACTION_DETAILS, func);
}

void TransactionProvider::unregisterGetTransactionDetailsCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_TRANSACTION_DETAILS, funcId);
}

unsigned long TransactionProvider::registerUploadToMerlinCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eUPLOAD_TO_MERLIN, func);
}

void TransactionProvider::unregisterUploadToMerlinCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eUPLOAD_TO_MERLIN, funcId);
}

void TransactionProvider::requestTransactionCreation(int patientId, std::string eventType)
{
    Json::Value payload;
    payload["PatientId"] = std::to_string(patientId);
    payload["EventType"] = eventType;

    m_parser.sendCommand(eCREATE_TRANSACTION_ID, payload.toStyledString());
}

void TransactionProvider::abandonTransaction(int transactionId)
{
    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);

    m_parser.sendCommand(eABANDON_TRANSACTION, payload.toStyledString());
}

void TransactionProvider::commitTransaction(int transactionId, int patientId)
{
    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    payload["PatientId"] = std::to_string(patientId);

    m_parser.sendCommand(eCOMMIT_TRANSACTION, payload.toStyledString());
}

void TransactionProvider::requestTransactionDetails(int transactionId, int patientId)
{
    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    payload["PatientId"] = std::to_string(patientId);

    m_parser.sendCommand(eGET_TRANSACTION_DETAILS, payload.toStyledString());
}

void TransactionProvider::uploadToMerlin(int transactionId, int patientId)
{
    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    payload["PatientId"] = std::to_string(patientId);

    m_parser.sendCommand(eUPLOAD_TO_MERLIN, payload.toStyledString());
}

int TransactionProvider::getTransactionId() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_transactionId;
}

bool TransactionProvider::haveValidTransactionId() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_transactionId != 0;
}

EventData TransactionProvider::getEventData() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_eventData;
}

PatientData TransactionProvider::getPatientData() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_patientData;
}

void TransactionProvider::handleMessage(unsigned int cmd, std::string const& payload)
{
    Json::Value jsonRoot;
    Json::CharReaderBuilder builder;
    std::shared_ptr<Json::CharReader> jsonReader(builder.newCharReader());

    if (1 != jsonReader->parse(payload.c_str(), payload.c_str() + payload.length(), &jsonRoot, nullptr)) return;

    if (eCREATE_TRANSACTION_ID == cmd) {
        Json::Value* jsonValue = &jsonRoot["TransactionId"];
        if (!jsonValue->isNull()) {
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_transactionId = std::atoi(jsonValue->asCString());
        } else {
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_transactionId = 0;
        }
    } else if (eABANDON_TRANSACTION == cmd || eCOMMIT_TRANSACTION == cmd) {
        // for both abandon and commit transaction responses, reset the transaction id.
        std::lock_guard<std::mutex> lock(m_dataMutex);
        m_transactionId = 0;
    } else if (eGET_TRANSACTION_DETAILS == cmd) {
        std::lock_guard<std::mutex> lock(m_dataMutex);
        // message consists of an event object and a patient object
        cmem_event event(&jsonRoot["Event"]);
        m_eventData = EventData(event);
        cmem_patient patient(&jsonRoot["Patient"]);
        m_patientData = PatientData(patient);
    } else if (eUPLOAD_TO_MERLIN == cmd) {
        Json::Value* jsonValue = &jsonRoot["Count"];
        m_uploadCount = std::atoi(jsonValue->asCString());
    }
    signalUpdate(cmd);
}
