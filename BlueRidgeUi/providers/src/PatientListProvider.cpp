/**
 * @file   PatientListProvider.cpp
 *
 * \brief This file defineds the Patient List Provider class.
 *
 * Description:
 * Provides commands and data for patient list information.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/PatientListProvider.h"
#include "providers/IMessageParser.h"

#include <cmem_command.h>
#include <cmem_date.h>
#include <cmem_time.h>
#include <json/json.h>
#include <iostream>

PatientListProvider::PatientListProvider(IMessageParser& parser)
    : MessageDataProvider(parser, std::vector<unsigned int>({eGET_PATIENT_LIST, ePERFORM_MERLIN_DNLD}))
{
}

unsigned long PatientListProvider::registerPatientListCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eGET_PATIENT_LIST, func);
}

void PatientListProvider::unregisterPatientListCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_PATIENT_LIST, funcId);
}

unsigned long PatientListProvider::registerRefreshCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(ePERFORM_MERLIN_DNLD, func);
}

void PatientListProvider::unregisterRefreshCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(ePERFORM_MERLIN_DNLD, funcId);
}

void PatientListProvider::requestPatientsWaitingForImplant() { requestPatientList(eRecordType::eWaitingImplant); }

void PatientListProvider::requestPatientsWithImplant() { requestPatientList(eRecordType::eImplanted); }

void PatientListProvider::requestAllPatients() { requestPatientList(eRecordType::eAll); }

void PatientListProvider::requestRefresh() { m_parser.sendCommand(ePERFORM_MERLIN_DNLD); }

std::string PatientListProvider::refreshDate()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    cmem_date refresh;
    m_patientList.getDate(refresh);
    return std::string(refresh.fmt_date());
}

std::string PatientListProvider::refreshTime()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    cmem_time refresh;
    m_patientList.getTime(refresh);
    return std::string(refresh.fmt_time());
}

std::vector<PatientData> PatientListProvider::patientList()
{
    std::vector<PatientData> patients;
    for (std::uint32_t ii(0); ii < static_cast<std::uint32_t>(m_patientList.size()); ++ii) {
        patients.push_back(PatientData(*m_patientList[ii]));
    }
    return patients;
}

void PatientListProvider::requestPatientList(eRecordType searchType)
{
    Json::Value payload;
    payload["PatientId"] = "0";
    payload["Type"] = searchType;

    m_parser.sendCommand(eGET_PATIENT_LIST, payload.toStyledString());
}

void PatientListProvider::handleMessage(unsigned int cmd, std::string const& payload)
{
    switch (cmd) {
        case eGET_PATIENT_LIST: {
            std::string nonconst_payload(payload);
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_patientList.clear();
            m_patientList.addPatients(nonconst_payload);
            MessageDataProvider::signalUpdate(eGET_PATIENT_LIST);
        } break;
        case ePERFORM_MERLIN_DNLD:
            MessageDataProvider::signalUpdate(ePERFORM_MERLIN_DNLD);
            break;
        default:
            std::cerr << "PatientListProvider cannot handle invalid command type:" << cmd << ", payload:" << payload
                      << std::endl;
            break;
    }
}
