/**
 * @file   PaMeanData.cpp
 *
 * \brief This file defines the Pa Mean Data class.
 *
 * Description:
 * Provides a simple data object for storing data received after saving the PA mean.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "PaMeanData.h"
#include <string.h>

PaMeanData::PaMeanData() : paMean(0), offset(0), systolic(0), diastolic(0), mean(0) {}

void PaMeanData::add_field(const char *key, const std::string &stringValue)
{
    if (key == nullptr || key[0] == '\0') {
        return;
    }

    const char *value = stringValue.c_str();

    if (strcmp(key, "PAMean") == 0) {
        paMean = atoi(value);
    } else if (strcmp(key, "Offset") == 0) {
        offset = atoi(value);
    } else if (strcmp(key, "Systolic") == 0) {
        systolic = atoi(value) & 0x00FF;
    } else if (strcmp(key, "Diastolic") == 0) {
        diastolic = atoi(value) & 0x00FF;
    } else if (strcmp(key, "Mean") == 0) {
        mean = atoi(value) & 0x00FF;
    }
}

PaMeanData::PaMeanData(const Json::Value *values) : paMean(0), offset(0), systolic(0), diastolic(0), mean(0)
{
    // no values or not an object to overwrite defaults
    if (values == nullptr || values->isNull() || !values->isObject()) {
        return;
    }

    // objects have 1 or more keys
    std::vector<std::string> keys;
    keys = values->getMemberNames();

    for (uint j = 0; j < keys.size(); j++) {
        add_field(keys[j].c_str(), std::string((*values)[keys[j]].asCString()));
    }
}
