/**
 * @file   SensorCalibrationData.cpp
 *
 * \brief This file defines the Sensor Calibration Data class.
 *
 * Description:
 * Provides a simple data object for storing data for a sensor calibration.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "SensorCalibrationData.h"

SensorCalibrationData::SensorCalibrationData()
    : systolic(0), diastolic(0), mean(0), reference(0), heartRate(0), signalStrength(0)
{
}

SensorCalibrationData::SensorCalibrationData(cmem_calibration& calibration)
{
    date = std::string(calibration.getDate().fmt_date());
    time = std::string(calibration.getTime().fmt_time());
    systolic = calibration.getSystolic();
    diastolic = calibration.getDiastolic();
    mean = calibration.getMean();
    reference = calibration.getPaMean();
    heartRate = calibration.getAvgHeartRate();
    signalStrength = calibration.getAvgSignalStrength();
    notes = std::string(calibration.getNotes());
    waveformFile = std::string(calibration.getWaveform());
}
