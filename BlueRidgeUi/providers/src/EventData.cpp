/**
 * @file   EventData.cpp
 *
 * \brief This file defines the Event Data class.
 *
 * Description:
 * Provides a simple data object for storing event data.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "EventData.h"

EventData::EventData()
    : patientId(0),
      eventId(0),
      eventType(""),
      date(""),
      time(""),
      paMean(0),
      clinic(""),
      clinician(""),
      sensorCalibration(nullptr),
      cardiacOutput(nullptr),
      rhc(nullptr)
{
}

EventData::EventData(cmem_event &event)
    : patientId(0),
      eventId(0),
      eventType(""),
      date(""),
      time(""),
      paMean(0),
      clinic(""),
      clinician(""),
      sensorCalibration(nullptr),
      cardiacOutput(nullptr),
      rhc(nullptr)
{
    patientId = event.getPatientId();
    eventId = event.getEventId();
    switch(event.getProcedure()) {
    case eImplant:
        eventType = "NewImplant";
        break;
    case eCalibration:
        eventType = "Calibration";
        break;
    case eFollowUp:
        eventType = "FollowUp";
        break;
    }
    date = event.getDate().fmt_date();
    time = event.getTime().fmt_time();
    paMean = event.getPAMean();
    clinic = event.getClinic();
    clinician = event.getClinician();

    if (event.mSensorCalibration != nullptr) {
        sensorCalibration = new SensorCalibrationData(*event.mSensorCalibration);
    }
    if (event.mCardiacOutput != nullptr) {
        cardiacOutput = new CardiacOutputData(*event.mCardiacOutput);
    }
    if (event.mRHCValues != nullptr) {
        rhc = new RhcData(*event.mRHCValues);
    }

    std::list<cmem_recording *>::iterator iter;
    for (iter = event.mRecordingList.begin(); iter != event.mRecordingList.end(); iter++) {
        cmem_recording *recording = *iter;
        RecordingData recordingData = RecordingData(*recording);
        recordings.push_back(recordingData);
    }
}
