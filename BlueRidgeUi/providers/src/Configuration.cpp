/**
 * @file Configuration.h
 *
 * \brief This file defines the Configuration class.
 *
 * Description:
 * Provides a data structure for storing system configuration.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/Configuration.h"

Configuration::Configuration()
    : _language(),
      _paMean(),
      _searchPressure(),
      _systolic(),
      _diastolic(),
      _referenceMean(),
      _hasDate(false),
      _hasTime(false)
{
}

Configuration::Configuration(std::string const& language, ConfigPAMean const& paMean,
                             ConfigSearchPressure const& searchPressure, ConfigSystolic const& systolic,
                             ConfigDiastolic const& diastolic, ConfigReferenceMean const& referenceMean, bool hasDate,
                             bool hasTime)
    : _language(language),
      _paMean(paMean),
      _searchPressure(searchPressure),
      _systolic(systolic),
      _diastolic(diastolic),
      _referenceMean(referenceMean),
      _hasDate(hasDate),
      _hasTime(hasTime)
{
}

Configuration::Configuration(const Configuration& other)
    : _language(other._language),
      _paMean(other._paMean),
      _searchPressure(other._searchPressure),
      _systolic(other._systolic),
      _diastolic(other._diastolic),
      _referenceMean(other._referenceMean),
      _hasDate(other._hasDate),
      _hasTime(other._hasTime)
{
}

Configuration::Configuration(Configuration&& other)
    : Configuration()
{
    swap(*this, other);
}

Configuration& Configuration::operator=(Configuration other)
{
    swap(*this, other);
    return *this;
}

void swap(Configuration& p1, Configuration& p2)
{
    using std::swap;
    swap(p1._language, p2._language);
    swap(p1._paMean, p2._paMean);
    swap(p1._searchPressure, p2._searchPressure);
    swap(p1._systolic, p2._systolic);
    swap(p1._diastolic, p2._diastolic);
    swap(p1._referenceMean, p2._referenceMean);
    swap(p1._hasDate, p2._hasDate);
    swap(p1._hasTime, p2._hasTime);
}
