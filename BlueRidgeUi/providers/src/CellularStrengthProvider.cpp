/**
 * @file   CellularStrengthProvider.cpp
 *
 * \brief This file defines the Cellular Strength Provider class.
 *
 * Description:
 * Provides cellular strength, status and carrier.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/CellularStrengthProvider.h"
#include "providers/IMessageParser.h"
#include "providers/ITimer.h"

#include <cmem_command.h>

#include <chrono>
#include <iostream>

CellularStrengthProvider::CellularStrengthProvider(ITimer& timer, IMessageParser& parser, unsigned int refreshRateHz)
    : MessageDataProvider(parser, std::vector<unsigned int>({
                                      eGET_CELL_STATUS,
                                  })),
      m_timer(timer)
{
    m_timer.start(refreshRateHz, [&] { m_parser.sendCommand(eGET_CELL_STATUS); });
}

CellularStrengthProvider::~CellularStrengthProvider() { m_timer.stop(); }

unsigned long CellularStrengthProvider::registerCellularStrengthCallback(std::function<void()> func)
{
    return MessageDataProvider::registerCallback(eGET_CELL_STATUS, func);
}

void CellularStrengthProvider::unregisterCellularStrengthCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_CELL_STATUS, funcId);
}

int CellularStrengthProvider::getStrength()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_cellStatus.getSignalStrength();
}

std::string CellularStrengthProvider::getCarrier()
{
    std::string carrier;
    {
        std::lock_guard<std::mutex> lock(m_dataMutex);
        m_cellStatus.getCarrier(carrier);
    }
    return carrier;
}

bool CellularStrengthProvider::isConnected()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_cellStatus.isCellularUp();
}

bool CellularStrengthProvider::isEnabled()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_cellStatus.isEnabled();
}

bool CellularStrengthProvider::isSet()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_cellStatus.isSet();
}

void CellularStrengthProvider::handleMessage(unsigned int cmd, std::string const& payload)
{
    if (eGET_CELL_STATUS == cmd) {
        {
            std::lock_guard<std::mutex> lock(m_dataMutex);
            m_cellStatus = cmem_cellStatus(payload);
        }
        signalUpdate(cmd);
    }
}
