/**
 * @file SystemSettingsProvider.cpp
 *
 * \brief This file defines the system settings class.
 *
 * Description:
 * Provides access to commands and data for system settings management.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/SystemSettingsProvider.h"
#include "providers/IMessageParser.h"
#include "providers/ISensorDataParser.h"

#include <cmem_command.h>
#include <cmem_config.h>
#include <iostream>

auto icompare = [](const std::string& strA, const std::string& strB) -> bool {
    return strA.length() == strB.length() &&
           std::equal(std::begin(strA), std::end(strA), std::begin(strB),
                      [](unsigned char a, unsigned char b) -> bool { return std::tolower(a) == std::tolower(b); });
};

SystemSettingsProvider::SystemSettingsProvider(IMessageParser& parser, ISensorDataParser& sensorDataParser)
    : MessageDataProvider(parser, {eGET_RF_STATE, eGET_CONFIGURATION}),
      m_sensorDataParser(sensorDataParser),
      m_rfState(false),
      m_configuration()
{
    m_handlers.insert(std::make_pair(eGET_RF_STATE, [&](const std::string& payload) { handleRFState(payload); }));
    m_handlers.insert(
        std::make_pair(eGET_CONFIGURATION, [&](const std::string& payload) { handleConfiguration(payload); }));
}

unsigned long SystemSettingsProvider::registerRFStateCallback(ISystemSettingsProvider::SettingsChangedCallback func)
{
    return MessageDataProvider::registerCallback(eGET_RF_STATE, func);
}

void SystemSettingsProvider::unregisterRFStateCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_RF_STATE, funcId);
}

unsigned long SystemSettingsProvider::registerSensorDataSocketErrorCallback(DataSocketErrorCallback func)
{
    return m_sensorDataParser.registerErrorCallback(func);
}

void SystemSettingsProvider::unregisterSensorDataSocketErrorCallback(unsigned long funcId)
{
    m_sensorDataParser.unregisterErrorCallback(funcId);
}

unsigned long SystemSettingsProvider::registerConfigurationCallback(
    ISystemSettingsProvider::SettingsChangedCallback func)
{
    return MessageDataProvider::registerCallback(eGET_CONFIGURATION, func);
}

void SystemSettingsProvider::unregisterConfigurationCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_CONFIGURATION, funcId);
}

void SystemSettingsProvider::requestRFState() { m_parser.sendCommand(eGET_RF_STATE); }

bool SystemSettingsProvider::rfState() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_rfState;
}

void SystemSettingsProvider::setRFState(bool state)
{
    Json::Value payload;

    payload["RF_State"] = state ? std::string("ON") : std::string("OFF");

    m_parser.sendCommand(eSET_RF_STATE, payload.toStyledString());
    m_parser.sendCommand(eGET_RF_STATE);
}

void SystemSettingsProvider::requestConfiguration() { m_parser.sendCommand(eGET_CONFIGURATION); }

Configuration const& SystemSettingsProvider::configuration() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_configuration;
}

void SystemSettingsProvider::connectSensorDataSocket() { m_sensorDataParser.restart(); }

void SystemSettingsProvider::disconnectSensorDataSocket() { m_sensorDataParser.stop(); }

void SystemSettingsProvider::enableSensorDataSource() { m_sensorDataParser.reading(true); }

void SystemSettingsProvider::disableSensorDataSource() { m_sensorDataParser.reading(false); }

void SystemSettingsProvider::enableSensorDataStream() { m_sensorDataParser.streaming(true); }

void SystemSettingsProvider::disableSensorDataStream() { m_sensorDataParser.streaming(false); }

void SystemSettingsProvider::handleMessage(unsigned int cmd, std::string const& payload)
{
    std::string data(payload);
    m_handlerActObj.send([&, cmd, data] {
        try {
            auto handler = m_handlers.find(cmd);
            if (handler == m_handlers.end()) throw std::runtime_error(std::string("No handler for command type"));

            handler->second(data);

        } catch (std::exception const& err) {
            std::cerr << "WARNING SystemSettingsProvider [" << err.what() << "]" << std::endl;
            std::cerr << "        Command: " << commandName(cmd) << " (" << cmd << ")" << std::endl;
            std::cerr << data << std::endl << std::endl;
        }
    });
}

void SystemSettingsProvider::handleRFState(std::string const& payload)
{
    Json::Value jsonRoot;
    Json::CharReaderBuilder builder;
    std::shared_ptr<Json::CharReader> jsonReader(builder.newCharReader());

    if (1 != jsonReader->parse(payload.c_str(), payload.c_str() + payload.length(), &jsonRoot, nullptr))
        throw std::runtime_error("JSON reader failed to parse payload data.");

    Json::Value* jsonValue = &jsonRoot["RF_State"];
    if (jsonValue->isNull() || !jsonValue->isString())
        throw std::runtime_error("Payload does not contain an 'RF_State' value.");

    std::string rfstateStr(jsonValue->asCString());
    bool rfstate(icompare(rfstateStr, std::string("ON")));
    if (!rfstate) {
        if (!icompare(rfstateStr, std::string("OFF")))
            throw std::runtime_error(std::string("Failed to convert 'RF_State' value: ") + rfstateStr);
    }

    bool needsUpdate(false);
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (rfstate != m_rfState) {
            m_rfState = rfstate;
            needsUpdate = true;
        }
    }

    if (needsUpdate) {
        signalUpdate(eGET_RF_STATE);
    }
}

void SystemSettingsProvider::handleConfiguration(std::string const& payload)
{
    cmem_config cfg = cmem_config(payload);
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_configuration = Configuration(
            cfg.getLanguage(), ConfigPAMean(cfg.getPaMeanMin(), cfg.getPAMeanMax()),
            ConfigSearchPressure(cfg.getSearchPressureMin(), static_cast<int>(cfg.getSearchPressureMax()),
                                 static_cast<int>(cfg.getSearchPressureStart())),
            ConfigSystolic(cfg.getSystolicMin(), cfg.getSystolicMax()),
            ConfigDiastolic(cfg.getDiastolicMin(), cfg.getDiastolicMax()),
            ConfigReferenceMean(cfg.getReferenceMeanMin(), cfg.getReferenceMeanMax()), cfg.hasDate(), cfg.hasTime());
    }
    signalUpdate(eGET_CONFIGURATION);
}
