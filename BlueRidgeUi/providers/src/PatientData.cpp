/**
 * @file   PatientData.cpp
 *
 * \brief This file defines the Patient Data class.
 *
 * Description:
 * Provides a simple data object for storing data from a single patient.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/PatientData.h"

#include <cardiomems_types.h>

PatientData::PatientData() : patientId(0), uploadPending(false) {}

PatientData::PatientData(cmem_patient& patient)
{
    std::string dummy;
    patientId = patient.getPatientId();
    patientKey = std::string(patient.getPatientKey(dummy));
    dob = std::string(patient.getDOB(dummy));
    implantDate = std::string(patient.getImplantDate(dummy));
    phone = std::string(patient.getPhoneNumber(dummy));
    clinic = std::string(patient.getClinic(dummy));
    clinician = std::string(patient.getClinician(dummy));
    implantDoctor = std::string(patient.getImplantDoctor(dummy));
    serialNumber = std::string(patient.getSerialNumber(dummy));
    uploadPending = patient.getUploadPending();
    lastName = std::string(patient.getLastName(dummy));
    firstName = std::string(patient.getFirstName(dummy));
    middleInitial = std::string(patient.getMiddleInitial(dummy));
    patientName = std::string(patient.getPatientName(dummy));

    switch (patient.getImplantPosition()) {
        case eImplantPositionType::eLeftImplant:
            implantLocation = "left";
            break;
        case eImplantPositionType::eRightImplant:
            implantLocation = "right";
            break;
        default:
            implantLocation = "";
            break;
    }
}

cmem_patient PatientData::toCMemPatient() const
{
    cmem_patient patient;
    patient.setName(firstName.c_str(), middleInitial.c_str(), lastName.c_str());
    patient.setDOB(dob.c_str());
    patient.setImplantDate(implantDate.c_str());
    patient.setUploadPending(uploadPending);
    patient.setClinician(clinician.c_str());
    patient.setClinic(clinic.c_str());
    patient.setImplantDoctor(implantDoctor.c_str());
    patient.setSerialNumber(serialNumber.c_str());
    patient.setPhoneNumber(phone.c_str());
    patient.setPatientKey(patientKey.c_str());
    patient.setPatientId(patientId);
    patient.setCalCode(calCode.c_str());
    patient.setBaselineCode(baselineCode.c_str());

    patient.setImplantPosition(implantLocation == "left"
                                   ? eImplantPositionType::eLeftImplant
                                   : implantLocation == "right" ? eImplantPositionType::eRightImplant
                                                                : eImplantPositionType::eNoImplant);

    return patient;
}
