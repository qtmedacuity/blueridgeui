/**
 * @file MessageDataProvider.cpp
 *
 * \brief This file defines the Message Data Provider class.
 *
 * Description:
 * Provides a handler for managing response data messages.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/MessageDataProvider.h"
#include "providers/IMessageParser.h"

MessageDataProvider::MessageDataProvider(IMessageParser& parser, std::vector<unsigned int> const& validCommandIds)
    : m_parser(parser), m_cbId(static_cast<unsigned long>(-1)), m_validCommandIds(validCommandIds)
{
    m_cbId = m_parser.registerCallback(
        [&](unsigned int cmd, std::shared_ptr<const std::string> payload) { receiveMessage(cmd, payload); });
}

MessageDataProvider::~MessageDataProvider() { m_parser.unregisterCallback(m_cbId); }

unsigned long MessageDataProvider::registerCallback(unsigned int cmd, Callback func)
{
    if (nullptr == func) return static_cast<unsigned long>(-1);

    std::lock_guard<std::mutex> lock(m_cbMutex);
    if (m_callbacks.end() == m_callbacks.find(cmd)) {
        m_callbacks.insert(std::pair<unsigned int, CallbackList>(cmd, CallbackList()));
    }

    CallbackList& callbacks(m_callbacks.at(cmd));
    unsigned long funcId(callbacks.size());
    callbacks.push_back(func);
    return funcId;
}

void MessageDataProvider::unregisterCallback(unsigned int cmd, unsigned long funcId)
{
    std::lock_guard<std::mutex> lock(m_cbMutex);
    if (m_callbacks.end() == m_callbacks.find(cmd)) {
        return;
    }
    CallbackList& callbacks(m_callbacks.at(cmd));
    if (callbacks.size() > funcId) {
        callbacks.at(funcId) = nullptr;
    }
}

void MessageDataProvider::receiveMessage(unsigned int cmd, std::shared_ptr<const std::string> payload)
{
    for (unsigned int cmdId : m_validCommandIds) {
        if (cmdId == cmd) {
            m_activeObj.send([&, cmd, payload] { handleMessage(cmd, *payload.get()); });
            return;
        }
    }
}

void MessageDataProvider::signalUpdate(unsigned int cmd)
{
    std::lock_guard<std::mutex> lock(m_cbMutex);
    if (m_callbacks.end() == m_callbacks.find(cmd)) {
        return;
    }
    CallbackList& callbacks(m_callbacks.at(cmd));
    for (Callback func : callbacks) {
        if (nullptr != func) {
            func();
        }
    }
}
