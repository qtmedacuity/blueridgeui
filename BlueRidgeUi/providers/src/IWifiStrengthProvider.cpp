/**
 * @file   IWifiStrengthProvider.cpp
 *
 * \brief This file defines the interface of a Wifi Strength Provider class.
 *
 * Description:
 * Providing at least one out-of-inline virtual method (in this case, the destructor)
 * prevents copying the vtable in each translation unit, reducing code footprint.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/IWifiStrengthProvider.h"

IWifiStrengthProvider::~IWifiStrengthProvider() {}
