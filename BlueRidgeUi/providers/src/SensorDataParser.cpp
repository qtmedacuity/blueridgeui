/**
 * @file   SensorDataParser.cpp
 *
 * \brief This file defines the Sensor Data Parser class.
 *
 * Description:
 * Parses sensor data from the data socket.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/SensorDataParser.h"
#include "providers/IDataSocket.h"
#include "providers/ITimer.h"

#include <chrono>
#include <cstring>
#include <iostream>
#include <limits>
#include <thread>

SensorDataParser::SensorDataParser(ITimer& timer, IDataSocket& socket, std::chrono::milliseconds socketReadTimeout)
    : m_timer(timer),
      m_socket(socket),
      m_socketReadTimeout(socketReadTimeout),
      m_readRateHz(0),
      m_flushRateHz(0),
      m_active(false),
      m_streaming(false),
      m_rfenabled(false),
      m_readTimeout(Clock::now() + socketReadTimeout),
      m_sensorReadings(new DataList())
{
}

SensorDataParser::~SensorDataParser() { stop(); }

unsigned long SensorDataParser::registerDataCallback(DataCallback func)
{
    if (nullptr == func) return std::numeric_limits<unsigned long>::max();

    std::lock_guard<std::mutex> lock(m_cbMutex);
    unsigned long funcId(m_dataCallbacks.size());
    m_dataCallbacks.push_back(func);
    return funcId;
}

void SensorDataParser::unregisterDataCallback(unsigned long funcId)
{
    std::lock_guard<std::mutex> lock(m_cbMutex);
    if (m_dataCallbacks.size() > funcId) {
        m_dataCallbacks.at(funcId) = nullptr;
    }
}

unsigned long SensorDataParser::registerDataRateCallback(DataRateCallback func)
{
    if (nullptr == func) return std::numeric_limits<unsigned long>::max();

    std::lock_guard<std::mutex> lock(m_cbMutex);
    unsigned long funcId(m_dataRateCallbacks.size());
    m_dataRateCallbacks.push_back(func);
    return funcId;
}

void SensorDataParser::unregisterDataRateCallback(unsigned long funcId)
{
    std::lock_guard<std::mutex> lock(m_cbMutex);
    if (m_dataRateCallbacks.size() > funcId) {
        m_dataRateCallbacks.at(funcId) = nullptr;
    }
}

unsigned long SensorDataParser::registerErrorCallback(DataSocketErrorCallback func)
{
    if (nullptr == func) return std::numeric_limits<unsigned long>::max();

    std::lock_guard<std::mutex> lock(m_cbMutex);
    unsigned long funcId(m_errorCallbacks.size());
    m_errorCallbacks.push_back(func);
    return funcId;
}

void SensorDataParser::unregisterErrorCallback(unsigned long funcId)
{
    std::lock_guard<std::mutex> lock(m_cbMutex);
    if (m_errorCallbacks.size() > funcId) {
        m_errorCallbacks.at(funcId) = nullptr;
    }
}

void SensorDataParser::start(unsigned int readRateHz, unsigned int flushRateHz)
{
    if (0 == readRateHz || 0 == flushRateHz) {
        std::cerr << "WARNING SensorDataParser: Requires non-zero read and flush rates." << std::endl
                  << "readRateHz:" << readRateHz << " flushRateHz:" << flushRateHz << std::endl;
        signalError(DataSocketError::UserFailed);
        return;
    }

    stop();

    m_flushActObj.send([&, readRateHz] {
        if (readRateHz != m_readRateHz) {
            m_readRateHz = readRateHz;
            std::lock_guard<std::mutex> lock(m_cbMutex);
            for (DataRateCallback func : m_dataRateCallbacks) {
                if (nullptr != func) {
                    func(m_readRateHz);
                }
            }
        }
    });

    m_flushActObj.send([&, flushRateHz] {
        m_flushRateHz = flushRateHz;
        if (streamingEnabled()) streaming(streamingEnabled());
    });

    m_readActObj.send([&] {
        setActive(true);
        readBytes();
    });
}

void SensorDataParser::restart() { start(m_readRateHz, m_flushRateHz); }

void SensorDataParser::stop()
{
    m_readActObj.send([&] { setActive(false); });
    m_flushActObj.send([&] { m_timer.stop(); });
}

void SensorDataParser::streaming(bool enabled)
{
    m_flushActObj.send([&, enabled] {
        if (enabled != streamingEnabled()) {
            if (true == enabled && (0 == m_readRateHz || 0 == m_flushRateHz)) {
                signalError(DataSocketError::UserFailed);
                return;
            }
            setStreamingEnabled(enabled);
            if (true == streamingEnabled()) {
                m_flushActObj.send([&] { m_timer.start(m_flushRateHz, [&] { flushData(); }); });
            } else {
                m_flushActObj.send([&] { m_timer.stop(); });
                m_flushActObj.send([&] { extractBuffers(); });
            }
        }
    });
}

void SensorDataParser::reading(bool enabled)
{
    m_readActObj.send([&, enabled] {
        if (enabled != m_rfenabled) {
            m_rfenabled = enabled;
            if (m_rfenabled) {
                m_readTimeout = Clock::now() + m_socketReadTimeout;
            }
        }
    });
}

ISensorDataParser::DataListPtr SensorDataParser::extractBuffers()
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    DataListPtr buffer(m_sensorReadings);
    m_sensorReadings = DataListPtr(new DataList());
    return buffer;
}

void SensorDataParser::setBuffer(DataReading_t const& buffer)
{
    if (streamingEnabled()) {
        std::lock_guard<std::mutex> lock(m_dataMutex);
        m_sensorReadings.get()->push_back(buffer);
    }
}

bool SensorDataParser::active() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_active;
}

void SensorDataParser::setActive(bool enabled)
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    m_active = enabled;
}

bool SensorDataParser::streamingEnabled() const
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    return m_streaming;
}

void SensorDataParser::setStreamingEnabled(bool enabled)
{
    std::lock_guard<std::mutex> lock(m_dataMutex);
    m_streaming = enabled;
}

void SensorDataParser::flushData()
{
    m_flushActObj.send([&] {
        DataListPtr buffer = extractBuffers();
        if (streamingEnabled() && active() && nullptr != buffer && !buffer.get()->empty()) {
            std::unique_lock<std::mutex> lock(m_cbMutex);
            for (DataCallback func : m_dataCallbacks) {
                if (nullptr != func) {
                    func(buffer);
                }
            }
        }
    });
}

void SensorDataParser::readBytes()
{
    int err(0);
    std::string errstr;
    DataReading_t buffer = {};
    ssize_t count = m_socket.readbytes(static_cast<void*>(&buffer), sizeof(DataReading_t), err, errstr);
    if (sizeof(DataReading_t) == count) {
        setBuffer(buffer);
        m_readTimeout = Clock::now() + m_socketReadTimeout;
    } else {
        if (EAGAIN == err) {
            if (true == m_rfenabled && Clock::now() >= m_readTimeout) {
                std::cerr << "WARNING SensorDataParser: Read failed on socket." << std::endl << errstr << std::endl;
                m_readTimeout = Clock::now() + m_socketReadTimeout;
                signalError(DataSocketError::ReadFailed);
            }
        } else if (ENOTCONN == err) {
            std::cerr << std::endl
                      << "WARNING SensorDataParser: Connection failed on socket. Reconnecting..." << std::endl
                      << errstr << std::endl;
            signalError(DataSocketError::ConnectionFailed);
            stop();
            return;
        } else if (0 != err) {
            std::cerr << std::endl
                      << "WARNING SensorDataParser: Unrecoverable socket failure." << std::endl
                      << errstr << std::endl;
            signalError(DataSocketError::Unknown);
            stop();
            return;
        }
    }

    m_readActObj.send([&] {
        if (active()) readBytes();
    });
}

void SensorDataParser::signalError(DataSocketError err)
{
    m_flushActObj.send([&, err] {
        std::unique_lock<std::mutex> lock(m_cbMutex);
        for (DataSocketErrorCallback func : m_errorCallbacks) {
            if (nullptr != func) {
                func(err);
            }
        }
    });
}
