/**
 * @file SearchbarProvider.cpp
 *
 * \brief This file defines the Searchbar Provider class.
 *
 * Description:
 * Provides access to commands and data for the pressure search bar.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/SearchbarProvider.h"
#include "providers/IMessageParser.h"

#include <cmem_command.h>

#include <cctype>
#include <chrono>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <memory>
#include <sstream>

auto clamp = [](unsigned int value) -> unsigned int { return value <= 100 ? value : 100; };
auto icompare = [](const std::string& strA, const std::string& strB) -> bool {
    return strA.length() == strB.length() &&
           std::equal(std::begin(strA), std::end(strA), std::begin(strB),
                      [](unsigned char a, unsigned char b) -> bool { return std::tolower(a) == std::tolower(b); });
};

SearchbarProvider::SearchbarProvider(IMessageParser& parser)
    : MessageDataProvider(parser, {eGET_SEARCH_PRESSURE, eASYNC_SEARCH_PRESSURE}),
      m_searchPresssure(0),
      m_autoSearching(false)
{
    m_handlers.insert(std::make_pair(eGET_SEARCH_PRESSURE, [&](unsigned int cmd, const std::string& payload) {
        handleSearchPressure(cmd, payload);
    }));
    m_handlers.insert(std::make_pair(
        eASYNC_SEARCH_PRESSURE, [&](unsigned int cmd, const std::string& payload) { handleAutoSearch(cmd, payload); }));
}

unsigned long SearchbarProvider::registerSearchPressureCallback(ISearchbarProvider::Callback func)
{
    return MessageDataProvider::registerCallback(eGET_SEARCH_PRESSURE, func);
}

void SearchbarProvider::unregisterSearchPressureCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eGET_SEARCH_PRESSURE, funcId);
}

unsigned long SearchbarProvider::registerAutoSearchingCallback(ISearchbarProvider::Callback func)
{
    return MessageDataProvider::registerCallback(eASYNC_SEARCH_PRESSURE, func);
}

void SearchbarProvider::unregisterAutoSearchingCallback(unsigned long funcId)
{
    MessageDataProvider::unregisterCallback(eASYNC_SEARCH_PRESSURE, funcId);
}

unsigned int SearchbarProvider::searchPressure() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_searchPresssure;
}

void SearchbarProvider::setSearchPressure(unsigned int value)
{
    if (!autoSearching()) {
        value = clamp(value);
        if (value == searchPressure()) {
            return;
        }

        Json::Value payload;
        payload["Pressure"] = std::to_string(value);
        m_parser.sendCommand(eSET_SEARCH_PRESSURE, payload.toStyledString());
        m_parser.sendCommand(eGET_SEARCH_PRESSURE);
    }
}

bool SearchbarProvider::autoSearching() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_autoSearching;
}

void SearchbarProvider::setAutoSearching()
{
    if (!autoSearching()) {
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            m_autoSearching = true;
        }
        signalUpdate(eASYNC_SEARCH_PRESSURE);
        m_parser.sendCommand(ePERFORM_PRESSURE_SEARCH);
    }
}

void SearchbarProvider::handleMessage(unsigned int cmd, const std::string& payload)
{
    std::string data(payload);
    m_handlerActObj.send([&, cmd, data] {
        try {
            auto handler = m_handlers.find(cmd);
            if (handler == m_handlers.end()) throw std::runtime_error(std::string("No handler for command type"));

            handler->second(cmd, data);
        } catch (std::exception const& err) {
            std::cerr << "WARNING: SearchbarProvider:handleMessage - " << err.what() << std::endl;
            std::cerr << "Command: " << commandName(cmd) << " (" << cmd << ")" << std::endl;
            std::cerr << data << std::endl << std::endl;
        }
    });
}

void SearchbarProvider::handleSearchPressure(unsigned int cmd, const std::string& payload)
{
    try {
        Json::Value jsonRoot;
        Json::CharReaderBuilder builder;
        std::shared_ptr<Json::CharReader> jsonReader(builder.newCharReader());

        if (1 != jsonReader->parse(payload.c_str(), payload.c_str() + payload.length(), &jsonRoot, nullptr))
            throw std::runtime_error("JSON reader failed to parse payload data.");

        Json::Value* jsonValue = &jsonRoot["Pressure"];
        if (jsonValue->isNull() || !jsonValue->isString())
            throw std::runtime_error("Payload does not contain a 'Pressure' value.");

        std::stringstream ss(jsonValue->asCString());
        unsigned int pressure(0);
        if (!(ss >> pressure)) throw std::runtime_error("Failed to convert 'Pressure' value.");

        pressure = clamp(pressure);
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            if (m_searchPresssure == pressure) return;

            m_searchPresssure = pressure;
        }
    } catch (std::exception const& err) {
        std::cerr << "WARNING: SearchbarProvider:handleSearchValue - " << err.what() << std::endl;
        std::cerr << "Command: " << commandName(cmd) << " (" << cmd << ")" << std::endl;
        std::cerr << payload << std::endl << std::endl;
    }
}

void SearchbarProvider::handleAutoSearch(unsigned int cmd, const std::string& payload)
{
    try {
        Json::Value jsonRoot;
        Json::CharReaderBuilder builder;
        std::shared_ptr<Json::CharReader> jsonReader(builder.newCharReader());

        if (1 != jsonReader->parse(payload.c_str(), payload.c_str() + payload.length(), &jsonRoot, nullptr))
            throw std::runtime_error("JSON reader failed to parse payload data.");

        Json::Value* jsonValue = &jsonRoot["SearchPressure"];
        if (jsonValue->isNull() || !jsonValue->isString())
            throw std::runtime_error("Payload does not contain a 'SearchPressure' value.");

        std::stringstream valueStr(jsonValue->asCString());
        unsigned int pressure(0);
        if (!(valueStr >> pressure))
            throw std::runtime_error(std::string("Failed to convert 'SearchPressure' value: ") + valueStr.str());

        pressure = clamp(pressure);

        jsonValue = &jsonRoot["SearchComplete"];
        if (jsonValue->isNull() || !jsonValue->isString())
            throw std::runtime_error("Payload does not contain a 'SearchComplete' value.");

        std::string completedStr(jsonValue->asCString());
        bool completed(icompare(completedStr, std::string("true")));
        if (!completed) {
            if (!icompare(completedStr, std::string("false")))
                throw std::runtime_error(std::string("Failed to convert 'SearchComplete' value: ") + completedStr);
        }
        bool searching(!completed);

        m_handlerActObj.send([&, pressure] {
            bool okToSignal(false);
            {
                std::lock_guard<std::mutex> lock(m_mutex);
                okToSignal = (m_searchPresssure != pressure);
                m_searchPresssure = pressure;
            }
            if (okToSignal) {
                signalUpdate(eGET_SEARCH_PRESSURE);
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        });

        m_handlerActObj.send([&, searching] {
            bool okToSignal(false);
            {
                std::lock_guard<std::mutex> lock(m_mutex);
                okToSignal = (m_autoSearching != searching);
                m_autoSearching = searching;
            }
            if (okToSignal) {
                signalUpdate(eASYNC_SEARCH_PRESSURE);
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        });
    } catch (std::exception const& err) {
        std::cerr << "WARNING: SearchbarProvider:handleAutoSearch - " << err.what() << std::endl;
        std::cerr << "Command: " << commandName(cmd) << " (" << cmd << ")" << std::endl;
        std::cerr << payload << std::endl << std::endl;
    }
}
