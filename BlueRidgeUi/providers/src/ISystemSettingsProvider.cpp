/**
 * @file ISystemSettingsProvider.cpp
 *
 * \brief This file defines the interface of a system settings class.
 *
 * Description:
 * Provides access to commands and data for system settings management.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/
#include "providers/ISystemSettingsProvider.h"

ISystemSettingsProvider::~ISystemSettingsProvider() {}
