/**
 * @file MessageParser.cpp
 *
 * \brief This file defines the Message Parser class.
 *
 * Description:
 * Provides command and response packet handling to and from the socket.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <cstring>
#include <iostream>
#include <limits>
#include <sstream>

#include "providers/IDataSocket.h"
#include "providers/MessageParser.h"

#include <cardiomems_types.h>
#include <cmem_command.h>
#include <json/json.h>

const unsigned int MessageParser::writeRetries(5);
const unsigned int MessageParser::readRetries(60);
const unsigned int MessageParser::socketConnectTimeout(2000);

MessageParser::MessageParser(IDataSocket& dataSock)
    : m_dataSock(dataSock), m_writeTries(writeRetries), m_readTries(readRetries), m_active(true)
{
}

MessageParser::~MessageParser() { m_active = false; }

unsigned long MessageParser::registerCallback(Callback func)
{
    if (nullptr == func) return std::numeric_limits<unsigned long>::max();

    std::lock_guard<std::mutex> lock(m_mutex);
    unsigned long funcId(m_callbacks.size());
    m_callbacks.push_back(func);
    return funcId;
}

void MessageParser::unregisterCallback(unsigned long funcId)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    if (m_callbacks.size() > funcId) {
        m_callbacks.at(funcId) = nullptr;
    }
}

unsigned long MessageParser::registerErrorCallback(ErrorCallback func)
{
    if (nullptr == func) return std::numeric_limits<unsigned long>::max();

    std::lock_guard<std::mutex> lock(m_mutex);
    unsigned long funcId(m_errorCallbacks.size());
    m_errorCallbacks.push_back(func);
    return funcId;
}

void MessageParser::unregisterErrorCallback(unsigned long funcId)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    if (m_errorCallbacks.size() > funcId) {
        m_errorCallbacks.at(funcId) = nullptr;
    }
}

bool MessageParser::sendCommand(unsigned int cmd)
{
    if (eUNKNOWN <= cmd || !m_active) return false;

    m_activeObj.send([&, cmd]() {
        cmd_hdr_t hdr = {cmd, 0};
        if (sizeof(hdr) == writebytes(&hdr, sizeof(hdr))) {
            readResponse();
        }
    });
    return true;
}

bool MessageParser::sendCommand(unsigned int cmd, std::string const& payload)
{
    std::shared_ptr<std::string> payloadPtr(new std::string(payload));
    return sendCommand(cmd, payloadPtr);
}

bool MessageParser::sendCommand(unsigned int cmd, Payload payload)
{
    if (eUNKNOWN <= cmd || nullptr == payload || 0 == payload.get()->size() || !m_active) return false;

    m_activeObj.send([&, cmd, payload]() {
        cmd_hdr_t hdr = {cmd, static_cast<std::uint32_t>(payload.get()->size())};
        ssize_t numBytes = writebytes(&hdr, sizeof(hdr));
        if (sizeof(hdr) == numBytes) {
            numBytes = writebytes(payload.get()->c_str(), static_cast<std::uint32_t>(payload.get()->size()));
            if (static_cast<std::uint32_t>(payload.get()->size()) <= numBytes) {
                numBytes = readResponse();
            }
        }
    });
    return true;
}

bool MessageParser::readResponse()
{
    m_activeObj.send([&]() {
        response_t rsp = {};
        ssize_t bytesRead = readbytes(&rsp, sizeof(rsp.header));
        if (sizeof(rsp.header) != bytesRead) {
            std::cerr << "MessageParser: Failed to read response." << std::endl;
            return;
        }

        unsigned int cmd(rsp.header.command);
        Payload payload(nullptr);
        if (0 < rsp.header.length) {
            std::shared_ptr<char> ptr(new char[rsp.header.length + 1], std::default_delete<char[]>());
            std::memset(ptr.get(), 0, sizeof(*ptr.get()));
            bytesRead = readbytes(ptr.get(), rsp.header.length);
            if (rsp.header.length != bytesRead) {
                std::cerr << "MessageParser: Failed to read response payload." << std::endl;
                return;
            }

            payload = std::make_shared<std::string>(std::string(ptr.get()));

            std::string const& json_payload(*payload.get());
            Json::Value jsonRoot;
            Json::CharReaderBuilder builder;
            std::shared_ptr<Json::CharReader> jsonReader(builder.newCharReader());
            std::string errors;
            auto error = jsonReader->parse(json_payload.c_str(), json_payload.c_str() + json_payload.length(),
                                           &jsonRoot, &errors);
            if (error != 1) {
                std::cerr << "MessageParser::readResponse jsonReader parse failed: " << error << ": " << errors.c_str();
                return;
            }

            if (!jsonRoot.isObject()) {
                std::cerr << "MessageParser::readResponse jsonRoot is not an object." << errors.c_str();
                return;
            }

            Json::Value* jResult = &jsonRoot["Status"];
            if (jResult->isNull() || strncmp(jResult->asCString(), "SUCCESS", 7) != 0) {
                std::cerr << "MessageParser::readResponse message has failed status";
                return;
            }
        }

        if (m_active) {
            m_activeObj.send([&, cmd, payload] {
                std::lock_guard<std::mutex> lock(m_mutex);
                for (Callback func : m_callbacks) {
                    if (nullptr != func) {
                        func(cmd, payload);
                    }
                }
            });
        }
    });
    return true;
}

ssize_t MessageParser::readbytes(void* buffer, size_t count)
{
    if (nullptr == buffer || 0 == count) {
        std::cerr << std::endl << "MessageParser: Invalid readbytes parameters." << std::endl;
        return -1;
    }

    int err(0);
    std::string errstr;
    size_t numBytes(0);
    ssize_t results(0);
    m_readTries = readRetries;

    while (m_active && count > numBytes && (0 == err || EAGAIN == err)) {
        results = m_dataSock.readbytes(static_cast<char*>(buffer) + numBytes, count - numBytes, err, errstr);

        if (0 < results) {
            numBytes += static_cast<size_t>(results);
        } else {
            if (EAGAIN == err) {
                if (0 < m_readTries) {
                    m_readTries--;
                } else {
                    std::cerr << "MessageParser: Read failed on socket." << std::endl << errstr << std::endl;
                    signalError(DataSocketError::ReadFailed);
                    break;
                }
            }
            if (ENOTCONN == err) {
                std::cerr << std::endl
                          << "MessageParser: Connection failed on socket. Reconnecting..." << std::endl
                          << errstr << std::endl;
                signalError(DataSocketError::ConnectionFailed);
                m_activeObj.send([&] {
                    m_dataSock.disconnect();
                    std::this_thread::sleep_for(std::chrono::milliseconds(socketConnectTimeout));
                    int er(0);
                    std::string ers;
                    m_dataSock.reconnect(er, ers);
                });
                return -1;
            }
            if (EAGAIN != err && ENOTCONN != err && 0 != err) {
                std::cerr << std::endl
                          << "MessageParser: Unrecoverable socket failure." << std::endl
                          << errstr << std::endl;
                signalError(DataSocketError::Unknown);
                return -1;
            }
        }
    }

    return static_cast<ssize_t>(numBytes);
}

ssize_t MessageParser::writebytes(void const* buffer, size_t count)
{
    if (nullptr == buffer || 0 == count) {
        std::cerr << std::endl << "MessageParser: Invalid writebytes parameters." << std::endl;
        return -1;
    }

    int err(0);
    std::string errstr;
    size_t numBytes(0);
    ssize_t results(0);
    m_writeTries = writeRetries;

    while (count > numBytes && (0 == err || EAGAIN == err)) {
        results = m_dataSock.writebytes(static_cast<char const*>(buffer) + numBytes, count - numBytes, err, errstr);
        if (0 < results) {
            numBytes += static_cast<size_t>(results);
        } else {
            if (EAGAIN == err) {
                if (0 < m_writeTries) {
                    m_writeTries--;
                } else {
                    std::cerr << "MessageParser: Write failed on socket." << std::endl << errstr << std::endl;
                    signalError(DataSocketError::WriteFailed);
                    break;
                }
            }
            if (ENOTCONN == err) {
                std::cerr << std::endl
                          << "MessageParser: Connection failed on socket. Reconnecting..." << std::endl
                          << errstr << std::endl;
                signalError(DataSocketError::ConnectionFailed);
                m_activeObj.send([&] {
                    m_dataSock.disconnect();
                    std::this_thread::sleep_for(std::chrono::milliseconds(socketConnectTimeout));
                    int er(0);
                    std::string ers;
                    m_dataSock.reconnect(er, ers);
                });
                return -1;
            }
            if (EAGAIN != err && ENOTCONN != err && 0 != err) {
                std::cerr << std::endl
                          << "MessageParser: Unrecoverable socket failure." << std::endl
                          << errstr << std::endl;
                signalError(DataSocketError::Unknown);
                return -1;
            }
        }
    }

    return static_cast<ssize_t>(numBytes);
}

void MessageParser::signalError(DataSocketError err)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    for (ErrorCallback func : m_errorCallbacks) {
        if (nullptr != func) {
            func(err);
        }
    }
}
