#pragma once
/**
 * @file   TransactionProvider.h
 *
 * \brief This file declares the Transaction Provider class.
 *
 * Description:
 * Provides controls and information for a transaction.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "providers/EventData.h"
#include "providers/ITransactionProvider.h"
#include "providers/MessageDataProvider.h"

#include <cmem_patient.h>

#include <mutex>

class TransactionProvider : public ITransactionProvider, public MessageDataProvider
{
   public:
    TransactionProvider(IMessageParser& parser);

    unsigned long registerTransactionReadyCallback(std::function<void()> func) override;
    void unregisterTransactionReadyCallback(unsigned long funcId) override;
    unsigned long registerTransactionAbandonedCallback(std::function<void()> func) override;
    void unregisterTransactionAbandonedCallback(unsigned long funcId) override;
    unsigned long registerCommitTransactionCallback(std::function<void()> func) override;
    void unregisterCommitTransactionCallback(unsigned long funcId) override;
    unsigned long registerGetTransactionDetailsCallback(std::function<void()> func) override;
    void unregisterGetTransactionDetailsCallback(unsigned long funcId) override;
    unsigned long registerUploadToMerlinCallback(std::function<void()> func) override;
    void unregisterUploadToMerlinCallback(unsigned long funcId) override;

    void requestTransactionCreation(int patientId, std::string eventType) override;
    void abandonTransaction(int transactionId) override;
    void commitTransaction(int transactionId, int patientId) override;
    void requestTransactionDetails(int transactionId, int patientId) override;
    void uploadToMerlin(int transactionId, int patientId) override;

    int getTransactionId() const override;
    bool haveValidTransactionId() const override;
    EventData getEventData() const override;
    PatientData getPatientData() const override;

   private:
    TransactionProvider() = delete;
    TransactionProvider(TransactionProvider const&) = delete;
    TransactionProvider& operator=(TransactionProvider const&) = delete;

    void handleMessage(unsigned int cmd, std::string const& payload) override;

   private:
    int m_transactionId;
    EventData m_eventData;
    PatientData m_patientData;
    int m_uploadCount;
    mutable std::mutex m_dataMutex;
};
