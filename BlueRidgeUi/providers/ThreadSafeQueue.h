#pragma once

/**
 * @file ThreadSafeQueue.h
 *
 * \brief This file defines the Thread Safe Queue template class.
 *
 * Description:
 * Provides a thread safe queue for an arbitrary data type.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <condition_variable>
#include <mutex>
#include <queue>

template <class T>
class ThreadSafeQueue
{
    std::queue<T> m_queue;
    mutable std::mutex m_mutex;
    std::condition_variable m_condition;

   public:
    void push(T const& item)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_queue.push(item);
        lock.unlock();
        m_condition.notify_one();
    }

    void push(T&& item)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_queue.push(std::move(item));
        lock.unlock();
        m_condition.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        while (m_queue.empty()) {
            m_condition.wait(lock);
        }

        item = m_queue.front();
        m_queue.pop();
    }

    T pop()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        while (m_queue.empty()) {
            m_condition.wait(lock);
        }

        T item = m_queue.front();
        m_queue.pop();
        return item;
    }
};
