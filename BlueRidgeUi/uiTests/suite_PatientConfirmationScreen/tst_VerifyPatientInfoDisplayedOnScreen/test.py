"""
 @file   tst_VerifyPatientInfoDisplayedOnScreen

 Description:

 Verify the patient information displayed on the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
               
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Verify Patient Confirmation screen is visible and the current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.PatientConfirmation_PatientConfirmation").visible, True)        
        test.compare(str(waitForObjectExists(":PatientConfirmation.PatientConfirmationTitle_Text").text), "Confirm Patient Information")
        test.compare(waitForObjectExists(":PatientConfirmation.PatientConfirmationTitle_Text").visible, True)

        # Verify the patient information of the selected patient displayed on the screen
        test.compare(waitForObjectExists(":PatientConfirmation.patientSerialNumber_PatientConfirmationText").visible, True)
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientSerialNumber_PatientConfirmationText").titleText), "SENSOR SERIAL #")
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientSerialNumber_PatientConfirmationText").infoText), "HJPSUZ")

        test.compare(waitForObjectExists(":PatientConfirmation.patientImplantDoctor_PatientConfirmationText").visible, True)
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientImplantDoctor_PatientConfirmationText").titleText), "IMPLANTING DOCTOR")
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientImplantDoctor_PatientConfirmationText").infoText), "Dollie Goodwin, MD")
        
        test.compare(waitForObjectExists(":PatientConfirmation.patientImplantDate_PatientConfirmationText").visible, True)
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientImplantDate_PatientConfirmationText").titleText), "IMPLANT DATE")
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientImplantDate_PatientConfirmationText").infoText), "01 Jan 2018")
        
        test.compare(waitForObjectExists(":PatientConfirmation.patientClinician_PatientConfirmationText").visible, True)
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientClinician_PatientConfirmationText").titleText), "FOLLOW UP CLINICIAN")
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientClinician_PatientConfirmationText").infoText), " Dollie Goodwin, MD ")
        
        test.compare(waitForObjectExists(":PatientConfirmation.patientName_PatientConfirmationText").visible, True)
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientName_PatientConfirmationText").titleText), "NAME")
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientName_PatientConfirmationText").infoText), "Doe, John Q")
        
        test.compare(waitForObjectExists(":PatientConfirmation.patientClinic_PatientConfirmationText").visible, True)      
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientClinic_PatientConfirmationText").titleText), "FOLLOW UP CLINIC")
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientClinic_PatientConfirmationText").infoText), " Fairview MN Heart ")
        
        test.compare(waitForObjectExists(":PatientConfirmation.patientDob_PatientConfirmationText").visible, True)        
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientDob_PatientConfirmationText").titleText), "D.O.B")
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientDob_PatientConfirmationText").infoText), "01 Jan 1970")
        
        test.compare(waitForObjectExists(":PatientConfirmation.patientId_PatientConfirmationText").visible, True)
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientId_PatientConfirmationText").titleText), "MRN/PATIENT ID")
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientId_PatientConfirmationText").infoText), "733141")
        
        test.compare(waitForObjectExists(":PatientConfirmation.patientPhone_PatientConfirmationText").visible, True)        
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientPhone_PatientConfirmationText").titleText), "PHONE")
        test.compare(str(waitForObjectExists(":PatientConfirmation.patientPhone_PatientConfirmationText").infoText), "555-555-5555")        

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
