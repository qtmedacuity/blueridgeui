"""
 @file   tst_TitleBarDisplaysSequenceTitle

 Description:

 Test the correct title is displayed on the Title Bar 

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent"))
        
        # Verify title of Title Bar displays the correct string
        test.compare(waitForObjectExists(":TitleBar.TitleText_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.TitleText_Text").text), "NEW IMPLANT")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
