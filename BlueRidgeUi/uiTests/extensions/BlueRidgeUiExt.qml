import QtQuick 2.12
import com.froglogic.squish.qtquick 0.1

/* Squish QML Extension for BlueRidge UI application. */

SquishHook {
    priority: 120;

    readonly property var containerTypes: [
        "ApplicationWindow",
        "StackView",
        "TitleBar",
        "TempSettingsMenu",
        "SearchBar",
        "PatientInformationBar",
        "Page",
        "StaticWaveOverlay",
        "PaMeanEntryOverlay",
        "CustomNumericKeyboard",
        "RecordingConfirmationPopup",
        "Keyboard",
        "RecordPressureBlankCard",
        "ListView",
        "RecordPressureCard",
        "Review",
        "BaseReviewCard",
        "ReviewDataValues",
        "RhcValuesCard",
        "SystolicDiastolicText",
        "UnderlineTextField",
        "NumericTextField",
        "ParaNumericTextField"
    ];

    readonly property var blacklistedTypes: [
        "Item",
        "RowLayout",
        "ColumnLayout",
        "Row",
        "Column",
        "MouseArea",
        "Rectangle",
        "Text",
        "AnimatedImage",
        "Repeater",
        "KeyboardLayout",
        "KeyboardRow"
    ];

    // Prefer picking of listed type
    readonly property var whitelistedTypes: [
        "ApplicationWindow",
        "TitleBar",
        "CellularStrengthIndicator",
        "WifiStrengthBars",
        "DateTimeIndicator",
        "IconButton",
        "TempSettingsMenu",,
        "Button",
        "SelectableListContent",
        "Page",
        "PressureSearchBar",
        "PressureSearchSlider",
        "BaseWaveChart",
        "SignalStrengthIndicator",
        "StaticWaveOverlay",
        "PaMeanEntryOverlay",
        "CustomNumericKeyboard",
        "TextArea",
        "Keyboard",
        "BaseKey",
        "ListView",
        "BaseReviewCard",
        "ReviewDataValues",
        "RhcValuesCard",
        "UnderlineTextField",
        "NumericTextField",
        "ParaNumericTextField"
    ];

    // Extend object name to contain the objectName property. This functionality is
    // similar to qtwrapper_descriptors.xml
    readonly property var objectNameProperties: {
        "ApplicationWindow" : ["objectName"],
        "TitleBar": ["objectName"],
        "CellularStrengthIndicator": ["objectName"],
        "WifiStrengthBars" : ["objectName"],
        "DateTimeIndicator" : ["objectName"],
        "IconButton" : ["objectName"],
        "TempSettingsMenu" : ["objectName"],
        "SearchBar" : ["objectName"],
        "PatientInformationBar" : ["objectName"],
        "Button" : ["objectName"],
        "SelectableListContent" : ["objectName"],
        "Page" : ["objectName"],
        "BaseWaveChart" : ["objectName"],
        "SignalStrengthIndicator" : ["objectName"],
        "PressureSearchBar" : ["objectName"],
        "PressureSearchSlider" : ["objectName"],
        "StaticWaveOverlay" : ["objectName"],
        "PaMeanEntryOverlay" : ["objectName"],
        "CustomNumericKeyboard" : ["objectName"],
        "TextArea" : ["objectName"],
        "RecordPressureBlankCard" : ["objectName"],
        "RecordPressureCard" : ["objectName"],
        "BaseReviewCard" : ["objectName"],
        "ReviewDataValues" : ["objectName"],
        "RhcValuesCard"  : ["objectName"],
        "UnderlineTextField" : ["objectName"],
        "NumericTextField" : ["objectName"],
        "ParaNumericTextField" : ["objectName"]
    }

    // Ignore direct children in listed type
    function isIgnored( item ) {

        if ( item.parent && qmlType( item.parent ) === "IconButton" ) {
            return true;
        }
        else if ( item.parent && qmlType( item.parent ) === "Button" ) {
            return true;
        }
        else if ( item.parent && qmlType( item.parent ) === "SelectableListContent" ) {
            return true;
        }
        else if ( item.parent && qmlType( item.parent ) === "ListView" ) {
            return true;
        }
        else if ( item.parent && qmlType( item.parent ) === "RecordPressureCard" ) {
            return true;
        }
        else if ( item.parent && qmlType( item.parent ) === "RecordPressureBlankCard" ) {
            return true;
        }
        else if ( item.parent && qmlType( item.parent ) === "BaseReviewCard" ) {
            return true;
        }
        else if ( item.parent && qmlType( item.parent ) === "ReviewDataValues" ) {
            return true;
        }
        else if ( item.parent && qmlType( item.parent ) === "UnderlineField" ) {
            return true;
        }
        return false;
    }
}
