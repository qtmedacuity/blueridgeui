"""
 @file   tst_PCWPGradientErrorMessageFunctionality

 Description:

 Test the functionality of the error message displayed on the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      

        # Navigate to RHC Entry overlay
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.EnterRHC_Button"))
        snooze(3)
        
        # Verify Save button before entering RHC values
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").enabled, False)
        
        # Enter pressure values
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.8_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))   
        
        # Verify PCWP field before entering value
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ParenNumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ParenNumericTextField").invalid, False)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueLabel_Text").color.name), "#ffffff")
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").invalid, False)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_Underline_Rectangle").color.name), "#009cde")
        
        # Enter a value were PAD - PCWP >= 5
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        # Verify entry field and error message after entering value
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ParenNumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ParenNumericTextField").invalid, False)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").invalid, False)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_Underline_Rectangle").color.name), "#004f71")
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueLabel_Text").color.name), "#ffffff")
        test.compare(waitForObjectExists(":RHCEntry.RHCErrorMessage_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RHCErrorMessage_Text").text), "Diastolic pulmonary gradient is > 5 mmHg.")
        
        # Verify Save button after entering value
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").enabled, True)
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
