"""
 @file   tst_CloseButtonFunctionality

 Description:

 Test the functionality of the Close button in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
        snooze(3)
        
        # Verify Enter RHC button before navigating to overlay
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").enabled, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonIconVisible, False)
        
        # Navigate to RHC Entry Overlay
        mouseClick(waitForObject(":Implant_recordPressures.EnterRHC_Button"))
        snooze(3)
        
        # Verify RHC Entry overlay is displayed
        test.compare(waitForObjectExists(":MainWindow.RHCEntry_RhcEntryOverlay").visible, True)
        test.compare(waitForObjectExists(":MainWindow.RHCEntry_RhcEntryOverlay").z, 2)
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").visible, True)
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").z, 0)
        test.verify(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").z < waitForObjectExists(":MainWindow.CoEntryOverlay_CoEntryOverlay").z)

        # Press the Close Button
        mouseClick(waitForObject(":RHCEntry.CloseButton_IconButton"))
        
        # Verify Record Pressures screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Implant_recordPressures")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        test.compare(waitForObjectExists(":MainWindow.RHCEntry_RhcEntryOverlay").visible, False)
        
        # Verify Enter RHC button after closing overlay
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").enabled, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonIconVisible, False)
       
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
