"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed components of the overlay
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
        
        # Navigate to RHC values entry overlay
        mouseClick(waitForObject(":Implant_recordPressures.EnterRHC_Button"))
        snooze(3)

        # Verify overlay components
        test.compare(waitForObjectExists(":MainWindow.RHCEntry_RhcEntryOverlay").visible, True)
        
        test.compare(waitForObjectExists(":RHCEntry.CloseButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.CloseButton_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":RHCEntry.CloseButton_IconButton").iconImage.path), "/images/closeIcon")
        
        test.compare(waitForObjectExists(":RHCEntry.Units_Text").visible, False)
        test.compare(waitForObjectExists(":RHCEntry.ZoomIn_IconButton").visible, False)
        test.compare(waitForObjectExists(":RHCEntry.ZoomOut_IconButton").visible, False)
        test.compare(waitForObjectExists(":RHCEntry.PressureSearchBar_PressureSearchBar").visible, False)
        test.compare(waitForObjectExists(":RHCEntry.StaticWaveChart_StaticWaveChart").visible, False)
        test.compare(waitForObjectExists(":RHCEntry.StaticSignalStrength_SignalStrengthIndicator").visible, False)
        
        test.compare(waitForObjectExists(":RHCEntry.DiscardButton_Button").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.DiscardButton_Button").enabled, True)
        test.compare(str(waitForObjectExists(":RHCEntry.DiscardButton_Button").buttonText), "Discard")
        test.compare(waitForObjectExists(":RHCEntry.DiscardButton_Button").buttonIconVisible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.DiscardButton_Button").buttonIconSource.path), "/images/trashIcon")
        
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.SaveButton_Button").buttonText), "Save")
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").enabled, False)
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").buttonIconVisible, False)

        test.compare(waitForObjectExists(":RHCEntry.RHCEntryLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RHCEntryLabel_Text").text), "Enter RHC Values")
        
        test.compare(waitForObjectExists(":RHCEntry.RALabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RALabel_Text").text), "RA")
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ParenNumericTextField").visible, True)
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_RightParen_Text").text), "(")
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").text), "")
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").state), "infocus")
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").minValue, 1)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").inputMethodHints, 65536)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").enabled, True)
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueLabel_Text").text), "mean")
        
        test.compare(waitForObjectExists(":RHCEntry.RVLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RVLabel_Text").text), "RV")
        
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicDiastolicTextField").visible, True)

        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").minValue, 1)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").inputMethodHints, 65536)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").enabled, True)
        
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_Separator_Text").text), "/")
        
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").minValue, 1)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").inputMethodHints, 65536)
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").enabled, True)
        
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_Label_Text").visible, False)
        
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_SystolicLabel_Text").text), "sys")
        
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicLabel_Text").text), "dia")
        
        test.compare(waitForObjectExists(":RHCEntry.PALabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PALabel_Text").text), "PA")
        
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicDiastolicTextField").visible, True)
        
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").minValue, 1)
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").inputMethodHints, 65536)
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").enabled, True)
        
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_Separator_Text").text), "/")
        
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").minValue, 1)
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").enabled, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").enteredValue), "")
        
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_Label_Text").visible, False)
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_SystolicLabel_Text").text), "sys")
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_DiastolicLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_DiastolicLabel_Text").text), "dia")
        
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ParenNumericTextField").visible, True)
        
        test.compare(waitForObjectExists(":RHCEntry.PAMean_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PAMean_RightParen_Text").text), "(")
        
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").minValue, 1)
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").inputMethodHints, 65536)
        test.compare(str(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").enteredValue), "")
        
        test.compare(waitForObjectExists(":RHCEntry.PAMean_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PAMean_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ValueLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PAMean_ValueLabel_Text").text), "mean")
        
        test.compare(waitForObjectExists(":RHCEntry.PCWPLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPLabel_Text").text), "PCWP")
        
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ParenNumericTextField").visible, True)
        
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_RightParen_Text").text), "(")
        
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").minValue, 1)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").inputMethodHints, 65536)
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").enabled, True)
        
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueLabel_Text").text), "mean")
        
        test.compare(waitForObjectExists(":RHCEntry.RHCErrorMessage_Text").visible, False)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
