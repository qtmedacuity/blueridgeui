"""
 @file   tst_EntryFieldsFunctionality

 Description:

 Test the functionality of the entry fields in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      

        # Navigate to RHC Entry overlay
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.EnterRHC_Button"))
        snooze(3)
        
        # Verify Save button before entering values
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").enabled, False)
        
        # Verify numeric entry field accept only digit inputs
        type(waitForObject(":RHCEntry.RAMean_ValueTextField_NumericTextField"), "abc")
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").enteredValue), "")

        # Verify entry field only accepts two digit values
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").text), "32")
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").enteredValue), "32")
                
        # Verify numeric entry field accept only digit inputs
        type(waitForObject(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField"), "abc")
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").enteredValue), "")

        # Verify entry field only accepts two digit values
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").text), "40")
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_SystolicTextField_NumericTextField").enteredValue), "40")

        # Verify numeric entry field accept only digit inputs
        type(waitForObject(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField"), "abc")
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").enteredValue), "")

        # Verify entry field only accepts two digit values
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").text), "30")
        test.compare(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RHCEntry.RVSysDia_DiastolicTextField_NumericTextField").enteredValue), "30")
        
        # Verify numeric entry field accept only digit inputs
        type(waitForObject(":RHCEntry.PASysDia_SystolicTextField_NumericTextField"), "abc")
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").enteredValue), "")

        # Verify entry field only accepts two digit values
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").text), "35")
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_SystolicTextField_NumericTextField").enteredValue), "35")
        
        # Verify numeric entry field accept only digit inputs
        type(waitForObject(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField"), "abc")
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").enteredValue), "")

        # Verify entry field only accepts two digit values
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").text), "25")
        test.compare(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RHCEntry.PASysDia_DiastolicTextField_NumericTextField").enteredValue), "25")
        
        # Verify numeric entry field accept only digit inputs
        type(waitForObject(":RHCEntry.PAMean_ValueTextField_NumericTextField"), "abc")
        test.compare(str(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").enteredValue), "")

        # Verify entry field only accepts two digit values
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.8_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.9_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        test.compare(str(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").text), "28")
        test.compare(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RHCEntry.PAMean_ValueTextField_NumericTextField").enteredValue), "28")
        
        # Verify numeric entry field accept only digit inputs
        type(waitForObject(":RHCEntry.PCWPMean_ValueTextField_NumericTextField"), "abc")
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").enteredValue), "")

        # Verify entry field only accepts two digit values
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").text), "24")
        test.compare(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RHCEntry.PCWPMean_ValueTextField_NumericTextField").enteredValue), "24")
        
        # Verify Save button after entering values
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.SaveButton_Button").enabled, True)
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
