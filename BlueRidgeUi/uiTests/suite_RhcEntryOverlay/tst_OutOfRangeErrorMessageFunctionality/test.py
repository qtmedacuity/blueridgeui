"""
 @file   tst_OutOfRangeErrorMessageFunctionality

 Description:

 Test the functionality of the error message displayed on the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""
def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)
    
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
    
        # Navigate to RHC Entry overlay
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.EnterRHC_Button"))
        snooze(3)
        
        # Verify entry field and error message before entering value
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ParenNumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ParenNumericTextField").invalid, False)
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").invalid, False)
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_Underline_Rectangle").visible, True)  
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_Underline_Rectangle").color.name), "#009cde")
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueLabel_Text").color.name), "#ffffff")
    
        test.compare(waitForObjectExists(":RHCEntry.RHCErrorMessage_Text").visible, False)
        test.compare(str(waitForObjectExists(":RHCEntry.RHCErrorMessage_Text").text), "")
            
        # Enter a pressure value out of range
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        # Verify entry field and error message
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ParenNumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ParenNumericTextField").invalid, True)
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").invalid, True)
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_Underline_Rectangle").visible, True)  
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_Underline_Rectangle").color.name), "#ffd100")
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueLabel_Text").color.name), "#ffd100")
    
        test.compare(waitForObjectExists(":RHCEntry.RHCErrorMessage_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RHCErrorMessage_Text").text), "Reference pressure is out of range.")
        
        # Enter a pressure value within range
        mouseClick(waitForObject(":RHCEntry.RAMean_ValueTextField_NumericTextField"))
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        # Verify entry field and error message after entering value
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ParenNumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ParenNumericTextField").invalid, False)
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").visible, True)
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_NumericTextField").invalid, False)
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_Underline_Rectangle").visible, True)  
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueTextField_Underline_Rectangle").color.name), "#004f71")
        
        test.compare(waitForObjectExists(":RHCEntry.RAMean_ValueLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCEntry.RAMean_ValueLabel_Text").color.name), "#ffffff")
    
        test.compare(waitForObjectExists(":RHCEntry.RHCErrorMessage_Text").visible, False)
        test.compare(str(waitForObjectExists(":RHCEntry.RHCErrorMessage_Text").text), "")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
