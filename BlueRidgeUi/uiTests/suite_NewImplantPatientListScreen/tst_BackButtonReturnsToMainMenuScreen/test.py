"""
 @file   tst_BackButtonReturnsToMainMenuScreen

 Description:

 Test the Back button functionality of the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Verify New Implant Patient List screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.NewImplantPatientList_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "NewImplantPatientList")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Navigate to Main Menu screen 
        mouseClick(waitForObject(":NewImplantPatientList.Back_Button"))
        
        #Verify Main Menu screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.MainMenu_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "MainMenu")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
