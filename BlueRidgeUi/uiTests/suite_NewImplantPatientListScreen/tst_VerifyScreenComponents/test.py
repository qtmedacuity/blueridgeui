"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Verify the screen components are displayed
        test.compare(waitForObjectExists(":NewImplantPatientList.Back_Button").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.Back_Button").buttonText, "< back")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientListTitle_Text").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientListTitle_Text").text), "Select Patient")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientSearch_SearchBar").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.SearchField_TextField").placeholderText), "search")
        test.compare(str(waitForObjectExists(":NewImplantPatientList.SearchButton_IconButton").iconImage.path), "/images/searchIcon")
        test.compare(waitForObjectExists(":NewImplantPatientList.LastUpdated_Text").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.RefreshList_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.RefreshList_IconButton").iconImage.path), "/images/refreshIcon")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientList_ListView").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.AddNewPatient_Button").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.AddNewPatient_Button").buttonText, "Add New Patient")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
