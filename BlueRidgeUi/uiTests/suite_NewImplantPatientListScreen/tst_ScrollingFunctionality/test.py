"""
 @file   tst_ScrollingFunctionality

 Description:

 Test the scrolling functionality of Patient list displayed on the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile_fullList.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Verify patient list count and the scrollbar is visible
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientList_ListView").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientList_ListView").count, 15)
        test.compare(waitForObjectExists(":NewImplantPatientList.ScrollBar_ScrollBar").visible, True)
         
        # Verify first set of delegates displayed in the list
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow0_NewImplantListRow").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow1_NewImplantListRow").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow2_NewImplantListRow").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow3_NewImplantListRow").visible, True)        
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow4_NewImplantListRow").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow5_NewImplantListRow").visible, True)        
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow6_NewImplantListRow").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow7_NewImplantListRow").visible, True)        
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow8_NewImplantListRow").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow9_NewImplantListRow").visible, True)        
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow10_NewImplantListRow").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow11_NewImplantListRow").visible, True)

        #scroll list to next set of delegates in the list
        mouseDrag(waitForObject(":NewImplantPatientList.ScrollBar_ScrollBar"), 5, 15, -10, 254, Qt.NoModifier, Qt.LeftButton)

        # Verify next set of delegates displayed in the list
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow12_NewImplantListRow").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow13_NewImplantListRow").visible, True)        
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientRow14_NewImplantListRow").visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
