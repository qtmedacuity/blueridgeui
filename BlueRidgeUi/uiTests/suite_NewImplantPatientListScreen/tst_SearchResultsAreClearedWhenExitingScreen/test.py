"""
 @file   tst_SearchResultsAreClearedWhenExitingScreen

 Description:

 Test the search results are cleared when exiting the New Implant Patient List screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        #Verify patient list count and patient name displayed
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientList_ListView").count, 3)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").contentText), "Doe, Jane R")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").contentText), "Doe, John Q")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").contentText), "Smith, David R")

        
        #Search for all patients with Smith in the last name
        mouseClick(waitForObject(":NewImplantPatientList.SearchField_TextField"))
        type(waitForObject(":NewImplantPatientList.SearchField_TextField"), "Smith")
        mouseClick(waitForObject(":NewImplantPatientList.SearchButton_IconButton"))
        
        #Verify patient list after search
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientList_ListView").count, 1)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").contentText), "Smith, David R")
  
        # Navigate to main menu screen
        mouseClick(waitForObject(":NewImplantPatientList.Back_Button"))
        
        #Verify Main Menu screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.MainMenu_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "MainMenu")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        #Verify full patient list is displayed
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientList_ListView").count, 3)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").contentText), "Doe, Jane R")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").contentText), "Doe, John Q")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").contentText), "Smith, David R")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
