"""
 @file   tst_SortingByPatientIdFunctionality

 Description:

 Test the sorting by patient id functionality of the Patient list screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
    
        # Verify each delegate displayed in the list before sorting
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").contentText), "Doe, Jane R")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate0_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate0_ListContent").contentText), "01 Feb 1960")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate0_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate0_ListContent").contentText), "733142")

        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").contentText), "Doe, John Q")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate1_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate1_ListContent").contentText), "01 Jan 1970")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate1_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate1_ListContent").contentText), "733141")
        
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").contentText), "Smith, David R")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate2_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate2_ListContent").contentText), "04 Aug 1933")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate2_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate2_ListContent").contentText), "733143")
    
        # Sort list by patient id in ascending order 
        mouseClick(waitForObject(":NewImplantPatientList.PatientIdColumn_SortableListHeader"))
        
        # Verify list is displayed in ascending order by patient id
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").contentText), "Doe, John Q")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate0_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate0_ListContent").contentText), "01 Jan 1970")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate0_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate0_ListContent").contentText), "733141")
        
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").contentText), "Doe, Jane R")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate1_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate1_ListContent").contentText), "01 Feb 1960")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate1_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate1_ListContent").contentText), "733142")
        
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").contentText), "Smith, David R")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").visible, True)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate2_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientDOB_Delegate2_ListContent").contentText), "04 Aug 1933")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate2_ListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientId_Delegate2_ListContent").contentText), "733143")
    
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
