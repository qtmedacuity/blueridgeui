"""
 @file   tst_RefreshFunctionality

 Description:

 Test the Refresh button functionality of the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

from datetime import datetime

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Format Last updated string to remove seconds
        updatedStrs = (str(waitForObjectExists(":NewImplantPatientList.LastUpdated_Text").text)).rpartition(" ")
        updatedStr =  updatedStrs[0] + updatedStrs[1] + datetime.strptime(updatedStrs[2], "%I:%M:%S").strftime("%I:%M")
        
        # Verify Last Updated string 
        test.compare(updatedStr, "Last updated {0}".format(datetime.now().strftime("%d %b %Y %I:%M")))
        test.compare(waitForObjectExists(":NewImplantPatientList.LastUpdated_Text").visible, True)
        
        # Refresh the patient list
        mouseClick(waitForObject(":NewImplantPatientList.RefreshList_IconButton"))
        
        # Format Last updated string to remove seconds
        updatedStrs = str(waitForObjectExists(":NewImplantPatientList.LastUpdated_Text").text).rpartition(" ")
        updatedStr =  updatedStrs[0] + updatedStrs[1] + datetime.strptime(updatedStrs[2], "%I:%M:%S").strftime("%I:%M")
        
        # Verify Last Updated string is updated after refresh list     
        test.compare(updatedStr, "Last updated {0}".format(datetime.now().strftime("%d %b %Y %I:%M")))
        test.compare(waitForObjectExists(":NewImplantPatientList.LastUpdated_Text").visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
