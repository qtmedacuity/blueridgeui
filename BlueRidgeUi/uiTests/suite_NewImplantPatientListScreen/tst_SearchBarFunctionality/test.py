"""
 @file   tst_SearchBarFunctionality

 Description:

 Test the Search bar functionality of the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Verify patient list count and patient name displayed
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientList_ListView").count, 3)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").contentText), "Doe, Jane R")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent").contentText), "Doe, John Q")
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent").contentText), "Smith, David R")
  
        # Search for all patients with Smith in the last name
        mouseClick(waitForObject(":NewImplantPatientList.SearchField_TextField"))
        type(waitForObject(":NewImplantPatientList.SearchField_TextField"), "Smith")
        mouseClick(waitForObject(":NewImplantPatientList.SearchButton_IconButton"))
        
        # Verify patient count and patient name displayed after search
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientList_ListView").count, 1)
        test.compare(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").visible, True)
        test.compare(str(waitForObjectExists(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent").contentText), "Smith, David R")
  
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
