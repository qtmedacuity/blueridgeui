"""
 @file   tst_SelectPatientFunctionality

 Description:

 Test the select patient functionality on the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Verify New Implant Patient List screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.NewImplantPatientList_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "NewImplantPatientList")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Navigate to Patient confirmation screen 
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent"))

        # Verify Patient Confirmation screen is visible and the current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.PatientConfirmation_PatientConfirmation").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "PatientConfirmation")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
  
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
