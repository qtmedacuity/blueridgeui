"""
 @file   tst_ScreenIsDisplayed

 Description:

 Test the Add New Patient screen is displayed
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Add New Patient Screen 
        mouseClick(waitForObject(":NewImplantPatientList.AddNewPatient_Button"))
        
        #Verify Add New Patient screen is visible and the current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.AddNewPatient_AddNewPatient").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "AddNewPatient")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
  
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
