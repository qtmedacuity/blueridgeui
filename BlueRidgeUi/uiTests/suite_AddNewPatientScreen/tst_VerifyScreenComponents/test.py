"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))

        # Navigate to Add New Patient Screen 
        mouseClick(waitForObject(":NewImplantPatientList.AddNewPatient_Button"))
                
        # Verify the screen components are displayed
        test.compare(waitForObjectExists(":AddNewPatient.Back_Button").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.Back_Button").buttonText), "< back")
        test.compare(waitForObjectExists(":AddNewPatient.SectionTitle_Text").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.SectionTitle_Text").text), "Add New Patient")
        test.compare(waitForObjectExists(":AddNewPatient.RequiredTextMessage_Text").visible, False)
        test.compare(str(waitForObjectExists(":AddNewPatient.RequiredTextMessage_Text").text), "Please complete all required fields (*).")
        test.compare(waitForObjectExists(":AddNewPatient.RequiredTextLineIndicator_Rectangle").visible, False)
        test.compare(waitForObjectExists(":AddNewPatient.SerialNumber_LabeledTextField").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.SerialNumber_LabeledTextField").textFieldLabel), "Sensor Serial Number")
        test.compare(waitForObjectExists(":AddNewPatient.ImplantDoctor_LabeledComboBox").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.ImplantDoctor_LabeledComboBox").comboBoxLabel), "Implanting Doctor")
        test.compare(waitForObjectExists(":AddNewPatient.ImplantDate_DateComboBox").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.ImplantDate_DateComboBox").comboBoxLabel), "Implant Date")
        test.compare(waitForObjectExists(":AddNewPatient.FollowUpClinician_LabeledComboBox").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.FollowUpClinician_LabeledComboBox").comboBoxLabel), "Follow Up Clinician")
        test.compare(waitForObjectExists(":AddNewPatient.FirstName_LabeledTextField").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.FirstName_LabeledTextField").textFieldLabel), "First Name")
        test.compare(waitForObjectExists(":AddNewPatient.MiddleInitial_LabeledTextField").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.MiddleInitial_LabeledTextField").textFieldLabel), "Middle")
        test.compare(waitForObjectExists(":AddNewPatient.LastName_LabeledTextField").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.LastName_LabeledTextField").textFieldLabel), "Last Name")
        test.compare(waitForObjectExists(":AddNewPatient.FollowUpClinic_LabeledComboBox").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.FollowUpClinic_LabeledComboBox").comboBoxLabel), "Follow Up Clinic")
        test.compare(waitForObjectExists(":AddNewPatient.DOB_DateComboBox").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.DOB_DateComboBox").comboBoxLabel), "DOB")
        test.compare(waitForObjectExists(":AddNewPatient.Phone_LabeledTextField").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.Phone_LabeledTextField").textFieldLabel), "Phone")
        test.compare(waitForObjectExists(":AddNewPatient.PatientId_LabeledTextField").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.PatientId_LabeledTextField").textFieldLabel), "MRN/Patient ID")
        test.compare(waitForObjectExists(":AddNewPatient.Next_Button").visible, True)
        test.compare(str(waitForObjectExists(":AddNewPatient.Next_Button").buttonText), "Next")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
