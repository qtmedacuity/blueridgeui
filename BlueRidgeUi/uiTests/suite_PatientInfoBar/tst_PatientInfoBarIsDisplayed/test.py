"""
 @file   tst_PatientInfoBarIsDisplayed

 Description:

 Test the patient info bar displayed on the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Store the patient info of selected patient
        patientName = str(waitForObjectExists(":PatientConfirmation.patientName_PatientConfirmationText").infoText)
        dob = str(waitForObjectExists(":PatientConfirmation.patientDob_PatientConfirmationText").infoText)
        serialNum = str(waitForObjectExists(":PatientConfirmation.patientSerialNumber_PatientConfirmationText").infoText)
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Verify patient info bar is displayed
        test.compare(waitForObjectExists(":MainWindow.PatientInfoBar_PatientInformationBar").visible, True)
        
        # Verify patient info is correctly displayed
        test.compare(waitForObjectExists(":MainWindow.Name_Text").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.Name_Text").text), patientName)
        
        test.compare(waitForObjectExists(":MainWindow.DobLabel_Text").visible, True)        
        test.compare(str(waitForObjectExists(":MainWindow.DobLabel_Text").text), "D.O.B:")
        test.compare(waitForObjectExists(":MainWindow.Dob_Text").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.Dob_Text").text), dob)
        
        test.compare(waitForObjectExists(":MainWindow.SnLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.SnLabel_Text").text), "SN:")
        test.compare(waitForObjectExists(":MainWindow.SerialNumber_Text").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.SerialNumber_Text").text), serialNum)
        
        test.compare(waitForObjectExists(":MainWindow.XButton_IconButton").visible, True)
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
