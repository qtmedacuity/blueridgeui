"""
 @file   tst_ExitPopupFunctionality

 Description:

 Test the Exit popup functionality of the Patient Info bar

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Press close button on Patient Info bar to display Exit popup
        mouseClick(waitForObjectExists(":MainWindow.XButton_IconButton"))
        
        # Verify exit popup is displayed        
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        
        # Press No button on popup
        mouseClick(waitForObject(":NoButton_Button"))
        
        # Verify same screen is still displayed and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.SensorCheck_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "SensorCheck")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Press close button on Patient Info bar to display Exit popup
        mouseClick(waitForObjectExists(":MainWindow.XButton_IconButton"))
        
        # Verify exit popup is displayed        
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        
        # Press Yes button on popup
        mouseClick(waitForObject(":YesButton_Button"))
        
        # Verify Main Menu screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.MainMenu_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "MainMenu")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
