"""
 @file   tst_CloseButtonFunctionality

 Description:

 Test the Close Button functionality of the Patient Info bar

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Verify sensor check screen and patient info bar are displayed 
        test.compare(waitForObjectExists(":MainWindow.SensorCheck_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "SensorCheck")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        test.compare(waitForObjectExists(":MainWindow.PatientInfoBar_PatientInformationBar").visible, True)
        
        # Press close button on Patient Info bar
        mouseClick(waitForObjectExists(":MainWindow.XButton_IconButton"))
        
        # Verify the Exit Popup displayed        
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        test.compare(waitForObjectExists(":WarningImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":WarningImage_Image").source.path), "/images/redwarning")
        test.compare(str(waitForObjectExists(":PopupTitle_Title_Text").text), "Are you sure you want to exit?")
        test.compare(waitForObjectExists(":PopupTitle_Title_Text").visible, True)
        test.compare(str(waitForObjectExists(":Instruction_Text").text), "The existing readings will not be saved.")
        test.compare(waitForObjectExists(":Instruction_Text").visible, True)
        test.compare(waitForObjectExists(":NoButton_Button").visible, True)
        test.compare(waitForObjectExists(":NoButton_Button").buttonText, "No, return to page")
        test.compare(waitForObjectExists(":YesButton_Button").visible, True)
        test.compare(waitForObjectExists(":YesButton_Button").buttonText, "YES, discard reading and exit")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
