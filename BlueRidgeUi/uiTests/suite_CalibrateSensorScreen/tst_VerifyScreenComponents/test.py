"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Verify the components displayed on the screen
        test.compare(waitForObjectExists(":Implant_calibrateSensor.Back_Button").visible, False)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.InstructionTitle_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.InstructionTitle_Text").text), "Calibrate Sensor")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.InstructionText_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.InstructionText_Text").text), "Freeze CardioMems and PA catheter waveforms when both are valid.")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").buttonText), "Freeze to Calibrate")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.CancelButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.CancelButton_Button").buttonText), "Cancel")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.Units_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.Units_Text").text), "mmHg")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.ZoomIn_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.ZoomIn_IconButton").iconImage.path), "/images/plusIcon")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.ZoomOut_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.ZoomOut_IconButton").iconImage.path), "/images/minusIcon")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.ActiveWaveChart_BaseWaveChart").visible, True)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.PressureSearchBar_PressureSearchBar").visible, True)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.Flicker_PressureSearchBar_Flickable").interactive, False)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.WaveExamplesButton_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.WaveExamplesButton_Button").buttonText, "Waveform Examples")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_SignalStrengthIndicator").visible, True)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.SensorSelectionImage_Image").visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
