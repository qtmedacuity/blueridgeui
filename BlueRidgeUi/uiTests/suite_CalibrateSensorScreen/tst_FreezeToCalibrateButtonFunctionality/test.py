"""
 @file   tst_FreezeToCalibrateButton

 Description:

 Test the functionality of the Freeze to Calibrate button in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.RightButton_Image"))
        
        # Navigate to Calibrate Sensor screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Verify Freeze to Calibrate button is disable when screen is displayed
        test.compare(waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled, False)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").visible, True)
        
        # Wait for button to be enabled
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        
        # Verify Freeze to Calibrate button is enabled after wait
        test.compare(waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled, True)
        
        # Verify Calibrate Sensor screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.Implant_calibrateSensor_ImplantRoot").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Implant_calibrateSensor")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Navigate to PA Catheter Mean screen
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))

        # Verify PA Mean overlay is visible
        test.compare(waitForObjectExists(":MainWindow.PaMeanOverlay_PaMeanEntryOverlay").visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
