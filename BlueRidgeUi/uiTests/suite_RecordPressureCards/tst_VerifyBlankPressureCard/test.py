"""
 @file   tst_VerifyBlankPressureCard
 
 Description:

 Test the displayed blank card and its components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))

        # Verify each blank card displayed by default        
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard1_RecordPressureBlankCard").visible, True)
        test.compare(waitForObjectExists(":BlankCard1.fakeBox1_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard1.fakeBox2_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard1.fakeBox3_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard1.fakeBox4_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard1.fakeBox5_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard1.fakeBox6_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard1.fakeBox7_Rectangle").visible, True)
        
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard2_RecordPressureBlankCard").visible, True)
        test.compare(waitForObjectExists(":BlankCard2.fakeBox1_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard2.fakeBox2_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard2.fakeBox3_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard2.fakeBox4_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard2.fakeBox5_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard2.fakeBox6_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard2.fakeBox7_Rectangle").visible, True)
        
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard3_RecordPressureBlankCard").visible, True)
        test.compare(waitForObjectExists(":BlankCard3.fakeBox1_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard3.fakeBox2_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard3.fakeBox3_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard3.fakeBox4_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard3.fakeBox5_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard3.fakeBox6_Rectangle").visible, True)
        test.compare(waitForObjectExists(":BlankCard3.fakeBox7_Rectangle").visible, True)


        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
