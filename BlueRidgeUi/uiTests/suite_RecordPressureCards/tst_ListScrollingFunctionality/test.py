"""
 @file   tst_ListScrollingFunctionality
 
 Description:

 Test the scrolling functionality of the card list
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button")) 
        snooze(5)
        
        # Record seven  Pressure cards
        for x in range(7):
            mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
            snooze(5)
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            snooze(5)
            mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
            snooze(5)
        
        # Verify list before scrolling
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 7)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").contentY, 0)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").contentX, 1041)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_3_RecordPressureDelegate").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_4_RecordPressureDelegate").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_5_RecordPressureDelegate").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_6_RecordPressureDelegate").visible, True)

        # Scroll to the beginning of the list
        mouseDrag(waitForObject(":Implant_recordPressures.RecordedPressuresList_ListView"), 747, 174, -541, 9, Qt.NoModifier, Qt.LeftButton)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 7)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").contentY, 0)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").contentX, 0)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_3_RecordPressureDelegate").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_6_RecordPressureDelegate_2").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_2_RecordPressureDelegate").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_1_RecordPressureDelegate").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_0_RecordPressureDelegate").visible, True)
        
        # Scroll to the middle of the list
        mouseDrag(waitForObject(":Implant_recordPressures.RecordedPressuresList_ListView"), 26, 175, 321, -13, Qt.NoModifier, Qt.LeftButton)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 7)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").contentY, 0)
        test.compare(int(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").contentX), 786)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_3_RecordPressureDelegate").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_6_RecordPressureDelegate").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_4_RecordPressureDelegate").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressureCard_5_RecordPressureDelegate").visible, True)
           
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
