"""
 @file   tst_AddCardToListFunctionality
 
 Description:

 Test the adding card to card list in the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button")) 
        
        # Verify card list before recording pressure
        test.compare(str(waitForObjectExists(":Implant_recordPressures.RecordedPressuresLabel_Text").text), "RECORDED PRESSURES:")
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 0)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard1_RecordPressureBlankCard").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard2_RecordPressureBlankCard").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard3_RecordPressureBlankCard").visible, True)
        
        # Record Pressure card
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)

        # Verify card list after recording
        test.compare(str(waitForObjectExists(":Implant_recordPressures.RecordedPressuresLabel_Text").text), "RECORDED PRESSURES (1):")
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 1)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard1_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard2_RecordPressureBlankCard").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard3_RecordPressureBlankCard").visible, True)
        
        #Record another card
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Verify card list after recording
        test.compare(str(waitForObjectExists(":Implant_recordPressures.RecordedPressuresLabel_Text").text), "RECORDED PRESSURES (2):")
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 2)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_1_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard1_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard2_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard3_RecordPressureBlankCard").visible, True)
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
