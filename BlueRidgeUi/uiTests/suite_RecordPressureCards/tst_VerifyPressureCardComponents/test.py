"""
 @file   tst_VerifyPressureCardComponents
 
 Description:

 Test the displayed card components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""
from datetime import datetime

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button")) 
        
        # Press Record Pressures button on screen
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        
        # Record Pressure
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        mouseClick(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"))
        mouseClick(waitForObject(":keyboard.n_BaseKey"))
        mouseClick(waitForObject(":keyboard.o_BaseKey"))
        mouseClick(waitForObject(":keyboard.t_BaseKey"))
        mouseClick(waitForObject(":keyboard.e_BaseKey"))
        mouseClick(waitForObject(":keyboard.\n_EnterKey"))
        
        mouseClick(waitForObject(":RecordingConfirmation.30PositionButton_RadioButton"))
        
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)

        # Verify pressure card components
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard").visible, True)
        
        test.compare(waitForObjectExists(":RecordedPressure_0.RecordedTime_Text").visible, True)
        timeValue = datetime.strptime(str(waitForObjectExists(":RecordedPressure_0.RecordedTime_Text").text), "%I:%M:%S")        
        test.compare(timeValue.strftime("%I:%M"),  datetime.now().strftime("%I:%M")) 
        
        test.compare(waitForObjectExists(":RecordedPressure_0.HeartRateLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.HeartRateLabel_Text").text), "HR")
        test.compare(waitForObjectExists(":RecordedPressure_0.HeartRate_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.HeartRate_Text").text), "78")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorLabel_Text").text), "SENSOR")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Systolic_Text").text), "38")
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Separator_Text").text), "/")
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Diastolic_Text").text), "19")
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Label_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorMean_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorMean_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorMean_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorMean_Mean_Text").text), "26")
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorMean_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorMean_LeftParen_Text").text), ")")
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorMean_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorMean_Label_Text").text), "Mean")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.ReferenceLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.ReferenceLabel_Text").text), "REF")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").text), "40")
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Separator_Text").text), "/")
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").text), "30")
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Label_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.ReferenceMean_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.ReferenceMean_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").text), "35")
        test.compare(waitForObjectExists(":RecordedPressure_0.ReferenceMean_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.ReferenceMean_LeftParen_Text").text), ")")
        test.compare(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Label_Text").text), "Mean")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.SignalStrength_SignalStrengthIcon").visible, True)
        test.compare(waitForObjectExists(":RecordedPressure_0.SignalStrength_SignalStrengthIcon").sensorStrengthVal, 98)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SignalStrength_SignalStrengthIcon").state), "good")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.30_DegreesPositionIndicator_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.30_DegreesPositionIndicator_Image").position), "30_Degrees")
        test.compare(str(waitForObjectExists(":RecordedPressure_0.30_DegreesPositionIndicator_Image").source.path), "/images/30Degree")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.NoteIndicator_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.NoteIndicator_Image").source.path), "/images/cardNoteIcon")
        
        test.compare(waitForObjectExists(":Implant_recordPressures.DeleteButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.DeleteButton_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.DeleteButton_IconButton").iconImage.path), "/images/trashIcon")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
