"""
 @file   tst_EditCardFunctionality
 
 Description:

 Test the editing functionality of a card
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button")) 
        
        # Press Record Pressures button on screen
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        
        # Record Pressure card
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        mouseClick(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"))
        mouseClick(waitForObject(":keyboard.n_BaseKey"))
        mouseClick(waitForObject(":keyboard.o_BaseKey"))
        mouseClick(waitForObject(":keyboard.t_BaseKey"))
        mouseClick(waitForObject(":keyboard.e_BaseKey"))
        mouseClick(waitForObject(":keyboard.\n_EnterKey"))
        
        mouseClick(waitForObject(":RecordingConfirmation.90PositionButton_RadioButton"))
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Verify reference values displayed on the card
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").text), "40")
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").text), "30")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").text), "35")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.90_DegreesPositionIndicator_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.90_DegreesPositionIndicator_Image").position), "90_Degrees")
        test.compare(str(waitForObjectExists(":RecordedPressure_0.90_DegreesPositionIndicator_Image").source.path), "/images/90Degree")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.NoteIndicator_Image").visible, True)
        
        # Edit reference values on card
        mouseClick(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard"))
        
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Verify reference values displayed on the card
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").text), "35")
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").text), "30")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").text), "32")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.90_DegreesPositionIndicator_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.90_DegreesPositionIndicator_Image").position), "90_Degrees")
        test.compare(str(waitForObjectExists(":RecordedPressure_0.90_DegreesPositionIndicator_Image").source.path), "/images/90Degree")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.NoteIndicator_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.NoteIndicator_Image").source.path), "/images/cardNoteIcon")

        # Delete the note and change the position
        mouseClick(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard"))
        mouseClick(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"))
        mouseClick(waitForObject(":keyboard_BaseKey_5"))
        mouseClick(waitForObject(":keyboard_BaseKey_5"))
        mouseClick(waitForObject(":keyboard_BaseKey_5"))
        mouseClick(waitForObject(":keyboard_BaseKey_5"))
        mouseClick(waitForObject(":keyboard_BaseKey_4"))
        mouseClick(waitForObject(":RecordingConfirmation.45PositionButton_RadioButton"))
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Verify reference values displayed on the card
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").text), "35")
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").text), "30")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").text), "32")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.45_DegreesPositionIndicator_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.45_DegreesPositionIndicator_Image").position), "45_Degrees")
        test.compare(str(waitForObjectExists(":RecordedPressure_0.45_DegreesPositionIndicator_Image").source.path), "/images/45Degree")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.NoteIndicator_Image").visible, False)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.NoteIndicator_Image").source.path), "/images/cardNoteIcon")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
