"""
 @file   tst_DeleteButtonFunctionality
 
 Description:

 Test the delete button functionality of a card
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button")) 
        
        # Record three  Pressure card
        for x in range(3):
            mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
            snooze(5)
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
            snooze(3)
        
        # Verify card list after recording
        test.compare(str(waitForObjectExists(":Implant_recordPressures.RecordedPressuresLabel_Text").text), "RECORDED PRESSURES (3):")
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 3)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard1_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard2_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard3_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_1_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_2_RecordPressureCard").visible, True)
        
        # Press Delete button on the second card
        mouseClick(waitForObject(":RecordedPressure_1.DeleteButton_IconButton"))
        
        # Verify Delete Recording popup
        test.compare(waitForObjectExists(":PopupTitle_Title_Text").visible, True)
        test.compare(str(waitForObjectExists(":PopupTitle_Title_Text").text), "Are you sure you want to delete?")
        test.compare(waitForObjectExists(":Instruction_Text").visible, True)
        test.compare(str(waitForObjectExists(":Instruction_Text").text), "This will delete the following recorded pressure:")
        test.compare(waitForObjectExists(":PressureCard_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":DeleteButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":DeleteButton_Button").buttonText), "Delete Recording")
        test.compare(waitForObjectExists(":DeleteButton_Button").buttonIconVisible, True)
        test.compare(str(waitForObjectExists(":DeleteButton_Button").buttonIconSource.path), "/images/whiteTrashIcon")
        test.compare(waitForObjectExists(":DeleteButton_Button").enabled, True)
        
        # Delete recording
        mouseClick(waitForObject(":DeleteButton_Button"))
        snooze(1)
        
        # Verify card list after deleting card
        test.compare(str(waitForObjectExists(":Implant_recordPressures.RecordedPressuresLabel_Text").text), "RECORDED PRESSURES (2):")
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 2)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_1_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard1_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard2_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard3_RecordPressureBlankCard").visible, True)
        
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
