"""
 @file   tst_TitleBarIsDisplayed

 Description:

 Test the Title Bar is displayed on the main menu screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        startApplication("BlueRidgeUI")
        
        #Verify current screen displayed is MainMenu
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "MainMenu")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        #Verify Title Bar is displayed
        test.compare(waitForObjectExists(":MainWindow.TitleBar_TitleBar").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.TitleBar_TitleBar").titleText), "MAIN MENU")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))

    
