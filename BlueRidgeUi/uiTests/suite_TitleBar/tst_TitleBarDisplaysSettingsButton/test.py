"""
 @file   tst_TitleBarSettingsButton

 Description:

 Test the Settings Button in the Title Bar

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try:
        source(findFile("scripts", "TestProcessesScript.py"))
        ts = TestProcesses()
        ts.runPressurePumpProc()
        
        startApplication("BlueRidgeUI")
        
        # Verify Setting Button is visible and enabled
        test.compare(waitForObjectExists(":TitleBar.Settings_IconButton").visible, True)
        test.compare(waitForObjectExists(":TitleBar.Settings_IconButton").enabled, True)
        
        # Open Settings menu and verify it is displayed
        # TODO: Update the following objects once Menu is implemented
        mouseClick(waitForObject(":TitleBar.Settings_IconButton"))
        snooze(5)
        test.compare(waitForObjectExists(":TempSettingsMenu_Popup").visible, True)

        test.compare(str(waitForObjectExists(":PopupTitle_Title_Text").text), "TEMP SETTINGS MENU")
        
        # Close Menu and verify it is closed
        mouseClick(waitForObject(":TempSettingsMenu_Settings_IconButton"))
        snooze(5)
        
        # Verify Settings menu closes after pressing button
        test.compare(waitForObjectExists(":MainWindow_Overlay").visible, False)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))    
