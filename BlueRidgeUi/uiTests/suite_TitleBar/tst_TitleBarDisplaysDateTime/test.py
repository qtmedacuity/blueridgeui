"""
 @file   tst_TitleBarDisplaysDateTime

 Description:

 Test Date/Time displayed on Title Bar

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

from datetime import datetime

def main():
    try:
        source(findFile("scripts", "TestProcessesScript.py"))
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        snooze(2)
        
        # Verify title bar is visible and Date/Time are displayed on it 
        test.compare(waitForObjectExists(":MainWindow.TitleBar_TitleBar").visible, True)
        test.compare(waitForObjectExists(":TitleBar.DateTimeIndicator_DateTimeIndicator").visible, True)
        
        # Verify current date is displayed in DD MM YYYY format
        test.compare(waitForObjectExists(":TitleBar.DateLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.DateLabel_Text").text), datetime.now().strftime("%d %b %Y"))
        
        # Verify current time is displayed in hh:mm format
        test.compare(waitForObjectExists(":TitleBar.TimeLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.TimeLabel_Text").text), datetime.now().strftime("%H:%M"))     

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))