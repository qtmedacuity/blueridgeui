"""
 @file   tst_TitleBarDisplaysCellularSignalIndicator

 Description:

 Test the Cellular Signal Indicator in the Title Bar

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try:
        source(findFile("scripts", "TestProcessesScript.py"))
        ts = TestProcesses()
        ts.runPressurePumpProc()
        
        # Test disabled Cell indicator    
        args = "--cell-enabled disabled"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
         
        test.compare(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").visible, True)
        test.compare(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").text), "Disabled")
        test.compare(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").source.path), "/images/cellular/cellularFault")
     
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
     
        #Test Cell enabled with zero bars (0 strength)     
        args = "-c 0 --carrier Carrier"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
         
        test.compare(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").visible, True) 
        test.compare(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").text), "Carrier")
        test.compare(str(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").state), "zeroBars")
        test.compare(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").source.path), "/images/cellular/cellularBars0")
     
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
      
        # Test Cell enabled with one bar icon (5 strength) and carrier label
        args = "-c 5 --carrier Carrier"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
          
        test.compare(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").visible, True)
        test.compare(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").text), "Carrier")
        test.compare(str(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").state), "oneBar")
        test.compare(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").source.path), "/images/cellular/cellularBars1")
          
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
      
        # Test Cell enabled with two bars icon (25 strength)  and carrier label
        args = "--cell-enabled enabled -c 25 --carrier Carrier"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
         
        test.compare(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").visible, True)
        test.compare(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").text), "Carrier")
        test.compare(str(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").state), "twoBars")
        test.compare(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").source.path), "/images/cellular/cellularBars2")
          
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
          
        # Test Cell enabled with three bars icon (50 strength)  and carrier label
        args = "--cell-enabled enabled -c 50 --carrier Carrier" 
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
          
        test.compare(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").visible, True)
        test.compare(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").text), "Carrier")
        test.compare(str(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").state), "threeBars")
        test.compare(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").source.path), "/images/cellular/cellularBars3")
          
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
          
        # Test Cell enabled with four bars icon (75 strength)  and carrier label
        args = "--cell-enabled enabled -c 75 --carrier Carrier"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
          
        test.compare(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").visible, True)
        test.compare(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").text), "Carrier")
        test.compare(str(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").state), "fourBars")
        test.compare(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").source.path), "/images/cellular/cellularBars4")
          
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()

        # Test Cell disconnected
        args = "--cell-connected false"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
         
        test.compare(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").visible, True)
        test.compare(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").visible, True)

        test.compare(str(waitForObjectExists(":TitleBar.CellularCarrierLabel_Text").text), "Not Initialized")
        test.compare(str(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").state), "fault")
        test.compare(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.CellularStrengthImage_Image").source.path), "/images/cellular/cellularFault")
         
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
