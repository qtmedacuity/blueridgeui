"""
 @file   tst_TitleBarDisplaysWifiSignalIndicator

 Description:

 Test the Wifi Signal Indicator in the Title Bar

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try:
        source(findFile("scripts", "TestProcessesScript.py"))
        ts = TestProcesses()
        ts.runPressurePumpProc()
        
        # Test disabled wifi indicator    
        args = "--wifi-enabled false"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
        test.compare(waitForObjectExists(":TitleBar.WifiIndicator_Item").visible, True)
        test.compare(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").source.path), "/images/wifi/wifiDisabled")
    
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
    
        # Test wifi enabled with zero bars (0 strength)
        
        args = "--wifi-enabled enabled -w 0"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
        test.compare(waitForObjectExists(":TitleBar.WifiIndicator_Item").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.WifiIndicator_Item").state), "zeroBars")
        test.compare(str(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").source.path), "/images/wifi/wifiBars0")
        test.compare(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").visible, True)
    
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
    
        # Test wifi enabled with one bar icon (10 strength)
        args = "--wifi-enabled enabled -w 10"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(2)
        test.compare(waitForObjectExists(":TitleBar.WifiIndicator_Item").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.WifiIndicator_Item").state), "oneBar")
        test.compare(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").source.path), "/images/wifi/wifiBars1")
        
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
    
        # Test wifi enabled with two bars icon (40 strength)
        args = "--wifi-enabled enabled -w 40"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(3)
        test.compare(waitForObjectExists(":TitleBar.WifiIndicator_Item").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.WifiIndicator_Item").state), "twoBars")
        test.compare(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").source.path), "/images/wifi/wifiBars2")
        
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
        
        # Test wifi enabled with three bars icon (60 strength)
        args = "--wifi-enabled enabled -w 60"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(3)
        test.compare(waitForObjectExists(":TitleBar.WifiIndicator_Item").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.WifiIndicator_Item").state), "twoBars")
        test.compare(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").source.path), "/images/wifi/wifiBars2")
        
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
        
        # Test wifi enabled with four bars icon (80 strength)
        args = "--wifi-enabled enabled -w 80"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(3)
        test.compare(waitForObjectExists(":TitleBar.WifiIndicator_Item").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.WifiIndicator_Item").state), "threeBars")
        test.compare(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").source.path), "/images/wifi/wifiBars3")
        
        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()
        
        # Test wifi disconnected
        args = "--wifi-connected false"
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        snooze(3)
        test.compare(waitForObjectExists(":TitleBar.WifiIndicator_Item").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.WifiIndicator_Item").state), "fault")
        test.compare(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.WifiStrengthImage_Image").source.path), "/images/wifi/wifiFault")

        ctx = currentApplicationContext()
        ctx.detach()
        ts.stopTestServerProc()

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))    
