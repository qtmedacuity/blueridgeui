"""
 @file   tst_TitleBarDisplaysTitle

 Description:

 Test the Title in the Title Bar

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try:
        source(findFile("scripts", "TestProcessesScript.py"))
        ts = TestProcesses()
        ts.runPressurePumpProc()
        
        startApplication("BlueRidgeUI")
        
        #Verify TitleBar is visible
        test.compare(waitForObjectExists(":MainWindow.TitleBar_TitleBar").visible, True)
    
        #Verify TitleBar title text is visible and its properties
        test.compare(waitForObjectExists(":TitleBar.TitleText_Text").visible, True)
        test.compare(str(waitForObjectExists(":TitleBar.TitleText_Text").text), "MAIN MENU")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
    

