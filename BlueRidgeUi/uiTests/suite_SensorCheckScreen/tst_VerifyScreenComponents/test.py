"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Verify the screen components displayed
        test.compare(waitForObjectExists(":SensorCheck.Back_Button").visible, False)
        
        test.compare(waitForObjectExists(":SensorCheck.SensorCheckVideo_AnimatedImage").visible, True)
        test.compare(str(waitForObjectExists(":SensorCheck.SensorCheckVideo_AnimatedImage").source.path), "/animations/sensorCheck")
        
        test.compare(waitForObjectExists(":SensorCheck.SignalStrengthText_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorCheck.SignalStrengthText_Text").text), "SIGNAL STRENGTH")
        
        test.compare(waitForObjectExists(":SensorCheck.SignalStrengthIndicator_SignalStrengthIndicator").visible, True)
        
        test.compare(waitForObjectExists(":SensorCheck.InstructionTitle_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorCheck.InstructionTitle_Text").text), "Sensor Check")
        
        test.compare(waitForObjectExists(":SensorCheck.Instruction1_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorCheck.Instruction1_Text").text), "Place sensor package on top of wand so that sensor is in the middle.")
        
        test.compare(waitForObjectExists(":SensorCheck.Instruction2_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorCheck.Instruction2_Text").text), "Continue when signal strength is green and over 70.")
        
        test.compare(waitForObjectExists(":SensorCheck.CancelButton_Button").visible, True)
        test.compare(waitForObjectExists(":SensorCheck.CancelButton_Button").buttonText, "Cancel")
        test.compare(waitForObjectExists(":SensorCheck.ContinueButton_Button").visible, True)
        test.compare(waitForObjectExists(":SensorCheck.ContinueButton_Button").buttonText, "Continue")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
