"""
 @file   tst_ScreenIsDisplayed
 
 Description:

 Test the screen is displayed after the Patient Confirmation screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Verify Sensor Check screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.SensorCheck_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "SensorCheck")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
