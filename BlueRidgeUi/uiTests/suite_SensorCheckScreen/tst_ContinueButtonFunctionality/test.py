"""
 @file   tst_ContinueButtonFunctionality

 Description:

 Test the functionality of the Continue button in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        #verify Sensor check screen is displayed
        test.compare(waitForObjectExists(":MainWindow.SensorCheck_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "SensorCheck")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Press Continue button on screen
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
        
        # Verify Wait for implant dialog is displayed
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        test.compare(str(waitForObjectExists(":Popup_Title_Text").text), "Wait For Implant")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
