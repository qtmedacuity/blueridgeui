"""
 @file   tst_VerifyRhcValuesCard
 
 Description:

 Test the displayed rhc values card
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressure screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))     
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        snooze(5)
        
        # Enter and save the RHC values
        mouseClick(waitForObject(":Implant_recordPressures.EnterRHC_Button"))
        snooze(2)
        values = ["25", "30", "20", "40", "35", "37", "18"]
        for val in values:
            for c in val:
                mouseClick(":NumericKeyboard.{0}_Key_Button".format(c))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":RHCEntry.SaveButton_Button"))
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        mouseClick(waitForObject(":RightButton_Button"))
        snooze(1)

        # Verify the RHC values card displayed on the screen
        test.compare(waitForObjectExists(":RHCValuesCard.Title_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.Title_Text").text), "RHC Values")
        
        test.compare(waitForObjectExists(":RHCValuesCard.EditButton_Button").visible, True)
        test.compare(waitForObjectExists(":RHCValuesCard.EditButton_Button").enabled, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.EditButton_Button").buttonText), "Edit")
        
        test.compare(waitForObjectExists(":Review.RHCValuesCard_RhcValuesCard").visible, True)
        
        test.compare(waitForObjectExists(":RHCValuesCard.RALabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RALabel_Text").text), "RA")
        
        test.compare(waitForObjectExists(":RHCValuesCard.RaMeanText_MeanText").visible, True)
        test.compare(waitForObjectExists(":RHCValuesCard.RaMeanText_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RaMeanText_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":RHCValuesCard.RaMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RaMeanText_Mean_Text").text), values[0])
        test.compare(waitForObjectExists(":RHCValuesCard.RaMeanText_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RaMeanText_LeftParen_Text").text), ")")
        test.compare(waitForObjectExists(":RHCValuesCard.RaMeanText_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RaMeanText_Label_Text").text), "mean")
        
        test.compare(waitForObjectExists(":RHCValuesCard.RVLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RVLabel_Text").text), "RV")
        
        test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_SystolicDiastolicText").visible, True)
        test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Systolic_Text").text), values[1])        
        test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_SysLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_SysLabel_Text").text), "sys")        
        test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Separator_Text").text), "/")        
        test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Diastolic_Text").text), values[2])        
        test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_DiaLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_DiaLabel_Text").text), "dia")        
        test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Label_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":RHCValuesCard.PcwpLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PcwpLabel_Text").text), "PCWP")
        
        test.compare(waitForObjectExists(":RHCValuesCard.PcwpMeanText_MeanText").visible, True)
        test.compare(waitForObjectExists(":RHCValuesCard.PcwpMeanText_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PcwpMeanText_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":RHCValuesCard.PcwpMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PcwpMeanText_Mean_Text").text), values[6])
        test.compare(waitForObjectExists(":RHCValuesCard.PcwpMeanText_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PcwpMeanText_LeftParen_Text").text), ")")
        test.compare(waitForObjectExists(":RHCValuesCard.PcwpMeanText_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PcwpMeanText_Label_Text").text), "mean")
        
        test.compare(waitForObjectExists(":RHCValuesCard.PALabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PALabel_Text").text), "PA")
        
        test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_SystolicDiastolicText").visible, True)
        test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Systolic_Text").text), values[3])
        test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_SysLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_SysLabel_Text").text), "sys")
        test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Separator_Text").text), "/")
        test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Diastolic_Text").text), values[4])
        test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_DiaLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_DiaLabel_Text").text), "dia")
        test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Label_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":RHCValuesCard.PaMeanText_MeanText").visible, True)
        test.compare(waitForObjectExists(":RHCValuesCard.PaMeanText_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaMeanText_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":RHCValuesCard.PaMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaMeanText_Mean_Text").text), values[5])
        test.compare(waitForObjectExists(":RHCValuesCard.PaMeanText_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaMeanText_LeftParen_Text").text), ")")
        test.compare(waitForObjectExists(":RHCValuesCard.PaMeanText_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaMeanText_Label_Text").text), "mean")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
