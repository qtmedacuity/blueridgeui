"""
 @file   tst_VerifyRecordedPressureCard
 
 Description:

 Test the displayed recorded pressure card
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        
        # Record pressure card
        refValues = ["35", "30", "32"]
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        for val in refValues:
            for c in val:
                mouseClick(":NumericKeyboard.{0}_Key_Button".format(c))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Store the card sensor values
        hearRate = str(waitForObjectExists(":RecordedPressure_0.HeartRate_Text").text)
        recordTime = str(waitForObjectExists(":RecordedPressure_0.RecordedTime_Text").text)
        sensorMean = str(waitForObjectExists(":RecordedPressure_0.SensorMean_Mean_Text").text)
        sensorSys = str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Systolic_Text").text)
        sensorDia = str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Diastolic_Text").text)
        strength = waitForObjectExists(":RecordedPressure_0.SignalStrength_SignalStrengthIcon").sensorStrengthVal
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        mouseClick(waitForObject(":RightButton_Button"))
        snooze(3)
        
        # Verify the Recorded Pressure Card displayed on the screen
        test.compare(waitForObjectExists(":RecordingListView.ReviewCard_0_RecordingReviewCard").visible, True)
        
        test.compare(waitForObjectExists(":ReviewCard_0.DeleteButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":ReviewCard_0.DeleteButton_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":ReviewCard_0.DeleteButton_IconButton").iconImage.path), "/images/trashIcon")
        
        test.compare(waitForObjectExists(":ReviewCard_0.EditButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":ReviewCard_0.EditButton_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":ReviewCard_0.EditButton_IconButton").iconImage.path), "/images/addnote")
        
        test.compare(waitForObjectExists(":ReviewCard_0.CardTitle_Text").visible, False)
        test.compare(str(waitForObjectExists(":ReviewCard_0.CardTitle_Text").text), "")
        
        test.compare(waitForObjectExists(":ReviewCard_0.StaticWaveChart_StaticWaveChart").visible, True)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.EventTime_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.EventTime_Text").text), recordTime)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SystolicDiastolicLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.SystolicDiastolicLabel_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.MeanLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.MeanLabel_Text").text), "Mean")
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.HeartRateLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.HeartRateLabel_Text").text), "HR")
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SignalLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.SignalLabel_Text").text), "Signal")
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SensorLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.SensorLabel_Text").text), "SENSOR")
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SystolicDiastolicText_SystolicDiastolicText").visible, True)
        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Label_Text").text), "Systolic/Diastolic")
        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_SysLabel_Text").visible, False)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_SysLabel_Text").text), "sys")
        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Systolic_Text").text), sensorSys)
        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Separator_Text").text), "/")
        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_DiaLabel_Text").visible, False)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_DiaLabel_Text").text), "dia")
        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Diastolic_Text").text), sensorDia)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.MeanText_MeanText").visible, True)
        test.compare(waitForObjectExists(":RecordingReview_DataValues.MeanText_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.MeanText_Label_Text").text), "Mean")
        test.compare(waitForObjectExists(":RecordingReview_DataValues.MeanText_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.MeanText_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":RecordingReview_DataValues.MeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.MeanText_Mean_Text").text), sensorMean)
        test.compare(waitForObjectExists(":RecordingReview_DataValues.MeanText_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.MeanText_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.HeartRate_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.HeartRate_Text").text), hearRate)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SignalStrength_SignalStrengthIcon").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.SignalStrength_SignalStrengthIcon").state), "good")
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SignalStrength_SignalStrengthIcon").sensorStrengthVal, strength)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.RefLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.RefLabel_Text").text), "REF")
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.RefSystolicDiastolicText_SystolicDiastolicText").visible, True)
        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Label_Text").text), "Systolic/Diastolic")
        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_SysLabel_Text").visible, False)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_SysLabel_Text").text), "sys")
        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Systolic_Text").text), refValues[0])
        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Separator_Text").text), "/")
        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_DiaLabel_Text").visible, False)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_DiaLabel_Text").text), "dia")
        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Diastolic_Text").text), refValues[1])
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_Label_Text").text), "Mean")
        test.compare(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_Mean_Text").text), refValues[2])
        test.compare(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_LeftParen_Text").text), ")")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
