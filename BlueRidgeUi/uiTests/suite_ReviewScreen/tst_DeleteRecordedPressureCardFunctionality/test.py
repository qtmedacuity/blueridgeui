"""
 @file   tst_DeleteRecordedPressureCardFunctionality
 
 Description:

 Test the Delete button functionality of recorded pressure card displayed in the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        paMean = "40"
        for n in paMean:
            mouseClick(waitForObject(":NumericKeyboard.{0}_Key_Button".format(n)))

        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        
        # Record three pressure cards
        refValues = [["35", "30", "32"], ["30", "20", "25"], ["40", "35", "37"]]
        
        for values in refValues:
            mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
            snooze(3)
            for value in values:
                for n in value:
                    mouseClick(waitForObject(":NumericKeyboard.{0}_Key_Button".format(n)))
                mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))            
            mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
            snooze(3)
        
        # Verify the recorded cards are displayed on the Record Pressure screen
        test.compare(str(waitForObjectExists(":Implant_recordPressures.RecordedPressuresLabel_Text").text), "RECORDED PRESSURES (3):")
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 3)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_1_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_2_RecordPressureCard").visible, True)
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        snooze(3)
        
        # Verify 3 Recorded Pressure cards are displayed on the Review screen
        test.compare(waitForObjectExists(":Review.RecordingListView_ListView").count, 3)
        test.compare(waitForObjectExists(":RecordingListView.ReviewCard_0_RecordingReviewCard").visible, True)
        test.compare(waitForObjectExists(":RecordingListView.ReviewCard_1_RecordingReviewCard").visible, True)
        test.compare(waitForObjectExists(":RecordingListView.ReviewCard_2_RecordingReviewCard").visible, True)

        # Delete the second card via the Delete button button
        mouseClick(waitForObject(":ReviewCard_1.DeleteButton_IconButton"))
        mouseClick(waitForObject(":DeleteButton_Button"))
        snooze(2)
        
        # Verify 2 Recorded Pressure cards are displayed on the Review screen
        test.compare(waitForObjectExists(":Review.RecordingListView_ListView").count, 2)
        test.compare(waitForObjectExists(":RecordingListView.ReviewCard_0_RecordingReviewCard").visible, True)
        test.compare(waitForObjectExists(":RecordingListView.ReviewCard_1_RecordingReviewCard").visible, True)
        test.compare(object.exists(":RecordingListView.ReviewCard_2_RecordingReviewCard"), False)
        
        # Return to record pressure screen and verify only 2 card are displayed
        mouseClick(waitForObject(":Review.BackButton_Button"))
        test.compare(str(waitForObjectExists(":Implant_recordPressures.RecordedPressuresLabel_Text").text), "RECORDED PRESSURES (2):")
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 2)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_1_RecordPressureCard").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard1_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard2_RecordPressureBlankCard").visible, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard3_RecordPressureBlankCard").visible, True)
    
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
