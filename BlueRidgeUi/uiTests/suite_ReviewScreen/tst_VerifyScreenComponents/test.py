"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        snooze(5)
        
        # Record 3 pressure recordings
        for x in range(3):
            mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
            snooze(5)
            mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
            snooze(3)
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        snooze(1)
        
        # Verify the components displayed on the screen
        test.compare(waitForObjectExists(":Review.BackButton_Button").visible, True)
        test.compare(waitForObjectExists(":Review.BackButton_Button").enabled, True)
        test.compare(str(waitForObjectExists(":Review.BackButton_Button").buttonText), "< back")
        
        test.compare(waitForObjectExists(":Review.HelpButton_Button").visible, True)
        test.compare(waitForObjectExists(":Review.HelpButton_Button").enabled, True)
        test.compare(str(waitForObjectExists(":Review.HelpButton_Button").buttonText), "Help")
    
        test.compare(waitForObjectExists(":Review.SensorSelectionImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":Review.SensorSelectionImage_Image").source.path), "/images/bodyleft")
        
        test.compare(waitForObjectExists(":Review.RHCValuesCard_RhcValuesCard").visible, True)
        test.compare(waitForObjectExists(":Review.SensorReviewCard_SensorReviewCard").visible, True)
        test.compare(waitForObjectExists(":Review.COReviewCard_CardiacOutputReviewCard").visible, False)
        
        test.compare(waitForObjectExists(":Review.RecordedPressuresLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":Review.RecordedPressuresLabel_Text").text), "RECORDED PRESSURES")
        
        test.compare(waitForObjectExists(":RecordingListView.ReviewCard_0_RecordingReviewCard").visible, True)
        test.compare(waitForObjectExists(":RecordingListView.ReviewCard_1_RecordingReviewCard").visible, True)
        test.compare(waitForObjectExists(":RecordingListView.ReviewCard_2_RecordingReviewCard").visible, True)
        
        test.compare(waitForObjectExists(":Review.UploadButton_Button").visible, True)
        test.compare(waitForObjectExists(":Review.UploadButton_Button").enabled, True)
        test.compare(str(waitForObjectExists(":Review.UploadButton_Button").buttonText), "Upload to merlin.net")
        
        test.compare(waitForObjectExists(":Review.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":Review.SaveButton_Button").enabled, False)
        test.compare(str(waitForObjectExists(":Review.SaveButton_Button").buttonText), "Save and End Case")
        
        test.compare(waitForObjectExists(":Review_ScrollBar").visible, True)
        test.compare(waitForObjectExists(":Review_ScrollBar_2").visible, False)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
