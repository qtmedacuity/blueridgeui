"""
 @file   tst_ScreenIsDisplayed
 
 Description:

 Test the screen is displayed in various use cases
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.RightButton_Image"))
        
        # Navigate to Calibrate Sensor screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
               
        # Verify Record Pressures screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Implant_recordPressures")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        
        # Verify Confirmation popup when no Recorded Pressures
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        test.compare(waitForObjectExists(":QuestionText_Text").visible, True)
        test.compare(str(waitForObjectExists(":QuestionText_Text").text), "You have not recorded pressures. Would you like to continue to the review screen?")
        test.compare(waitForObjectExists(":CloseButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":CloseButton_IconButton").enabled, True)
        test.compare(waitForObjectExists(":LeftButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":LeftButton_Button").buttonText), "Cancel")
        test.compare(waitForObjectExists(":LeftButton_Button").buttonIconVisible, False)
        test.compare(waitForObjectExists(":RightButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":RightButton_Button").buttonText), "Continue")
        test.compare(waitForObjectExists(":RightButton_Button").buttonIconVisible, False)

        # Continue to Review screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        #  Verify Review screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainStackView.Review_Review").visible, True)
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Review")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)

        # Close review screen
        mouseClick(waitForObject(":Review.BackButton_Button"))

        # Record pressure reading
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        
        # Verify confirmation popup when less than 3 Recorded Pressures
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        test.compare(waitForObjectExists(":QuestionText_Text").visible, True)
        test.compare(str(waitForObjectExists(":QuestionText_Text").text), "You have taken less than three recordings. Would you like to continue to the review screen?")
        test.compare(waitForObjectExists(":CloseButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":CloseButton_IconButton").enabled, True)
        test.compare(waitForObjectExists(":LeftButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":LeftButton_Button").buttonText), "Cancel")
        test.compare(waitForObjectExists(":LeftButton_Button").buttonIconVisible, False)
        test.compare(waitForObjectExists(":RightButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":RightButton_Button").buttonText), "Continue")
        test.compare(waitForObjectExists(":RightButton_Button").buttonIconVisible, False)
        
        # Continue to Review screen
        mouseClick(waitForObject(":RightButton_Button"))
        snooze(3)
        
        #  Verify Review screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainStackView.Review_Review").visible, True)
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Review")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Return to previous screen
        mouseClick(waitForObject(":Review.BackButton_Button"))
        
        # Record 2 additional recordings
        for x in range(2):
            mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
            snooze(5)
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
            snooze(3)
        
        # Navigate to Review screen after 3 recorded pressure readings
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        snooze(2)
        
        #  Verify Review screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainStackView.Review_Review").visible, True)
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Review")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
