"""
 @file   tst_DeleteSensorCalibrationFunctionality
 
 Description:

 Test the functionality of the Delete button on the Sensor Review card
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.RightButton_Image"))
        
        # Navigate to Calibrate Sensor screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Enter PA mean
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        snooze(2)
        
        refMean = str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").text)
        systolic = str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Systolic_Text").text)
        diastolic = str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Diastolic_Text").text)
        mean = str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Mean_Text").text)
        
        # Navigate to Record Pressure screen
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button")) 
        mouseClick(waitForObject(":RightButton_Button"))
        snooze(1)
        
        # Verify Sensor calibration card before editing
        test.compare(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Systolic_Text").text), systolic)
               
        test.compare(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Diastolic_Text").text), diastolic)
                
        test.compare(waitForObjectExists(":SensorReview_DataValues.MeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.MeanText_Mean_Text").text), mean)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.RefMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.RefMeanText_Mean_Text").text), refMean)
        
        # Press Delete button on Sensor Calibration
        mouseClick(waitForObject(":SensorReviewCard.DeleteButton_IconButton"))
        
        # TODO: Delete Sensor Calibration once functionality is implemented
        test.log("TODO: Delete Sensor Calibration once functionality is implemented")
        
        # TODO:  Verify Sensor calibration card after deleteing
        test.log("TODO: Verify Sensor calibration card after deleting")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
