"""
 @file   tst_UploadToMerlinButtonFunctionality
 
 Description:

 Test the functionality of the Upload to Merlin button
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        args = "-cell-enabled disabled --cell-connected false --wifi-connected false --wifi-enabled disabled PatientFile.txt"
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc(args)
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        patientName = str(waitForObjectExists(":PatientConfirmation.patientName_PatientConfirmationText").infoText)
        dob = str(waitForObjectExists(":PatientConfirmation.patientDob_PatientConfirmationText").infoText)
        serialNum = str(waitForObjectExists(":PatientConfirmation.patientSerialNumber_PatientConfirmationText").infoText)
        implantDate = str(waitForObjectExists(":PatientConfirmation.patientImplantDate_PatientConfirmationText").infoText)
        patientId = str(waitForObjectExists(":PatientConfirmation.patientId_PatientConfirmationText").infoText)
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        
        # Record pressure card
        refValues = [["35", "30", "32"], ["34", "28", "29"], ["40", "30", "35"]]

        for values in refValues:
            mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
            snooze(5)
            for val in values:
                for c in val:
                    mouseClick(":NumericKeyboard.{0}_Key_Button".format(c))
                mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
            snooze(3)
        
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        snooze(1)
        
        # Verify Save and End Case button is enabled since Wifi and Cellular are disabled and disconnected
        test.compare(waitForObjectExists(":Review.UploadButton_Button").visible, True)
        test.compare(waitForObjectExists(":Review.UploadButton_Button").enabled, False)
        test.compare(str(waitForObjectExists(":Review.UploadButton_Button").buttonText), "Upload to merlin.net")
        
        test.compare(waitForObjectExists(":Review.SaveButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":Review.SaveButton_Button").buttonText), "Save and End Case")
        test.compare(waitForObjectExists(":Review.SaveButton_Button").enabled, True)
        
        test.compare(str(waitForObjectExists(":TitleBar.CellularStrengthBars_Item").state), "fault")
        test.compare(str(waitForObjectExists(":TitleBar.WifiIndicator_Item").state), "fault")

        # Press Save and End case button
        mouseClick(waitForObject(":Review.SaveButton_Button"))
        snooze(1)
        
        # Verify Reading successfully sent popup
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        
        test.compare(waitForObjectExists(":OK_Button").visible, True)
        test.compare(str(waitForObjectExists(":OK_Button").buttonText), "OK")
        test.compare(waitForObjectExists(":OK_Button").enabled, True)
        
        test.compare(waitForObjectExists(":Title_Text").visible, True)
        test.compare(str(waitForObjectExists(":Title_Text").text), "Reading successfully saved.")
        
        test.compare(waitForObjectExists(":Icon_Image").visible, True)
        test.compare(str(waitForObjectExists(":Icon_Image").source.path), "/images/checkmark_large")
        
        test.compare(waitForObjectExists(":ReadingLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":ReadingLabel_Text").text), "READING TAKEN")
        
        test.compare(waitForObjectExists(":NameLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":NameLabel_Text").text), "NAME")
        
        test.compare(waitForObjectExists(":DobLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":DobLabel_Text").text), "DOB")
        
        test.compare(waitForObjectExists(":MrnLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":MrnLabel_Text").text), "MRN/PATIENT ID")
        
        test.compare(waitForObjectExists(":SensorSerialLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorSerialLabel_Text").text), "SENSOR SERIAL #")
        
        test.compare(waitForObjectExists(":ImplantDateLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":ImplantDateLabel_Text").text), "IMPLANT DATE")
        
        test.compare(waitForObjectExists(":DateTime_Text").visible, True)
        
        test.compare(waitForObjectExists(":PatientName_Text").visible, True)
        test.compare(str(waitForObjectExists(":PatientName_Text").text), patientName)
        
        test.compare(waitForObjectExists(":DOB_Text").visible, True)
        test.compare(str(waitForObjectExists(":DOB_Text").text), dob)
        
        test.compare(waitForObjectExists(":PatientId_Text").visible, True)
        test.compare(str(waitForObjectExists(":PatientId_Text").text), patientId)
        
        test.compare(waitForObjectExists(":SerialNumber_Text").visible, True)
        test.compare(str(waitForObjectExists(":SerialNumber_Text").text), serialNum)
        
        test.compare(waitForObjectExists(":ImplantDate_Text").visible, True)
        test.compare(str(waitForObjectExists(":ImplantDate_Text").text), implantDate)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
