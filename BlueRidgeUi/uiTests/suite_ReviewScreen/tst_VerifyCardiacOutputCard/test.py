"""
 @file   tst_VerifyCardiacOutputCard
 
 Description:

 Test the displayed cardiac ouput card
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressure screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        
        # Enter Cardiac output and store the value

        mouseClick(waitForObject(":Implant_recordPressures.CalibrateCO_Button"))
        mouseClick(waitForObject(":NumericKeyboard.1_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.8_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        cardiacOuput = str(waitForObject(":CoEntryOverlay.CardiacOutput_CardiacOutputTextField").value)

        mouseClick(waitForObject(":CoEntryOverlay.SaveButton_Button"))

        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        mouseClick(waitForObject(":RightButton_Button"))
        snooze(1)
        
        # Verify the Cardiac Output card displayed on the screen
        test.compare(waitForObjectExists(":Review.COReviewCard_CardiacOutputReviewCard").visible, True)
        
        test.compare(waitForObjectExists(":COReviewCard.DeleteButton_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":COReviewCard.DeleteButton_IconButton").iconImage.path), "/images/trashIcon")
        test.compare(waitForObjectExists(":COReviewCard.DeleteButton_IconButton").enabled, True)
        
        test.compare(waitForObjectExists(":COReviewCard.EditButton_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":COReviewCard.EditButton_IconButton").iconImage.path), "/images/addnote")
        test.compare(waitForObjectExists(":COReviewCard.EditButton_IconButton").enabled, True)
        
        test.compare(waitForObjectExists(":COReviewCard.CardTitle_Text").visible, True)
        test.compare(waitForObjectExists(":COReviewCard.CardTitle_Text").text, "CARDIAC OUTPUT CALIBRATION")
        
        test.compare(waitForObjectExists(":COReviewCard.StaticWaveChart_StaticWaveChart").visible, True)
        
        test.compare(waitForObjectExists(":COReviewCard.EventTime_Text").visible, True)
        
        test.compare(waitForObjectExists(":COReviewCard.HeartRateLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":COReviewCard.HeartRateLabel_Text").text), "HR")
        
        test.compare(waitForObjectExists(":COReviewCard.Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":COReviewCard.Label_Text").text), "Cardiac Output")
        
        test.compare(waitForObjectExists(":COReviewCard.SignalLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":COReviewCard.SignalLabel_Text").text), "Signal")
        
        test.compare(waitForObjectExists(":COReviewCard.CardiacOutput_Text").visible, True)
        test.compare(str(waitForObjectExists(":COReviewCard.CardiacOutput_Text").text), cardiacOuput)
        
        test.compare(waitForObjectExists(":COReviewCard.HeartRate_Text").visible, True)
        test.compare(str(waitForObjectExists(":COReviewCard.HeartRate_Text").text), "70")
        
        test.compare(waitForObjectExists(":COReviewCard.SignalStrength_SignalStrengthIcon").visible, True)
        test.compare(waitForObjectExists(":COReviewCard.SignalStrength_SignalStrengthIcon").sensorStrengthVal, 90)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
