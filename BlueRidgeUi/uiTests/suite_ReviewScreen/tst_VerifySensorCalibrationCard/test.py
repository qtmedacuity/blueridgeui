"""
 @file   tst_VerifySensorCalibrationCard
 
 Description:

 Test the displayed sensor calibration card
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Enter PA Mean and store values
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))     
        snooze(5)
        
        refMean = str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").text)
        systolic = str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Systolic_Text").text)
        diastolic = str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Diastolic_Text").text)
        mean = str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Mean_Text").text)
        
        # Navigate to Record Pressure screen
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        mouseClick(waitForObject(":RightButton_Button"))
        snooze(1)
        
        # Verify the Sensor Calibration Review card displayed on the screen
        test.compare(waitForObjectExists(":Review.SensorReviewCard_SensorReviewCard").visible, True)

        test.compare(waitForObjectExists(":SensorReviewCard.CardTitle_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReviewCard.CardTitle_Text").text), "SENSOR CALIBRATION")
        
        test.compare(waitForObjectExists(":SensorReviewCard.StaticWaveChart_StaticWaveChart").visible, True)
        
        test.compare(waitForObjectExists(":SensorReviewCard.EditButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":SensorReviewCard.EditButton_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":SensorReviewCard.EditButton_IconButton").iconImage.path), "/images/addnote")
        
        test.compare(waitForObjectExists(":SensorReviewCard.DeleteButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":SensorReviewCard.DeleteButton_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":SensorReviewCard.DeleteButton_IconButton").iconImage.path), "/images/trashIcon")
        
        test.compare(waitForObjectExists(":SensorReviewCard.SensorReview_DataValues_ReviewDataValues").visible, True)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.EventTime_Text").visible, True)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicLabel_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.MeanLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.MeanLabel_Text").text), "Mean")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.HeartRateLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.HeartRateLabel_Text").text), "HR")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SignalLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.SignalLabel_Text").text), "Signal")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SensorLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.SensorLabel_Text").text), "SENSOR")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Systolic_Text").text), systolic)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_SysLabel_Text").visible, False)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Separator_Text").text), "/")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Diastolic_Text").text), diastolic)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_DiaLabel_Text").visible, False)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SystolicDiastolicText_Label_Text").visible, False)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.MeanText_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.MeanText_RightParen_Text").text), "(")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.MeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.MeanText_Mean_Text").text), mean)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.MeanText_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.MeanText_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.MeanText_Label_Text").visible, False)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.HeartRate_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.HeartRate_Text").text), "70")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.SignalStrength_SignalStrengthIcon").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.SignalStrength_SignalStrengthIcon").state), "good")
        test.compare(waitForObjectExists(":SensorReview_DataValues.SignalStrength_SignalStrengthIcon").sensorStrengthVal, 90)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.RefLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.RefLabel_Text").text), "REF")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.RefBlankItem_Item").visible, True)

        test.compare(waitForObjectExists(":SensorReview_DataValues.RefSystolicDiastolicText_SystolicDiastolicText").visible, False)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.RefMeanText_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.RefMeanText_RightParen_Text").text), "(")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.RefMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.RefMeanText_Mean_Text").text), refMean)
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.RefMeanText_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorReview_DataValues.RefMeanText_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":SensorReview_DataValues.RefMeanText_Label_Text").visible, False)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
