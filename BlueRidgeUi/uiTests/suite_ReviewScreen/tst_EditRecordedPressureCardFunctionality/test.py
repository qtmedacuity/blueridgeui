"""
 @file   tst_EditRecordedPressureCardFunctionality
 
 Description:

 Test the edit button functionality of recorded pressure card displayed in the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        paMean = "40"
        for n in paMean:
            mouseClick(waitForObject(":NumericKeyboard.{0}_Key_Button".format(n)))

        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        
        # Record pressure card
        refValues = ["35", "30", "32"]
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        for val in refValues:
            for n in val:
                mouseClick(waitForObject(":NumericKeyboard.{0}_Key_Button".format(n)))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Store the card sensor values
        hearRate = str(waitForObjectExists(":RecordedPressure_0.HeartRate_Text").text)
        sensorMean = str(waitForObjectExists(":RecordedPressure_0.SensorMean_Mean_Text").text)
        sensorSys = str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Systolic_Text").text)
        sensorDia = str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Diastolic_Text").text)
        strength = waitForObjectExists(":RecordedPressure_0.SignalStrength_SignalStrengthIcon").sensorStrengthVal
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        mouseClick(waitForObject(":RightButton_Button"))
        snooze(1)
        
        # Verify the Recorded Pressure Card sensor and reference values before editing
        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Systolic_Text").text), sensorSys)

        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Diastolic_Text").text), sensorDia)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.MeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.MeanText_Mean_Text").text), sensorMean)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.HeartRate_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.HeartRate_Text").text), hearRate)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SignalStrength_SignalStrengthIcon").visible, True)
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SignalStrength_SignalStrengthIcon").sensorStrengthVal, strength)
        
        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Systolic_Text").text), refValues[0])

        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Diastolic_Text").text), refValues[1])
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_Mean_Text").text), refValues[2])
        
        # Edit the card via the Edit button
        mouseClick(waitForObject(":ReviewCard_0.EditButton_IconButton"))

        # Record pressure card
        newValues = ["30", "28", "29"]
        snooze(5)
        for val in newValues:
            mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
            for c in val:
                mouseClick(":NumericKeyboard.{0}_Key_Button".format(c))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Verify the Recorded Pressure Card sensor and reference values after editing card
        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Systolic_Text").text), sensorSys)

        test.compare(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SystolicDiastolicText.SystolicDiastolicText_Diastolic_Text").text), sensorDia)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.MeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.MeanText_Mean_Text").text), sensorMean)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.HeartRate_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.HeartRate_Text").text), hearRate)
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SignalStrength_SignalStrengthIcon").visible, True)
        test.compare(waitForObjectExists(":RecordingReview_DataValues.SignalStrength_SignalStrengthIcon").sensorStrengthVal, strength)
        
        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Systolic_Text").text), newValues[0])

        test.compare(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RefSystolicDiastolicText.RefSystolicDiastolicText_Diastolic_Text").text), newValues[1])
        
        test.compare(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingReview_DataValues.RefMeanText_Mean_Text").text), newValues[2])
        
        # Return to record pressure screen and verify reference values were updated on the card
        mouseClick(waitForObject(":Review.BackButton_Button"))
        
        test.compare(waitForObjectExists(":RecordedPressure_0.HeartRate_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.HeartRate_Text").text), hearRate)
           
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Systolic_Text").text), sensorSys)

        test.compare(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorSystolicDiastolic_Diastolic_Text").text), sensorDia)
        
        test.compare(waitForObjectExists(":RecordedPressure_0.SensorMean_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.SensorMean_Mean_Text").text), sensorMean)
           
        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Systolic_Text").text), newValues[0])

        test.compare(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.RefSystolicDiastolic_Diastolic_Text").text), newValues[1])
        
        test.compare(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.ReferenceMean_Mean_Text").text), newValues[2])
        
        test.compare(waitForObjectExists(":RecordedPressure_0.SignalStrength_SignalStrengthIcon").visible, True)
        test.compare(waitForObjectExists(":RecordedPressure_0.SignalStrength_SignalStrengthIcon").sensorStrengthVal, 98)
        
        test.compare(waitForObjectExists(":RecordedPressure_0.HorizontalPositionIndicator_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordedPressure_0.HorizontalPositionIndicator_Image").position), "Horizontal")
        
        test.compare(waitForObjectExists(":RecordedPressure_0.NoteIndicator_Image").visible, False)
    
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
