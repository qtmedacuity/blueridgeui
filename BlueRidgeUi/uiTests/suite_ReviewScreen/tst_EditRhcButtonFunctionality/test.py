"""
 @file   tst_RHCEditButtonFunctionality
 
 Description:

 Test the functionality of the Back button on the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressure screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
        snooze(5)
        
        # Enter and save the RHC Values
        mouseClick(waitForObject(":Implant_recordPressures.EnterRHC_Button"))
        snooze(3)
        values = ["25", "30", "20", "40", "30", "35", "20"]
        for val in values:
            for c in val:
                mouseClick(":NumericKeyboard.{0}_Key_Button".format(c))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":RHCEntry.SaveButton_Button"))
        
        # Navigate to Review screen
        mouseClick(waitForObject(":Implant_recordPressures.ReviewButton_Button"))
        mouseClick(waitForObject(":RightButton_Button"))
        snooze(1)
        
        # Verify the RHC values before editing        
        test.compare(waitForObjectExists(":RHCValuesCard.EditButton_Button").visible, True)
        test.compare(waitForObjectExists(":RHCValuesCard.EditButton_Button").enabled, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.EditButton_Button").buttonText), "Edit")
        
        test.compare(waitForObjectExists(":Review.RHCValuesCard_RhcValuesCard").visible, True)
        
        test.compare(waitForObjectExists(":RHCValuesCard.RALabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RALabel_Text").text), "RA")
        
        test.compare(waitForObjectExists(":RHCValuesCard.RaMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RaMeanText_Mean_Text").text), values[0])
        
        test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Systolic_Text").text), values[1])        
          
        test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Diastolic_Text").text), values[2])        

        test.compare(waitForObjectExists(":RHCValuesCard.PcwpMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PcwpMeanText_Mean_Text").text), values[6])
        
        test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Systolic_Text").text), values[3])
        
        test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Diastolic_Text").text), values[4])
        
        test.compare(waitForObjectExists(":RHCValuesCard.PaMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RHCValuesCard.PaMeanText_Mean_Text").text), values[5])
        
        # TODO: Edit the RHC values
        test.log("TODO: Edit values once functionality implemented")
        mouseClick(waitForObjectExists(":RHCValuesCard.EditButton_Button"))
        
        # TODO: Verify the RHC values after editing
        test.log("TODO: Verify RHC values after editing")
#         
#         test.compare(waitForObjectExists(":RHCValuesCard.EditButton_Button").visible, True)
#         test.compare(waitForObjectExists(":RHCValuesCard.EditButton_Button").enabled, True)
#         test.compare(str(waitForObjectExists(":RHCValuesCard.EditButton_Button").buttonText), "Edit")
#         
#         test.compare(waitForObjectExists(":Review.RHCValuesCard_RhcValuesCard").visible, True)
#         
#         test.compare(waitForObjectExists(":RHCValuesCard.RALabel_Text").visible, True)
#         test.compare(str(waitForObjectExists(":RHCValuesCard.RALabel_Text").text), "RA")
#         
#         test.compare(waitForObjectExists(":RHCValuesCard.RaMeanText_Mean_Text").visible, True)
#         test.compare(str(waitForObjectExists(":RHCValuesCard.RaMeanText_Mean_Text").text), " ")
#         
#         test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Systolic_Text").visible, True)
#         test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Systolic_Text").text), " ")        
#           
#         test.compare(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Diastolic_Text").visible, True)
#         test.compare(str(waitForObjectExists(":RHCValuesCard.RvSystolicDiastolicText_Diastolic_Text").text), " ")        
# 
#         test.compare(waitForObjectExists(":RHCValuesCard.PcwpMeanText_Mean_Text").visible, True)
#         test.compare(str(waitForObjectExists(":RHCValuesCard.PcwpMeanText_Mean_Text").text), " ")
#         
#         test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Systolic_Text").visible, True)
#         test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Systolic_Text").text), " ")
#         
#         test.compare(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Diastolic_Text").visible, True)
#         test.compare(str(waitForObjectExists(":RHCValuesCard.PaSystolicDiastolicText_Diastolic_Text").text), " ")
#         
#         test.compare(waitForObjectExists(":RHCValuesCard.PaMeanText_Mean_Text").visible, True)
#         test.compare(str(waitForObjectExists(":RHCValuesCard.PaMeanText_Mean_Text").text), " ")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
