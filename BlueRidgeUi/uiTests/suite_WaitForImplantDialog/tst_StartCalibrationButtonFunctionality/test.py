"""
 @file   tst_StartCalibrationButtonFunctionality

 Description:

 Test the functionality of the Start Calibration button displayed in the dialog

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Verify Sensor check screen is displayed
        test.compare(waitForObjectExists(":MainWindow.SensorCheck_Page").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "SensorCheck")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Navigate to Wait for Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
        
        # Verify Wait for implant dialog is displayed
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        
        # Press Start Calibration button on dialog
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Verify the Select Implant Position screen is displayed
        test.compare(waitForObjectExists(":MainWindow.Implant_selectSensor_ImplantRoot").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Implant_selectSensor")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)      

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
