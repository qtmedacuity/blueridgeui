"""
 @file   tst_VerifyDialogComponents

 Description:

 Test the components displayed on the dialog

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
               
        # Press Continue button on screen
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
        
        # Verify Wait for implant dialog is displayed and its components
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        test.compare(waitForObjectExists(":CloseButton_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":Popup_Title_Text").text), "Wait For Implant")
        test.compare(waitForObjectExists(":Popup_Title_Text").visible, True)
        test.compare(str(waitForObjectExists(":Popup_Instruction_Text").text), "The next step will be to calibrate the sensor with data from\nthe PA catheter.\n\nWait until:\n1. CardioMEMS has been implanted, and\n2. The PA catheter is in position")
        test.compare(waitForObjectExists(":Popup_Instruction_Text").visible, True)
        test.compare(waitForObjectExists(":LeftButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":LeftButton_Button").buttonText), "Cancel")
        test.compare(waitForObjectExists(":RightButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":RightButton_Button").buttonText), "Start Calibration")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
