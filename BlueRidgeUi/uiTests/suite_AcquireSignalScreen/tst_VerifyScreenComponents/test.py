"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.RightButton_Image"))
        
        # Verify the components displayed on the screen
        test.compare(waitForObjectExists(":Implant_acquireSignal.Back_Button").visible, False)
        test.compare(waitForObjectExists(":Implant_acquireSignal.InstructionTitle_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_acquireSignal.InstructionTitle_Text").text), "Acquire Signal")
        test.compare(waitForObjectExists(":Implant_acquireSignal.InstructionText_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_acquireSignal.InstructionText_Text").text), "Place wand under sensor. Adjust wand slightly until signal strength is green and above 70.")
        test.compare(waitForObjectExists(":Implant_acquireSignal.AcquireSignalVideo_AnimatedImage").visible, True)
        test.compare(waitForObjectExists(":Implant_acquireSignal.ContinueButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":Implant_acquireSignal.ContinueButton_Button").buttonText), "Continue")
        test.compare(waitForObjectExists(":Implant_acquireSignal.CancelButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":Implant_acquireSignal.CancelButton_Button").buttonText), "Cancel")
        test.compare(waitForObjectExists(":Implant_acquireSignal.Units_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_acquireSignal.Units_Text").text), "mmHg")
        test.compare(waitForObjectExists(":Implant_acquireSignal.ZoomIn_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":Implant_acquireSignal.ZoomIn_IconButton").iconImage.path), "/images/plusIcon")
        test.compare(waitForObjectExists(":Implant_acquireSignal.ZoomOut_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":Implant_acquireSignal.ZoomOut_IconButton").iconImage.path), "/images/minusIcon")
        test.compare(waitForObjectExists(":Implant_acquireSignal.ActiveWaveChart_BaseWaveChart").visible, True)
        test.compare(waitForObjectExists(":Implant_acquireSignal.PressureSearchBar_PressureSearchBar").visible, True)
        test.compare(waitForObjectExists(":Implant_acquireSignal.Flicker_PressureSearchBar_Flickable").interactive, True)
        test.compare(waitForObjectExists(":Implant_acquireSignal.WaveExamplesButton_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_acquireSignal.WaveExamplesButton_Button").buttonText, "Waveform Examples")
        test.compare(waitForObjectExists(":Implant_acquireSignal.SignalStrengthIndicator_SignalStrengthIndicator").visible, True)
        
        buttonVisible = False
        
        while(not buttonVisible):
            adjustSPButton = waitForObjectExists(":Implant_acquireSignal.AutoSearchPressureButton_Button").visible
            if (adjustSPButton):
                test.compare(str(waitForObjectExists(":Implant_acquireSignal.AutoSearchPressureButton_Button").buttonText), "Adjust Search Pressure")
                test.compare(waitForObjectExists(":Implant_acquireSignal.AutoSearchPressureButton_Button").enabled, True)
                buttonVisible = True
                break
            else:
                continue

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
