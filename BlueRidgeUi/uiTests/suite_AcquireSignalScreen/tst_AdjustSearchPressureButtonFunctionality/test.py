"""
 @file   tst_AdjustSearchPressureButtonFunctionality
 
 Description:

 Test the functionality of the Adjust Search Presure button in the screen
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.RightButton_Image"))
        snooze(5)
        
        # Verify Pressure Searchbar before adjusting search pressure
        test.compare(waitForObjectExists(":Implant_acquireSignal.PressureSearchBar_PressureSearchBar").visible, True)
        test.compare(waitForObjectExists(":Implant_acquireSignal.PressureSearchBar_PressureSearchBar").sliderValue, 0)
        test.compare(waitForObjectExists(":Implant_acquireSignal.Flicker_PressureSearchBar_Flickable").interactive, True)
        test.compare(waitForObjectExists(":Implant_acquireSignal.Flicker_PressureSearchBar_Flickable").contentY, 0)
        test.compare(waitForObjectExists(":Implant_acquireSignal.Slider_Flicker_PressureSearchBar_PressureSearchSlider").flashlightVisible, False)
        
        # Wait for adjust button to be visible and then click button
        buttonVisible = waitForObjectExists(":Implant_acquireSignal.AutoSearchPressureButton_Button").visible
        while(not buttonVisible):
            buttonVisible = waitForObjectExists(":Implant_acquireSignal.AutoSearchPressureButton_Button").visible
        
        mouseClick(waitForObject(":Implant_acquireSignal.AutoSearchPressureButton_Button"))
        snooze(9)
        
        # Verify Pressure Searchbar after adjusting search pressure
        test.compare(waitForObjectExists(":Implant_acquireSignal.PressureSearchBar_PressureSearchBar").visible, True)
        test.compare(waitForObjectExists(":Implant_acquireSignal.PressureSearchBar_PressureSearchBar").sliderValue, 37)
        test.compare(waitForObjectExists(":Implant_acquireSignal.Flicker_PressureSearchBar_Flickable").interactive, False)
        test.compare(waitForObjectExists(":Implant_acquireSignal.Flicker_PressureSearchBar_Flickable").contentY, 61.272)
        test.compare(waitForObjectExists(":Implant_acquireSignal.Slider_Flicker_PressureSearchBar_PressureSearchSlider").flashlightVisible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
