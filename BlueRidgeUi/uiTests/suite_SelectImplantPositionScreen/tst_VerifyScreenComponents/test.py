"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Verify the components displayed on the screen
        test.compare(waitForObjectExists(":Implant_selectSensor.Units_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_selectSensor.Units_Text").text), "mmHg")
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").iconImage.path), "/images/plusIcon")
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").iconImage.path), "/images/minusIcon")
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").visible, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.Flicker_PressureSearchBar_Flickable").interactive, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.PressureSearchBar_PressureSearchBar").visible, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.Back_Button").visible, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.WaveExamplesButton_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.WaveExamplesButton_Button").buttonText, "Waveform Examples")
        test.compare(waitForObjectExists(":Implant_selectSensor.SignalStrengthIndicator_SignalStrengthIndicator").visible, True)
        test.compare(waitForObjectExists(":Implant_selectSensor_SelectSensorPosition").visible, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.InstructionText_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_selectSensor.InstructionText_Text").text), "Select Implanted Sensor Position:")
        test.compare(waitForObjectExists(":Implant_selectSensor.BodyImg_Image").visible, True)
        test.compare(str(waitForObjectExists(":Implant_selectSensor.BodyImg_Image").source.path), "/images/body")
        test.compare(waitForObjectExists(":Implant_selectSensor.RightButton_Image").visible, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.LeftButton_Image").visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
