"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Verify the components displayed on the overlay
        test.compare(waitForObjectExists(":MainWindow.PaMeanOverlay_PaMeanEntryOverlay").visible, True)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.Units_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.Units_Text").text), "mmHg")
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").iconImage.path), "/images/plusIcon")
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").enabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").iconImage.path), "/images/minusIcon")
        test.compare(waitForObjectExists(":PaMeanOverlay.PressureSearchBar_PressureSearchBar").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.Flicker_PressureSearchBar_Flickable").interactive, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.Slider_Flicker_PressureSearchBar_PressureSearchSlider").visible, False)
                
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticSignalStrength_SignalStrengthIndicator").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticSignalStrength_SignalStrengthIndicator").isStatic, True)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.DiscardButton_Button").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.DiscardButton_Button").enabled, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.DiscardButton_Button").buttonText), "Discard")
        test.compare(waitForObjectExists(":PaMeanOverlay.DiscardButton_Button").buttonIconVisible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.DiscardButton_Button").buttonIconSource.path), "/images/trashIcon")
        
        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").enabled, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").buttonText), "Save")
        
        test.compare(waitForObjectExists(":PaMeanOverlay.CloseButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.CloseButton_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.CloseButton_IconButton").iconImage.path), "/images/closeIcon")
        
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_SystolicDiastolicText").visible, False)        
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Systolic_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Systolic_Text").text), "")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Separator_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Separator_Text").text), "/")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Diastolic_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Diastolic_Text").text), "")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Label_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_MeanText").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Label_Text").text), "Mean")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Label_Text").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_RightParen_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Mean_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Mean_Text").text), "")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_LeftParen_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_HeartRateText").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_Label_Text").text), "HR")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_HeartRate_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_HeartRate_Text").text), "78")      
        
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanLabel_Text").text), "PA CATHETER MEAN")
        
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").text), "")
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").isEditable, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").enabled, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").displayText), "")
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").enteredValue), "")
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").maxValue, 150)
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").minValue, 0)
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").maximumLength, 3)
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").inputMethodHints, 65536)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").text), "Enter a value between 0 and 150")

        test.compare(waitForObjectExists(":MainWindow.NumericKeyboard_CustomNumericKeyboard").visible, True)
        test.compare(waitForObjectExists(":MainWindow.NumericKeyboard_CustomNumericKeyboard").enabled, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
