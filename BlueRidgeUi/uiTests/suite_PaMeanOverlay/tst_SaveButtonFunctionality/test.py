"""
 @file   tst_SaveButtonFunctionality

 Description:

 Test the functionality of the Save button in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Verify Save button before entering mean value
        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").enabled, False)

        # Enter PA Mean value
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        snooze(2)
        
        # Verify Save button after entering mean value
        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").enabled, True)
            
        # Verify PA Mean overlay is visible above the Calibrate Sensor screen
        test.compare(waitForObjectExists(":MainWindow.PaMeanOverlay_PaMeanEntryOverlay").visible, True)

        # Press the Discard button
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))
                
        # Verify Record Pressures screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.PaMeanOverlay_PaMeanEntryOverlay").visible, False)
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Implant_recordPressures")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
       
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
