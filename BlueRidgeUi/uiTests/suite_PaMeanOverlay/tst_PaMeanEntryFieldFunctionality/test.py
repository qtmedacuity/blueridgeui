"""
 @file   tst_PaMeanEntryFieldFunctionality

 Description:

 Test the functionality of the entry field in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)
        
        # Verify PA Mean entry only accepts digit values
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").text), "")
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").displayText), "")
        
        type(waitForObject(":MainWindow.PaMeanOverlay_PaMeanEntryOverlay"), "abcabc")
        type(waitForObject(":MainWindow.PaMeanOverlay_PaMeanEntryOverlay"), "<Tab>")

        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").text), "")
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").displayText), "")
        
        # Verify PA Mean entry does not accept values larger than 150
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").invalid, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").visible, False)        
        test.compare(str(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").text), "Enter a value between 0 and 150")
         
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.1_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").invalid, True) 
        test.compare(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").text), "Enter a value between 0 and 150")

        # Delete input entered
        type(waitForObject(":MainWindow.PaMeanOverlay_PaMeanEntryOverlay"), "<Tab>")
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        
        # Verify PA Mean entry does not accept values less than 0
        test.compare(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").visible, False)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").text), "Enter a value between 0 and 150")

        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        test.compare(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.ValidationMessage_Text").text), "Enter a value between 0 and 150")
        
        # Verify PA Mean entry accepts input
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").text), "")
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").displayText), "")
        
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Label_Text").text), "Systolic/Diastolic")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Label_Text").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Systolic_Text").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Separator_Text").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Diastolic_Text").visible, False)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Label_Text").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_RightParen_Text").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Mean_Text").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_LeftParen_Text").visible, False)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_Label_Text").visible, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_HeartRate_Text").visible, False)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").enabled, False)
        
        type(waitForObject(":MainWindow.PaMeanOverlay_PaMeanEntryOverlay"), "<Tab>")
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        snooze(2)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").text), "30")
        test.compare(str(waitForObjectExists(":PaMeanOverlay.PaMeanText_NumericTextField").displayText), "30")

        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Label_Text").text), "Systolic/Diastolic")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Label_Text").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Systolic_Text").text), "44")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Separator_Text").text), "/")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorSystolicDiastolicText_Diastolic_Text").text), "32")
        
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Label_Text").text), "Mean")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_Mean_Text").text), "36")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorMeanText_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorMeanText_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_Label_Text").text), "HR")
        test.compare(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_HeartRate_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SensorHeartRate_HeartRate_Text").text), "78")

        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.SaveButton_Button").enabled, True)

       
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
