"""
 @file   tst_WaveformExamplesButtonFunctionality

 Description:

 Test the functionality of the Waveform Examples button on the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate0_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button")) 
        
        # Verify waveform examples are not displayed
        test.compare(waitForObjectExists(":Implant_recordPressures.WaveExamplesButton_Button").visible, True)
        test.compare(waitForObjectExists(":MainWindow.WaveExamples_WaveformExamples").visible, False)
        test.compare(waitForObjectExists(":MainWindow.WaveExamples_WaveformExamples").implicitWidth, 0)
        
        # Press waveform examples button
        mouseClick(waitForObject(":Implant_recordPressures.WaveExamplesButton_Button"))
        snooze(0.5)
        
        # Verify Waveform examples are displayed
        test.compare(waitForObjectExists(":Implant_recordPressures.WaveExamplesButton_Button").visible, False)
        test.compare(waitForObjectExists(":MainWindow.WaveExamples_WaveformExamples").visible, True)
        test.compare(waitForObjectExists(":MainWindow.WaveExamples_WaveformExamples").implicitWidth, 300)
        test.compare(waitForObjectExists(":MainWindow.WaveformTitle_Text").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.WaveformTitle_Text").text), "WAVEFORM EXAMPLES")
        test.compare(waitForObjectExists(":MainWindow.HideButton_Button").visible, True)
        test.compare(waitForObjectExists(":MainWindow.HideButton_Button").buttonText, "hide")
        test.compare(waitForObjectExists(":MainWindow.ExamplesImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.ExamplesImage_Image").source.path), "/images/waveformExamples")
        
        # Press the hide button in the Waveform examples 
        mouseClick(waitForObject(":MainWindow.HideButton_Button"))
        snooze(0.5)
        
        # Verify the waveform examples are not displayed
        test.compare(waitForObjectExists(":Implant_recordPressures.WaveExamplesButton_Button").visible, True)
        test.compare(waitForObjectExists(":MainWindow.WaveExamples_WaveformExamples").implicitWidth, 0)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
