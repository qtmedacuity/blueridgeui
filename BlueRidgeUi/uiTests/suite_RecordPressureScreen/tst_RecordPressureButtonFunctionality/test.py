"""
 @file   tst_RecordPressuresButtonFunctionality

 Description:

 Test the functionality of the Record Pressures button in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.RightButton_Image"))
        
        # Navigate to Calibrate Sensor screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(not enabled):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.1_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
            
        # Verify Record Pressures screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Implant_recordPressures")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        test.compare(waitForObjectExists(":MainWindow.RecordingConfirmation_RecordConfirmationPopup").visible, False)
        
        # Verify Record Pressure button before any recording are saved
        snooze(10)
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordButton_RecordButton").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.InnerButton_Rectangle").color.name), "#004f71")
        test.compare(waitForObjectExists(":Implant_recordPressures.checkIcon_Image").visible, False)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.checkIcon_Image").source.path), "/images/checkmark")
        test.compare(waitForObjectExists(":Implant_recordPressures.ButtonText_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.ButtonText_Text").text), "Record Pressures")

        # Press Record Pressures button on screen
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        
        # Verify Record button when recording 
        test.compare(str(waitForObjectExists(":Implant_recordPressures.InnerButton_Rectangle").color.name), "#009cde")
        test.compare(waitForObjectExists(":Implant_recordPressures.checkIcon_Image").visible, False)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.checkIcon_Image").source.path), "/images/checkmark")
        test.compare(waitForObjectExists(":Implant_recordPressures.ButtonText_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.ButtonText_Text").text), "Wait")
        snooze(2)

        # Verify Record Confirmation Overlay is displayed above Record Pressures Screen
        test.compare(waitForObjectExists(":MainWindow.RecordingConfirmation_RecordConfirmationPopup").visible, True)
        test.compare(waitForObjectExists(":MainWindow.RecordingConfirmation_RecordConfirmationPopup").z, 2)
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").visible, True)
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").z, 0)
        test.verify(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").z < waitForObjectExists(":MainWindow.RecordingConfirmation_RecordConfirmationPopup").z)
        
        # Save recording
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        
        # Record and save 2 additional cards
        for x in range(2):
            mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
            snooze(5)
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
            mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
            snooze(3)
            
        # Verify record button after saving 3 recordings
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordButton_RecordButton").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.InnerButton_Rectangle").color.name), "#000000")
        test.compare(waitForObjectExists(":Implant_recordPressures.checkIcon_Image").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.checkIcon_Image").source.path), "/images/checkmark")
        test.compare(waitForObjectExists(":Implant_recordPressures.ButtonText_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.ButtonText_Text").text), "Record Pressures")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
