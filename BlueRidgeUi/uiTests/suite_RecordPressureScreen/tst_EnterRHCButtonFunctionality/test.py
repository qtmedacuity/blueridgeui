"""
 @file   tst_EnterRHCButtonFunctionality

 Description:

 Test the functionality of the Enter RHC button in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.RightButton_Image"))
        
        # Navigate to Calibrate Sensor screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.7_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
        snooze(5)
        
        # Verify Record Pressures screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Implant_recordPressures")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        
        # Verify button before entering RHC values
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").enabled, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonText), "Enter RHC")
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonIconVisible, False)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonIconSource.path), "/images/checkmark")
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonColor.name), "#004f71")
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").borderColor.name), "#009cde")
        
        # Press Enter RHC button on screen
        mouseClick(waitForObject(":Implant_recordPressures.EnterRHC_Button"))
        snooze(2)
        
        # Verify Enter RHC screen is displayed
        test.compare(waitForObjectExists(":MainWindow.RHCEntry_RhcEntryOverlay").visible, True)
        test.compare(waitForObjectExists(":MainWindow.RHCEntry_RhcEntryOverlay").z, 2)
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").visible, True)
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").z, 0)
        test.verify(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").z < waitForObjectExists(":MainWindow.RHCEntry_RhcEntryOverlay").z)

        # Enter RHC values
        values = ["25", "30", "20", "40", "30", "35", "20"]
        for val in values:
            for c in val:
                mouseClick(":NumericKeyboard.{0}_Key_Button".format(c))
            mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        # Save the RHC values
        mouseClick(waitForObject(":RHCEntry.SaveButton_Button"))
        
        # Verify button after entering RHC values        
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").enabled, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonIconVisible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonText), "Enter RHC")
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonIconSource.path), "/images/checkmark")
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonColor.name), "#000000")
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").borderColor.name), "#009cde")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
