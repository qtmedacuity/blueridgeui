"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button")) 
        snooze(5)
        
        # Verify the components displayed on the screen
        test.compare(waitForObjectExists(":Implant_recordPressures.Back_Button").visible, False)
        
        test.compare(waitForObjectExists(":Implant_recordPressures.Units_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.Units_Text").text), "mmHg")
        test.compare(waitForObjectExists(":Implant_recordPressures.ZoomIn_IconButton").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.ZoomOut_IconButton").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.PressureSearchBar_PressureSearchBar").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.Flicker_PressureSearchBar_Flickable").interactive, False)
        test.compare(waitForObjectExists(":Implant_recordPressures.ActiveWaveChart_BaseWaveChart").visible, True)
        
        test.compare(waitForObjectExists(":Implant_recordPressures.WaveExamplesButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.WaveExamplesButton_Button").buttonText), "Waveform Examples")
        test.compare(waitForObjectExists(":Implant_recordPressures.SignalStrengthIndicator_SignalStrengthIndicator").visible, True)
       
        test.compare(waitForObjectExists(":Implant_recordPressures.SystolicDiastolicText_SystolicDiastolicText").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.SystolicDiastolicText_Systolic_Text").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.SystolicDiastolicText_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.SystolicDiastolicText_Separator_Text").text), "/")
        test.compare(waitForObjectExists(":Implant_recordPressures.SystolicDiastolicText_Diastolic_Text").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.SystolicDiastolicText_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.SystolicDiastolicText_Label_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":Implant_recordPressures.MeanText_MeanText").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.MeanText_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.MeanText_RightParen_Text").text), "(")
        test.compare(waitForObjectExists(":Implant_recordPressures.MeanText_Mean_Text").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.MeanText_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.MeanText_LeftParen_Text").text), ")")
        test.compare(waitForObjectExists(":Implant_recordPressures.MeanText_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.MeanText_Label_Text").text), "Mean")
        
        test.compare(waitForObjectExists(":Implant_recordPressures.HeartRateText_HeartRateText").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.HeartRateText_HeartRate_Text").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.HeartRateText_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.HeartRateText_Label_Text").text), "HR")
        
        test.compare(waitForObjectExists(":Implant_recordPressures.SensorSelectionImage_Image").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.SensorSelectionImage_Image").source.path), "/images/bodyleft")
        
        test.compare(waitForObjectExists(":Implant_recordPressures.HelpButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.HelpButton_Button").buttonText), "Help")
        
        test.compare(waitForObjectExists(":Implant_recordPressures.RecalibrateButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.RecalibrateButton_Button").buttonText), "Recalibrate Sensor")
        
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.RecordedPressuresLabel_Text").text), "RECORDED PRESSURES:")
        
        test.compare(waitForObjectExists(":Implant_recordPressures.CalibrateCO_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.CalibrateCO_Button").enabled, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.CalibrateCO_Button").buttonText), "Calibrate CO")
        test.compare(waitForObjectExists(":Implant_recordPressures.CalibrateCO_Button").buttonIconVisible, False)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.CalibrateCO_Button").buttonIconSource.path), "/images/checkmark")
        test.compare(str(waitForObjectExists(":Implant_recordPressures.CalibrateCO_Button").buttonColor.name), "#004f71")
        test.compare(str(waitForObjectExists(":Implant_recordPressures.CalibrateCO_Button").borderColor.name), "#009cde")
        
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").enabled, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonText), "Enter RHC")
        test.compare(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonIconVisible, False)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonIconSource.path), "/images/checkmark")
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").buttonColor.name), "#004f71")
        test.compare(str(waitForObjectExists(":Implant_recordPressures.EnterRHC_Button").borderColor.name), "#009cde")

        test.compare(waitForObjectExists(":Implant_recordPressures.ReviewButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":Implant_recordPressures.ReviewButton_Button").buttonText), "Review")
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordButton_RecordButton").visible, True)
        
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard1_RecordPressureBlankCard").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard2_RecordPressureBlankCard").visible, True)
        test.compare(waitForObjectExists(":Implant_recordPressures.BlankCard3_RecordPressureBlankCard").visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
