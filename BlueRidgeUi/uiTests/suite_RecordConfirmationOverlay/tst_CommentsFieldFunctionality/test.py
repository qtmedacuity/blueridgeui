"""
 @file   tst_CommentsFieldFunctionality

 Description:

 Test the functionality of the comment field in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      

        # Press Record Pressures button on screen
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        
        # Verify text field accepts input
        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").text), "")
        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").length, 0)

        mouseClick(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"))
        mouseClick(waitForObject(":keyboard.a_BaseKey"))
        mouseClick(waitForObject(":keyboard. _BaseKey"))
        mouseClick(waitForObject(":keyboard.c_BaseKey"))
        mouseClick(waitForObject(":keyboard.o_BaseKey"))
        mouseClick(waitForObject(":keyboard.m_BaseKey"))
        mouseClick(waitForObject(":keyboard.m_BaseKey"))
        mouseClick(waitForObject(":keyboard.e_BaseKey"))
        mouseClick(waitForObject(":keyboard.n_BaseKey"))
        mouseClick(waitForObject(":keyboard.t_BaseKey"))

        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").text), "A comment")
        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").length, 9)

        #Verify text field does not accept more than max length

        mouseClick(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), 325, 28, Qt.LeftButton)
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        type(waitForObject(":RecordingConfirmation.NotesEntry_TextArea"), "Testing ")
        mouseClick(waitForObject(":keyboard.\n_EnterKey"))

        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").text), "A commentTesting Testing Testing Testing Testing Testing Testing Testing Testing Testing Testing Tes")
        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").length, 100)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
