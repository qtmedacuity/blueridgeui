"""
 @file   tst_PositionIndicatorButtonsFunctionality

 Description:

 Test the functionality of the Position Indicator buttons in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)
            
        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
        
        # Press Record Pressures button on screen
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)

        # Verify zero position button is selected by default
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").checked, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").source.path), "/images/0DegreeSelected")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").source.path), "/images/30DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").source.path), "/images/45DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").source.path), "/images/90DegreeClear")
        
        # Select 30 position button 
        mouseClick(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton"))
        
        # Verify thirty position button is selected by default
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").source.path), "/images/0DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").checked, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").source.path), "/images/30DegreeSelected")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").source.path), "/images/45DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").source.path), "/images/90DegreeClear")
        
        # Select 45 position button 
        mouseClick(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton"))
        
        # Verify thirty position button is selected by default
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").source.path), "/images/0DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").source.path), "/images/30DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").checked, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").source.path), "/images/45DegreeSelected")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").source.path), "/images/90DegreeClear")
        
        # Select 90 position button 
        mouseClick(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton"))
        
        # Verify thirty position button is selected by default
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").source.path), "/images/0DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").source.path), "/images/30DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").source.path), "/images/45DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").checked, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").source.path), "/images/90DegreeSelected")
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
