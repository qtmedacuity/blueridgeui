"""
 @file   tst_ReferenceMeanFieldFunctionality

 Description:

 Test the functionality of the entry field in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      

        # Press Record Pressures button on screen
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)

        # Enter Systolic and Diastolic pressures and move to mean field
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        # Verify numeric entry field accept only digit inputs        
        type(waitForObject(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField"), "bc")
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").enteredValue), "")

        # Verify entry field only accepts two digit values
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.7_Key_Button"))
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").text), "35")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").enteredValue), "35")
        
        # Delete input
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        
        # Verify value must be between Systolic and Diastolic pressure   
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueLabel_Text").color.name), "#ffd100")
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").state), "invalid")
        test.compare(waitForObjectExists(":RecordingConfirmation.ValidationMessage_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ValidationMessage_Text").text), "Mean must be between Systolic and Diastolic values")
        
        # Delete input
        mouseClick(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField"))
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        
        # Verify entry field accepts the input
        mouseClick(waitForObject(":NumericKeyboard.3_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))

        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").text), "35")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").enteredValue), "35")
        test.compare(waitForObjectExists(":RecordingConfirmation.ValidationMessage_Text").visible, False)
        
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Verify Record Pressures screen is visible and current Item in Stackview
        test.compare(waitForObjectExists(":MainWindow.Implant_recordPressures_ImplantRoot").visible, True)
        test.compare(str(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.objectName), "Implant_recordPressures")
        test.compare(waitForObjectExists(":MainWindow.MainStackView_StackView").currentItem.visible, True)
        test.compare(waitForObjectExists(":MainWindow.RecordingConfirmation_RecordConfirmationPopup").visible, False)
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
