"""
 @file   tst_DiscardButtonFunctionalityExistingRecording

 Description:

 Test the functionality of the Discard button in the screen on a existing recording

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
        
        # Record and save pressure
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        mouseClick(waitForObject(":RecordingConfirmation.SaveButton_Button"))
        snooze(3)
        
        # Verify recording card has been added
        test.compare(waitForObjectExists(":Implant_recordPressures.RecordedPressuresList_ListView").count, 1)
        test.compare(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard").visible, True)
        
        # Delete recording via confirmation overlay Discard button
        mouseClick(waitForObjectExists(":RecordedPressuresList.RecordedPressure_0_RecordPressureCard"))
        mouseClick(waitForObject(":RecordingConfirmation.DiscardButton_Button"))
        
        # Verify Delete Recording Popup is displayed
        test.compare(waitForObjectExists(":MainWindow.RecordingConfirmation_RecordConfirmationPopup").visible, True)
        test.compare(waitForObjectExists(":MainWindow.RecordingConfirmation_RecordConfirmationPopup").z, 2)
        test.compare(waitForObjectExists(":Popup_PopupItem").visible, True)
        test.compare(waitForObjectExists(":MainWindow_Overlay").z, 1000001.0)
        test.compare(waitForObjectExists(":MainWindow_Overlay").visible, True)
        test.verify(waitForObjectExists(":MainWindow_Overlay").z > waitForObjectExists(":MainWindow.RecordingConfirmation_RecordConfirmationPopup").z)
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
