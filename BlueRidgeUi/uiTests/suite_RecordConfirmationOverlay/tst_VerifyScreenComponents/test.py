"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed screen components
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""
from datetime import datetime

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
        
        # Press Record Pressures button on screen
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)

        # Verify overlay components

        test.compare(waitForObjectExists(":MainWindow.NumericKeyboard_CustomNumericKeyboard").visible, True)
        test.compare(waitForObjectExists(":MainWindow.NumericKeyboard_CustomNumericKeyboard").enabled, True)

        test.compare(waitForObjectExists(":MainWindow.RecordingConfirmation_RecordConfirmationPopup").visible, True)
         
        test.compare(waitForObjectExists(":RecordingConfirmation.CloseButton_IconButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.CloseButton_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.CloseButton_IconButton").iconImage.path), "/images/closeIcon")
         
        test.compare(waitForObjectExists(":RecordingConfirmation.Units_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.Units_Text").text), "mmHg")
         
        test.compare(waitForObjectExists(":RecordingConfirmation.ZoomIn_IconButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.ZoomIn_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ZoomIn_IconButton").iconImage.path), "/images/plusIcon")
         
        test.compare(waitForObjectExists(":RecordingConfirmation.ZoomOut_IconButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.ZoomOut_IconButton").enabled, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ZoomOut_IconButton").iconImage.path), "/images/minusIcon")
         
        test.compare(waitForObjectExists(":RecordingConfirmation.PressureSearchBar_PressureSearchBar").visible, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.Slider_Flicker_PressureSearchBar_PressureSearchSlider").visible, False)
         
        test.compare(waitForObjectExists(":RecordingConfirmation.StaticWaveChart_StaticWaveChart").visible, True)
         
        test.compare(waitForObjectExists(":RecordingConfirmation.StaticSignalStrength_SignalStrengthIndicator").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.StaticSignalStrength_SignalStrengthIndicator").isStatic, True)
         
        test.compare(waitForObjectExists(":RecordingConfirmation.DiscardButton_Button").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.DiscardButton_Button").enabled, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.DiscardButton_Button").buttonText), "Discard")
        test.compare(str(waitForObjectExists(":RecordingConfirmation.DiscardButton_Button").buttonIconSource.path), "/images/trashIcon")
        test.compare(waitForObjectExists(":RecordingConfirmation.DiscardButton_Button").buttonIconVisible, True)
         
        test.compare(waitForObjectExists(":RecordingConfirmation.SaveButton_Button").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.SaveButton_Button").buttonIconVisible, False)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SaveButton_Button").buttonText), "Save")
        test.compare(waitForObjectExists(":RecordingConfirmation.SaveButton_Button").enabled, True)
        
        test.compare(waitForObjectExists(":RecordingConfirmation.TimeText_Text").visible, True)        
        timeValue = datetime.strptime(str(waitForObjectExists(":RecordingConfirmation.TimeText_Text").text), "%I:%M:%S %p")        
        test.compare(timeValue.strftime("%I:%M %p"),  datetime.now().strftime("%I:%M %p"))        
        
        test.compare(waitForObjectExists(":RecordingConfirmation.DateText_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.DateText_Text").text), datetime.now().strftime("%d %b %Y"))
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorLabel_Text").text), "SENSOR")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorSystolicDiastolic_SystolicDiastolicText").visible, True)
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorSystolicDiastolic_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorSystolicDiastolic_Systolic_Text").text), "38")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorSystolicDiastolic_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorSystolicDiastolic_Separator_Text").text), "/")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorSystolicDiastolic_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorSystolicDiastolic_Diastolic_Text").text), "19")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorSystolicDiastolic_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorSystolicDiastolic_Label_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorMean_MeanText").visible, True)
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorMean_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorMean_RightParen_Text").text), "(")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorMean_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorMean_Mean_Text").text), "26")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorMean_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorMean_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorMean_Label_Text").visible, False)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorMean_Label_Text").text), "Mean")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorHeartRate_HeartRateText").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorHeartRate_HeartRate_Text").text), "78")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.SensorHeartRate_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.SensorHeartRate_Label_Text").text), "HR")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceLabel_Text").text), "REFERENCE")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").minValue, 0)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").isEditable, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").inputMethodHints, 65536)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").enabled, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").enteredValue), "")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSeparator_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceSeparator_Text").text), "/")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").minValue, 0)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").isEditable, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").inputMethodHints, 65536)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").enteredValue), "")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceDiastolic_NumericTextField").enabled, True)
        
        test.compare(waitForObjectExists(":RecordingConfirmation.RefSystolicDiastolicLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.RefSystolicDiastolicLabel_Text").text), "Systolic/Diastolic")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_RightParen_Text").text), "(")

        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").minValue, 0)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").length, 0)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").isEditable, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").inputMethodHints, 65536)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").enteredValue), "")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueTextField_NumericTextField").enabled, True)
        
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_LeftParen_Text").text), ")")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceMeanData_ValueLabel_Text").text), "mean")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.ValidationMessage_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ValidationMessage_Text").text), "")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.NotesLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.NotesLabel_Text").text), "COMMENT")
        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").text), "")
        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").length, 0)
        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").enabled, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.NotesEntry_TextArea").maxNotesLength, 100)

        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").checkable, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").checked, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_RadioButton").enabled, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.0PositionButton_Image").source.path), "/images/0DegreeSelected")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").checkable, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_RadioButton").enabled, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.30PositionButton_Image").source.path), "/images/30DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").enabled, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_RadioButton").checkable, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.45PositionButton_Image").source.path), "/images/45DegreeClear")
        
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").visible, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").enabled, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").checkable, True)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_RadioButton").checked, False)
        test.compare(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").visible, True)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.90PositionButton_Image").source.path), "/images/90DegreeClear")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
