"""
 @file   tst_RefereneSystolicFieldFunctionality

 Description:

 Test the functionality of the entry field in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      

        # Press Record Pressures button on screen
        snooze(10)
        mouseClick(waitForObject(":Implant_recordPressures.RecordButton_RecordButton"))
        snooze(5)
        
        # Verify numeric entry field accept only digit inputs
        type(waitForObject(":RecordingConfirmation.ReferenceSystolic_NumericTextField"), "ab")
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").text), "")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").enteredValue), "")

        # Verify entry field only accepts two digit values
        mouseClick(waitForObject(":NumericKeyboard.1_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").text), "12")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").enteredValue), "12")
        
        # Delete input
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Delete_Key_Button"))
        
        # Verify entry field accepts the input
        mouseClick(waitForObject(":NumericKeyboard.4_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))

        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").text), "40")
        test.compare(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":RecordingConfirmation.ReferenceSystolic_NumericTextField").enteredValue), "40")
       
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
