"""
 @file   TestProcessesScript.py

 Description:

 Run the pressure pump and cmemTestServer as process for the application to connect.

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""
import os
import subprocess
import shlex
import shutil 

class TestProcesses:
    
    def __init__(self):
        self.pressurePumpProc = None
        self.testServerProc = None
        
    def runPressurePumpProc(self, arg="SampleWaveform_IncrementalStrengthSignal.xml"):
        cwd = os.getcwd()
        os.chdir("../../../../../blueridgeapi/PressurePump")
        proc = shlex.split("./pressure_pump " + arg)
        self.pressurePumpProc = subprocess.Popen(proc, stdout=subprocess.PIPE)
        os.chdir(cwd)
        
    def runTestServerProc(self, args = "", recordingFile="valid_file.xml"):
        cwd = os.getcwd()
        # Copy recording xml to recording path /tmp
        os.chdir("../../../../../blueridgeapi/PressurePump")
        shutil.copy(os.path.join(os.getcwd(), recordingFile), "/tmp/valid_file.xml")
        os.chdir(cwd)
        
        #start test server proc
        os.chdir("../../../../../blueridgeapi/cmemTestServer/Source")
        proc = shlex.split("./cmemTestServer --clear-output-json  1 " + args)
        self.testServerProc = subprocess.Popen(proc, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        os.chdir(cwd)
        snooze(2)
    
    def stopTestServerProc(self):
        if self.testServerProc != None and self.testServerProc.poll() == None:
            self.testServerProc.terminate()
        snooze(2)
        
    def __del__(self):
        if self.pressurePumpProc != None and self.pressurePumpProc.poll() == None:
            self.testServerProc.terminate()
        if self.testServerProc != None and self.testServerProc.poll() == None:
            self.testServerProc.terminate()
        if self.pressurePumpProc.poll() != None:
            "Pressure Pump Proc terminated"
        if self.testServerProc.poll() != None:
            "Test Server Proc terminated"