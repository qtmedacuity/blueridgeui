"""
 @file   tst_StaticWaveChartScrollingFunctionality

 Description:

 Test the static wave chart scrolling functionality

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))

        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
     
        # Verify scrolling enable for y axis and axes display limits
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").scrollType, 2)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMin, 0)

        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMax, 100)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMin, 0)

        # Scroll up on the y axis
        mouseDrag(waitForObject(":PaMeanOverlay.StaticWaveChart_StaticWaveChart"), 0, 175, 0, -140, Qt.NoModifier, Qt.LeftButton)
        
        # Verify the axes display limits
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMax, 70)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMin, -30)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMin, 0)
        
        # Scroll down on the y axis
        mouseDrag(waitForObject(":PaMeanOverlay.StaticWaveChart_StaticWaveChart"), 0, 15, 0, 184, Qt.NoModifier, Qt.LeftButton)
        
        # Verify the axes display limits
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMin, 0)
        test.compare(int(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMax), 110)
        test.compare(int(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMin), 10)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
