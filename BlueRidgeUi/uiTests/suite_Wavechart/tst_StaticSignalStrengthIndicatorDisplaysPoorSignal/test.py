"""
 @file   tst_StaticSignalStrengthIndicatorDisplaysPoorSignal

 Description:

 Test the static signal strength indicator displaying of a poor signal strength

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc("SampleWaveform_WeakSignalStrength.xml")
        ts.runTestServerProc("PatientFile.txt", "valid_file_poor.xml")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))

        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
        
        # Verify Signal strength indicator for a poor signal strength (< 40)
        signalStrength = 25
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticSignalStrength_SignalStrengthIndicator").visible, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticSignalStrength_SignalStrengthIndicator").isStatic, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticSignalStrength_SignalStrengthIndicator").staticSignalStrength, signalStrength)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticSignalStrength_SignalStrengthIndicator").signalStrength, signalStrength)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.StaticSignalStrength_SignalStrengthIndicator").state), "")      
        test.compare(str(waitForObjectExists(":PaMeanOverlay.StaticSignalStrength_SignalStrengthIndicator").indicatorColor.name), "#e4002b")
        test.compare(str(waitForObjectExists(":PaMeanOverlay.StaticSignalStrength_SignalStrengthIndicator").backgroundColor.name), "#63666a")

        test.compare(waitForObjectExists(":PaMeanOverlay.SignalStrengthIndicator_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":PaMeanOverlay.SignalStrengthIndicator_Label_Text").text), str(signalStrength))
        test.compare(waitForObjectExists(":PaMeanOverlay.SignalStrengthIndicator_Label_Text").value, signalStrength)
        
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
