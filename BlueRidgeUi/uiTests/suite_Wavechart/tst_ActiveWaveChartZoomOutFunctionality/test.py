"""
 @file   tst_ActiveWaveChartZoomInFunctionality

 Description:

 Test the active wave chart zoom out functionality

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Verify zoom enable properties and axis display limits
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").visible, True)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInHorizontalEnabled, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInVerticalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").enabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutHorizontalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutVerticalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").enabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").visible, True)

        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMin, 0)

        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMax, 100)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMin, 0)
    
        # Zoom in chart
        mouseClick(waitForObject(":Implant_selectSensor.ZoomIn_IconButton"))
        mouseClick(waitForObject(":Implant_selectSensor.ZoomIn_IconButton"))
        # Zoom out chart
        mouseClick(waitForObject(":Implant_selectSensor.ZoomOut_IconButton"))
        
        # Verify zoom enable properties and axis display limits 
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInHorizontalEnabled, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInVerticalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").enabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutHorizontalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutVerticalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").enabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMin, 0)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMax, 50)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMin, 0)
        
        # Zoom out chart
        mouseClick(waitForObject(":Implant_selectSensor.ZoomOut_IconButton"))
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInHorizontalEnabled, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInVerticalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").enabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutHorizontalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutVerticalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").enabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").visible, True)

        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMin, 0)

        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMax, 100)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMin, 0)
        
        # Zoom out chart
        mouseClick(waitForObject(":Implant_selectSensor.ZoomOut_IconButton"))

        # Verify zoom enable properties and axis display limits
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInHorizontalEnabled, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInVerticalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").enabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutHorizontalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutVerticalEnabled, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").enabled, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMax, 150)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMin, 0)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMin, 0)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
