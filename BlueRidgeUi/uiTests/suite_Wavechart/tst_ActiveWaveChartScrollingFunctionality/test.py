"""
 @file   tst_ActiveWaveChartScrollingFunctionality

 Description:

 Test the active wave chart scrolling functionality

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))

        # Verify scrolling enable for y axis and axes display limits
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").visible, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").scrollType, 2)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMin, 0)

        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMax, 100)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMin, 0)

        # Scroll up on the y axis
        mouseDrag(waitForObject(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart"), 762, 173, -3, -139, Qt.NoModifier, Qt.LeftButton)
        
        # Verify the axes display limits
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMax, 70)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMin, -30)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMin, 0)
        
        # Scroll down on the y axis
        mouseDrag(waitForObject(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart"), 31, 15, -1, 139, Qt.NoModifier, Qt.LeftButton)
        
        # Verify the axes display limits
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMin, 0)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMax, 100.0)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMin, 0.0)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
