"""
 @file   tst_SignalStrengthIndicatorDisplaysGoodSignal

 Description:

 Test the signal strength indicator displaying of a good signal strength

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.
"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc("SampleWaveform_GoodSignalStrength.xml")
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Verify Signal strength indicator for a good signal strength (> 70)
        signalStrength = 95
        test.compare(str(waitForObjectExists(":SensorCheck.SignalStrengthIndicator_SignalStrengthIndicator").indicatorColor.name), "#00b140")
        test.compare(str(waitForObjectExists(":SensorCheck.SignalStrengthIndicator_SignalStrengthIndicator").backgroundColor.name), "#63666a")
        test.compare(waitForObjectExists(":SensorCheck.SignalStrengthIndicator_SignalStrengthIndicator").signalStrength, signalStrength)
        test.compare(str(waitForObjectExists(":SensorCheck.SignalStrengthIndicator_SignalStrengthIndicator").state), "good")
        test.compare(waitForObjectExists(":SensorCheck.SignalStrengthIndicator_SignalStrengthIndicator").visible, True)

        test.compare(waitForObjectExists(":SensorCheck.SignalStrengthIndicator_Label_Text").value, signalStrength)
        test.compare(str(waitForObjectExists(":SensorCheck.SignalStrengthIndicator_Label_Text").text), str(signalStrength))
        test.compare(waitForObjectExists(":SensorCheck.SignalStrengthIndicator_Label_Text").visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
