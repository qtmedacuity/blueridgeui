"""
 @file   tst_ActiveWaveChartAxis

 Description:

 Test the active wave chart and axes properties 

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Verify the axes on the chart
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").visible, True)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInHorizontalEnabled, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomInVerticalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").visible, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomIn_IconButton").enabled, True)
        
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").visible, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ZoomOut_IconButton").enabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutHorizontalEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").zoomOutVerticalEnabled, True)
        
        test.compare(str(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.axisColor.name), "#ffffff")
        test.compare(str(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.backgroundColor.name), "#000000")
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.displayMin, 0)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.gridEnabled, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.labelsEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.lineEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.lineWeight, 1)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.scrollOffset, 0)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.span, 30)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").xAxis.ticksEnabled, False)
        
        test.compare(str(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.axisColor.name), "#ffffff")
        test.compare(str(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.backgroundColor.name), "#000000")
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMax, 100)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.displayMin, 0)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.gridEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.labelsEnabled, True)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.lineEnabled, False)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.lineWeight, 1)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.scrollOffset, 0)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.span, 30)
        test.compare(waitForObjectExists(":Implant_selectSensor.ActiveWaveChart_BaseWaveChart").yAxis.ticksEnabled, False)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
