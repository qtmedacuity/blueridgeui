
"""
 @file   tst_PressureSearchBarDisplaysGoodSignal

 Description:

 Test the pressure search bar displaying of a good signal strength

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.
"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc("SampleWaveform_GoodSignalStrength.xml")
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Verify Signal strength indicator for a good signal strength (> 70)
        test.compare(waitForObjectExists(":Implant_selectSensor.PressureSearchBar_PressureSearchBar").visible, True)
        test.compare(str(waitForObjectExists(":Implant_selectSensor.PressureSearchBar_PressureSearchBar").statusColor.name), "#00b140")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))


    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
