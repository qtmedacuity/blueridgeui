
"""
 @file   tst_SignalStrengthIndicatorDisplaysWeakSignal

 Description:

 Test the signal strength indicator displaying of a weak signal strength

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.
"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc("SampleWaveform_WeakSignalStrength.xml")
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))

        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Verify Signal strength indicator for a weak signal strength (> 40, < 70)
        signalStrength = 55
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_SignalStrengthIndicator").indicatorColor.name), "#e77a2b")
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_SignalStrengthIndicator").backgroundColor.name), "#63666a")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_SignalStrengthIndicator").signalStrength, signalStrength)
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_SignalStrengthIndicator").state), "weak")
        test.compare(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_SignalStrengthIndicator").visible, True)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_SignalStrengthIndicator").isStatic, False)
 
        test.compare(str(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_Label_Text").text), str(signalStrength))
        test.compare(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_Label_Text").value, signalStrength)
        test.compare(waitForObjectExists(":Implant_calibrateSensor.SignalStrengthIndicator_Label_Text").visible, True)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
