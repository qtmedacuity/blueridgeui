"""
 @file   tst_StaticWaveChartZoomOutFunctionality

 Description:

 Test the active wave chart zoom out functionality

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))

        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean screen
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))
                
        # Verify zoom enable properties and axis display limits
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").visible, True)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomInHorizontalEnabled, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomInVerticalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").enabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomOutHorizontalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomOutVerticalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").enabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").visible, True)

        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMin, 0)

        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMax, 100)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMin, 0)
    
        # Zoom in chart
        mouseClick(waitForObject(":PaMeanOverlay.ZoomIn_IconButton"))
        mouseClick(waitForObject(":PaMeanOverlay.ZoomIn_IconButton"))
        # Zoom out chart
        mouseClick(waitForObject(":PaMeanOverlay.ZoomOut_IconButton"))
        
        # Verify zoom enable properties and axis display limits 
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomInHorizontalEnabled, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomInVerticalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").enabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomOutHorizontalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomOutVerticalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").enabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMin, 0)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMax, 50)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMin, 0)
        
        # Zoom out chart
        mouseClick(waitForObject(":PaMeanOverlay.ZoomOut_IconButton"))
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomInHorizontalEnabled, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomInVerticalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").enabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomOutHorizontalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomOutVerticalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").enabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").visible, True)

        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMin, 0)

        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMax, 100)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMin, 0)
        
        # Zoom out chart
        mouseClick(waitForObject(":PaMeanOverlay.ZoomOut_IconButton"))

        # Verify zoom enable properties and axis display limits
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomInHorizontalEnabled, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomInVerticalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").enabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomIn_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomOutHorizontalEnabled, True)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").zoomOutVerticalEnabled, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").enabled, False)
        test.compare(waitForObjectExists(":PaMeanOverlay.ZoomOut_IconButton").visible, True)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMax, 150)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").yAxis.displayMin, 0)
        
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMax, 10)
        test.compare(waitForObjectExists(":PaMeanOverlay.StaticWaveChart_StaticWaveChart").xAxis.displayMin, 0)

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
