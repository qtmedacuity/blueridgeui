"""
 @file   tst_VerifyScreenComponents
 
 Description:

 Test the displayed components of the overlay
 
 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""
from datetime import datetime

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate2_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
        
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      
        
        # Navigate to CO Entry Overlay
        mouseClick(waitForObject(":Implant_recordPressures.CalibrateCO_Button"))
        snooze(3)

        # Verify overlay components
        test.compare(waitForObjectExists(":MainWindow.CoEntryOverlay_CoEntryOverlay").visible, True)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.DateText_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.DateText_Text").text), datetime.now().strftime("%d %b %Y"))
        
        test.compare(waitForObjectExists(":CoEntryOverlay.TimeText_Text").visible, True)
        timeValue = datetime.strptime(str(waitForObjectExists(":CoEntryOverlay.TimeText_Text").text), "%I:%M:%S %p")        
        test.compare(timeValue.strftime("%I:%M %p"),  datetime.now().strftime("%I:%M %p"))        
        
        test.compare(waitForObjectExists(":CoEntryOverlay.Units_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.Units_Text").text), "mmHg")
        
        test.compare(waitForObjectExists(":CoEntryOverlay.ZoomIn_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.ZoomIn_IconButton").iconImage.path), "/images/plusIcon")
        test.compare(waitForObjectExists(":CoEntryOverlay.ZoomIn_IconButton").enabled, True)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.ZoomOut_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.ZoomOut_IconButton").iconImage.path), "/images/minusIcon")
        test.compare(waitForObjectExists(":CoEntryOverlay.ZoomOut_IconButton").enabled, True)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.PressureSearchBar_PressureSearchBar").visible, False)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.StaticWaveChart_StaticWaveChart").visible, True)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.CloseButton_IconButton").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.CloseButton_IconButton").iconImage.path), "/images/closeIcon")
        test.compare(waitForObjectExists(":CoEntryOverlay.CloseButton_IconButton").enabled, True)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.DiscardButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.DiscardButton_Button").buttonText), "Discard")
        test.compare(waitForObjectExists(":CoEntryOverlay.DiscardButton_Button").buttonIconVisible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.DiscardButton_Button").buttonIconSource.path), "/images/trashIcon")
        test.compare(waitForObjectExists(":CoEntryOverlay.DiscardButton_Button").enabled, True)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.SaveButton_Button").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.SaveButton_Button").buttonText), "Save")
        test.compare(waitForObjectExists(":CoEntryOverlay.SaveButton_Button").buttonIconVisible, False)
        test.compare(waitForObjectExists(":CoEntryOverlay.SaveButton_Button").enabled, False)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.StaticSignalStrength_SignalStrengthIndicator").visible, True)
        test.compare(waitForObjectExists(":CoEntryOverlay.StaticSignalStrength_SignalStrengthIndicator").staticSignalStrength, 98)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.StaticSignalStrength_SignalStrengthIndicator").state), "good")
        test.compare(waitForObjectExists(":CoEntryOverlay.StaticSignalStrength_SignalStrengthIndicator").signalStrength, 98)
        test.compare(waitForObjectExists(":CoEntryOverlay.StaticSignalStrength_SignalStrengthIndicator").isStatic, True)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.SensorHeartRate_HeartRateText").visible, True)
        test.compare(waitForObjectExists(":CoEntryOverlay.SensorHeartRate_HeartRate_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.SensorHeartRate_HeartRate_Text").text), "78")
        test.compare(waitForObjectExists(":CoEntryOverlay.SensorHeartRate_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.SensorHeartRate_Label_Text").text), "HR")
        
        test.compare(waitForObjectExists(":CoEntryOverlay.SensorMean_MeanText").visible, True)
        test.compare(waitForObjectExists(":CoEntryOverlay.SensorMean_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.SensorMean_Label_Text").text), "Mean")
        test.compare(waitForObjectExists(":CoEntryOverlay.SensorMean_LeftParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.SensorMean_LeftParen_Text").text), ")")
        test.compare(waitForObjectExists(":CoEntryOverlay.SensorMean_Mean_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.SensorMean_Mean_Text").text), "26")
        test.compare(waitForObjectExists(":CoEntryOverlay.SensorMean_RightParen_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.SensorMean_RightParen_Text").text), "(")
        
        test.compare(waitForObjectExists(":CoEntryOverlay.SensorSystolicDiastolic_SystolicDiastolicText").visible, True)
        test.compare(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_Label_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_Label_Text").text), "Systolic/Diastolic")
        test.compare(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_SysLabel_Text").visible, False)
        test.compare(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_Systolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_Systolic_Text").text), "38")
        test.compare(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_DiaLabel_Text").visible, False)
        test.compare(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_Diastolic_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_Diastolic_Text").text), "19")
        test.compare(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_Separator_Text").visible, True)
        test.compare(str(waitForObjectExists(":SensorSystolicDiastolic.SensorSystolicDiastolic_Separator_Text").text), "/")
        
        test.compare(waitForObjectExists(":CoEntryOverlay.cardiacOutputLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.cardiacOutputLabel_Text").text), "CARDIAC OUTPUT")
        
        test.compare(waitForObjectExists(":CoEntryOverlay.CardiacOutput_CardiacOutputTextField").visible, True)
        test.compare(waitForObjectExists(":CoEntryOverlay.CardiacOutput_CardiacOutputTextField").value, 0)
        test.compare(waitForObjectExists(":CoEntryOverlay.CardiacOutput_CardiacOutputTextField").enabled, True)
        
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").text), "")
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").minValue, 1)
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").maxValue, 9)
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").maximumLength, 1)
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").isEditable, True)
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").inputMethodHints, 65536)
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").enabled, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").enteredValue), "")
        
        test.compare(waitForObjectExists(":CoEntryOverlay.DecimalPoint_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.DecimalPoint_Text").text), ".")
        
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").text), "")
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").minValue, 0)
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").maxValue, 99)
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").maximumLength, 2)
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").isEditable, True)
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").inputMethodHints, 65536)
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").enabled, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").enteredValue), "")
        
        test.compare(waitForObjectExists(":CoEntryOverlay.UnitsLabel_Text").visible, True)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.UnitsLabel_Text").text), "L/min")

    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
