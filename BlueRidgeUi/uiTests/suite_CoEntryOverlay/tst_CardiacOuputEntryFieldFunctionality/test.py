"""
 @file   tst_CardiacOuputEntryFieldFunctionality

 Description:

 Test the functionality of the entry field in the screen

 @section LICENSE

 This software is property of Abbott

 Copyright 2019 Abbott All rights reserved.

"""

def main():
    try: 
        source(findFile("scripts", "TestProcessesScript.py"))
        
        ts = TestProcesses()
        ts.runPressurePumpProc()
        ts.runTestServerProc("PatientFile.txt")
        startApplication("BlueRidgeUI")
        
        # Navigate to New Implant Patient screen
        mouseClick(waitForObject(":MainMenu.NewImplant_Button"))
        
        # Navigate to Patient Confirmation screen
        mouseClick(waitForObject(":NewImplantPatientList.PatientName_Delegate1_SelectableListContent"))
        
        # Navigate to Sensor check screen
        mouseClick(waitForObject(":PatientConfirmation.Confirm_Button"))
        
        # Navigate to Wait For Implant dialog
        mouseClick(waitForObject(":SensorCheck.ContinueButton_Button"))
    
        # Navigate to Select Implant Position screen
        mouseClick(waitForObject(":RightButton_Button"))
        
        # Navigate to Acquire Signal Screen
        mouseClick(waitForObject(":Implant_selectSensor.LeftButton_Image"))
        
        # Navigate to Calibrate Sensor Screen
        mouseClick(waitForObject(":Implant_acquireSignal.ContinueButton_Button"))
       
        # Navigate to PA Catheter Mean overlay
        enabled = waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        while(enabled != True):
            enabled= waitForObjectExists(":Implant_calibrateSensor.CalibrateButton_Button").enabled
        mouseClick(waitForObject(":Implant_calibrateSensor.CalibrateButton_Button"))
        snooze(5)

        # Navigate to Record Pressures screen
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))        
        mouseClick(waitForObject(":PaMeanOverlay.SaveButton_Button"))      

        # Navigate to CO Entry Overlay
        snooze(5)
        mouseClick(waitForObject(":Implant_recordPressures.CalibrateCO_Button"))
        snooze(3)
        
        # Verify left of decimal numeric entry field accept only digit inputs
        type(waitForObject(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField"), "abc")
        test.compare(str(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").text), "")
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").enteredValue), "")
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").inputMethodHints, 65536)

        # Verify left of decimal numeric entry field accept only digit inputs
        type(waitForObject(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField"), "abc")
        test.compare(str(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").text), "")
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").length, 0)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").enteredValue), "")
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").inputMethodHints, 65536)
        
        # Verify left of decimal entry field only accepts one digit values
        mouseClick(waitForObject(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField"))
        mouseClick(waitForObject(":NumericKeyboard.2_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.7_Key_Button"))
        
        test.compare(str(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").text), "2")
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").length, 1)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").enteredValue), "2")
        test.compare(waitForObjectExists(":CoEntryOverlay.LeftOfDecimalEntry_NumericTextField").maximumLength, 1)     

        # Verify right of decimal entry field only accepts up to 2 digits digit values
        mouseClick(waitForObject(":NumericKeyboard.5_Key_Button"))
        mouseClick(waitForObject(":NumericKeyboard.0_Key_Button"))
        
        test.compare(str(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").text), "75")
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").length, 2)
        test.compare(str(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").enteredValue), "75")
        test.compare(waitForObjectExists(":CoEntryOverlay.RightOfDecimalEntry_NumericTextField").maximumLength, 2)
        
        # Verify entry field accepts the input
        mouseClick(waitForObject(":NumericKeyboard.Next_Key_Button"))

        test.compare(waitForObjectExists(":CoEntryOverlay.CardiacOutput_CardiacOutputTextField").visible, True)
        test.compare(waitForObjectExists(":CoEntryOverlay.CardiacOutput_CardiacOutputTextField").value, 2.75)
        test.compare(waitForObjectExists(":CoEntryOverlay.CardiacOutput_CardiacOutputTextField").enabled, True)
       
    except Exception, err:
        test.fail("Test failed due to following error: ", str(err))
