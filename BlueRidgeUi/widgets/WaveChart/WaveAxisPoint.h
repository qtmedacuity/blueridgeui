#pragma once

#include <string>
#include <tuple>

class WaveAxisPoint
{
public:
    typedef std::tuple<double, double> Coord;

    WaveAxisPoint();
    WaveAxisPoint(double initvalue, std::string const& initlabel, int ticklength = 3, int tickwidth = 1, int grid = 1);
    WaveAxisPoint(WaveAxisPoint const& other);
    WaveAxisPoint(WaveAxisPoint&& other);
    WaveAxisPoint& operator=(WaveAxisPoint const& rhs);
    WaveAxisPoint& operator=(WaveAxisPoint&& rhs);
    bool operator==(WaveAxisPoint const& rhs) const;
    bool operator>(WaveAxisPoint const& rhs) const;
    bool operator>=(WaveAxisPoint const& rhs) const;
    bool operator<(WaveAxisPoint const& rhs) const;
    bool operator<=(WaveAxisPoint const& rhs) const;

    double value() const;
    void setValue(double newvalue);

    std::string label() const;
    void setLabel(std::string const& newlabel);

    int gridWidth() const;
    void setGridWidth(int width);

    int tickWidth() const;
    void setTickWidth(int width);

    int tickLength() const;
    void setTickLength(int length);

    Coord coord() const;
    void setCoord(Coord const& point);
    void setCoord(double x, double y);

private:
    double m_value;
    std::string m_label;
    int m_gridWidth;
    int m_tickWidth;
    int m_tickLength;
    Coord m_coord;
};
