/**
 * @file   WaveChartWidget.cpp
 *
 * \brief This file defines the Active Chart Controller class.
 *
 * Description:
 *  Provides a custom chart component to render a data wave-form.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include "widgets/WaveChart/WaveChartWidget.h"
#include "widgets/WaveChart/WaveAxis.h"
#include "widgets/WaveChart/WaveAxisPoint.h"

#include <QtCharts/QXYSeries>
#include <QPainter>
#include <QPointF>
#include <QDebug>

#include <algorithm>
#include <cmath>
#include <limits>
#include <exception>

auto normalize = [](qreal coord, qreal range, qreal span) -> qreal { return ((coord) * span) / range; };

using namespace QtCharts;

WaveChartWidget::WaveChartWidget(QQuickItem* parent)
    : QQuickPaintedItem(parent),
      m_resizeField(true),
      m_updateAxes(true),
      m_backgroundColor(Qt::black),
      m_plotAreaColor(Qt::transparent),
      m_verticalPadding(15.0),
      m_horizontalPadding(20.0),
      m_xAxis(WaveAxis::Orientation::Horizontal),
      m_yAxis(WaveAxis::Orientation::Vertical)
{
    QObject::connect(this, &WaveChartWidget::widthChanged, [&]() { m_resizeField = true; update(); });
    QObject::connect(this, &WaveChartWidget::heightChanged, [&]() { m_resizeField = true; update(); });
    QObject::connect(&m_yAxis, &WaveAxis::spanChanged, [&]() { m_resizeField = true; update(); });
    QObject::connect(&m_xAxis, &WaveAxis::spanChanged, [&]() { m_resizeField = true; update(); });
    QObject::connect(&m_yAxis, &WaveAxis::updated, [&]() { m_updateAxes = true; update(); });
    QObject::connect(&m_xAxis, &WaveAxis::updated, [&]() { m_updateAxes = true; update(); });
}

WaveChartWidget::~WaveChartWidget()
{
    m_seriesList.clear();
}

void WaveChartWidget::addSeries(QXYSeries* series)
{
    if (nullptr == series || m_seriesList.contains(series))
        return;

    if (QAbstractSeries::SeriesTypeLine!= series->type() &&  QAbstractSeries::SeriesTypeSpline != series->type()) {
        qWarning() << QString("WaveChartWidget cannot add invalid series type: %0").arg(series->type());
        return;
    }

    QVector<QMetaObject::Connection> connections;
    m_seriesConnections.append(connections);
    m_seriesConnections.back().push_back(QObject::connect(series, &QXYSeries::colorChanged, [&](QColor) { update(); }));
    m_seriesConnections.back().push_back(QObject::connect(series, &QXYSeries::penChanged, [&](const QPen&) { update(); }));
    m_seriesConnections.back().push_back(QObject::connect(series, &QXYSeries::pointAdded, [&](int) { update(); }));
    m_seriesConnections.back().push_back(QObject::connect(series, &QXYSeries::pointRemoved, [&](int) { update(); }));
    m_seriesConnections.back().push_back(QObject::connect(series, &QXYSeries::pointReplaced, [&](int) { update(); }));
    m_seriesConnections.back().push_back(QObject::connect(series, &QXYSeries::pointsRemoved, [&](int, int) { update(); }));
    m_seriesConnections.back().push_back(QObject::connect(series, &QXYSeries::pointsReplaced, [&]() { update(); }));
    m_seriesConnections.back().push_back(QObject::connect(series, &QXYSeries::destroyed, [&](QObject* obj) { removeSeries(dynamic_cast<QXYSeries*>(obj)); }));

    m_seriesList.append(series);
    update();
}

void WaveChartWidget::removeSeries(QXYSeries* series)
{
    if (nullptr == series || !m_seriesList.contains(series))
        return;

    int index(m_seriesList.indexOf(series));
    m_seriesList.removeAt(index);
    for (QMetaObject::Connection const& connection : m_seriesConnections.at(index)) {
        QObject::disconnect(connection);
    }
    m_seriesConnections.removeAt(index);
    update();
}

void WaveChartWidget::removeAllSeries()
{
    m_seriesList.clear();
    update();
}

QtCharts::QXYSeries* WaveChartWidget::series(int index) const
{
    if (index < m_seriesList.count())
        return m_seriesList.at(index);

    return nullptr;
}

WaveAxis* WaveChartWidget::xAxis()
{
    return &m_xAxis;
}

WaveAxis* WaveChartWidget::yAxis()
{
    return &m_yAxis;
}

QString WaveChartWidget::objectName() const
{
    return QQuickPaintedItem::objectName();
}

void WaveChartWidget::setObjectName(QString const& name)
{
    if (name != objectName()) {
        QQuickPaintedItem::setObjectName(name);
        emit objectNameChanged(objectName());
    }
}

QColor WaveChartWidget::backgroundColor() const
{
    return m_backgroundColor;
}

void WaveChartWidget::setBackgroundColor(QColor const& color)
{
    if (color != m_backgroundColor) {
        m_backgroundColor = color;
        emit backgroundColorChanged(m_backgroundColor);
    }
}

QColor WaveChartWidget::plotAreaColor() const
{
    return m_plotAreaColor;
}

void WaveChartWidget::setPlotAreaColor(const QColor &color)
{
    if (color != m_plotAreaColor) {
        m_plotAreaColor = color;
        emit plotAreaColorChanged(m_plotAreaColor);
    }
}

qreal WaveChartWidget::verticalPadding() const
{
    return m_verticalPadding;
}

void WaveChartWidget::setVerticalPadding(qreal value)
{
    if (std::abs(value - m_verticalPadding) < std::numeric_limits<double>::epsilon()) return;

    m_verticalPadding = value;
    emit verticalPaddingChanged(m_verticalPadding);
}

qreal WaveChartWidget::horizontalPadding() const
{
    return m_horizontalPadding;
}

void WaveChartWidget::setHorizontalPadding(qreal value)
{
    if (std::abs(value - m_horizontalPadding) < std::numeric_limits<double>::epsilon()) return;

    m_horizontalPadding = value;
    emit horizontalPaddingChanged(m_horizontalPadding);
}

void WaveChartWidget::paint(QPainter* painter)
{
    if (m_resizeField)
        resizeField();

    if (m_updateAxes)
        updateAxes();

    painter->save();
    painter->setRenderHint(QPainter::Antialiasing);

    clearField(painter, m_backgroundColor);
    drawPlotArea(painter, m_xAxis, m_yAxis, m_plotArea, m_plotAreaColor);
    drawAxis(painter, m_yAxis);
    drawAxis(painter, m_xAxis);
    for (QtCharts::QXYSeries* series : m_seriesList) {
        drawSeries(painter, m_xAxis, m_yAxis, series, m_plotArea);
    }
    painter->restore();
}

void WaveChartWidget::clearField(QPainter* painter, QColor const& color)
{
    painter->save();
    QRectF rect(0, 0, width(), height());
    painter->fillRect(rect, color);
    painter->restore();
}

void WaveChartWidget::drawPlotArea(QPainter* painter, WaveAxis const& horiz, WaveAxis const& vert, QRectF const& rect, QColor const& color)
{
    painter->save();
    painter->fillRect(rect, color);
    drawGrid(painter, horiz);
    drawGrid(painter, vert);
    painter->restore();
}

void WaveChartWidget::drawAxis(QPainter* painter, WaveAxis const& axis)
{
    painter->save();
    painter->fillRect(axis.rect(), axis.backgroundColor());

    if (axis.lineEnabled() && 0 < axis.lineWeight()) {
        QPen pen(QBrush(axis.axisColor()), axis.lineWeight(), Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin);
        painter->setPen(pen);
        QLineF line(axis.line());
        if (WaveAxis::Orientation::Horizontal == axis.orientation()) {
            line.setP2(QPointF(width(), line.y2()));
        } else if (WaveAxis::Orientation::Vertical == axis.orientation()) {
            line.setP2(QPointF(line.x2(), 0.0));
        }
        painter->drawLine(line);
    }

    drawTicks(painter, axis);
    drawLabels(painter, axis);
    painter->restore();
}

void WaveChartWidget::drawTicks(QPainter* painter, WaveAxis const& axis)
{
    if (!axis.ticksEnabled())
        return;

    painter->save();

    QTransform identity;
    QTransform rotation;
    QTransform translation;

    if (WaveAxis::Orientation::Vertical == axis.orientation()) {
        rotation.rotate(180.0);

        for (WaveAxisPoint& point : axis.points()) {
            if (axis.displayMin() <= point.value() && point.value() <= axis.displayMax() && 0 < point.tickWidth() && 0 < point.tickLength()) {
                QLineF tick(QPointF(0.0, 0.0), QPointF(static_cast<double>(point.tickLength()), 0.0));

                QPen pen(QBrush(axis.axisColor()), point.tickWidth(), Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin);
                painter->setPen(pen);

                translation = identity;
                translation.translate(std::get<0>(point.coord()), std::get<1>(point.coord()));
                painter->setTransform(rotation * translation);
                painter->drawLine(tick);
                painter->setTransform(identity);
            }
        }
    } else if (WaveAxis::Orientation::Horizontal == axis.orientation()) {
        rotation.rotate(90.0);

        for (WaveAxisPoint& point : axis.points()) {
            if (axis.displayMin() <= point.value() && point.value() <= axis.displayMax() && 0 < point.tickWidth() && 0 < point.tickLength()) {
                QLineF tick(QPointF(0.0, 0.0), QPointF(static_cast<double>(point.tickLength()), 0.0));

                QPen pen(QBrush(axis.axisColor()), point.tickWidth(), Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin);
                painter->setPen(pen);

                translation = identity;
                translation.translate(std::get<0>(point.coord()), std::get<1>(point.coord()));
                painter->setTransform(rotation * translation);
                painter->drawLine(tick);
                painter->setTransform(identity);
            }
        }
    }
    painter->restore();
}

void WaveChartWidget::drawLabels(QPainter *painter, const WaveAxis &axis)
{
    if (!axis.labelsEnabled())
        return;

    painter->save();

    QTransform identity;
    QTransform translation;
    QPoint labelSize(axis.labelSize());

    if (WaveAxis::Orientation::Vertical == axis.orientation()) {
        translation = identity;
        translation.translate(-(((axis.span() - labelSize.rx()) * 0.5) + labelSize.rx()), -labelSize.ry() * 0.6);
        for (WaveAxisPoint& point : axis.points()) {
            if (axis.displayMin() <= point.value() && point.value() <= axis.displayMax()) {
                QPen pen(axis.axisColor());
                painter->setFont(axis.font());
                painter->setPen(pen);
                painter->setTransform(translation);
                painter->drawText(QRectF(std::get<0>(point.coord()), std::get<1>(point.coord()), labelSize.rx(), labelSize.ry()), Qt::AlignRight|Qt::AlignBottom, QString().fromStdString(point.label()));
                painter->setTransform(identity);
            }
        }
    } else if (WaveAxis::Orientation::Horizontal == axis.orientation()) {
        translation = identity;
        translation.translate(-labelSize.rx(), (axis.span() - labelSize.ry()) * 0.5);
        for (WaveAxisPoint& point : axis.points()) {
            if (axis.displayMin() <= point.value() && point.value() <= axis.displayMax()) {
                QPen pen(axis.axisColor());
                painter->setFont(axis.font());
                painter->setPen(pen);
                painter->setTransform(translation);
                painter->drawText(QRectF(std::get<0>(point.coord()), std::get<1>(point.coord()), labelSize.rx(), labelSize.ry()), Qt::AlignLeft|Qt::AlignTop, QString().fromStdString(point.label()));
                painter->setTransform(identity);
            }
        }
    }

    painter->restore();
}

void WaveChartWidget::drawGrid(QPainter *painter, const WaveAxis &axis)
{
    if (!axis.gridEnabled())
        return;

    painter->save();

    QTransform identity;
    QTransform translation;

    if (WaveAxis::Orientation::Vertical == axis.orientation()) {
        qreal length(width() - axis.rect().width());
        for (WaveAxisPoint& point : axis.points()) {
            if (axis.displayMin() <= point.value() && point.value() <= axis.displayMax() && 0 < point.gridWidth()) {
                QLineF grid(QPointF(0.0, 0.0), QPointF(length, 0.0));

                QPen pen(QBrush(axis.axisColor()), point.gridWidth(), Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin);
                painter->setPen(pen);

                translation = identity;
                translation.translate(std::get<0>(point.coord()), std::get<1>(point.coord()));
                painter->setTransform(translation);
                painter->drawLine(grid);
                painter->setTransform(identity);
            }
        }
    } else if (WaveAxis::Orientation::Horizontal == axis.orientation()) {
        qreal length(height() - axis.rect().height());
        for (WaveAxisPoint& point : axis.points()) {
            if (axis.displayMin() <= point.value() && point.value() <= axis.displayMax() && 0 < point.gridWidth()) {
                QLineF grid(QPointF(0.0, 0.0), QPointF(0.0, -length));

                QPen pen(QBrush(axis.axisColor()), point.gridWidth(), Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin);
                painter->setPen(pen);

                translation = identity;
                translation.translate(std::get<0>(point.coord()), std::get<1>(point.coord()));
                painter->setTransform(translation);
                painter->drawLine(grid);
                painter->setTransform(identity);
            }
        }
    }

    painter->restore();
}

void WaveChartWidget::drawSeries(QPainter *painter, const WaveAxis &horiz, const WaveAxis &vert, QXYSeries *series, QRectF const& rect)
{
    if (nullptr == series || 0 == series->count() || false == series->pointsVisible())
        return;

    qreal horizMinimum(horiz.displayMin());
    qreal horizRange(horiz.displayMax() - horizMinimum);
    qreal horizSpan(horiz.line().x2() - horiz.line().x1());

    qreal vertMinimum(vert.displayMin());
    qreal vertRange(vert.displayMax() - vertMinimum);
    qreal vertSpan(vert.line().y1() - vert.line().y2());

    painter->save();
    painter->setClipRect(rect);

    QVector<QPointF> normalizedPoints(series->pointsVector());
    for (QPointF& point : normalizedPoints) {
        point.setX(vert.line().x1() + normalize(point.rx() - horizMinimum, horizRange, horizSpan));
        point.setY(horiz.line().y1() - normalize(point.ry()- vertMinimum, vertRange, vertSpan));
    }

    painter->setPen(series->pen());
    QPainterPath wavePath;
    wavePath.addPolygon(QPolygonF(normalizedPoints));
    painter->drawPath(wavePath);

    painter->restore();
}

void WaveChartWidget::resizeField()
{
    m_yAxis.setRect(QRectF(0.0, 0.0, m_yAxis.span(), height()));
    m_xAxis.setRect(QRectF(0.0, height() - m_xAxis.span(), width(), m_xAxis.span()));

    m_plotArea = QRectF(m_yAxis.span(), m_verticalPadding, width() - m_yAxis.span() - m_horizontalPadding, height() - m_xAxis.span() - m_verticalPadding);

    m_yAxis.setLine(QLineF(m_plotArea.bottomLeft(), m_plotArea.topLeft()));
    m_xAxis.setLine(QLineF(m_plotArea.bottomLeft(), m_plotArea.bottomRight()));

    m_plotArea = QRectF(m_yAxis.span(), 0.0, width() - m_yAxis.span(), height() - m_xAxis.span());

    updateAxes();

    m_resizeField = false;
}

void WaveChartWidget::updateAxes()
{
    auto update = [&](WaveAxis& axis) {
        qreal axisMax(axis.displayMax());
        qreal axisMin(axis.displayMin());
        qreal range(axisMax - axisMin);

        int pixWidth(0);
        int pixHeight(0);
        QFontMetrics metrics(axis.font());
        if (WaveAxis::Orientation::Vertical == axis.orientation()) {
            qreal lower(axis.line().y1());
            qreal upper(axis.line().y2());
            qreal span(lower - upper);

            WaveAxis::PointList points(axis.points());
            for (WaveAxisPoint& point : points) {
                qreal value(normalize(point.value() - axisMin, range, span));
                point.setCoord(axis.line().x1(), axis.line().y1() - value);
                pixWidth = std::max(pixWidth, metrics.horizontalAdvance(QString().fromStdString(point.label())));
                pixHeight = std::max(pixHeight, metrics.height());
            }
            axis.setPoints(std::move(points));
        } else if (WaveAxis::Orientation::Horizontal == axis.orientation()) {
            qreal lower(axis.line().x1());
            qreal upper(axis.line().x2());
            qreal span(upper - lower);

            WaveAxis::PointList points(axis.points());
            for (WaveAxisPoint& point : points) {
                qreal value(normalize(point.value() - axisMin, range, span));
                point.setCoord(value + axis.line().x1(), axis.line().y1());
                pixWidth = std::max(pixWidth, metrics.horizontalAdvance(QString().fromStdString(point.label())));
                pixHeight = std::max(pixHeight, metrics.height());
            }
            axis.setPoints(std::move(points));
        }

        axis.setLabelSize(QPoint(pixWidth, pixHeight));
    };

    update(m_xAxis);
    update(m_yAxis);

    m_updateAxes = false;
}
