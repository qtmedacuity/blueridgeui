#include "widgets/WaveChart/WaveAxisPoint.h"

#include <cmath>
#include <limits>

WaveAxisPoint::WaveAxisPoint()
    : m_value(0.0),
      m_label(),
      m_gridWidth(1),
      m_tickWidth(1),
      m_tickLength(5),
      m_coord(std::make_tuple(0.0, 0.0))
{}

WaveAxisPoint::WaveAxisPoint(double initvalue, std::string const& initlabel, int ticklength, int tickwidth, int grid)
    : m_value(initvalue),
      m_label(initlabel),
      m_gridWidth(grid >= 0 ? grid : 0),
      m_tickWidth(tickwidth >= 0 ? tickwidth : 0),
      m_tickLength(ticklength >= 0 ? ticklength : 0),
      m_coord(std::make_tuple(0.0, 0.0))
{}

WaveAxisPoint::WaveAxisPoint(const WaveAxisPoint &other)
    : m_value(other.m_value),
      m_label(other.m_label),
      m_gridWidth(other.m_gridWidth),
      m_tickWidth(other.m_tickWidth),
      m_tickLength(other.m_tickLength),
      m_coord(other.m_coord)
{}

WaveAxisPoint::WaveAxisPoint(WaveAxisPoint&& other)
    : m_value(std::move(other.m_value)),
      m_label(std::move(other.m_label)),
      m_gridWidth(std::move(other.m_gridWidth)),
      m_tickWidth(std::move(other.m_tickWidth)),
      m_tickLength(std::move(other.m_tickLength)),
      m_coord(std::move(other.m_coord))
{}

WaveAxisPoint &WaveAxisPoint::operator=(const WaveAxisPoint &rhs)
{
    m_value = rhs.m_value;
    m_label = rhs.m_label;
    m_gridWidth = rhs.m_gridWidth;
    m_tickWidth = rhs.m_tickWidth;
    m_tickLength = rhs.m_tickLength;
    m_coord = rhs.m_coord;
    return *this;
}

WaveAxisPoint &WaveAxisPoint::operator=(WaveAxisPoint&& rhs)
{
    m_value = std::move(rhs.m_value);
    m_label = std::move(rhs.m_label);
    m_gridWidth = std::move(rhs.m_gridWidth);
    m_tickWidth = std::move(rhs.m_tickWidth);
    m_tickLength = std::move(rhs.m_tickLength);
    m_coord = std::move(rhs.m_coord);
    return *this;
}

bool WaveAxisPoint::operator==(const WaveAxisPoint &rhs) const
{
    return std::abs(rhs.m_value - m_value) <= std::numeric_limits<double>::epsilon();
}

bool WaveAxisPoint::operator>(const WaveAxisPoint &rhs) const
{
    return m_value > rhs.m_value;
}

bool WaveAxisPoint::operator>=(const WaveAxisPoint &rhs) const
{
    return operator>(rhs) || operator==(rhs);
}

bool WaveAxisPoint::operator<(const WaveAxisPoint &rhs) const
{
    return m_value < rhs.m_value;
}

bool WaveAxisPoint::operator<=(const WaveAxisPoint &rhs) const
{
    return operator<(rhs) || operator==(rhs);
}

double WaveAxisPoint::value() const
{
    return m_value;
}

void WaveAxisPoint::setValue(double newvalue)
{
    m_value = newvalue;
}

std::string WaveAxisPoint::label() const
{
    return m_label;
}

void WaveAxisPoint::setLabel(std::string const& newlabel)
{
    m_label = newlabel;
}

int WaveAxisPoint::gridWidth() const
{
    return m_gridWidth;
}

void WaveAxisPoint::setGridWidth(int width)
{
    m_gridWidth = width >= 0 ? width : 0;
}

int WaveAxisPoint::tickWidth() const
{
    return m_tickWidth;
}

void WaveAxisPoint::setTickWidth(int width)
{
    m_tickWidth = width >= 0 ? width : 0;
}

int WaveAxisPoint::tickLength() const
{
    return m_tickLength;
}

void WaveAxisPoint::setTickLength(int length)
{
    m_tickLength = length >= 0 ? length : 0;
}

WaveAxisPoint::Coord WaveAxisPoint::coord() const
{
    return m_coord;
}

void WaveAxisPoint::setCoord(Coord const& point)
{
    m_coord = point;
}

void WaveAxisPoint::setCoord(double x, double y)
{
    m_coord = std::make_tuple(x, y);
}
