#include "widgets/WaveChart/WaveAxis.h"

#include <algorithm>
#include <cmath>
#include <limits>

WaveAxis::WaveAxis(Orientation orientation, QObject* parent)
    : QObject(parent),
      m_points(),
      m_rect(),
      m_line(),
      m_orientation(orientation),
      m_labelSize(0, 0),
      m_font(),
      m_backgroundColor(Qt::transparent),
      m_axisColor(Qt::white),
      m_span(0.0),
      m_lineWeight(1),
      m_lineEnabled(true),
      m_ticksEnabled(true),
      m_labelsEnabled(true),
      m_gridEnabled(true),
      m_displayMax(std::numeric_limits<double>::min()),
      m_displayMin(std::numeric_limits<double>::max()),
      m_scrollOffset(0.0)
{
    QObject::connect(this, &WaveAxis::fontChanged, [&](QFont) { emit updated(); });
    QObject::connect(this, &WaveAxis::backgroundColorChanged, [&](QColor) { emit updated(); });
    QObject::connect(this, &WaveAxis::axisColorChanged, [&](QColor) { emit updated(); });
    QObject::connect(this, &WaveAxis::spanChanged, [&](qreal) { emit updated(); });
    QObject::connect(this, &WaveAxis::lineWeightChanged, [&](int) { emit updated(); });
    QObject::connect(this, &WaveAxis::lineEnabledChanged, [&](bool) { emit updated(); });
    QObject::connect(this, &WaveAxis::ticksEnabledChanged, [&](bool) { emit updated(); });
    QObject::connect(this, &WaveAxis::labelsEnabledChanged, [&](bool) { emit updated(); });
    QObject::connect(this, &WaveAxis::gridEnabledChanged, [&](bool) { emit updated(); });
    QObject::connect(this, &WaveAxis::displayMaxChanged, [&](qreal) { emit updated(); });
    QObject::connect(this, &WaveAxis::displayMinChanged, [&](qreal) { emit updated(); });
    QObject::connect(this, &WaveAxis::scrollOffsetChanged, [&](qreal) { emit updated(); });
}

WaveAxis::PointList WaveAxis::points() const
{
    return m_points;
}

void WaveAxis::setPoints(WaveAxis::PointList const& values)
{
    m_points = values;
    emit pointsChanged();
}

void WaveAxis::setPoints(WaveAxis::PointList&& values)
{
   m_points = std::move(values);
}

QRectF const& WaveAxis::rect() const
{
    return m_rect;
}

void WaveAxis::setRect(const QRectF& value)
{
    m_rect = value;
}

const QLineF &WaveAxis::line() const
{
    return m_line;
}

void WaveAxis::setLine(const QLineF &value)
{
    m_line = value;
}

WaveAxis::Orientation WaveAxis::orientation() const
{
    return m_orientation;
}

void WaveAxis::setOrientation(WaveAxis::Orientation value)
{
    m_orientation = value;
}

QPoint WaveAxis::labelSize() const
{
    return m_labelSize;
}

void WaveAxis::setLabelSize(const QPoint &value)
{
    m_labelSize = value;
}

QFont WaveAxis::font() const
{
    return m_font;
}

void WaveAxis::setFont(QFont const& value)
{
    if (value != m_font) {
        m_font = value;
        emit fontChanged(m_font);
    }
}

QColor WaveAxis::backgroundColor() const
{
    return m_backgroundColor;
}

void WaveAxis::setBackgroundColor(QColor const& value)
{
    if (value != m_backgroundColor) {
        m_backgroundColor = value;
        emit backgroundColorChanged(m_backgroundColor);
    }
}

QColor WaveAxis::axisColor() const
{
    return m_axisColor;
}

void WaveAxis::setAxisColor(QColor const& value)
{
    if (value != m_axisColor) {
        m_axisColor = value;
        emit axisColorChanged(m_axisColor);
    }
}

qreal WaveAxis::span() const
{
    return m_span;
}

void WaveAxis::setSpan(qreal value)
{
    value = value >= 0.0 ? value : 0.0;
    if (std::abs(value - m_span) < std::numeric_limits<double>::epsilon())
        return;

    m_span = value;
    emit spanChanged(m_span);
}

int WaveAxis::lineWeight() const
{
    return m_lineWeight;
}

void WaveAxis::setLineWeight(int value)
{
    value = value >= 0 ? value : 0;
    if (value != m_lineWeight) {
        m_lineWeight = value;
        emit lineWeightChanged(m_lineWeight);
    }
}

bool WaveAxis::lineEnabled() const
{
    return m_lineEnabled;
}

void WaveAxis::setLineEnabled(bool value)
{
    if (value != m_lineEnabled) {
        m_lineEnabled = value;
        emit lineEnabledChanged(m_lineEnabled);
    }
}

bool WaveAxis::ticksEnabled() const
{
    return m_ticksEnabled;
}

void WaveAxis::setTicksEnabled(bool value)
{
    if (value != m_ticksEnabled) {
        m_ticksEnabled = value;
        emit ticksEnabledChanged(m_ticksEnabled);
    }
}

bool WaveAxis::labelsEnabled() const
{
    return m_labelsEnabled;
}

void WaveAxis::setLabelsEnabled(bool value)
{
    if (value != m_labelsEnabled) {
        m_labelsEnabled = value;
        emit labelsEnabledChanged(m_labelsEnabled);
    }
}

bool WaveAxis::gridEnabled() const
{
    return m_gridEnabled;
}

void WaveAxis::setGridEnabled(bool value)
{
    if (value != m_gridEnabled) {
        m_gridEnabled = value;
        emit gridEnabledChanged(m_gridEnabled);
    }
}

qreal WaveAxis::displayMax() const
{
    return m_displayMax;
}

void WaveAxis::setDisplayMax(qreal value)
{
    if (std::abs(value - m_displayMax) < std::numeric_limits<double>::epsilon())
        return;

    m_displayMax = value;
    emit displayMaxChanged(m_displayMax);
}

qreal WaveAxis::displayMin() const
{
    return m_displayMin;
}

void WaveAxis::setDisplayMin(qreal value)
{
    if (std::abs(value - m_displayMin) < std::numeric_limits<double>::epsilon())
        return;

    m_displayMin = value;
    emit displayMinChanged(m_displayMin);
}

qreal WaveAxis::scrollOffset() const
{
    return m_scrollOffset;
}

void WaveAxis::setScrollOffset(qreal value)
{
    if (std::abs(value - m_scrollOffset) < std::numeric_limits<double>::epsilon())
        return;

    m_scrollOffset = value;
    emit scrollOffsetChanged(m_scrollOffset);
}
