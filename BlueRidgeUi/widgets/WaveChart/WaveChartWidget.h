/**
 * @file   WaveChartWidget.cpp
 *
 * \brief This file declares the Active Chart Controller class.
 *
 * Description:
 * Provides a custom chart component to render a data wave-form.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#pragma once

#include "widgets/WaveChart/WaveAxis.h"

#include <QQuickPaintedItem>
#include <QLineF>
#include <QList>
#include <QVector>

namespace QtCharts {
class QXYSeries;
}

class WaveChartWidget : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString objectName READ objectName WRITE setObjectName NOTIFY objectNameChanged)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor NOTIFY backgroundColorChanged)
    Q_PROPERTY(QColor plotAreaColor READ plotAreaColor WRITE setPlotAreaColor NOTIFY plotAreaColorChanged)
    Q_PROPERTY(qreal verticalPadding READ verticalPadding WRITE setVerticalPadding NOTIFY verticalPaddingChanged)
    Q_PROPERTY(qreal horizontalPadding READ horizontalPadding WRITE setHorizontalPadding NOTIFY horizontalPaddingChanged)

    Q_PROPERTY(WaveAxis* xAxis READ xAxis)
    Q_PROPERTY(WaveAxis* yAxis READ yAxis)

public:
    WaveChartWidget(QQuickItem *parent = nullptr);
    ~WaveChartWidget() override;

    Q_INVOKABLE void addSeries(QtCharts::QXYSeries* series);
    Q_INVOKABLE void removeSeries(QtCharts::QXYSeries* series);
    Q_INVOKABLE void removeAllSeries();
    Q_INVOKABLE QtCharts::QXYSeries* series(int index) const;

signals:
    void objectNameChanged(QString name);
    void backgroundColorChanged(QColor color);
    void plotAreaColorChanged(QColor color);
    void verticalPaddingChanged(qreal);
    void horizontalPaddingChanged(qreal);

public slots:
    QString objectName() const;
    void setObjectName(QString const& name);

    QColor backgroundColor() const;
    void setBackgroundColor(QColor const& color);

    QColor plotAreaColor() const;
    void setPlotAreaColor(QColor const& color);

    qreal verticalPadding() const;
    void setVerticalPadding(qreal value);

    qreal horizontalPadding() const;
    void setHorizontalPadding(qreal value);

    WaveAxis* xAxis();
    WaveAxis* yAxis();

public:
    void paint(QPainter *painter) override;

private:
    void clearField(QPainter* painter, QColor const& color);
    void drawPlotArea(QPainter* painter, WaveAxis const& horiz, WaveAxis const& vert, QRectF const& rect, QColor const& color);
    void drawAxis(QPainter* painter, WaveAxis const& axis);
    void drawTicks(QPainter* painter, WaveAxis const& axis);
    void drawLabels(QPainter* painter, WaveAxis const& axis);
    void drawGrid(QPainter* painter, WaveAxis const& axis);
    void drawSeries(QPainter* painter, WaveAxis const& horiz, WaveAxis const& vert, QtCharts::QXYSeries* series, QRectF const& rect);
    void resizeField();
    void updateAxes();

private:
    bool m_resizeField;
    bool m_updateAxes;
    QColor m_backgroundColor;
    QColor m_plotAreaColor;
    qreal m_verticalPadding;
    qreal m_horizontalPadding;
    WaveAxis m_xAxis;
    WaveAxis m_yAxis;
    QList<QtCharts::QXYSeries*> m_seriesList;
    QList<QVector<QMetaObject::Connection>> m_seriesConnections;
    QRectF m_plotArea;
};
