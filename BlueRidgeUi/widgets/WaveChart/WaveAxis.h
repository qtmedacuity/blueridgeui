#pragma once

#include "widgets/WaveChart/WaveAxisPoint.h"

#include <QColor>
#include <QFont>
#include <QLineF>
#include <QObject>
#include <QRectF>

#include <vector>
#include <cstdint>

class WaveAxis : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QFont font READ font WRITE setFont NOTIFY fontChanged)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor NOTIFY backgroundColorChanged)
    Q_PROPERTY(QColor axisColor READ axisColor WRITE setAxisColor NOTIFY axisColorChanged)
    Q_PROPERTY(qreal span READ span WRITE setSpan NOTIFY spanChanged)
    Q_PROPERTY(int lineWeight READ lineWeight WRITE setLineWeight NOTIFY lineWeightChanged)
    Q_PROPERTY(bool lineEnabled READ lineEnabled WRITE setLineEnabled NOTIFY lineEnabledChanged)
    Q_PROPERTY(bool ticksEnabled READ ticksEnabled WRITE setTicksEnabled NOTIFY ticksEnabledChanged)
    Q_PROPERTY(bool labelsEnabled READ labelsEnabled WRITE setLabelsEnabled NOTIFY labelsEnabledChanged)
    Q_PROPERTY(bool gridEnabled READ gridEnabled WRITE setGridEnabled NOTIFY gridEnabledChanged)
    Q_PROPERTY(qreal displayMax READ displayMax WRITE setDisplayMax NOTIFY displayMaxChanged)
    Q_PROPERTY(qreal displayMin READ displayMin WRITE setDisplayMin NOTIFY displayMinChanged)
    Q_PROPERTY(qreal scrollOffset READ scrollOffset WRITE setScrollOffset NOTIFY scrollOffsetChanged)

public:
    enum class Orientation : std::uint8_t { Horizontal = 0, Vertical = 1 };
    typedef std::vector<WaveAxisPoint> PointList;

    WaveAxis(Orientation orientation = Orientation::Horizontal, QObject *parent = nullptr);

    PointList points() const;
    void setPoints(PointList const& values);
    void setPoints(PointList&& values);

    QRectF const& rect() const;
    void setRect(QRectF const& value);

    QLineF const& line() const;
    void setLine(QLineF const& value);

    Orientation orientation() const;
    void setOrientation(Orientation value);

    QPoint labelSize() const;
    void setLabelSize(QPoint const& value);

signals:
    void updated();
    void pointsChanged();
    void fontChanged(QFont);
    void backgroundColorChanged(QColor);
    void axisColorChanged(QColor);
    void spanChanged(qreal);
    void lineWeightChanged(int);
    void lineEnabledChanged(bool);
    void ticksEnabledChanged(bool);
    void labelsEnabledChanged(bool);
    void gridEnabledChanged(bool);
    void displayMaxChanged(qreal);
    void displayMinChanged(qreal);
    void scrollOffsetChanged(qreal);

public slots:
    QFont font() const;
    void setFont(QFont const& value);

    QColor backgroundColor() const;
    void setBackgroundColor(QColor const& value);

    QColor axisColor() const;
    void setAxisColor(QColor const& value);

    qreal span() const;
    void setSpan(qreal value);

    int lineWeight() const;
    void setLineWeight(int value);

    bool lineEnabled() const;
    void setLineEnabled(bool value);

    bool ticksEnabled() const;
    void setTicksEnabled(bool value);

    bool labelsEnabled() const;
    void setLabelsEnabled(bool value);

    bool gridEnabled() const;
    void setGridEnabled(bool value);

    qreal displayMax() const;
    void setDisplayMax(qreal value);

    qreal displayMin() const;
    void setDisplayMin(qreal value);

    qreal scrollOffset() const;
    void setScrollOffset(qreal value);

private:
    PointList m_points;
    QRectF m_rect;
    QLineF m_line;
    Orientation m_orientation;
    QPoint m_labelSize;
    QFont m_font;
    QColor m_backgroundColor;
    QColor m_axisColor;
    qreal m_span;
    int m_lineWeight;
    bool m_lineEnabled;
    bool m_ticksEnabled;
    bool m_labelsEnabled;
    bool m_gridEnabled;
    qreal m_displayMax;
    qreal m_displayMin;
    qreal m_scrollOffset;
};
