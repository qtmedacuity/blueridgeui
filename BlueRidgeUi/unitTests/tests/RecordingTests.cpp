#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include <cstring>
#include <vector>

#include "presentation/Recording.h"


using namespace ::testing;

class RecordingTests : public Test
{
public:
    RecordingTests()
    {
        _recData.recordingId = 123456;
        _recData.uploadPending = true;
        _recData.recordingDate = "date";
        _recData.recordingTime = "time";
        _recData.systolic = 30;
        _recData.diastolic = 22;
        _recData.mean = 26;
        _recData.heartRate = 72;
        _recData.signalStrength = 80;
        _recData.refSystolic = 35;
        _recData.refDiastolic = 25;
        _recData.refMean = 28;
        _recData.notes = "notes";
        _recData.patientPosition = "left";
        _recData.waveformFile = "file";

        _recording = std::shared_ptr<Recording>(new Recording(_recData));
    }

    ~RecordingTests() override;

    RecordingData _recData;
    std::shared_ptr<Recording> _recording;
};

RecordingTests::~RecordingTests() {}

TEST_F(RecordingTests, ConstructorSetsDefaultValues)
{
    Recording recording;

    ASSERT_EQ(recording.recordingId(), 0);
    ASSERT_EQ(recording.uploadPending(), true);
    ASSERT_EQ(recording.recordingDate(), "");
    ASSERT_EQ(recording.recordingTime(), "");
    ASSERT_EQ(recording.systolic(), 0);
    ASSERT_EQ(recording.diastolic(), 0);
    ASSERT_EQ(recording.mean(), 0);
    ASSERT_EQ(recording.heartRate(), 0);
    ASSERT_EQ(recording.referenceSystolic(), 0);
    ASSERT_EQ(recording.referenceDiastolic(), 0);
    ASSERT_EQ(recording.referenceMean(), 0);
    ASSERT_EQ(recording.sensorStrength(), 0);
    ASSERT_EQ(recording.position(), "");
    ASSERT_EQ(recording.notes(), "");
    ASSERT_EQ(recording.waveFormFile(), "");
}

TEST_F(RecordingTests, ConstructorSetsValuesToRecordingDataValues)
{
    Recording recording(_recData);

    ASSERT_EQ(recording.recordingId(), _recData.recordingId);
    ASSERT_EQ(recording.uploadPending(), _recData.uploadPending);
    ASSERT_EQ(recording.recordingDate().toStdString(), _recData.recordingDate);
    ASSERT_EQ(recording.recordingTime().toStdString(), _recData.recordingTime);
    ASSERT_EQ(recording.systolic(), _recData.systolic);
    ASSERT_EQ(recording.diastolic(), _recData.diastolic);
    ASSERT_EQ(recording.mean(), _recData.mean);
    ASSERT_EQ(recording.heartRate(), _recData.heartRate);
    ASSERT_EQ(recording.referenceSystolic(), _recData.refSystolic);
    ASSERT_EQ(recording.referenceDiastolic(), _recData.refDiastolic);
    ASSERT_EQ(recording.referenceMean(), _recData.refMean);
    ASSERT_EQ(recording.sensorStrength(), _recData.signalStrength);
    ASSERT_EQ(recording.position().toStdString(), _recData.patientPosition);
    ASSERT_EQ(recording.notes().toStdString(), _recData.notes);
    ASSERT_EQ(recording.waveFormFile().toStdString(), _recData.waveformFile);
}

TEST_F(RecordingTests, ToRecordingDataReturnsCorrectValuesForRecording)
{
    Recording recording(_recData);

    RecordingData data = recording.toRecordingData();

    ASSERT_EQ(data.recordingId, _recData.recordingId);
    ASSERT_EQ(data.uploadPending, _recData.uploadPending);
    ASSERT_EQ(data.recordingDate, _recData.recordingDate);
    ASSERT_EQ(data.recordingTime, _recData.recordingTime);
    ASSERT_EQ(data.systolic, _recData.systolic);
    ASSERT_EQ(data.diastolic, _recData.diastolic);
    ASSERT_EQ(data.mean, _recData.mean);
    ASSERT_EQ(data.heartRate, _recData.heartRate);
    ASSERT_EQ(data.refSystolic, _recData.refSystolic);
    ASSERT_EQ(data.refDiastolic, _recData.refDiastolic);
    ASSERT_EQ(data.refMean, _recData.refMean);
    ASSERT_EQ(data.signalStrength, _recData.signalStrength);
    ASSERT_EQ(data.patientPosition, _recData.patientPosition);
    ASSERT_EQ(data.notes, _recData.notes);
    ASSERT_EQ(data.waveformFile, _recData.waveformFile);
}

TEST_F(RecordingTests, RecordingIdReturnsRecordingRecordingId)
{
    ASSERT_EQ(123456, _recording->recordingId());
}

TEST_F(RecordingTests, SetRecordingIdSetsTheRecordingId)
{
    _recording->setRecordingId(789012);
    ASSERT_EQ(789012, _recording->recordingId());
}

TEST_F(RecordingTests, SetRecordingIdEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setRecordingId(234);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, RecordingIdPropertyReturnsTheRecordingId)
{
    int id = _recording->property("recordingId").toInt();

    ASSERT_EQ(id, 123456);
}

TEST_F(RecordingTests, RecordingIdPropertySetsTheRecordingId)
{
    _recording->setProperty("recordingId", 234567);
    ASSERT_EQ(_recording->recordingId(), 234567);
}

TEST_F(RecordingTests, RecordingIdPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("recordingId");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, UploadPendingReturnsRecordingUploadPending)
{
    ASSERT_EQ(true, _recording->uploadPending());
}

TEST_F(RecordingTests, SetUploadPendingSetsTheUploadPending)
{
    _recording->setUploadPending(false);
    ASSERT_EQ(false, _recording->uploadPending());
}

TEST_F(RecordingTests, SetUploadPendingEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setUploadPending(false);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, UploadPendingPropertyReturnsTheUploadPending)
{
    bool value = _recording->property("uploadPending").toBool();

    ASSERT_EQ(value, true);
}

TEST_F(RecordingTests, UploadPendingPropertySetsTheUploadPending)
{
    _recording->setProperty("uploadPending", false);
    ASSERT_EQ(_recording->uploadPending(), false);
}

TEST_F(RecordingTests, UploadPendingPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("uploadPending");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, RecordingDateReturnsRecordingRecordingDate)
{
    ASSERT_EQ("date", _recording->recordingDate());
}

TEST_F(RecordingTests, SetRecordingDateSetsTheRecordingDate)
{
    _recording->setRecordingDate("120419");
    ASSERT_EQ("120419", _recording->recordingDate());
}

TEST_F(RecordingTests, SetRecordingDateEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setRecordingDate("recDate");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, RecordingDatePropertyReturnsTheRecordingDate)
{
    QString value = _recording->property("recordingDate").toString();

    ASSERT_EQ(value, "date");
}

TEST_F(RecordingTests, RecordingDatePropertySetsTheRecordingDate)
{
    _recording->setProperty("recordingDate", "recDate");
    ASSERT_EQ(_recording->recordingDate(), "recDate");
}

TEST_F(RecordingTests, RecordingDatePropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("recordingDate");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, RecordingTimeReturnsRecordingRecordingTime)
{
    ASSERT_EQ("time", _recording->recordingTime());
}

TEST_F(RecordingTests, SetRecordingTimeSetsTheRecordingTime)
{
    _recording->setRecordingTime("2245");
    ASSERT_EQ("2245", _recording->recordingTime());
}

TEST_F(RecordingTests, SetRecordingTimeEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setRecordingTime("recTime");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, RecordingTimePropertyReturnsTheRecordingTime)
{
    QString value = _recording->property("recordingTime").toString();

    ASSERT_EQ(value, "time");
}

TEST_F(RecordingTests, RecordingTimePropertySetsTheRecordingTime)
{
    _recording->setProperty("recordingTime", "recTime");
    ASSERT_EQ(_recording->recordingTime(), "recTime");
}

TEST_F(RecordingTests, RecordingTimePropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("recordingTime");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, SystolicReturnsRecordingSystolic)
{
    ASSERT_EQ(30, _recording->systolic());
}

TEST_F(RecordingTests, SetSystolicSetsTheSystolic)
{
    _recording->setSystolic(40);
    ASSERT_EQ(40, _recording->systolic());
}

TEST_F(RecordingTests, SetSystolicEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setSystolic(50);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, SystolicPropertyReturnsTheSystolic)
{
    int value = _recording->property("systolic").toInt();

    ASSERT_EQ(value, 30);
}

TEST_F(RecordingTests, SystolicPropertySetsTheSystolic)
{
    _recording->setProperty("systolic", 35);
    ASSERT_EQ(_recording->systolic(), 35);
}

TEST_F(RecordingTests, SystolicPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("systolic");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, DiastolicReturnsRecordingDiastolic)
{
    ASSERT_EQ(22, _recording->diastolic());
}

TEST_F(RecordingTests, SetDiastolicSetsTheDiastolic)
{
    _recording->setDiastolic(20);
    ASSERT_EQ(20, _recording->diastolic());
}

TEST_F(RecordingTests, SetDiastolicEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setDiastolic(25);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, DiastolicPropertyReturnsTheDiastolic)
{
    int value = _recording->property("diastolic").toInt();

    ASSERT_EQ(value, 22);
}

TEST_F(RecordingTests, DiastolicPropertySetsTheDiastolic)
{
    _recording->setProperty("diastolic", 25);
    ASSERT_EQ(_recording->diastolic(), 25);
}

TEST_F(RecordingTests, DiastolicPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("diastolic");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, MeanReturnsRecordingMean)
{
    ASSERT_EQ(26, _recording->mean());
}

TEST_F(RecordingTests, SetMeanSetsTheMean)
{
    _recording->setMean(28);
    ASSERT_EQ(28, _recording->mean());
}

TEST_F(RecordingTests, SetMeanEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setMean(29);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, MeanPropertyReturnsTheMean)
{
    int value = _recording->property("mean").toInt();

    ASSERT_EQ(value, 26);
}

TEST_F(RecordingTests, MeanPropertySetsTheMean)
{
    _recording->setProperty("mean", 28);
    ASSERT_EQ(_recording->mean(), 28);
}

TEST_F(RecordingTests, MeanPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("mean");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, HeartRateReturnsRecordingHeartRate)
{
    ASSERT_EQ(72, _recording->heartRate());
}

TEST_F(RecordingTests, SetHeartRateSetsTheHeartRate)
{
    _recording->setHeartRate(80);
    ASSERT_EQ(80, _recording->heartRate());
}

TEST_F(RecordingTests, SetHeartRateEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setHeartRate(75);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, HeartRatePropertyReturnsTheHeartRate)
{
    int value = _recording->property("heartRate").toInt();

    ASSERT_EQ(value, 72);
}

TEST_F(RecordingTests, HeartRatePropertySetsTheHeartRate)
{
    _recording->setProperty("heartRate", 70);
    ASSERT_EQ(_recording->heartRate(), 70);
}

TEST_F(RecordingTests, HeartRatePropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("heartRate");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, ReferenceSystolicReturnsRecordingReferenceSystolic)
{
    ASSERT_EQ(35, _recording->referenceSystolic());
}

TEST_F(RecordingTests, SetReferenceSystolicSetsTheReferenceSystolic)
{
    _recording->setReferenceSystolic(38);
    ASSERT_EQ(38, _recording->referenceSystolic());
}

TEST_F(RecordingTests, SetReferenceSystolicEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setReferenceSystolic(42);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, ReferenceSystolicPropertyReturnsTheReferenceSystolic)
{
    int value = _recording->property("referenceSystolic").toInt();

    ASSERT_EQ(value, 35);
}

TEST_F(RecordingTests, ReferenceSystolicPropertySetsTheReferenceSystolic)
{
    _recording->setProperty("referenceSystolic", 40);
    ASSERT_EQ(_recording->referenceSystolic(), 40);
}

TEST_F(RecordingTests, ReferenceSystolicPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("referenceSystolic");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, ReferenceDiastolicReturnsRecordingReferenceDiastolic)
{
    ASSERT_EQ(25, _recording->referenceDiastolic());
}

TEST_F(RecordingTests, SetReferenceDiastolicSetsTheReferenceDiastolic)
{
    _recording->setReferenceDiastolic(28);
    ASSERT_EQ(28, _recording->referenceDiastolic());
}

TEST_F(RecordingTests, SetReferenceDiastolicEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setReferenceDiastolic(32);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, ReferenceDiastolicPropertyReturnsTheReferenceDiastolic)
{
    int value = _recording->property("referenceDiastolic").toInt();

    ASSERT_EQ(value, 25);
}

TEST_F(RecordingTests, ReferenceDiastolicPropertySetsTheReferenceDiastolic)
{
    _recording->setProperty("referenceDiastolic", 30);
    ASSERT_EQ(_recording->referenceDiastolic(), 30);
}

TEST_F(RecordingTests, ReferenceDiastolicPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("referenceDiastolic");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, ReferenceMeanReturnsRecordingReferenceMean)
{
    ASSERT_EQ(28, _recording->referenceMean());
}

TEST_F(RecordingTests, SetReferenceMeanSetsTheReferenceMean)
{
    _recording->setReferenceMean(29);
    ASSERT_EQ(29, _recording->referenceMean());
}

TEST_F(RecordingTests, SetReferenceMeanEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setReferenceMean(27);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, ReferenceMeanPropertyReturnsTheReferenceMean)
{
    int value = _recording->property("referenceMean").toInt();

    ASSERT_EQ(value, 28);
}

TEST_F(RecordingTests, ReferenceMeanPropertySetsTheReferenceMean)
{
    _recording->setProperty("referenceMean", 30);
    ASSERT_EQ(_recording->referenceMean(), 30);
}

TEST_F(RecordingTests, ReferenceMeanPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("referenceMean");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, SensorStrengthReturnsRecordingSensorStrength)
{
    ASSERT_EQ(80, _recording->sensorStrength());
}

TEST_F(RecordingTests, SetSensorStrengthSetsTheSensorStrength)
{
    _recording->setSensorStrength(90);
    ASSERT_EQ(90, _recording->sensorStrength());
}

TEST_F(RecordingTests, SetSensorStrengthEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setSensorStrength(20);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, SensorStrengthPropertyReturnsTheSensorStrength)
{
    int value = _recording->property("sensorStrength").toInt();

    ASSERT_EQ(value, 80);
}

TEST_F(RecordingTests, SensorStrengthPropertySetsTheSensorStrength)
{
    _recording->setProperty("sensorStrength", 85);
    ASSERT_EQ(_recording->sensorStrength(), 85);
}

TEST_F(RecordingTests, SensorStrengthPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("sensorStrength");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, PositionReturnsRecordingPosition)
{
    ASSERT_EQ("left", _recording->position());
}

TEST_F(RecordingTests, SetPositionSetsThePosition)
{
    _recording->setPosition("right");
    ASSERT_EQ("right", _recording->position());
}

TEST_F(RecordingTests, SetPositionEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setPosition("right");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, PositionPropertyReturnsThePosition)
{
    QString value = _recording->property("position").toString();

    ASSERT_EQ(value, "left");
}

TEST_F(RecordingTests, PositionPropertySetsThePosition)
{
    _recording->setProperty("position", "right");
    ASSERT_EQ(_recording->position(), "right");
}

TEST_F(RecordingTests, PositionPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("position");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, NotesReturnsRecordingNotes)
{
    ASSERT_EQ("notes", _recording->notes());
}

TEST_F(RecordingTests, SetNotesSetsTheNotes)
{
    _recording->setNotes("recording notes");
    ASSERT_EQ("recording notes", _recording->notes());
}

TEST_F(RecordingTests, SetNotesEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setNotes("recording notes");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, NotesPropertyReturnsTheNotes)
{
    QString value = _recording->property("notes").toString();

    ASSERT_EQ(value, "notes");
}

TEST_F(RecordingTests, NotesPropertySetsTheNotes)
{
    _recording->setProperty("notes", "recording notes");
    ASSERT_EQ(_recording->notes(), "recording notes");
}

TEST_F(RecordingTests, NotesPropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("notes");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}

TEST_F(RecordingTests, WaveFormFileReturnsRecordingWaveFormFileName)
{
    ASSERT_EQ("file", _recording->waveFormFile());
}

TEST_F(RecordingTests, SetWaveFormFileSetsTheWaveFormFile)
{
    _recording->setWaveFormFile("fileName");
    ASSERT_EQ("fileName", _recording->waveFormFile());
}

TEST_F(RecordingTests, SetWaveFormFileEmitsRecordingChangedSignal)
{
    QSignalSpy spy(_recording.get(), SIGNAL(recordingChanged()));
    _recording->setWaveFormFile("fileName");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingTests, WaveFormFilePropertyReturnsTheWaveFormFile)
{
    QString value = _recording->property("waveFormFile").toString();

    ASSERT_EQ(value, "file");
}

TEST_F(RecordingTests, WaveFormFilePropertySetsTheWaveFormFile)
{
    _recording->setProperty("waveFormFile", "file name");
    ASSERT_EQ(_recording->waveFormFile(), "file name");
}

TEST_F(RecordingTests, WaveFormFilePropertyHasNotifySignal)
{
    int propertyIndex = _recording->metaObject()->indexOfProperty("waveFormFile");
    QMetaProperty property = _recording->metaObject()->property(propertyIndex);

    ASSERT_STREQ("recordingChanged", property.notifySignal().name().data());
}
