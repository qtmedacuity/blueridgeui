#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmem_command.h>

#include "unitTests/mocks/MockTimer.h"
#include "unitTests/mocks/MockMessageParser.h"
#include "providers/CellularStrengthProvider.h"

using namespace ::testing;

class CellularStrengthProviderTests : public Test
{
public:
    CellularStrengthProviderTests()
    {
        _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                         "Up"
                         "\",\n\t\"State\" : \""
                         "Enabled"
                         "\",\n\t\"Carrier\" : \""
                         "Cellular Carrier"
                         "\",\n\t\"SignalStrength\" : \""
                         "25"
                         "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";
    }

    ~CellularStrengthProviderTests() override;

    std::string _cellularStatusStr;
    std::condition_variable _condition;
};

CellularStrengthProviderTests::~CellularStrengthProviderTests() {}

TEST_F(CellularStrengthProviderTests, ConstructorCallsMessageParserRegisterCallback)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();

    EXPECT_CALL(parser, registerCallback(_))
            .Times(1);

    CellularStrengthProvider provider(timer, parser);
}

TEST_F(CellularStrengthProviderTests, GetStrengthReturnsZeroIfSignalStrengthNotSetInCellularStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback( [&]{ _condition.notify_one(); });

    _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"Carrier\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, provider.getStrength());
}

TEST_F(CellularStrengthProviderTests, GetStrengthReturnsASignalStrengthFromTheCellularStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(25, provider.getStrength());
}

TEST_F(CellularStrengthProviderTests, GetStrengthReturnsADifferentSignalStrengthFromTheCellularStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"Carrier\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     "70"
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));

    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(70, provider.getStrength());
}

TEST_F(CellularStrengthProviderTests, getCarrierReturnsAnEmptyStringIfCarrierNotSetInCellularStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"Carrier\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("", provider.getCarrier());
}

TEST_F(CellularStrengthProviderTests, getCarrierReturnsACarrierFromTheCellularStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("Cellular Carrier", provider.getCarrier());
}

TEST_F(CellularStrengthProviderTests, getCarrierReturnsADifferentCarrierFromTheCellularStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"Carrier\" : \""
                     "Carrier"
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("Carrier", provider.getCarrier());
}

TEST_F(CellularStrengthProviderTests, IsConnectedReturnsTrueIfCellularStatusIsSetToUp)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.isConnected());
}

TEST_F(CellularStrengthProviderTests, IsConnectedReturnsFalseIfCellularStatusIsSetToDown)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                     "Down"
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"Carrier\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));

    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isConnected());
}

TEST_F(CellularStrengthProviderTests, IsConnectedReturnsFalseIfCellularStatusIsNotSet)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"Carrier\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));

    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isConnected());
}

TEST_F(CellularStrengthProviderTests, IsEnabledReturnsTrueIfStateIsSetToEnabled)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.isEnabled());
}

TEST_F(CellularStrengthProviderTests, IsEnabledReturnsFalseIfStateIsSetToDisabled)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     "Disabled"
                     "\",\n\t\"Carrier\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));

    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isEnabled());
}

TEST_F(CellularStrengthProviderTests, IsEnabledReturnsFalseIfStateIsNotSet)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"Carrier\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));

    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isEnabled());
}

TEST_F(CellularStrengthProviderTests, IsSetReturnsTrueIfCellularStatusValuesAreSet)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.isSet());
}

TEST_F(CellularStrengthProviderTests, IsSetReturnsFalseIfCellularStatusValuesAreNotSet)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    provider.registerCellularStrengthCallback([&]{ _condition.notify_one(); });

    _cellularStatusStr = "{\n\t\"CellStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"Carrier\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isSet());
}

TEST_F(CellularStrengthProviderTests, CallbackRegisterWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    unsigned long count = 0;
    provider.registerCellularStrengthCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}

TEST_F(CellularStrengthProviderTests, CallbacksRegisteredWithProviderAreExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    unsigned long count = 0;
    provider.registerCellularStrengthCallback([&]{ count++; });

    std::string value = "";
    provider.registerCellularStrengthCallback([&]{ value = "value changed"; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
    ASSERT_EQ("value changed", value);
}

TEST_F(CellularStrengthProviderTests, UnregisteredCallbacksAreNotExecutedByTheProvider)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    unsigned long count = 0;
    unsigned long cmdId = provider.registerCellularStrengthCallback([&]{ count++; });

    std::string value = "";
    provider.registerCellularStrengthCallback([&]{ value = "value"; _condition.notify_one(); });

    provider.unregisterCellularStrengthCallback(cmdId);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_cellularStatusStr));
    parser._fake._func(eGET_CELL_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count);
    ASSERT_EQ("value", value);
}

TEST_F(CellularStrengthProviderTests, CallbacksFromParserOfDifferentCommandTypeAreNotHandledByProvider)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    CellularStrengthProvider provider(timer, parser);

    unsigned long count = 0;
    provider.registerCellularStrengthCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("unkown string"));
    parser._fake._func(eUNKNOWN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(0, count);
}
