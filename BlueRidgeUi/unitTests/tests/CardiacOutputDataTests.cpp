#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "providers/CardiacOutputData.h"

using namespace ::testing;

class CardiacOutputDataTests : public Test
{

public:
    CardiacOutputDataTests() {}
    ~CardiacOutputDataTests() override;
};

CardiacOutputDataTests::~CardiacOutputDataTests() {}


TEST_F(CardiacOutputDataTests, DefaultConstructorDefaultsValuesToZero)
{
    CardiacOutputData coData;

    ASSERT_EQ(0.0, coData.cardiacOutput);
    ASSERT_EQ(0, coData.heartRate);
    ASSERT_EQ(0, coData.signalStrength);
}


TEST_F(CardiacOutputDataTests, ConstructorSetsCardiacOutputDataValuesFromCmemCardiacOutputValues)
{

    cmem_cardiacOutput cmemCo;
    cmemCo.add_field("Date", "23 May 2019");
    cmemCo.add_field("Time", "09:45:30");
    cmemCo.add_field("CardiacOutput", "2.3");
    cmemCo.add_field("HeartRate", "78");
    cmemCo.add_field("SignalStrength", "98");
    cmemCo.add_field("WaveformData", "valid_file.xml");

    CardiacOutputData coData(cmemCo);

    ASSERT_EQ("23 May 2019", coData.date);
    ASSERT_EQ("09:45:30", coData.time);
    ASSERT_EQ(2.3F, coData.cardiacOutput);
    ASSERT_EQ(78, coData.heartRate);
    ASSERT_EQ(98, coData.signalStrength);
    ASSERT_EQ("valid_file.xml", coData.waveformFile);

}


TEST_F(CardiacOutputDataTests, ToCmemCardiacOutputReturnsACmemCardiacOutputForACardiacOutputData)
{
    CardiacOutputData coData;
    coData.date = "23 May 2019";
    coData.time = "11:33:22";
    coData.cardiacOutput = 3.3F;
    coData.heartRate = 75;
    coData.signalStrength = 91;
    coData.waveformFile = "file";

    cmem_cardiacOutput cmemCo = coData.toCmemCardiacOutput();

    ASSERT_EQ(cmemCo.getDate().fmt_date(), coData.date);
    ASSERT_EQ(cmemCo.getTime().fmt_time(), coData.time);
    ASSERT_EQ(cmemCo.getCO(), coData.cardiacOutput);
    ASSERT_EQ(cmemCo.avgSignalStrength(), coData.signalStrength);
    ASSERT_EQ(cmemCo.waveformPath(), coData.waveformFile);
}

