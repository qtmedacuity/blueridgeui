#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QCoreApplication>
#include <QSignalSpy>
#include <QLineSeries>

#include "unitTests/mocks/MockSensorDataProvider.h"
#include "presentation/ActiveChartController.h"
#include "providers/ActiveObject.h"

#include <condition_variable>
#include <chrono>
#include <mutex>

using namespace QtCharts;
using namespace ::testing;

class ActiveChartControllerTests : public Test
{
public:
    ActiveChartControllerTests()
        : _fileName("testAxisConfig.json"),
          _controller(_provider, _fileName)
    {}
    ~ActiveChartControllerTests() override;

    void SetUp() override
    {
        _series.setName(QString("test_series"));
    }

    void sendPoint(unsigned long seq, double pressure)
    {
        _provider._fake._sensorDataPointCallback(seq, pressure);
        std::this_thread::sleep_for(std::chrono::milliseconds(8));
    }

    QString _fileName;
    QLineSeries _series;
    NiceMock<MockSensorDataProvider> _provider;
    ActiveChartController _controller;
};


ActiveChartControllerTests::~ActiveChartControllerTests()
{}

TEST_F(ActiveChartControllerTests, ConstructorSetsDefaultValues)
{
    ActiveChartController controller(_provider, _fileName);

    ASSERT_EQ(controller.verticalOffset(), 0.0);
    ASSERT_EQ(controller.horizontalOffset(), 0.0);
    ASSERT_EQ(controller.zoomInVerticalEnabled(), true);
    ASSERT_EQ(controller.zoomOutVerticalEnabled(), false);
    ASSERT_EQ(controller.zoomInHorizontalEnabled(), false);
    ASSERT_EQ(controller.zoomOutHorizontalEnabled(), false);
    ASSERT_EQ(controller.gapInSeconds(), 0.3);
    ASSERT_EQ(controller.rangeInSeconds(), 10.0);
}

TEST_F(ActiveChartControllerTests, ConstructorRegistersCallbacksWithProvider)
{
    EXPECT_CALL(_provider, registerDataRateCallback(_))
            .Times(1);

    EXPECT_CALL(_provider, registerSensorDataPointCallback(_))
            .Times(1);

    ActiveChartController controller(_provider, _fileName);
}

TEST_F(ActiveChartControllerTests, RangeInSecondsReturnsTheValueOfRangeInSeconds)
{
    ASSERT_EQ(_controller.rangeInSeconds(), 10.0);
}

TEST_F(ActiveChartControllerTests, SetRangeInSecondsSetsTheValueOfRangeInSeconds)
{
    _controller.setRangeInSeconds(20.0);
    ASSERT_EQ(_controller.rangeInSeconds(), 20.0);
}

TEST_F(ActiveChartControllerTests, SetRangeInSecondsEmitsRangeInSecondsChangedSignal)
{
    QSignalSpy spy(&_controller, SIGNAL(rangeInSecondsChanged(double)));

    _controller.setRangeInSeconds(20.0);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(ActiveChartControllerTests, RangeInSecondsEnabledPropertyReturnsTheRangeInSecondsdValue)
{
    double value = _controller.property("rangeInSeconds").toDouble();
    ASSERT_EQ(value, 10.0);
}

TEST_F(ActiveChartControllerTests, RangeInSecondsEnabledPropertySetsTheRangeInSecondsdValue)
{
    _controller.setProperty("rangeInSeconds", 15.0);
    ASSERT_EQ(_controller.rangeInSeconds(), 15.0);
}

TEST_F(ActiveChartControllerTests, RangeInSecondsPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("rangeInSeconds");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("rangeInSecondsChanged", property.notifySignal().name().data());
}

TEST_F(ActiveChartControllerTests, GapInSecondsReturnsTheValueOfGapInSeconds)
{
    ASSERT_EQ(_controller.gapInSeconds(), 0.3);
}

TEST_F(ActiveChartControllerTests, SetGapInSecondsSetsTheValueOfGapInSeconds)
{
    _controller.setGapInSeconds(1.0);
    ASSERT_EQ(_controller.gapInSeconds(), 1.0);
}

TEST_F(ActiveChartControllerTests, SetGapInSecondsEmitsGapInSecondsChangedSignal)
{
    QSignalSpy spy(&_controller, SIGNAL(gapInSecondsChanged(double)));

    _controller.setGapInSeconds(2.0);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(ActiveChartControllerTests, GapInSecondsEnabledPropertyReturnsTheGapInSecondsdValue)
{
    double value = _controller.property("gapInSeconds").toDouble();
    ASSERT_EQ(value, 0.3);
}

TEST_F(ActiveChartControllerTests, GapInSecondsEnabledPropertySetssTheGapInSecondsdValue)
{
    _controller.setProperty("gapInSeconds", 0.5);
    ASSERT_EQ(_controller.gapInSeconds(), 0.5);
}

TEST_F(ActiveChartControllerTests, GapInSecondsPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("gapInSeconds");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("gapInSecondsChanged", property.notifySignal().name().data());
}

TEST_F(ActiveChartControllerTests, SeriesHasCorrectData)
{
    _provider.DelegateToFake();

    ActiveChartController controller(_provider, _fileName);
    controller.registerSeries(&_series);
    _provider._fake._dataRateCallback(125);

    std::condition_variable condition;
    ActiveObject actObj;

    actObj.send([&] { condition.notify_all(); });
    for (unsigned long ii(10); ii < 20; ++ii) {
        actObj.send([&, ii] { sendPoint(ii, static_cast<double>(ii)); });
        QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
    }
    actObj.send([&] { condition.notify_all(); });

    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        condition.wait_for(lock, std::chrono::seconds(5));
    }
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(10, _series.count());
    ASSERT_DOUBLE_EQ(0.0, _series.at(0).x());
    ASSERT_DOUBLE_EQ(10.0, _series.at(0).y());
    ASSERT_DOUBLE_EQ(0.008, _series.at(1).x());
    ASSERT_DOUBLE_EQ(11.0, _series.at(1).y());
    ASSERT_DOUBLE_EQ(0.016, _series.at(2).x());
    ASSERT_DOUBLE_EQ(12.0, _series.at(2).y());
    ASSERT_DOUBLE_EQ(0.024, _series.at(3).x());
    ASSERT_DOUBLE_EQ(13.0, _series.at(3).y());
    ASSERT_DOUBLE_EQ(0.032, _series.at(4).x());
    ASSERT_DOUBLE_EQ(14.0, _series.at(4).y());
    ASSERT_DOUBLE_EQ(0.040, _series.at(5).x());
    ASSERT_DOUBLE_EQ(15.0, _series.at(5).y());
    ASSERT_DOUBLE_EQ(0.048, _series.at(6).x());
    ASSERT_DOUBLE_EQ(16.0, _series.at(6).y());
    ASSERT_DOUBLE_EQ(0.056, _series.at(7).x());
    ASSERT_DOUBLE_EQ(17.0, _series.at(7).y());
    ASSERT_DOUBLE_EQ(0.064, _series.at(8).x());
    ASSERT_DOUBLE_EQ(18.0, _series.at(8).y());
    ASSERT_DOUBLE_EQ(0.072, _series.at(9).x());
    ASSERT_DOUBLE_EQ(19.0, _series.at(9).y());
}

TEST_F(ActiveChartControllerTests, SeriesHasCorrectDataWithASequenceRollover)
{
    _provider.DelegateToFake();

    ActiveChartController controller(_provider, _fileName);
    controller.registerSeries(&_series);
    _provider._fake._dataRateCallback(125);

    std::condition_variable condition;
    ActiveObject actObj;


    actObj.send([&] { condition.notify_all(); });
    for (unsigned long ii(20); ii < 25; ++ii) {
        actObj.send([&, ii] { sendPoint(ii, static_cast<double>(ii)); });
        QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
    }
    for (unsigned long ii(0); ii < 5; ++ii) {
        actObj.send([&, ii] { sendPoint(ii, static_cast<double>(ii)); });
        QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
    }
    actObj.send([&] { condition.notify_all(); });

    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        condition.wait_for(lock, std::chrono::seconds(5));
    }
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(10, _series.count());
    ASSERT_DOUBLE_EQ(0.0, _series.at(0).x());
    ASSERT_DOUBLE_EQ(20.0, _series.at(0).y());
    ASSERT_DOUBLE_EQ(0.008, _series.at(1).x());
    ASSERT_DOUBLE_EQ(21.0, _series.at(1).y());
    ASSERT_DOUBLE_EQ(0.016, _series.at(2).x());
    ASSERT_DOUBLE_EQ(22.0, _series.at(2).y());
    ASSERT_DOUBLE_EQ(0.024, _series.at(3).x());
    ASSERT_DOUBLE_EQ(23.0, _series.at(3).y());
    ASSERT_DOUBLE_EQ(0.032, _series.at(4).x());
    ASSERT_DOUBLE_EQ(24.0, _series.at(4).y());
    ASSERT_DOUBLE_EQ(0.0, _series.at(5).x());
    ASSERT_DOUBLE_EQ(0.0, _series.at(5).y());
    ASSERT_DOUBLE_EQ(0.008, _series.at(6).x());
    ASSERT_DOUBLE_EQ(1.0, _series.at(6).y());
    ASSERT_DOUBLE_EQ(0.016, _series.at(7).x());
    ASSERT_DOUBLE_EQ(2.0, _series.at(7).y());
    ASSERT_DOUBLE_EQ(0.024, _series.at(8).x());
    ASSERT_DOUBLE_EQ(3.0, _series.at(8).y());
    ASSERT_DOUBLE_EQ(0.032, _series.at(9).x());
    ASSERT_DOUBLE_EQ(4.0, _series.at(9).y());
}

TEST_F(ActiveChartControllerTests, RefreshClearsSeriesData)
{
    _provider.DelegateToFake();

    ActiveChartController controller(_provider, _fileName);
    controller.registerSeries(&_series);
    _provider._fake._dataRateCallback(125);

    std::condition_variable condition;
    ActiveObject actObj;

    actObj.send([&] { condition.notify_all(); });
    for (unsigned long ii(5); ii < 10; ++ii) {
        actObj.send([&, ii] { sendPoint(ii, static_cast<double>(ii)); });
        QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
    }
    actObj.send([&] { condition.notify_all(); });

    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        condition.wait_for(lock, std::chrono::seconds(5));
    }
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(5, _series.count());
    ASSERT_DOUBLE_EQ(0.0, _series.at(0).x());
    ASSERT_DOUBLE_EQ(5.0, _series.at(0).y());
    ASSERT_DOUBLE_EQ(0.008, _series.at(1).x());
    ASSERT_DOUBLE_EQ(6.0, _series.at(1).y());
    ASSERT_DOUBLE_EQ(0.016, _series.at(2).x());
    ASSERT_DOUBLE_EQ(7.0, _series.at(2).y());
    ASSERT_DOUBLE_EQ(0.024, _series.at(3).x());
    ASSERT_DOUBLE_EQ(8.0, _series.at(3).y());
    ASSERT_DOUBLE_EQ(0.032, _series.at(4).x());
    ASSERT_DOUBLE_EQ(9.0, _series.at(4).y());

    controller.refresh();
    actObj.send([&] { condition.notify_all(); });

    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        condition.wait_for(lock, std::chrono::seconds(5));
    }
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(0, _series.count());
}

TEST_F(ActiveChartControllerTests, SeriesHasDataRemovedAccoringToWaveGaps)
{
    _provider.DelegateToFake();
    QLineSeries altSeries;
    altSeries.setName("alt_series");

    ActiveChartController controller(_provider, _fileName);
    controller.setRangeInSeconds(1);
    controller.registerSeries(&_series);
    controller.registerSeries(&altSeries);
    _provider._fake._dataRateCallback(10);

    std::condition_variable condition;
    ActiveObject actObj;

    actObj.send([&] { condition.notify_all(); });
    for (unsigned long ii(0); ii < 15; ++ii) {
        actObj.send([&, ii] { sendPoint(ii, static_cast<double>(ii)); });
        QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
    }
    actObj.send([&] { condition.notify_all(); });

    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        condition.wait_for(lock, std::chrono::seconds(5));
    }
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(3, _series.count());

    ASSERT_DOUBLE_EQ(0.7, _series.at(0).x());
    ASSERT_DOUBLE_EQ(7.0, _series.at(0).y());
    ASSERT_DOUBLE_EQ(0.8, _series.at(1).x());
    ASSERT_DOUBLE_EQ(8.0, _series.at(1).y());
    ASSERT_DOUBLE_EQ(0.9, _series.at(2).x());
    ASSERT_DOUBLE_EQ(9.0, _series.at(2).y());

    ASSERT_EQ(5, altSeries.count());
    ASSERT_DOUBLE_EQ(0.0, altSeries.at(0).x());
    ASSERT_DOUBLE_EQ(10.0, altSeries.at(0).y());
    ASSERT_DOUBLE_EQ(0.1, altSeries.at(1).x());
    ASSERT_DOUBLE_EQ(11.0, altSeries.at(1).y());
    ASSERT_DOUBLE_EQ(0.2, altSeries.at(2).x());
    ASSERT_DOUBLE_EQ(12.0, altSeries.at(2).y());
    ASSERT_DOUBLE_EQ(0.3, altSeries.at(3).x());
    ASSERT_DOUBLE_EQ(13.0, altSeries.at(3).y());
    ASSERT_DOUBLE_EQ(0.4, altSeries.at(4).x());
    ASSERT_DOUBLE_EQ(14.0, altSeries.at(4).y());
}


