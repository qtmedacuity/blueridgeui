#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "providers/RhcData.h"
#include <cmem_rhc.h>

using namespace ::testing;

class RhcDataTests : public Test
{
public:
    RhcDataTests()
    {}
    ~RhcDataTests() override;

    void SetUp() override
    {}
};

RhcDataTests::~RhcDataTests()
{}

TEST_F(RhcDataTests, RhcDataDefaultConstructorInitializesToAZeroState)
{
    RhcData rhc;

    ASSERT_STREQ("", rhc.date.c_str());
    ASSERT_STREQ("", rhc.time.c_str());
    ASSERT_EQ(0, rhc.ra);
    ASSERT_EQ(0, rhc.rvSystolic);
    ASSERT_EQ(0, rhc.rvDiastoic);
    ASSERT_EQ(0, rhc.paSystolic);
    ASSERT_EQ(0, rhc.paDiastolic);
    ASSERT_EQ(0, rhc.paMean);
    ASSERT_EQ(0, rhc.pcwp);
    ASSERT_EQ(0, rhc.heartRate);
    ASSERT_EQ(0, rhc.signalStrength);
    ASSERT_STREQ("", rhc.notes.c_str());
    ASSERT_STREQ("", rhc.waveformFile.c_str());
    ASSERT_STREQ("", rhc.position.c_str());
}

TEST_F(RhcDataTests, RhcDataInitializeConstructorInitializesToACorrectState)
{
    // TODO: Bug QM-187
    //       cmem_rhc contains several unused private member variables with matching public
    //       accessors, but without matching mutators.  The constructor of cmem_rhc leaves
    //       these unsused variables in an undefined state.  Since these variables cannot be
    //       initialized or modified, they are not copied to the RhcData structure.
    //       Unused cmem_rhc members: mRecordingId, mDate, mTime, mAverageHeartRate, nNotes,
    //                                mAverageSignalStrength, mPosition, and mWaveformData.
    cmem_rhc cmem;
    cmem.add_field("PAMean", "1");
    cmem.add_field("PADiastolic", "2");
    cmem.add_field("PASystolic", "3");
    cmem.add_field("PCWP", "4");
    cmem.add_field("RVDiastolic", "5");
    cmem.add_field("RVSystolic", "6");
    cmem.add_field("RA", "7");

    RhcData rhc(cmem);

    ASSERT_EQ(7, rhc.ra);
    ASSERT_EQ(6, rhc.rvSystolic);
    ASSERT_EQ(5, rhc.rvDiastoic);
    ASSERT_EQ(3, rhc.paSystolic);
    ASSERT_EQ(2, rhc.paDiastolic);
    ASSERT_EQ(1, rhc.paMean);
    ASSERT_EQ(4, rhc.pcwp);
}

TEST_F(RhcDataTests, ToCmemRhcSetsTheCmemRhcToACorrectState)
{
    // TODO: Bug QM-187
    //       cmem_rhc contains several unused private member variables with matching public
    //       accessors, but without matching mutators.  The constructor of cmem_rhc leaves
    //       these unsused variables in an undefined state.  Since these variables cannot be
    //       initialized or modified, they are not copied to the RhcData structure.
    //       Unused cmem_rhc members: mRecordingId, mDate, mTime, mAverageHeartRate, nNotes,
    //                                mAverageSignalStrength, mPosition, and mWaveformData.
    cmem_rhc cmem;
    ASSERT_EQ(0, cmem.getRA());
    ASSERT_EQ(0, cmem.getRVSystolic());
    ASSERT_EQ(0, cmem.getRVDiastolic());
    ASSERT_EQ(0, cmem.getPCWP());
    ASSERT_EQ(0, cmem.getPASystolic());
    ASSERT_EQ(0, cmem.getPADiastolic());
    ASSERT_EQ(0, cmem.getPAMean());

    RhcData rhc;
    rhc.ra = 99;
    rhc.rvSystolic = 88;
    rhc.rvDiastoic = 77;
    rhc.paSystolic = 66;
    rhc.paDiastolic = 55;
    rhc.paMean = 44;
    rhc.pcwp = 33;

    rhc.toCmemRhc(cmem);
    ASSERT_EQ(99, cmem.getRA());
    ASSERT_EQ(88, cmem.getRVSystolic());
    ASSERT_EQ(77, cmem.getRVDiastolic());
    ASSERT_EQ(33, cmem.getPCWP());
    ASSERT_EQ(66, cmem.getPASystolic());
    ASSERT_EQ(55, cmem.getPADiastolic());
    ASSERT_EQ(44, cmem.getPAMean());
}

