#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "widgets/WaveChart/WaveAxisPoint.h"

using namespace ::testing;

class WaveAxisPointTests : public Test
{
public:
    WaveAxisPointTests()
        : _point(1.1, "point", 2, 3, 4)
    {}
    ~WaveAxisPointTests() override;

    WaveAxisPoint _point;
};

WaveAxisPointTests::~WaveAxisPointTests() {}

TEST_F(WaveAxisPointTests, ConstructorSetsDefaultValues)
{
    WaveAxisPoint point;
    ASSERT_EQ(point.value(), 0.0);
    ASSERT_EQ(point.label(), "");
    ASSERT_EQ(point.gridWidth(), 1);
    ASSERT_EQ(point.tickWidth(), 1);
    ASSERT_EQ(point.tickLength(), 5);
    ASSERT_EQ(point.coord(), std::make_tuple(0.0,0.0));
}

TEST_F(WaveAxisPointTests, ConstructorSetsValuesToValuesPassed)
{
    WaveAxisPoint point(5.5, "label", 2, 3, 4);
    ASSERT_EQ(point.value(), 5.5);
    ASSERT_EQ(point.label(), "label");
    ASSERT_EQ(point.tickWidth(), 3);
    ASSERT_EQ(point.tickLength(), 2);
    ASSERT_EQ(point.gridWidth(), 4);
    ASSERT_EQ(point.coord(), std::make_tuple(0.0,0.0));
}

TEST_F(WaveAxisPointTests, CopyConstructorCopiesObjectToAnotherObject)
{
    WaveAxisPoint point(_point);
    ASSERT_EQ(point.value(), _point.value());
    ASSERT_EQ(point.label(), _point.label());
    ASSERT_EQ(point.tickWidth(), _point.tickWidth());
    ASSERT_EQ(point.tickLength(), _point.tickLength());
    ASSERT_EQ(point.gridWidth(), _point.gridWidth());
    ASSERT_EQ(point.coord(), _point.coord());
}

TEST_F(WaveAxisPointTests, MoveConstructorMovesObjectToAnotherObject)
{
    WaveAxisPoint point(std::move(_point));
    ASSERT_EQ(point.value(), 1.1);
    ASSERT_EQ(point.label(), "point");
    ASSERT_EQ(point.gridWidth(), 4);
    ASSERT_EQ(point.tickWidth(), 3);
    ASSERT_EQ(point.tickLength(), 2);
    ASSERT_EQ(point.coord(), std::make_tuple(0.0,0.0));
}

TEST_F(WaveAxisPointTests, AssignmentOperatorAssignsObjectToAnotherObject)
{
    WaveAxisPoint point1(5.5, "label", 2, 3, 4);
    WaveAxisPoint point2;
    point1 = point2;

    ASSERT_EQ(point1.value(), point2.value());
    ASSERT_EQ(point1.label(), point2.label());
    ASSERT_EQ(point1.tickWidth(), point2.tickWidth());
    ASSERT_EQ(point1.tickLength(), point2.tickLength());
    ASSERT_EQ(point1.gridWidth(), point2.gridWidth());
    ASSERT_EQ(point1.coord(), point2.coord());
}

TEST_F(WaveAxisPointTests, MoveAssignmentOperatorMovesObjectToAnotherObject)
{
    WaveAxisPoint point;
    point = std::move(_point);

    ASSERT_EQ(point.value(), 1.1);
    ASSERT_EQ(point.label(), "point");
    ASSERT_EQ(point.gridWidth(), 4);
    ASSERT_EQ(point.tickWidth(), 3);
    ASSERT_EQ(point.tickLength(), 2);
    ASSERT_EQ(point.coord(), std::make_tuple(0.0,0.0));
}

TEST_F(WaveAxisPointTests, EqualComparisonOperatorReturnsIfValueIsEqual)
{
     WaveAxisPoint point(1.1, "label", 2, 3, 4);

     ASSERT_TRUE(point == _point);
}

TEST_F(WaveAxisPointTests, EqualComparisonOperatorReturnsIfValueISNotEqual)
{
     WaveAxisPoint point(2.2, "label", 2, 3, 4);

     ASSERT_FALSE(point == _point);
}

TEST_F(WaveAxisPointTests, LargerThanOperatorReturnsTrueIfLValueLargerThanRValue)
{
    WaveAxisPoint point(5.0, "label", 2, 3, 4);

    ASSERT_TRUE(point > _point);
}

TEST_F(WaveAxisPointTests, LargerThanOperatorReturnsFalseIfLValueLessThanRValue)
{
    WaveAxisPoint point(0.7, "label", 2, 3, 4);

    ASSERT_FALSE(point > _point);
}

TEST_F(WaveAxisPointTests, LargerThanOrEqualOperatorReturnsFalseIfLValueLessThanRValue)
{
    WaveAxisPoint point(0.7, "label", 2, 3, 4);

    ASSERT_FALSE(point >= _point);
}

TEST_F(WaveAxisPointTests, LargerThanOrEqualOperatorReturnsTrueIfLValueLargerThanRValue)
{
    WaveAxisPoint point(5.0, "label", 2, 3, 4);

    ASSERT_TRUE(point >= _point);
}

TEST_F(WaveAxisPointTests, LargerThanOrEqualOperatorReturnsTrueIfLValueEqualsRValue)
{
    WaveAxisPoint point(1.1, "label", 2, 3, 4);

    ASSERT_TRUE(point >= _point);
}

TEST_F(WaveAxisPointTests, LessThanOperatorReturnsTrueIfLValueLessThanRValue)
{
    WaveAxisPoint point(1.0, "label", 2, 3, 4);

    ASSERT_TRUE(point < _point);
}

TEST_F(WaveAxisPointTests, LessThanOperatorReturnsFalseIfLValueLargerThanRValue)
{
    WaveAxisPoint point(2.0, "label", 2, 3, 4);

    ASSERT_FALSE(point < _point);
}

TEST_F(WaveAxisPointTests, LessThanOrEqualOperatorReturnsTrueIfLValueLessThanRValue)
{
    WaveAxisPoint point(0.7, "label", 2, 3, 4);

    ASSERT_TRUE(point <= _point);
}

TEST_F(WaveAxisPointTests, LessThanOrEqualOperatorReturnsFalseIfLValueLargerThanRValue)
{
    WaveAxisPoint point(5.0, "label", 2, 3, 4);

    ASSERT_FALSE(point <= _point);
}

TEST_F(WaveAxisPointTests, LessThanOrEqualOperatorReturnsTrueIfLValueEqualsRValue)
{
    WaveAxisPoint point(1.1, "label", 2, 3, 4);

    ASSERT_TRUE(point <= _point);
}

TEST_F(WaveAxisPointTests, ValueReturnsTheCorrectValue)
{
    ASSERT_EQ(_point.value(), 1.1);
}

TEST_F(WaveAxisPointTests, SetValueSetsThePointValueToCorrectValue)
{
    _point.setValue(7.0);

    ASSERT_EQ(_point.value(), 7.0);
}

TEST_F(WaveAxisPointTests, LabelReturnsTheCorrectValue)
{
    ASSERT_EQ(_point.label(), "point");
}

TEST_F(WaveAxisPointTests, SetLabelSetsThePointLabelToCorrectValue)
{
    _point.setLabel("label");

    ASSERT_EQ(_point.label(), "label");
}

TEST_F(WaveAxisPointTests, GridWidthReturnsTheCorrectValue)
{
    ASSERT_EQ(_point.gridWidth(), 4);
}

TEST_F(WaveAxisPointTests, SetGridWidthSetsTheGridWidthToTheCorrectValue)
{
    _point.setGridWidth(10);

    ASSERT_EQ(_point.gridWidth(), 10);
}

TEST_F(WaveAxisPointTests, SetGridWidthSetsTheGridWidthToZeroIfValuePassedLessThanZero)
{
    _point.setGridWidth(-10);

    ASSERT_EQ(_point.gridWidth(), 0);
}

TEST_F(WaveAxisPointTests, tickWidthReturnsTheCorrectValue)
{
    ASSERT_EQ(_point.tickWidth(), 3);
}

TEST_F(WaveAxisPointTests, SetTickWidthSetsTheTickWidthToTheCorrectValue)
{
    _point.setTickWidth(9);

    ASSERT_EQ(_point.tickWidth(), 9);
}

TEST_F(WaveAxisPointTests, SetTickWidthSetsTheTickWidthToZeroIfValuePassedLessThanZero)
{
    _point.setTickWidth(-9);

    ASSERT_EQ(_point.tickWidth(), 0);
}

TEST_F(WaveAxisPointTests, tickLengthReturnsTheCorrectValue)
{
    ASSERT_EQ(_point.tickLength(), 2);
}

TEST_F(WaveAxisPointTests, SetTickLengthSetsTheTickLengthToTheCorrectValue)
{
    _point.setTickLength(8);

    ASSERT_EQ(_point.tickLength(), 8);
}

TEST_F(WaveAxisPointTests, SetTickLengthSetsTheTickLengthToZeroIfValuePassedLessThanZero)
{
    _point.setTickLength(-8);

    ASSERT_EQ(_point.tickLength(), 0);
}

TEST_F(WaveAxisPointTests, CoordReturnsTheCorrectValue)
{
    WaveAxisPoint::Coord coord = std::make_tuple(0.0, 0.0);
    ASSERT_EQ(_point.coord(), coord);
}

TEST_F(WaveAxisPointTests, SetCoordSetsThePointCoordToCoordValuePassed)
{
    WaveAxisPoint::Coord coord = std::make_tuple(2.0, 5.0);

    _point.setCoord(coord);
    ASSERT_EQ(_point.coord(), coord);
}

TEST_F(WaveAxisPointTests, SetCoordSetsThePointCoordToCoorectValues)
{
    WaveAxisPoint::Coord coord = std::make_tuple(2.0, 5.0);

    _point.setCoord(2.0, 5.0);
    ASSERT_EQ(_point.coord(), coord);
}






















