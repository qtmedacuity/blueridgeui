#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmem_command.h>

#include "unitTests/mocks/MockMessageParser.h"
#include "providers/PatientProvider.h"
#include "providers/PatientData.h"

using namespace ::testing;

class PatientProviderTests : public Test
{
public:
    PatientProviderTests()
    {}

    ~PatientProviderTests() override;

    void SetUp() override
    {
        _setPatientStatus = "{\n\t\"TransactionId\" : \""
                            "1553195112 "
                            "\",\n\t\"PatientId\" : \""
                            "1234556"
                            "\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

        _implantDoctors = "{\n\t\"Type\" : \"ImplantDoctors\","
                          "\n\t\"Status\" : \"SUCCESS\","
                          "\n\t\"Names\" : ["
                          "\n\t\t{\"Name\" : \"Dr. Strange\"},"
                          "\n\t\t{\"Name\" : \"Dr. Spock\"}"
                          "\n\t]"
                          "\n}\n";
    }

    std::string _setPatientStatus;
    std::string _implantDoctors;
    std::condition_variable _condition;

};

PatientProviderTests::~PatientProviderTests() {}

TEST_F(PatientProviderTests, ConstructorCallsMessageParserRegisterCallback)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();

    EXPECT_CALL(parser, registerCallback(_))
            .Times(1);

    PatientProvider provider(parser);
}

TEST_F(PatientProviderTests, PatientSavedCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    unsigned long count = 0;
    provider.registerPatientSavedCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_setPatientStatus));
    parser._fake._func(eSET_PATIENT, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}

TEST_F(PatientProviderTests, AllPatientSavedCallbacksRegisteredWithProviderAreExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerPatientSavedCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerPatientSavedCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_setPatientStatus));
    parser._fake._func(eSET_PATIENT, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(PatientProviderTests, PatientSavedCallbackUnregisterWithProviderAreNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerPatientSavedCallback([&]{ count1++; });

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerPatientSavedCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterPatientSavedCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_setPatientStatus));
    parser._fake._func(eSET_PATIENT, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(PatientProviderTests, ListDataCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    unsigned long count = 0;
    provider.registerListDataCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_implantDoctors));
    parser._fake._func(eGET_DROPDOWN_LIST_DATA, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}

TEST_F(PatientProviderTests, AllListDataCallbacksRegisteredWithProviderAreExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerListDataCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerListDataCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_implantDoctors));
    parser._fake._func(eGET_DROPDOWN_LIST_DATA, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(PatientProviderTests, ListDataSavedCallbackUnregisterWithProviderAreNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerListDataCallback([&]{ count1++; });

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerListDataCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterListDataCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_implantDoctors));
    parser._fake._func(eGET_DROPDOWN_LIST_DATA, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}


TEST_F(PatientProviderTests, SavePatientCallsParserSendCommandWitheSetPatientCommandAndPatientStr)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    PatientData patient;
    patient.patientId = 789123;
    int transactionId = 1547560320;

    cmem_patient cmemPt = patient.toCMemPatient();
    std::string ptStr;
    cmemPt.updatePatientCmd(static_cast<std::uint32_t>(transactionId), ptStr);

    EXPECT_CALL(parser, sendCommand(eSET_PATIENT, ptStr))
            .Times(1);

    provider.savePatient(transactionId, patient);
}

TEST_F(PatientProviderTests, GetPatientIdReturnsTheSavedPatientPatientId)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    provider.registerPatientSavedCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_setPatientStatus));
    parser._fake._func(eSET_PATIENT, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1234556, provider.getPatientId());
}

TEST_F(PatientProviderTests, CallbacksFromParserOfDifferentCommandTypeAreNotHandledByProvider)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    unsigned long count = 0;
    provider.registerPatientSavedCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_setPatientStatus));
    parser._fake._func(eUNKNOWN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(0, count);
}

TEST_F(PatientProviderTests, PatientIdIsSetToZeroWhenParserReturnsAnEmptyPayloadString)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    provider.registerPatientSavedCallback([&]{ _condition.notify_one(); });

    std::string setPatientStatus = "";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(setPatientStatus));
    parser._fake._func(eUNKNOWN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, provider.getPatientId());
}

TEST_F(PatientProviderTests, RequestListDataCallParserWithDropDownCommandAndType)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    std::string type = "ImplantDoctors";
    std::string payload = "{\n\t\"Names\" : \"" +
                          type +
                          "\"\n}\n";
    EXPECT_CALL(parser, sendCommand(eGET_DROPDOWN_LIST_DATA, payload))
            .Times(1);

    provider.requestListData(type);
}

TEST_F(PatientProviderTests, GetListDataReturnsVectorOfStringsExtractedFromResponse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientProvider provider(parser);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_implantDoctors));
    parser._fake._func(eGET_DROPDOWN_LIST_DATA, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    std::vector<std::string> dataItems = provider.getListData("ImplantDoctors");
    ASSERT_EQ(2, dataItems.size());
    ASSERT_EQ("Dr. Strange", dataItems[0]);
    ASSERT_EQ("Dr. Spock", dataItems[1]);
}
