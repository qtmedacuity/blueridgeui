﻿#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <QSignalSpy>
#include "presentation/SignalStrengthController.h"

using namespace ::testing;

class SignalStrengthControllerTests : public Test
{
public:
    SignalStrengthControllerTests()
        : controller({{"twoBars",40},{"threeBars",60},{"oneBar",20},{"fourBars",80},{"fiveBars",100}})
    {}

protected:
    SignalStrengthController controller;
};


TEST_F(SignalStrengthControllerTests, strengthReturnsZeroIfNotSet)
{
    int expectedStrength = 0;

    ASSERT_EQ(expectedStrength, controller.strength());
}

TEST_F(SignalStrengthControllerTests, statusReturnsEmptyStringIfNotSet)
{
    QString expectedStatus("");

    ASSERT_EQ(expectedStatus, controller.status());
}

TEST_F(SignalStrengthControllerTests, strengthReturnsASignalStrengthNumber)
{

    int expectedStrength = 10;
    controller.setStrength(expectedStrength);

    ASSERT_EQ(expectedStrength, controller.strength());
}

TEST_F(SignalStrengthControllerTests, setStrengthSetsSignalStrengthToZeroIfLessThanZero)
{

    int expectedStrength = 0;
    controller.setStrength(-1);

    ASSERT_EQ(expectedStrength, controller.strength());
}

TEST_F(SignalStrengthControllerTests, setStrengthSetsSignalStrengthToOneHundredIfLargerThanOneHundred)
{

    int expectedStrength = 100;
    controller.setStrength(expectedStrength);

    ASSERT_EQ(expectedStrength, controller.strength());
}

TEST_F(SignalStrengthControllerTests, setStrengthSetsCorrectSignalStrengthIfLargerThanZeroAndLessThanOneHundred)
{

    int expectedStrength = 52;
    controller.setStrength(expectedStrength);

    ASSERT_EQ(expectedStrength, controller.strength());
}

TEST_F(SignalStrengthControllerTests, setStrengthSetsStatusToEmptyStringIfLessThanZero)
{

    QString expectedStatus = "";
    controller.setStrength(-1);

    ASSERT_EQ(expectedStatus, controller.status());
}

TEST_F(SignalStrengthControllerTests, setStrengthSetsStatusToCorrespondingBarString)
{

    QString expectedStatus = "twoBars";
    controller.setStrength(44);

    ASSERT_EQ(expectedStatus, controller.status());
}

TEST_F(SignalStrengthControllerTests, statusReturnsTheCorrespondingBarStringForSignalStrength)
{

    QString expectedStatus = "fourBars";
    controller.setStrength(85);

    ASSERT_EQ(expectedStatus, controller.status());
}

TEST_F(SignalStrengthControllerTests, setStatusSetsStatusStringToCorrectString)
{

    QString expectedStatus = "threeBars";
    controller.setStatus(expectedStatus);

    ASSERT_EQ(expectedStatus, controller.status());
}

TEST_F(SignalStrengthControllerTests, setStrengthEmitsSignalWhenSignalStrengthChanged)
{
    QSignalSpy spy(&controller, SIGNAL(strengthChanged(int)));
    controller.setStrength(85);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SignalStrengthControllerTests, setStatusEmitsSignalWhenStatusChanged)
{
    QSignalSpy spy(&controller, SIGNAL(statusChanged(QString)));
    controller.setStatus("status");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SignalStrengthControllerTests, strengthPropertySetsTheSignalStrength)
{
    controller.setProperty("strength", 25);

    ASSERT_EQ(controller.strength(), 25);
}

TEST_F(SignalStrengthControllerTests, strengthPropertyReturnsTheSignalStrength)
{
    int expectedStrength = 74;
    controller.setStrength(expectedStrength);
    int actualStrength = controller.property("strength").toInt();

    ASSERT_EQ(actualStrength, expectedStrength);
}

TEST_F(SignalStrengthControllerTests, strengthPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("strength");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("strengthChanged", property.notifySignal().name().data());
}

TEST_F(SignalStrengthControllerTests, statusPropertySetsTheStatus)
{
    controller.setProperty("status", "tenBars");

    ASSERT_EQ(controller.status(), "tenBars");
}

TEST_F(SignalStrengthControllerTests, statusPropertyReturnsTheSetStatus)
{
    QString expectedStatus = "oneBar";
    controller.setStatus(expectedStatus);
    QString actualStatus = controller.property("status").toString();

    ASSERT_EQ(actualStatus, expectedStatus);
}

TEST_F(SignalStrengthControllerTests, statusPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("status");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("statusChanged", property.notifySignal().name().data());
}
