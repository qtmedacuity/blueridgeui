#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include <cstring>
#include <vector>

#include "presentation/RhcValues.h"
#include "providers/RhcData.h"


using namespace ::testing;

class RhcValuesTests : public Test
{
public:
    RhcValuesTests()
    {

        _rhcData.date = "22 MAY 2019";
        _rhcData.time = "11:34:22";
        _rhcData.ra = 33;
        _rhcData.rvSystolic = 32;
        _rhcData.rvDiastoic = 23;
        _rhcData.paSystolic = 31;
        _rhcData.paDiastolic = 21;
        _rhcData.paMean = 25;
        _rhcData.pcwp = 22;
        _rhcData.heartRate = 82;
        _rhcData.signalStrength = 98;
        _rhcData.waveformFile = "/tmp/valid_form.xml";

        _rhcValues = std::shared_ptr<RhcValues>(new RhcValues(&_rhcData));
    }

    ~RhcValuesTests() override;

    RhcData _rhcData;
    std::shared_ptr<RhcValues> _rhcValues;
};

RhcValuesTests::~RhcValuesTests() {}

TEST_F(RhcValuesTests, ConstructorSetsDefaultValues)
{
    RhcValues rhcValues;

    ASSERT_EQ(rhcValues.date(), "");
    ASSERT_EQ(rhcValues.time(), "");
    ASSERT_EQ(rhcValues.raValue(), 0);
    ASSERT_EQ(rhcValues.rvSystolic(), 0);
    ASSERT_EQ(rhcValues.rvDiastolic(), 0);
    ASSERT_EQ(rhcValues.pcwpValue(), 0);
    ASSERT_EQ(rhcValues.paSystolic(), 0);
    ASSERT_EQ(rhcValues.paDiastolic(), 0);
    ASSERT_EQ(rhcValues.paMean(), 0);
    ASSERT_EQ(rhcValues.heartRate(), 0);
    ASSERT_EQ(rhcValues.sensorStrength(), 0);
    ASSERT_EQ(rhcValues.waveFormFile(), "");
}

TEST_F(RhcValuesTests, ConstructorSetsValuesFromRhcValuesDataValues)
{
    RhcValues rhcValues(&_rhcData);

    ASSERT_EQ(rhcValues.date(), "22 MAY 2019");
    ASSERT_EQ(rhcValues.time(), "11:34:22");
    ASSERT_EQ(rhcValues.raValue(), _rhcData.ra);
    ASSERT_EQ(rhcValues.rvSystolic(), _rhcData.rvSystolic);
    ASSERT_EQ(rhcValues.rvDiastolic(), _rhcData.rvDiastoic);
    ASSERT_EQ(rhcValues.pcwpValue(), _rhcData.pcwp);
    ASSERT_EQ(rhcValues.paSystolic(), _rhcData.paSystolic);
    ASSERT_EQ(rhcValues.paDiastolic(), _rhcData.paDiastolic);
    ASSERT_EQ(rhcValues.paMean(), _rhcData.paMean);
    ASSERT_EQ(rhcValues.heartRate(), _rhcData.heartRate);
    ASSERT_EQ(rhcValues.sensorStrength(), _rhcData.signalStrength);
    ASSERT_EQ(rhcValues.waveFormFile(), "/tmp/valid_form.xml");
}

TEST_F(RhcValuesTests, ToRhcDataConvertsRhcValueToRhcData)
{
    RhcData rhcData;
    _rhcValues->toRhcData(&rhcData);

    ASSERT_EQ("22 MAY 2019", rhcData.date);
    ASSERT_EQ("11:34:22", _rhcData.time);
    ASSERT_EQ(_rhcValues->raValue(), rhcData.ra);
    ASSERT_EQ(_rhcValues->rvSystolic(), rhcData.rvSystolic);
    ASSERT_EQ(_rhcValues->rvDiastolic(), rhcData.rvDiastoic);
    ASSERT_EQ(_rhcValues->pcwpValue(), rhcData.pcwp);
    ASSERT_EQ(_rhcValues->paSystolic(), rhcData.paSystolic);
    ASSERT_EQ(_rhcValues->paDiastolic(), rhcData.paDiastolic);
    ASSERT_EQ(_rhcValues->paMean(), rhcData.paMean);
    ASSERT_EQ(_rhcValues->heartRate(), rhcData.heartRate);
    ASSERT_EQ(_rhcValues->sensorStrength(), rhcData.signalStrength);
    ASSERT_EQ("/tmp/valid_form.xml", rhcData.waveformFile);
}

TEST_F(RhcValuesTests, FromRchDataSetsValuesFromRhcValuesDataValues)
{
    RhcValues rhcValues;
    rhcValues.fromRhcData(&_rhcData);

    ASSERT_EQ(rhcValues.date(), "22 MAY 2019");
    ASSERT_EQ(rhcValues.time(), "11:34:22");
    ASSERT_EQ(rhcValues.raValue(), _rhcData.ra);
    ASSERT_EQ(rhcValues.rvSystolic(), _rhcData.rvSystolic);
    ASSERT_EQ(rhcValues.rvDiastolic(), _rhcData.rvDiastoic);
    ASSERT_EQ(rhcValues.pcwpValue(), _rhcData.pcwp);
    ASSERT_EQ(rhcValues.paSystolic(), _rhcData.paSystolic);
    ASSERT_EQ(rhcValues.paDiastolic(), _rhcData.paDiastolic);
    ASSERT_EQ(rhcValues.paMean(), _rhcData.paMean);
    ASSERT_EQ(rhcValues.heartRate(), _rhcData.heartRate);
    ASSERT_EQ(rhcValues.sensorStrength(), _rhcData.signalStrength);
    ASSERT_EQ(rhcValues.waveFormFile(), "/tmp/valid_form.xml");
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenValuesAreChanged)
{
    RhcValues rhcValues;
    QSignalSpy spy(&rhcValues, SIGNAL(rhcValuesChanged()));
    rhcValues.fromRhcData(&_rhcData);

    ASSERT_EQ(spy.count(), 1);
}


TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenDateHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.date = "22 JUN 2019";
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->date(), "22 JUN 2019");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenTimeHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.time = "12:35:22";
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->time(), "12:35:22");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenRaHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.ra = 30;
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->raValue(), 30);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenRvStystolicHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.rvSystolic = 30;
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->rvSystolic(), 30);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenRvDiastolicHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.rvDiastoic = 22;
     _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->rvDiastolic(), 22);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenPaSystolicHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.paSystolic = 32;
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->paSystolic(), 32);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenPaDiastolicHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.paDiastolic = 20;
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->paDiastolic(), 20);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenPaMeanHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.paMean = 26;
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->paMean(), 26);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenPcwpHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.pcwp = 21;
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->pcwpValue(), 21);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenHeartRateHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.heartRate = 80;
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->heartRate(), 80);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenSignalStrengthHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.signalStrength = 99;
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->sensorStrength(), 99);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalEmittedInFromRchDataSetsValuesWhenWaveformFileHasChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcData.waveformFile = "/tmp/valid_form2.xml";
    _rhcValues->fromRhcData(&_rhcData);
    ASSERT_EQ(_rhcValues->waveFormFile(), "/tmp/valid_form2.xml");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RhcValuesChangedSignalNotEmittedInFromRchDataSetsValuesWhenDataHasNotChanged)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));

    _rhcValues->fromRhcData(&_rhcData);

    ASSERT_EQ(spy.count(), 0);
}

TEST_F(RhcValuesTests, DateReturnsRhcValuesDate)
{
    ASSERT_EQ("22 MAY 2019", _rhcValues->date());
}

TEST_F(RhcValuesTests, SetDateSetsTheRhcValuesDate)
{
    _rhcValues->setDate("120419");
    ASSERT_EQ("120419", _rhcValues->date());
}

TEST_F(RhcValuesTests, SetDateEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setDate("120419");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, DatePropertyReturnsTheRhcValuesDate)
{
    QString value = _rhcValues->property("date").toString();

    ASSERT_EQ(value, "22 MAY 2019");
}

TEST_F(RhcValuesTests, DatePropertySetsTheRhcValuesDate)
{
    _rhcValues->setProperty("date", "coDate");
    ASSERT_EQ(_rhcValues->date(), "coDate");
}

TEST_F(RhcValuesTests, DatePropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("date");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}


TEST_F(RhcValuesTests, TimeReturnsRhcValuesTime)
{
    ASSERT_EQ("11:34:22", _rhcValues->time());
}

TEST_F(RhcValuesTests, SetTimeSetsTheRhcValuesTime)
{
    _rhcValues->setTime("120419");
    ASSERT_EQ("120419", _rhcValues->time());
}

TEST_F(RhcValuesTests, SetTimeEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setTime("time");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, TimePropertyReturnsTheRhcValuesTime)
{
    QString value = _rhcValues->property("time").toString();

    ASSERT_EQ(value, "11:34:22");
}

TEST_F(RhcValuesTests, TimePropertySetsTheRhcValuesTime)
{
    _rhcValues->setProperty("time", "coTime");
    ASSERT_EQ(_rhcValues->time(), "coTime");
}

TEST_F(RhcValuesTests, TimePropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("time");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}


TEST_F(RhcValuesTests, RaValueReturnsRhcValuesRaValue)
{
    ASSERT_EQ(33, _rhcValues->raValue());
}

TEST_F(RhcValuesTests, SetRaValueSetsTheRhcValuesRaValue)
{
    _rhcValues->setRaValue(32);
    ASSERT_EQ(32, _rhcValues->raValue());
}

TEST_F(RhcValuesTests, SetRaValueEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setRaValue(31);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RaValuePropertyReturnsTheRhcValuesRaValue)
{
    int value = _rhcValues->property("raValue").toInt();

    ASSERT_EQ(value, 33);
}

TEST_F(RhcValuesTests, RaValuePropertySetsTheRhcValuesRaValue)
{
    _rhcValues->setProperty("raValue", 30);
    ASSERT_EQ(_rhcValues->raValue(), 30);
}

TEST_F(RhcValuesTests, RaValuePropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("raValue");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}

TEST_F(RhcValuesTests, RvSystolicReturnsRhcValuesRvSystolic)
{
    ASSERT_EQ(32, _rhcValues->rvSystolic());
}

TEST_F(RhcValuesTests, SetRvSystolicSetsTheRhcValuesRvSystolic)
{
    _rhcValues->setRvSystolic(30);
    ASSERT_EQ(30, _rhcValues->rvSystolic());
}

TEST_F(RhcValuesTests, SetRvSystolicEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setRvSystolic(31);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RvSystolicPropertyReturnsTheRhcValuesRvSystolic)
{
    int value = _rhcValues->property("rvSystolic").toInt();

    ASSERT_EQ(value, 32);
}

TEST_F(RhcValuesTests, RvSystolicPropertySetsTheRhcValuesRvSystolic)
{
    _rhcValues->setProperty("rvSystolic", 30);
    ASSERT_EQ(_rhcValues->rvSystolic(), 30);
}

TEST_F(RhcValuesTests, RvSystolicPropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("rvSystolic");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}

TEST_F(RhcValuesTests, RvDiastolicReturnsRhcValuesRvDiastolic)
{
    ASSERT_EQ(23, _rhcValues->rvDiastolic());
}

TEST_F(RhcValuesTests, SetRvDiastolicSetsTheRhcValuesRvDiastolic)
{
    _rhcValues->setRvDiastolic(22);
    ASSERT_EQ(22, _rhcValues->rvDiastolic());
}

TEST_F(RhcValuesTests, SetRvDiastolicEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setRvDiastolic(22);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, RvDiastolicPropertyReturnsTheRhcValuesRvDiastolic)
{
    int value = _rhcValues->property("rvDiastolic").toInt();

    ASSERT_EQ(value, 23);
}

TEST_F(RhcValuesTests, RvDiastolicPropertySetsTheRhcValuesRvDiastolic)
{
    _rhcValues->setProperty("rvDiastolic", 22);
    ASSERT_EQ(_rhcValues->rvDiastolic(), 22);
}

TEST_F(RhcValuesTests, RvDiastolicPropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("rvDiastolic");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}

TEST_F(RhcValuesTests, PaSystolicReturnsRhcValuesPaSystolic)
{
    ASSERT_EQ(31, _rhcValues->paSystolic());
}

TEST_F(RhcValuesTests, SetPaSystolicSetsTheRhcValuesPaSystolic)
{
    _rhcValues->setPaSystolic(30);
    ASSERT_EQ(30, _rhcValues->paSystolic());
}

TEST_F(RhcValuesTests, SetPaSystolicEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setPaSystolic(30);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, PaSystolicPropertyReturnsTheRhcValuesPaSystolic)
{
    int value = _rhcValues->property("paSystolic").toInt();

    ASSERT_EQ(value, 31);
}

TEST_F(RhcValuesTests, PaSystolicPropertySetsTheRhcValuesPaSystolic)
{
    _rhcValues->setProperty("paSystolic", 30);
    ASSERT_EQ(_rhcValues->paSystolic(), 30);
}

TEST_F(RhcValuesTests, PaSystolicPropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("paSystolic");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}

TEST_F(RhcValuesTests, PaDiastolicReturnsRhcValuesPaDiastolic)
{
    ASSERT_EQ(21, _rhcValues->paDiastolic());
}

TEST_F(RhcValuesTests, SetPaDiastolicSetsTheRhcValuesPaDiastolic)
{
    _rhcValues->setPaDiastolic(22);
    ASSERT_EQ(22, _rhcValues->paDiastolic());
}

TEST_F(RhcValuesTests, SetPaDiastolicEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setPaDiastolic(22);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, PaDiastolicPropertyReturnsTheRhcValuesPaDiastolic)
{
    int value = _rhcValues->property("paDiastolic").toInt();

    ASSERT_EQ(value, 21);
}

TEST_F(RhcValuesTests, PaDiastolicPropertySetsTheRhcValuesPaDiastolic)
{
    _rhcValues->setProperty("paDiastolic", 22);
    ASSERT_EQ(_rhcValues->paDiastolic(), 22);
}

TEST_F(RhcValuesTests, PaDiastolicPropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("paDiastolic");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}

TEST_F(RhcValuesTests, PaMeanReturnsRhcValuesPaMean)
{
    ASSERT_EQ(25, _rhcValues->paMean());
}

TEST_F(RhcValuesTests, SetPaMeanSetsTheRhcValuesPaMean)
{
    _rhcValues->setPaMean(22);
    ASSERT_EQ(22, _rhcValues->paMean());
}

TEST_F(RhcValuesTests, SetPaMeanEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setPaMean(22);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, PaMeanPropertyReturnsTheRhcValuesPaMean)
{
    int value = _rhcValues->property("paMean").toInt();

    ASSERT_EQ(value, 25);
}

TEST_F(RhcValuesTests, PaMeanPropertySetsTheRhcValuesPaMean)
{
    _rhcValues->setProperty("paMean", 22);
    ASSERT_EQ(_rhcValues->paMean(), 22);
}

TEST_F(RhcValuesTests, PaMeanPropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("paMean");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}

TEST_F(RhcValuesTests, PcwpValueReturnsRhcValuesPcwpValue)
{
    ASSERT_EQ(22, _rhcValues->pcwpValue());
}

TEST_F(RhcValuesTests, SetPcwpValueSetsTheRhcValuesPcwpValue)
{
    _rhcValues->setPcwpValue(21);
    ASSERT_EQ(21, _rhcValues->pcwpValue());
}

TEST_F(RhcValuesTests, SetPcwpValueEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setPcwpValue(21);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, PcwpValuePropertyReturnsTheRhcValuesPcwpValue)
{
    int value = _rhcValues->property("pcwpValue").toInt();

    ASSERT_EQ(value, 22);
}

TEST_F(RhcValuesTests, PcwpValuePropertySetsTheRhcValuesPcwpValue)
{
    _rhcValues->setProperty("pcwpValue", 22);
    ASSERT_EQ(_rhcValues->pcwpValue(), 22);
}

TEST_F(RhcValuesTests, PcwpValuePropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("pcwpValue");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}

TEST_F(RhcValuesTests, HeartRateReturnsRhcValuesHeartRate)
{
    ASSERT_EQ(82, _rhcValues->heartRate());
}

TEST_F(RhcValuesTests, SetHeartRateSetsTheRhcValuesHeartRate)
{
    _rhcValues->setHeartRate(75);
    ASSERT_EQ(75, _rhcValues->heartRate());
}

TEST_F(RhcValuesTests, SetHeartRatetEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setHeartRate(75);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, HeartRatePropertyReturnsTheRhcValuesHeartRate)
{
    int value = _rhcValues->property("heartRate").toInt();

    ASSERT_EQ(value, 82);
}

TEST_F(RhcValuesTests, HeartRatePropertySetsTheRhcValuesHeartRate)
{
    _rhcValues->setProperty("heartRate", 75);
    ASSERT_EQ(_rhcValues->heartRate(), 75);
}

TEST_F(RhcValuesTests, HeartRatePropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("heartRate");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}


TEST_F(RhcValuesTests, SensorStrengthReturnsRhcValuesSensorStrength)
{
    ASSERT_EQ(98, _rhcValues->sensorStrength());
}

TEST_F(RhcValuesTests, SetSensorStrengthSetsTheRhcValuesSensorStrength)
{
    _rhcValues->setSensorStrength(75);
    ASSERT_EQ(75, _rhcValues->sensorStrength());
}

TEST_F(RhcValuesTests, SetSensorStrengthtEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setSensorStrength(75);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, SensorStrengthPropertyReturnsTheRhcValuesSensorStrength)
{
    int value = _rhcValues->property("sensorStrength").toInt();

    ASSERT_EQ(value, 98);
}

TEST_F(RhcValuesTests, SensorStrengthPropertySetsTheRhcValuesSensorStrength)
{
    _rhcValues->setProperty("sensorStrength", 95);
    ASSERT_EQ(_rhcValues->sensorStrength(), 95);
}

TEST_F(RhcValuesTests, SensorStrengthPropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("sensorStrength");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}

TEST_F(RhcValuesTests, WaveFormFileReturnsRhcValuesWaveFormFile)
{
    ASSERT_EQ("/tmp/valid_form.xml", _rhcValues->waveFormFile());
}

TEST_F(RhcValuesTests, SetWaveFormFileSetsTheRhcValuesWaveFormFile)
{
    _rhcValues->setWaveFormFile("file");
    ASSERT_EQ("file", _rhcValues->waveFormFile());
}

TEST_F(RhcValuesTests, SetWaveFormFileEmitsRhcValuesChangedSignal)
{
    QSignalSpy spy(_rhcValues.get(), SIGNAL(rhcValuesChanged()));
    _rhcValues->setWaveFormFile("file");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RhcValuesTests, WaveFormFilePropertyReturnsTheRhcValuesWaveFormFile)
{
    QString value = _rhcValues->property("waveFormFile").toString();

    ASSERT_EQ(value, "/tmp/valid_form.xml");
}

TEST_F(RhcValuesTests, WaveFormFilePropertySetsTheRhcValuesWaveFormFile)
{
    _rhcValues->setProperty("waveFormFile", "file");
    ASSERT_EQ(_rhcValues->waveFormFile(), "file");
}

TEST_F(RhcValuesTests, WaveFormFilePropertyHasNotifySignal)
{
    int propertyIndex = _rhcValues->metaObject()->indexOfProperty("waveFormFile");
    QMetaProperty property = _rhcValues->metaObject()->property(propertyIndex);

    ASSERT_STREQ("rhcValuesChanged", property.notifySignal().name().data());
}
