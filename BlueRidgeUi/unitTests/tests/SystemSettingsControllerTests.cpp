#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include <cstring>
#include <vector>

#include "unitTests/mocks/MockSystemSettingsProvider.h"
#include "presentation/SystemSettingsController.h"
#include "providers/Configuration.h"

using namespace ::testing;

class SystemSettingsControllerTests : public Test
{
public:
    SystemSettingsControllerTests()
          : _controller(_settingsProvider)
    {

    }

    ~SystemSettingsControllerTests() override;

    void SetUp() override
    {

    }

    NiceMock<MockSystemSettingsProvider> _settingsProvider;
    SystemSettingsController _controller;
    Configuration _configuration;
};

SystemSettingsControllerTests::~SystemSettingsControllerTests() {}

TEST_F(SystemSettingsControllerTests, ConstructorRegisterCallbacksWithProvider)
{

    EXPECT_CALL(_settingsProvider, registerConfigurationCallback(_))
            .Times(1);
    EXPECT_CALL(_settingsProvider, registerRFStateCallback(_))
            .Times(1);
    EXPECT_CALL(_settingsProvider, registerSensorDataSocketErrorCallback(_))
            .Times(1);

    EXPECT_CALL(_settingsProvider, requestConfiguration())
            .Times(1);

    EXPECT_CALL(_settingsProvider, requestRFState())
            .Times(1);

    SystemSettingsController controller(_settingsProvider);
}

TEST_F(SystemSettingsControllerTests, RfStateCallsTheProviderToGettheState)
{
    bool state = true;
    EXPECT_CALL(_settingsProvider, setRFState(state))
            .Times(1);

    _controller.setRFState(state);
}


TEST_F(SystemSettingsControllerTests, SetRfStateCallsTheProviderToSettheState)
{
    bool state = true;
    ON_CALL(_settingsProvider, rfState())
            .WillByDefault(Return(state));;

    ASSERT_EQ(state, _controller.rfState());
}

TEST_F(SystemSettingsControllerTests, DefaultLanguageCallsProviderToGetLanguageConfigurationValue)
{
    _configuration._language = "EN";
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ("EN", _controller.defaultLanguage());
}

TEST_F(SystemSettingsControllerTests, PaMeanMinumumCallsProviderToGetPaMeanConfigurationValue)
{
    unsigned int min = 1;
    unsigned int max = 150;
    ConfigPAMean paMean(min, max);
    _configuration._paMean = paMean;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(min, _controller.paMeanMinimum());
}

TEST_F(SystemSettingsControllerTests, PaMeanMaximumallsProviderToGetPaMeanConfigurationValue)
{
    unsigned int min = 1;
    unsigned int max = 150;
    ConfigPAMean paMean(min, max);
    _configuration._paMean = paMean;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(max, _controller.paMeanMaximum());
}

TEST_F(SystemSettingsControllerTests, SearcPressureMinumumCallsProviderToGetSearchPressureConfigurationValue)
{
    int min = 5;
    int max = 100;
    int dft = 50;
    ConfigSearchPressure searchPressure(min, max, dft);
    _configuration._searchPressure = searchPressure;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(min, _controller.searchPressureMinimum());
}

TEST_F(SystemSettingsControllerTests, SearcPressureMaximumCallsProviderToGetSearchPressureConfigurationValue)
{
    int min = 5;
    int max = 100;
    int dft = 50;
    ConfigSearchPressure searchPressure(min, max, dft);
    _configuration._searchPressure = searchPressure;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(max, _controller.searchPressureMaximum());
}

TEST_F(SystemSettingsControllerTests, SearcPressureStartCallsProviderToGetSearchPressureConfigurationValue)
{
    int min = 5;
    int max = 100;
    int dft = 50;
    ConfigSearchPressure searchPressure(min, max, dft);
    _configuration._searchPressure = searchPressure;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(dft, _controller.searchPressureStart());
}

TEST_F(SystemSettingsControllerTests, SystloicMinumumCallsProviderToSystolicConfigurationValue)
{
    unsigned int min = 0;
    unsigned int max = 99;
    ConfigSystolic systolic(min, max);
    _configuration._systolic = systolic;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(min, _controller.systolicMinimum());
}

TEST_F(SystemSettingsControllerTests, SystloicMaxmimumCallsProviderToSystolicConfigurationValue)
{
    unsigned int min = 0;
    unsigned int max = 99;
    ConfigSystolic systolic(min, max);
    _configuration._systolic = systolic;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(max, _controller.systolicMaximum());
}

TEST_F(SystemSettingsControllerTests, DiastloicMinumumCallsProviderToDiastolicConfigurationValue)
{
    unsigned int min = 1;
    unsigned int max = 98;
    ConfigDiastolic diastolic(min, max);
    _configuration._diastolic = diastolic;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(min, _controller.diastolicMinimum());
}

TEST_F(SystemSettingsControllerTests, DiastloicMaxmimumCallsProviderToDiastolicConfigurationValue)
{
    unsigned int min = 1;
    unsigned int max = 98;
    ConfigDiastolic diastolic(min, max);
    _configuration._diastolic = diastolic;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(max, _controller.diastolicMaximum());
}

TEST_F(SystemSettingsControllerTests, ReferenceMeanMinumumCallsProviderToRefereneMeanConfigurationValue)
{
    unsigned int min = 2;
    unsigned int max = 97;
    ConfigReferenceMean referenceMean(min, max);
    _configuration._referenceMean = referenceMean;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(min, _controller.referenceMeanMinimum());
}

TEST_F(SystemSettingsControllerTests, ReferenceMeanMaximumCallsProviderToRefereneMeanConfigurationValue)
{
    unsigned int min = 2;
    unsigned int max = 97;
    ConfigReferenceMean referenceMean(min, max);
    _configuration._referenceMean = referenceMean;
    ON_CALL(_settingsProvider, configuration())
           .WillByDefault(ReturnRef(_configuration));

    ASSERT_EQ(max, _controller.referenceMeanMaximum());
}

TEST_F(SystemSettingsControllerTests, RfStateReturnsTheProvidersRfState)
{
    bool state = true;
    ON_CALL(_settingsProvider, rfState())
            .WillByDefault(Return(state));

    ASSERT_EQ(state, _controller.rfState());
}

TEST_F(SystemSettingsControllerTests, SetRfStateCallsTheProvidersSetRfStateWithValueProvided)
{
    bool state = true;

    EXPECT_CALL(_settingsProvider, setRFState(state))
            .Times(1);

    _controller.setRFState(state);
}

TEST_F(SystemSettingsControllerTests, EnableSensorDataStreamCallsProvidersEnableSensorDataStreamMethod)
{
    EXPECT_CALL(_settingsProvider, enableSensorDataStream())
            .Times(1);

    _controller.enableSensorDataStream();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
}

TEST_F(SystemSettingsControllerTests, DisableSensorDataStreamCallsProvidersDisableSensorDataStreamMethod)
{
    EXPECT_CALL(_settingsProvider, disableSensorDataStream())
            .Times(1);

    _controller.disableSensorDataStream();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
}

TEST_F(SystemSettingsControllerTests, ConfigurationChangedSignalEmittedWhenRegisterConfigurationCallbackInvoked)
{

    NiceMock<MockSystemSettingsProvider> settingsProvider;
    settingsProvider.DelegateToFake();

    EXPECT_CALL(settingsProvider, requestConfiguration())
            .Times(1);
    // request configuration called in constructor
    SystemSettingsController controller(settingsProvider);

    QSignalSpy spy(&controller, SIGNAL(configurationChanged()));

    settingsProvider._fake._configurationCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SystemSettingsControllerTests, RfStateChangedSignalEmittedWhenRfStateCallbackInvoked)
{

    NiceMock<MockSystemSettingsProvider> settingsProvider;
    settingsProvider.DelegateToFake();

    EXPECT_CALL(settingsProvider, requestRFState())
            .Times(1);
    // rf state request called in constructor
    SystemSettingsController controller(settingsProvider);

    QSignalSpy spy(&controller, SIGNAL(rfStateChanged(bool)));

    settingsProvider._fake._rfStateCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SystemSettingsControllerTests, RfStateChangedToTrueCallsProvidersEnableSensorDataSource)
{
    bool state = true;
    NiceMock<MockSystemSettingsProvider> settingsProvider;
    SystemSettingsController controller(settingsProvider);

    ON_CALL(settingsProvider, rfState())
            .WillByDefault(Return(state));

    EXPECT_CALL(settingsProvider, enableSensorDataSource())
            .Times(1);

    emit controller.rfStateChanged(state);
}

TEST_F(SystemSettingsControllerTests, RfStateChangedToFalseCallsProvidersDisableSensorDataSource)
{
    bool state = false;
    NiceMock<MockSystemSettingsProvider> settingsProvider;
    SystemSettingsController controller(settingsProvider);

    ON_CALL(settingsProvider, rfState())
            .WillByDefault(Return(state));

    EXPECT_CALL(settingsProvider, disableSensorDataSource())
            .Times(1);

    emit controller.rfStateChanged(state);
}
