#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include <cstring>
#include <vector>

#include "presentation/CardiacOutput.h"
#include "presentation/Recording.h"
#include "providers/CardiacOutputData.h"


using namespace ::testing;

class CardiacOutputTests : public Test
{
public:
    CardiacOutputTests()
    {

        _coData.date = "22 MAY 2019";
        _coData.time = "11:34:22";
        _coData.cardiacOutput = 3.3F;
        _coData.heartRate = 82;
        _coData.signalStrength = 98;
        _coData.waveformFile = "/tmp/valid_form.xml";

        _cardiacOutput = std::shared_ptr<CardiacOutput>(new CardiacOutput(&_coData));
    }

    ~CardiacOutputTests() override;

    CardiacOutputData _coData;
    std::shared_ptr<CardiacOutput> _cardiacOutput;
};

CardiacOutputTests::~CardiacOutputTests() {}

TEST_F(CardiacOutputTests, ConstructorSetsDefaultValues)
{
    CardiacOutput cardiacOutput;

    ASSERT_EQ(cardiacOutput.date(), "");
    ASSERT_EQ(cardiacOutput.time(), "");
    ASSERT_EQ(cardiacOutput.cardiacOutput(), 0.0);
    ASSERT_EQ(cardiacOutput.heartRate(), 0);
    ASSERT_EQ(cardiacOutput.sensorStrength(), 0);
    ASSERT_EQ(cardiacOutput.waveFormFile(), "");
}

TEST_F(CardiacOutputTests, ConstructorSetsValuesFromCardiacOutputDataValues)
{
    CardiacOutput cardiacOutput(&_coData);

    ASSERT_EQ(cardiacOutput.date(), "22 MAY 2019");
    ASSERT_EQ(cardiacOutput.time(), "11:34:22");
    ASSERT_EQ(cardiacOutput.cardiacOutput(), _coData.cardiacOutput);
    ASSERT_EQ(cardiacOutput.heartRate(), _coData.heartRate);
    ASSERT_EQ(cardiacOutput.sensorStrength(), _coData.signalStrength);
    ASSERT_EQ(cardiacOutput.waveFormFile(), "/tmp/valid_form.xml");
}

TEST_F(CardiacOutputTests, ConstructorSetsValuesFromRecordingAndCoValues)
{
    Recording recording;
    recording.setRecordingDate("22 MAY 2019");
    recording.setRecordingTime("11:34:22");
    recording.setHeartRate(82);
    recording.setSensorStrength(98);
    recording.setWaveFormFile("/tmp/valid_form.xml");

    float coValue = 2.3F;

    CardiacOutput cardiacOutput(&recording, coValue);

    ASSERT_EQ(cardiacOutput.date(), recording.recordingDate());
    ASSERT_EQ(cardiacOutput.time(), recording.recordingTime());
    ASSERT_EQ(cardiacOutput.cardiacOutput(), coValue);
    ASSERT_EQ(cardiacOutput.heartRate(), recording.heartRate());
    ASSERT_EQ(cardiacOutput.sensorStrength(), recording.sensorStrength());
    ASSERT_EQ(cardiacOutput.waveFormFile(), recording.waveFormFile());
}

TEST_F(CardiacOutputTests, ToCardiacOutputDataReturnsCorrectValuesForCardiacOutput)
{
    CardiacOutput cardiacOutput;
    cardiacOutput.setDate("22 MAY 2019");
    cardiacOutput.setTime("11:33:22");
    cardiacOutput.setCardiacOutput(3.2F);
    cardiacOutput.setHeartRate(75);
    cardiacOutput.setSensorStrength(99);
    cardiacOutput.setWaveFormFile("/tmp/validfile.xml");

    CardiacOutputData coData = cardiacOutput.toCardiacOutputData();

    ASSERT_EQ(coData.date, "22 MAY 2019");
    ASSERT_EQ(coData.time, "11:33:22");
    ASSERT_EQ(coData.cardiacOutput, 3.2F);
    ASSERT_EQ(coData.heartRate, 75);
    ASSERT_EQ(coData.signalStrength, 99);
    ASSERT_EQ(coData.waveformFile, "/tmp/validfile.xml");

}

TEST_F(CardiacOutputTests, DateReturnsCardiacOutputDate)
{
    ASSERT_EQ("22 MAY 2019", _cardiacOutput->date());
}

TEST_F(CardiacOutputTests, SetDateSetsTheCardiacOutputDate)
{
    _cardiacOutput->setDate("120419");
    ASSERT_EQ("120419", _cardiacOutput->date());
}

TEST_F(CardiacOutputTests, SetDateEmitsCardiacOutputChangedSignal)
{
    QSignalSpy spy(_cardiacOutput.get(), SIGNAL(cardiacOutputChanged()));
    _cardiacOutput->setDate("120419");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(CardiacOutputTests, DatePropertyReturnsTheCardiacOutputDate)
{
    QString value = _cardiacOutput->property("date").toString();

    ASSERT_EQ(value, "22 MAY 2019");
}

TEST_F(CardiacOutputTests, DatePropertySetsTheCardiacOutputDate)
{
    _cardiacOutput->setProperty("date", "coDate");
    ASSERT_EQ(_cardiacOutput->date(), "coDate");
}

TEST_F(CardiacOutputTests, DatePropertyHasNotifySignal)
{
    int propertyIndex = _cardiacOutput->metaObject()->indexOfProperty("date");
    QMetaProperty property = _cardiacOutput->metaObject()->property(propertyIndex);

    ASSERT_STREQ("cardiacOutputChanged", property.notifySignal().name().data());
}


TEST_F(CardiacOutputTests, TimeReturnsCardiacOutputTime)
{
    ASSERT_EQ("11:34:22", _cardiacOutput->time());
}

TEST_F(CardiacOutputTests, SetTimeSetsTheCardiacOutputTime)
{
    _cardiacOutput->setTime("120419");
    ASSERT_EQ("120419", _cardiacOutput->time());
}

TEST_F(CardiacOutputTests, SetTimeEmitsCardiacOutputChangedSignal)
{
    QSignalSpy spy(_cardiacOutput.get(), SIGNAL(cardiacOutputChanged()));
    _cardiacOutput->setTime("time");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(CardiacOutputTests, TimePropertyReturnsTheCardiacOutputTime)
{
    QString value = _cardiacOutput->property("time").toString();

    ASSERT_EQ(value, "11:34:22");
}

TEST_F(CardiacOutputTests, TimePropertySetsTheCardiacOutputTime)
{
    _cardiacOutput->setProperty("time", "coTime");
    ASSERT_EQ(_cardiacOutput->time(), "coTime");
}

TEST_F(CardiacOutputTests, TimePropertyHasNotifySignal)
{
    int propertyIndex = _cardiacOutput->metaObject()->indexOfProperty("time");
    QMetaProperty property = _cardiacOutput->metaObject()->property(propertyIndex);

    ASSERT_STREQ("cardiacOutputChanged", property.notifySignal().name().data());
}


TEST_F(CardiacOutputTests, CardiacOutputReturnsCardiacOutputCardiacOutput)
{
    ASSERT_EQ(3.3F, _cardiacOutput->cardiacOutput());
}

TEST_F(CardiacOutputTests, SetCardiacOutputSetsTheCardiacOutputCardiacOutput)
{
    _cardiacOutput->setCardiacOutput(2.3F);
    ASSERT_EQ(2.3F, _cardiacOutput->cardiacOutput());
}

TEST_F(CardiacOutputTests, SetCardiacOutputEmitsCardiacOutputChangedSignal)
{
    QSignalSpy spy(_cardiacOutput.get(), SIGNAL(cardiacOutputChanged()));
    _cardiacOutput->setCardiacOutput(2.3F);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(CardiacOutputTests, CardiacOutputPropertyReturnsTheCardiacOutputCardiacOutput)
{
    float value = _cardiacOutput->property("cardiacOutput").toFloat();

    ASSERT_EQ(value, 3.3F);
}

TEST_F(CardiacOutputTests, CardiacOutputPropertySetsTheCardiacOutputCardiacOutput)
{
    _cardiacOutput->setProperty("cardiacOutput", 2.3F);
    ASSERT_EQ(_cardiacOutput->cardiacOutput(), 2.3F);
}

TEST_F(CardiacOutputTests, CardiacOutputPropertyHasNotifySignal)
{
    int propertyIndex = _cardiacOutput->metaObject()->indexOfProperty("cardiacOutput");
    QMetaProperty property = _cardiacOutput->metaObject()->property(propertyIndex);

    ASSERT_STREQ("cardiacOutputChanged", property.notifySignal().name().data());
}

TEST_F(CardiacOutputTests, HeartRateReturnsCardiacOutputHeartRate)
{
    ASSERT_EQ(82, _cardiacOutput->heartRate());
}

TEST_F(CardiacOutputTests, SetHeartRateSetsTheCardiacOutputHeartRate)
{
    _cardiacOutput->setHeartRate(75);
    ASSERT_EQ(75, _cardiacOutput->heartRate());
}

TEST_F(CardiacOutputTests, SetHeartRatetEmitsCardiacOutputChangedSignal)
{
    QSignalSpy spy(_cardiacOutput.get(), SIGNAL(cardiacOutputChanged()));
    _cardiacOutput->setHeartRate(75);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(CardiacOutputTests, HeartRatePropertyReturnsTheCardiacOutputHeartRate)
{
    int value = _cardiacOutput->property("heartRate").toInt();

    ASSERT_EQ(value, 82);
}

TEST_F(CardiacOutputTests, HeartRatePropertySetsTheCardiacOutputHeartRate)
{
    _cardiacOutput->setProperty("heartRate", 75);
    ASSERT_EQ(_cardiacOutput->heartRate(), 75);
}

TEST_F(CardiacOutputTests, HeartRatePropertyHasNotifySignal)
{
    int propertyIndex = _cardiacOutput->metaObject()->indexOfProperty("heartRate");
    QMetaProperty property = _cardiacOutput->metaObject()->property(propertyIndex);

    ASSERT_STREQ("cardiacOutputChanged", property.notifySignal().name().data());
}


TEST_F(CardiacOutputTests, SensorStrengthReturnsCardiacOutputSensorStrength)
{
    ASSERT_EQ(98, _cardiacOutput->sensorStrength());
}

TEST_F(CardiacOutputTests, SetSensorStrengthSetsTheCardiacOutputSensorStrength)
{
    _cardiacOutput->setSensorStrength(75);
    ASSERT_EQ(75, _cardiacOutput->sensorStrength());
}

TEST_F(CardiacOutputTests, SetSensorStrengthtEmitsCardiacOutputChangedSignal)
{
    QSignalSpy spy(_cardiacOutput.get(), SIGNAL(cardiacOutputChanged()));
    _cardiacOutput->setSensorStrength(75);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(CardiacOutputTests, SensorStrengthPropertyReturnsTheCardiacOutputSensorStrength)
{
    int value = _cardiacOutput->property("sensorStrength").toInt();

    ASSERT_EQ(value, 98);
}

TEST_F(CardiacOutputTests, SensorStrengthPropertySetsTheCardiacOutputSensorStrength)
{
    _cardiacOutput->setProperty("sensorStrength", 95);
    ASSERT_EQ(_cardiacOutput->sensorStrength(), 95);
}

TEST_F(CardiacOutputTests, SensorStrengthPropertyHasNotifySignal)
{
    int propertyIndex = _cardiacOutput->metaObject()->indexOfProperty("sensorStrength");
    QMetaProperty property = _cardiacOutput->metaObject()->property(propertyIndex);

    ASSERT_STREQ("cardiacOutputChanged", property.notifySignal().name().data());
}

TEST_F(CardiacOutputTests, WaveFormFileReturnsCardiacOutputWaveFormFile)
{
    ASSERT_EQ("/tmp/valid_form.xml", _cardiacOutput->waveFormFile());
}

TEST_F(CardiacOutputTests, SetWaveFormFileSetsTheCardiacOutputWaveFormFile)
{
    _cardiacOutput->setWaveFormFile("file");
    ASSERT_EQ("file", _cardiacOutput->waveFormFile());
}

TEST_F(CardiacOutputTests, SetWaveFormFileEmitsCardiacOutputChangedSignal)
{
    QSignalSpy spy(_cardiacOutput.get(), SIGNAL(cardiacOutputChanged()));
    _cardiacOutput->setWaveFormFile("file");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(CardiacOutputTests, WaveFormFilePropertyReturnsTheCardiacOutputWaveFormFile)
{
    QString value = _cardiacOutput->property("waveFormFile").toString();

    ASSERT_EQ(value, "/tmp/valid_form.xml");
}

TEST_F(CardiacOutputTests, WaveFormFilePropertySetsTheCardiacOutputWaveFormFile)
{
    _cardiacOutput->setProperty("waveFormFile", "file");
    ASSERT_EQ(_cardiacOutput->waveFormFile(), "file");
}

TEST_F(CardiacOutputTests, WaveFormFilePropertyHasNotifySignal)
{
    int propertyIndex = _cardiacOutput->metaObject()->indexOfProperty("waveFormFile");
    QMetaProperty property = _cardiacOutput->metaObject()->property(propertyIndex);

    ASSERT_STREQ("cardiacOutputChanged", property.notifySignal().name().data());
}
