#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "presentation/PatientInfo.h"

using namespace ::testing;

class PatientInfoTests : public Test
{
public:
    PatientInfoTests()
    {
        ptInfo = std::shared_ptr<PatientInfo>(new PatientInfo());
        ptInfo->setPatientId(123);
        ptInfo->setFirstName("PtFirstName");
        ptInfo->setMiddleInitial("PtMiddleInitial");
        ptInfo->setLastName("PtLastName");
        ptInfo->setPatientName("PtName");
        ptInfo->setDob("03 Mar 2019");
        ptInfo->setImplantDate("03 Mar 2019");
        ptInfo->setPhone("555-555-5555");
        ptInfo->setClinician("AClinician");
        ptInfo->setClinic("AClinic");
        ptInfo->setSerialNumber("ABC123");
        ptInfo->setImplantDoctor("ImplantDr");
        ptInfo->setImplantLocation("ImplantLocation");
        ptInfo->setBaselineCode("baseline");
        ptInfo->setCalCode("calcode");
    }

    ~PatientInfoTests();

    std::shared_ptr<PatientInfo> ptInfo;
};

PatientInfoTests::~PatientInfoTests() {}

TEST_F(PatientInfoTests, patientIdReturnsPatientInfoPatientId)
{
    ASSERT_EQ(123, ptInfo->patientId());
}

TEST_F(PatientInfoTests, setPatientIdSetsThePatientId)
{
    ptInfo->setPatientId(234);
    ASSERT_EQ(234, ptInfo->patientId());
}

TEST_F(PatientInfoTests, setPatientIdEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));
    ptInfo->setPatientId(234);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, patientIdPropertyReturnsThePatientId)
{
    int id = ptInfo->property("patientId").toInt();

    ASSERT_EQ(id, 123);
}

TEST_F(PatientInfoTests, patientIdPropertySetsThePatientId)
{
    ptInfo->setProperty("patientId", 234);
    ASSERT_EQ(ptInfo->patientId(), 234);
}

TEST_F(PatientInfoTests, patientIdPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("patientId");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, firstNameReturnsPatientInfoFirstName)
{
    ASSERT_EQ("PtFirstName", ptInfo->firstName());
}

TEST_F(PatientInfoTests, setFirstNameSetsTheFirstName)
{
    ptInfo->setFirstName("FirstName");
    ASSERT_EQ("FirstName", ptInfo->firstName());
}

TEST_F(PatientInfoTests, setFirstNameEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));
    ptInfo->setFirstName("FirstName");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, firstNamePropertyReturnsTheFirstName)
{
    QString firstName = ptInfo->property("firstName").toString();

    ASSERT_EQ(firstName, "PtFirstName");
}

TEST_F(PatientInfoTests, firstNamePropertySetsTheFirstName)
{
    ptInfo->setProperty("firstName", "name");
    ASSERT_EQ(ptInfo->firstName(), "name");
}

TEST_F(PatientInfoTests, firstNamePropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("firstName");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}


TEST_F(PatientInfoTests, middleInitialReturnsPatientInfoMiddleInitial)
{
    ASSERT_EQ("PtMiddleInitial", ptInfo->middleInitial());
}

TEST_F(PatientInfoTests, setMiddleInitialSetsTheMiddleInitial)
{
    ptInfo->setMiddleInitial("MiddleInitial");
    ASSERT_EQ("MiddleInitial", ptInfo->middleInitial());
}

TEST_F(PatientInfoTests, setMiddleInitialEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));
    ptInfo->setMiddleInitial("MInitial");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, middleInitialPropertyReturnsTheMiddleInitial)
{
    QString middleInitial = ptInfo->property("middleInitial").toString();

    ASSERT_EQ(middleInitial, "PtMiddleInitial");
}

TEST_F(PatientInfoTests, middleInitialPropertySetsTheMiddleInitial)
{
    ptInfo->setProperty("middleInitial", "initial");

    ASSERT_EQ(ptInfo->middleInitial(), "initial");
}

TEST_F(PatientInfoTests, middleInitialPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("middleInitial");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}


TEST_F(PatientInfoTests, lastNameReturnsPatientInfoLastName)
{
    ASSERT_EQ("PtLastName", ptInfo->lastName());
}

TEST_F(PatientInfoTests, setLastNameSetsTheLastName)
{
    ptInfo->setLastName("LastName");
    ASSERT_EQ("LastName", ptInfo->lastName());
}

TEST_F(PatientInfoTests, setLastNameEmitpatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));
    ptInfo->setLastName("LName");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, lastNamePropertyReturnsTheLastName)
{
    QString lastName = ptInfo->property("lastName").toString();

    ASSERT_EQ(lastName, "PtLastName");
}

TEST_F(PatientInfoTests, lastNamePropertySetsThelastName)
{
    ptInfo->setProperty("lastName", "lastname");
    ASSERT_EQ(ptInfo->lastName(), "lastname");
}

TEST_F(PatientInfoTests, lastNamePropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("lastName");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}


TEST_F(PatientInfoTests, patientNameReturnsPatientInfoPatientName)
{
    ASSERT_EQ("PtName", ptInfo->patientName());
}

TEST_F(PatientInfoTests, setPatientNameSetsThePatientName)
{
    ptInfo->setPatientName("PatientName");
    ASSERT_EQ("PatientName", ptInfo->patientName());
}

TEST_F(PatientInfoTests, setPatientNameEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));
    ptInfo->setPatientName("name");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, patientNamePropertyReturnsThePatientName)
{
    QString patientName = ptInfo->property("patientName").toString();

    ASSERT_EQ(patientName, "PtName");
}

TEST_F(PatientInfoTests, patientNamePropertySetsThePatientName)
{
    ptInfo->setProperty("patientName", "patient");
    ASSERT_EQ(ptInfo->patientName(), "patient");
}

TEST_F(PatientInfoTests, patientNamePropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("patientName");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, dobReturnsPatientInfoDateofBirth)
{
    ASSERT_EQ("03 Mar 2019", ptInfo->dob());
}

TEST_F(PatientInfoTests, setDobSetsTheDateofBirth)
{
    ptInfo->setDob("02 Feb 2019");
    ASSERT_EQ("02 Feb 2019", ptInfo->dob());
}

TEST_F(PatientInfoTests, setDobEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));

    ptInfo->setDob("02 Feb 2019");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, dobPropertyReturnsTheDateOfBirth)
{
    QString dob = ptInfo->property("dob").toString();

    ASSERT_EQ(dob, "03 Mar 2019");
}

TEST_F(PatientInfoTests, dobPropertySetsTheDateofBirth)
{
    ptInfo->setProperty("dob", "04 Mar 2019");
    ASSERT_EQ(ptInfo->dob(), "04 Mar 2019");
}

TEST_F(PatientInfoTests, dobPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("dob");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, implantDateReturnsPatientInfoImplantDate)
{
    ASSERT_EQ("03 Mar 2019", ptInfo->implantDate());
}

TEST_F(PatientInfoTests, setImplantDateSetsTheImplantDate)
{
    ptInfo->setImplantDate("02 Feb 2019");
    ASSERT_EQ("02 Feb 2019", ptInfo->implantDate());
}

TEST_F(PatientInfoTests, setImplantDateEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));

    ptInfo->setImplantDate("02 Feb 2019");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, implantDatePropertyReturnsTheImplantDate)
{
    QString implantDate = ptInfo->property("implantDate").toString();

    ASSERT_EQ(implantDate, "03 Mar 2019");
}

TEST_F(PatientInfoTests, implantDatePropertySetsTheImplantDate)
{
    ptInfo->setProperty("implantDate", "04 Mar 2019");
    ASSERT_EQ(ptInfo->implantDate(), "04 Mar 2019");
}

TEST_F(PatientInfoTests, implantDatePropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("implantDate");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, phoneReturnsPatientInfoPhone)
{
    ASSERT_EQ("555-555-5555", ptInfo->phone());
}

TEST_F(PatientInfoTests, setPhoneSetsThePhone)
{
    ptInfo->setPhone("111-111-1111");
    ASSERT_EQ("111-111-1111", ptInfo->phone());
}

TEST_F(PatientInfoTests, setPhoneEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));

    ptInfo->setPhone("222-222-2222");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, phonePropertyReturnsThePhone)
{
    QString phone = ptInfo->property("phone").toString();

    ASSERT_EQ(phone, "555-555-5555");
}

TEST_F(PatientInfoTests, phonePropertySetsThePhone)
{
    ptInfo->setProperty("phone", "222-222-2222");
    ASSERT_EQ(ptInfo->phone(), "222-222-2222");
}

TEST_F(PatientInfoTests, phonePropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("phone");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, clinicianReturnsPatientInfoClinician)
{
    ASSERT_EQ("AClinician", ptInfo->clinician());
}

TEST_F(PatientInfoTests, setClinicianSetsTheClinician)
{
    ptInfo->setClinician("PtClinician");
    ASSERT_EQ("PtClinician", ptInfo->clinician());
}

TEST_F(PatientInfoTests, setClinicianEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));

    ptInfo->setClinician("PtClinician");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, clinicianPropertyReturnsTheClinician)
{
    QString clinician = ptInfo->property("clinician").toString();

    ASSERT_EQ(clinician, "AClinician");
}

TEST_F(PatientInfoTests, clinicianPropertySetsTheClinician)
{
    ptInfo->setProperty("clinician", "Clinician");
    ASSERT_EQ(ptInfo->clinician(), "Clinician");
}

TEST_F(PatientInfoTests, clinicianPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("clinician");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, clinicReturnsPatientInfoClinic)
{
    ASSERT_EQ("AClinic", ptInfo->clinic());
}

TEST_F(PatientInfoTests, setClinicSetsTheClinic)
{
    ptInfo->setClinic("PtClinic");
    ASSERT_EQ("PtClinic", ptInfo->clinic());
}

TEST_F(PatientInfoTests, setClinicEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));

    ptInfo->setClinic("PtClinic");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, clinicPropertyReturnsTheClinic)
{
    QString clinic = ptInfo->property("clinic").toString();

    ASSERT_EQ(clinic, "AClinic");
}

TEST_F(PatientInfoTests, clinicPropertySetsTheClinic)
{
    ptInfo->setProperty("clinic", "Clinic");
    ASSERT_EQ(ptInfo->clinic(), "Clinic");
}

TEST_F(PatientInfoTests, clinicPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("clinic");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, serialNumberReturnsPatientInfoSerialNumber)
{
    ASSERT_EQ("ABC123", ptInfo->serialNumber());
}

TEST_F(PatientInfoTests, setSerialNumberSetsTheSerialNumber)
{
    ptInfo->setSerialNumber("DEF456");
    ASSERT_EQ("DEF456", ptInfo->serialNumber());
}

TEST_F(PatientInfoTests, setSerialNumberEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));

    ptInfo->setSerialNumber("DEF456");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, serialNumberPropertyReturnsTheSerialNumber)
{
    QString serialNumber = ptInfo->property("serialNumber").toString();

    ASSERT_EQ(serialNumber, "ABC123");
}

TEST_F(PatientInfoTests, serialNumberPropertySetsTheSerialNumber)
{
    ptInfo->setProperty("serialNumber", "SerialNum");
    ASSERT_EQ(ptInfo->serialNumber(), "SerialNum");
}

TEST_F(PatientInfoTests, serialNumberPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("serialNumber");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, implantDoctorReturnsPatientInfoImplantDoctor)
{
    ASSERT_EQ("ImplantDr", ptInfo->implantDoctor());
}

TEST_F(PatientInfoTests, setImplantDoctorSetsTheImplantDoctor)
{
    ptInfo->setImplantDoctor("ImplantDoctor");
    ASSERT_EQ("ImplantDoctor", ptInfo->implantDoctor());
}

TEST_F(PatientInfoTests, setImplantDoctorEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));

    ptInfo->setImplantDoctor("DoctorWho");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, implantDoctorPropertyReturnsTheImplantDoctor)
{
    QString implantDoctor = ptInfo->property("implantDoctor").toString();

    ASSERT_EQ(implantDoctor, "ImplantDr");
}

TEST_F(PatientInfoTests, implantDoctorPropertySetsTheImplantDoctor)
{
    ptInfo->setProperty("implantDoctor", "DoctorWho");
    ASSERT_EQ(ptInfo->implantDoctor(), "DoctorWho");
}

TEST_F(PatientInfoTests, implantDoctorPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("implantDoctor");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, implantLocationReturnsPatientInfoImplantLocation)
{
    ASSERT_EQ("ImplantLocation", ptInfo->implantLocation());
}

TEST_F(PatientInfoTests, setImplantLocationSetsTheImplantLocation)
{
    ptInfo->setImplantLocation("Location");
    ASSERT_EQ("Location", ptInfo->implantLocation());
}

TEST_F(PatientInfoTests, setImplantLocationEmitspatientInfoChangedSignal)
{
    QSignalSpy spy(ptInfo.get(), SIGNAL(patientInfoChanged()));

    ptInfo->setImplantLocation("Location");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientInfoTests, implantLocationPropertyReturnsTheImplantLocation)
{
    QString implantLocation = ptInfo->property("implantLocation").toString();

    ASSERT_EQ(implantLocation, "ImplantLocation");
}

TEST_F(PatientInfoTests, implantLocationPropertySetsTheImplantLocation)
{
    ptInfo->setProperty("implantLocation", "Location");
    ASSERT_EQ(ptInfo->implantLocation(), "Location");
}

TEST_F(PatientInfoTests, implantLocationPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("implantLocation");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("patientInfoChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, uploadPendingReturnsTrueWhenUploadIsPending)
{
    ptInfo->setUploadPending(true);
    ASSERT_TRUE(ptInfo->uploadPending());
}

TEST_F(PatientInfoTests, uploadPendingReturnsFalseWhenUploadIsNotPending)
{
    ptInfo->setUploadPending(false);
    ASSERT_FALSE(ptInfo->uploadPending());
}

TEST_F(PatientInfoTests, uploadPendingPropertyReturnsTrueWhenUpdloadIsPending)
{
    ptInfo->setUploadPending(true);
    bool pending = ptInfo->property("uploadPending").toBool();

    ASSERT_TRUE(pending);
}

TEST_F(PatientInfoTests, uploadPendingPropertyReturnsFalseWhenUpdloadIsNotPending)
{
    ptInfo->setUploadPending(false);
    bool pending = ptInfo->property("uploadPending").toBool();

    ASSERT_FALSE(pending);
}

TEST_F(PatientInfoTests, uploadPendingPropertySetsTheUploadPending)
{
    ptInfo->setProperty("uploadPending", true);
    ASSERT_EQ(ptInfo->uploadPending(), true);
}

TEST_F(PatientInfoTests, uploadPendingPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("uploadPending");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("uploadPendingChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, selectedReturnsFalseWhenPatientInfoIsNotSelected)
{
    ptInfo->setSelected(false);
    ASSERT_FALSE(ptInfo->selected());
}

TEST_F(PatientInfoTests, selectedReturnsTrueWhenPatientInfoIsSelected)
{
    ptInfo->setSelected(true);
    ASSERT_TRUE(ptInfo->selected());
}

TEST_F(PatientInfoTests, selectedPropertyReturnsTrueWhenPatientInfoIsSelected)
{
    ptInfo->setSelected(true);
    bool isSelected = ptInfo->property("selected").toBool();

    ASSERT_TRUE(isSelected);
}

TEST_F(PatientInfoTests, selectedPropertyReturnsFalseWhenNotPatientInfoIsNotSelected)
{
    ptInfo->setSelected(false);
    bool isSelected = ptInfo->property("selected").toBool();

    ASSERT_FALSE(isSelected);
}

TEST_F(PatientInfoTests, selectedPropertySetsPatientInfoSelected)
{
    ptInfo->setProperty("selected", true);
    ASSERT_EQ(ptInfo->selected(), true);
}

TEST_F(PatientInfoTests, selectedPropertyHasNotifySignal)
{
    int propertyIndex = ptInfo->metaObject()->indexOfProperty("selected");
    QMetaProperty property = ptInfo->metaObject()->property(propertyIndex);

    ASSERT_STREQ("selectedChanged", property.notifySignal().name().data());
}

TEST_F(PatientInfoTests, baselineCodeReturnsBaselineCode)
{
    ASSERT_EQ("baseline", ptInfo->baselineCode());
}

TEST_F(PatientInfoTests, setBaselineCodeSetsTheBaselineCode)
{
    ptInfo->setBaselineCode("code");
    ASSERT_EQ("code", ptInfo->baselineCode());
}

TEST_F(PatientInfoTests, calCodeReturnsCalCode)
{
    ASSERT_EQ("calcode", ptInfo->calCode());
}

TEST_F(PatientInfoTests, setCalCodeSetsTheCalCode)
{
    ptInfo->setCalCode("code");
    ASSERT_EQ("code", ptInfo->calCode());
}

TEST_F(PatientInfoTests, ConstructorSetsValuesToPatientDataValues)
{
    PatientData ptData;
    ptData.patientId = 845656;
    ptData.firstName = "Paula";
    ptData.middleInitial = "A";
    ptData.lastName = "Tyler";
    ptData.dob = "08 Aug 1945";
    ptData.phone = "(555)212-2222";
    ptData.clinic = "Clinic";
    ptData.clinician = "Clinician";
    ptData.implantDoctor = "Doctor";
    ptData.implantDate = "(555)111-1111";
    ptData.serialNumber = "ABC123";
    ptData.implantLocation = "right";
    ptData.uploadPending = false;

    PatientInfo ptInfo(ptData);

    ASSERT_EQ(static_cast<int>(ptData.patientId), ptInfo.patientId());
    ASSERT_EQ(QString::fromStdString(ptData.firstName), ptInfo.firstName());
    ASSERT_EQ(QString::fromStdString(ptData.middleInitial), ptInfo.middleInitial());
    ASSERT_EQ(QString::fromStdString(ptData.lastName), ptInfo.lastName());
    ASSERT_EQ(QString::fromStdString(ptData.dob), ptInfo.dob());
    ASSERT_EQ(QString::fromStdString(ptData.implantDate), ptInfo.implantDate());
    ASSERT_EQ(QString::fromStdString(ptData.phone), ptInfo.phone());
    ASSERT_EQ(QString::fromStdString(ptData.clinic), ptInfo.clinic());
    ASSERT_EQ(QString::fromStdString(ptData.clinician), ptInfo.clinician());
    ASSERT_EQ(QString::fromStdString(ptData.implantDoctor), ptInfo.implantDoctor());
    ASSERT_EQ(QString::fromStdString(ptData.serialNumber), ptInfo.serialNumber());
    ASSERT_EQ(ptData.uploadPending, ptInfo.uploadPending());
    ASSERT_EQ(QString::fromStdString(ptData.implantLocation), ptInfo.implantLocation());
}

TEST_F(PatientInfoTests, ToPatientDataReturnsCorrectValuesForPatientInfo)
{
    PatientData ptData;
    ptData.patientId = 845656;
    ptData.firstName = "Paula";
    ptData.middleInitial = "A";
    ptData.lastName = "Tyler";
    ptData.dob = "08 Aug 1945";
    ptData.phone = "(555)212-2222";
    ptData.clinic = "Clinic";
    ptData.clinician = "Clinician";
    ptData.implantDoctor = "Doctor";
    ptData.implantDate = "(555)111-1111";
    ptData.serialNumber = "ABC123";
    ptData.implantLocation = "right";
    ptData.uploadPending = false;

    PatientInfo ptInfo(ptData);

    ASSERT_EQ(ptData.patientId, ptInfo.toPatientData().patientId);
    ASSERT_EQ(ptData.firstName, ptInfo.toPatientData().firstName);
    ASSERT_EQ(ptData.middleInitial, ptInfo.toPatientData().middleInitial);
    ASSERT_EQ(ptData.lastName, ptInfo.toPatientData().lastName);
    ASSERT_EQ(ptData.dob, ptInfo.toPatientData().dob);
    ASSERT_EQ(ptData.implantDate, ptInfo.toPatientData().implantDate);
    ASSERT_EQ(ptData.phone, ptInfo.toPatientData().phone);
    ASSERT_EQ(ptData.clinic, ptInfo.toPatientData().clinic);
    ASSERT_EQ(ptData.clinician, ptInfo.toPatientData().clinician);
    ASSERT_EQ(ptData.implantDoctor, ptInfo.toPatientData().implantDoctor);
    ASSERT_EQ(ptData.serialNumber, ptInfo.toPatientData().serialNumber);
    ASSERT_EQ(ptData.uploadPending, ptInfo.toPatientData().uploadPending);
    ASSERT_EQ(ptData.implantLocation, ptInfo.toPatientData().implantLocation);
}
