﻿#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <QDate>
#include <QCoreApplication>
#include <QSignalSpy>
#include <thread>

#include "presentation/DateTimeController.h"

using namespace ::testing;

class DateTimeControllerTests : public Test
{
public:
    DateTimeControllerTests()
        : controller ("d MMM yyyy", "hh:mm")
    {}

protected:
    DateTimeController controller;
};

TEST_F(DateTimeControllerTests, dateChangedSignalEmittedWhenDateChanges)
{
    QSignalSpy spy(&controller, SIGNAL(dateChanged(QString)));

    std::this_thread::sleep_for(std::chrono::seconds(2));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(DateTimeControllerTests, timeChangedSignalEmittedWhenTimeChanges)
{
    QSignalSpy spy(&controller, SIGNAL(timeChanged(QString)));

    std::this_thread::sleep_for(std::chrono::seconds(1));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(DateTimeControllerTests, dateNowReturnsFormattedCurrentDate)
{
    QString expectedDate = QDate::currentDate().toString("d MMM yyyy");

    std::this_thread::sleep_for(std::chrono::seconds(1));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(expectedDate, controller.dateNow());
}

TEST_F(DateTimeControllerTests, timeNowReturnsFormattedCurrentTime)
{
    QString expectedTimeAlpha = QTime::currentTime().toString("hh:mm");

    std::this_thread::sleep_for(std::chrono::seconds(1));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    QString actualTime = controller.timeNow();

    std::this_thread::sleep_for(std::chrono::seconds(1));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    QString expectedTimeBeta = QTime::currentTime().toString("hh:mm");

    ASSERT_TRUE((expectedTimeAlpha == actualTime) || (expectedTimeBeta == actualTime));
}

TEST_F(DateTimeControllerTests, dateNowPropertyReturnsTheDate)
{
    QString expectedDate = QDate::currentDate().toString("d MMM yyyy");

    std::this_thread::sleep_for(std::chrono::seconds(2));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    QString actualDate = controller.property("dateNow").toString();

    ASSERT_EQ(expectedDate, actualDate);
}

TEST_F(DateTimeControllerTests, dateNowPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("dateNow");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("dateChanged", property.notifySignal().name().data());
}

TEST_F(DateTimeControllerTests, timeNowPropertyReturnsTheTime)
{
    QString expectedTimeAlpha = QTime::currentTime().toString("hh:mm");

    std::this_thread::sleep_for(std::chrono::seconds(1));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    QString actualTime = controller.property("timeNow").toString();

    std::this_thread::sleep_for(std::chrono::seconds(1));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    QString expectedTimeBeta = QTime::currentTime().toString("hh:mm");

    ASSERT_TRUE((expectedTimeAlpha == actualTime) || (expectedTimeBeta == actualTime));
}

TEST_F(DateTimeControllerTests, timeNowPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("timeNow");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("timeChanged", property.notifySignal().name().data());
}
