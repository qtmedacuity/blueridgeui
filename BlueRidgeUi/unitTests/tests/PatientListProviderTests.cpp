﻿#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmem_command.h>

#include "unitTests/mocks/MockMessageParser.h"
#include "providers/PatientListProvider.h"

using namespace ::testing;

class PatientListProviderTests : public Test
{
public:
    PatientListProviderTests()
    {

        PatientData patient;
        patient.patientId = 845656;
        patient.patientKey = "845656";
        patient.firstName = "Paula";
        patient.lastName = "Tyler";
        patient.dob = "08 Aug 1945";
        patient.phone = "(555)212-2222";
        patient.uploadPending = false;

        _patientList.push_back(patient);

        patient.patientId = 456843;
        patient.patientKey = "456843";
        patient.lastName = "Campbell";
        patient.firstName = "Joseph";
        patient.middleInitial = "P" ;
        patient.dob = "25 Dec 1967";
        patient.phone = "(555)212-2222";
        patient.uploadPending = false;

        _patientList.push_back(patient);

        patient.patientId = 354888;
        patient.patientKey = "354888";
        patient.lastName = "Pond";
        patient.firstName = "Amelia";
        patient.dob = "17 Sep 1955";
        patient.phone = "(555)212-2222";
        patient.uploadPending = false;

        _patientList.push_back(patient);

        _patientListStr << "{\n\t\"Status\" : \"SUCCESS\",";
        _patientListStr << "\n\t\"RefreshDate\" : \"" << cmem_date::now() << "\",";
        _patientListStr << "\n\t\"RefreshTime\" : \"02:33:10 PM\",";
        _patientListStr << "\n\t\"Patients\": [";

        for(unsigned int i =0; i < _patientList.size(); ++i)
        {
            _patientListStr << _patientList.at(i).toCMemPatient().jsonify();

            if(i < (_patientList.size() - 1))
                _patientListStr << ",\n";
            else {
                _patientListStr << "\n";
            }
        }

        _patientListStr <<  "]\n}\n";
    }

    ~PatientListProviderTests() override;

    bool PatientDataEqual(PatientData const& pt1, PatientData const& pt2)
    {
        return (pt1.patientId       == pt2.patientId &&
                pt1.patientKey      == pt2.patientKey &&
                pt1.dob             == pt2.dob &&
                pt1.implantDate     == pt2.implantDate &&
                pt1.phone           == pt2.phone &&
                pt1.serialNumber    == pt2.serialNumber &&
                pt1.uploadPending   == pt2.uploadPending &&
                pt1.implantLocation == pt2.implantLocation &&
                pt1.lastName        == pt2.lastName &&
                pt1.firstName       == pt2.firstName &&
                pt1.middleInitial   == pt2.middleInitial);
    }

    std::condition_variable _condition;
    std::stringstream _patientListStr;
    std::vector<PatientData> _patientList;
};

PatientListProviderTests::~PatientListProviderTests() {}

TEST_F(PatientListProviderTests, ConstructorCallsMessageParserRegisterCallback)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();

    EXPECT_CALL(parser, registerCallback(_))
            .Times(1);

    PatientListProvider provider(parser);
}

TEST_F(PatientListProviderTests, PatientListCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    unsigned long count = 0;
    provider.registerPatientListCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(""));
    parser._fake._func(eGET_PATIENT_LIST, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}

TEST_F(PatientListProviderTests, PatientListCallbacksRegisteredWithProviderAreExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    unsigned long count = 0;
    provider.registerPatientListCallback([&]{ count++; });

    std::string value = "";
    provider.registerPatientListCallback([&]{ value = "string set"; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(""));
    parser._fake._func(eGET_PATIENT_LIST, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
    ASSERT_EQ("string set", value);
}

TEST_F(PatientListProviderTests, PatientListCallbackUnregisterdWithProviderAreNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    unsigned long count = 0;
    unsigned long cmdId = provider.registerPatientListCallback([&]{ count++; });

    std::string value = "string not set";
    provider.registerPatientListCallback([&]{ value = "string set"; _condition.notify_one(); });

    provider.unregisterPatientListCallback(cmdId);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(""));
    parser._fake._func(eGET_PATIENT_LIST, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count);
    ASSERT_EQ("string set", value);
}

TEST_F(PatientListProviderTests, RefreshCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    unsigned long count = 0;
    provider.registerRefreshCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(""));
    parser._fake._func(ePERFORM_MERLIN_DNLD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}

TEST_F(PatientListProviderTests, RefreshCallbacksRegisteredWithProviderAreExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    unsigned long count = 0;
    provider.registerRefreshCallback([&]{ count++; });

    std::string value = "";
    provider.registerRefreshCallback([&]{ value = "string set"; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(""));
    parser._fake._func(ePERFORM_MERLIN_DNLD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
    ASSERT_EQ("string set", value);
}

TEST_F(PatientListProviderTests, RefreshCallbackUnregisterdWithProviderAreNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    unsigned long count = 0;
    unsigned long cmdId = provider.registerRefreshCallback([&]{ count++; });

    std::string value = "string not set";
    provider.registerRefreshCallback([&]{ value = "string set"; _condition.notify_all(); });

    provider.unregisterRefreshCallback(cmdId);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(""));
    parser._fake._func(ePERFORM_MERLIN_DNLD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count);
    ASSERT_EQ("string set", value);
}

TEST_F(PatientListProviderTests, CallbacksOfDiffrentCommandTypesRegisteredWithProviderAreExecutedOnlyWhenSignalUpdateForThatCommandIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    unsigned long count1 = 1;
    provider.registerPatientListCallback([&]{ count1++; _condition.notify_all(); });

    unsigned long count2 = 1;
    provider.registerRefreshCallback([&]{ count2++; });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(""));
    parser._fake._func(eGET_PATIENT_LIST, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(1, count2);
}

TEST_F(PatientListProviderTests, CallbacksFromParserOfDifferentCommandTypeAreNotHandledByProvider)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    unsigned long count1 = 1;
    provider.registerPatientListCallback([&]{ count1++; _condition.notify_all(); });

    unsigned long count2 = 1;
    provider.registerRefreshCallback([&]{ count2++; });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(""));
    parser._fake._func(eUNKNOWN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(1, count2);
}

TEST_F(PatientListProviderTests, RequestPatientsWaitingForImplantSendsCommandToParserWithGetPatientListCommandAndWaitingImplantType)
{
    NiceMock<MockMessageParser> parser;
    PatientListProvider provider(parser);

    Json::Value payload;
    payload["PatientId"] = "0";
    payload["Type"] = eRecordType::eWaitingImplant;

    EXPECT_CALL(parser, sendCommand(eGET_PATIENT_LIST, payload.toStyledString()))
            .Times(1);

    provider.requestPatientsWaitingForImplant();
}

TEST_F(PatientListProviderTests, RequestPatientWithImplantSendsCommandToParserWithGetPatientListCommandAndImplantedType)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    Json::Value payload;
    payload["PatientId"] = "0";
    payload["Type"] = eRecordType::eImplanted;

    EXPECT_CALL(parser, sendCommand(eGET_PATIENT_LIST, payload.toStyledString()))
            .Times(1);

    provider.requestPatientsWithImplant();
}

TEST_F(PatientListProviderTests, RequestAllPatientsSendsCommandToParserWithGetPatientListCommandAndAllType)
{
    NiceMock<MockMessageParser> parser;
    PatientListProvider provider(parser);

    Json::Value payload;
    payload["PatientId"] = "0";
    payload["Type"] = eRecordType::eAll;

    EXPECT_CALL(parser, sendCommand(eGET_PATIENT_LIST, payload.toStyledString()))
            .Times(1);

    provider.requestAllPatients();
}

TEST_F(PatientListProviderTests, RequestRefreshSendsCommandToParserWithPerformMerlinDnldCommand)
{
    NiceMock<MockMessageParser> parser;
    PatientListProvider provider(parser);

    EXPECT_CALL(parser, sendCommand(ePERFORM_MERLIN_DNLD))
            .Times(1);

    provider.requestRefresh();
}

TEST_F(PatientListProviderTests, GetPatientListReturnsAListOfPatients)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    provider.registerPatientListCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_patientListStr.str()));
    parser._fake._func(eGET_PATIENT_LIST, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    std::vector<PatientData> patientList = provider.patientList();

    ASSERT_EQ(patientList.size(), _patientList.size());

    for(unsigned int i=0; i < _patientList.size(); ++i)
    {
        PatientData pt1 = patientList.at(i);
        PatientData pt2 = _patientList.at(i);
        ASSERT_TRUE(PatientDataEqual(pt1, pt2));
    }
}

TEST_F(PatientListProviderTests, RefreshDateReturnsThePatientListRefreshDate)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    provider.registerPatientListCallback([&]{_condition.notify_one();});

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_patientListStr.str()));
    parser._fake._func(eGET_PATIENT_LIST, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    provider.registerRefreshCallback([&]{ _condition.notify_one(); });
    parser._fake._func(ePERFORM_MERLIN_DNLD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(cmem_date::now(), provider.refreshDate());
}

TEST_F(PatientListProviderTests, RefreshTimeReturnsThePatientListRefreshTime)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    PatientListProvider provider(parser);

    provider.registerPatientListCallback([&]{_condition.notify_one();});

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_patientListStr.str()));
    parser._fake._func(eGET_PATIENT_LIST, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    provider.registerRefreshCallback([&]{ _condition.notify_one(); });
    parser._fake._func(ePERFORM_MERLIN_DNLD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("02:33:10", provider.refreshTime());
}
