#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QDate>
#include <QSignalSpy>

#include "unitTests/mocks/MockRecordingProvider.h"
#include "unitTests/mocks/MockTransactionProvider.h"
#include "presentation/RecordingListModel.h"
#include "presentation/Recording.h"
#include "providers/RecordingProvider.h"

using namespace ::testing;

class RecordingListModelTests : public Test
{
public:
    RecordingListModelTests()
        : recListModel(_recordingProvider, _transactionProvider)
    {
        Recording *recording = new Recording();
        recording->setRecordingId(1234);
        recording->setRecordingTime("11:42:23 AM");
        recording->setRecordingDate("18 APR 2019");
        recording->setSystolic(33);
        recording->setDiastolic(19);
        recording->setMean(23);
        recording->setReferenceSystolic(34);
        recording->setReferenceDiastolic(20);
        recording->setReferenceMean(24);
        recording->setSensorStrength(91);
        recording->setHeartRate(72);
        recording->setNotes("Something");
        recording->setPosition("Horizontal");
        recording->setWaveFormFile("/tmp/valid_file.xml");

        _testRecordings.append(recording);
        recListModel.append(recording);

        recording = new Recording();
        recording->setRecordingId(5678);
        recording->setRecordingTime("10:38:11 AM");
        recording->setRecordingDate("19 APR 2019");
        recording->setSystolic(32);
        recording->setDiastolic(18);
        recording->setMean(22);
        recording->setReferenceSystolic(35);
        recording->setReferenceDiastolic(21);
        recording->setReferenceMean(25);
        recording->setSensorStrength(89);
        recording->setHeartRate(75);
        recording->setNotes("");
        recording->setPosition("30_Degrees");
        recording->setWaveFormFile("/tmp/valid_file2.xml");

        _testRecordings.append(recording);
        recListModel.append(recording);
    }
    ~RecordingListModelTests();

    NiceMock<MockRecordingProvider> _recordingProvider;
    NiceMock<MockTransactionProvider> _transactionProvider;
    RecordingListModel recListModel;
    QList<Recording*> _testRecordings;
};

RecordingListModelTests::~RecordingListModelTests()
{
    _testRecordings.clear();
}

TEST_F(RecordingListModelTests, constructorRegistersCallbacksWithProvider)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    NiceMock<MockTransactionProvider> transactionProvider;

    EXPECT_CALL(recordingProvider, registerSaveRecordingCallback(_))
            .Times(1);
    EXPECT_CALL(recordingProvider, registerDeleteRecordingCallback(_))
            .Times(1);
    EXPECT_CALL(transactionProvider, registerGetTransactionDetailsCallback(_))
            .Times(1);

    RecordingListModel listModel(recordingProvider, transactionProvider);
}

TEST_F(RecordingListModelTests, rowCountReturnsTheNumbeRowsInTheModel)
{
    int expectedCount = _testRecordings.count();
    int count = recListModel.rowCount();

    ASSERT_EQ(expectedCount, count);
}

TEST_F(RecordingListModelTests, getRecordingByIndexWithValidIndexReturnsData)
{
    int index = 1;
    Recording *result = recListModel.get(index);

    ASSERT_EQ(_testRecordings.at(index)->recordingId(), result->recordingId());
    ASSERT_EQ(_testRecordings.at(index)->recordingTime(), result->recordingTime());
    ASSERT_EQ(_testRecordings.at(index)->recordingDate(), result->recordingDate());
    ASSERT_EQ(_testRecordings.at(index)->systolic(), result->systolic());
    ASSERT_EQ(_testRecordings.at(index)->diastolic(), result->diastolic());
    ASSERT_EQ(_testRecordings.at(index)->mean(), result->mean());
    ASSERT_EQ(_testRecordings.at(index)->referenceSystolic(), result->referenceSystolic());
    ASSERT_EQ(_testRecordings.at(index)->referenceDiastolic(), result->referenceDiastolic());
    ASSERT_EQ(_testRecordings.at(index)->referenceMean(), result->referenceMean());
    ASSERT_EQ(_testRecordings.at(index)->sensorStrength(), result->sensorStrength());
    ASSERT_EQ(_testRecordings.at(index)->heartRate(), result->heartRate());
    ASSERT_EQ(_testRecordings.at(index)->notes(), result->notes());
    ASSERT_EQ(_testRecordings.at(index)->position(), result->position());
    ASSERT_EQ(_testRecordings.at(index)->waveFormFile(), result->waveFormFile());

}

TEST_F(RecordingListModelTests, getRecordingByIndexWithInvalidIndexReturnsNullPtr)
{
    int index = 4;
    Recording *result = recListModel.get(index);
    ASSERT_EQ(nullptr, result);
}

TEST_F(RecordingListModelTests, dataReturnsRecordingIdForIndexAndRole)
{
    int index = 0;
    int expectedValue = _testRecordings.at(index)->recordingId();
    QModelIndex idx = recListModel.index(index, 0);
    int value = recListModel.data(idx, RecordingListModel::RecordingRole::RecordingId).toInt();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsRecordingTimeForIndexAndRole)
{
    int index = 0;
    QString expectedValue = _testRecordings.at(index)->recordingTime();
    QModelIndex idx = recListModel.index(index, 0);
    QString value = recListModel.data(idx, RecordingListModel::RecordingRole::RecordingTime).toString();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsSystolicForIndexAndRole)
{
    int index = 0;
    int expectedValue = _testRecordings.at(index)->systolic();
    QModelIndex idx = recListModel.index(index, 0);
    int value = recListModel.data(idx, RecordingListModel::RecordingRole::Systolic).toInt();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsDiastolicForIndexAndRole)
{
    int index = 0;
    int expectedValue = _testRecordings.at(index)->diastolic();
    QModelIndex idx = recListModel.index(index, 0);
    int value = recListModel.data(idx, RecordingListModel::RecordingRole::Diastolic).toInt();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsMeanForIndexAndRole)
{
    int index = 0;
    int expectedValue = _testRecordings.at(index)->mean();
    QModelIndex idx = recListModel.index(index, 0);
    int value = recListModel.data(idx, RecordingListModel::RecordingRole::Mean).toInt();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsHeartRateForIndexAndRole)
{
    int index = 0;
    int expectedValue = _testRecordings.at(index)->heartRate();
    QModelIndex idx = recListModel.index(index, 0);
    int value = recListModel.data(idx, RecordingListModel::RecordingRole::HeartRate).toInt();

    ASSERT_EQ(expectedValue, value);
}


TEST_F(RecordingListModelTests, dataReturnsReferenceSystolicForIndexAndRole)
{
    int index = 0;
    int expectedValue = _testRecordings.at(index)->referenceSystolic();
    QModelIndex idx = recListModel.index(index, 0);
    int value = recListModel.data(idx, RecordingListModel::RecordingRole::ReferenceSystolic).toInt();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsReferenceDiastolicForIndexAndRole)
{
    int index = 0;
    int expectedValue = _testRecordings.at(index)->referenceDiastolic();
    QModelIndex idx = recListModel.index(index, 0);
    int value = recListModel.data(idx, RecordingListModel::RecordingRole::ReferenceDiastolic).toInt();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsReferenceMeanForIndexAndRole)
{
    int index = 0;
    int expectedValue = _testRecordings.at(index)->referenceMean();
    QModelIndex idx = recListModel.index(index, 0);
    int value = recListModel.data(idx, RecordingListModel::RecordingRole::ReferenceMean).toInt();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsSensorStrengthForIndexAndRole)
{
    int index = 0;
    int expectedValue = _testRecordings.at(index)->sensorStrength();
    QModelIndex idx = recListModel.index(index, 0);
    int value = recListModel.data(idx, RecordingListModel::RecordingRole::SensorStrength).toInt();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsPositionForIndexAndRole)
{
    int index = 0;
    QString expectedValue = _testRecordings.at(index)->position();
    QModelIndex idx = recListModel.index(index, 0);
    QString value = recListModel.data(idx, RecordingListModel::RecordingRole::Position).toString();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsTrueForHaveNotesRoleWhenRecordingHasNotes)
{
    int index = 0;
    QModelIndex idx = recListModel.index(index, 0);
    bool value = recListModel.data(idx, RecordingListModel::RecordingRole::HaveNotes).toBool();

    ASSERT_TRUE(value);
}

TEST_F(RecordingListModelTests, dataReturnsFalseForHaveNotesRoleWhenRecordingHasNoNotes)
{
    int index = 1;
    QModelIndex idx = recListModel.index(index, 0);
    bool value = recListModel.data(idx, RecordingListModel::RecordingRole::HaveNotes).toBool();

    ASSERT_FALSE(value);
}

TEST_F(RecordingListModelTests, dataReturnsWaveformFileForIndexAndRole)
{
    int index = 0;
    QString expectedValue = _testRecordings.at(index)->waveFormFile();
    QModelIndex idx = recListModel.index(index, 0);
    QString value = recListModel.data(idx, RecordingListModel::RecordingRole::WaveFormFile).toString();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(RecordingListModelTests, dataReturnsEmptyVariantForUndefinedIndex)
{
    int index = 4;
    QModelIndex idx = recListModel.index(index, 0);
    QVariant value = recListModel.data(idx, RecordingListModel::RecordingRole::RecordingId);

    ASSERT_EQ(QVariant(), value);
}

TEST_F(RecordingListModelTests, dataReturnsEmptyVariantForUndefinedRole)
{
    int index = 1;
    QModelIndex idx = recListModel.index(index, 0);
    QVariant value = recListModel.data(idx, 32);

    ASSERT_EQ(QVariant(), value);
}

TEST_F(RecordingListModelTests, roleNamesReturnsCorrectNamesForRoles)
{
    QHash<int, QByteArray> roles = recListModel.roleNames();

    ASSERT_EQ(roles[RecordingListModel::RecordingRole::RecordingId], "recordingId");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::RecordingTime], "recordingTime");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::Systolic], "systolic");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::Diastolic], "diastolic");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::Mean], "mean");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::HeartRate], "heartRate");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::ReferenceSystolic], "referenceSystolic");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::ReferenceDiastolic], "referenceDiastolic");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::ReferenceMean], "referenceMean");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::SensorStrength], "sensorStrength");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::Position], "position");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::HaveNotes], "haveNotes");
    ASSERT_EQ(roles[RecordingListModel::RecordingRole::WaveFormFile], "waveFormFile");
}

TEST_F(RecordingListModelTests, recordingIsAddedToListWhenSavedRecordingCallbackInvoked)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingListModel listModel(recordingProvider, _transactionProvider);

    RecordingData recordingData;
    recordingData.recordingId = 6789;
    recordingData.systolic = 38;
    recordingData.diastolic = 19;
    recordingData.mean = 22;
    recordingData.heartRate = 79;
    recordingData.signalStrength = 85;
    recordingData.patientPosition = "Horizontal";

    ON_CALL(recordingProvider, getSavedRecording())
            .WillByDefault(Return(recordingData));

    int listCount = listModel.rowCount();

    recordingProvider._fake._saveRecordingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(listCount + 1, listModel.rowCount());
}

TEST_F(RecordingListModelTests, recordingUpdatedWhenSavedRecordingCallbackInvokedAndRecordingIdInList)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingListModel listModel(recordingProvider, _transactionProvider);

    listModel.append(_testRecordings.at(0));
    int listCount = listModel.rowCount();
    ASSERT_EQ(1, listCount);

    RecordingData recordingData = _testRecordings.at(0)->toRecordingData();
    recordingData.notes = "some other notes";
    recordingData.mean = 25;

    ON_CALL(recordingProvider, getSavedRecording())
            .WillByDefault(Return(recordingData));

    recordingProvider._fake._saveRecordingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    Recording *updatedRecording = listModel.get(0);

    ASSERT_EQ(listCount, listModel.rowCount());
    ASSERT_EQ("some other notes", updatedRecording->notes());
    ASSERT_EQ(25, updatedRecording->mean());
}

TEST_F(RecordingListModelTests, recordingDeletedWhenDeletedRecordingCallbackInvoked)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingListModel listModel(recordingProvider, _transactionProvider);

    int recordingId = _testRecordings.at(0)->recordingId();
    listModel.append(_testRecordings.at(0));
    int listCount = listModel.rowCount();
    ASSERT_EQ(1, listCount);

    ON_CALL(recordingProvider, getDeletedRecordingId())
            .WillByDefault(Return(recordingId));

    recordingProvider._fake._deleteRecordingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(listCount - 1, listModel.rowCount());
}

TEST_F(RecordingListModelTests, recordingListIsCreatedWhenTransactionDetailsCallbackInvoked)
{
    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    RecordingListModel listModel(_recordingProvider, transactionProvider);

    EventData eventData;
    RecordingData recordingData1;
    recordingData1.recordingId = 6789;
    recordingData1.systolic = 38;
    recordingData1.diastolic = 19;
    recordingData1.mean = 22;
    recordingData1.heartRate = 79;
    recordingData1.signalStrength = 85;
    recordingData1.patientPosition = "Horizontal";
    eventData.recordings.push_back(recordingData1);
    RecordingData recordingData2;
    recordingData2.recordingId = 6789;
    recordingData2.systolic = 39;
    recordingData2.diastolic = 20;
    recordingData2.mean = 23;
    recordingData2.heartRate = 80;
    recordingData2.signalStrength = 86;
    recordingData2.patientPosition = "30_Degrees";
    eventData.recordings.push_back(recordingData2);

    ON_CALL(transactionProvider, getEventData())
            .WillByDefault(Return(eventData));

    transactionProvider._fake._getTransactionDetailsCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(2, listModel.rowCount());
}

TEST_F(RecordingListModelTests, clearRecordingsClearsTheRecordingList)
{
    recListModel.clearRecordings();
    int count = recListModel.rowCount();

    ASSERT_EQ(0, count);
}
