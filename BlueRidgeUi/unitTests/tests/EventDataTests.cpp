#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "providers/EventData.h"

using namespace ::testing;

class EventDataTests : public Test
{

public:
    EventDataTests() {}
    ~EventDataTests() override;

    void SetUp() override
    {
        _cmem_event.add_field("EventId", "1234");
        _cmem_event.add_field("PatientId", "4567");
        _cmem_event.add_field("Procedure", "Implant");
        _cmem_event.add_field("Clinician", "Clinician 1");
        _cmem_event.add_field("Clinic", "Clinic 1");
        _cmem_event.add_field("PAMean", "23");
        _cmem_event.add_field("Date", _eventDate.c_str());
        _cmem_event.add_field("Time", _eventTime.c_str());
    }

    cmem_event _cmem_event;
    std::string _eventDate = "23 May 2019";
    std::string _eventTime = "11:33:22";
};

EventDataTests::~EventDataTests() {}


TEST_F(EventDataTests, DefaultConstructorDefaultsValues)
{
    EventData eventData;

    ASSERT_EQ(0, eventData.patientId);
    ASSERT_EQ(0, eventData.eventId);
    ASSERT_EQ("", eventData.eventType);
    ASSERT_EQ("", eventData.date);
    ASSERT_EQ("", eventData.time);
    ASSERT_EQ(0, eventData.paMean);
    ASSERT_EQ("", eventData.clinician);
    ASSERT_EQ("", eventData.clinic);
    ASSERT_EQ(nullptr, eventData.sensorCalibration);
    ASSERT_EQ(nullptr, eventData.cardiacOutput);
    ASSERT_EQ(nullptr, eventData.rhc);
}


TEST_F(EventDataTests, ConstructorSetsEventDataValuesFromCmemEventValues)
{
    EventData eventData(_cmem_event);

    ASSERT_EQ(_cmem_event.getPatientId(), eventData.patientId);
    ASSERT_EQ(_cmem_event.getEventId(), eventData.eventId);
    ASSERT_EQ("NewImplant", eventData.eventType);
    ASSERT_EQ(_eventDate, eventData.date);
    ASSERT_EQ(_eventTime, eventData.time);
    ASSERT_EQ(_cmem_event.getPAMean(), eventData.paMean);
    ASSERT_EQ(_cmem_event.getClinician(), eventData.clinician);
    ASSERT_EQ(_cmem_event.getClinic(), eventData.clinic);

    ASSERT_EQ(nullptr, eventData.sensorCalibration);
    ASSERT_EQ(nullptr, eventData.cardiacOutput);
    ASSERT_EQ(nullptr, eventData.rhc);
    ASSERT_EQ(0, eventData.recordings.size());
}

TEST_F(EventDataTests, ConstructorSetsEventTypeToCalibrationFromCmemEventTypeOfCalibration)
{
    _cmem_event.add_field("Procedure", "Calibration");
    EventData eventData(_cmem_event);

    ASSERT_EQ("Calibration", eventData.eventType);
}

TEST_F(EventDataTests, ConstructorSetsEventTypeToFollowupFromCmemEventType)
{
    _cmem_event.add_field("Procedure", "FollowUp");
    EventData eventData(_cmem_event);

    ASSERT_EQ("FollowUp", eventData.eventType);
}

TEST_F(EventDataTests, ConstructorSetsEventSensorCalibartionDataValuesFromCmemEventCalibrationValues)
{
    cmem_calibration cmemCalibration;
    cmem_date cmemDate = cmem_date::now();
    std::string dateStr = cmemDate.fmt_date();
    cmemCalibration.setDate(cmemDate);
    cmem_time cmemTime = cmem_time::now();
    std::string timeStr = cmemTime.fmt_time();
    cmemCalibration.setTime(cmemTime);
    cmemCalibration.setSystolic(33);
    cmemCalibration.setDiastolic(25);
    cmemCalibration.setMean(28);
    cmemCalibration.setPaMean(29);
    cmemCalibration.setAvgHeartRate(75);
    cmemCalibration.setAvgSignalStrength(92);
    cmemCalibration.add_field("Notes", "notes");
    cmemCalibration.add_field("WaveformData", "valid_file.xml");

    _cmem_event.mSensorCalibration = &cmemCalibration;
    EventData eventData(_cmem_event);

    ASSERT_NE(nullptr, eventData.sensorCalibration);
    ASSERT_EQ(dateStr, eventData.sensorCalibration->date);
    ASSERT_EQ(timeStr, eventData.sensorCalibration->time);
    ASSERT_EQ(cmemCalibration.getSystolic(), eventData.sensorCalibration->systolic);
    ASSERT_EQ(cmemCalibration.getDiastolic(), eventData.sensorCalibration->diastolic);
    ASSERT_EQ(cmemCalibration.getMean(), eventData.sensorCalibration->mean);
    ASSERT_EQ(cmemCalibration.getPaMean(), eventData.sensorCalibration->reference);
    ASSERT_EQ(cmemCalibration.getAvgHeartRate(), eventData.sensorCalibration->heartRate);
    ASSERT_EQ(cmemCalibration.getAvgSignalStrength(), eventData.sensorCalibration->signalStrength);
    ASSERT_EQ("valid_file.xml", eventData.sensorCalibration->waveformFile);
    ASSERT_EQ("notes", eventData.sensorCalibration->notes);

    _cmem_event.mSensorCalibration = nullptr;
}

TEST_F(EventDataTests, ConstructorSetsEventCardiacOutputDataValuesFromCmemEventCardiacOutputValues)
{
    cmem_cardiacOutput cmemCo;
    cmemCo.add_field("Date", "23 May 2019");
    cmemCo.add_field("Time", "09:45:30");
    cmemCo.add_field("CardiacOutput", "2.3");
    cmemCo.add_field("HeartRate", "78");
    cmemCo.add_field("SignalStrength", "98");
    cmemCo.add_field("WaveformData", "valid_file.xml");

    _cmem_event.mCardiacOutput = &cmemCo;
    EventData eventData(_cmem_event);

    ASSERT_NE(nullptr, eventData.cardiacOutput);
    ASSERT_EQ("23 May 2019", eventData.cardiacOutput->date);
    ASSERT_EQ("09:45:30", eventData.cardiacOutput->time);
    ASSERT_EQ(2.3F, eventData.cardiacOutput->cardiacOutput);
    ASSERT_EQ(78, eventData.cardiacOutput->heartRate);
    ASSERT_EQ(98, eventData.cardiacOutput->signalStrength);
    ASSERT_EQ("valid_file.xml", eventData.cardiacOutput->waveformFile);

    _cmem_event.mCardiacOutput = nullptr;
}


TEST_F(EventDataTests, ConstructorSetsEventRhcDataValuesFromCmemEventRhcValues)
{
    cmem_rhc cmemRhc;
    cmemRhc.add_field("PAMean", "23");
    cmemRhc.add_field("PADiastolic", "33");
    cmemRhc.add_field("PASystolic", "24");
    cmemRhc.add_field("PCWP", "31");
    cmemRhc.add_field("RVDiastolic", "34");
    cmemRhc.add_field("RVSystolic", "25");
    cmemRhc.add_field("RA", "21");

    _cmem_event.mRHCValues = &cmemRhc;
    EventData eventData(_cmem_event);

    ASSERT_NE(nullptr, eventData.rhc);
    ASSERT_EQ(23, eventData.rhc->paMean);
    ASSERT_EQ(33, eventData.rhc->paDiastolic);
    ASSERT_EQ(24, eventData.rhc->paSystolic);
    ASSERT_EQ(31, eventData.rhc->pcwp);
    ASSERT_EQ(34, eventData.rhc->rvDiastoic);
    ASSERT_EQ(25, eventData.rhc->rvSystolic);
    ASSERT_EQ(21, eventData.rhc->ra);

    _cmem_event.mRHCValues = nullptr;

}

TEST_F(EventDataTests, ConstructorSetsEventRecordingListFromCmemEventRecordingList)
{
    cmem_recording _cmem_recording;
    std::string _recordingDate = "23 May 2019";
    std::string _recordingTime = "11:33:22";

    _cmem_recording.add_field("RecordingId", 1234);
    _cmem_recording.add_field("Notes", "notes");
    _cmem_recording.add_field("WaveformData", "valid_file.xml");
    _cmem_recording.add_field("UploadPending", "YES");
    _cmem_recording.add_field("Systolic", "33");
    _cmem_recording.add_field("Diastolic", "22");
    _cmem_recording.add_field("Mean", "25");
    _cmem_recording.add_field("ReferenceSystolic", "32");
    _cmem_recording.add_field("ReferenceDiastolic", "21");
    _cmem_recording.add_field("ReferenceMean", "24");
    _cmem_recording.add_field("AverageHeartRate", "78");
    _cmem_recording.add_field("AverageSignalStrength", "91");
    _cmem_recording.add_field("PatientPosition", "Horizontal");
    _cmem_recording.add_field("Date", _recordingDate.c_str());
    _cmem_recording.add_field("Time", _recordingTime.c_str());

    _cmem_event.mRecordingList.push_back(&_cmem_recording);

    cmem_recording _cmem_recording2;
    std::string _recordingDate2 = "22 May 2019";
    std::string _recordingTime2 = "09:43:12";

    _cmem_recording2.add_field("RecordingId", 4567);
    _cmem_recording2.add_field("Notes", "notes2");
    _cmem_recording2.add_field("WaveformData", "valid_file2.xml");
    _cmem_recording2.add_field("UploadPending", "NO");
    _cmem_recording2.add_field("Systolic", "34");
    _cmem_recording2.add_field("Diastolic", "23");
    _cmem_recording2.add_field("Mean", "26");
    _cmem_recording2.add_field("ReferenceSystolic", "33");
    _cmem_recording2.add_field("ReferenceDiastolic", "22");
    _cmem_recording2.add_field("ReferenceMean", "25");
    _cmem_recording2.add_field("AverageHeartRate", "79");
    _cmem_recording2.add_field("AverageSignalStrength", "92");
    _cmem_recording2.add_field("PatientPosition", "Horizontal");
    _cmem_recording2.add_field("Date", _recordingDate2.c_str());
    _cmem_recording2.add_field("Time", _recordingTime2.c_str());

    _cmem_event.mRecordingList.push_back(&_cmem_recording2);
    EventData eventData(_cmem_event);

    ASSERT_EQ(2, eventData.recordings.size());
    std::list<RecordingData>::iterator iter;
    iter = eventData.recordings.begin();
    RecordingData recordingData = *iter;

    ASSERT_EQ(_cmem_recording.getRecordingId(), recordingData.recordingId);
    ASSERT_EQ(_cmem_recording.uploadPending(), recordingData.uploadPending);
    ASSERT_EQ(_cmem_recording.getSystolic(), recordingData.systolic);
    ASSERT_EQ(_cmem_recording.getDiastolic(), recordingData.diastolic);
    ASSERT_EQ(_cmem_recording.getMean(), recordingData.mean);
    ASSERT_EQ(_cmem_recording.getRefSystolic(), recordingData.refSystolic);
    ASSERT_EQ(_cmem_recording.getRefDiastolic(), recordingData.refDiastolic);
    ASSERT_EQ(_cmem_recording.getRefMean(), recordingData.refMean);
    ASSERT_EQ(_cmem_recording.getAvgHeartRate(), recordingData.heartRate);
    ASSERT_EQ(_cmem_recording.getAvgSignalStrength(), recordingData.signalStrength);
    ASSERT_EQ("Horizontal", recordingData.patientPosition);
    ASSERT_EQ(_recordingDate, recordingData.recordingDate);
    ASSERT_EQ(_recordingTime, recordingData.recordingTime);

    std::advance(iter, 1);

    RecordingData recordingData2 = *iter;
    ASSERT_EQ(_cmem_recording2.getRecordingId(), recordingData2.recordingId);
    ASSERT_EQ(_cmem_recording2.uploadPending(), recordingData2.uploadPending);
    ASSERT_EQ(_cmem_recording2.getSystolic(), recordingData2.systolic);
    ASSERT_EQ(_cmem_recording2.getDiastolic(), recordingData2.diastolic);
    ASSERT_EQ(_cmem_recording2.getMean(), recordingData2.mean);
    ASSERT_EQ(_cmem_recording2.getRefSystolic(), recordingData2.refSystolic);
    ASSERT_EQ(_cmem_recording2.getRefDiastolic(), recordingData2.refDiastolic);
    ASSERT_EQ(_cmem_recording2.getRefMean(), recordingData2.refMean);
    ASSERT_EQ(_cmem_recording2.getAvgHeartRate(), recordingData2.heartRate);
    ASSERT_EQ(_cmem_recording2.getAvgSignalStrength(), recordingData2.signalStrength);
    ASSERT_EQ("Horizontal", recordingData2.patientPosition);
    ASSERT_EQ(_recordingDate2, recordingData2.recordingDate);
    ASSERT_EQ(_recordingTime2, recordingData2.recordingTime);

    _cmem_event.mRecordingList.clear();
}
