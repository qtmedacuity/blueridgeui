#include <gmock/gmock.h>
#include <gtest/gtest.h>

//#include "cmem_patient.h"
#include "providers/PatientData.h"

using namespace ::testing;

class PatientDataTests : public Test
{

public:
    PatientDataTests() {}
    ~PatientDataTests() override;

    void SetUp() override
    {
        cmemPatient.setPatientId(733141);
        cmemPatient.setName("FName", "MInitial", "LName");
        cmemPatient.setDOB("01 Jan 1970");
        cmemPatient.setImplantDate("01 Jan 2018");
        cmemPatient.setPhoneNumber("555-555-5555");
        cmemPatient.setClinic("Clinic Name");
        cmemPatient.setClinician("Clinician Name");
        cmemPatient.setImplantDoctor("Implant Doctor");
        cmemPatient.setSerialNumber("ABC123");
        cmemPatient.setImplantPosition(eImplantPositionType::eRightImplant);
        cmemPatient.setUploadPending(true);
    }

    cmem_patient cmemPatient;
};

PatientDataTests::~PatientDataTests() {}


TEST_F(PatientDataTests, DefaultConstructorDefaultsPatientIdToZero)
{
    PatientData ptData;

    ASSERT_EQ(0, ptData.patientId);
}

TEST_F(PatientDataTests, DefaultConstructorDefaultsUploadPendingToFalse)
{
    PatientData ptData;

    ASSERT_FALSE(ptData.uploadPending);
}


TEST_F(PatientDataTests, ConstructorSetsPatientDataValuesFromCmemPatientValues)
{
    PatientData ptData(cmemPatient);

    std::string str;

    ASSERT_EQ(ptData.patientId, cmemPatient.getPatientId());
    ASSERT_EQ(ptData.patientKey, std::string(cmemPatient.getPatientKey(str)));
    ASSERT_EQ(ptData.dob, std::string(cmemPatient.getDOB(str)));
    ASSERT_EQ(ptData.implantDate, std::string(cmemPatient.getImplantDate(str)));
    ASSERT_EQ(ptData.phone, std::string(cmemPatient.getPhoneNumber(str)));
    ASSERT_EQ(ptData.clinic, std::string(cmemPatient.getClinic(str)));
    ASSERT_EQ(ptData.clinician, std::string(cmemPatient.getClinician(str)));
    ASSERT_EQ(ptData.implantDoctor, std::string(cmemPatient.getImplantDoctor(str)));
    ASSERT_EQ(ptData.serialNumber, std::string(cmemPatient.getSerialNumber(str)));
    ASSERT_EQ(ptData.uploadPending, cmemPatient.getUploadPending());
    ASSERT_EQ(ptData.implantLocation, "right");
    ASSERT_EQ(ptData.lastName, std::string(cmemPatient.getLastName(str)));
    ASSERT_EQ(ptData.firstName, std::string(cmemPatient.getFirstName(str)));
    ASSERT_EQ(ptData.middleInitial, std::string(cmemPatient.getMiddleInitial(str)));
    ASSERT_EQ(ptData.patientName, std::string(cmemPatient.getPatientName(str)));
}

TEST_F(PatientDataTests, ConstructorSetsPatientDataValuesFromADifferentCmemPatient)
{
    cmem_patient _cmemPatient;
    _cmemPatient.setPatientId(113456);
    _cmemPatient.setName("FName", "MInitial", "LName");
    _cmemPatient.setDOB("01 Feb 1980");
    _cmemPatient.setImplantDate("20 Jan 2019");
    _cmemPatient.setPhoneNumber("555-555-1234");
    _cmemPatient.setClinic("A Clinic Name");
    _cmemPatient.setClinician("A Clinician Name");
    _cmemPatient.setImplantDoctor("The Implant Doctor");
    _cmemPatient.setSerialNumber("123ABC");
    _cmemPatient.setImplantPosition(eImplantPositionType::eLeftImplant);
    _cmemPatient.setUploadPending(false);

    PatientData ptData(_cmemPatient);

    std::string str;

    ASSERT_EQ(ptData.patientId, _cmemPatient.getPatientId());
    ASSERT_EQ(ptData.patientKey, std::string(_cmemPatient.getPatientKey(str)));
    ASSERT_EQ(ptData.dob, std::string(_cmemPatient.getDOB(str)));
    ASSERT_EQ(ptData.implantDate, std::string(_cmemPatient.getImplantDate(str)));
    ASSERT_EQ(ptData.phone, std::string(_cmemPatient.getPhoneNumber(str)));
    ASSERT_EQ(ptData.clinic, std::string(_cmemPatient.getClinic(str)));
    ASSERT_EQ(ptData.clinician, std::string(_cmemPatient.getClinician(str)));
    ASSERT_EQ(ptData.implantDoctor, std::string(_cmemPatient.getImplantDoctor(str)));
    ASSERT_EQ(ptData.serialNumber, std::string(_cmemPatient.getSerialNumber(str)));
    ASSERT_EQ(ptData.uploadPending, _cmemPatient.getUploadPending());
    ASSERT_EQ(ptData.implantLocation, "left");
    ASSERT_EQ(ptData.lastName, std::string(_cmemPatient.getLastName(str)));
    ASSERT_EQ(ptData.firstName, std::string(_cmemPatient.getFirstName(str)));
    ASSERT_EQ(ptData.middleInitial, std::string(_cmemPatient.getMiddleInitial(str)));
    ASSERT_EQ(ptData.patientName, std::string(_cmemPatient.getPatientName(str)));
}


TEST_F(PatientDataTests, ToCmemPatientReturnsACmemPatientForAPatientData)
{
    PatientData ptData(cmemPatient);

    cmem_patient patient = ptData.toCMemPatient();
    std::string str;

    ASSERT_EQ(ptData.patientId, patient.getPatientId());
    ASSERT_EQ(ptData.patientKey, std::string(patient.getPatientKey(str)));
    ASSERT_EQ(ptData.dob, std::string(patient.getDOB(str)));
    ASSERT_EQ(ptData.implantDate, std::string(patient.getImplantDate(str)));
    ASSERT_EQ(ptData.phone, std::string(patient.getPhoneNumber(str)));
    ASSERT_EQ(ptData.clinic, std::string(patient.getClinic(str)));
    ASSERT_EQ(ptData.clinician, std::string(patient.getClinician(str)));
    ASSERT_EQ(ptData.implantDoctor, std::string(patient.getImplantDoctor(str)));
    ASSERT_EQ(ptData.serialNumber, std::string(patient.getSerialNumber(str)));
    ASSERT_EQ(ptData.uploadPending, patient.getUploadPending());
    ASSERT_EQ(ptData.implantLocation, "right");
    ASSERT_EQ(ptData.lastName, std::string(patient.getLastName(str)));
    ASSERT_EQ(ptData.firstName, std::string(patient.getFirstName(str)));
    ASSERT_EQ(ptData.middleInitial, std::string(patient.getMiddleInitial(str)));
    ASSERT_EQ(ptData.patientName, std::string(patient.getPatientName(str)));
}

TEST_F(PatientDataTests, ToCmemPatientReturnsACmemPatientForADifferentPatientData)
{
    cmem_patient _cmemPatient;
    _cmemPatient.setPatientId(113456);
    _cmemPatient.setName("FName", "MInitial", "LName");
    _cmemPatient.setDOB("01 Feb 1980");
    _cmemPatient.setImplantDate("20 Jan 2019");
    _cmemPatient.setPhoneNumber("555-555-1234");
    _cmemPatient.setClinic("A Clinic Name");
    _cmemPatient.setClinician("A Clinician Name");
    _cmemPatient.setImplantDoctor("The Implant Doctor");
    _cmemPatient.setSerialNumber("123ABC");
    _cmemPatient.setImplantPosition(eImplantPositionType::eLeftImplant);
    _cmemPatient.setUploadPending(false);

    PatientData ptData(_cmemPatient);
    cmem_patient patient = ptData.toCMemPatient();
    std::string str;

    ASSERT_EQ(ptData.patientId, patient.getPatientId());
    ASSERT_EQ(ptData.patientKey, std::string(patient.getPatientKey(str)));
    ASSERT_EQ(ptData.dob, std::string(patient.getDOB(str)));
    ASSERT_EQ(ptData.implantDate, std::string(patient.getImplantDate(str)));
    ASSERT_EQ(ptData.phone, std::string(patient.getPhoneNumber(str)));
    ASSERT_EQ(ptData.clinic, std::string(patient.getClinic(str)));
    ASSERT_EQ(ptData.clinician, std::string(patient.getClinician(str)));
    ASSERT_EQ(ptData.implantDoctor, std::string(patient.getImplantDoctor(str)));
    ASSERT_EQ(ptData.serialNumber, std::string(patient.getSerialNumber(str)));
    ASSERT_EQ(ptData.uploadPending, patient.getUploadPending());
    ASSERT_EQ(ptData.implantLocation, "left");
    ASSERT_EQ(ptData.lastName, std::string(patient.getLastName(str)));
    ASSERT_EQ(ptData.firstName, std::string(patient.getFirstName(str)));
    ASSERT_EQ(ptData.middleInitial, std::string(patient.getMiddleInitial(str)));
    ASSERT_EQ(ptData.patientName, std::string(patient.getPatientName(str)));
}
