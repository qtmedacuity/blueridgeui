#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>
#include <QLineSeries>

#include <iostream>

#include "presentation/StaticChartController.h"

using namespace QtCharts;
using namespace ::testing;

class StaticChartControllerTests : public Test
{
public:
    StaticChartControllerTests()
        : _resource("testAxisConfig.json"),
          _sourceFile("ValidWaveform.xml"),
          _differentFile("OtherWaveform.xml"),
          _controller(_resource, _sourceFile)
    {}
    ~StaticChartControllerTests() override;

    void SetUp() override
    {
        _series.setName(QString("test_series"));
    }

    QString _resource;
    QString _sourceFile;
    QString _differentFile;
    QLineSeries _series;
    StaticChartController _controller;
};


StaticChartControllerTests::~StaticChartControllerTests()
{}

TEST_F(StaticChartControllerTests, ConstructorSetsDefaultValues)
{
    StaticChartController controller(_resource, _sourceFile);

    ASSERT_EQ(controller.verticalOffset(), 0.0);
    ASSERT_EQ(controller.horizontalOffset(), 0.0);
    ASSERT_EQ(controller.zoomInVerticalEnabled(), true);
    ASSERT_EQ(controller.zoomOutVerticalEnabled(), false);
    ASSERT_EQ(controller.zoomInHorizontalEnabled(), false);
    ASSERT_EQ(controller.zoomOutHorizontalEnabled(), false);
    ASSERT_EQ(controller.samplesPerSecond(), 125);
    ASSERT_EQ(controller.rangeInSeconds(), 10.25);
    ASSERT_EQ(controller.source(), _sourceFile);
}

TEST_F(StaticChartControllerTests, RangeInSecondsReturnsTheValueOfRangeInSeconds)
{
    ASSERT_EQ(_controller.rangeInSeconds(), 10.25);
}

TEST_F(StaticChartControllerTests, SetRangeInSecondsSetsTheValueOfRangeInSeconds)
{
    _controller.setRangeInSeconds(20.0);
    ASSERT_EQ(_controller.rangeInSeconds(), 20.0);
}

TEST_F(StaticChartControllerTests, SetRangeInSecondsEmitsRangeInSecondsChangedSignal)
{
    QSignalSpy spy(&_controller, SIGNAL(rangeInSecondsChanged(double)));

    _controller.setRangeInSeconds(20.0);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(StaticChartControllerTests, RangeInSecondsEnabledPropertyReturnsTheRangeInSecondsValue)
{
    double value = _controller.property("rangeInSeconds").toDouble();
    ASSERT_EQ(value, 10.25);
}

TEST_F(StaticChartControllerTests, RangeInSecondsEnabledPropertySetsTheRangeInSecondsValue)
{
    _controller.setProperty("rangeInSeconds", 15.0);
    ASSERT_EQ(_controller.rangeInSeconds(), 15.0);
}

TEST_F(StaticChartControllerTests, RangeInSecondsPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("rangeInSeconds");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("rangeInSecondsChanged", property.notifySignal().name().data());
}

TEST_F(StaticChartControllerTests, SamplesPerSecondReturnsTheValueOfSamplesPerSecond)
{
    ASSERT_EQ(_controller.samplesPerSecond(), 125);
}

TEST_F(StaticChartControllerTests, SetSamplesPerSecondSetsTheValueOfSamplesPerSecond)
{
    _controller.setSamplesPerSecond(60);
    ASSERT_EQ(_controller.samplesPerSecond(), 60);
}

TEST_F(StaticChartControllerTests, SetSamplesPerSecondEmitsSamplesPerSecondChangedSignal)
{
    QSignalSpy spy(&_controller, SIGNAL(samplesPerSecondChanged(unsigned long)));

    _controller.setSamplesPerSecond(30);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(StaticChartControllerTests, SamplesPerSecondEnabledPropertyReturnsTheSamplesPerSecondValue)
{
    double value = _controller.property("samplesPerSecond").toUInt();
    ASSERT_EQ(value, 125);
}

TEST_F(StaticChartControllerTests, SamplesPerSecondEnabledPropertySetsTheSamplesPerSecondValue)
{
    _controller.setProperty("samplesPerSecond", 15);
    ASSERT_EQ(_controller.samplesPerSecond(), 15);
}

TEST_F(StaticChartControllerTests, SamplesPerSecondPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("samplesPerSecond");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("samplesPerSecondChanged", property.notifySignal().name().data());
}

TEST_F(StaticChartControllerTests, SourceReturnsTheValueOfSource)
{
    ASSERT_EQ(_controller.source(), _sourceFile);
}

TEST_F(StaticChartControllerTests, SetSourceSetsTheValueOfSource)
{
    _controller.setSource(_differentFile);
    ASSERT_EQ(_controller.source(), _differentFile);
}

TEST_F(StaticChartControllerTests, SetSourceEmitsSourceChangedSignal)
{
    QSignalSpy spy(&_controller, SIGNAL(sourceChanged(QString)));

    _controller.setSource(_differentFile);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(StaticChartControllerTests, SourceEnabledPropertyReturnsTheSourceValue)
{
    QString value = _controller.property("source").toString();
    ASSERT_EQ(value, _sourceFile);
}

TEST_F(StaticChartControllerTests, SourceEnabledPropertySetsTheSourceValue)
{
    _controller.setProperty("source", _differentFile);
    ASSERT_EQ(_controller.source(), _differentFile);
}

TEST_F(StaticChartControllerTests, SourcePropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("source");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("sourceChanged", property.notifySignal().name().data());
}

TEST_F(StaticChartControllerTests, SeriesHasCorrectData)
{
    _controller.registerSeries(&_series);

    ASSERT_EQ(4, _series.count());
    ASSERT_DOUBLE_EQ(0.0, _series.at(0).x());
    ASSERT_DOUBLE_EQ(19.8, _series.at(0).y());
    ASSERT_DOUBLE_EQ(0.008, _series.at(1).x());
    ASSERT_DOUBLE_EQ(19.7, _series.at(1).y());
    ASSERT_DOUBLE_EQ(0.016, _series.at(2).x());
    ASSERT_DOUBLE_EQ(19.5, _series.at(2).y());
    ASSERT_DOUBLE_EQ(0.024, _series.at(3).x());
    ASSERT_DOUBLE_EQ(19.2, _series.at(3).y());
}

TEST_F(StaticChartControllerTests, SeriesHasDifferentData)
{
    _controller.registerSeries(&_series);
    _controller.setSource(_differentFile);

    ASSERT_EQ(10, _series.count());
    ASSERT_DOUBLE_EQ(0.0, _series.at(0).x());
    ASSERT_DOUBLE_EQ(0.0, _series.at(0).y());
    ASSERT_DOUBLE_EQ(0.008, _series.at(1).x());
    ASSERT_DOUBLE_EQ(1.0, _series.at(1).y());
    ASSERT_DOUBLE_EQ(0.016, _series.at(2).x());
    ASSERT_DOUBLE_EQ(2.0, _series.at(2).y());
    ASSERT_DOUBLE_EQ(0.024, _series.at(3).x());
    ASSERT_DOUBLE_EQ(3.0, _series.at(3).y());
    ASSERT_DOUBLE_EQ(0.032, _series.at(4).x());
    ASSERT_DOUBLE_EQ(4.0, _series.at(4).y());
    ASSERT_DOUBLE_EQ(0.040, _series.at(5).x());
    ASSERT_DOUBLE_EQ(5.0, _series.at(5).y());
    ASSERT_DOUBLE_EQ(0.048, _series.at(6).x());
    ASSERT_DOUBLE_EQ(6.0, _series.at(6).y());
    ASSERT_DOUBLE_EQ(0.056, _series.at(7).x());
    ASSERT_DOUBLE_EQ(7.0, _series.at(7).y());
    ASSERT_DOUBLE_EQ(0.064, _series.at(8).x());
    ASSERT_DOUBLE_EQ(8.0, _series.at(8).y());
    ASSERT_DOUBLE_EQ(0.072, _series.at(9).x());
    ASSERT_DOUBLE_EQ(9.0, _series.at(9).y());
}
