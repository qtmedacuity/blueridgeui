#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "unitTests/mocks/MockTimer.h"
#include "unitTests/mocks/MockDataSocket.h"
#include "providers/SensorDataParser.h"
#include "providers/DataSocketError.h"

#include <cardiomems_types.h>

#include <condition_variable>
#include <chrono>
#include <cstring>
#include <limits>
#include <mutex>
#include <thread>

using namespace ::testing;

class SensorDataParserTests : public Test
{
public:
    SensorDataParserTests()
        : _data({19.8f, 42, 92, 78, 38, 19, 26, true}),
          _parser(_timer, _socket, std::chrono::milliseconds(500))
    {}

    void SetUp() override
    {
        ON_CALL(_socket, readbytes(_,_,_,_))
                .WillByDefault(Invoke([&](void* buffer, size_t count, int&, std::string&) -> ssize_t
                                {
                                    if (nullptr != _timer._fake._func) { _timer._fake._func(); }
                                    std::this_thread::sleep_for(std::chrono::milliseconds(33));
                                    std::memcpy(buffer, &_data, count);
                                    return static_cast<ssize_t>(count);
                                }));
    }

    ~SensorDataParserTests() override;

    DataReading_t _data;
    std::condition_variable _condition;
    NiceMock<MockTimer> _timer;
    NiceMock<MockDataSocket> _socket;
    SensorDataParser _parser;
};

SensorDataParserTests::~SensorDataParserTests() {}

TEST_F(SensorDataParserTests, RegisterDataCallbackReturnsInvalidFunctionDescriptorForNullCallback)
{
    SensorDataParser::DataCallback cb = nullptr;
    unsigned long funcId = _parser.registerDataCallback(cb);

    EXPECT_EQ(std::numeric_limits<unsigned long>::max(), funcId);
}

TEST_F(SensorDataParserTests, RegisterDataCallbackReturnsFuncIdForRegisteredDataCallback)
{
    SensorDataParser::DataCallback cb =[&](ISensorDataParser::DataListPtr) {};
    unsigned long funcId = _parser.registerDataCallback(cb);

    EXPECT_EQ(0, funcId);
}

TEST_F(SensorDataParserTests, RegisterDataCallbackReturnsFuncIdsForEachRegisteredCallback)
{
    SensorDataParser::DataCallback cb1 =[&](ISensorDataParser::DataListPtr) {};
    unsigned long funcId1 = _parser.registerDataCallback(cb1);

    SensorDataParser::DataCallback cb2 =[&](ISensorDataParser::DataListPtr) {};
    unsigned long funcId2 = _parser.registerDataCallback(cb2);

    EXPECT_EQ(0, funcId1);
    EXPECT_EQ(1, funcId2);
}

TEST_F(SensorDataParserTests, RegisteredDataCallbackIsExecutedByParser)
{
    _timer.DelegateToFake();
    unsigned int count = 0;
    SensorDataParser::DataCallback cb =[&](ISensorDataParser::DataListPtr) {count++; _condition.notify_all();};
    _parser.registerDataCallback(cb);

    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(1, count);
}

TEST_F(SensorDataParserTests, RegisteredDataCallbacksAreExecutedByParser)
{
    _timer.DelegateToFake();
    unsigned int count1 = 0;
    SensorDataParser::DataCallback cb1 =[&](ISensorDataParser::DataListPtr) {count1++; };
    _parser.registerDataCallback(cb1);

    unsigned int count2 = 1;
    SensorDataParser::DataCallback cb2 =[&](ISensorDataParser::DataListPtr) {count2++; _condition.notify_all();};
    _parser.registerDataCallback(cb2);

    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(SensorDataParserTests, UnregisteredDataCallbacksAreNotExecutedByParser)
{
    _timer.DelegateToFake();
    unsigned int count1 = 0;
    SensorDataParser::DataCallback cb1 =[&](ISensorDataParser::DataListPtr) {count1++; };
    unsigned long funcId1 = _parser.registerDataCallback(cb1);

    unsigned int count2 = 1;
    SensorDataParser::DataCallback cb2 =[&](ISensorDataParser::DataListPtr) {count2++; _condition.notify_all();};
    unsigned long funcId2 = _parser.registerDataCallback(cb2);

    ASSERT_EQ(0, funcId1);
    ASSERT_EQ(1, funcId2);

    _parser.unregisterDataCallback(funcId1);
    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(SensorDataParserTests, RegisterDataRateCallbackReturnsInvalidFunctionDescriptorForNullCallback)
{
    SensorDataParser::DataRateCallback cb = nullptr;
    unsigned long funcId = _parser.registerDataRateCallback(cb);

    EXPECT_EQ(std::numeric_limits<unsigned long>::max(), funcId);
}


TEST_F(SensorDataParserTests, RegisterDataRateCallbackReturnsFuncIdForRegisteredCallback)
{
    SensorDataParser::DataRateCallback cb =[&](unsigned int) {};
    unsigned long funcId = _parser.registerDataRateCallback(cb);

    EXPECT_EQ(0, funcId);
}

TEST_F(SensorDataParserTests, RegisterDataRateCallbackReturnsFuncIdsForEachRegisterdCallback)
{
    SensorDataParser::DataRateCallback cb1 =[&](unsigned int) {};
    unsigned long funcId1 = _parser.registerDataRateCallback(cb1);

    SensorDataParser::DataRateCallback cb2 =[&](unsigned int) {};
    unsigned long funcId2 = _parser.registerDataRateCallback(cb2);

    EXPECT_EQ(0, funcId1);
    EXPECT_EQ(1, funcId2);
}

TEST_F(SensorDataParserTests, RegisteredDataRateCallbackIsExecutedByParser)
{
    unsigned int count = 0;
    SensorDataParser::DataRateCallback cb =[&](unsigned int) {count++; _condition.notify_all();};
    _parser.registerDataRateCallback(cb);

    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(1, count);
}

TEST_F(SensorDataParserTests, RegisteredDataRateCallbacksAreExecutedByParser)
{
    unsigned int count1 = 0;
    SensorDataParser::DataRateCallback cb1 =[&](unsigned int) {count1++; };
    _parser.registerDataRateCallback(cb1);

    unsigned int count2 = 1;
    SensorDataParser::DataRateCallback cb2 =[&](unsigned int) {count2++; _condition.notify_all();};
    _parser.registerDataRateCallback(cb2);

    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(SensorDataParserTests, UnregisteredDataRateCallbacksAreNotExecutedByParser)
{
    _timer.DelegateToFake();
    unsigned int count1 = 0;
    SensorDataParser::DataRateCallback cb1 =[&](unsigned int) {count1++; };
    unsigned long funcId1 = _parser.registerDataRateCallback(cb1);

    unsigned int count2 = 1;
    SensorDataParser::DataRateCallback cb2 =[&](unsigned int) {count2++; _condition.notify_all();};
    unsigned long funcId2 = _parser.registerDataRateCallback(cb2);

    ASSERT_EQ(0, funcId1);
    ASSERT_EQ(1, funcId2);

    _parser.unregisterDataRateCallback(funcId1);
    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(SensorDataParserTests, StartCallsTimerStartWithDataRateArgument)
{
    unsigned int dataRate = 125;
    unsigned int flushRate = 10;

    EXPECT_CALL(_timer, start(flushRate, _))
            .Times(1);

    _parser.start(dataRate, flushRate);
    _parser.reading(true);
    _parser.streaming(true);
}

TEST_F(SensorDataParserTests, StartBeginsReadingFromSocket)
{
    unsigned int dataRate = 125;
    unsigned int flushRate = 10;

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(AtLeast(1));

    _parser.start(dataRate, flushRate);
    _parser.reading(true);
    _parser.streaming(true);

    std::this_thread::sleep_for(std::chrono::seconds(1));
}


TEST_F(SensorDataParserTests, StartDoesNotReadFromSocketIfReadingDisabled)
{
    unsigned int dataRate = 125;
    unsigned int flushRate = 10;

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(AtLeast(1));

    _parser.start(dataRate, flushRate);
    _parser.reading(false);
    _parser.streaming(true);

    std::this_thread::sleep_for(std::chrono::seconds(1));
}


TEST_F(SensorDataParserTests, StopCallsTimerStopOnce)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockDataSocket> socket;
    SensorDataParser parser(timer, socket);

    EXPECT_CALL(timer, stop())
            .Times(AtLeast(1));

    parser.stop();
}


TEST_F(SensorDataParserTests, RegisterErrorCallbackReturnsInvalidFunctionDescriptorForNullCallback)
{
    DataSocketErrorCallback cb = nullptr;
    unsigned long funcId = _parser.registerErrorCallback(cb);

    EXPECT_EQ(std::numeric_limits<unsigned long>::max(), funcId);
}

TEST_F(SensorDataParserTests, RegisterErrorCallbackReturnsFuncIdForCallbackRegistered)
{
    DataSocketErrorCallback cb =[&](DataSocketError) {};
    unsigned long funcId = _parser.registerErrorCallback(cb);

    EXPECT_EQ(0, funcId);
}

TEST_F(SensorDataParserTests, RegisterErrorCallbackReturnsFuncIdsForEachCallbackRegistered)
{
    DataSocketErrorCallback cb1 =[&](DataSocketError) {};
    unsigned long funcId1 = _parser.registerErrorCallback(cb1);

    DataSocketErrorCallback cb2 =[&](DataSocketError) {};
    unsigned long funcId2 = _parser.registerErrorCallback(cb2);

    EXPECT_EQ(0, funcId1);
    EXPECT_EQ(1, funcId2);
}

TEST_F(SensorDataParserTests, RegisterErrorCallbackAreExecutedByParser)
{
    ON_CALL(_socket, readbytes(_,_,_,_))
            .WillByDefault(Invoke([&](void*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                std::this_thread::sleep_for(std::chrono::milliseconds(8));
                                err = EPERM;
                                errstr = "Testing permissions failed";
                                return 0;
                            }));

    unsigned int count1 = 0;
    _parser.registerErrorCallback([&](DataSocketError) { count1++; });

    unsigned int count2 = 1;
    _parser.registerErrorCallback([&](DataSocketError) { count2++; _condition.notify_all(); });

    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}


TEST_F(SensorDataParserTests, UnregisteredErrorCallbacksAreNotExecutedByParser)
{
    ON_CALL(_socket, readbytes(_,_,_,_))
            .WillByDefault(Invoke([&](void*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                std::this_thread::sleep_for(std::chrono::milliseconds(8));
                                err = EPERM;
                                errstr = "Testing permissions failed";
                                return 0;
                            }));

    unsigned int count1 = 0;
    unsigned long funcId1 = _parser.registerErrorCallback([&](DataSocketError) { count1++; _condition.notify_all(); });

    unsigned int count2 = 1;
    unsigned long funcId2 = _parser.registerErrorCallback([&](DataSocketError) { count2++; _condition.notify_all(); });

    ASSERT_EQ(0, funcId1);
    ASSERT_EQ(1, funcId2);

    _parser.unregisterErrorCallback(funcId1);
    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}


TEST_F(SensorDataParserTests, RegisteredErrorCallbackReceivesReadFailedErrorIfReadBytesReturnsZeroBytesRead)
{
    unsigned int dataCount(0);
    unsigned int readErrorCount(0);
    unsigned int connectErrorCount(0);
    unsigned int otherErrorCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, readbytes(_,_,_,_))
            .WillByDefault(Invoke([&](void*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                                err = EAGAIN;
                                errstr = "Testing zero bytes read";
                                return 0;
                            }));

    _parser.registerDataCallback([&](ISensorDataParser::DataListPtr) {
                                    dataCount++;
                                    _condition.notify_one();
                                });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorValue = err;
        switch (errorValue) {
        case DataSocketError::ReadFailed:
            readErrorCount++;
            break;
        case DataSocketError::ConnectionFailed:
            connectErrorCount++;
            break;
        default:
            otherErrorCount++;
            break;
        }
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(AtLeast(5));

    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(1, readErrorCount);
    ASSERT_EQ(0, connectErrorCount);
    ASSERT_EQ(0, otherErrorCount);
    ASSERT_EQ(DataSocketError::ReadFailed, errorValue);
}

TEST_F(SensorDataParserTests, RegisteredErrorCallbackDoesNotReceiveReadFailedErrorIfReadBytesReturnsZeroBytesReadAndReadingIsDisabled)
{
    unsigned int dataCount(0);
    unsigned int readErrorCount(0);
    unsigned int connectErrorCount(0);
    unsigned int otherErrorCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, readbytes(_,_,_,_))
            .WillByDefault(Invoke([&](void*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                                err = EAGAIN;
                                errstr = "Testing zero bytes read";
                                return 0;
                            }));

    _parser.registerDataCallback([&](ISensorDataParser::DataListPtr) {
                                    dataCount++;
                                    _condition.notify_one();
                                });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorValue = err;
        switch (errorValue) {
        case DataSocketError::ReadFailed:
            readErrorCount++;
            break;
        case DataSocketError::ConnectionFailed:
            connectErrorCount++;
            break;
        default:
            otherErrorCount++;
            break;
        }
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(AtLeast(5));

    _parser.start(2, 1);
    _parser.reading(false);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(0, readErrorCount);
    ASSERT_EQ(0, connectErrorCount);
    ASSERT_EQ(0, otherErrorCount);
    ASSERT_EQ(DataSocketError::NoError, errorValue);
}

TEST_F(SensorDataParserTests, RegisteredErrorCallbackReceivesConnectionFailedErrorIfReadBytesEncountersASocketDisconnectionError)
{
    unsigned int dataCount(0);
    unsigned int readErrorCount(0);
    unsigned int connectErrorCount(0);
    unsigned int otherErrorCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, readbytes(_,_,_,_))
            .WillByDefault(Invoke([&](void*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                std::this_thread::sleep_for(std::chrono::milliseconds(8));
                                err = ENOTCONN;
                                errstr = "Testing connection failed";
                                return 0;
                            }));

    _parser.registerDataCallback([&](ISensorDataParser::DataListPtr) {
                                    dataCount++;
                                    _condition.notify_one();
                                });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorValue = err;
        switch (errorValue) {
        case DataSocketError::ReadFailed:
            readErrorCount++;
            break;
        case DataSocketError::ConnectionFailed:
            connectErrorCount++;
            break;
        default:
            otherErrorCount++;
            break;
        }
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(AtLeast(1));

    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1000));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(0, readErrorCount);
    ASSERT_EQ(1, connectErrorCount);
    ASSERT_EQ(0, otherErrorCount);
    ASSERT_EQ(DataSocketError::ConnectionFailed, errorValue);
}

TEST_F(SensorDataParserTests, RegisteredErrorCallbackReceivesUnknownErrorIfReadBytesEncountersAnUnknownSocketFailure)
{
    unsigned int dataCount(0);
    unsigned int readErrorCount(0);
    unsigned int connectErrorCount(0);
    unsigned int otherErrorCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, readbytes(_,_,_,_))
            .WillByDefault(Invoke([&](void*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                std::this_thread::sleep_for(std::chrono::milliseconds(8));
                                err = EPERM;
                                errstr = "Testing permissions failed";
                                return 0;
                            }));

    _parser.registerDataCallback([&](ISensorDataParser::DataListPtr) {
                                    dataCount++;
                                    _condition.notify_one();
                                });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorValue = err;
        switch (errorValue) {
        case DataSocketError::ReadFailed:
            readErrorCount++;
            break;
        case DataSocketError::ConnectionFailed:
            connectErrorCount++;
            break;
        default:
            otherErrorCount++;
            break;
        }
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(AtLeast(1));

    _parser.start(2, 1);
    _parser.reading(true);
    _parser.streaming(true);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(10000));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(0, readErrorCount);
    ASSERT_EQ(0, connectErrorCount);
    ASSERT_EQ(1, otherErrorCount);
    ASSERT_EQ(DataSocketError::Unknown, errorValue);
}

