#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include <cstring>
#include <vector>

#include "unitTests/mocks/MockTransactionProvider.h"
#include "unitTests/mocks/MockRecordingProvider.h"
#include "presentation/RecordingController.h"
#include "providers/RecordingProvider.h"
#include "presentation/Recording.h"
#include "providers/RecordingData.h"
#include "presentation/CardiacOutput.h"
#include "providers/CardiacOutputData.h"
#include "providers/RhcData.h"
#include "presentation/RhcValues.h"

using namespace ::testing;

class RecordingControllerTests : public Test
{
public:
    RecordingControllerTests()
          : _controller(_transactionProvider, _recordingProvider)
    {

    }

    ~RecordingControllerTests() override;

    void SetUp() override
    {
        ON_CALL(_transactionProvider, getTransactionId())
                .WillByDefault(Return(456123789));       
    }

    NiceMock<MockTransactionProvider> _transactionProvider;
    NiceMock<MockRecordingProvider> _recordingProvider;
    RecordingController _controller;
};

RecordingControllerTests::~RecordingControllerTests() {}

TEST_F(RecordingControllerTests, ConstructorRegisterCallbacksWithProviders)
{
    NiceMock<MockTransactionProvider> transactionProvider;
    NiceMock<MockRecordingProvider> recordingProvider;

    EXPECT_CALL(recordingProvider, registerReadyToRecordCallback(_))
            .Times(1);
    EXPECT_CALL(recordingProvider, registerRecordingStatusCallback(_))
            .Times(1);
    EXPECT_CALL(recordingProvider, registerTakeReadingCallback(_))
            .Times(1);
    EXPECT_CALL(recordingProvider, registerSetPaMeanCallback(_))
            .Times(1);
    EXPECT_CALL(recordingProvider, registerSaveRecordingCallback(_))
            .Times(1);
    EXPECT_CALL(recordingProvider, registerDeleteRecordingCallback(_))
            .Times(1);

    RecordingController controller(transactionProvider, recordingProvider);
}

TEST_F(RecordingControllerTests, GetRecordingStatusCallsProviderMethodWithCommandSupplied)
{
    QString command = "Start";

    EXPECT_CALL(_recordingProvider, requestRecordingStatus(command.toStdString()))
            .Times(1);

    _controller.getRecordingStatus(command);
}

TEST_F(RecordingControllerTests, TakeReadingDoesNotCallProviderMethodWhenProvidersTransactionIdIsZero)
{
    ON_CALL(_transactionProvider, getTransactionId())
            .WillByDefault(Return(0));

    EXPECT_CALL(_recordingProvider, takeReading(_))
            .Times(0);

    _controller.takeReading();
}


TEST_F(RecordingControllerTests, TakeReadingCallsProviderMethodWithPovidersTransactionId)
{
    int transactionId = 12345;
    ON_CALL(_transactionProvider, getTransactionId())
            .WillByDefault(Return(transactionId));

    EXPECT_CALL(_recordingProvider, takeReading(transactionId))
            .Times(1);

    _controller.takeReading();
}

TEST_F(RecordingControllerTests, SetPaMeanCallsProviderMethodWhenMeanWithinRange)
{
    int paMean = 45;
    int transactionId = 12345;

    ON_CALL(_transactionProvider, getTransactionId())
            .WillByDefault(Return(transactionId));

    _controller.setPaMean(paMean);

    EXPECT_CALL(_recordingProvider, setPaMean(transactionId, paMean))
            .Times(1);

    _controller.savePaMean();
}

TEST_F(RecordingControllerTests, SetPaMeanDoesNotCallProviderMethodWhenMeanIsBelowRange)
{
    int paMean = -1;
    _controller.setPaMean(paMean);

    EXPECT_CALL(_recordingProvider, setPaMean(0, paMean))
            .Times(0);

    _controller.savePaMean();
}

TEST_F(RecordingControllerTests, SetPaMeanDoesNotCallProviderMethodWhenMeanIsAboveRange)
{
    int paMean = 155;
    _controller.setPaMean(paMean);
    EXPECT_CALL(_recordingProvider, setPaMean(0, paMean))
            .Times(0);

    _controller.savePaMean();
}

TEST_F(RecordingControllerTests, ReadyToRecordSignalEmittedWhenCallbackInvokedIfProviderReadyToRecordIsTrue)
{

    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    bool readyToRecord = true;
    ON_CALL(recordingProvider, isReadyToRecord())
            .WillByDefault(Return(readyToRecord));

    QSignalSpy spy(&controller, SIGNAL(readyToRecord()));

    recordingProvider._fake._readyToRecordCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingControllerTests, ReadyToRecordSignalIsNotEmittedWhenCallbackInvokedIfProviderReadyToRecordIsFalse)
{

    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    bool readyToRecord = false;
    ON_CALL(recordingProvider, isReadyToRecord())
            .WillByDefault(Return(readyToRecord));

    QSignalSpy spy(&controller, SIGNAL(readyToRecord()));

    recordingProvider._fake._readyToRecordCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 0);
}

TEST_F(RecordingControllerTests, TakeReadingFinishedSignalEmittedWhenCallbackInvoked)
{

    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    QSignalSpy spy(&controller, SIGNAL(takeReadingFinished()));

    recordingProvider._fake._takeReadingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingControllerTests, PaMeanSaveCompleteSignalEmittedWhenCallbackInvoked)
{

    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    QSignalSpy spy(&controller, SIGNAL(paMeanSaveComplete()));

    recordingProvider._fake._setPaMeanCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingControllerTests, PaMeanDataSetAndChangedSignalEmittedWhenCallbackInvoked)
{

    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    QSignalSpy spy(&controller, SIGNAL(paMeanChanged()));

    recordingProvider._fake._setPaMeanCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(recordingProvider.getPaMeanData().offset, controller.paMeanOffset());
    ASSERT_EQ(recordingProvider.getPaMeanData().systolic, controller.offsetSystolic());
    ASSERT_EQ(recordingProvider.getPaMeanData().diastolic, controller.offsetDiastolic());
    ASSERT_EQ(recordingProvider.getPaMeanData().mean, controller.offsetMean());
    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingControllerTests, SetPaMeanSetsValueAndEmitsChangedSignal)
{
    int paMean = 45;

    QSignalSpy spy(&_controller, SIGNAL(paMeanChanged()));

    _controller.setPaMean(paMean);

    ASSERT_EQ(paMean, _controller.paMean());
    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingControllerTests, ReadySignalEmittedWhenProviderCallbackInvoked)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    ON_CALL(recordingProvider, isReadyToRecord())
            .WillByDefault(Return(true));

    QSignalSpy spy(&controller, SIGNAL(readyToRecord()));

    recordingProvider._fake._recordingStatusCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}


TEST_F(RecordingControllerTests, RecordPressuresCallsProviderMethodWithPovidersTransactionId)
{
    int transactionId = 12345;
    ON_CALL(_transactionProvider, getTransactionId())
            .WillByDefault(Return(transactionId));

    EXPECT_CALL(_recordingProvider, takeReading(transactionId))
            .Times(1);

    _controller.recordPressures();
}

TEST_F(RecordingControllerTests, RecordingPressuresFinishedSignalEmittedWhenCallbackInvoked)
{

    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    controller.recordPressures();

    QSignalSpy spy(&controller, SIGNAL(recordingPressuresFinished()));

    recordingProvider._fake._takeReadingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingControllerTests, RecordingCreatedWhenTakeReadingCallbackInvoked)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    int recordingId = 12345;
    std::string waveFormFile = "ValidWaveform.xml";
    ON_CALL(recordingProvider, getRecordingId())
            .WillByDefault(Return(recordingId));

    ON_CALL(recordingProvider, getWaveFormFileName())
            .WillByDefault(Return(waveFormFile));

    recordingProvider._fake._takeReadingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    QObject *obj = qvariant_cast<QObject*>(controller.getNewRecording());
    Recording *recording = dynamic_cast<Recording *>(obj);
    ASSERT_EQ(recordingId, recording->recordingId());
    ASSERT_EQ("Horizontal", recording->position());
    ASSERT_EQ(38, recording->systolic());
    ASSERT_EQ(19, recording->diastolic());
    ASSERT_EQ(26, recording->mean());
    ASSERT_EQ(78, recording->heartRate());
    ASSERT_EQ(66, recording->sensorStrength());
}

MATCHER_P(RecordingDataEqual, rec, "")
{
    return (rec.recordingId == arg.recordingId &&
            rec.uploadPending == arg.uploadPending &&
            rec.systolic == arg.systolic &&
            rec.diastolic == arg.diastolic &&
            rec.mean == arg.mean &&
            rec.heartRate == arg.heartRate &&
            rec.signalStrength == arg.signalStrength &&
            rec.refSystolic == arg.refSystolic &&
            rec.refDiastolic == arg.refDiastolic &&
            rec.refMean == arg.refMean);
}

TEST_F(RecordingControllerTests, SaveRecordingCallsRecordingProviderMethodWithTransactionIdAndRecording)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    NiceMock<MockTransactionProvider> transactionProvider;

    RecordingController controller(transactionProvider, recordingProvider);

    int transactionId = 1111;
    ON_CALL(transactionProvider, getTransactionId())
            .WillByDefault(Return(transactionId));

    int recordingId = 2222;
    std::shared_ptr<Recording> recording(new Recording());
    recording->setRecordingId(recordingId);
    RecordingData recordingData = recording->toRecordingData();

    EXPECT_CALL(recordingProvider, saveRecording(transactionId, RecordingDataEqual(recording->toRecordingData())))
            .Times(1);

    controller.saveRecording(recording.get());
}

TEST_F(RecordingControllerTests, DeleteRecordingCallsRecordingProviderMethodWithTransactionIdAndRecordingId)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    NiceMock<MockTransactionProvider> transactionProvider;

    RecordingController controller(transactionProvider, recordingProvider);

    int transactionId = 1111;
    ON_CALL(transactionProvider, getTransactionId())
            .WillByDefault(Return(transactionId));

    int recordingId = 2222;

    EXPECT_CALL(recordingProvider, deleteRecording(transactionId, recordingId))
            .Times(1);

    controller.deleteRecording(recordingId);
}

TEST_F(RecordingControllerTests, RecordingSaveCompleteSignalEmittedWhenCallbackInvoked)
{

    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    QSignalSpy spy(&controller, SIGNAL(recordingSaveComplete()));

    recordingProvider._fake._saveRecordingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingControllerTests, RecordingDeleteCompleteSignalEmittedWhenCallbackInvoked)
{

    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    QSignalSpy spy(&controller, SIGNAL(recordingDeleteComplete()));

    recordingProvider._fake._deleteRecordingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(RecordingControllerTests, SaveCardiacOutputCallsProviderWithTransactionIdAndCoData)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    NiceMock<MockTransactionProvider> transactionProvider;
    RecordingController controller(transactionProvider, recordingProvider);

    // setup m_newRecording data
    int recordingId = 12345;
    std::string waveFormFile = "ValidWaveform.xml";
    ON_CALL(recordingProvider, getRecordingId())
            .WillByDefault(Return(recordingId));

    ON_CALL(recordingProvider, getWaveFormFileName())
            .WillByDefault(Return(waveFormFile));

    recordingProvider._fake._takeReadingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    EXPECT_CALL(transactionProvider, getTransactionId())
            .Times(1);

    EXPECT_CALL(recordingProvider, setCardiacOutput(_, _))
            .Times(1);

    controller.saveCardiacOutput(3.3F);
}

TEST_F(RecordingControllerTests, SetCardiacOutputCompleteSignalEmittedWhenCallbackInvoked)
{

    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    QSignalSpy spy(&controller, SIGNAL(setCardiacOutputComplete()));

    recordingProvider._fake._setCardiacOutputCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

MATCHER_P(RHCDataEqual, data, "")
{
    return (data.date == arg.date &&
            data.time == arg.time &&
            data.ra == arg.ra &&
            data.rvSystolic == arg.rvSystolic &&
            data.rvDiastoic == arg.rvDiastoic &&
            data.paSystolic == arg.paSystolic &&
            data.paDiastolic == arg.paDiastolic &&
            data.paMean == arg.paMean &&
            data.pcwp == arg.pcwp &&
            data.heartRate == arg.heartRate &&
            data.signalStrength == arg.signalStrength &&
            data.waveformFile == arg.waveformFile);
}

TEST_F(RecordingControllerTests, SaveRHCValesCallsProviderWithTransactionIdAndRHCData)
{
     RhcData rhcData;
     rhcData.date = "22 MAY 2019";
     rhcData.time = "11:34:22";
     rhcData.ra = 33;
     rhcData.rvSystolic = 32;
     rhcData.rvDiastoic = 23;
     rhcData.paSystolic = 31;
     rhcData.paDiastolic = 21;
     rhcData.paMean = 25;
     rhcData.pcwp = 22;
     rhcData.heartRate = 82;
     rhcData.signalStrength = 98;
     rhcData.waveformFile = "ValidWaveform.xml";

    std::shared_ptr<RhcValues> rhcValues = std::shared_ptr<RhcValues>(new RhcValues(&rhcData));

    EXPECT_CALL(_transactionProvider, getTransactionId())
            .Times(1);

    EXPECT_CALL(_recordingProvider, setRhcValues(456123789, RHCDataEqual(rhcData)))
            .Times(1);

    _controller.saveRhcValues(rhcValues.get());
}

TEST_F(RecordingControllerTests, setRhcValuesCompleteSignalEmittedWhenCallbackInvoked)
{
    NiceMock<MockRecordingProvider> recordingProvider;
    recordingProvider.DelegateToFake();
    RecordingController controller(_transactionProvider, recordingProvider);

    QSignalSpy spy(&controller, SIGNAL(setRhcValuesComplete()));

    recordingProvider._fake._setRhcValuesCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}
