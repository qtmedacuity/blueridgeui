#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include <cstring>
#include <vector>

#include "presentation/SensorCalibration.h"
#include "providers/SensorCalibrationData.h"


using namespace ::testing;

class SensorCalibrationTests : public Test
{
public:
    SensorCalibrationTests()
    {

        _sensorCalibrationData.date = "22 MAY 2019";
        _sensorCalibrationData.time = "11:34:22";
        _sensorCalibrationData.systolic = 33;
        _sensorCalibrationData.diastolic = 22;
        _sensorCalibrationData.mean = 25;
        _sensorCalibrationData.reference = 28;
        _sensorCalibrationData.heartRate = 82;
        _sensorCalibrationData.signalStrength = 98;
        _sensorCalibrationData.waveformFile = "/tmp/valid_form.xml";

        _sensorCalibration = std::shared_ptr<SensorCalibration>(new SensorCalibration(&_sensorCalibrationData));
    }

    ~SensorCalibrationTests() override;

    SensorCalibrationData _sensorCalibrationData;
    std::shared_ptr<SensorCalibration> _sensorCalibration;
};

SensorCalibrationTests::~SensorCalibrationTests() {}

TEST_F(SensorCalibrationTests, ConstructorSetsDefaultValues)
{
    SensorCalibration sensorCalibration;

    ASSERT_EQ(sensorCalibration.date(), "");
    ASSERT_EQ(sensorCalibration.time(), "");
    ASSERT_EQ(sensorCalibration.systolic(), 0);
    ASSERT_EQ(sensorCalibration.diastolic(), 0);
    ASSERT_EQ(sensorCalibration.mean(), 0);
    ASSERT_EQ(sensorCalibration.reference(), 0);
    ASSERT_EQ(sensorCalibration.sensorStrength(), 0);
    ASSERT_EQ(sensorCalibration.waveFormFile(), "");
}

TEST_F(SensorCalibrationTests, ConstructorSetsValuesFromSensorCalibrationDataValues)
{
    SensorCalibration sensorCalibration(&_sensorCalibrationData);

    ASSERT_EQ(sensorCalibration.date(), "22 MAY 2019");
    ASSERT_EQ(sensorCalibration.time(), "11:34:22");
    ASSERT_EQ(sensorCalibration.systolic(), _sensorCalibrationData.systolic);
    ASSERT_EQ(sensorCalibration.diastolic(), _sensorCalibrationData.diastolic);
    ASSERT_EQ(sensorCalibration.mean(), _sensorCalibrationData.mean);
    ASSERT_EQ(sensorCalibration.reference(), _sensorCalibrationData.reference);
    ASSERT_EQ(sensorCalibration.heartRate(), _sensorCalibrationData.heartRate);
    ASSERT_EQ(sensorCalibration.sensorStrength(), _sensorCalibrationData.signalStrength);
    ASSERT_EQ(sensorCalibration.waveFormFile(), "/tmp/valid_form.xml");
}

TEST_F(SensorCalibrationTests, DateReturnsSensorCalibrationDate)
{
    ASSERT_EQ("22 MAY 2019", _sensorCalibration->date());
}

TEST_F(SensorCalibrationTests, SetDateSetsTheSensorCalibrationDate)
{
    _sensorCalibration->setDate("120419");
    ASSERT_EQ("120419", _sensorCalibration->date());
}

TEST_F(SensorCalibrationTests, SetDateEmitsSensorCalibrationChangedSignal)
{
    QSignalSpy spy(_sensorCalibration.get(), SIGNAL(calibrationChanged()));
    _sensorCalibration->setDate("120419");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SensorCalibrationTests, DatePropertyReturnsTheSensorCalibrationDate)
{
    QString value = _sensorCalibration->property("date").toString();

    ASSERT_EQ(value, "22 MAY 2019");
}

TEST_F(SensorCalibrationTests, DatePropertySetsTheSensorCalibrationDate)
{
    _sensorCalibration->setProperty("date", "coDate");
    ASSERT_EQ(_sensorCalibration->date(), "coDate");
}

TEST_F(SensorCalibrationTests, DatePropertyHasNotifySignal)
{
    int propertyIndex = _sensorCalibration->metaObject()->indexOfProperty("date");
    QMetaProperty property = _sensorCalibration->metaObject()->property(propertyIndex);

    ASSERT_STREQ("calibrationChanged", property.notifySignal().name().data());
}


TEST_F(SensorCalibrationTests, TimeReturnsSensorCalibrationTime)
{
    ASSERT_EQ("11:34:22", _sensorCalibration->time());
}

TEST_F(SensorCalibrationTests, SetTimeSetsTheSensorCalibrationTime)
{
    _sensorCalibration->setTime("120419");
    ASSERT_EQ("120419", _sensorCalibration->time());
}

TEST_F(SensorCalibrationTests, SetTimeEmitsSensorCalibrationChangedSignal)
{
    QSignalSpy spy(_sensorCalibration.get(), SIGNAL(calibrationChanged()));
    _sensorCalibration->setTime("time");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SensorCalibrationTests, TimePropertyReturnsTheSensorCalibrationTime)
{
    QString value = _sensorCalibration->property("time").toString();

    ASSERT_EQ(value, "11:34:22");
}

TEST_F(SensorCalibrationTests, TimePropertySetsTheSensorCalibrationTime)
{
    _sensorCalibration->setProperty("time", "coTime");
    ASSERT_EQ(_sensorCalibration->time(), "coTime");
}

TEST_F(SensorCalibrationTests, TimePropertyHasNotifySignal)
{
    int propertyIndex = _sensorCalibration->metaObject()->indexOfProperty("time");
    QMetaProperty property = _sensorCalibration->metaObject()->property(propertyIndex);

    ASSERT_STREQ("calibrationChanged", property.notifySignal().name().data());
}

TEST_F(SensorCalibrationTests, SystolicReturnsSensorCalibrationSystolic)
{
    ASSERT_EQ(33, _sensorCalibration->systolic());
}

TEST_F(SensorCalibrationTests, SetSystolicSetsTheSensorCalibrationSystolic)
{
    _sensorCalibration->setSystolic(35);
    ASSERT_EQ(35, _sensorCalibration->systolic());
}

TEST_F(SensorCalibrationTests, SetSystolictEmitsSensorCalibrationChangedSignal)
{
    QSignalSpy spy(_sensorCalibration.get(), SIGNAL(calibrationChanged()));
    _sensorCalibration->setSystolic(35);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SensorCalibrationTests, SystolicPropertyReturnsTheSensorCalibrationSystolic)
{
    int value = _sensorCalibration->property("systolic").toInt();

    ASSERT_EQ(value, 33);
}

TEST_F(SensorCalibrationTests, SystolicropertySetsTheSensorCalibrationSystolic)
{
    _sensorCalibration->setProperty("systolic", 35);
    ASSERT_EQ(_sensorCalibration->systolic(), 35);
}

TEST_F(SensorCalibrationTests,SystolicPropertyHasNotifySignal)
{
    int propertyIndex = _sensorCalibration->metaObject()->indexOfProperty("systolic");
    QMetaProperty property = _sensorCalibration->metaObject()->property(propertyIndex);

    ASSERT_STREQ("calibrationChanged", property.notifySignal().name().data());
}


TEST_F(SensorCalibrationTests, DiastolicReturnsSensorCalibrationDiastolic)
{
    ASSERT_EQ(22, _sensorCalibration->diastolic());
}

TEST_F(SensorCalibrationTests, SetDiastolicSetsTheSensorCalibrationDiastolic)
{
    _sensorCalibration->setDiastolic(25);
    ASSERT_EQ(25, _sensorCalibration->diastolic());
}

TEST_F(SensorCalibrationTests, SetDiastolictEmitsSensorCalibrationChangedSignal)
{
    QSignalSpy spy(_sensorCalibration.get(), SIGNAL(calibrationChanged()));
    _sensorCalibration->setDiastolic(25);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SensorCalibrationTests, DiastolicPropertyReturnsTheSensorCalibrationDiastolic)
{
    int value = _sensorCalibration->property("diastolic").toInt();

    ASSERT_EQ(value, 22);
}

TEST_F(SensorCalibrationTests, DiastolicropertySetsTheSensorCalibrationDiastolic)
{
    _sensorCalibration->setProperty("diastolic", 25);
    ASSERT_EQ(_sensorCalibration->diastolic(), 25);
}

TEST_F(SensorCalibrationTests,DiastolicPropertyHasNotifySignal)
{
    int propertyIndex = _sensorCalibration->metaObject()->indexOfProperty("diastolic");
    QMetaProperty property = _sensorCalibration->metaObject()->property(propertyIndex);

    ASSERT_STREQ("calibrationChanged", property.notifySignal().name().data());
}

TEST_F(SensorCalibrationTests, MeanReturnsSensorCalibrationMean)
{
    ASSERT_EQ(25, _sensorCalibration->mean());
}

TEST_F(SensorCalibrationTests, SetMeanSetsTheSensorCalibrationMean)
{
    _sensorCalibration->setMean(26);
    ASSERT_EQ(26, _sensorCalibration->mean());
}

TEST_F(SensorCalibrationTests, SetMeantEmitsSensorCalibrationChangedSignal)
{
    QSignalSpy spy(_sensorCalibration.get(), SIGNAL(calibrationChanged()));
    _sensorCalibration->setMean(26);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SensorCalibrationTests, MeanPropertyReturnsTheSensorCalibrationMean)
{
    int value = _sensorCalibration->property("mean").toInt();

    ASSERT_EQ(value, 25);
}

TEST_F(SensorCalibrationTests, MeanropertySetsTheSensorCalibrationMean)
{
    _sensorCalibration->setProperty("mean", 26);
    ASSERT_EQ(_sensorCalibration->mean(), 26);
}

TEST_F(SensorCalibrationTests,MeanPropertyHasNotifySignal)
{
    int propertyIndex = _sensorCalibration->metaObject()->indexOfProperty("mean");
    QMetaProperty property = _sensorCalibration->metaObject()->property(propertyIndex);

    ASSERT_STREQ("calibrationChanged", property.notifySignal().name().data());
}


TEST_F(SensorCalibrationTests, ReferenceReturnsSensorCalibrationReference)
{
    ASSERT_EQ(28, _sensorCalibration->reference());
}

TEST_F(SensorCalibrationTests, SetReferenceSetsTheSensorCalibrationReference)
{
    _sensorCalibration->setReference(27);
    ASSERT_EQ(27, _sensorCalibration->reference());
}

TEST_F(SensorCalibrationTests, SetReferencetEmitsSensorCalibrationChangedSignal)
{
    QSignalSpy spy(_sensorCalibration.get(), SIGNAL(calibrationChanged()));
    _sensorCalibration->setReference(27);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SensorCalibrationTests, ReferencePropertyReturnsTheSensorCalibrationReference)
{
    int value = _sensorCalibration->property("reference").toInt();

    ASSERT_EQ(value, 28);
}

TEST_F(SensorCalibrationTests, ReferenceropertySetsTheSensorCalibrationReference)
{
    _sensorCalibration->setProperty("reference", 27);
    ASSERT_EQ(_sensorCalibration->reference(), 27);
}

TEST_F(SensorCalibrationTests,ReferencePropertyHasNotifySignal)
{
    int propertyIndex = _sensorCalibration->metaObject()->indexOfProperty("reference");
    QMetaProperty property = _sensorCalibration->metaObject()->property(propertyIndex);

    ASSERT_STREQ("calibrationChanged", property.notifySignal().name().data());
}

TEST_F(SensorCalibrationTests, HeartRateReturnsSensorCalibrationHeartRate)
{
    ASSERT_EQ(82, _sensorCalibration->heartRate());
}

TEST_F(SensorCalibrationTests, SetHeartRateSetsTheSensorCalibrationHeartRate)
{
    _sensorCalibration->setHeartRate(75);
    ASSERT_EQ(75, _sensorCalibration->heartRate());
}

TEST_F(SensorCalibrationTests, SetHeartRatetEmitsSensorCalibrationChangedSignal)
{
    QSignalSpy spy(_sensorCalibration.get(), SIGNAL(calibrationChanged()));
    _sensorCalibration->setHeartRate(75);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SensorCalibrationTests, HeartRatePropertyReturnsTheSensorCalibrationHeartRate)
{
    int value = _sensorCalibration->property("heartRate").toInt();

    ASSERT_EQ(value, 82);
}

TEST_F(SensorCalibrationTests, HeartRatePropertySetsTheSensorCalibrationHeartRate)
{
    _sensorCalibration->setProperty("heartRate", 75);
    ASSERT_EQ(_sensorCalibration->heartRate(), 75);
}

TEST_F(SensorCalibrationTests, HeartRatePropertyHasNotifySignal)
{
    int propertyIndex = _sensorCalibration->metaObject()->indexOfProperty("heartRate");
    QMetaProperty property = _sensorCalibration->metaObject()->property(propertyIndex);

    ASSERT_STREQ("calibrationChanged", property.notifySignal().name().data());
}


TEST_F(SensorCalibrationTests, SensorStrengthReturnsSensorCalibrationSensorStrength)
{
    ASSERT_EQ(98, _sensorCalibration->sensorStrength());
}

TEST_F(SensorCalibrationTests, SetSensorStrengthSetsTheSensorCalibrationSensorStrength)
{
    _sensorCalibration->setSensorStrength(75);
    ASSERT_EQ(75, _sensorCalibration->sensorStrength());
}

TEST_F(SensorCalibrationTests, SetSensorStrengthtEmitsSensorCalibrationChangedSignal)
{
    QSignalSpy spy(_sensorCalibration.get(), SIGNAL(calibrationChanged()));
    _sensorCalibration->setSensorStrength(75);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SensorCalibrationTests, SensorStrengthPropertyReturnsTheSensorCalibrationSensorStrength)
{
    int value = _sensorCalibration->property("sensorStrength").toInt();

    ASSERT_EQ(value, 98);
}

TEST_F(SensorCalibrationTests, SensorStrengthPropertySetsTheSensorCalibrationSensorStrength)
{
    _sensorCalibration->setProperty("sensorStrength", 95);
    ASSERT_EQ(_sensorCalibration->sensorStrength(), 95);
}

TEST_F(SensorCalibrationTests, SensorStrengthPropertyHasNotifySignal)
{
    int propertyIndex = _sensorCalibration->metaObject()->indexOfProperty("sensorStrength");
    QMetaProperty property = _sensorCalibration->metaObject()->property(propertyIndex);

    ASSERT_STREQ("calibrationChanged", property.notifySignal().name().data());
}

TEST_F(SensorCalibrationTests, WaveFormFileReturnsSensorCalibrationWaveFormFile)
{
    ASSERT_EQ("/tmp/valid_form.xml", _sensorCalibration->waveFormFile());
}

TEST_F(SensorCalibrationTests, SetWaveFormFileSetsTheSensorCalibrationWaveFormFile)
{
    _sensorCalibration->setWaveFormFile("file");
    ASSERT_EQ("file", _sensorCalibration->waveFormFile());
}

TEST_F(SensorCalibrationTests, SetWaveFormFileEmitsSensorCalibrationChangedSignal)
{
    QSignalSpy spy(_sensorCalibration.get(), SIGNAL(calibrationChanged()));
    _sensorCalibration->setWaveFormFile("file");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(SensorCalibrationTests, WaveFormFilePropertyReturnsTheSensorCalibrationWaveFormFile)
{
    QString value = _sensorCalibration->property("waveFormFile").toString();

    ASSERT_EQ(value, "/tmp/valid_form.xml");
}

TEST_F(SensorCalibrationTests, WaveFormFilePropertySetsTheSensorCalibrationWaveFormFile)
{
    _sensorCalibration->setProperty("waveFormFile", "file");
    ASSERT_EQ(_sensorCalibration->waveFormFile(), "file");
}

TEST_F(SensorCalibrationTests, WaveFormFilePropertyHasNotifySignal)
{
    int propertyIndex = _sensorCalibration->metaObject()->indexOfProperty("waveFormFile");
    QMetaProperty property = _sensorCalibration->metaObject()->property(propertyIndex);

    ASSERT_STREQ("calibrationChanged", property.notifySignal().name().data());
}
