#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmem_command.h>

#include "unitTests/mocks/MockMessageParser.h"
#include "providers/RecordingProvider.h"
#include "providers/RecordingData.h"
#include "providers/CardiacOutputData.h"

using namespace ::testing;

class RecordingProviderTests : public Test
{
public:
    RecordingProviderTests()
    {}

    ~RecordingProviderTests() override;

    void SetUp() override
    {
        _recordStatusResponse = "{\n\t\"ReadyToRecord\" : \"True\","
                                "\n\t\"Status\":\"SUCCESS\"\n}\n";

        _readyToRecordResponse = "{\n\t\"ReadyToRecord\" : \"True\","
                                "\n\t\"Status\":\"SUCCESS\"\n}\n";

        _takeReadingResponse = "{\n\t\"TransactionId\":\"1234567\","
                                  "\n\t\"RecordingId\":\"987654\","
                                  "\n\t\"WaveformData\":\"/files/valid_file.xml\","
                                  "\n\t\"Status\" : \"SUCCESS\"\n}\n";

        _paMeanResponse = "{\n\t\"PAMean\" : \"30\","
                          "\n\t\"Status\":\"SUCCESS\"\n}\n";

        _saveRecordingResponse = "{\n\t\"TransactionId\":\"1234567\","
                                 "\n\t\"RecordingId\":\"987654\","
                                 "\n\t\"Status\":\"SUCCESS\"\n}\n";

        _deleteRecordingResponse = "{\n\t\"TransactionId\":\"1234567\","
                                   "\n\t\"RecordingId\":\"987654\","
                                   "\n\t\"Status\":\"SUCCESS\"\n}\n";

        _setCardiacOutputResponse = "{\n\t\"TransactionId\":\"1234567\","
                                   "\n\t\"Status\":\"SUCCESS\"\n}\n";
    }

    std::string _recordStatusResponse;
    std::string _readyToRecordResponse;
    std::string _takeReadingResponse;
    std::string _paMeanResponse;
    std::string _saveRecordingResponse;
    std::string _deleteRecordingResponse;
    std::string _setCardiacOutputResponse;
    std::condition_variable _condition;
};

RecordingProviderTests::~RecordingProviderTests() {}

TEST_F(RecordingProviderTests, ConstructorCallsMessageParserRegisterCallback)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();

    EXPECT_CALL(parser, registerCallback(_))
            .Times(1);

    RecordingProvider provider(parser);
}

TEST_F(RecordingProviderTests, RecordingStatusCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerRecordingStatusCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_recordStatusResponse));
    parser._fake._func(eGET_RECORDING_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(RecordingProviderTests, AllRecordingStatusCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerRecordingStatusCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerRecordingStatusCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_recordStatusResponse));
    parser._fake._func(eGET_RECORDING_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, AllRecordingStatusCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerRecordingStatusCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerRecordingStatusCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterRecordingStatusCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_recordStatusResponse));
    parser._fake._func(eGET_RECORDING_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, ReadyToRecordCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerReadyToRecordCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_readyToRecordResponse));
    parser._fake._func(eASYNC_READY_TO_RECORD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}

TEST_F(RecordingProviderTests, AllReadyToRecordCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerReadyToRecordCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerReadyToRecordCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_readyToRecordResponse));
    parser._fake._func(eASYNC_READY_TO_RECORD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, AllReadyToRecordCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerReadyToRecordCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerReadyToRecordCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterReadyToRecordCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_readyToRecordResponse));
    parser._fake._func(eASYNC_READY_TO_RECORD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, TakeReadingCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerTakeReadingCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_takeReadingResponse));
    parser._fake._func(eTAKE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(RecordingProviderTests, AllTakeReadingCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerTakeReadingCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerTakeReadingCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_takeReadingResponse));
    parser._fake._func(eTAKE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, AllTakeReadingCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerTakeReadingCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerTakeReadingCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterTakeReadingCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_takeReadingResponse));
    parser._fake._func(eTAKE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, DefaultPaMeanCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerGetPaMeanCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_paMeanResponse));
    parser._fake._func(eGET_PA_CATHETER_MEAN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(RecordingProviderTests, AllDefaultPaMeanCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerGetPaMeanCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerGetPaMeanCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_paMeanResponse));
    parser._fake._func(eGET_PA_CATHETER_MEAN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, AllDefaultPaMeanCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerGetPaMeanCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerGetPaMeanCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterGetPaMeanCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_paMeanResponse));
    parser._fake._func(eGET_PA_CATHETER_MEAN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, SetPaMeanCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerSetPaMeanCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_paMeanResponse));
    parser._fake._func(eSET_PA_CATHETER_MEAN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(RecordingProviderTests, AllSetPaMeanCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerSetPaMeanCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerSetPaMeanCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_paMeanResponse));
    parser._fake._func(eSET_PA_CATHETER_MEAN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, AllSetPaMeanCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerSetPaMeanCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerSetPaMeanCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterSetPaMeanCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_paMeanResponse));
    parser._fake._func(eSET_PA_CATHETER_MEAN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, SaveRecordingCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerSaveRecordingCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_saveRecordingResponse));
    parser._fake._func(eSAVE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(RecordingProviderTests, SaveRecordingCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerSaveRecordingCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerSaveRecordingCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_saveRecordingResponse));
    parser._fake._func(eSAVE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, AllSaveRecordingCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerSaveRecordingCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerSaveRecordingCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterSaveRecordingCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_saveRecordingResponse));
    parser._fake._func(eSAVE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, DeleteRecordingCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerDeleteRecordingCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_deleteRecordingResponse));
    parser._fake._func(eDELETE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(RecordingProviderTests, DeleteRecordingCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerDeleteRecordingCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerDeleteRecordingCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_deleteRecordingResponse));
    parser._fake._func(eDELETE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, AllDeleteRecordingCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerDeleteRecordingCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerDeleteRecordingCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterDeleteRecordingCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_deleteRecordingResponse));
    parser._fake._func(eDELETE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, SetCardiacOutputCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerSetCardiacOutputCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_setCardiacOutputResponse));
    parser._fake._func(eSET_CO_DATA, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(RecordingProviderTests, SetCardiacOutputCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerSetCardiacOutputCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerSetCardiacOutputCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_setCardiacOutputResponse));
    parser._fake._func(eSET_CO_DATA, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(RecordingProviderTests, AllSetCardiacOutputCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerSetCardiacOutputCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerSetCardiacOutputCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterSetCardiacOutputCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_deleteRecordingResponse));
    parser._fake._func(eSET_CO_DATA, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}


TEST_F(RecordingProviderTests, RequestRecordingStatusCallsParserSendWithRecordingStatusCommandAndCommandPayload)
{
    NiceMock<MockMessageParser> parser;
    RecordingProvider provider(parser);
    std::string command = "Start";
    std::string payload = "{\n\t\"Command\" : \"" +
                          command +
                          "\"\n}\n";

    EXPECT_CALL(parser, sendCommand(eGET_RECORDING_STATUS, payload))
            .Times(1);

    provider.requestRecordingStatus(command);
}

TEST_F(RecordingProviderTests, RequestTakeReadingCallsParserSendWithTakeReadingCommandAndTransactionIdPayload)
{
    NiceMock<MockMessageParser> parser;
    RecordingProvider provider(parser);
    int transactionId = 12345;
    std::string payload = "{\n\t\"TransactionId\" : \"" +
                          std::to_string(transactionId) +
                          "\"\n}\n";

    EXPECT_CALL(parser, sendCommand(eTAKE_READING, payload))
            .Times(1);

    provider.takeReading(transactionId);
}

TEST_F(RecordingProviderTests, requestDefaultPaMeanCallsParserSendWithGetPaCatheterMeanCommand)
{
    NiceMock<MockMessageParser> parser;
    RecordingProvider provider(parser);

    EXPECT_CALL(parser, sendCommand(eGET_PA_CATHETER_MEAN))
            .Times(1);

    provider.requestPaMean();
}

TEST_F(RecordingProviderTests, setDefaultPaMeanCallsParserSendWithSetPaCatheterMeanCommandAndMeanValue)
{
    NiceMock<MockMessageParser> parser;
    RecordingProvider provider(parser);

    int transactionId = 12345;
    int paMean = 30;
    std::string payload = "{\n\t\"PAMean\" : \"" +
                          std::to_string(paMean) +
                          "\",\n\t\"TransactionId\" : \"" +
                          std::to_string(transactionId) +
                          "\"\n}\n";

    EXPECT_CALL(parser, sendCommand(eSET_PA_CATHETER_MEAN, payload))
            .Times(1);

    provider.setPaMean(transactionId, paMean);
}

TEST_F(RecordingProviderTests, IsReadyToRecordReturnsTrueIfTheReadyToRecordValueFromTheRecordStatusResponseIsTrue)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerRecordingStatusCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_recordStatusResponse));
    parser._fake._func(eGET_RECORDING_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.isReadyToRecord());
}

TEST_F(RecordingProviderTests, IsReadyToRecordReturnsFalseIfTheReadyToRecordValueFromTheRecordStatusResponseIsFalse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerRecordingStatusCallback([&]{ count++; _condition.notify_one(); });

    _recordStatusResponse = "{\n\t\"ReadyToRecord\" : \"false\","
                            "\n\t\"Status\":\"SUCCESS\"\n}\n";
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_recordStatusResponse));
    parser._fake._func(eGET_RECORDING_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isReadyToRecord());
}

TEST_F(RecordingProviderTests, RecordStatusCallbackRegisteredIsNotExecutedWhenThereIsNoReadyToRecordValueInStatusResponse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerRecordingStatusCallback([&]{ count++; _condition.notify_one(); });

    _recordStatusResponse = "{\n\t\"Status\":\"SUCCESS\"\n}\n";
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_recordStatusResponse));
    parser._fake._func(eGET_RECORDING_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(0, count);
}

TEST_F(RecordingProviderTests, IsReadyToRecordReturnsTrueIfTheReadyToRecordValueFromTheAsyncReadyToRecordResponseIsTrue)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerRecordingStatusCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_readyToRecordResponse));
    parser._fake._func(eASYNC_READY_TO_RECORD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_TRUE(provider.isReadyToRecord());
}

TEST_F(RecordingProviderTests, IsReadyToRecordReturnsFalseIfTheReadyToRecordValueFromTheAsyncReadyToRecordResponseIsFalse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerRecordingStatusCallback([&]{ count++; _condition.notify_one(); });

    _readyToRecordResponse = "{\n\t\"ReadyToRecord\" : \"false\","
                            "\n\t\"Status\":\"SUCCESS\"\n}\n";
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_readyToRecordResponse));
    parser._fake._func(eASYNC_READY_TO_RECORD, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_FALSE(provider.isReadyToRecord());
}

TEST_F(RecordingProviderTests, GetRecordingIdReturnsRecordIdValueFromResponse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerTakeReadingCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_takeReadingResponse));
    parser._fake._func(eTAKE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(987654, provider.getRecordingId());
}

TEST_F(RecordingProviderTests, GetWaveFormFileNameReturnedByTakeReadingCall)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerTakeReadingCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_takeReadingResponse));
    parser._fake._func(eTAKE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("/files/valid_file.xml", provider.getWaveFormFileName());
}

TEST_F(RecordingProviderTests, GetPaMeanReturnsPaMeanValueFromResponse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    unsigned long count = 0;
    provider.registerGetPaMeanCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_paMeanResponse));
    parser._fake._func(eGET_PA_CATHETER_MEAN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(30, provider.getPaMean());
}

TEST_F(RecordingProviderTests, SetPaMeanReturnsPaMeanValueFromResponse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    _paMeanResponse = "{\n\t\"PAMean\" : \"45\","
                      "\n\t\"Offset\" : \"10\","
                      "\n\t\"Systolic\" : \"38\","
                      "\n\t\"Diastolic\" : \"19\","
                      "\n\t\"Mean\" : \"24\","
                      "\n\t\"Status\":\"SUCCESS\"\n}\n";
    unsigned long count = 0;
    provider.registerSetPaMeanCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_paMeanResponse));
    parser._fake._func(eSET_PA_CATHETER_MEAN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    PaMeanData paMeanData = provider.getPaMeanData();
    ASSERT_EQ(45, paMeanData.paMean);
    ASSERT_EQ(10, paMeanData.offset);
    ASSERT_EQ(38, paMeanData.systolic);
    ASSERT_EQ(19, paMeanData.diastolic);
    ASSERT_EQ(24, paMeanData.mean);
}

TEST_F(RecordingProviderTests, SaveRecordingCallsParserWithSaveCommandAndAllData)
{
    NiceMock<MockMessageParser> parser;
    RecordingProvider provider(parser);

    int transactionId = 12345;
    RecordingData recording;
    recording.recordingId = 6789;
    recording.systolic = 38;
    recording.diastolic = 19;
    recording.mean = 22;
    recording.refSystolic = 37;
    recording.refDiastolic = 18;
    recording.refMean = 21;
    recording.heartRate = 79;
    recording.signalStrength = 85;
    recording.notes = "Some notes";
    recording.patientPosition = "Horizontal";

    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    payload["RecordingId"] = std::to_string(recording.recordingId);
    payload["Systolic"] = std::to_string(recording.systolic);
    payload["Diastolic"] = std::to_string(recording.diastolic);
    payload["Mean"] = std::to_string(recording.mean);
    payload["ReferenceSystolic"] = std::to_string(recording.refSystolic);
    payload["ReferenceDiastolic"] = std::to_string(recording.refDiastolic);
    payload["ReferenceMean"] = std::to_string(recording.refMean);
    payload["AverageHeartRate"] = std::to_string(recording.heartRate);
    payload["AverageSignalStrength"] = std::to_string(recording.signalStrength);
    payload["Notes"] = recording.notes;
    payload["PatientPosition"] = recording.patientPosition;

    EXPECT_CALL(parser, sendCommand(eSAVE_READING, payload.toStyledString()))
            .Times(1);

    provider.saveRecording(transactionId, recording);
}

TEST_F(RecordingProviderTests, SaveRecordingWithNoRefDataCallsParserWithSaveCommandWithoutRefData)
{
    NiceMock<MockMessageParser> parser;
    RecordingProvider provider(parser);

    int transactionId = 12345;
    RecordingData recording;
    recording.recordingId = 6789;
    recording.systolic = 38;
    recording.diastolic = 19;
    recording.mean = 22;
    recording.heartRate = 79;
    recording.signalStrength = 85;
    recording.notes = "Some notes";
    recording.patientPosition = "Horizontal";

    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    payload["RecordingId"] = std::to_string(recording.recordingId);
    payload["Systolic"] = std::to_string(recording.systolic);
    payload["Diastolic"] = std::to_string(recording.diastolic);
    payload["Mean"] = std::to_string(recording.mean);
    payload["AverageHeartRate"] = std::to_string(recording.heartRate);
    payload["AverageSignalStrength"] = std::to_string(recording.signalStrength);
    payload["Notes"] = recording.notes;
    payload["PatientPosition"] = recording.patientPosition;

    EXPECT_CALL(parser, sendCommand(eSAVE_READING, payload.toStyledString()))
            .Times(1);

    provider.saveRecording(transactionId, recording);
}

TEST_F(RecordingProviderTests, SaveRecordingWithNoNotesCallsParserWithSaveCommandWithoutNotesData)
{
    NiceMock<MockMessageParser> parser;
    RecordingProvider provider(parser);

    int transactionId = 12345;
    RecordingData recording;
    recording.recordingId = 6789;
    recording.systolic = 38;
    recording.diastolic = 19;
    recording.mean = 22;
    recording.heartRate = 79;
    recording.signalStrength = 85;
    recording.patientPosition = "Horizontal";

    Json::Value payload;
    payload["TransactionId"] = std::to_string(transactionId);
    payload["RecordingId"] = std::to_string(recording.recordingId);
    payload["Systolic"] = std::to_string(recording.systolic);
    payload["Diastolic"] = std::to_string(recording.diastolic);
    payload["Mean"] = std::to_string(recording.mean);
    payload["AverageHeartRate"] = std::to_string(recording.heartRate);
    payload["AverageSignalStrength"] = std::to_string(recording.signalStrength);
    payload["PatientPosition"] = recording.patientPosition;

    EXPECT_CALL(parser, sendCommand(eSAVE_READING, payload.toStyledString()))
            .Times(1);

    provider.saveRecording(transactionId, recording);
}

TEST_F(RecordingProviderTests, SaveRecordingCommandReturnsRecordingDataFromResponse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    RecordingProvider provider(parser);

    _saveRecordingResponse = "{\n\"TransactionId\" : \"1556143217\","
       "\n\"RecordingId\" : \"1556143244\","
       "\n\"Status\" : \"SUCCESS\","
        "\n\"Recordings\": ["
                "\n{\n\"RecordingId\": \"1556143244\","
                    "\n\"UploadPending\": \"YES\","
                    "\n\"Date\": \"24 Apr 2019\","
                    "\n\"Time\": \"06:00:44\","
                    "\n\"Systolic\": \"38\","
                    "\n\"Diastolic\": \"19\","
                    "\n\"Mean\": \"26\","
                    "\n\"ReferenceSystolic\": \"37\","
                    "\n\"ReferenceDiastolic\": \"20\","
                    "\n\"ReferenceMean\": \"25\","
                    "\n\"AverageHeartRate\": \"78\","
                    "\n\"AverageSignalStrength\": \"49\","
                    "\n\"PatientPosition\": \"Horizontal\","
                    "\n\"Notes\": \"some notes\","
                    "\n\"WaveformData\": \"/tmp/valid_file.xml\""
         "}]}";
    unsigned long count = 0;
    provider.registerSaveRecordingCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_saveRecordingResponse));
    parser._fake._func(eSAVE_READING, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    RecordingData savedRecording = provider.getSavedRecording();
    ASSERT_EQ(1556143244, savedRecording.recordingId);
    ASSERT_EQ(true, savedRecording.uploadPending);
    ASSERT_EQ("24 Apr 2019", savedRecording.recordingDate);
    ASSERT_EQ("06:00:44", savedRecording.recordingTime);
    ASSERT_EQ(38, savedRecording.systolic);
    ASSERT_EQ(19, savedRecording.diastolic);
    ASSERT_EQ(26, savedRecording.mean);
    ASSERT_EQ(37, savedRecording.refSystolic);
    ASSERT_EQ(20, savedRecording.refDiastolic);
    ASSERT_EQ(25, savedRecording.refMean);
    ASSERT_EQ(78, savedRecording.heartRate);
    ASSERT_EQ(49, savedRecording.signalStrength);
    ASSERT_EQ("Horizontal", savedRecording.patientPosition);
    ASSERT_EQ("/tmp/valid_file.xml", savedRecording.waveformFile);

}

TEST_F(RecordingProviderTests, DeleteRecordingCallsParserWithDeleteCommandAndTransactionAndRecordingId)
{
    NiceMock<MockMessageParser> parser;
    RecordingProvider provider(parser);

    int transactionId = 12345;
    int recordingId = 6789;

    Json::Value json;
    json["TransactionId"] = std::to_string(transactionId);
    json["RecordingId"] = std::to_string(recordingId);
    std::string payload = json.toStyledString();

    EXPECT_CALL(parser, sendCommand(eDELETE_READING, payload))
            .Times(1);

    provider.deleteRecording(transactionId, recordingId);
    ASSERT_EQ(recordingId, provider.getDeletedRecordingId());
}

TEST_F(RecordingProviderTests, SetCardiacOutputCallsParserWithSetCommandTransactionIdAndCardiacOutput)
{
    NiceMock<MockMessageParser> parser;
    RecordingProvider provider(parser);

    int transactionId = 12345;
    CardiacOutputData cardiacOutput;
    cardiacOutput.cardiacOutput = 3.3F;
    cardiacOutput.date = "20 MAY 2019";
    cardiacOutput.time = "11:34:22";
    cardiacOutput.heartRate = 78;
    cardiacOutput.signalStrength = 92;
    cardiacOutput.waveformFile = "/tmp/valid_form.xml";
    cmem_cardiacOutput cmemCo = cardiacOutput.toCmemCardiacOutput();

    std::string payload = "{\n\t\"TransactionId\" : \"12345\","
                          "\t\t\t\"CardiacOutput\": "
                          "{\n\t\t\t\t\"Date\": \"20 May 2019\","
                          "\n\t\t\t\t\"Time\": \"11:34:22\","
                          "\n\t\t\t\t\"CardiacOutput\": \"3.3\","
                          "\n\t\t\t\t\"HeartRate\": \"78\","
                          "\n\t\t\t\t\"SignalStrength\": \"92\","
                          "\n\t\t\t\t\"WaveformData\": \"/tmp/valid_form.xml\","
                          "\n\t\t\t\t\"Notes\": \"\"\n\t\t\t}\n}";

    EXPECT_CALL(parser, sendCommand(eSET_CO_DATA, payload))
            .Times(1);

    provider.setCardiacOutput(transactionId, cardiacOutput);
}
