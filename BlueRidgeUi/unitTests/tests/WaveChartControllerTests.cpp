#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include <QtCharts/QLineSeries>
#include "widgets/WaveChart/WaveChartWidget.h"
#include "presentation/WaveChartController.h"

using namespace QtCharts;
using namespace ::testing;

class WaveChartControllerTests : public Test
{
public:
    WaveChartControllerTests()
        : _fileName("testAxisConfig.json"),
          _controller(_fileName)
    {
        _widget = std::make_shared<WaveChartWidget>(new WaveChartWidget());
        _series = std::make_shared<QLineSeries>(new QLineSeries());
        _controller.setVerticalOffset(1.0);
        _controller.setHorizontalOffset(1.0);
    }
    ~WaveChartControllerTests() override;

    void SetUp() override
    {}

    QString _fileName;
    std::shared_ptr<WaveChartWidget> _widget;
    std::shared_ptr<QLineSeries> _series;
    WaveChartController _controller;
};


WaveChartControllerTests::~WaveChartControllerTests()
{}

TEST_F(WaveChartControllerTests, ConstructorSetsDefaultValues)
{
    WaveChartController controller(_fileName);

    ASSERT_EQ(controller.verticalOffset(), 0.0);
    ASSERT_EQ(controller.horizontalOffset(), 0.0);
    ASSERT_EQ(controller.zoomInVerticalEnabled(), true);
    ASSERT_EQ(controller.zoomOutVerticalEnabled(), false);
    ASSERT_EQ(controller.zoomInHorizontalEnabled(), false);
    ASSERT_EQ(controller.zoomOutHorizontalEnabled(), false);

}

TEST_F(WaveChartControllerTests, RegisterSeriesReturnsAndDoesNotEmitSignalIfSeriesIsNull)
{
    QSignalSpy spy(&_controller, SIGNAL(seriesRegistered(QString)));

    _series = nullptr;

    _controller.registerSeries(_series.get());

    ASSERT_EQ(spy.count(), 0);
}

TEST_F(WaveChartControllerTests, RegisterSeriesEmitSignalIfSeriesIsRegistered)
{
    QSignalSpy spy(&_controller, SIGNAL(seriesRegistered(QString)));

    _series->setName("Series");
    _series->append(0, 0);
    _series->append(1, 1);
    _series->append(2, 2);

    _controller.registerSeries(_series.get());

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, RegisterSeriesDoesNotEmitSignalIfSeriesIsAlreadyRegistered)
{
    QSignalSpy spy(&_controller, SIGNAL(seriesRegistered(QString)));

    _series->setName("Series");
    _series->append(0, 0);
    _series->append(1, 1);
    _series->append(2, 2);

    _controller.registerSeries(_series.get());

    ASSERT_EQ(spy.count(), 1);

    _controller.registerSeries(_series.get());

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, UnregisterSeriesReturnsAndDoesNotEmitSignalIfSeriesIsNull)
{
    QSignalSpy spy(&_controller, SIGNAL(seriesUnegistered(QString)));

    _series = nullptr;

    _controller.unregisterSeries(_series.get());

    ASSERT_EQ(spy.count(), 0);
}

TEST_F(WaveChartControllerTests, UnregisterSeriesEmitSignalIfSeriesIsUnregistered)
{
    QSignalSpy spy(&_controller, SIGNAL(seriesUnegistered(QString)));

    _series->setName("Series");
    _series->append(0, 0);
    _series->append(1, 1);
    _series->append(2, 2);

    _controller.registerSeries(_series.get());
    _controller.unregisterSeries(_series.get());

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, RegisterSeriesDoesNotEmitSignalIfSeriesIsNotRegistered)
{
    QSignalSpy spy(&_controller, SIGNAL(seriesRegistered(QString)));

    _series->setName("Series");
    _series->append(0, 0);
    _series->append(1, 1);
    _series->append(2, 2);

    _controller.unregisterSeries(_series.get());

    ASSERT_EQ(spy.count(), 0);
}

TEST_F(WaveChartControllerTests, RegisterComponentDoesNotRegisterComponentIfComponentAlreadyRegistered)
{
    QSignalSpy spy1(_widget->xAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy2(_widget->xAxis(), SIGNAL(displayMinChanged(qreal)));
    QSignalSpy spy3(_widget->xAxis(), SIGNAL(pointsChanged()));

    QSignalSpy spy4(_widget->yAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy5(_widget->yAxis(), SIGNAL(displayMinChanged(qreal)));
    QSignalSpy spy6(_widget->yAxis(), SIGNAL(pointsChanged()));

    _controller.registerComponent(_widget.get());

    ASSERT_EQ(spy1.count(), 1);
    ASSERT_EQ(spy2.count(), 1);
    ASSERT_EQ(spy3.count(), 1);
    ASSERT_EQ(spy4.count(), 1);
    ASSERT_EQ(spy5.count(), 1);
    ASSERT_EQ(spy6.count(), 1);

    _controller.registerComponent(_widget.get());

    ASSERT_EQ(spy1.count(), 1);
    ASSERT_EQ(spy2.count(), 1);
    ASSERT_EQ(spy3.count(), 1);
    ASSERT_EQ(spy4.count(), 1);
    ASSERT_EQ(spy5.count(), 1);
    ASSERT_EQ(spy6.count(), 1);
}

TEST_F(WaveChartControllerTests, RegisterComponentEmitsWaveAxisSignals)
{
    QSignalSpy spy1(_widget->xAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy2(_widget->xAxis(), SIGNAL(displayMinChanged(qreal)));
    QSignalSpy spy3(_widget->xAxis(), SIGNAL(pointsChanged()));

    QSignalSpy spy4(_widget->yAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy5(_widget->yAxis(), SIGNAL(displayMinChanged(qreal)));
    QSignalSpy spy6(_widget->yAxis(), SIGNAL(pointsChanged()));

    _controller.registerComponent(_widget.get());

    ASSERT_EQ(spy1.count(), 1);
    ASSERT_EQ(spy2.count(), 1);
    ASSERT_EQ(spy3.count(), 1);
    ASSERT_EQ(spy4.count(), 1);
    ASSERT_EQ(spy5.count(), 1);
    ASSERT_EQ(spy6.count(), 1);
}

TEST_F(WaveChartControllerTests, RegisterComponentDoesNotEmitWaveAxisSignalsIfWidgetNullPtr)
{
    _controller.registerComponent(_widget.get());

    QSignalSpy spy1(_widget->xAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy2(_widget->xAxis(), SIGNAL(displayMinChanged(qreal)));
    QSignalSpy spy3(_widget->xAxis(), SIGNAL(pointsChanged()));

    QSignalSpy spy4(_widget->yAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy5(_widget->yAxis(), SIGNAL(displayMinChanged(qreal)));
    QSignalSpy spy6(_widget->yAxis(), SIGNAL(pointsChanged()));

    _widget = nullptr;
    _controller.registerComponent(_widget.get());

    ASSERT_EQ(spy1.count(), 0);
    ASSERT_EQ(spy2.count(), 0);
    ASSERT_EQ(spy3.count(), 0);
    ASSERT_EQ(spy4.count(), 0);
    ASSERT_EQ(spy5.count(), 0);
    ASSERT_EQ(spy6.count(), 0);
}

TEST_F(WaveChartControllerTests, ZoomInHorizontalReturnsAndDoesNotEmitSignalsIfXAxisNullPtr)
{
    _widget = nullptr;
    _controller.registerComponent(_widget.get());

    QSignalSpy spy1(&_controller, SIGNAL(zoomInHorizontalEnabledChanged(bool)));
    QSignalSpy spy2(&_controller, SIGNAL(zoomOutHorizontalEnabledChanged(bool)));

    _controller.zoomInHorizontal();

    ASSERT_EQ(spy1.count(), 0);
    ASSERT_EQ(spy2.count(), 0);
}

TEST_F(WaveChartControllerTests, ZoomInHorizontaEmitZoomOutEnabledSignal)
{
    _controller.registerComponent(_widget.get());

    QSignalSpy spy(&_controller, SIGNAL(zoomOutHorizontalEnabledChanged(bool)));

    _controller.zoomInHorizontal();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, ZoomInHorizontaEmitZoomInEnabledSignal)
{
    _controller.registerComponent(_widget.get());
    _controller.zoomOutHorizontal();

    QSignalSpy spy(&_controller, SIGNAL(zoomInHorizontalEnabledChanged(bool)));

    _controller.zoomInHorizontal();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, ZoomOutHorizontalReturnsAndDoesNotEmitSignalsIfXAxisNullPtr)
{
    _widget = nullptr;
    _controller.registerComponent(_widget.get());

    QSignalSpy spy1(&_controller, SIGNAL(zoomInHorizontalEnabledChanged(bool)));
    QSignalSpy spy2(&_controller, SIGNAL(zoomOutHorizontalEnabledChanged(bool)));

    _controller.zoomOutHorizontal();

    ASSERT_EQ(spy1.count(), 0);
    ASSERT_EQ(spy2.count(), 0);
}

TEST_F(WaveChartControllerTests, ZoomOutHorizontalEmitSZoomInEnabledignal)
{
    _controller.registerComponent(_widget.get());

    QSignalSpy spy(&_controller, SIGNAL(zoomInHorizontalEnabledChanged(bool)));

    _controller.zoomOutHorizontal();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, ZoomOutHorizontalEmitSZoomOutEnabledignal)
{
    _controller.registerComponent(_widget.get());
    _controller.zoomInHorizontal();

    QSignalSpy spy(&_controller, SIGNAL(zoomOutHorizontalEnabledChanged(bool)));

    _controller.zoomOutHorizontal();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, ZoomInVeritcalReturnsAndDoesNotEmitSignalsIfXAxisNullPtr)
{
    _widget = nullptr;
    _controller.registerComponent(_widget.get());

    QSignalSpy spy1(&_controller, SIGNAL(zoomInVerticalEnabledChanged(bool)));
    QSignalSpy spy2(&_controller, SIGNAL(zoomOutVerticalEnabledChanged(bool)));

    _controller.zoomInVertical();

    ASSERT_EQ(spy1.count(), 0);
    ASSERT_EQ(spy2.count(), 0);
}

TEST_F(WaveChartControllerTests, ZoomInVericalaEmitZoomOutEnabledSignal)
{
    _controller.registerComponent(_widget.get());

    QSignalSpy spy(&_controller, SIGNAL(zoomOutVerticalEnabledChanged(bool)));

    _controller.zoomInVertical();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, ZoomInVerticalEmitZoomInEnabledSignal)
{
    _controller.registerComponent(_widget.get());

    QSignalSpy spy(&_controller, SIGNAL(zoomInVerticalEnabledChanged(bool)));

    _controller.zoomInVertical();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, ZoomOutVerticalReturnsAndDoesNotEmitSignalsIfXAxisNullPtr)
{
    _widget = nullptr;
    _controller.registerComponent(_widget.get());

    QSignalSpy spy1(&_controller, SIGNAL(zoomInVerticalEnabledChanged(bool)));
    QSignalSpy spy2(&_controller, SIGNAL(zoomOutVerticalEnabledChanged(bool)));

    _controller.zoomOutVertical();

    ASSERT_EQ(spy1.count(), 0);
    ASSERT_EQ(spy2.count(), 0);
}

TEST_F(WaveChartControllerTests, ZoomOutVericalaEmitZoomOutEnabledSignal)
{
    _controller.registerComponent(_widget.get());
    _controller.zoomInVertical();

    QSignalSpy spy(&_controller, SIGNAL(zoomOutVerticalEnabledChanged(bool)));

    _controller.zoomOutVertical();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, ZoomOutVerticalEmitZoomInEnabledSignal)
{
    _controller.registerComponent(_widget.get());
    _controller.zoomInVertical();

    QSignalSpy spy(&_controller, SIGNAL(zoomInVerticalEnabledChanged(bool)));

    _controller.zoomOutVertical();

    ASSERT_EQ(spy.count(), 1);
}


TEST_F(WaveChartControllerTests, ZoomInHorizontalEnabledReturnsTrueIfZoomInHorizontalEnabled)
{
    _controller.registerComponent(_widget.get());
    _controller.zoomOutHorizontal();
    ASSERT_TRUE(_controller.zoomInHorizontalEnabled());
}

TEST_F(WaveChartControllerTests, ZoomInHorizontalEnabledReturnsFalseIfZoomInHorizontalDisabled)
{
    ASSERT_FALSE(_controller.zoomInHorizontalEnabled());
}

TEST_F(WaveChartControllerTests, ZoomOutHorizontalEnabledReturnsTrueIfZoomOutHorizontalEnabled)
{
    _controller.registerComponent(_widget.get());
    _controller.zoomInHorizontal();
    ASSERT_TRUE(_controller.zoomOutHorizontalEnabled());
}

TEST_F(WaveChartControllerTests, ZoomOutHorizontalEnabledReturnsFalseIfZoomOutHorizontalDisabled)
{
    ASSERT_FALSE(_controller.zoomOutHorizontalEnabled());
}

TEST_F(WaveChartControllerTests, VerticalOffsetReturnsTheControllerVerticalOffset)
{
    ASSERT_EQ(1.0, _controller.verticalOffset());
}

TEST_F(WaveChartControllerTests, SetVerticalOffsetSetsTheControllerVerticalOffset)
{
    _controller.setVerticalOffset(10.0);
    ASSERT_EQ(10.0, _controller.verticalOffset());
}

TEST_F(WaveChartControllerTests, SetVerticalOffsetReturnsIfOffsetDifferenceIsLessThanEpsilon)
{
    QSignalSpy spy(&_controller, SIGNAL(verticalOffsetChanged(double)));

    _controller.setVerticalOffset(1.0);

    ASSERT_EQ(1.0, _controller.verticalOffset());
    ASSERT_EQ(spy.count(), 0);
}

TEST_F(WaveChartControllerTests, SetVerticalOffsetEmitsVerticalOffsetChangedSignal)
{
    QSignalSpy spy(&_controller, SIGNAL(verticalOffsetChanged(double)));

    _controller.setVerticalOffset(10.0);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, HorizontalOffsetReturnsTheControllerHorizontalOffset)
{
    ASSERT_EQ(1.0, _controller.horizontalOffset());
}

TEST_F(WaveChartControllerTests, SetHorizontalOffsetSetsTheControllerHorizontalOffset)
{
    _controller.setHorizontalOffset(5.0);
    ASSERT_EQ(5.0, _controller.horizontalOffset());
}

TEST_F(WaveChartControllerTests, SetHorizontalOffsetReturnsIfOffsetDifferenceIsLessThanEpsilon)
{
    QSignalSpy spy(&_controller, SIGNAL(horizontalOffsetChanged(double)));

    _controller.setHorizontalOffset(1.0);

    ASSERT_EQ(1.0, _controller.horizontalOffset());
    ASSERT_EQ(spy.count(), 0);
}

TEST_F(WaveChartControllerTests, SetHorizontalOffsetEmitsHorizontalOffsetChangedSignal)
{
    QSignalSpy spy(&_controller, SIGNAL(horizontalOffsetChanged(double)));

    _controller.setHorizontalOffset(5.0);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartControllerTests, ZoomInVerticalEnabledEnabledPropertyReturnsTheZoomInVerticalEnabledValue)
{
    bool value = _controller.property("zoomInVerticalEnabled").toBool();
    ASSERT_TRUE(value);
}

TEST_F(WaveChartControllerTests, ZoomInVerticalEnabledPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("zoomInVerticalEnabled");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("zoomInVerticalEnabledChanged", property.notifySignal().name().data());
}

TEST_F(WaveChartControllerTests, ZoomOutVerticalEnabledEnabledPropertyReturnsTheZoomOutVerticalEnabledValue)
{
    bool value = _controller.property("zoomOutVerticalEnabled").toBool();
    ASSERT_FALSE(value);
}

TEST_F(WaveChartControllerTests, ZoomOutVerticalEnabledPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("zoomOutVerticalEnabled");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("zoomOutVerticalEnabledChanged", property.notifySignal().name().data());
}

TEST_F(WaveChartControllerTests, ZoomInHorizontalEnabledEnabledPropertyReturnsTheZoomInHorizontalEnabledValue)
{
    bool value = _controller.property("zoomInHorizontalEnabled").toBool();
    ASSERT_FALSE(value);
}

TEST_F(WaveChartControllerTests, ZoomInHorizontalEnabledPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("zoomInHorizontalEnabled");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("zoomInHorizontalEnabledChanged", property.notifySignal().name().data());
}

TEST_F(WaveChartControllerTests, ZoomOutHorizontalEnabledEnabledPropertyReturnsTheZoomOutHorizontalEnabledValue)
{
    bool value = _controller.property("zoomOutHorizontalEnabled").toBool();
    ASSERT_FALSE(value);
}

TEST_F(WaveChartControllerTests, ZoomOutHorizontalEnabledPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("zoomOutHorizontalEnabled");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("zoomOutHorizontalEnabledChanged", property.notifySignal().name().data());
}

TEST_F(WaveChartControllerTests, ScrollHorizontalDoesNotEmitDisplayUpdatedXAxisSignalsIfAxisNullPtr)
{
    QSignalSpy spy1(_widget->xAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy2(_widget->xAxis(), SIGNAL(displayMinChanged(qreal)));

    _widget = nullptr;
    _controller.registerComponent(_widget.get());

    _controller.scrollHorizontal(50, false);

    ASSERT_EQ(spy1.count(), 0);
    ASSERT_EQ(spy2.count(), 0);
}

TEST_F(WaveChartControllerTests, ScrollHorizontalEmitsDisplayUpdatedXAxisSignals)
{
    _controller.registerComponent(_widget.get());

    QSignalSpy spy1(_widget->xAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy2(_widget->xAxis(), SIGNAL(displayMinChanged(qreal)));

    _controller.scrollHorizontal(-50, true);

    ASSERT_EQ(spy1.count(), 1);
    ASSERT_EQ(spy2.count(), 1);
}

TEST_F(WaveChartControllerTests, ScrollHorizontalReturnsAndDoesNotEmitsDisplayUpdatedIfScrollingNotActive)
{
    _controller.registerComponent(_widget.get());

    QSignalSpy spy1(_widget->xAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy2(_widget->xAxis(), SIGNAL(displayMinChanged(qreal)));

    _controller.scrollHorizontal(-50, false);

    ASSERT_EQ(spy1.count(), 0);
    ASSERT_EQ(spy2.count(), 0);
}

TEST_F(WaveChartControllerTests, ScrollVericalDoesNotEmitDisplayUpdatedYAxisSignalsIfAxisNullPtr)
{
    QSignalSpy spy1(_widget->yAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy2(_widget->yAxis(), SIGNAL(displayMinChanged(qreal)));

    _widget = nullptr;
    _controller.registerComponent(_widget.get());

    _controller.scrollVertical(50, false);

    ASSERT_EQ(spy1.count(), 0);
    ASSERT_EQ(spy2.count(), 0);
}

TEST_F(WaveChartControllerTests, ScrollVerticalEmitsDisplayUpdatedYAxisSignals)
{
    _controller.registerComponent(_widget.get());

    QSignalSpy spy1(_widget->yAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy2(_widget->yAxis(), SIGNAL(displayMinChanged(qreal)));

    _controller.scrollVertical(-50, true);

    ASSERT_EQ(spy1.count(), 1);
    ASSERT_EQ(spy2.count(), 1);
}

TEST_F(WaveChartControllerTests, ScrollVerticalReturnsAndDoesNotEmitsDisplayUpdatedIfScrollingNotActive)
{
    _controller.registerComponent(_widget.get());

    QSignalSpy spy1(_widget->yAxis(), SIGNAL(displayMaxChanged(qreal)));
    QSignalSpy spy2(_widget->yAxis(), SIGNAL(displayMinChanged(qreal)));

    _controller.scrollVertical(-50, false);

    ASSERT_EQ(spy1.count(), 0);
    ASSERT_EQ(spy2.count(), 0);
}
