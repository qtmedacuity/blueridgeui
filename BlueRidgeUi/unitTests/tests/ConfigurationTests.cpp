#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "providers/Configuration.h"

using namespace ::testing;

class ConfigurationTests : public Test
{
public:
    ConfigurationTests()
    {}
    ~ConfigurationTests() override;

    void SetUp() override
    {}
};

ConfigurationTests::~ConfigurationTests()
{}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultConstructorInitializesToZeroStateForIntegers)
{
    ConfigMinMax<int> config;

    EXPECT_EQ(0, config._min);
    EXPECT_EQ(0, config._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultConstructorInitializesToZeroStateForDoubles)
{
    ConfigMinMax<double> config;

    EXPECT_DOUBLE_EQ(0.0, config._min);
    EXPECT_DOUBLE_EQ(0.0, config._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxInitializeConstructorInitializesToCorrectStateForIntegers)
{
    ConfigMinMax<int> config(1,2);

    EXPECT_EQ(1, config._min);
    EXPECT_EQ(2, config._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxInitializeConstructorInitializesToCorrectStateForDoubles)
{
    ConfigMinMax<double> config(2.34567, 9.87654);

    EXPECT_DOUBLE_EQ(2.34567, config._min);
    EXPECT_DOUBLE_EQ(9.87654, config._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxCopyConstructorCopiesToCorrectStateForIntegers)
{
    ConfigMinMax<int> cfg(3,4);
    ConfigMinMax<int> config(cfg);

    EXPECT_EQ(3, config._min);
    EXPECT_EQ(4, config._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxCopyConstructorCopiesToCorrectStateForDoubles)
{
    ConfigMinMax<double> cfg(23.4567, 98.7654);
    ConfigMinMax<double> config(cfg);

    EXPECT_DOUBLE_EQ(23.4567, config._min);
    EXPECT_DOUBLE_EQ(98.7654, config._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxMoveConstructorMovesToCorrectStateForIntegers)
{
    ConfigMinMax<int> config(std::move(ConfigMinMax<int>(4,5)));    // std::move prevents copy elision

    EXPECT_EQ(4, config._min);
    EXPECT_EQ(5, config._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxMoveConstructorMovesToCorrectStateForDoubles)
{
    ConfigMinMax<double> config(std::move(ConfigMinMax<double>(234.567, 987.654))); // std::move prevents copy elision

    EXPECT_DOUBLE_EQ(234.567, config._min);
    EXPECT_DOUBLE_EQ(987.654, config._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxAssignmentOperatorAssignsToCorrectStateForIntegers)
{
    ConfigMinMax<int> cfg(5,6);
    ConfigMinMax<int> config(1,2);

    config = cfg;

    EXPECT_EQ(5, config._min);
    EXPECT_EQ(6, config._max);
    EXPECT_EQ(5, cfg._min);
    EXPECT_EQ(6, cfg._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxAssignmentOperatorAssignsToCorrectStateForDoubles)
{
    ConfigMinMax<double> cfg(2345.67, 9876.54);
    ConfigMinMax<double> config(2.34567, 9.87654);

    config = cfg;

    EXPECT_DOUBLE_EQ(2345.67, config._min);
    EXPECT_DOUBLE_EQ(9876.54, config._max);
    EXPECT_DOUBLE_EQ(2345.67, cfg._min);
    EXPECT_DOUBLE_EQ(9876.54, cfg._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxSwapFunctionSwapsCorrectStatesForIntegers)
{
    ConfigMinMax<int> cfg(6,7);
    ConfigMinMax<int> config(2,3);

    swap(config, cfg);

    EXPECT_EQ(6, config._min);
    EXPECT_EQ(7, config._max);
    EXPECT_EQ(2, cfg._min);
    EXPECT_EQ(3, cfg._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxSwapFunctionSwapsCorrectStatesForDoubles)
{
    ConfigMinMax<double> cfg(23456.7, 98765.4);
    ConfigMinMax<double> config(23.4567, 98.7654);

    swap(config, cfg);

    EXPECT_DOUBLE_EQ(23456.7, config._min);
    EXPECT_DOUBLE_EQ(98765.4, config._max);
    EXPECT_DOUBLE_EQ(23.4567, cfg._min);
    EXPECT_DOUBLE_EQ(98.7654, cfg._max);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultDefaultConstructorInitializesToZeroStateForIntegers)
{
    ConfigMinMaxDefault<int> config;

    EXPECT_EQ(0, config._min);
    EXPECT_EQ(0, config._max);
    EXPECT_EQ(0, config._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultDefaultConstructorInitializesToZeroStateForDoubles)
{
    ConfigMinMaxDefault<double> config;

    EXPECT_DOUBLE_EQ(0.0, config._min);
    EXPECT_DOUBLE_EQ(0.0, config._max);
    EXPECT_DOUBLE_EQ(0.0, config._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultInitializeConstructorInitializesToCorrectStateForIntegers)
{
    ConfigMinMaxDefault<int> config(2,4,3);

    EXPECT_EQ(2, config._min);
    EXPECT_EQ(4, config._max);
    EXPECT_EQ(3, config._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultInitializeConstructorInitializesToCorrectStateForDoubles)
{
    ConfigMinMaxDefault<double> config(2.34567, 9.87654, 3.45678);

    EXPECT_DOUBLE_EQ(2.34567, config._min);
    EXPECT_DOUBLE_EQ(9.87654, config._max);
    EXPECT_DOUBLE_EQ(3.45678, config._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultCopyConstructorCopiesToCorrectStateForIntegers)
{
    ConfigMinMaxDefault<int> cfg(3,5,4);
    ConfigMinMaxDefault<int> config(cfg);

    EXPECT_EQ(3, config._min);
    EXPECT_EQ(5, config._max);
    EXPECT_EQ(4, config._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultCopyConstructorCopiesToCorrectStateForDoubles)
{
    ConfigMinMaxDefault<double> cfg(23.4567, 98.7654, 34.5678);
    ConfigMinMaxDefault<double> config(cfg);

    EXPECT_DOUBLE_EQ(23.4567, config._min);
    EXPECT_DOUBLE_EQ(98.7654, config._max);
    EXPECT_DOUBLE_EQ(34.5678, config._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultMoveConstructorMovesToCorrectStateForIntegers)
{
    ConfigMinMaxDefault<int> config(std::move(ConfigMinMaxDefault<int>(4,6,5)));    // std::move prevents copy elision

    EXPECT_EQ(4, config._min);
    EXPECT_EQ(6, config._max);
    EXPECT_EQ(5, config._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultMoveConstructorMovesToCorrectStateForDoubles)
{
    ConfigMinMaxDefault<double> config(std::move(ConfigMinMaxDefault<double>(234.567, 987.654, 345.678)));  // std::move prevents copy elision

    EXPECT_DOUBLE_EQ(234.567, config._min);
    EXPECT_DOUBLE_EQ(987.654, config._max);
    EXPECT_DOUBLE_EQ(345.678, config._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultAssignmentOperatorAssignsToCorrectStateForIntegers)
{
    ConfigMinMaxDefault<int> cfg(5,7,6);
    ConfigMinMaxDefault<int> config(1,3,2);

    config = cfg;

    EXPECT_EQ(5, config._min);
    EXPECT_EQ(7, config._max);
    EXPECT_EQ(6, config._default);
    EXPECT_EQ(5, cfg._min);
    EXPECT_EQ(7, cfg._max);
    EXPECT_EQ(6, cfg._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultAssignmentOperatorAssignsToCorrectStateForDoubles)
{
    ConfigMinMaxDefault<double> cfg(2345.67, 9876.54, 3456.78);
    ConfigMinMaxDefault<double> config(2.34567, 9.87654, 3.45678);

    config = cfg;

    EXPECT_DOUBLE_EQ(2345.67, config._min);
    EXPECT_DOUBLE_EQ(9876.54, config._max);
    EXPECT_DOUBLE_EQ(3456.78, config._default);
    EXPECT_DOUBLE_EQ(2345.67, cfg._min);
    EXPECT_DOUBLE_EQ(9876.54, cfg._max);
    EXPECT_DOUBLE_EQ(3456.78, cfg._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultSwapFunctionSwapsCorrectStatesForIntegers)
{
    ConfigMinMaxDefault<int> cfg(6,8,7);
    ConfigMinMaxDefault<int> config(2,4,3);

    swap(config, cfg);

    EXPECT_EQ(6, config._min);
    EXPECT_EQ(8, config._max);
    EXPECT_EQ(7, config._default);
    EXPECT_EQ(2, cfg._min);
    EXPECT_EQ(4, cfg._max);
    EXPECT_EQ(3, cfg._default);
}

TEST_F(ConfigurationTests, ConfigMinMaxDefaultSwapFunctionSwapsCorrectStatesForDoubles)
{
    ConfigMinMaxDefault<double> cfg(23456.7, 98765.4, 34567.8);
    ConfigMinMaxDefault<double> config(23.4567, 98.7654, 34.5678);

    swap(config, cfg);

    EXPECT_DOUBLE_EQ(23456.7, config._min);
    EXPECT_DOUBLE_EQ(98765.4, config._max);
    EXPECT_DOUBLE_EQ(34567.8, config._default);
    EXPECT_DOUBLE_EQ(23.4567, cfg._min);
    EXPECT_DOUBLE_EQ(98.7654, cfg._max);
    EXPECT_DOUBLE_EQ(34.5678, cfg._default);
}

TEST_F(ConfigurationTests, ConfigurationDefaultConstructorInitializesToZeroState)
{
    Configuration config;

    EXPECT_STREQ("", config._language.c_str());
    EXPECT_EQ(0, config._paMean._min);
    EXPECT_EQ(0, config._paMean._max);
    EXPECT_EQ(0, config._searchPressure._min);
    EXPECT_EQ(0, config._searchPressure._max);
    EXPECT_EQ(0, config._searchPressure._default);
    EXPECT_EQ(0, config._systolic._min);
    EXPECT_EQ(0, config._systolic._max);
    EXPECT_EQ(0, config._diastolic._min);
    EXPECT_EQ(0, config._diastolic._max);
    EXPECT_EQ(0, config._referenceMean._min);
    EXPECT_EQ(0, config._referenceMean._max);
    EXPECT_FALSE(config._hasDate);
    EXPECT_FALSE(config._hasTime);
}

TEST_F(ConfigurationTests, ConfigurationInitializeConstructorInitializesToCorrectState)
{
    Configuration config(std::string("klingon"), ConfigPAMean(1,2), ConfigSearchPressure(2,4,3), ConfigSystolic(4,5), ConfigDiastolic(5,6), ConfigReferenceMean(6,7), true, true);

    EXPECT_STREQ("klingon", config._language.c_str());
    EXPECT_EQ(1, config._paMean._min);
    EXPECT_EQ(2, config._paMean._max);
    EXPECT_EQ(2, config._searchPressure._min);
    EXPECT_EQ(4, config._searchPressure._max);
    EXPECT_EQ(3, config._searchPressure._default);
    EXPECT_EQ(4, config._systolic._min);
    EXPECT_EQ(5, config._systolic._max);
    EXPECT_EQ(5, config._diastolic._min);
    EXPECT_EQ(6, config._diastolic._max);
    EXPECT_EQ(6, config._referenceMean._min);
    EXPECT_EQ(7, config._referenceMean._max);
    EXPECT_TRUE(config._hasDate);
    EXPECT_TRUE(config._hasTime);
}

TEST_F(ConfigurationTests, ConfigurationCopyConstructorCopiesToCorrectState)
{
    Configuration cfg(std::string("klingon"), ConfigPAMean(1,2), ConfigSearchPressure(2,4,3), ConfigSystolic(4,5), ConfigDiastolic(5,6), ConfigReferenceMean(6,7), true, true);
    Configuration config(cfg);

    EXPECT_STREQ("klingon", config._language.c_str());
    EXPECT_EQ(1, config._paMean._min);
    EXPECT_EQ(2, config._paMean._max);
    EXPECT_EQ(2, config._searchPressure._min);
    EXPECT_EQ(4, config._searchPressure._max);
    EXPECT_EQ(3, config._searchPressure._default);
    EXPECT_EQ(4, config._systolic._min);
    EXPECT_EQ(5, config._systolic._max);
    EXPECT_EQ(5, config._diastolic._min);
    EXPECT_EQ(6, config._diastolic._max);
    EXPECT_EQ(6, config._referenceMean._min);
    EXPECT_EQ(7, config._referenceMean._max);
    EXPECT_TRUE(config._hasDate);
    EXPECT_TRUE(config._hasTime);
}

TEST_F(ConfigurationTests, ConfigurationMoveConstructorMovesToCorrectState)
{
    Configuration config(std::move(Configuration(std::string("borg"), ConfigPAMean(2,3), ConfigSearchPressure(3,5,4), ConfigSystolic(5,6), ConfigDiastolic(6,7), ConfigReferenceMean(7,8), false, true)));  // std::move prevents copy elision

    EXPECT_STREQ("borg", config._language.c_str());
    EXPECT_EQ(2, config._paMean._min);
    EXPECT_EQ(3, config._paMean._max);
    EXPECT_EQ(3, config._searchPressure._min);
    EXPECT_EQ(5, config._searchPressure._max);
    EXPECT_EQ(4, config._searchPressure._default);
    EXPECT_EQ(5, config._systolic._min);
    EXPECT_EQ(6, config._systolic._max);
    EXPECT_EQ(6, config._diastolic._min);
    EXPECT_EQ(7, config._diastolic._max);
    EXPECT_EQ(7, config._referenceMean._min);
    EXPECT_EQ(8, config._referenceMean._max);
    EXPECT_FALSE(config._hasDate);
    EXPECT_TRUE(config._hasTime);
}

TEST_F(ConfigurationTests, ConfigurationAssignmentOperatorAssignsToCorrectState)
{
    Configuration cfg(std::string("babytalk"), ConfigPAMean(0,1), ConfigSearchPressure(1,3,2), ConfigSystolic(3,4), ConfigDiastolic(4,5), ConfigReferenceMean(5,6), true, false);
    Configuration config(cfg);

    config = cfg;

    EXPECT_STREQ("babytalk", config._language.c_str());
    EXPECT_EQ(0, config._paMean._min);
    EXPECT_EQ(1, config._paMean._max);
    EXPECT_EQ(1, config._searchPressure._min);
    EXPECT_EQ(3, config._searchPressure._max);
    EXPECT_EQ(2, config._searchPressure._default);
    EXPECT_EQ(3, config._systolic._min);
    EXPECT_EQ(4, config._systolic._max);
    EXPECT_EQ(4, config._diastolic._min);
    EXPECT_EQ(5, config._diastolic._max);
    EXPECT_EQ(5, config._referenceMean._min);
    EXPECT_EQ(6, config._referenceMean._max);
    EXPECT_TRUE(config._hasDate);
    EXPECT_FALSE(config._hasTime);
}

TEST_F(ConfigurationTests, ConfigurationSwapFunctionSwapsCorrectStates)
{
    Configuration cfg(std::string("borg"), ConfigPAMean(2,3), ConfigSearchPressure(3,5,4), ConfigSystolic(5,6), ConfigDiastolic(6,7), ConfigReferenceMean(7,8), false, true);
    Configuration config(std::string("babytalk"), ConfigPAMean(0,1), ConfigSearchPressure(1,3,2), ConfigSystolic(3,4), ConfigDiastolic(4,5), ConfigReferenceMean(5,6), true, false);

    swap(config, cfg);

    EXPECT_STREQ("borg", config._language.c_str());
    EXPECT_EQ(2, config._paMean._min);
    EXPECT_EQ(3, config._paMean._max);
    EXPECT_EQ(3, config._searchPressure._min);
    EXPECT_EQ(5, config._searchPressure._max);
    EXPECT_EQ(4, config._searchPressure._default);
    EXPECT_EQ(5, config._systolic._min);
    EXPECT_EQ(6, config._systolic._max);
    EXPECT_EQ(6, config._diastolic._min);
    EXPECT_EQ(7, config._diastolic._max);
    EXPECT_EQ(7, config._referenceMean._min);
    EXPECT_EQ(8, config._referenceMean._max);
    EXPECT_FALSE(config._hasDate);
    EXPECT_TRUE(config._hasTime);

    EXPECT_STREQ("babytalk", cfg._language.c_str());
    EXPECT_EQ(0, cfg._paMean._min);
    EXPECT_EQ(1, cfg._paMean._max);
    EXPECT_EQ(1, cfg._searchPressure._min);
    EXPECT_EQ(3, cfg._searchPressure._max);
    EXPECT_EQ(2, cfg._searchPressure._default);
    EXPECT_EQ(3, cfg._systolic._min);
    EXPECT_EQ(4, cfg._systolic._max);
    EXPECT_EQ(4, cfg._diastolic._min);
    EXPECT_EQ(5, cfg._diastolic._max);
    EXPECT_EQ(5, cfg._referenceMean._min);
    EXPECT_EQ(6, cfg._referenceMean._max);
    EXPECT_TRUE(cfg._hasDate);
    EXPECT_FALSE(cfg._hasTime);
}
