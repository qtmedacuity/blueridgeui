#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <thread>
#include "providers/Timer.h"

using namespace ::testing;

class TimerTests : public Test
{
public:
    TimerTests()
    {}

    ~TimerTests() override;

    std::condition_variable _condition;
};

TimerTests::~TimerTests() {}

TEST_F(TimerTests, TimerStartExecutesCallbackAfterTimeoutInMicrosecondsPeriodCompletes)
{
    unsigned int count = 1;
    Timer::Callback cb = [&](){count++; _condition.notify_one(); };

    std::chrono::microseconds timeout(500000);

    Timer timer;
    timer.start(timeout, cb);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(600));
    }

    ASSERT_EQ(count, 2);
}

TEST_F(TimerTests, TimerStartExecutesSameCallbackAgainAfterTimeoutInMicrosecondsPeriodCompletes)
{
    unsigned int count = 1;
    Timer::Callback cb = [&](){count++; _condition.notify_one(); };

    std::chrono::microseconds timeout(500000);

    Timer timer;
    timer.start(timeout, cb);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(600));
    }

    timer.start(timeout);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(600));
    }

    ASSERT_EQ(count, 3);
}

TEST_F(TimerTests, TimerStartExecutesCallbackAfterTimeoutInHzPeriodCompletes)
{
    unsigned int count = 5;
    Timer::Callback cb = [&](){count++; _condition.notify_one(); };

    unsigned int timeoutHz = 1;

    Timer timer;
    timer.start(timeoutHz, cb);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1100));
    }

    ASSERT_EQ(count, 6);
}

TEST_F(TimerTests, TimerStartExecutesSameCallbackAfterTimeoutInHzPeriodCompletes)
{
    unsigned int count = 5;
    Timer::Callback cb = [&](){count++; _condition.notify_one(); };

    unsigned int timeoutHz = 1;

    Timer timer;
    timer.start(timeoutHz, cb);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1100));
    }

    timer.start(timeoutHz);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(1100));
    }

    ASSERT_EQ(count, 7);
}

TEST_F(TimerTests, TimerStopStopsTheTimerAndDoesNotExecuteTheCallback)
{
    unsigned int count = 2;
    Timer::Callback cb = [&](){count++; _condition.notify_one(); };

    unsigned int timeoutHz = 1;

    Timer timer;
    timer.start(timeoutHz, cb);
    timer.stop();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(count, 2);
}
