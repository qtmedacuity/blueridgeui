#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "unitTests/mocks/MockDataSocket.h"
#include "providers/MessageParser.h"

#include <cmem_command.h>

#include <cstring>
#include <limits>
#include <mutex>
#include <condition_variable>

using namespace ::testing;
using ::testing::DoAll;
using ::testing::Invoke;

class MessageParserTests : public Test
{
public:
    MessageParserTests()
        : _rsp({{eUNKNOWN, 0}, nullptr}),
          _parser(_socket)
    {}

    void SetUp() override
    {
        ON_CALL(_socket, readbytes(_,_,_,_))
                .WillByDefault(Invoke([&](void* buffer, size_t count, int&, std::string&) -> ssize_t
                                {
                                    if (sizeof(_rsp.header) == count) {
                                        std::memcpy(buffer, &_rsp, count);
                                    } else if (0 != _rsp.header.length && _rsp.header.length == count) {
                                        std::memcpy(buffer, _rsp.response, count);
                                    }
                                    return static_cast<ssize_t>(count);
                                }));

        ON_CALL(_socket, writebytes(_,_,_,_))
                .WillByDefault(Invoke([&](void const*, size_t count, int&, std::string&) -> ssize_t
                                {
                                    return static_cast<ssize_t>(count);
                                }));
    }

    ~MessageParserTests() override;

    response_t _rsp;

    NiceMock<MockDataSocket> _socket;
    MessageParser _parser;

    std::condition_variable _condition;
};

MessageParserTests::~MessageParserTests() {}

TEST_F(MessageParserTests, RegisterCallbackReturnsInvalidFunctionDescriptorForNullCallback)
{
    MessageParser::Callback cb = nullptr;
    unsigned long funcId = _parser.registerCallback(cb);

    EXPECT_EQ(std::numeric_limits<unsigned long>::max(), funcId);
}

TEST_F(MessageParserTests, RegisterCallbackReturnsFuncIdForCallbackRegistered)
{
    MessageParser::Callback cb =[&](unsigned int, MessageParser::Payload) {};
    unsigned long funcId = _parser.registerCallback(cb);

    EXPECT_EQ(0, funcId);
}

TEST_F(MessageParserTests, RegisterCallbackReturnsFuncIdsForEachCallbackRegistered)
{
    MessageParser::Callback cb1 =[&](unsigned int, MessageParser::Payload) {};
    unsigned long funcId1 = _parser.registerCallback(cb1);

    MessageParser::Callback cb2 =[&](unsigned int, MessageParser::Payload) {};
    unsigned long funcId2 = _parser.registerCallback(cb2);

    EXPECT_EQ(0, funcId1);
    EXPECT_EQ(1, funcId2);
}

TEST_F(MessageParserTests, RegisteredCallbackIsExecutedByReadResponse)
{
    unsigned int count = 0;
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { count++; _condition.notify_one(); });

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(1, count);
}

TEST_F(MessageParserTests, RegisteredCallbacksAreExecutedByReadResponse)
{
    unsigned int count1 = 1;
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { count1++; });
    unsigned int count2 = 2;
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { count2++; _condition.notify_one(); });

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(3, count2);
}

TEST_F(MessageParserTests, UnRegisteredCallbackIsNotExecutedByReadResponse)
{
    unsigned int count = 0;
    unsigned long funcId = _parser.registerCallback([&](unsigned int, MessageParser::Payload) { count++; _condition.notify_one(); });

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(1, count);

    _parser.unregisterCallback(funcId);

    _parser.sendCommand(eCOMMAND_CLOSE);

    ASSERT_EQ(1, count);
}

TEST_F(MessageParserTests, UnRegisteredCallbacksAreNotExecutedByReadResponse)
{
    unsigned int count1 = 1;
    unsigned long funcId = _parser.registerCallback([&](unsigned int, MessageParser::Payload) { count1++; });
    unsigned int count2 = 2;
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { count2++; _condition.notify_one(); });

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(3, count2);

    _parser.unregisterCallback(funcId);
    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(4, count2);
}

TEST_F(MessageParserTests, SendCommandCallsDataSocketWriteBytesOnce)
{
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    EXPECT_CALL(_socket, writebytes(_,_,_,_))
            .Times(1);

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }
}

TEST_F(MessageParserTests, SendCommandCallsDataSocketWriteBytesUntilAllBytesWritten)
{
    ON_CALL(_socket, writebytes(_,_,_,_))
            .WillByDefault(Return(2));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    EXPECT_CALL(_socket, writebytes(_,_,_,_))
            .Times(4);

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }
}

TEST_F(MessageParserTests, SendCommandReturnsTrueAfterResponseFromDataSocket)
{
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    bool returnVal = _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_TRUE(returnVal);
}

TEST_F(MessageParserTests, SendCommandReturnsFalseIfCommandSentIsUnkownCommand)
{
    bool returnVal = _parser.sendCommand(eUNKNOWN);
    ASSERT_FALSE(returnVal);
}

TEST_F(MessageParserTests, SendCommandWithStringCallsDataSocketWriteBytesTwice)
{
    unsigned int idx(0);
    int bytes[] {8, 7};
    ON_CALL(_socket, writebytes(_,_,_,_))
            .WillByDefault(Invoke([&](void const*, size_t, int&, std::string&) -> ssize_t { return bytes[idx++]; }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    EXPECT_CALL(_socket, writebytes(_,_,_,_))
            .Times(2);

    _parser.sendCommand(eCOMMAND_CLOSE, "payload");

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }
}

TEST_F(MessageParserTests, SendCommandWithStringCallsDataSocketWriteBytesUntilAllBytesWritten)
{
    unsigned int idx(0);
    int bytes[] {4, 4, 4, 3};
    ON_CALL(_socket, writebytes(_,_,_,_))
            .WillByDefault(Invoke([&](void const*, size_t, int&, std::string&) -> ssize_t { return bytes[idx++]; }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    EXPECT_CALL(_socket, writebytes(_,_,_,_))
            .Times(4);

    _parser.sendCommand(eCOMMAND_CLOSE, "payload");

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }
}

TEST_F(MessageParserTests, SendCommandWithStringReturnsTrueAfterResponseFromDataSocket)
{
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    bool returnVal = _parser.sendCommand(eCOMMAND_CLOSE, "payload");

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_TRUE(returnVal);
}

TEST_F(MessageParserTests, SendCommandWithStringReturnsFalseIfCommandSentIsUnkownCommand)
{
    bool returnVal = _parser.sendCommand(eUNKNOWN, "payload");
    ASSERT_FALSE(returnVal);
}

TEST_F(MessageParserTests, SendCommandWithPayloadCallsDataSocketWriteBytesTwice)
{
    unsigned int idx(0);
    int bytes[] {8, 7};
    ON_CALL(_socket, writebytes(_,_,_,_))
            .WillByDefault(Invoke([&](void const*, size_t, int&, std::string&) -> ssize_t { return bytes[idx++]; }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    EXPECT_CALL(_socket, writebytes(_,_,_,_))
            .Times(2);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload"));
    _parser.sendCommand(eCOMMAND_CLOSE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }
}

TEST_F(MessageParserTests, SendCommandWithPayloadCallsDataSocketWriteBytesUntilAllBytesWritten)
{
    unsigned int idx(0);
    int bytes[] {3, 3, 2, 3, 3, 1};
    ON_CALL(_socket, writebytes(_,_,_,_))
            .WillByDefault(Invoke([&](void const*, size_t, int&, std::string&) -> ssize_t { return bytes[idx++]; }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    EXPECT_CALL(_socket, writebytes(_,_,_,_))
            .Times(6);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload"));
    _parser.sendCommand(eCOMMAND_CLOSE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }
}

TEST_F(MessageParserTests, SendCommandWithPayloadReturnsTrueAfterResponseFromDataSocket)
{
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload"));
    bool returnVal = _parser.sendCommand(eCOMMAND_CLOSE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_TRUE(returnVal);
}

TEST_F(MessageParserTests, SendCommandWithPayloadReturnsFalseIfCommandSentIsUnkownCommand)
{
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload"));
    bool returnVal = _parser.sendCommand(eUNKNOWN, payload);
    ASSERT_FALSE(returnVal);
}

TEST_F(MessageParserTests, SendCommandCallsDataSocketReadBytes)
{
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(1);

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }
}

TEST_F(MessageParserTests, SendCommandWithStringCallsDataSocketReadBytes)
{
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(1);

    _parser.sendCommand(eCOMMAND_CLOSE, "payload");

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }
}

TEST_F(MessageParserTests, SendCommandWithPayloadCallsDataSocketReadBytes)
{
    _parser.registerCallback([&](unsigned int, MessageParser::Payload) { _condition.notify_one(); });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload"));
    _parser.sendCommand(eCOMMAND_CLOSE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }
}

TEST_F(MessageParserTests, RegisteredCallbackReceivesAResponseWithNoPayload)
{
    unsigned int cmdId(eCOMMAND_CLOSE);
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload"));

    _parser.registerCallback([&](unsigned int id, MessageParser::Payload data) {
        cmdId = id;
        payload = data;
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(1);

    _parser.sendCommand(cmdId, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(cmdId, eUNKNOWN);
    ASSERT_EQ(nullptr, payload);
}

TEST_F(MessageParserTests, RegisteredCallbackReceivesAResponseWithAPayload)
{
    unsigned int cmdId(eCOMMAND_CLOSE);
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload"));
    char response_data[] = "{\n\t\"UserName\" : \"Test User\",\n\t\"Status\" : \"SUCCESS\"\n}\n";
    _rsp = {{eGET_LOGIN_STATUS, sizeof(response_data)}, response_data};

    _parser.registerCallback([&](unsigned int id, MessageParser::Payload data) {
        cmdId = id;
        payload = data;
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(2);

    _parser.sendCommand(cmdId, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(eGET_LOGIN_STATUS, cmdId);
    ASSERT_NE(nullptr, payload);
    ASSERT_STREQ(response_data, payload.get()->c_str());
}

TEST_F(MessageParserTests, RegisteredCallbackDoesNotReceivesAResponseWithABadPayload)
{
    unsigned int cmdId(eCOMMAND_CLOSE);
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload"));
    char response_data[] = "barbage";
    _rsp = {{eGET_LOGIN_STATUS, sizeof(response_data)}, response_data};

    _parser.registerCallback([&](unsigned int id, MessageParser::Payload data) {
        cmdId = id;
        payload = data;
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(2);

    _parser.sendCommand(cmdId, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(eCOMMAND_CLOSE, cmdId);
    ASSERT_NE(nullptr, payload);
    ASSERT_STREQ("payload", payload.get()->c_str());
}

TEST_F(MessageParserTests, RegisterErrorCallbackReturnsInvalidFunctionDescriptorForNullCallback)
{
    MessageParser::ErrorCallback cb = nullptr;
    unsigned long funcId = _parser.registerErrorCallback(cb);

    EXPECT_EQ(std::numeric_limits<unsigned long>::max(), funcId);
}

TEST_F(MessageParserTests, RegisterErrorCallbackReturnsFuncIdForCallbackRegistered)
{
    MessageParser::ErrorCallback cb =[&](DataSocketError) {};
    unsigned long funcId = _parser.registerErrorCallback(cb);

    EXPECT_EQ(0, funcId);
}

TEST_F(MessageParserTests, RegisterErrorCallbackReturnsFuncIdsForEachCallbackRegistered)
{
    MessageParser::ErrorCallback cb1 =[&](DataSocketError) {};
    unsigned long funcId1 = _parser.registerErrorCallback(cb1);

    MessageParser::ErrorCallback cb2 =[&](DataSocketError) {};
    unsigned long funcId2 = _parser.registerErrorCallback(cb2);

    EXPECT_EQ(0, funcId1);
    EXPECT_EQ(1, funcId2);
}

TEST_F(MessageParserTests, RegisteredErrorCallbackReceivesReadFailedErrorIfReadBytesReturnsZeroBytesRead)
{
    unsigned int dataCount(0);
    unsigned int errorCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, readbytes(_,_,_,_))
            .WillByDefault(Invoke([&](void*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                err = EAGAIN;
                                errstr = "Testing zero bytes read";
                                return 0;
                            }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) {
        dataCount++;
        _condition.notify_one();
    });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorCount++;
        errorValue = err;
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(61);

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(1, errorCount);
    ASSERT_EQ(DataSocketError::ReadFailed, errorValue);
}

TEST_F(MessageParserTests, RegisteredErrorCallbackWriteFailedErrorIfWriteBytesReturnsZeroBytesWritten)
{
    unsigned int dataCount(0);
    unsigned int errorCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, writebytes(_,_,_,_))
            .WillByDefault(Invoke([&](void const*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                err = EAGAIN;
                                errstr = "Testing zero bytes written";
                                return 0;
                            }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) {
        dataCount++;
        _condition.notify_one();
    });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorCount++;
        errorValue = err;
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, writebytes(_,_,_,_))
            .Times(6);

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(1, errorCount);
    ASSERT_EQ(DataSocketError::WriteFailed, errorValue);
}

TEST_F(MessageParserTests, RegisteredErrorCallbackReceivesConnectionFailedErrorIfReadBytesEncountersASocketDisconnectionError)
{
    unsigned int dataCount(0);
    unsigned int errorCount(0);
    unsigned int disconnectCount(0);
    unsigned int reconnectCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, readbytes(_,_,_,_))
            .WillByDefault(Invoke([&](void*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                err = ENOTCONN;
                                errstr = "Testing connection failed";
                                return -1;
                            }));

    ON_CALL(_socket, disconnect())
            .WillByDefault(Invoke([&]() -> int
                            {
                                disconnectCount++;
                                return 0;
                            }));

    ON_CALL(_socket, reconnect(_,_))
            .WillByDefault(Invoke([&](int&, std::string&) -> int
                            {
                                reconnectCount++;
                                _condition.notify_one();
                                return 0;
                            }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) {
        dataCount++;
        _condition.notify_one();
    });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorCount++;
        errorValue = err;
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(1);
    EXPECT_CALL(_socket, disconnect())
            .Times(1);
    EXPECT_CALL(_socket, reconnect(_,_))
            .Times(1);

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(5000));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(1, errorCount);
    ASSERT_EQ(DataSocketError::ConnectionFailed, errorValue);
}

TEST_F(MessageParserTests, RegisteredErrorCallbackReceivesConnectionFailedErrorIfWriteBytesEncountersASocketDisconnectionError)
{
    unsigned int dataCount(0);
    unsigned int errorCount(0);
    unsigned int disconnectCount(0);
    unsigned int reconnectCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, writebytes(_,_,_,_))
            .WillByDefault(Invoke([&](void const*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                err = ENOTCONN;
                                errstr = "Testing connection failed";
                                return -1;
                            }));

    ON_CALL(_socket, disconnect())
            .WillByDefault(Invoke([&]() -> int
                            {
                                disconnectCount++;
                                return 0;
                            }));

    ON_CALL(_socket, reconnect(_,_))
            .WillByDefault(Invoke([&](int&, std::string&) -> int
                            {
                                reconnectCount++;
                                _condition.notify_one();
                                return 0;
                            }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) {
        dataCount++;
        _condition.notify_one();
    });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorCount++;
        errorValue = err;
    });

    EXPECT_CALL(_socket, writebytes(_,_,_,_))
            .Times(1);
    EXPECT_CALL(_socket, disconnect())
            .Times(1);
    EXPECT_CALL(_socket, reconnect(_,_))
            .Times(1);

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(5000));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(1, errorCount);
    ASSERT_EQ(DataSocketError::ConnectionFailed, errorValue);
}

TEST_F(MessageParserTests, RegisteredErrorCallbackReceivesUnknownErrorIfReadBytesEncountersAnUnknownSocketFailure)
{
    unsigned int dataCount(0);
    unsigned int errorCount(0);
    unsigned int disconnectCount(0);
    unsigned int reconnectCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, readbytes(_,_,_,_))
            .WillByDefault(Invoke([&](void*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                err = EPERM;
                                errstr = "Testing permissions failed";
                                return -1;
                            }));

    ON_CALL(_socket, disconnect())
            .WillByDefault(Invoke([&]() -> int
                            {
                                disconnectCount++;
                                return 0;
                            }));

    ON_CALL(_socket, reconnect(_,_))
            .WillByDefault(Invoke([&](int&, std::string&) -> int
                            {
                                reconnectCount++;
                                _condition.notify_one();
                                return 0;
                            }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) {
        dataCount++;
        _condition.notify_one();
    });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorCount++;
        errorValue = err;
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, readbytes(_,_,_,_))
            .Times(1);
    EXPECT_CALL(_socket, disconnect())
            .Times(0);
    EXPECT_CALL(_socket, reconnect(_,_))
            .Times(0);

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(5000));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(1, errorCount);
    ASSERT_EQ(DataSocketError::Unknown, errorValue);
}

TEST_F(MessageParserTests, RegisteredErrorCallbackReceivesUnknownErrorIfWriteBytesEncountersAnUnknownSocketFailure)
{
    unsigned int dataCount(0);
    unsigned int errorCount(0);
    unsigned int disconnectCount(0);
    unsigned int reconnectCount(0);
    DataSocketError errorValue(DataSocketError::NoError);

    ON_CALL(_socket, writebytes(_,_,_,_))
            .WillByDefault(Invoke([&](void const*, size_t, int& err, std::string& errstr) -> ssize_t
                            {
                                err = EINVAL;
                                errstr = "Testing invalid argument";
                                return -1;
                            }));

    ON_CALL(_socket, disconnect())
            .WillByDefault(Invoke([&]() -> int
                            {
                                disconnectCount++;
                                return 0;
                            }));

    ON_CALL(_socket, reconnect(_,_))
            .WillByDefault(Invoke([&](int&, std::string&) -> int
                            {
                                reconnectCount++;
                                _condition.notify_one();
                                return 0;
                            }));

    _parser.registerCallback([&](unsigned int, MessageParser::Payload) {
        dataCount++;
        _condition.notify_one();
    });

    _parser.registerErrorCallback([&](DataSocketError err) {
        errorCount++;
        errorValue = err;
        _condition.notify_one();
    });

    EXPECT_CALL(_socket, writebytes(_,_,_,_))
            .Times(1);
    EXPECT_CALL(_socket, disconnect())
            .Times(0);
    EXPECT_CALL(_socket, reconnect(_,_))
            .Times(0);

    _parser.sendCommand(eCOMMAND_CLOSE);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(5000));
    }

    ASSERT_EQ(0, dataCount);
    ASSERT_EQ(1, errorCount);
    ASSERT_EQ(DataSocketError::Unknown, errorValue);
}
