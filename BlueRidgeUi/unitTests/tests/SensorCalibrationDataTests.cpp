#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "providers/SensorCalibrationData.h"

using namespace ::testing;

class SensorCalibrationDataTests : public Test
{

public:
    SensorCalibrationDataTests() {}
    ~SensorCalibrationDataTests() override;
};

SensorCalibrationDataTests::~SensorCalibrationDataTests() {}


TEST_F(SensorCalibrationDataTests, DefaultConstructorDefaultsValuesToZero)
{
    SensorCalibrationData sensorCalData;

    ASSERT_EQ(0, sensorCalData.systolic);
    ASSERT_EQ(0, sensorCalData.diastolic);
    ASSERT_EQ(0, sensorCalData.mean);
    ASSERT_EQ(0, sensorCalData.reference);
    ASSERT_EQ(0, sensorCalData.heartRate);
    ASSERT_EQ(0, sensorCalData.signalStrength);
}


TEST_F(SensorCalibrationDataTests, ConstructorSetsSensorCalibrationDataValuesFromCmemCalibrationValues)
{
    cmem_calibration cmemCalibration;
    cmem_date cmemDate = cmem_date::now();
    std::string dateStr = cmemDate.fmt_date();
    cmemCalibration.setDate(cmemDate);
    cmem_time cmemTime = cmem_time::now();
    std::string timeStr = cmemTime.fmt_time();
    cmemCalibration.setTime(cmemTime);
    cmemCalibration.setSystolic(33);
    cmemCalibration.setDiastolic(25);
    cmemCalibration.setMean(28);
    cmemCalibration.setPaMean(29);
    cmemCalibration.setAvgHeartRate(75);
    cmemCalibration.setAvgSignalStrength(92);
    cmemCalibration.add_field("Notes", "notes");
    cmemCalibration.add_field("WaveformData", "valid_file.xml");

    SensorCalibrationData sensorCalData(cmemCalibration);

    ASSERT_EQ(dateStr, sensorCalData.date);
    ASSERT_EQ(timeStr, sensorCalData.time);
    ASSERT_EQ(cmemCalibration.getSystolic(), sensorCalData.systolic);
    ASSERT_EQ(cmemCalibration.getDiastolic(), sensorCalData.diastolic);
    ASSERT_EQ(cmemCalibration.getMean(), sensorCalData.mean);
    ASSERT_EQ(cmemCalibration.getPaMean(), sensorCalData.reference);
    ASSERT_EQ(cmemCalibration.getAvgHeartRate(), sensorCalData.heartRate);
    ASSERT_EQ(cmemCalibration.getAvgSignalStrength(), sensorCalData.signalStrength);
    ASSERT_EQ("valid_file.xml", sensorCalData.waveformFile);
    ASSERT_EQ("notes", sensorCalData.notes);

}


