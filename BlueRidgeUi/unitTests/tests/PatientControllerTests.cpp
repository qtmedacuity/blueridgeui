#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "unitTests/mocks/MockTransactionProvider.h"
#include "unitTests/mocks/MockPatientProvider.h"
#include "unitTests/mocks/MockUsbProvider.h"
#include "presentation/PatientController.h"
#include "presentation/PatientInfo.h"

using namespace ::testing;

class PatientControllerTests : public Test
{
public:
    PatientControllerTests()
          : _controller(_patientProvider, _usbProvider, _transactionProvider)
    {
        patient = std::shared_ptr<PatientInfo>(new PatientInfo());
        patient->setPatientId(123);
        patient->setFirstName("PtName");
        patient->setLastName("PtLastName");
        patient->setPatientName("Patient Name");
        patient->setDob("03 Mar 2019");
        patient->setImplantDate("03 Mar 2019");
        patient->setPhone("555-555-5555");
        patient->setClinician("AClinician");
        patient->setClinic("AClinic");
        patient->setSerialNumber("ABC123");
        patient->setImplantDoctor("ImplantDr");
        patient->setImplantLocation("right");
        patient->setBaselineCode("baseline");
        patient->setCalCode("calcode");

        _controller.setCurrentPatient(patient.get());
    }

    ~PatientControllerTests() override;

    void SetUp() override
    {
        ON_CALL(_transactionProvider, getTransactionId())
                .WillByDefault(Return(456123789));

        ON_CALL(_patientProvider, getPatientId())
                .WillByDefault(Return(4561230));

        ON_CALL(_usbProvider, getSerialNumber())
                .WillByDefault(Return("TJU487"));

        ON_CALL(_usbProvider, getCalCode())
                .WillByDefault(Return("AA3AA-A3AAA"));

        ON_CALL(_usbProvider, getBaseline())
                .WillByDefault(Return("2AAAA-A568A"));        
    }

    bool PatientInfoEqual(PatientInfo* pt1, PatientInfo* pt2)
    {
        return(pt1->patientId() == pt2->patientId() &&
               pt1->firstName() == pt2->firstName() &&
               pt1->lastName() == pt2->lastName() &&
               pt1->patientName() == pt2->patientName() &&
               pt1->dob() == pt2->dob() &&
               pt1->implantDate() == pt2->implantDate() &&
               pt1->phone() == pt2->phone() &&
               pt1->clinic() == pt2->clinic() &&
               pt1->clinician() == pt2->clinician() &&
               pt1->serialNumber() == pt2->serialNumber() &&
               pt1->implantDoctor() == pt2->implantDoctor() &&
               pt1->implantLocation() == pt2->implantLocation() &&
               pt1->baselineCode() == pt2->baselineCode() &&
               pt1->calCode() == pt2->calCode());
    }
    std::shared_ptr<PatientInfo> patient;

    NiceMock<MockPatientProvider> _patientProvider;
    NiceMock<MockTransactionProvider> _transactionProvider;
    NiceMock<MockUsbProvider> _usbProvider;
    PatientController _controller;
};

PatientControllerTests::~PatientControllerTests() {}

TEST_F(PatientControllerTests, ConstructorRegisterCallbacksWithProviders)
{
    NiceMock<MockPatientProvider> patientProvider;
    NiceMock<MockTransactionProvider> transactionProvider;
    NiceMock<MockUsbProvider> usbProvider;

    EXPECT_CALL(patientProvider, registerPatientSavedCallback(_))
            .Times(1);
    EXPECT_CALL(patientProvider, registerListDataCallback(_))
            .Times(1);
    EXPECT_CALL(transactionProvider, registerTransactionReadyCallback(_))
            .Times(1);
    EXPECT_CALL(transactionProvider, registerCommitTransactionCallback(_))
            .Times(1);
    EXPECT_CALL(usbProvider, registerUsbCallback(_))
            .Times(1);

    PatientController controller(patientProvider, usbProvider, transactionProvider);
}

TEST_F(PatientControllerTests, CurrentPatientReturnsTheCurrentPatientSetForTheController)
{
    ASSERT_TRUE(PatientInfoEqual(patient.get(), _controller.currentPatient()));
}

TEST_F(PatientControllerTests, SetCurrentPatientSetsCurrentPatientOfTheController)
{
    std::shared_ptr<PatientInfo> pt (new PatientInfo());
    pt->setPatientId(123);
    pt->setFirstName("PtName");
    pt->setLastName("PtLastName");
    pt->setDob("03 Mar 2019");
    pt->setImplantDate("03 Mar 2019");
    pt->setPhone("555-555-5555");
    pt->setClinician("AClinician");
    pt->setClinic("AClinic");
    pt->setSerialNumber("ABC123");
    pt->setImplantDoctor("ImplantDr");
    pt->setImplantLocation("ImplantLocation");
    pt->setBaselineCode("baseline");
    pt->setCalCode("calcode");

    _controller.setCurrentPatient(pt.get());
    ASSERT_TRUE(PatientInfoEqual(pt.get(), _controller.currentPatient()));
}

TEST_F(PatientControllerTests, GetUsbDataCallsUsbProviderRequestUsbStatusOnce)
{
    EXPECT_CALL(_usbProvider, requestUsbStatus())
            .Times(1);

    _controller.getUsbData();
}

TEST_F(PatientControllerTests, GetPatientBarInfoReturnsPatientInfoForCurrentPatient)
{
    QString expectedPtInfo ="{\n    "
                            "\"dob\": \"03 Mar 2019\",\n    "
                            "\"patientName\": \"Patient Name\",\n    "
                            "\"serialNumber\": \"ABC123\"\n}\n";

    QVariant ptInfo = _controller.getPatientBarInfo();

    ASSERT_EQ(expectedPtInfo, ptInfo.toString());
}

TEST_F(PatientControllerTests, CreateTransanctionCallsTransactionProviderRequestTransactionCreation)
{
    QString eventType = "NewImplant";
    EXPECT_CALL(_transactionProvider, requestTransactionCreation(_,_))
            .Times(1);

    _controller.createTransaction(eventType);
}

TEST_F(PatientControllerTests, CreateTransanctionCallsTransactionProviderRequestTransactionCreationWithCurrentPatientId)
{
    QString eventType = "NewImplant";
    EXPECT_CALL(_transactionProvider, requestTransactionCreation(patient->patientId(), eventType.toStdString()))
            .Times(1);

    _controller.createTransaction(eventType);
}

TEST_F(PatientControllerTests, CreateTransanctionDoesNotCallsTransactionProviderRequestTransactionCreationIfCurrentPatientNull)
{
    QString eventType = "NewImplant";
    std::shared_ptr<PatientInfo> pt = nullptr;
    _controller.setCurrentPatient(pt.get());

    EXPECT_CALL(_transactionProvider, requestTransactionCreation(_,_))
            .Times(0);

    _controller.createTransaction(eventType);
}

TEST_F(PatientControllerTests, AbandonTransactionCallsTransactionProviderAbandonTransaction)
{
    EXPECT_CALL(_transactionProvider, abandonTransaction(_))
            .Times(1);

    _controller.abandonTransaction();
}

TEST_F(PatientControllerTests, AbandonTransactionCallsTransactionProviderAbandonTransactionWithTransactionId)
{
    EXPECT_CALL(_transactionProvider, abandonTransaction(456123789))
            .Times(1);

    emit _controller.transactionComplete();

    _controller.abandonTransaction();
}

TEST_F(PatientControllerTests, SetSensorPositionUpdatesTheCurrentPatientImplantLocation)
{
    ASSERT_EQ("right", patient->implantLocation());
    _controller.setSensorPosition("left");
    ASSERT_EQ("left", _controller.currentPatient()->implantLocation());
}

MATCHER_P(PatientDataEqual, pt, "")
{
    return (pt.patientId == static_cast<unsigned int>(arg.patientId) &&
    pt.firstName == arg.firstName &&
    pt.middleInitial == arg.middleInitial &&
    pt.lastName == arg.lastName &&
    pt.dob == arg.dob &&
    pt.implantDate == arg.implantDate &&
    pt.phone == arg.phone &&
    pt.clinic == arg.clinic &&
    pt.clinician == arg.clinician &&
    pt.implantDoctor == arg.implantDoctor &&
    pt.serialNumber == arg.serialNumber &&
    pt.implantLocation == arg.implantLocation &&
    pt.calCode == arg.calCode &&
    pt.baselineCode == arg.baselineCode &&
    pt.patientName == arg.patientName &&
    pt.uploadPending == arg.uploadPending);
}

TEST_F(PatientControllerTests, SetSensorPositionCallsPatientProviderSavePatientWithUpdatedPatient)
{
    EXPECT_CALL(_patientProvider, savePatient(_,PatientDataEqual(_controller.currentPatient()->toPatientData())))
            .Times(1);

    _controller.setSensorPosition("right");
}

TEST_F(PatientControllerTests, SetSensorPositionUpdatesCurrentPatientCalCodeAndCallsPatientProviderSavePatient)
{
    _controller.currentPatient()->setCalCode("");

    EXPECT_CALL(_patientProvider, savePatient(_,_))
            .Times(1);

    emit _controller.usbDataUpdated();
    _controller.setSensorPosition("right");

    ASSERT_EQ(_controller.currentPatient()->calCode(), "AA3AA-A3AAA");
    ASSERT_EQ(_controller.currentPatient()->baselineCode(), "2AAAA-A568A");
}

TEST_F(PatientControllerTests, TransactionProviderGetTransactionIdCalledWhenTransactionCompleteSignalIsEmitted)
{
    EXPECT_CALL(_transactionProvider, getTransactionId())
            .Times(1);

    emit _controller.transactionComplete();
    ASSERT_EQ(_controller.getTransactionId(), 456123789);
}

TEST_F(PatientControllerTests, CurrentPatientIdUpdatedWhenPatientDataSavedSignalEmitted)
{
    _controller.currentPatient()->setPatientId(0);

    EXPECT_CALL(_patientProvider, getPatientId())
            .Times(1);

    emit _controller.patientDataSaved();

    ASSERT_EQ(4561230, _controller.currentPatient()->patientId());
}

TEST_F(PatientControllerTests, CurrentPatientIdIsNotUpdatedWhenPatientDataSavedSignalEmittedIfPatientIdAlreadySet)
{
    EXPECT_CALL(_patientProvider, getPatientId())
            .Times(0);

    emit _controller.patientDataSaved();

    ASSERT_EQ(patient->patientId(), _controller.currentPatient()->patientId());
}

TEST_F(PatientControllerTests, UsbProviderFunctionCalledWhenUsbDataUpdatedSignalEmitted)
{
    EXPECT_CALL(_usbProvider, getSerialNumber())
            .Times(1);
    EXPECT_CALL(_usbProvider, getCalCode())
            .Times(1);
    EXPECT_CALL(_usbProvider, getBaseline())
            .Times(1);

    emit _controller.usbDataUpdated();
}

TEST_F(PatientControllerTests, UsbDataSignalIsEmittedAfterUsbDataIsUpdated)
{
    QSignalSpy spy(&_controller, SIGNAL(usbData(QString)));

    emit _controller.usbDataUpdated();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientControllerTests, PatientProviderFunctionCalledWhenRequestListOfImplantDoctors)
{
    std::string listType = "ImplantDoctors";
    EXPECT_CALL(_patientProvider, requestListData(listType))
            .Times(1);

    _controller.requestListOfImplantDoctors();
}

TEST_F(PatientControllerTests, PatientProviderFunctionCalledWhenRequestListOfClinicians)
{
    std::string listType = "Clinicians";
    EXPECT_CALL(_patientProvider, requestListData(listType))
            .Times(1);

    _controller.requestListOfClinicians();
}

TEST_F(PatientControllerTests, PatientProviderFunctionCalledWhenRequestListOfClinics)
{
    std::string listType = "Clinics";
    EXPECT_CALL(_patientProvider, requestListData(listType))
            .Times(1);

    _controller.requestListOfClinics();
}

TEST_F(PatientControllerTests, AddNewPatientSetCurrentPatientCreatesTransaction)
{
    int patientId = 456;
    QString eventType = "NewImplant";
    std::shared_ptr<PatientInfo> pt (new PatientInfo());
    pt->setPatientId(patientId);
    pt->setFirstName("FName");
    pt->setLastName("LName");
    pt->setMiddleInitial("I");
    pt->setDob("03 Mar 2019");
    pt->setImplantDate("03 Mar 2019");
    pt->setPhone("555-555-5555");
    pt->setClinician("Clinician1");
    pt->setClinic("Clinic1");
    pt->setSerialNumber("DEF987");
    pt->setImplantDoctor("ImplantDr");
    pt->setImplantLocation("ImplantLocation");
    pt->setBaselineCode("baseline");
    pt->setCalCode("calcode");

    EXPECT_CALL(_transactionProvider, requestTransactionCreation(patientId, ""))
            .Times(1);

    _controller.addNewPatient(pt.get());
    PatientInfo* curPat = _controller.currentPatient();
    ASSERT_TRUE(PatientInfoEqual(pt.get(), curPat));
    ASSERT_EQ("LName, FName I", curPat->patientName());
}

TEST_F(PatientControllerTests, AddNewPatientCreatesTransactionThenSavesPatientThenCommitsTransaction)
{
    int patientId = 456;
    std::shared_ptr<PatientInfo> pt (new PatientInfo());
    pt->setPatientId(patientId);
    pt->setFirstName("FName");
    pt->setLastName("LName");
    pt->setMiddleInitial("I");
    pt->setPatientName("LName, FName I");
    pt->setDob("03 Mar 2019");
    pt->setImplantDate("03 Mar 2019");
    pt->setPhone("555-555-5555");
    pt->setClinician("Clinician1");
    pt->setClinic("Clinic1");
    pt->setSerialNumber("DEF987");
    pt->setImplantDoctor("ImplantDr");
    pt->setImplantLocation("ImplantLocation");
    pt->setBaselineCode("baseline");
    pt->setCalCode("calcode");

    EXPECT_CALL(_transactionProvider, requestTransactionCreation(patientId, ""))
            .Times(1);
    EXPECT_CALL(_patientProvider, savePatient(456123789, PatientDataEqual(pt->toPatientData())))
            .Times(1);
    EXPECT_CALL(_transactionProvider, commitTransaction(456123789, patientId))
            .Times(1);

    _controller.addNewPatient(pt.get());

    emit _controller.transactionComplete();
    emit _controller.patientDataSaved();
}

TEST_F(PatientControllerTests, GetListOfImplantDoctorsReturnsListOfNames) {

    std::vector<std::string> implantDoctors;
    implantDoctors.push_back("Dr. Who");
    implantDoctors.push_back("Dr. Spock");

    std::string type = "ImplantDoctors";

    ON_CALL(_patientProvider, getListData(type))
            .WillByDefault(Return(implantDoctors));

    _controller.requestListOfImplantDoctors();
    emit _controller.listDataReady();

    QList<QString> listOfDoctors = _controller.getListOfImplantDoctors();
    ASSERT_EQ(2, listOfDoctors.size());
    ASSERT_EQ("Dr. Who", listOfDoctors.at(0));
    ASSERT_EQ("Dr. Spock", listOfDoctors.at(1));
}

TEST_F(PatientControllerTests, GetListOfCliniciansReturnsListOfNames) {

    std::vector<std::string> clinicians;
    clinicians.push_back("Dr. Jones");
    clinicians.push_back("Dr. Watson");

    std::string type = "Clinicians";

    ON_CALL(_patientProvider, getListData(type))
            .WillByDefault(Return(clinicians));

    _controller.requestListOfClinicians();
    emit _controller.listDataReady();

    QList<QString> listOfClinicians = _controller.getListOfClinicians();
    ASSERT_EQ(2, listOfClinicians.size());
    ASSERT_EQ("Dr. Jones", listOfClinicians.at(0));
    ASSERT_EQ("Dr. Watson", listOfClinicians.at(1));
}

TEST_F(PatientControllerTests, GetListOfClinicsReturnsListOfNames) {

    std::vector<std::string> clinics;
    clinics.push_back("Clinic 1");
    clinics.push_back("Clinic 2");

    std::string type = "Clinics";

    ON_CALL(_patientProvider, getListData(type))
            .WillByDefault(Return(clinics));

    _controller.requestListOfClinics();
    emit _controller.listDataReady();

    QList<QString> listOfClinics = _controller.getListOfClinics();
    ASSERT_EQ(2, listOfClinics.size());
    ASSERT_EQ("Clinic 1", listOfClinics.at(0));
    ASSERT_EQ("Clinic 2", listOfClinics.at(1));
}

TEST_F(PatientControllerTests, HandleTransactionCommitReadyEmitsNewPatientSavedFinishedSignal)
{
    _controller.addNewPatient(patient.get());
    QSignalSpy spy(&_controller, SIGNAL(newPatientSavedFinished()));


    emit _controller.transactionCommitFinished();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientControllerTests, TransactionCompleteSignalEmittedWhenProviderExecutesCallback)
{
    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();

    PatientController controller(_patientProvider, _usbProvider, transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(transactionComplete()));

    transactionProvider._fake._transactionReadyCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(PatientControllerTests, TransactionCommitFinishedSignalEmittedWhenProviderExecutesCallback)
{
    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();

    PatientController controller(_patientProvider, _usbProvider, transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(transactionCommitFinished()));

    transactionProvider._fake._commitTransactionCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(PatientControllerTests, PatientDataSavedSignalEmittedWhenProviderExecutesCallback)
{
    NiceMock<MockPatientProvider> patientProvider;
    patientProvider.DelegateToFake();

    PatientController controller(patientProvider, _usbProvider, _transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(patientDataSaved()));

    patientProvider._fake._patientSavedCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(PatientControllerTests, UsbDataSavedSignalEmittedWhenProviderExecutesCallback)
{
    NiceMock<MockUsbProvider> usbProvider;
    usbProvider.DelegateToFake();

    PatientController controller(_patientProvider, usbProvider, _transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(usbDataUpdated()));

    usbProvider._fake._usbCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(PatientControllerTests, ListDataReadySignalEmittedWhenProviderExecutesCallback)
{
    NiceMock<MockPatientProvider> patientProvider;
    patientProvider.DelegateToFake();

    PatientController controller(patientProvider, _usbProvider, _transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(listDataReady()));

    patientProvider._fake._listDataCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(PatientControllerTests, GetPatientIdReturnsPatientIdOfCurrentPatient) {

    ASSERT_EQ(123, _controller.getPatientId());
}
