#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include <QtCharts/QLineSeries>
#include <QtCharts/QScatterSeries>
#include "widgets/WaveChart/WaveChartWidget.h"

using namespace ::testing;
using namespace QtCharts;
class WaveChartWidgetTests : public Test
{
public:
    WaveChartWidgetTests()
        : _series(new QLineSeries()),
          _widget()
    {
        _series->append(0, 6);
        _series->append(2, 4);
        _widget.addSeries(_series);
        _widget.setObjectName("WaveChart");
    }

    ~WaveChartWidgetTests() override;

    QLineSeries* _series;
    WaveChartWidget _widget;
};

WaveChartWidgetTests::~WaveChartWidgetTests() {}

TEST_F(WaveChartWidgetTests, ConstructorSetsDefaultValues)
{
    WaveChartWidget widget;

    ASSERT_EQ(widget.backgroundColor(), Qt::black);
    ASSERT_EQ(widget.plotAreaColor(), Qt::transparent);
    ASSERT_EQ(15.0, widget.verticalPadding());
    ASSERT_EQ(20.0, widget.horizontalPadding());
    ASSERT_EQ(WaveAxis::Orientation::Horizontal, widget.xAxis()->orientation());
    ASSERT_EQ(widget.xAxis()->labelSize(), QPoint(0,0));
    ASSERT_EQ(widget.xAxis()->font(), QFont());
    ASSERT_EQ(widget.xAxis()->backgroundColor(), Qt::transparent);
    ASSERT_EQ(widget.xAxis()->axisColor(), Qt::white);
    ASSERT_EQ(widget.xAxis()->span(), 0.0);
    ASSERT_EQ(widget.xAxis()->lineWeight(), 1);
    ASSERT_EQ(widget.xAxis()->lineEnabled(), true);
    ASSERT_EQ(widget.xAxis()->ticksEnabled(), true);
    ASSERT_EQ(widget.xAxis()->labelsEnabled(), true);
    ASSERT_EQ(widget.xAxis()->gridEnabled(), true);
    ASSERT_EQ(widget.xAxis()->displayMax(), std::numeric_limits<double>::min());
    ASSERT_EQ(widget.xAxis()->displayMin(), std::numeric_limits<double>::max());
    ASSERT_EQ(widget.xAxis()->scrollOffset(), 0.0);
    ASSERT_EQ(WaveAxis::Orientation::Vertical, widget.yAxis()->orientation());
    ASSERT_EQ(widget.yAxis()->labelSize(), QPoint(0,0));
    ASSERT_EQ(widget.yAxis()->font(), QFont());
    ASSERT_EQ(widget.yAxis()->backgroundColor(), Qt::transparent);
    ASSERT_EQ(widget.yAxis()->axisColor(), Qt::white);
    ASSERT_EQ(widget.yAxis()->span(), 0.0);
    ASSERT_EQ(widget.yAxis()->lineWeight(), 1);
    ASSERT_EQ(widget.yAxis()->lineEnabled(), true);
    ASSERT_EQ(widget.yAxis()->ticksEnabled(), true);
    ASSERT_EQ(widget.yAxis()->labelsEnabled(), true);
    ASSERT_EQ(widget.yAxis()->gridEnabled(), true);
    ASSERT_EQ(widget.yAxis()->displayMax(), std::numeric_limits<double>::min());
    ASSERT_EQ(widget.yAxis()->displayMin(), std::numeric_limits<double>::max());
    ASSERT_EQ(widget.yAxis()->scrollOffset(), 0.0);
}

TEST_F(WaveChartWidgetTests, ObjectNameReturnsTheWidgetObjectName)
{
    ASSERT_EQ(_widget.objectName(), "WaveChart");
}

TEST_F(WaveChartWidgetTests, SetObjectNameSetsTheWidgetObjectName)
{
    _widget.setObjectName("Chart");

    ASSERT_EQ(_widget.objectName(), "Chart");
}

TEST_F(WaveChartWidgetTests, SetObjectNameEmitsObjectNameChangedSignal)
{
    QSignalSpy spy(&_widget, SIGNAL(objectNameChanged(QString)));

    _widget.setObjectName("Chart");

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartWidgetTests, backgroundColorReturnsTheWidgetBackgroundColor)
{
    ASSERT_EQ(_widget.backgroundColor(), Qt::black);
}

TEST_F(WaveChartWidgetTests, SetBackgroundColorSetsTheWidgetBackgroundColor)
{
    _widget.setBackgroundColor(Qt::white);

    ASSERT_EQ(_widget.backgroundColor(), Qt::white);
}

TEST_F(WaveChartWidgetTests, SetBackgroundColorEmitsBackgroundColorChangedSignal)
{
    QSignalSpy spy(&_widget, SIGNAL(backgroundColorChanged(QColor)));

    _widget.setBackgroundColor(Qt::red);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartWidgetTests, plotAreaColorReturnsTheWidgetPlotAreaColor)
{
    ASSERT_EQ(_widget.plotAreaColor(), Qt::transparent);
}

TEST_F(WaveChartWidgetTests, SetPlotAreaColorSetsTheWidgetPlotAreaColor)
{
    _widget.setPlotAreaColor(Qt::white);

    ASSERT_EQ(_widget.plotAreaColor(), Qt::white);
}

TEST_F(WaveChartWidgetTests, SetPlotAreaColorEmitsPlotAreaColorChangedSignal)
{
    QSignalSpy spy(&_widget, SIGNAL(plotAreaColorChanged(QColor)));

    _widget.setPlotAreaColor(Qt::red);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartWidgetTests, VerticalPaddingReturnsTheWidgetVerticalPadding)
{
    ASSERT_EQ(_widget.verticalPadding(), 15.0);
}

TEST_F(WaveChartWidgetTests, SetVerticalPaddingSetsTheWidgetVerticalPadding)
{
    _widget.setVerticalPadding(10.0);

    ASSERT_EQ(_widget.verticalPadding(), 10.0);
}

TEST_F(WaveChartWidgetTests, SetVerticalPaddingEmitsVerticalPaddingChangedSignal)
{
    QSignalSpy spy(&_widget, SIGNAL(verticalPaddingChanged(qreal)));

    _widget.setVerticalPadding(12.0);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartWidgetTests, HorizontalPaddingReturnsTheWidgetHorizontalPadding)
{
    ASSERT_EQ(_widget.horizontalPadding(), 20.0);
}

TEST_F(WaveChartWidgetTests, SetHorizontalPaddingSetsTheWidgetHorizontalPadding)
{
    _widget.setHorizontalPadding(30.0);

    ASSERT_EQ(_widget.horizontalPadding(), 30.0);
}

TEST_F(WaveChartWidgetTests, SetHorizontalPaddingEmitsHorizontalPaddingChangedSignal)
{
    QSignalSpy spy(&_widget, SIGNAL(horizontalPaddingChanged(qreal)));

    _widget.setHorizontalPadding(32.0);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WaveChartWidgetTests, XAxisReturnsTheWidgetXAxis)
{
    WaveAxis* axis = _widget.xAxis();

    ASSERT_EQ(axis->orientation(), WaveAxis::Orientation::Horizontal);
    ASSERT_EQ(axis->labelSize(), QPoint(0,0));
    ASSERT_EQ(axis->font(), QFont());
    ASSERT_EQ(axis->backgroundColor(), Qt::transparent);
    ASSERT_EQ(axis->axisColor(), Qt::white);
    ASSERT_EQ(axis->span(), 0.0);
    ASSERT_EQ(axis->lineWeight(), 1);
    ASSERT_EQ(axis->lineEnabled(), true);
    ASSERT_EQ(axis->ticksEnabled(), true);
    ASSERT_EQ(axis->labelsEnabled(), true);
    ASSERT_EQ(axis->gridEnabled(), true);
    ASSERT_EQ(axis->displayMax(), std::numeric_limits<double>::min());
    ASSERT_EQ(axis->displayMin(), std::numeric_limits<double>::max());
    ASSERT_EQ(axis->scrollOffset(), 0.0);
}

TEST_F(WaveChartWidgetTests, YAxisReturnsTheWidgetYAxis)
{
    WaveAxis* axis = _widget.yAxis();

    ASSERT_EQ(axis->orientation(), WaveAxis::Orientation::Vertical);
    ASSERT_EQ(axis->labelSize(), QPoint(0,0));
    ASSERT_EQ(axis->font(), QFont());
    ASSERT_EQ(axis->backgroundColor(), Qt::transparent);
    ASSERT_EQ(axis->axisColor(), Qt::white);
    ASSERT_EQ(axis->span(), 0.0);
    ASSERT_EQ(axis->lineWeight(), 1);
    ASSERT_EQ(axis->lineEnabled(), true);
    ASSERT_EQ(axis->ticksEnabled(), true);
    ASSERT_EQ(axis->labelsEnabled(), true);
    ASSERT_EQ(axis->gridEnabled(), true);
    ASSERT_EQ(axis->displayMax(), std::numeric_limits<double>::min());
    ASSERT_EQ(axis->displayMin(), std::numeric_limits<double>::max());
    ASSERT_EQ(axis->scrollOffset(), 0.0);
}

TEST_F(WaveChartWidgetTests, AddSeriesAddsASeriesToTheWidget)
{
    QLineSeries* series = new QLineSeries();
    series->append(0, 0);
    series->append(1, 1);
    series->append(2, 2);
    _widget.addSeries(series);

    ASSERT_EQ(series, _widget.series(1));
}

TEST_F(WaveChartWidgetTests, AddSeriesDoesNotAddSeriesToWidgetIfSeriesIsANullPtr)
{
    QLineSeries* series = nullptr;

    _widget.addSeries(series);

    ASSERT_EQ(nullptr, _widget.series(1));
}

TEST_F(WaveChartWidgetTests, AddSeriesDoesNotAddSeriesToWidgetIfSeriesIsAlreadyInWidget)
{
    _widget.addSeries(_series);

    ASSERT_EQ(_series, _widget.series(0));
    ASSERT_EQ(nullptr, _widget.series(1));
}

TEST_F(WaveChartWidgetTests, AddSeriesDoesNotAddSeriesToWidgetIfSeriesIsOfLineSeriesType)
{
    QScatterSeries* series = new QScatterSeries();
    series->append(0, 0);
    series->append(1, 1);
    _widget.addSeries(series);

    ASSERT_EQ(_series, _widget.series(0));
    ASSERT_EQ(nullptr, _widget.series(1));
}

TEST_F(WaveChartWidgetTests, RemoverSeriesReturnsNullPtrIfIndexLargerOrEqualToSeriesListCount)
{
    _widget.removeSeries(_series);

    ASSERT_EQ(nullptr, _widget.series(0));
}

TEST_F(WaveChartWidgetTests, RemoverSeriesRemovesTheGivenSeriesFromTheWidget)
{
    QLineSeries* series = new QLineSeries();
    series->append(0, 0);
    series->append(1, 1);
    series->append(2, 2);
    _widget.addSeries(series);

    _widget.removeSeries(_series);

    ASSERT_EQ(series, _widget.series(0));
    ASSERT_NE(_series, _widget.series(0));
}

TEST_F(WaveChartWidgetTests, RemoverSerieDoesNotRemoveSeriesFromTheWidgetIfSeriesIsNullPtr)
{
    QLineSeries* series = new QLineSeries();
    series->append(0, 0);
    series->append(1, 1);
    series->append(2, 2);
    _widget.addSeries(series);

    QLineSeries* series2 = new QLineSeries();
    _widget.removeSeries(series2);

    ASSERT_EQ(_series, _widget.series(0));
    ASSERT_EQ(series, _widget.series(1));
}

TEST_F(WaveChartWidgetTests, RemoverSerieDoesNotREmoveSeriesFromTheWidgetIfSeriesIsNotInWidget)
{
    QLineSeries* series = new QLineSeries();
    series->append(0, 0);
    series->append(1, 1);
    series->append(2, 2);

    _widget.removeSeries(series);

    ASSERT_EQ(_series, _widget.series(0));
    ASSERT_NE(series, _widget.series(1));
}

TEST_F(WaveChartWidgetTests, RemoveAllSeriesFromTheWidget)
{
    QLineSeries* series = new QLineSeries();
    series->append(0, 0);
    series->append(1, 1);
    series->append(2, 2);
    _widget.addSeries(series);

    ASSERT_EQ(_series, _widget.series(0));
    ASSERT_EQ(series, _widget.series(1));

    _widget.removeAllSeries();

    ASSERT_EQ(nullptr, _widget.series(0));
    ASSERT_EQ(nullptr, _widget.series(1));
}

TEST_F(WaveChartWidgetTests, SeriesReturnsTheSeriesAtIndex)
{
    QLineSeries* series =  static_cast<QLineSeries*>(_widget.series(0));

    ASSERT_EQ(_series, series);
}

TEST_F(WaveChartWidgetTests, objectNamePropertyReturnsTheObjectName)
{
    QString name = _widget.property("objectName").toString();

    ASSERT_EQ("WaveChart", name);
}

TEST_F(WaveChartWidgetTests, objectNamePropertySetsTheObjectName)
{
    _widget.setProperty("objectName", "object");

    ASSERT_EQ("object", _widget.objectName());
}

TEST_F(WaveChartWidgetTests, objectNamePropertyHasNotifySignal)
{
    int propertyIndex = _widget.metaObject()->indexOfProperty("objectName");
    QMetaProperty property = _widget.metaObject()->property(propertyIndex);

    ASSERT_STREQ("objectNameChanged", property.notifySignal().name().data());
}

TEST_F(WaveChartWidgetTests, backgroundColorPropertyReturnsTheBackgroundColor)
{
    QColor color = _widget.property("backgroundColor").value<QColor>();

    ASSERT_EQ(color, Qt::black);
}

TEST_F(WaveChartWidgetTests, backgroundColorPropertySetsTheBackgroundColor)
{
    QColor color = Qt::green;
    _widget.setProperty("backgroundColor", color);

    ASSERT_EQ(color, _widget.backgroundColor());
}

TEST_F(WaveChartWidgetTests, backgroundColorPropertyHasNotifySignal)
{
    int propertyIndex = _widget.metaObject()->indexOfProperty("backgroundColor");
    QMetaProperty property = _widget.metaObject()->property(propertyIndex);

    ASSERT_STREQ("backgroundColorChanged", property.notifySignal().name().data());
}

TEST_F(WaveChartWidgetTests, plotAreaColorPropertyReturnsThePlotAreaColor)
{
    QColor color = _widget.property("plotAreaColor").value<QColor>();

    ASSERT_EQ(color, Qt::transparent);
}

TEST_F(WaveChartWidgetTests, plotAreaColorPropertySetsThePlotAreaColor)
{
    QColor color = Qt::white;
    _widget.setProperty("plotAreaColor", color);

    ASSERT_EQ(color, _widget.plotAreaColor());
}

TEST_F(WaveChartWidgetTests, plotAreaColorPropertyHasNotifySignal)
{
    int propertyIndex = _widget.metaObject()->indexOfProperty("plotAreaColor");
    QMetaProperty property = _widget.metaObject()->property(propertyIndex);

    ASSERT_STREQ("plotAreaColorChanged", property.notifySignal().name().data());
}

TEST_F(WaveChartWidgetTests, verticalPaddingPropertyReturnsVerticalPadding)
{
   double verticalPadding = _widget.property("verticalPadding").toDouble();

   ASSERT_EQ(verticalPadding, 15.0);
}

TEST_F(WaveChartWidgetTests, verticalPaddingPropertySetsTheAxisVerticalPadding)
{
    double verticalPadding = 10.0;
    _widget.setProperty("verticalPadding", verticalPadding);

    ASSERT_EQ(verticalPadding, _widget.verticalPadding());
}

TEST_F(WaveChartWidgetTests, verticalPaddingPropertyHasNotifySignal)
{
    int propertyIndex = _widget.metaObject()->indexOfProperty("verticalPadding");
    QMetaProperty property = _widget.metaObject()->property(propertyIndex);

    ASSERT_STREQ("verticalPaddingChanged", property.notifySignal().name().data());
}

TEST_F(WaveChartWidgetTests, horizontalPaddingPropertyReturnsHorizontalPadding)
{
   double horizontalPadding = _widget.property("horizontalPadding").toDouble();

   ASSERT_EQ(horizontalPadding, 20.0);
}

TEST_F(WaveChartWidgetTests, horizontalPaddingPropertySetsTheAxisHorizontalPadding)
{
    double horizontalPadding = 40.0;
    _widget.setProperty("horizontalPadding", horizontalPadding);

    ASSERT_EQ(horizontalPadding, _widget.horizontalPadding());
}

TEST_F(WaveChartWidgetTests, horizontalPaddingPropertyHasNotifySignal)
{
    int propertyIndex = _widget.metaObject()->indexOfProperty("horizontalPadding");
    QMetaProperty property = _widget.metaObject()->property(propertyIndex);

    ASSERT_STREQ("horizontalPaddingChanged", property.notifySignal().name().data());
}

TEST_F(WaveChartWidgetTests, xAxisPropertyReturnsTheXAxis)
{
    WaveAxis* axis = _widget.property("xAxis").value<WaveAxis*>();

    ASSERT_EQ(axis->orientation(), WaveAxis::Orientation::Horizontal);
    ASSERT_EQ(axis->labelSize(), QPoint(0,0));
    ASSERT_EQ(axis->font(), QFont());
    ASSERT_EQ(axis->backgroundColor(), Qt::transparent);
    ASSERT_EQ(axis->axisColor(), Qt::white);
    ASSERT_EQ(axis->span(), 0.0);
    ASSERT_EQ(axis->lineWeight(), 1);
    ASSERT_EQ(axis->lineEnabled(), true);
    ASSERT_EQ(axis->ticksEnabled(), true);
    ASSERT_EQ(axis->labelsEnabled(), true);
    ASSERT_EQ(axis->gridEnabled(), true);
    ASSERT_EQ(axis->displayMax(), std::numeric_limits<double>::min());
    ASSERT_EQ(axis->displayMin(), std::numeric_limits<double>::max());
    ASSERT_EQ(axis->scrollOffset(), 0.0);
}

TEST_F(WaveChartWidgetTests, yAxisPropertyReturnsTheYAxis)
{
    WaveAxis* axis = _widget.property("yAxis").value<WaveAxis*>();

    ASSERT_EQ(axis->orientation(), WaveAxis::Orientation::Vertical);
    ASSERT_EQ(axis->labelSize(), QPoint(0,0));
    ASSERT_EQ(axis->font(), QFont());
    ASSERT_EQ(axis->backgroundColor(), Qt::transparent);
    ASSERT_EQ(axis->axisColor(), Qt::white);
    ASSERT_EQ(axis->span(), 0.0);
    ASSERT_EQ(axis->lineWeight(), 1);
    ASSERT_EQ(axis->lineEnabled(), true);
    ASSERT_EQ(axis->ticksEnabled(), true);
    ASSERT_EQ(axis->labelsEnabled(), true);
    ASSERT_EQ(axis->gridEnabled(), true);
    ASSERT_EQ(axis->displayMax(), std::numeric_limits<double>::min());
    ASSERT_EQ(axis->displayMin(), std::numeric_limits<double>::max());
    ASSERT_EQ(axis->scrollOffset(), 0.0);
}
