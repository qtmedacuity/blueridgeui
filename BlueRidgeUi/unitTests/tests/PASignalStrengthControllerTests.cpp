#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "unitTests/mocks/MockSensorDataProvider.h"
#include "presentation/PASignalStrengthController.h"

using namespace ::testing;

class PASignalStrengthControllerTests : public Test
{
public:
    PASignalStrengthControllerTests()
        : _statusList({{"weak", 41}, {"good", 71}}),
          _noPulseTag("noPulse"),
          _controller(_statusList, _noPulseTag, _provider)
    {}

    ~PASignalStrengthControllerTests();

    SignalStrengthController::StatusValueList _statusList;
    QString _noPulseTag;
    NiceMock<MockSensorDataProvider> _provider;
    PASignalStrengthController _controller;
};

PASignalStrengthControllerTests::~PASignalStrengthControllerTests() {}

TEST_F(PASignalStrengthControllerTests, ConstructorRegistersCallbackWithSensorDataProviderAndExecutesCallbacks)
{
    NiceMock<MockSensorDataProvider> provider;

    EXPECT_CALL(provider, registerSignalStrengthCallback(_))
            .Times(1);

    EXPECT_CALL(provider, registerPulsatilityCallback(_))
            .Times(1);

    EXPECT_CALL(provider, sendPulsatility())
            .Times(1);

    EXPECT_CALL(provider, sendSignalStrength())
            .Times(1);

    PASignalStrengthController controller({{"weak", 41}, {"good", 71}}, "noPulse", provider);
}

TEST_F(PASignalStrengthControllerTests, GetStatusAtReturnsNoPulseTagIfPulsatilityFalseAndPulsatilityOverrideFalse)
{
    QString value = _controller.getStatusAt(90);

    ASSERT_EQ(_noPulseTag, value);
}

TEST_F(PASignalStrengthControllerTests, GetStatusAtReturnsWeakStatusTagForSignalStrength)
{
    QString expectedValue = QString().fromStdString(std::get<0>(_statusList.at(0)));

    _controller.setPulsatilityOverride(true);

    QString value = _controller.getStatusAt(45);


    ASSERT_EQ(value, expectedValue);
}

TEST_F(PASignalStrengthControllerTests, GetStatusAtReturnsGoodStatusTagForSignalStrength)
{
    QString expectedValue = QString().fromStdString(std::get<0>(_statusList.at(1)));

    _controller.setPulsatilityOverride(true);

    QString value = _controller.getStatusAt(71);

    ASSERT_EQ(value, expectedValue);
}

TEST_F(PASignalStrengthControllerTests, PulsatilityReturnsFalseIfProviderSendsFalseForPulsatility)
{
    NiceMock<MockSensorDataProvider> provider;
    provider._fake._pulsatility = false;
    provider.DelegateToFake();

    PASignalStrengthController controller(_statusList, _noPulseTag, provider);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_FALSE(controller.pulsatility());
}

TEST_F(PASignalStrengthControllerTests, PulsatilityReturnsTrueIfProviderSendsTrueForPulsatility)
{
    NiceMock<MockSensorDataProvider> provider;
    provider._fake._pulsatility = true;
    provider.DelegateToFake();

    PASignalStrengthController controller(_statusList, _noPulseTag, provider);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_TRUE(controller.pulsatility());
}

TEST_F(PASignalStrengthControllerTests, PulsatilityOverrideReturnsFalseWhenOverrideSetToFalse)
{
    ASSERT_FALSE(_controller.pulsatilityOverride());
}

TEST_F(PASignalStrengthControllerTests, PulsatilityOverrideReturnsTrueWhenOverrideSetToTrue)
{
    _controller.setPulsatilityOverride(true);
    ASSERT_TRUE(_controller.pulsatilityOverride());
}

TEST_F(PASignalStrengthControllerTests, SetPulasitilityOverrideDoesNotEmitPulsatilitySignalChangedIfValueNotChanged)
{
    QSignalSpy spy(&_controller, SIGNAL(pulsatilityOverrideChanged(bool)));

    _controller.setPulsatilityOverride(false);

    ASSERT_EQ(0, spy.count());
}

TEST_F(PASignalStrengthControllerTests, SetPulasitilityOverrideEmitsPulsatilitySignalChangedIfValueChanged)
{
    QSignalSpy spy(&_controller, SIGNAL(pulsatilityOverrideChanged(bool)));

    _controller.setPulsatilityOverride(true);

    ASSERT_EQ(1, spy.count());
}

TEST_F(PASignalStrengthControllerTests, PulsatilityChangedSignalEmittedWhenPulsatilityValueChanged)
{
    _provider.DelegateToFake();
    _provider._fake._pulsatility = true;

    PASignalStrengthController controller(_statusList, _noPulseTag, _provider);
    QSignalSpy spy(&controller, SIGNAL(pulsatilityChanged(bool)));

    _provider.sendPulsatility();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(PASignalStrengthControllerTests, SetStrengthSetsSignalStrengthToStrengthFromProvider)
{
    _provider.DelegateToFake();
    _provider._fake._signalStrength = 50;

    PASignalStrengthController controller(_statusList, _noPulseTag, _provider);

    _provider.sendSignalStrength();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(50, controller.strength());
}

TEST_F(PASignalStrengthControllerTests, StrengthChangedSignalEmittedWhenSignalStrengthIsChanged)
{
    _provider.DelegateToFake();
    _provider._fake._signalStrength = 50;

    PASignalStrengthController controller(_statusList, _noPulseTag, _provider);
    QSignalSpy spy(&controller, SIGNAL(strengthChanged(int)));

    _provider.sendSignalStrength();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(PASignalStrengthControllerTests, SetStatusSetsSignalStatusWhenProviderSendsSignalStrength)
{
    _provider.DelegateToFake();
    _provider._fake._signalStrength = 45;

    PASignalStrengthController controller(_statusList, _noPulseTag, _provider);
    controller.setPulsatilityOverride(true);

    _provider.sendSignalStrength();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ("weak", controller.status());
}

TEST_F(PASignalStrengthControllerTests, StatusChangedSignalEmittedWhenSignalStrengthIsChanged)
{
    _provider.DelegateToFake();
    _provider._fake._signalStrength = 50;

    PASignalStrengthController controller(_statusList, _noPulseTag, _provider);
    QSignalSpy spy(&controller, SIGNAL(statusChanged(QString)));

    _provider.sendSignalStrength();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(PASignalStrengthControllerTests, PulsatilityPropertyReturnsThePulsatilityValue)
{
    bool value = _controller.property("pulsatility").toBool();
    ASSERT_EQ(value, false);
}

TEST_F(PASignalStrengthControllerTests, PulsatilityPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("pulsatility");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("pulsatilityChanged", property.notifySignal().name().data());
}

TEST_F(PASignalStrengthControllerTests, PulsatilityOverridePropertyReturnsThePulsatilityOverrideValue)
{
    bool value = _controller.property("pulsatilityOverride").toBool();
    ASSERT_EQ(value, false);
}

TEST_F(PASignalStrengthControllerTests, PulsatilityOverridePropertySetsThePulsatilityOverrideValue)
{
    _controller.setProperty("pulsatilityOverride", true);
    ASSERT_EQ(_controller.pulsatilityOverride(), true);
}

TEST_F(PASignalStrengthControllerTests, PulsatilityOverridePropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("pulsatilityOverride");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("pulsatilityOverrideChanged", property.notifySignal().name().data());
}

TEST_F(PASignalStrengthControllerTests, StrengthPropertyReturnsTheSignalStrength)
{
    _provider.DelegateToFake();
    _provider._fake._signalStrength = 50;

    PASignalStrengthController controller(_statusList, _noPulseTag, _provider);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    int strength = controller.property("strength").toInt();

    ASSERT_EQ(strength, 50);
}

TEST_F(PASignalStrengthControllerTests, StrengthPropertySetsTheSignalStrength)
{
    _controller.setProperty("strength", 99);

    ASSERT_EQ(_controller.strength(), 99);
}

TEST_F(PASignalStrengthControllerTests, StrengthPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("strength");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("strengthChanged", property.notifySignal().name().data());
}

TEST_F(PASignalStrengthControllerTests, StatusPropertyReturnsTheSignalStatus)
{
    _provider.DelegateToFake();
    _provider._fake._pulsatility = true;
    _provider._fake._signalStrength = 90;

    PASignalStrengthController controller(_statusList, _noPulseTag, _provider);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    QString status = controller.property("status").toString();

    ASSERT_EQ(status, "good");
}

TEST_F(PASignalStrengthControllerTests, StatusPropertySetsTheSignalStatus)
{
    _controller.setProperty("status", "weak");

    ASSERT_EQ(_controller.status(), "weak");
}

TEST_F(PASignalStrengthControllerTests, strengthPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("status");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("statusChanged", property.notifySignal().name().data());
}

TEST_F(PASignalStrengthControllerTests, PulsatilityChangedSignalEmittedCallsProviderSendSignalStrength)
{
    EXPECT_CALL(_provider, sendSignalStrength())
            .Times(1);

    emit _controller.pulsatilityChanged(true);
}

TEST_F(PASignalStrengthControllerTests, PulsatilityOverrideChangedSignalEmittedCallsProviderSendSignalStrength)
{
    EXPECT_CALL(_provider, sendSignalStrength())
            .Times(1);

    emit _controller.pulsatilityOverrideChanged(true);
}
