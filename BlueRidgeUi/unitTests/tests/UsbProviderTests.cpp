#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmem_command.h>

#include "unitTests/mocks/MockMessageParser.h"
#include "providers/UsbProvider.h"

using namespace ::testing;

class UsbProviderTests : public Test
{
public:
    UsbProviderTests()
    {}

    ~UsbProviderTests() override;

    void SetUp() override
    {
        _usbResponse = "{\n\t\"UsbPresent\" : \"True\","
                        "\n\t\"Valid\":\"TRUE\","
                        "\n\t\"SerialNumber\":\"HJPSUZ\","
                        "\n\t\"CalCode\":\"AA3AA-A3AAA\","
                        "\n\t\"Baseline\":\"2AAAA-A568A\","
                        "\n\t\"Status\" : \"SUCCESS\"\n}\n";
    }

    std::string _usbResponse;
    std::condition_variable _condition;
};

UsbProviderTests::~UsbProviderTests() {}

TEST_F(UsbProviderTests, ConstructorCallsMessageParserRegisterCallback)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();

    EXPECT_CALL(parser, registerCallback(_))
            .Times(1);

    UsbProvider provider(parser);
}

TEST_F(UsbProviderTests, UsbCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count = 0;
    provider.registerUsbCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(UsbProviderTests, AllUsbCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerUsbCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerUsbCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(UsbProviderTests, AllUsbCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerUsbCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerUsbCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterUsbCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}


TEST_F(UsbProviderTests, RequestUsbStatusCallsParserSendCommandWithUsbStatusCommand)
{
    NiceMock<MockMessageParser> parser;
    UsbProvider provider(parser);

    EXPECT_CALL(parser, sendCommand(eGET_USB_STATUS))
            .Times(1);

    provider.requestUsbStatus();
}

TEST_F(UsbProviderTests, IsPresentReturnsTrueIfTheUsbPresentValueFromTheResponseIsTrue)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count = 0;
    provider.registerUsbCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.isPresent());
}

TEST_F(UsbProviderTests, IsPresentReturnsFalseIfTheUsbPresentValueFromTheResponseIsFalse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count = 0;
    provider.registerUsbCallback([&]{ count++; _condition.notify_one(); });

    _usbResponse = "{\n\t\"UsbPresent\" : \"False\","
                    "\n\t\"Valid\":\"TRUE\","
                    "\n\t\"SerialNumber\":\"HJPSUZ\","
                    "\n\t\"CalCode\":\"AA3AA-A3AAA\","
                    "\n\t\"Baseline\":\"2AAAA-A568A\","
                    "\n\t\"Status\" : \"SUCCESS\"\n}\n";
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isPresent());
}

TEST_F(UsbProviderTests, IsValidReturnsTrueIfTheValidValueFromTheResponseIsTrue)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count = 0;
    provider.registerUsbCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.isValid());
}

TEST_F(UsbProviderTests, IsValidReturnsFalseIfTheValidValueFromTheResponseIsFalse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count = 0;
    provider.registerUsbCallback([&]{ count++; _condition.notify_one(); });

    _usbResponse = "{\n\t\"UsbPresent\" : \"True\","
                    "\n\t\"Valid\":\"False\","
                    "\n\t\"SerialNumber\":\"HJPSUZ\","
                    "\n\t\"CalCode\":\"AA3AA-A3AAA\","
                    "\n\t\"Baseline\":\"2AAAA-A568A\","
                    "\n\t\"Status\" : \"SUCCESS\"\n}\n";
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isValid());
}

TEST_F(UsbProviderTests, GetSerialNumberReturnsTheSerialNumberValueFromTheResponse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count = 0;
    provider.registerUsbCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("HJPSUZ", provider.getSerialNumber());
}

TEST_F(UsbProviderTests, GetCalCodeReturnsTheCalCodeValueFromTheResponse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count = 0;
    provider.registerUsbCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("AA3AA-A3AAA", provider.getCalCode());
}

TEST_F(UsbProviderTests, GetBaselineReturnsTheBaselineCodeValueFromTheResponse)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    UsbProvider provider(parser);

    unsigned long count = 0;
    provider.registerUsbCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_usbResponse));
    parser._fake._func(eGET_USB_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("2AAAA-A568A", provider.getBaseline());
}
