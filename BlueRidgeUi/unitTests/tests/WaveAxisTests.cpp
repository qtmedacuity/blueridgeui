#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "widgets/WaveChart/WaveAxis.h"

using namespace ::testing;

class WaveAxisTests : public Test
{
public:
    WaveAxisTests()
        : _points(),
          _rect(QRectF(0.0, 0.0, 1, 1)),
          _line(QLineF(1.0, 2.0, 3.0, 4.0)),
          _labelSize(QPoint(1,2)),
          _font(QFont("Times", 10, QFont::Bold)),
          _axis()
    {
        for(unsigned int i=0; i < 5; ++i)
        {
            WaveAxisPoint point(i, "label" + std::to_string(i));
            _points.push_back(point);
        }
        _axis.setPoints(_points);
        _axis.setRect(_rect);
        _axis.setLine(_line);
        _axis.setLabelSize(_labelSize);
        _axis.setFont(_font);
    }
    ~WaveAxisTests() override;

    WaveAxis::PointList _points;
    QRectF _rect;
    QLineF _line;
    QPoint _labelSize;
    QFont _font;
    WaveAxis _axis;
};

WaveAxisTests::~WaveAxisTests() {}

TEST_F(WaveAxisTests, ConstructorSetsDefaultValues)
{
    WaveAxis axis;

    ASSERT_EQ(axis.points(), WaveAxis::PointList());
    ASSERT_EQ(axis.rect(), QRectF());
    ASSERT_EQ(axis.line(), QLineF());
    ASSERT_EQ(axis.orientation(), WaveAxis::Orientation::Horizontal);
    ASSERT_EQ(axis.labelSize(), QPoint(0,0));
    ASSERT_EQ(axis.font(), QFont());
    ASSERT_EQ(axis.backgroundColor(), Qt::transparent);
    ASSERT_EQ(axis.axisColor(), Qt::white);
    ASSERT_EQ(axis.span(), 0.0);
    ASSERT_EQ(axis.lineWeight(), 1);
    ASSERT_EQ(axis.lineEnabled(), true);
    ASSERT_EQ(axis.ticksEnabled(), true);
    ASSERT_EQ(axis.labelsEnabled(), true);
    ASSERT_EQ(axis.gridEnabled(), true);
    ASSERT_EQ(axis.displayMax(), std::numeric_limits<double>::min());
    ASSERT_EQ(axis.displayMin(), std::numeric_limits<double>::max());
    ASSERT_EQ(axis.scrollOffset(), 0.0);
}

TEST_F(WaveAxisTests, PointsReturnsAVectorOfWaveAxisPoints)
{
    ASSERT_EQ(_axis.points(), _points);
}

TEST_F(WaveAxisTests, SetPointsSetsTheAxisPointListToTheListPassed)
{
    WaveAxis::PointList points({WaveAxisPoint(2, "label")});
    _axis.setPoints(points);

    ASSERT_EQ(points, _axis.points());
}

TEST_F(WaveAxisTests, SetPointsEmitsPointChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(pointsChanged()));
    WaveAxis::PointList points({WaveAxisPoint(2, "label")});
    _axis.setPoints(points);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, SetPointsSetsTheAxisPointListToCorrectList)
{
    _axis.setPoints(WaveAxis::PointList());

    ASSERT_EQ(WaveAxis::PointList(), _axis.points());
}

TEST_F(WaveAxisTests, RectReturnsTheRectSetForTheAxis)
{
    ASSERT_EQ(_rect, _axis.rect());
}

TEST_F(WaveAxisTests, SetRectSetsTheAxisRectToCorrectValue)
{
    QRectF rect(5,6,7,8);

    _axis.setRect(rect);

    ASSERT_EQ(rect, _axis.rect());
}

TEST_F(WaveAxisTests, LineReturnsTheLinetSetForTheAxis)
{
    ASSERT_EQ(_line, _axis.line());
}

TEST_F(WaveAxisTests, SetLineSetsTheAxisLineToCorrectValue)
{
    QLineF line(5,6,7,8);

    _axis.setLine(line);

    ASSERT_EQ(line, _axis.line());
}

TEST_F(WaveAxisTests, OrientationReturnsTheOrientationtSetForTheAxis)
{
    ASSERT_EQ(WaveAxis::Orientation::Horizontal, _axis.orientation());
}

TEST_F(WaveAxisTests, SetOrientationtSetsTheAxisOrientationToCorrectValue)
{
    _axis.setOrientation(WaveAxis::Orientation::Vertical);

    ASSERT_EQ(WaveAxis::Orientation::Vertical, _axis.orientation());
}

TEST_F(WaveAxisTests, LabelSizeReturnsTheLabelSizetSetForTheAxis)
{
    ASSERT_EQ(_labelSize, _axis.labelSize());
}

TEST_F(WaveAxisTests, SetLabelSizeSetsTheAxisLableSizeToCorrectValue)
{
    QPoint size(5,6);

    _axis.setLabelSize(size);

    ASSERT_EQ(size, _axis.labelSize());
}

TEST_F(WaveAxisTests, FontReturnsTheFontSetForTheAxis)
{
    ASSERT_EQ(_font, _axis.font());
}

TEST_F(WaveAxisTests, SetFontSetsTheAxisFontToTheCorrectValue)
{
    QFont font("Helvetica", 8, QFont::Normal);

    _axis.setFont(font);

    ASSERT_EQ(_axis.font(), font);
}

TEST_F(WaveAxisTests, SetFontEmitsFontChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(fontChanged(QFont)));
    QFont font("Helvetica", 8, QFont::Normal);

    _axis.setFont(font);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, BackgroundColorReturnsTheBackgroundColorOfTheAxis)
{
    ASSERT_EQ(_axis.backgroundColor(), Qt::transparent);
}

TEST_F(WaveAxisTests, SetBackgroundColorSetsTheBackgroundColorOfTheAxis)
{
    _axis.setBackgroundColor(Qt::red);

    ASSERT_EQ(_axis.backgroundColor(), Qt::red);
}

TEST_F(WaveAxisTests, SetBackgroundColorEmitsBackgroundColorChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(backgroundColorChanged(QColor)));
    _axis.setBackgroundColor(Qt::red);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisColorReturnsTheColorOfTheAxis)
{
    ASSERT_EQ(_axis.axisColor(), Qt::white);
}

TEST_F(WaveAxisTests, SeAxisColorSetsTheColorOfTheAxis)
{
    _axis.setAxisColor(Qt::red);

    ASSERT_EQ(_axis.axisColor(), Qt::red);
}

TEST_F(WaveAxisTests, SetAxisColorEmitsAxisColorChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(axisColorChanged(QColor)));
    _axis.setAxisColor(Qt::red);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, SpanReturnsTheSpanSetForTheAxis)
{
    ASSERT_EQ(_axis.span(), 0.0);
}

TEST_F(WaveAxisTests, SetSpanSetsTheSpanOfTheAxis)
{
    _axis.setSpan(2.5);
    ASSERT_EQ(_axis.span(), 2.5);
}

TEST_F(WaveAxisTests, SetSpanEmitsSpanChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(spanChanged(qreal)));
    _axis.setSpan(2.5);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, LineWeightnReturnsTheLineWeightSetForTheAxis)
{
    ASSERT_EQ(_axis.lineWeight(), 1);
}

TEST_F(WaveAxisTests, SetLineWeightSetsTheLineWeightOfTheAxis)
{
    _axis.setLineWeight(5);
    ASSERT_EQ(_axis.lineWeight(), 5);
}

TEST_F(WaveAxisTests, SetLineWeightEmitsLineWeightChanged)
{
     QSignalSpy spy(&_axis, SIGNAL(lineWeightChanged(int)));

    _axis.setLineWeight(5);

    ASSERT_EQ(_axis.lineWeight(), 5);
}

TEST_F(WaveAxisTests, LineEnabledReturnsTrueIfLineEnabledForAxis)
{
    ASSERT_TRUE(_axis.lineEnabled());
}

TEST_F(WaveAxisTests, LineEnabledReturnsFalseIfLineDisabledForAxis)
{
    _axis.setLineEnabled(false);
    ASSERT_FALSE(_axis.lineEnabled());
}

TEST_F(WaveAxisTests, SetLineEnabledEmitsLineEnabledChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(lineEnabledChanged(bool)));
    _axis.setLineEnabled(false);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, TicksEnabledReturnsTrueIfTicksEnabledForAxis)
{
    ASSERT_TRUE(_axis.ticksEnabled());
}

TEST_F(WaveAxisTests, TicksEnabledReturnsFalseIfTicksDisabledForAxis)
{
    _axis.setLineEnabled(false);
    ASSERT_FALSE(_axis.lineEnabled());
}

TEST_F(WaveAxisTests, SetTicksEnabledEmitsTicksEnabledChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(ticksEnabledChanged(bool)));
    _axis.setTicksEnabled(false);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, LabelsEnabledReturnsTrueIfLabelsEnabledForAxis)
{
    ASSERT_TRUE(_axis.labelsEnabled());
}

TEST_F(WaveAxisTests, LabelsEnabledReturnsFalseIfLabelsDisabledForAxis)
{
    _axis.setLabelsEnabled(false);
    ASSERT_FALSE(_axis.labelsEnabled());
}

TEST_F(WaveAxisTests, SetLabelsEnabledEmitsLabelsEnabledChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(labelsEnabledChanged(bool)));
    _axis.setLabelsEnabled(false);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, GridEnabledReturnsTrueIfGridEnabledForAxis)
{
    ASSERT_TRUE(_axis.gridEnabled());
}

TEST_F(WaveAxisTests, GridEnabledReturnsFalseIfGridDisabledForAxis)
{
    _axis.setGridEnabled(false);
    ASSERT_FALSE(_axis.gridEnabled());
}

TEST_F(WaveAxisTests, SeGridEnabledEmitsGridEnabledChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(gridEnabledChanged(bool)));
    _axis.setGridEnabled(false);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, DisplayMaxReturnsMaxSetForAxis)
{
    ASSERT_EQ(std::numeric_limits<double>::min(), _axis.displayMax());
}

TEST_F(WaveAxisTests, SetDisplayMaxSetsMaxForAxis)
{
    _axis.setDisplayMax(199.99);

    ASSERT_EQ(199.99, _axis.displayMax());
}

TEST_F(WaveAxisTests, SetDisplayMaxEmitsDisplayMaxChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(displayMaxChanged(qreal)));
    _axis.setDisplayMax(199.99);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, DisplayMinReturnsMaxSetForAxis)
{
    ASSERT_EQ(std::numeric_limits<double>::max(), _axis.displayMin());
}

TEST_F(WaveAxisTests, SetDisplayMinSetsMinForAxis)
{
    _axis.setDisplayMin(1.99);

    ASSERT_EQ(1.99, _axis.displayMin());
}

TEST_F(WaveAxisTests, SetDisplayMinEmitsDisplayMinChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(displayMinChanged(qreal)));
    _axis.setDisplayMin(1.99);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, ScrollOffsetReturnsTheScrollOffsetOfTheAxis)
{
    ASSERT_EQ(_axis.scrollOffset(), 0.0);
}

TEST_F(WaveAxisTests, SetScrollOffsetSetsTheAxisScrollOffset)
{
    _axis.setScrollOffset(3.0);

    ASSERT_EQ(3.0, _axis.scrollOffset());
}

TEST_F(WaveAxisTests, SetScrollOffsetEmitsScrollOffsetChangedSignal)
{
    QSignalSpy spy(&_axis, SIGNAL(scrollOffsetChanged(qreal)));
    _axis.setScrollOffset(3.0);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, fontPropertyReturnsTheFont)
{
    QFont font = _axis.property("font").value<QFont>();

    ASSERT_EQ(_font, font);
}

TEST_F(WaveAxisTests, fontPropertySetsTheFont)
{
    QFont font("Helvetica", 8, QFont::Normal);
    _axis.setProperty("font", font);

    ASSERT_EQ(_axis.font(), font);
}

TEST_F(WaveAxisTests, fontPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("font");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("fontChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, backgroundColorPropertyReturnsTheBackgroundColor)
{
    QColor expectedColor = Qt::transparent;
    QColor color = _axis.property("backgroundColor").value<QColor>();

    ASSERT_EQ(expectedColor, color);
}

TEST_F(WaveAxisTests, backgroundColorPropertySetsTheBackgroundColor)
{
    QColor color = Qt::green;
    _axis.setProperty("backgroundColor", color);

    ASSERT_EQ(color, _axis.backgroundColor());
}

TEST_F(WaveAxisTests, backgroundColorPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("backgroundColor");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("backgroundColorChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, axisColorPropertyReturnsTheAxisColor)
{
    QColor expectedColor = Qt::white;
    QColor color = _axis.property("axisColor").value<QColor>();

    ASSERT_EQ(expectedColor, color);
}

TEST_F(WaveAxisTests, axisColorPropertySetsTheBackgroundColor)
{
    QColor color = Qt::green;
    _axis.setProperty("axisColor", color);

    ASSERT_EQ(color, _axis.axisColor());
}

TEST_F(WaveAxisTests, axisColorPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("axisColor");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("axisColorChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, spanPropertyReturnsTheAxisSpan)
{
   double span = _axis.property("span").toDouble();

   ASSERT_EQ(span, 0.0);
}

TEST_F(WaveAxisTests, spanPropertySetsTheAxisSpan)
{
    double span = 1.2;
    _axis.setProperty("span", span);

    ASSERT_EQ(span, _axis.span());
}

TEST_F(WaveAxisTests, spanPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("span");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("spanChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, lineWeightPropertyReturnsTheAxisLineWeight)
{
   int lineWeight = _axis.property("lineWeight").toInt();

   ASSERT_EQ(lineWeight, 1);
}

TEST_F(WaveAxisTests, lineWeightPropertySetsTheAxisLineWeight)
{
    int lineWeight = 5;
    _axis.setProperty("lineWeight", lineWeight);

    ASSERT_EQ(lineWeight, _axis.lineWeight());
}

TEST_F(WaveAxisTests, lineWeightPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("lineWeight");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("lineWeightChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, lineEnabledPropertyReturnsTheAxisLineEnabled)
{
   bool enabled = _axis.property("lineEnabled").toBool();

   ASSERT_EQ(enabled, true);
}

TEST_F(WaveAxisTests, lineEnabledPropertySetsTheAxisLineEnabled)
{
    _axis.setProperty("lineEnabled", false);

    ASSERT_EQ(false, _axis.lineEnabled());
}

TEST_F(WaveAxisTests, lineEnabledPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("lineEnabled");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("lineEnabledChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, ticksEnabledPropertyReturnsTheAxisTicksEnabled)
{
   bool enabled = _axis.property("ticksEnabled").toBool();

   ASSERT_EQ(enabled, true);
}

TEST_F(WaveAxisTests, ticksEnabledPropertySetsTheAxisTicksEnabled)
{
    _axis.setProperty("ticksEnabled", false);

    ASSERT_EQ(false, _axis.ticksEnabled());
}

TEST_F(WaveAxisTests, ticksEnabledPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("ticksEnabled");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("ticksEnabledChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, labelsEnabledPropertyReturnsTheAxisLabelsEnabled)
{
   bool enabled = _axis.property("labelsEnabled").toBool();

   ASSERT_EQ(enabled, true);
}

TEST_F(WaveAxisTests, labelsEnabledPropertySetsTheAxisLabelsEnabled)
{
    _axis.setProperty("labelsEnabled", false);

    ASSERT_EQ(false, _axis.labelsEnabled());
}

TEST_F(WaveAxisTests, labelsEnabledPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("labelsEnabled");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("labelsEnabledChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, gridEnabledPropertyReturnsTheAxisGridEnabled)
{
   bool enabled = _axis.property("gridEnabled").toBool();

   ASSERT_EQ(enabled, true);
}

TEST_F(WaveAxisTests, gridEnabledPropertySetsTheAxisGridEnabled)
{
    _axis.setProperty("gridEnabled", false);

    ASSERT_EQ(false, _axis.gridEnabled());
}

TEST_F(WaveAxisTests, gridEnabledPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("gridEnabled");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("gridEnabledChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, displayMaxPropertyReturnsTheAxisDisplayMax)
{
    double max = std::numeric_limits<double>::min();
    double displayMax = _axis.property("displayMax").toDouble();

    ASSERT_EQ(displayMax, max);
}

TEST_F(WaveAxisTests, displayMaxPropertySetsTheAxisDisplayMax)
{
    double displayMax = 1.2;
    _axis.setProperty("displayMax", displayMax);

    ASSERT_EQ(displayMax, _axis.displayMax());
}

TEST_F(WaveAxisTests, displayMaxPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("displayMax");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("displayMaxChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, displayMinPropertyReturnsTheAxisDisplayMin)
{
    double min = std::numeric_limits<double>::max();
    double displayMin = _axis.property("displayMin").toDouble();

    ASSERT_EQ(displayMin, min);
}

TEST_F(WaveAxisTests, displayMinPropertySetsTheAxisDisplayMin)
{
    double displayMin = 122.2;
    _axis.setProperty("displayMin", displayMin);

    ASSERT_EQ(displayMin, _axis.displayMin());
}

TEST_F(WaveAxisTests, displayMinPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("displayMin");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("displayMinChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, scrollOffsetPropertyReturnsTheAxisScrollOffset)
{
    double scrollOffset = _axis.property("scrollOffset").toDouble();

    ASSERT_EQ(scrollOffset, 0.0);
}

TEST_F(WaveAxisTests, scrollOffsetPropertySetsTheAxisScrollOffset)
{
    double scrollOffset = 3.5;
    _axis.setProperty("scrollOffset", scrollOffset);

    ASSERT_EQ(scrollOffset, _axis.scrollOffset());
}

TEST_F(WaveAxisTests, scrollOffsetPropertyHasNotifySignal)
{
    int propertyIndex = _axis.metaObject()->indexOfProperty("scrollOffset");
    QMetaProperty property = _axis.metaObject()->property(propertyIndex);

    ASSERT_STREQ("scrollOffsetChanged", property.notifySignal().name().data());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenFontChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.fontChanged(QFont());

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenBackgroundColorChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.backgroundColorChanged(QColor());

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenAxisColorChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.axisColorChanged(QColor());

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenSpanChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.spanChanged(0.0);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenLineWeightChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.lineWeightChanged(0);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenLineEnabledChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.lineEnabledChanged(true);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenTicksEnabledChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.ticksEnabledChanged(true);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenLabelsEnabledChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.labelsEnabledChanged(true);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenGridEnabledChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.gridEnabledChanged(true);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenDisplayMaxChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.displayMaxChanged(0.0);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenDisplayMinChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.displayMinChanged(0.0);

    ASSERT_EQ(1, spy.count());
}

TEST_F(WaveAxisTests, AxisEmitUpdatedSignalWhenScrollOffsetChangedSignalEmitted)
{
    QSignalSpy spy(&_axis, SIGNAL(updated()));

    emit _axis.scrollOffsetChanged(0.0);

    ASSERT_EQ(1, spy.count());
}
