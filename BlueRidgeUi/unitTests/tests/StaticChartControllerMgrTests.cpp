#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "presentation/StaticChartControllerMgr.h"
#include "presentation/StaticChartController.h"
#include <exception>

using namespace ::testing;

class StaticChartControllerMgrTests : public Test
{
public:
    StaticChartControllerMgrTests()
        : _resource("./testAxisConfig.json")
    {}

    ~StaticChartControllerMgrTests() override;

    QString _resource;
};

StaticChartControllerMgrTests::~StaticChartControllerMgrTests()
{}

TEST_F(StaticChartControllerMgrTests, ConstructorDoesNotThrowExceptionOnGoodInput)
{
    EXPECT_NO_THROW({
                        StaticChartControllerMgr manager(_resource);
                    });
}

TEST_F(StaticChartControllerMgrTests, ConstructorDoesNotThrowExceptionOnBadInput)
{
    EXPECT_NO_THROW({
                        StaticChartControllerMgr manager(QString("./garbage.txt"));
                    });
}

TEST_F(StaticChartControllerMgrTests, ControllerDoesNotThrowExceptionWithGoodResourceFile)
{
    StaticChartControllerMgr manager(_resource);
    EXPECT_NO_THROW({ manager.controller(QString()); });
}

TEST_F(StaticChartControllerMgrTests, ControllerDoesThrowExceptionWithBadResourceFile)
{
    StaticChartControllerMgr manager(QString("./garbage.txt"));
    EXPECT_THROW(manager.controller(QString()), std::runtime_error);
}

TEST_F(StaticChartControllerMgrTests, ControllerReturnsAPointerToStaticChartControllerForGivenFilename)
{
    StaticChartController* ctrlPtr(nullptr);
    StaticChartControllerMgr manager(_resource);
    EXPECT_NO_THROW({ ctrlPtr = manager.controller(QString("./A/Wave/Chart/File.xml")); });
    ASSERT_NE(ctrlPtr, nullptr);
}

TEST_F(StaticChartControllerMgrTests, ControllerReturnsDifferentStaticChartControllersForDifferentFilenames)
{
    StaticChartController* ctrlPtr1(nullptr);
    StaticChartController* ctrlPtr2(nullptr);
    StaticChartControllerMgr manager(_resource);
    EXPECT_NO_THROW({ ctrlPtr1 = manager.controller(QString("./A/Wave/Chart/File.xml")); });
    ASSERT_NE(ctrlPtr1, nullptr);
    EXPECT_NO_THROW({ ctrlPtr2 = manager.controller(QString("./A/Different/Wave/Chart/File.xml")); });
    ASSERT_NE(ctrlPtr2, nullptr);
    ASSERT_NE(ctrlPtr1, ctrlPtr2);
}

TEST_F(StaticChartControllerMgrTests, ControllerReturnsSameStaticChartControllersForSameFilenames)
{
    StaticChartController* ctrlPtr1(nullptr);
    StaticChartController* ctrlPtr2(nullptr);
    StaticChartController* ctrlPtr3(nullptr);
    StaticChartControllerMgr manager(_resource);
    EXPECT_NO_THROW({ ctrlPtr1 = manager.controller(QString("./A/Wave/Chart/File.xml")); });
    ASSERT_NE(ctrlPtr1, nullptr);
    EXPECT_NO_THROW({ ctrlPtr2 = manager.controller(QString("./A/Different/Wave/Chart/File.xml")); });
    ASSERT_NE(ctrlPtr2, nullptr);
    ASSERT_NE(ctrlPtr1, ctrlPtr2);
    EXPECT_NO_THROW({ ctrlPtr3 = manager.controller(QString("./A/Wave/Chart/File.xml")); });
    ASSERT_NE(ctrlPtr3, nullptr);
    ASSERT_NE(ctrlPtr2, ctrlPtr3);
    ASSERT_EQ(ctrlPtr1, ctrlPtr3);
}

TEST_F(StaticChartControllerMgrTests, ManagerManagesStaticChartControllerLifetimesByMakingItselfControllersParent)
{
    StaticChartController* ctrlPtr(nullptr);
    StaticChartControllerMgr manager(_resource);
    EXPECT_NO_THROW({ ctrlPtr = manager.controller(QString("./A/Wave/Chart/File.xml")); });
    ASSERT_NE(ctrlPtr, nullptr);
    ASSERT_EQ(&manager, ctrlPtr->parent());
}
