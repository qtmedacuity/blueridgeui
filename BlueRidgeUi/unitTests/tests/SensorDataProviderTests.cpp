#include "unitTests/mocks/MockSensorDataParser.h"
#include "providers/SensorDataProvider.h"

#include <mutex>
#include <condition_variable>

using namespace ::testing;

class SensorDataProviderTests : public Test
{
public:
    SensorDataProviderTests()
        : _data(new ISensorDataParser::DataList())
    {
        DataReading_t data;
        data.CM_Mean = 26;
        data.pressure = 19.8f;
        data.heartRate = 78;
        data.CM_Systolic = 38;
        data.CM_Diastolic = 19;
        data.CM_Pulsatility = true;
        data.signalStrength = 92;

        _data.get()->push_back(data);
    }

    ~SensorDataProviderTests();

    std::condition_variable _condition;
    ISensorDataParser::DataListPtr _data;
};

SensorDataProviderTests::~SensorDataProviderTests() {}

TEST_F(SensorDataProviderTests, ConstructorRegisterDataCallbackWithSensorDataParser)
{
    NiceMock<MockSensorDataParser> parser;

    EXPECT_CALL(parser, registerDataCallback(_))
            .Times(1);

    SensorDataProvider provider(parser);
}

TEST_F(SensorDataProviderTests, RegisterDataRateCallbackCallsParserRegisterDataRateCallback)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider::UIntCallback cb;

    EXPECT_CALL(parser, registerDataRateCallback(_))
            .Times(1);

    SensorDataProvider provider(parser);

    provider.registerDataRateCallback(cb);
}

TEST_F(SensorDataProviderTests, RegisterDataRateCallbackReturnsFunctionIdForCallback)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    ON_CALL(parser, registerDataRateCallback(_))
            .WillByDefault(Return(2));

    SensorDataProvider::UIntCallback cb;
    unsigned long result = provider.registerDataRateCallback(cb);

    ASSERT_EQ(2, result);
}

TEST_F(SensorDataProviderTests, RegisterDataRateCallbackReturnsTwoFunctionIdsForCallbacksOfSameType)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    ON_CALL(parser, registerDataRateCallback(_))
            .WillByDefault(Return(1));

    SensorDataProvider::UIntCallback cb1 = [&](unsigned int){};
    unsigned long result = provider.registerDataRateCallback(cb1);

    ASSERT_EQ(1, result);

    ON_CALL(parser, registerDataRateCallback(_))
            .WillByDefault(Return(2));

    SensorDataProvider::UIntCallback cb2= [&](unsigned int){};
    result = provider.registerDataRateCallback(cb2);

    ASSERT_EQ(2, result);
}

TEST_F(SensorDataProviderTests, UnregisterDataRateCallbackCallsParserUnregisterDataRateCallback)
{
    NiceMock<MockSensorDataParser> parser;

    unsigned long funcId = 1;
    EXPECT_CALL(parser, unregisterDataRateCallback(funcId))
            .Times(1);

    SensorDataProvider provider(parser);
    provider.unregisterDataRateCallback(funcId);
}

TEST_F(SensorDataProviderTests, RegisterSensorDataPointCallbackRegisterCallbackAndReturnsCorrectFunctionId)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::ULongDoubleCallback cb = [&](unsigned long, double){};
    unsigned long result = provider.registerSensorDataPointCallback(cb);

    ASSERT_EQ(0, result);
}

TEST_F(SensorDataProviderTests, RegisterSensorDataPointCallbackRegisterCallbacksAndReturnsCorrectFunctionIds)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::ULongDoubleCallback cb1 = [&](unsigned long, double){};
    unsigned long result = provider.registerSensorDataPointCallback(cb1);

    ASSERT_EQ(0, result);

    SensorDataProvider::ULongDoubleCallback cb2 = [&](unsigned long, double){};
    result = provider.registerSensorDataPointCallback(cb2);

    ASSERT_EQ(1, result);
}

TEST_F(SensorDataProviderTests, RegisteredSensorDataPointCallbackIsExecutedWhenParserExecutesADataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 0;
    SensorDataProvider::ULongDoubleCallback cb = [&](unsigned long, double){count++; _condition.notify_all(); };
    provider.registerSensorDataPointCallback(cb);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}

TEST_F(SensorDataProviderTests, RegisteredSensorDataPointCallbackIsExecutedWithDataFromDataReading)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned long sequenceNumber = 0;
    double pressure = 0.0;
    SensorDataProvider::ULongDoubleCallback cb = [&](unsigned long sn, double p){ sequenceNumber = sn; pressure = p; _condition.notify_one(); };
    provider.registerSensorDataPointCallback(cb);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(_data.get()->back().sequenceNumber, sequenceNumber);
    ASSERT_EQ(_data.get()->back().pressure, pressure);
}

TEST_F(SensorDataProviderTests, UnregisterSensorDataPointCallbackIsNotExecutedWhenParserExecutesDataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count1 = 1;
    SensorDataProvider::ULongDoubleCallback cb = [&](unsigned long, double){ count1++; };
    unsigned long funcId = provider.registerSensorDataPointCallback(cb);

    unsigned int count2 = 2;
    SensorDataProvider::ULongDoubleCallback cb2 = [&](unsigned long, double){count2++; _condition.notify_all(); };
    provider.registerSensorDataPointCallback(cb2);

    provider.unregisterSensorDataPointCallback(funcId);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(count1, 1);
    ASSERT_EQ(count2, 3);
}

TEST_F(SensorDataProviderTests, RegisterSignalStrengthCallbackRegisterCallbackAndReturnsCorrectFunctionId)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::UIntCallback cb = [&](unsigned int){};
    unsigned long result = provider.registerSignalStrengthCallback(cb);

    ASSERT_EQ(0, result);
}

TEST_F(SensorDataProviderTests, RegisterSignalStrengthCallbackRegisterCallbacksAndReturnsCorrectFunctionIds)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::UIntCallback cb1 = [&](unsigned int){};
    unsigned long result = provider.registerSignalStrengthCallback(cb1);

    ASSERT_EQ(0, result);

    SensorDataProvider::UIntCallback cb2 = [&](unsigned int){};
    result = provider.registerSignalStrengthCallback(cb2);

    ASSERT_EQ(1, result);
}

TEST_F(SensorDataProviderTests, RegisteredSignalStrengthCallbackIsExecutedWhenParserExecutesADataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 1;
    SensorDataProvider::UIntCallback cb = [&](unsigned int){count++; _condition.notify_all(); };
    provider.registerSignalStrengthCallback(cb);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, RegisteredSignalStrengthCallbackIsExecutedWithDataFromDataReading)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int signalStrength = 0;
    SensorDataProvider::UIntCallback cb = [&](unsigned int ss){ signalStrength = ss; _condition.notify_one(); };
    provider.registerSignalStrengthCallback(cb);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(_data.get()->back().signalStrength, signalStrength);
}

TEST_F(SensorDataProviderTests, UnregisterSignalStrengthCallbackIsNotExecutedWhenParserExecutesDataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count1 = 1;
    SensorDataProvider::UIntCallback cb = [&](unsigned int){ count1++; };
    unsigned long funcId = provider.registerSignalStrengthCallback(cb);

    unsigned int count2 = 0;
    SensorDataProvider::UIntCallback cb2 = [&](unsigned int){count2++; _condition.notify_all(); };
    provider.registerSignalStrengthCallback(cb2);

    provider.unregisterSignalStrengthCallback(funcId);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(count1, 1);
    ASSERT_EQ(count2, 1);
}

TEST_F(SensorDataProviderTests, RegisterMeanValueCallbackRegisterCallbackAndReturnsCorrectFunctionId)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::UIntCallback cb = [&](unsigned int){};
    unsigned long result = provider.registerMeanValueCallback(cb);

    ASSERT_EQ(0, result);
}

TEST_F(SensorDataProviderTests, RegisterMeanValueCallbackRegisterCallbacksAndReturnsCorrectFunctionIds)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::UIntCallback cb1 = [&](unsigned int){};
    unsigned long result = provider.registerMeanValueCallback(cb1);

    ASSERT_EQ(0, result);

    SensorDataProvider::UIntCallback cb2 = [&](unsigned int){};
    result = provider.registerMeanValueCallback(cb2);

    ASSERT_EQ(1, result);
}

TEST_F(SensorDataProviderTests, RegisteredMeanValueCallbackIsExecutedWhenParserExecutesADataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    parser.registerDataRateCallback([&](unsigned int){_condition.notify_all(); });
    parser._fake._dataRateCb(10);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    unsigned int count = 1;
    provider.registerMeanValueCallback([&](unsigned int){count++; });
    parser._fake._dataCb(_data);
    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, RegisteredMeanValueCallbackIsExecutedWithDataFromDataReading)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    parser.registerDataRateCallback([&](unsigned int){ _condition.notify_all(); });
    parser._fake._dataRateCb(10);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    unsigned int meanValue = 0;
    SensorDataProvider::UIntCallback cb = [&](unsigned int mv){ meanValue = mv; };
    provider.registerMeanValueCallback(cb);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(_data.get()->back().CM_Mean, meanValue);
}

TEST_F(SensorDataProviderTests, UnregisterMeanValueCallbackIsNotExecutedWhenParserExecutesDataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    parser.registerDataRateCallback([&](unsigned int){ _condition.notify_all(); });
    parser._fake._dataRateCb(10);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    unsigned int count1 = 1;
    SensorDataProvider::UIntCallback cb = [&](unsigned int){ count1++; };
    unsigned long funcId = provider.registerMeanValueCallback(cb);

    unsigned int count2 = 1;
    SensorDataProvider::UIntCallback cb2 = [&](unsigned int){count2++; _condition.notify_all(); };
    provider.registerMeanValueCallback(cb2);

    provider.unregisterMeanValueCallback(funcId);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(count1, 1);
    ASSERT_EQ(count2, 2);
}


TEST_F(SensorDataProviderTests, RegisterHeartRateCallbackRegisterCallbackAndReturnsCorrectFunctionId)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::UIntCallback cb = [&](unsigned int){};
    unsigned long result = provider.registerHeartRateCallback(cb);

    ASSERT_EQ(0, result);
}

TEST_F(SensorDataProviderTests, RegisterHeartRateCallbackRegisterCallbacksAndReturnsCorrectFunctionIds)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::UIntCallback cb1 = [&](unsigned int){};
    unsigned long result = provider.registerHeartRateCallback(cb1);

    ASSERT_EQ(0, result);

    SensorDataProvider::UIntCallback cb2 = [&](unsigned int){};
    result = provider.registerHeartRateCallback(cb2);

    ASSERT_EQ(1, result);
}

TEST_F(SensorDataProviderTests, RegisteredHeartRateCallbackIsExecutedWhenParserExecutesADataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 1;
    provider.registerHeartRateCallback([&](unsigned int){count++; });
    parser._fake._dataCb(_data);
    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, RegisteredHeartRateCallbackIsExecutedWithDataFromDataReading)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int heartRate = 0;
    SensorDataProvider::UIntCallback cb = [&](unsigned int hr){ heartRate = hr; };
    provider.registerHeartRateCallback(cb);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(_data.get()->back().heartRate, heartRate);
}

TEST_F(SensorDataProviderTests, UnregisterHeartRateCallbackIsNotExecutedWhenParserExecutesDataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count1 = 1;
    SensorDataProvider::UIntCallback cb = [&](unsigned int){ count1++; };
    unsigned long funcId = provider.registerHeartRateCallback(cb);

    unsigned int count2 = 1;
    SensorDataProvider::UIntCallback cb2 = [&](unsigned int){count2++; _condition.notify_all(); };
    provider.registerHeartRateCallback(cb2);

    provider.unregisterHeartRateCallback(funcId);

    _data.get()->back().heartRate = 45;
    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(count1, 1);
    ASSERT_EQ(count2, 2);
}

TEST_F(SensorDataProviderTests, RegisterPulsatilityCallbackRegisterCallbackAndReturnsCorrectFunctionId)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::BoolCallback cb = [&](bool){};
    unsigned long result = provider.registerPulsatilityCallback(cb);

    ASSERT_EQ(0, result);
}

TEST_F(SensorDataProviderTests, RegisterPulsatilityCallbackRegisterCallbacksAndReturnsCorrectFunctionIds)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::BoolCallback cb1 = [&](bool){};
    unsigned long result = provider.registerPulsatilityCallback(cb1);

    ASSERT_EQ(0, result);

    SensorDataProvider::BoolCallback cb2 = [&](bool){};
    result = provider.registerPulsatilityCallback(cb2);

    ASSERT_EQ(1, result);
}

TEST_F(SensorDataProviderTests, RegisteredPulsatilityCallbackIsExecutedWhenParserExecutesADataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 1;
    provider.registerPulsatilityCallback([&](bool){count++; });
    parser._fake._dataCb(_data);
    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, RegisteredPulsatilityCallbackIsExecutedWithDataFromDataReading)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    bool pulsatility = false;
    SensorDataProvider::BoolCallback cb = [&](bool p){ pulsatility = p; };
    provider.registerPulsatilityCallback(cb);

    _data.get()->back().CM_Pulsatility = true;
    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_TRUE(pulsatility);
    _data.get()->back().CM_Pulsatility = false;
    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_FALSE(pulsatility);
}

TEST_F(SensorDataProviderTests, UnregisterPulsatilityCallbackIsNotExecutedWhenParserExecutesDataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count1 = 1;
    SensorDataProvider::BoolCallback cb = [&](bool){ count1++; };
    unsigned long funcId = provider.registerPulsatilityCallback(cb);

    unsigned int count2 = 1;
    SensorDataProvider::BoolCallback cb2 = [&](bool){count2++; _condition.notify_all(); };
    provider.registerPulsatilityCallback(cb2);

    provider.unregisterPulsatilityCallback(funcId);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(count1, 1);
    ASSERT_EQ(count2, 2);
}

TEST_F(SensorDataProviderTests, RegisterSystolicValuesCallbackRegisterCallbackAndReturnsCorrectFunctionId)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::UIntUIntCallback cb = [&](unsigned int, unsigned int){};
    unsigned long result = provider.registerSystolicValuesCallback(cb);

    ASSERT_EQ(0, result);
}

TEST_F(SensorDataProviderTests, RegisterSystolicValuesCallbackRegisterCallbacksAndReturnsCorrectFunctionIds)
{
    NiceMock<MockSensorDataParser> parser;
    SensorDataProvider provider(parser);

    SensorDataProvider::UIntUIntCallback cb1 = [&](unsigned int, unsigned int){};
    unsigned long result = provider.registerSystolicValuesCallback(cb1);

    ASSERT_EQ(0, result);

    SensorDataProvider::UIntUIntCallback cb2 = [&](unsigned int, unsigned int){};
    result = provider.registerSystolicValuesCallback(cb2);

    ASSERT_EQ(1, result);
}

TEST_F(SensorDataProviderTests, RegisteredSystolicValuesCallbackIsExecutedWhenParserExecutesADataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 1;
    provider.registerSystolicValuesCallback([&](unsigned int, unsigned int){count++; });
    parser._fake._dataCb(_data);
    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, RegisteredSystolicValuesCallbackIsExecutedWithDataFromDataReading)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int systolic = 0;
    unsigned int diastolic = 0;
    SensorDataProvider::UIntUIntCallback cb = [&](unsigned int sy, unsigned int di){ systolic = sy; diastolic = di; };
    provider.registerSystolicValuesCallback(cb);

    _data.get()->back().CM_Systolic = 33;
    _data.get()->back().CM_Diastolic = 10;
    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(_data.get()->back().CM_Systolic, systolic);
    ASSERT_EQ(_data.get()->back().CM_Diastolic, diastolic);
}

TEST_F(SensorDataProviderTests, UnregisterSystolicValuesCallbackIsNotExecutedWhenParserExecutesDataCallback)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count1 = 1;
    SensorDataProvider::UIntUIntCallback cb = [&](unsigned int, unsigned int){ count1++; };
    unsigned long funcId = provider.registerSystolicValuesCallback(cb);

    unsigned int count2 = 1;
    SensorDataProvider::UIntUIntCallback cb2 = [&](unsigned int, unsigned int){count2++; _condition.notify_all(); };
    provider.registerSystolicValuesCallback(cb2);

    provider.unregisterSystolicValuesCallback(funcId);

    parser._fake._dataCb(_data);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(count1, 1);
    ASSERT_EQ(count2, 2);
}

TEST_F(SensorDataProviderTests, SendSignalStrengthExecutesSignalStrengthCallbacks)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 1;
    SensorDataProvider::UIntCallback cb = [&](unsigned int){count++; _condition.notify_all(); };
    provider.registerSignalStrengthCallback(cb);

    parser._fake._dataCb(_data);
    provider.sendSignalStrength();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, SendSignalStrengthExecutesSignalStrengthCallbackWithSignalStrength)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    parser._fake._dataCb(_data);

    unsigned int signalStrength = 0;
    SensorDataProvider::UIntCallback cb = [&](unsigned int ss){ signalStrength = ss; _condition.notify_all(); };
    provider.registerSignalStrengthCallback(cb);

    provider.sendSignalStrength();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(_data.get()->back().signalStrength, signalStrength);
}

TEST_F(SensorDataProviderTests,  SendMeanExecutesMeanValueCallbacks)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 1;
    SensorDataProvider::UIntCallback cb = [&](unsigned int){count++; _condition.notify_all(); };
    provider.registerMeanValueCallback(cb);

    provider.sendMean();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, SendMeanExecutesMeanValueCallbacksWithMeanValue)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    parser._fake._dataCb(_data);

    unsigned int meanValue = 0;
    SensorDataProvider::UIntCallback cb = [&](unsigned int mv){ meanValue = mv; _condition.notify_all(); };
    provider.registerMeanValueCallback(cb);

    provider.sendMean();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(_data.get()->back().CM_Mean, meanValue);
}

TEST_F(SensorDataProviderTests, SendSystolicDiastolicExecutesSystolicValuesCallbacks)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 1;
    provider.registerSystolicValuesCallback([&](unsigned int, unsigned int){count++; });

    provider.sendSystolicDiastolic();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, SendSystolicDiastolicExecutesSystolicValuesCallbacksWithValues)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);
    parser._fake._dataCb(_data);

    unsigned int systolic = 0;
    unsigned int diastolic = 0;
    SensorDataProvider::UIntUIntCallback cb = [&](unsigned int sy, unsigned int di){ systolic = sy; diastolic = di; };
    provider.registerSystolicValuesCallback(cb);

    provider.sendSystolicDiastolic();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(_data.get()->back().CM_Systolic, systolic);
    ASSERT_EQ(_data.get()->back().CM_Diastolic, diastolic);
}

TEST_F(SensorDataProviderTests, SendHeartRateExecutesHeartRateCallbacks)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 1;
    SensorDataProvider::UIntCallback cb = [&](unsigned int){count++; _condition.notify_all(); };
    provider.registerHeartRateCallback(cb);

    provider.sendHeartRate();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, SendHeartRateExecutesHeartRateCallbacksWithHeartRateValue)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    parser._fake._dataCb(_data);

    unsigned int heartRate = 0;
    SensorDataProvider::UIntCallback cb = [&](unsigned int hr){ heartRate = hr; };
    provider.registerHeartRateCallback(cb);

    provider.sendHeartRate();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(_data.get()->back().heartRate, heartRate);
}

TEST_F(SensorDataProviderTests, SendPulsatilityExecutesPulsatilityCallbacks)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    unsigned int count = 1;
    SensorDataProvider::BoolCallback cb = [&](bool){count++; _condition.notify_all(); };
    provider.registerPulsatilityCallback(cb);

    provider.sendPulsatility();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count);
}

TEST_F(SensorDataProviderTests, SendPulsatilityExecutesPulsatilityCallbacksWithValue)
{
    NiceMock<MockSensorDataParser> parser;
    parser.DelegateToFake();
    SensorDataProvider provider(parser);

    parser._fake._dataCb(_data);

    bool pulsatility;

    SensorDataProvider::BoolCallback cb = [&](bool p){ pulsatility = p; };
    provider.registerPulsatilityCallback(cb);

    provider.sendPulsatility();

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(_data.get()->back().CM_Pulsatility, pulsatility);
}
