#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "unitTests/mocks/MockSensorDataProvider.h"
#include "presentation/PressureValueController.h"

using namespace ::testing;

class PressureValueControllerTests : public Test
{
public:
    PressureValueControllerTests()
        : _controller(_provider)
    {}

    ~PressureValueControllerTests();

    NiceMock<MockSensorDataProvider> _provider;
    PressureValueController _controller;
};

PressureValueControllerTests::~PressureValueControllerTests() {}

TEST_F(PressureValueControllerTests, ConstructorRegistersCallbackWithSensorDataProvider)
{
    NiceMock<MockSensorDataProvider> provider;

    EXPECT_CALL(provider, registerMeanValueCallback(_))
            .Times(1);

    EXPECT_CALL(provider, registerSystolicValuesCallback(_))
            .Times(1);

    EXPECT_CALL(provider, registerHeartRateCallback(_))
            .Times(1);

    PressureValueController controller(provider);
}

TEST_F(PressureValueControllerTests, GetPressureValuesUpdatesPressuresIfCommandStartAfter1Second)
{
    QSignalSpy meanSpy(&_controller, SIGNAL(meanChanged()));
    QSignalSpy systolicSpy(&_controller, SIGNAL(systolicChanged()));
    QSignalSpy diastoliSpy(&_controller, SIGNAL(diastolicChanged()));
    QSignalSpy heartReateSpy(&_controller, SIGNAL(heartRateChanged()));

    _controller.getPressureValues("Start");
    std::this_thread::sleep_for(std::chrono::milliseconds(1050));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, meanSpy.count());
    ASSERT_EQ(1, systolicSpy.count());
    ASSERT_EQ(1, diastoliSpy.count());
    ASSERT_EQ(1, heartReateSpy.count());
}

TEST_F(PressureValueControllerTests, GetPressureValuesStopsTimerIfCommandStop)
{
    QSignalSpy meanSpy(&_controller, SIGNAL(meanChanged()));
    QSignalSpy systolicSpy(&_controller, SIGNAL(systolicChanged()));
    QSignalSpy diastoliSpy(&_controller, SIGNAL(diastolicChanged()));
    QSignalSpy heartReateSpy(&_controller, SIGNAL(heartRateChanged()));

    _controller.getPressureValues("Stop");


    ASSERT_EQ(0, meanSpy.count());
    ASSERT_EQ(0, systolicSpy.count());
    ASSERT_EQ(0, diastoliSpy.count());
    ASSERT_EQ(0, heartReateSpy.count());
}

TEST_F(PressureValueControllerTests, UpdateMeanCalledWhenProviderCallbackExecuted)
{
    NiceMock<MockSensorDataProvider> provider;
    provider.DelegateToFake();
    PressureValueController controller(provider);

    provider._fake._meanValueCallback(30);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(controller.mean(), 30);
}

TEST_F(PressureValueControllerTests, UpdateSystolicPressuresCalledWhenProviderCallbackExecuted)
{
    NiceMock<MockSensorDataProvider> provider;
    provider.DelegateToFake();
    PressureValueController controller(provider);

    provider._fake._systolicPressuresCallback(20,40);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(controller.systolic(), 20);
    ASSERT_EQ(controller.diastolic(), 40);
}

TEST_F(PressureValueControllerTests, UpdateHeartRateCalledWhenProviderCallbackExecuted)
{
    NiceMock<MockSensorDataProvider> provider;
    provider.DelegateToFake();
    PressureValueController controller(provider);

    provider._fake._heartRateCallback(75);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(controller.heartRate(), 75);
}

TEST_F(PressureValueControllerTests, SystolicPropertyReturnsTheSystolicPressureValue)
{
    NiceMock<MockSensorDataProvider> provider;
    provider.DelegateToFake();
    PressureValueController controller(provider);

    provider._fake._systolicPressuresCallback(30,40);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    int value = controller.property("systolic").toInt();
    ASSERT_EQ(value, 30);
}

TEST_F(PressureValueControllerTests, SystolicPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("systolic");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("systolicChanged", property.notifySignal().name().data());
}

TEST_F(PressureValueControllerTests, DiastolicPropertyReturnsTheDiastolicPressureValue)
{
    NiceMock<MockSensorDataProvider> provider;
    provider.DelegateToFake();
    PressureValueController controller(provider);

    provider._fake._systolicPressuresCallback(30,40);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    int value = controller.property("diastolic").toInt();
    ASSERT_EQ(value, 40);
}

TEST_F(PressureValueControllerTests, DiastolicPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("diastolic");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("diastolicChanged", property.notifySignal().name().data());
}

TEST_F(PressureValueControllerTests, MeanPropertyReturnsTheMeanPressureValue)
{
    NiceMock<MockSensorDataProvider> provider;
    provider.DelegateToFake();
    PressureValueController controller(provider);

    provider._fake._meanValueCallback(28);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    int value = controller.property("mean").toInt();
    ASSERT_EQ(value, 28);
}

TEST_F(PressureValueControllerTests, MeanPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("mean");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("meanChanged", property.notifySignal().name().data());
}

TEST_F(PressureValueControllerTests, HeartRatePropertyReturnsTheHeartRateValue)
{
    NiceMock<MockSensorDataProvider> provider;
    provider.DelegateToFake();
    PressureValueController controller(provider);

    provider._fake._heartRateCallback(75);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    int value = controller.property("heartRate").toInt();
    ASSERT_EQ(value, 75);
}

TEST_F(PressureValueControllerTests, HeartRatePropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("heartRate");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("heartRateChanged", property.notifySignal().name().data());
}
