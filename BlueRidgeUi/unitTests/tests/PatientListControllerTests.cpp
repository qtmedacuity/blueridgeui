#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "unitTests/mocks/MockPatientListProvider.h"
#include "presentation/PatientListController.h"
#include "presentation/PatientInfo.h"

using namespace ::testing;

class PatientListControllerTests : public Test
{
public:
    PatientListControllerTests()
          : controller(provider)
    {

        PatientData patient;
        patient.patientId = 845656;
        patient.patientName = "Tyler, Paula C";
        patient.dob = "8 Aug 1945";
        patient.phone = "(555)212-2222";
        patient.uploadPending = false;

        patientList.push_back(patient);

        patient.patientId = 456843;
        patient.patientName = "Campbell, Joseph P" ;
        patient.dob = "25 Dec 1967";
        patient.phone = "(555)212-2222";
        patient.uploadPending = false;

        patientList.push_back(patient);

        patient.patientId = 354888;
        patient.patientName = "Pond, Maria";
        patient.dob = "17 Sep 1955";
        patient.phone = "(555)212-2222";
        patient.uploadPending = false;

        patientList.push_back(patient);

        std::sort(patientList.begin(), patientList.end(),
                  [](PatientData a, PatientData b) {
            return a.patientName < b.patientName; });
    }

    ~PatientListControllerTests();

    void SetUp()
    {
        ON_CALL(provider, patientList())
                .WillByDefault(Return(patientList));

        ON_CALL(provider, refreshDate())
                .WillByDefault(Return("06 Mar 2019"));

        ON_CALL(provider, refreshTime())
                .WillByDefault(Return("16:27:50"));
    }

    bool PatientInfoEqual(std::shared_ptr<PatientInfo> pt1, std::shared_ptr<PatientInfo> pt2)
    {
        if (pt1->patientId() == pt2->patientId() &&
               pt1->patientName() == pt2->patientName() &&
               pt1->dob() == pt2->dob() &&
               pt1->phone() == pt2->phone() &&
               pt1->uploadPending() == pt2->uploadPending())
       {
           return  true;
       }
        return false;
    }

    std::vector<PatientData> patientList;
    NiceMock<MockPatientListProvider> provider;
    PatientListController controller;
};

PatientListControllerTests::~PatientListControllerTests() {}

TEST_F(PatientListControllerTests, ConstructorRegistersCallbackWithProvider)
{
    NiceMock<MockPatientListProvider> _provider;

    EXPECT_CALL(_provider, registerPatientListCallback(_))
            .Times(1);

    EXPECT_CALL(_provider, registerRefreshCallback(_))
            .Times(1);

    PatientListController _controller(_provider);
}
TEST_F(PatientListControllerTests, getAllPatientsCallsProviderRequestAllPatients)
{
    EXPECT_CALL(provider, requestAllPatients())
            .Times(1);

    controller.getAllPatients();
}

TEST_F(PatientListControllerTests, getAllPatientsSetsSourceModelToPatientListReturnFromProvider)
{
    controller.getAllPatients();
    emit controller.patientListUpdated();

    for(int i = 0; i < patientList.size(); ++i)
    {
        std::shared_ptr<PatientInfo> expectedPatient(new PatientInfo(patientList.at(i)));
        std::shared_ptr<PatientInfo> actualPatient(controller.get(i).value<PatientInfo*>());
        ASSERT_TRUE(PatientInfoEqual(expectedPatient, actualPatient));
    }
}

TEST_F(PatientListControllerTests, getImplantPatientsCallsProviderRequestPatientsWithImplant)
{
    EXPECT_CALL(provider, requestPatientsWithImplant())
            .Times(1);

    controller.getImplantPatients();
}

TEST_F(PatientListControllerTests, getImplantPatientsSetsSourceModelToPatientListReturnFromProvider)
{
    controller.getImplantPatients();
    emit controller.patientListUpdated();

    for(int i = 0; i < patientList.size(); ++i)
    {
        std::shared_ptr<PatientInfo> expectedPatient(new PatientInfo(patientList.at(i)));
        std::shared_ptr<PatientInfo> actualPatient(controller.get(i).value<PatientInfo*>());
        ASSERT_TRUE(PatientInfoEqual(expectedPatient, actualPatient));
    }
}

TEST_F(PatientListControllerTests, getWaitingImplantPatientsCallsProviderRequestPatientsWaitingImplant)
{
    EXPECT_CALL(provider, requestPatientsWaitingForImplant())
            .Times(1);

    controller.getWaitingImplantPatients();
}

TEST_F(PatientListControllerTests, getWaitingImplantPatientsSetsSourceModelToPatientListReturnFromProvider)
{
    controller.getImplantPatients();
    emit controller.patientListUpdated();

    for(int i = 0; i < patientList.size(); ++i)
    {
        std::shared_ptr<PatientInfo> expectedPatient(new PatientInfo(patientList.at(i)));
        std::shared_ptr<PatientInfo> actualPatient(controller.get(i).value<PatientInfo*>());
        ASSERT_TRUE(PatientInfoEqual(expectedPatient, actualPatient));
    }
}

TEST_F(PatientListControllerTests, refreshCallsProviderRequestRefresh)
{
    EXPECT_CALL(provider, requestRefresh())
            .Times(1);

    controller.refresh();
}

TEST_F(PatientListControllerTests, selectAllPatientsWithFalseReturnsZeroNumberSelectedPatientsInModel)
{
    int expectedTotal = 0;

    controller.getAllPatients();
    emit controller.patientListUpdated();

    int actualTotal = controller.selectAllPatients(false);

    ASSERT_EQ(expectedTotal, actualTotal);
}

TEST_F(PatientListControllerTests, selectAllPatientsWithTrueReturnsTotalNumberOfSelectedPatientsInModel)
{
    int expectedTotal = patientList.size();

    controller.getAllPatients();
    emit controller.patientListUpdated();

    int actualTotal = controller.selectAllPatients(true);

    ASSERT_EQ(expectedTotal, actualTotal);
}

TEST_F(PatientListControllerTests, setfilterStringUpdatesSetsControllerSortFilterProxyModel)
{
    controller.setFilterString("filter");

    ASSERT_EQ(controller.filterCaseSensitivity(), Qt::CaseInsensitive);
    ASSERT_EQ(controller.filterRegExp().pattern(), "filter");
}

TEST_F(PatientListControllerTests, getReturnsPatientInfoAtIndex)
{
    controller.getImplantPatients();
    emit controller.patientListUpdated();

    std::shared_ptr<PatientInfo> expectedPatient(new PatientInfo(patientList.at(1)));

    std::shared_ptr<PatientInfo> actualPatient(controller.get(1).value<PatientInfo*>());

    ASSERT_TRUE(PatientInfoEqual(expectedPatient, actualPatient));
}

TEST_F(PatientListControllerTests, lastUpdatedReturnsDateTimeOfLatestPatientListUpdate)
{
    controller.getAllPatients();
    emit controller.patientListUpdated();

    QString expectedLastUpdated = "06 Mar 2019 16:27:50";

    ASSERT_EQ(expectedLastUpdated, controller.lastUpdated());
}

TEST_F(PatientListControllerTests, lastUpdatedPropertyReturnslastUpdatedString)
{
    QString expectedLastUpdated = "06 Mar 2019 16:27:50";

    controller.getWaitingImplantPatients();
    emit controller.patientListUpdated();

    QString actualLastUpdated= controller.property("lastUpdated").toString();

    ASSERT_EQ(actualLastUpdated, expectedLastUpdated);
}

TEST_F(PatientListControllerTests, lastUpdatedPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("lastUpdated");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("lastUpdatedChanged", property.notifySignal().name().data());
}

TEST_F(PatientListControllerTests, lastUpdatedSignalEmittedAfterProviderReturnsUpdatedPatientList)
{
    QSignalSpy spy(&controller, SIGNAL(lastUpdatedChanged(QString)));
    controller.getWaitingImplantPatients();
    emit controller.patientListUpdated();

    ASSERT_EQ(1, spy.count());
}

TEST_F(PatientListControllerTests, refreshCompleteSignalEmittedAfterProviderNotifiesRefreshCompleted)
{
    QSignalSpy spy(&controller, SIGNAL(refreshComplete()));
    emit controller.refreshUpdated();

    ASSERT_EQ(1, spy.count());
}

TEST_F(PatientListControllerTests, sortByColumnAscendingSortsThePatientListModelByColumnInAscendingOrder)
{
    controller.getAllPatients();
    emit controller.patientListUpdated();

    controller.sortByColumn(0, true);

    for(int i = 0; i < patientList.size(); ++i)
    {
        std::shared_ptr<PatientInfo> expectedPatient(new PatientInfo(patientList.at(i)));
        std::shared_ptr<PatientInfo> actualPatient(controller.get(i).value<PatientInfo*>());
        ASSERT_TRUE(PatientInfoEqual(expectedPatient, actualPatient));
    }
}

TEST_F(PatientListControllerTests, sortByColumnDescendingSortsThePatientListModelByColumnInDescendingOrder)
{
    std::sort(patientList.begin(), patientList.end(),
              [](PatientData a, PatientData b) {
        return a.patientName > b.patientName; });

    controller.getAllPatients();
    emit controller.patientListUpdated();

    controller.sortByColumn(0, false);

    for(int i = 0; i < patientList.size(); ++i)
    {
        std::shared_ptr<PatientInfo> expectedPatient(new PatientInfo(patientList.at(i)));
        std::shared_ptr<PatientInfo> actualPatient(controller.get(i).value<PatientInfo*>());
        ASSERT_TRUE(PatientInfoEqual(expectedPatient, actualPatient));
    }
}

TEST_F(PatientListControllerTests, getAllPatientClearsPreviouslySetFilterString)
{
    controller.setFilterString("filter");
    controller.getAllPatients();

    ASSERT_EQ(controller.filterRegExp().pattern(), "");
}

TEST_F(PatientListControllerTests, getImplantPatientClearsPreviouslySetFilterString)
{
    controller.setFilterString("filter");
    controller.getImplantPatients();

    ASSERT_EQ(controller.filterRegExp().pattern(), "");
}

TEST_F(PatientListControllerTests, getWaitingImplantPatientClearsPreviouslySetFilterString)
{
    controller.setFilterString("filter");
    controller.getWaitingImplantPatients();

    ASSERT_EQ(controller.filterRegExp().pattern(), "");
}

TEST_F(PatientListControllerTests, patientListUpdatedSignalEmittedWhenProviderCallbackExcuted)
{
    NiceMock<MockPatientListProvider> _provider;
    _provider.DelegateToFake();

    ON_CALL(_provider, patientList())
            .WillByDefault(Return(patientList));

    ON_CALL(_provider, refreshDate())
            .WillByDefault(Return("06 Mar 2019"));

    ON_CALL(_provider, refreshTime())
            .WillByDefault(Return("16:27:50"));

    PatientListController _controller(_provider);
    _controller.getAllPatients();

    QSignalSpy spy(&_controller, SIGNAL(patientListUpdated()));

    _provider._fake._patientListCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientListControllerTests, refreshUpdatedSignalEmittedWhenProviderCallbackExcuted)
{
    NiceMock<MockPatientListProvider> _provider;
    _provider.DelegateToFake();
    PatientListController _controller(_provider);

    QSignalSpy spy(&_controller, SIGNAL(refreshUpdated()));

    _provider._fake._refreshCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(PatientListControllerTests, havePendingUploadsReturnsFalseWhenListHasNoItemsWithUploadPendingSet)
{
    NiceMock<MockPatientListProvider> listProvider;
    std::vector<PatientData> patientList;
    PatientData patient;
    patient.patientId = 845656;
    patient.patientName = "Tyler, Paula C";
    patient.dob = "8 Aug 1945";
    patient.phone = "(555)212-2222";
    patient.uploadPending = false;

    patientList.push_back(patient);

    patient.patientId = 456843;
    patient.patientName = "Campbell, Joseph P" ;
    patient.dob = "25 Dec 1967";
    patient.phone = "(555)212-2222";
    patient.uploadPending = false;

    patientList.push_back(patient);

    ON_CALL(listProvider, patientList())
            .WillByDefault(Return(patientList));

    PatientListController controller(listProvider);
    controller.getImplantPatients();
    emit controller.patientListUpdated();

    ASSERT_FALSE(controller.havePendingUploads());

}

TEST_F(PatientListControllerTests, havePendingUploadsReturnsTrueWhenListHasNoItemsWithUploadPendingSet)
{
    NiceMock<MockPatientListProvider> listProvider;
    std::vector<PatientData> patientList;
    PatientData patient;
    patient.patientId = 845656;
    patient.patientName = "Tyler, Paula C";
    patient.dob = "8 Aug 1945";
    patient.phone = "(555)212-2222";
    patient.uploadPending = false;

    patientList.push_back(patient);

    patient.patientId = 456843;
    patient.patientName = "Campbell, Joseph P" ;
    patient.dob = "25 Dec 1967";
    patient.phone = "(555)212-2222";
    patient.uploadPending = true;

    patientList.push_back(patient);

    ON_CALL(listProvider, patientList())
            .WillByDefault(Return(patientList));

    PatientListController controller(listProvider);
    controller.getImplantPatients();
    emit controller.patientListUpdated();

    ASSERT_TRUE(controller.havePendingUploads());

}

