#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "providers/RecordingData.h"

using namespace ::testing;

class RecordingDataTests : public Test
{

public:
    RecordingDataTests() {}
    ~RecordingDataTests() override;

    void SetUp() override
    {
        _cmem_recording.add_field("RecordingId", 1234);
        _cmem_recording.add_field("Notes", "notes");
        _cmem_recording.add_field("WaveformData", "valid_file.xml");
        _cmem_recording.add_field("UploadPending", "YES");
        _cmem_recording.add_field("Systolic", "33");
        _cmem_recording.add_field("Diastolic", "22");
        _cmem_recording.add_field("Mean", "25");
        _cmem_recording.add_field("ReferenceSystolic", "32");
        _cmem_recording.add_field("ReferenceDiastolic", "21");
        _cmem_recording.add_field("ReferenceMean", "24");
        _cmem_recording.add_field("AverageHeartRate", "78");
        _cmem_recording.add_field("AverageSignalStrength", "91");
        _cmem_recording.add_field("PatientPosition", "Horizontal");
        _cmem_recording.add_field("Date", _recordingDate.c_str());
        _cmem_recording.add_field("Time", _recordingTime.c_str());
    }

    cmem_recording _cmem_recording;
    std::string _recordingDate = "23 May 2019";
    std::string _recordingTime = "11:33:22";
};

RecordingDataTests::~RecordingDataTests() {}


TEST_F(RecordingDataTests, DefaultConstructorDefaultsValuesToZero)
{
    RecordingData recordingData;

    ASSERT_EQ(0, recordingData.recordingId);
    ASSERT_FALSE(recordingData.uploadPending);
    ASSERT_EQ(0, recordingData.systolic);
    ASSERT_EQ(0, recordingData.diastolic);
    ASSERT_EQ(0, recordingData.mean);
    ASSERT_EQ(0, recordingData.refSystolic);
    ASSERT_EQ(0, recordingData.refDiastolic);
    ASSERT_EQ(0, recordingData.refMean);
    ASSERT_EQ(0, recordingData.heartRate);
    ASSERT_EQ(0, recordingData.signalStrength);
}


TEST_F(RecordingDataTests, ConstructorSetsRecordingDataValuesFromCmemRecordingValues)
{

    RecordingData recordingData(_cmem_recording);

    ASSERT_EQ(_cmem_recording.getRecordingId(), recordingData.recordingId);
    ASSERT_EQ(_cmem_recording.uploadPending(), recordingData.uploadPending);
    ASSERT_EQ(_cmem_recording.getSystolic(), recordingData.systolic);
    ASSERT_EQ(_cmem_recording.getDiastolic(), recordingData.diastolic);
    ASSERT_EQ(_cmem_recording.getMean(), recordingData.mean);
    ASSERT_EQ(_cmem_recording.getRefSystolic(), recordingData.refSystolic);
    ASSERT_EQ(_cmem_recording.getRefDiastolic(), recordingData.refDiastolic);
    ASSERT_EQ(_cmem_recording.getRefMean(), recordingData.refMean);
    ASSERT_EQ(_cmem_recording.getAvgHeartRate(), recordingData.heartRate);
    ASSERT_EQ(_cmem_recording.getAvgSignalStrength(), recordingData.signalStrength);
    ASSERT_EQ("Horizontal", recordingData.patientPosition);
    ASSERT_EQ(_recordingDate, recordingData.recordingDate);
    ASSERT_EQ(_recordingTime, recordingData.recordingTime);
}

TEST_F(RecordingDataTests, ConstructorSetsRecordingDataPatientPositionTo30Degrees)
{
    _cmem_recording.add_field("PatientPosition", "30 Degrees");
    RecordingData recordingData(_cmem_recording);

    ASSERT_EQ("30_Degrees", recordingData.patientPosition);
}

TEST_F(RecordingDataTests, ConstructorSetsRecordingDataPatientPositionTo45Degrees)
{
    _cmem_recording.add_field("PatientPosition", "45 Degrees");
    RecordingData recordingData(_cmem_recording);

    ASSERT_EQ("45_Degrees", recordingData.patientPosition);
}

TEST_F(RecordingDataTests, ConstructorSetsRecordingDataPatientPositionTo90Degrees)
{
    _cmem_recording.add_field("PatientPosition", "90 Degrees");
    RecordingData recordingData(_cmem_recording);

    ASSERT_EQ("90_Degrees", recordingData.patientPosition);
}

TEST_F(RecordingDataTests, ConstructorSetsRecordingDataPatientPositionToHorizontalIfPositionUnknown)
{
    _cmem_recording.add_field("PatientPosition", "blah");
    RecordingData recordingData(_cmem_recording);

    ASSERT_EQ("Horizontal", recordingData.patientPosition);
}
