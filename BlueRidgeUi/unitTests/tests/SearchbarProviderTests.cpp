#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmem_command.h>

#include "unitTests/mocks/MockMessageParser.h"
#include "providers/SearchbarProvider.h"

using namespace ::testing;

class SearchbarProviderTests : public Test
{
public:
    SearchbarProviderTests()
    {
        _searchPressureStatus = "{\n\t\"Pressure\" : \" 50\","
                                "\n\t\"Status\" : \"SUCCESS\"\n}\n";

        _autoSearchStatus = "{\n\"Status\" : \"SUCCESS\","
                                "\n\"SearchComplete\" : \"True\","
                                "\n\"SearchPressure\" : \"30\"}";

        _searchingStatus = "{\n\"Status\" : \"SUCCESS\","
                                "\n\"SearchComplete\" : \"False\","
                                "\n\"SearchPressure\" : \"0\"}";
    }

    ~SearchbarProviderTests() override;

    std::string _searchPressureStatus;
    std::string _autoSearchStatus;
    std::string _searchingStatus;
    std::condition_variable _condition;
};

SearchbarProviderTests::~SearchbarProviderTests() {}

TEST_F(SearchbarProviderTests, ConstructorRegistersCallbackWithMessageParser)
{
    NiceMock<MockMessageParser> parser;

    EXPECT_CALL(parser, registerCallback(_))
            .Times(1);

    SearchbarProvider provider(parser);
}

TEST_F(SearchbarProviderTests, CallbacksFromParserOfDifferentCommandTypeAreNotHandledByProvider)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    unsigned long count1 = 1;
    provider.registerSearchPressureCallback([&]{ count1++; _condition.notify_one(); });

    unsigned long count2 = 2;
    provider.registerAutoSearchingCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("unkown string"));
    parser._fake._func(eUNKNOWN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(SearchbarProviderTests, DISABLED_SearchPressureRegisteredCallbackIsExecutedOnSignalUpdate)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    unsigned long count = 0;
    provider.registerSearchPressureCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchPressureStatus));
    parser._fake._func(eGET_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(1, count);
}

TEST_F(SearchbarProviderTests, DISABLED_SearchPressureRegisteredCallbacksAreExecutedOnSignalUpdate)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    unsigned long count1 = 1;
    provider.registerSearchPressureCallback([&]{ count1++; });

    unsigned long count2 = 2;
    provider.registerSearchPressureCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchPressureStatus));
    parser._fake._func(eGET_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(3, count2);
}

TEST_F(SearchbarProviderTests, DISABLED_UnregisteredSearchPressureCallbacksAreNotExecutedByTheProvider)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    unsigned long count = 1;
    unsigned long cmdId = provider.registerSearchPressureCallback([&]{ count++; });

    std::string value = "";
    provider.registerSearchPressureCallback([&]{ value = "a string"; _condition.notify_one(); });

    provider.unregisterSearchPressureCallback(cmdId);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchPressureStatus));
    parser._fake._func(eGET_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(1, count);
    ASSERT_EQ("a string", value);
}

TEST_F(SearchbarProviderTests, AutoSearchingRegisteredCallbackIsExecutedOnSignalUpdate)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    unsigned long count = 0;
    provider.registerAutoSearchingCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchingStatus));
    parser._fake._func(eASYNC_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}

TEST_F(SearchbarProviderTests, AutoSearchingRegisteredCallbacksAreExecutedOnSignalUpdate)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    unsigned long count1 = 1;
    provider.registerAutoSearchingCallback([&]{ count1++; });

    unsigned long count2 = 2;
    provider.registerAutoSearchingCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchingStatus));
    parser._fake._func(eASYNC_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(3, count2);
}

TEST_F(SearchbarProviderTests, UnregisteredAutoSearchCallbacksAreNotExecutedByTheProvider)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    unsigned long count = 1;
    unsigned long cmdId = provider.registerAutoSearchingCallback([&]{ count++; });

    std::string value = "";
    provider.registerAutoSearchingCallback([&]{ value = "a string"; _condition.notify_one(); });

    provider.unregisterAutoSearchingCallback(cmdId);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchingStatus));
    parser._fake._func(eASYNC_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
    ASSERT_EQ("a string", value);
}

TEST_F(SearchbarProviderTests, AutoSearchingRegisteredCallbackIsNotExecutedIfSearchCompleteIsTrueInStatus)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    unsigned long count = 0;
    provider.registerAutoSearchingCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_autoSearchStatus));
    parser._fake._func(eASYNC_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(0, count);
}

TEST_F(SearchbarProviderTests, SearchPressureReturnsZeroIfPressureNotSetInSearchPressureStatus)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    provider.registerSearchPressureCallback([&]{ _condition.notify_one(); });

    _searchPressureStatus = "{\n\t\"Pressure\" : \"\","
                            "\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchPressureStatus));
    parser._fake._func(eGET_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(0, provider.searchPressure());
}

TEST_F(SearchbarProviderTests, SearchPressureReturnsPressureValueSetInSearchPressureStatus)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    provider.registerSearchPressureCallback([&]{ _condition.notify_one(); });


    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchPressureStatus));
    parser._fake._func(eGET_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(50, provider.searchPressure());
}

TEST_F(SearchbarProviderTests, SearchPressureReturnsDifferentPressureValueSetInSearchPressureStatus)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    provider.registerSearchPressureCallback([&]{ _condition.notify_one(); });

    _searchPressureStatus = "{\n\t\"Pressure\" : \"15\","
                            "\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchPressureStatus));
    parser._fake._func(eGET_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(15, provider.searchPressure());
}

TEST_F(SearchbarProviderTests, SearchPressureReturnsOneHundredForPressureIfValueSetInSearchPressureStatusLargerThanOneHundred)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    provider.registerSearchPressureCallback([&]{ _condition.notify_one(); });

    _searchPressureStatus = "{\n\t\"Pressure\" : \"150\","
                            "\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_searchPressureStatus));
    parser._fake._func(eGET_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(100, provider.searchPressure());
}

TEST_F(SearchbarProviderTests, DISABLED_SetSearchPressureSetsPressureToValuePassed)
{
    NiceMock<MockMessageParser> parser;
    SearchbarProvider provider(parser);

    provider.setSearchPressure(70);

    ASSERT_EQ(70, provider.searchPressure());
}

TEST_F(SearchbarProviderTests, SetSearchPressureCallsParserSendCommandTwice)
{
    unsigned int pressure = 25;
    Json::Value payload;
    payload["Pressure"] = std::to_string(pressure);

    NiceMock<MockMessageParser> parser;
    SearchbarProvider provider(parser);

    EXPECT_CALL(parser, sendCommand(eSET_SEARCH_PRESSURE, payload.toStyledString()))
            .Times(1);

    EXPECT_CALL(parser, sendCommand(eGET_SEARCH_PRESSURE))
            .Times(1);

    provider.setSearchPressure(pressure);
}

TEST_F(SearchbarProviderTests, SearchPressureReturnsPressureValueSetAutoSearchStatusIfSearchIsComplete)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    provider.registerSearchPressureCallback([&]{ _condition.notify_one(); });


    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_autoSearchStatus));
    parser._fake._func(eASYNC_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(30, provider.searchPressure());
}

TEST_F(SearchbarProviderTests, AutoSearchingDefaultsToFalse)
{
    NiceMock<MockMessageParser> parser;
    SearchbarProvider provider(parser);

    ASSERT_FALSE(provider.autoSearching());
}

TEST_F(SearchbarProviderTests, AutoSearchingReturnsTrueWhenSetAutoSearchingCalledAndAutoSearchingIsFalse)
{
    NiceMock<MockMessageParser> parser;
    SearchbarProvider provider(parser);

    provider.setAutoSearching();

    ASSERT_TRUE(provider.autoSearching());
}

TEST_F(SearchbarProviderTests, SetAutoSearchingCallsParserSendCommandWithCorrectCommand)
{
    NiceMock<MockMessageParser> parser;
    SearchbarProvider provider(parser);

    EXPECT_CALL(parser, sendCommand(ePERFORM_PRESSURE_SEARCH))
            .Times(1);

    provider.setAutoSearching();
}

TEST_F(SearchbarProviderTests, SetAutoSearchingExecutesAutoSearchingCallbacks)
{
    NiceMock<MockMessageParser> parser;
    SearchbarProvider provider(parser);

    unsigned int count = 0;
    provider.registerAutoSearchingCallback([&]{ count++; _condition.notify_one(); });

    provider.setAutoSearching();

    ASSERT_EQ(1, count);
}

TEST_F(SearchbarProviderTests, AutosearchingUpdatedToValueFromAutoSearchStatus)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    SearchbarProvider provider(parser);

    provider.registerAutoSearchingCallback([&]{ _condition.notify_one(); });

    _autoSearchStatus = "{\n\"Status\" : \"SUCCESS\","
                            "\n\"SearchComplete\" : \"False\","
                            "\n\"SearchPressure\" : \"50\"}";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_autoSearchStatus));
    parser._fake._func(eASYNC_SEARCH_PRESSURE, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_TRUE(provider.autoSearching());
}
