#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QDate>
#include "presentation/PatientListModel.h"
#include "presentation/PatientInfo.h"

using namespace ::testing;

class PatientListModelTests : public Test
{
public:
    PatientListModelTests()
        : ptListModel(new PatientListModel())
    {
        PatientData patient ;
        patient.patientId = 845656;
        patient.patientName = "Tyler, Paula C";
        patient.dob = "8 Aug 1945";
        patient.phone = "(555)212-2222";
        patient.uploadPending = false;

        ptList.push_back(new PatientInfo(patient));


        patient.patientId = 456843;
        patient.patientName = "Campbell, Joseph P";
        patient.dob = "25 Dec 1967";
        patient.phone = "(555)212-2222";
        patient.uploadPending = false;

        ptList.push_back(new PatientInfo(patient));


        patient.patientId = 354888;
        patient.patientName = "Pond, Amy";
        patient.dob = "17 Sep 1955";
        patient.phone = "(555)212-2222";
        patient.uploadPending = false;

        ptList.push_back(new PatientInfo(patient));


        patient.patientId = 456843;
        patient.patientName = "Everett, Daniel O";
        patient.dob = "25 Dec 1967";
        patient.serialNumber = "CKGLKL";
        patient.implantDate = "6 Jun 2017";
        patient.implantDoctor = "John Smith, MD";
        patient.clinician = "John Watson, MD";
        patient.clinic = "City Line Clinic";
        patient.phone = "(555)463-5555";
        patient.uploadPending = true;

        ptList.push_back(new PatientInfo(patient));


        patient.patientId = 125423;
        patient.patientName = "Williams, Rory";
        patient.dob = "22 Mar 1948";
        patient.serialNumber = "PBLGYA";
        patient.implantDoctor = "Leonard McCoy, MD";
        patient.clinician = "Hawkeye Pierce, MD";
        patient.clinic = "Boston General";
        patient.phone = "(555)212-2222";
        patient.implantDate = "12 Dec 2017";
        patient.uploadPending = false;

        ptList.push_back(new PatientInfo(patient));

        ptListModel->setPatientList(ptList);
        std::sort(ptList.begin(), ptList.end(), [](PatientInfo* a, PatientInfo* b) { return a->patientName() < b->patientName(); });
    }

    ~PatientListModelTests();

    bool PatientInfoEqual(const PatientInfo* pt1, const PatientInfo* pt2)
    {
        if (pt1->patientId() == pt2->patientId() &&
               pt1->patientName() == pt2->patientName() &&
               pt1->dob() == pt2->dob() &&
               pt1->phone() == pt2->phone() &&
               pt1->uploadPending() == pt2->uploadPending())
       {
           return  true;
       }
        return false;
    }

    QList<PatientInfo*> ptList;
    PatientListModel* ptListModel;
};

PatientListModelTests::~PatientListModelTests()
{
    ptList.clear();
    delete ptListModel;
    ptListModel = nullptr;
}

TEST_F(PatientListModelTests, rowCountReturnsTheNumbeRowsInTheModel)
{
    int expectedCount = ptList.count();
    int count = ptListModel->rowCount();

    ASSERT_EQ(expectedCount, count);
}

TEST_F(PatientListModelTests, dataReturnsEmptyVariantForUndefinedRole)
{
    int index = 0;
    QModelIndex idx = ptListModel->index(index,0);
    QVariant value = ptListModel->data(idx, -1);

    ASSERT_EQ(QVariant(), value);
}

TEST_F(PatientListModelTests, dataReturnsEmptyVariantForUndefinedIndex)
{
    int index = -1;
    QModelIndex idx = ptListModel->index(index,0);
    QVariant value = ptListModel->data(idx, PatientListModel::PatientRole::FullNameRole);

    ASSERT_EQ(QVariant(), value);
}

TEST_F(PatientListModelTests, dataReturnsCorrectValueForFullNameRoleAtIndex)
{
    int index = 0;
    QString expectedValue = ptList.at(index)->patientName();
    QModelIndex idx = ptListModel->index(index,0);
    QString value = ptListModel->data(idx, PatientListModel::PatientRole::FullNameRole).toString();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(PatientListModelTests, dataReturnsCorrectValueForDobRoleAtIndex)
{
    int index = 1;
    QString expectedValue = ptList.at(index)->dob();
    QModelIndex idx = ptListModel->index(index,0);
    QString value = ptListModel->data(idx, PatientListModel::PatientRole::DobRole).toString();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(PatientListModelTests, dataReturnsCorrectValueForPatientIdRoleAtIndex)
{
    int index = 2;
    int expectedValue = ptList.at(index)->patientId();
    QModelIndex idx = ptListModel->index(index,0);
    int value = ptListModel->data(idx, PatientListModel::PatientRole::PatientIdRole).toInt();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(PatientListModelTests, dataReturnsCorrectValueForSerialNumberRoleAtIndex)
{
    int index = 3;
    QString expectedValue = ptList.at(index)->serialNumber();
    QModelIndex idx = ptListModel->index(index,0);
    QString value = ptListModel->data(idx, PatientListModel::PatientRole::SerialNumberRole).toString();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(PatientListModelTests, dataReturnsCorrectValueForImplantDateRoleAtIndex)
{
    int index = 4;
    QString expectedValue = ptList.at(index)->implantDate();
    QModelIndex idx = ptListModel->index(index,0);
    QString value = ptListModel->data(idx, PatientListModel::PatientRole::ImplantDateRole).toString();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(PatientListModelTests, dataReturnsCorrectValueForSelectedRoleAtIndex)
{
    int index = 4;
    bool expectedValue = ptList.at(index)->selected();
    QModelIndex idx = ptListModel->index(index,0);
    bool value = ptListModel->data(idx, PatientListModel::PatientRole::SelectedRole).toBool();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(PatientListModelTests, dataReturnsCorrectValueForUploadPendingRoleAtIndex)
{
    int index = 4;
    bool expectedValue = ptList.at(index)->uploadPending();
    QModelIndex idx = ptListModel->index(index,0);
    bool value = ptListModel->data(idx, PatientListModel::PatientRole::UploadPendingRole).toBool();

    ASSERT_EQ(expectedValue, value);
}

TEST_F(PatientListModelTests, setDataSetstSelectedRoleAtIndexToValue)
{
    int index = 0;
    bool expectedValue = ptList.at(index)->selected();
    ASSERT_FALSE(expectedValue);

    QModelIndex idx = ptListModel->index(index, 0);
    ptListModel->setData(idx, QVariant(true), PatientListModel::PatientRole::SelectedRole);

    expectedValue = ptList.at(index)->selected();
    ASSERT_TRUE(expectedValue);
}

TEST_F(PatientListModelTests, roleNamesReturnsRoleStringForEachRoleAvailableInModel)
{
     QHash<int, QByteArray> expectedValues{{PatientListModel::PatientRole::FullNameRole, "fullName"},
                                           {PatientListModel::PatientRole::DobRole, "dob"},
                                           {PatientListModel::PatientRole::PatientIdRole, "patientId"},
                                           {PatientListModel::PatientRole::SerialNumberRole, "serialNumber"},
                                           {PatientListModel::PatientRole::ImplantDateRole, "implantDate"},
                                           {PatientListModel::PatientRole::UploadPendingRole, "uploadPending"},
                                           {PatientListModel::PatientRole::SelectedRole, "selected"}};

    QHash<int, QByteArray> values = ptListModel->roleNames();

    ASSERT_EQ(expectedValues, values);

}

TEST_F(PatientListModelTests, sortSortsTheModelPatientListByPatientNameColumnInAscendingOrder)
{
    ptListModel->sort(0, Qt::AscendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);

        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}

TEST_F(PatientListModelTests, sortSortsTheModelPatientListByDobColumnInAscendingOrder)
{
    std::sort(ptList.begin(), ptList.end(),
              [](PatientInfo* a, PatientInfo* b) {
        return  QDate::fromString(a->dob(), "d MMM yyyy") < QDate::fromString(b->dob(), "d MMM yyyy"); });

    ptListModel->sort(1, Qt::AscendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);

        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}

TEST_F(PatientListModelTests, sortSortsTheModelPatientListByPatientIdColumnInAscendingOrder)
{
    std::sort(ptList.begin(), ptList.end(),
              [](PatientInfo* a, PatientInfo* b) {
        return a->patientId() < b->patientId(); });

    ptListModel->sort(2, Qt::AscendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);

        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}

TEST_F(PatientListModelTests, sortSortsTheModelPatientListBySerialNumberColumnInAscendingOrder)
{
    std::sort(ptList.begin(), ptList.end(),
              [](PatientInfo* a, PatientInfo* b) {
        return a->serialNumber() < b->serialNumber(); });

    ptListModel->sort(3, Qt::AscendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);

        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}

TEST_F(PatientListModelTests, sortSortsTheModelPatientListByImplantDateColumnInAscendingOrder)
{
    std::sort(ptList.begin(), ptList.end(),
              [](PatientInfo* a, PatientInfo* b) {
        return  QDate::fromString(a->implantDate(), "d MMM yyyy") < QDate::fromString(b->implantDate(), "d MMM yyyy"); });

    ptListModel->sort(4, Qt::AscendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);

        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}

TEST_F(PatientListModelTests, sortSortsTheModelPatientListByPatientNameColumnInDescendingOrder)
{
    std::sort(ptList.begin(), ptList.end(), [](PatientInfo* a, PatientInfo* b) { return a->patientName() > b->patientName(); });

    ptListModel->sort(0, Qt::DescendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);

        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}


TEST_F(PatientListModelTests, sortSortsTheModelPatientListByDobColumnInDescendingOrder)
{
    std::sort(ptList.begin(), ptList.end(),
              [](PatientInfo* a, PatientInfo* b) {
        return  QDate::fromString(a->dob(), "d MMM yyyy") > QDate::fromString(b->dob(), "d MMM yyyy"); });

    ptListModel->sort(1, Qt::DescendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);

        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}

TEST_F(PatientListModelTests, sortSortsTheModelPatientListByPatientIdColumnInDescendingOrder)
{
    std::sort(ptList.begin(), ptList.end(),
              [](PatientInfo* a, PatientInfo* b) { return a->patientId() > b->patientId(); });

    ptListModel->sort(2, Qt::DescendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);

        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}

TEST_F(PatientListModelTests, sortSortsTheModelPatientListBySerialNumberColumnInDescendingOrder)
{
    std::sort(ptList.begin(), ptList.end(), [](PatientInfo* a, PatientInfo* b) { return a->serialNumber() > b->serialNumber(); });

    ptListModel->sort(3, Qt::DescendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);

        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}

TEST_F(PatientListModelTests, sortSortsTheModelPatientListByImplantDateColumnInDescendingOrder)
{
    std::sort(ptList.begin(), ptList.end(),
              [](PatientInfo* a, PatientInfo* b) {
        return  QDate::fromString(a->implantDate(), "d MMM yyyy") > QDate::fromString(b->implantDate(), "d MMM yyyy"); });

    ptListModel->sort(4, Qt::DescendingOrder);

    for(int idx=0; idx < ptListModel->rowCount(); ++idx)
    {
        PatientInfo* expectedPatient = ptList.at(idx);
        PatientInfo* patient = ptListModel->get(idx);
        ASSERT_TRUE(PatientInfoEqual(patient, expectedPatient));
    }
}

TEST_F(PatientListModelTests, selectAllPatientsSetsSelectedToTrueForEachPatientInTheModel)
{
    int total = ptListModel->selectAllPatients(true);

    for(int idx = 0; idx < total; ++idx)
    {
        PatientInfo* pt = ptListModel->get(idx);
        ASSERT_TRUE(pt->selected());
    }
}

TEST_F(PatientListModelTests, selectAllPatientsSetsSelectedToFalseForEachPatientInTheModel)
{
    int total = ptListModel->selectAllPatients(false);

    for(int idx = 0; idx < total; ++idx)
    {
        PatientInfo* pt = ptListModel->get(idx);
        ASSERT_FALSE(pt->selected());
    }
}

TEST_F(PatientListModelTests, getSelectedPatientsReturnsListOfPatientWithSelectedTrue)
{
    ptList.at(0)->setSelected(true);
    ptList.at(3)->setSelected(true);
    QList<PatientInfo*> expectedList = {ptList.at(0), ptList.at(3)};

    ptListModel->get(0)->setSelected(true);
    ptListModel->get(3)->setSelected(true);

    QList<PatientInfo*> list = ptListModel->getSelectedPatients();

    ASSERT_EQ(expectedList.count(), list.count());

    for(int i=0; i < list.count(); ++i)
    {
        PatientInfo* pt = list.at(i);
        PatientInfo* expectedPt = expectedList.at(i);
        ASSERT_TRUE(PatientInfoEqual(pt, expectedPt));
    }
}

TEST_F(PatientListModelTests, getReturnsANullPointerIfIndexNotFound)
{
    int index = -1;

    PatientInfo* patient = ptListModel->get(index);

    ASSERT_EQ(patient, nullptr);
}

TEST_F(PatientListModelTests, getReturnsPatientInfoAtIndex)
{
    int index = 2;

    PatientInfo* expectedPt = ptList.at(index);
    PatientInfo* patient = ptListModel->get(index);

    ASSERT_TRUE(PatientInfoEqual(patient, expectedPt));
}
