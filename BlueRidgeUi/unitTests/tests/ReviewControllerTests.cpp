#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include <cstring>
#include <vector>

#include "unitTests/mocks/MockTransactionProvider.h"
#include "unitTests/mocks/MockRecordingProvider.h"
#include "presentation/ReviewController.h"
#include "providers/RecordingProvider.h"
#include "presentation/Recording.h"
#include "presentation/SensorCalibration.h"
#include "presentation/CardiacOutput.h"
#include "presentation/RhcValues.h"
#include "presentation/PatientInfo.h"
#include "providers/RecordingData.h"
#include "providers/SensorCalibrationData.h"
#include "providers/CardiacOutputData.h"
#include "providers/RhcData.h"

using namespace ::testing;

class ReviewControllerTests : public Test
{
public:
    ReviewControllerTests()
          : _controller(_transactionProvider)
    {
        _eventData.eventId = 123;
        _eventData.patientId = 456;
        _eventData.eventType = "NewImplant";
        _eventData.date = "05 MAY 2019";
        _eventData.time = "09:02:44";

        _eventData.sensorCalibration = new SensorCalibrationData();
        _eventData.sensorCalibration->date = "05 MAY 2019";
        _eventData.sensorCalibration->time = "09:03:23";
        _eventData.sensorCalibration->systolic = 33;
        _eventData.sensorCalibration->diastolic = 22;
        _eventData.sensorCalibration->mean = 25;
        _eventData.sensorCalibration->reference = 30;
        _eventData.sensorCalibration->heartRate = 70;
        _eventData.sensorCalibration->signalStrength = 91;
        _eventData.sensorCalibration->waveformFile = "/tmp/valid_file.xml";

        _eventData.cardiacOutput = new CardiacOutputData();
        _eventData.cardiacOutput->date = "05 MAY 2019";
        _eventData.cardiacOutput->time = "09:04:23";
        _eventData.cardiacOutput->cardiacOutput = 3.3F;
        _eventData.cardiacOutput->heartRate = 71;
        _eventData.cardiacOutput->signalStrength = 92;
        _eventData.cardiacOutput->waveformFile = "/tmp/valid_file2.xml";

        _eventData.rhc = new RhcData();
        _eventData.rhc->date = "05 MAY 2019";
        _eventData.rhc->time = "09:05:23";
        _eventData.rhc->ra = 30;
        _eventData.rhc->rvSystolic = 31;
        _eventData.rhc->rvDiastoic = 21;
        _eventData.rhc->paSystolic = 32;
        _eventData.rhc->paDiastolic = 22;
        _eventData.rhc->paMean = 25;
        _eventData.rhc->pcwp = 11;
        _eventData.rhc->heartRate = 72;
        _eventData.rhc->signalStrength = 93;
        _eventData.rhc->waveformFile = "/tmp/valid_file3.xml";

        RecordingData recordingData1;
        recordingData1.recordingId = 6789;
        recordingData1.systolic = 38;
        recordingData1.diastolic = 19;
        recordingData1.mean = 22;
        recordingData1.heartRate = 79;
        recordingData1.signalStrength = 85;
        recordingData1.patientPosition = "Horizontal";
        _eventData.recordings.push_back(recordingData1);
        RecordingData recordingData2;
        recordingData2.recordingId = 6789;
        recordingData2.systolic = 39;
        recordingData2.diastolic = 20;
        recordingData2.mean = 23;
        recordingData2.heartRate = 80;
        recordingData2.signalStrength = 86;
        recordingData2.patientPosition = "30_Degrees";
        _eventData.recordings.push_back(recordingData2);
        _eventData.eventId = 123;
        _eventData.patientId = 456;
        _eventData.eventType = "NewImplant";
        _eventData.date = "05 MAY 2019";
        _eventData.time = "09:02:44";

        _eventData.sensorCalibration = new SensorCalibrationData();
        _eventData.sensorCalibration->date = "05 MAY 2019";
        _eventData.sensorCalibration->time = "09:03:23";
        _eventData.sensorCalibration->systolic = 33;
        _eventData.sensorCalibration->diastolic = 22;
        _eventData.sensorCalibration->mean = 25;
        _eventData.sensorCalibration->reference = 30;
        _eventData.sensorCalibration->heartRate = 70;
        _eventData.sensorCalibration->signalStrength = 91;
        _eventData.sensorCalibration->waveformFile = "/tmp/valid_file.xml";

        _eventData.cardiacOutput = new CardiacOutputData();
        _eventData.cardiacOutput->date = "05 MAY 2019";
        _eventData.cardiacOutput->time = "09:04:23";
        _eventData.cardiacOutput->cardiacOutput = 3.3F;
        _eventData.cardiacOutput->heartRate = 71;
        _eventData.cardiacOutput->signalStrength = 92;
        _eventData.cardiacOutput->waveformFile = "/tmp/valid_file2.xml";

        _eventData.rhc = new RhcData();
        _eventData.rhc->date = "05 MAY 2019";
        _eventData.rhc->time = "09:05:23";
        _eventData.rhc->ra = 30;
        _eventData.rhc->rvSystolic = 31;
        _eventData.rhc->rvDiastoic = 21;
        _eventData.rhc->paSystolic = 32;
        _eventData.rhc->paDiastolic = 22;
        _eventData.rhc->paMean = 25;
        _eventData.rhc->pcwp = 11;
        _eventData.rhc->heartRate = 72;
        _eventData.rhc->signalStrength = 93;
        _eventData.rhc->waveformFile = "/tmp/valid_file3.xml";

        _patientData.patientId = 456;
        _patientData.firstName = "Jane";
        _patientData.lastName = "Doe";
        _patientData.dob = "09 MAR 1945";
        _patientData.implantDate = "03 MAR 2018";
    }

    ~ReviewControllerTests() override;

    void SetUp() override
    {

    }

    NiceMock<MockTransactionProvider> _transactionProvider;
    ReviewController _controller;
    EventData _eventData;
    PatientData _patientData;
};

ReviewControllerTests::~ReviewControllerTests() {}

TEST_F(ReviewControllerTests, ConstructorRegisterCallbacksWithProviders)
{
    NiceMock<MockTransactionProvider> transactionProvider;

    EXPECT_CALL(transactionProvider, registerCommitTransactionCallback(_))
            .Times(1);
    EXPECT_CALL(transactionProvider, registerGetTransactionDetailsCallback(_))
            .Times(1);
    EXPECT_CALL(transactionProvider, registerUploadToMerlinCallback(_))
            .Times(1);

    ReviewController controller(transactionProvider);
}

TEST_F(ReviewControllerTests, GetReviewDataCallsProviderMethodWithTransactionAndPatientIdsSupplied)
{
    int transactionId = 1234;
    int patientId = 4567;

    EXPECT_CALL(_transactionProvider, requestTransactionDetails(transactionId, patientId))
            .Times(1);

    _controller.getReviewData(transactionId, patientId);
}


TEST_F(ReviewControllerTests, CommitTransactionCallsProviderMethosWithTransactionAndPatientIdsSupplied)
{
    int transactionId = 1234;
    int patientId = 4567;

    EXPECT_CALL(_transactionProvider, commitTransaction(transactionId, patientId))
            .Times(1);

    _controller.commitTransaction(transactionId, patientId);
}

TEST_F(ReviewControllerTests, CommitAndUploadCallsProviderMethosWithTransactionAndPatientIdsSupplied)
{
    int transactionId = 1234;
    int patientId = 4567;

    EXPECT_CALL(_transactionProvider, commitTransaction(transactionId, patientId))
            .Times(1);

    _controller.commitAndUpload(transactionId, patientId);
}

TEST_F(ReviewControllerTests, TransactionCommittedSignalEmittedWhenCallbackInvokedForTransactionCommit)
{

    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    ReviewController controller(transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(transactionCommitted()));

    transactionProvider._fake._commitTransactionCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(ReviewControllerTests, UploadCompleteSignalEmittedWhenCallbackInvokedForUpload)
{

    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    ReviewController controller(transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(uploadCompleted()));

    transactionProvider._fake._uploadToMerlinCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(ReviewControllerTests, ProviderUploadToMerlinCalledWhenCommitAndUploadAndCommitCallbackCalled)
{
    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    ReviewController controller(transactionProvider);

    int transactionId = 123;
    int patientId = 456;
    controller.commitAndUpload(transactionId, patientId);

    transactionProvider._fake._commitTransactionCallback();

    EXPECT_CALL(transactionProvider, uploadToMerlin(transactionId, patientId))
            .Times(1);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

}

TEST_F(ReviewControllerTests, ReviewDataReadySignalEmittedWhenCallbackInvokedForTransactionDetails)
{

    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    ReviewController controller(transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(reviewDataReady()));

    ON_CALL(transactionProvider, getEventData())
            .WillByDefault(Return(_eventData));

    ON_CALL(transactionProvider, getPatientData())
            .WillByDefault(Return(_patientData));

    transactionProvider._fake._getTransactionDetailsCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(ReviewControllerTests, GetEventDateTimeReturnsDataRetrievedFromTransactionDetailsCallback)
{

    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    ReviewController controller(transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(reviewDataReady()));

    ON_CALL(transactionProvider, getEventData())
            .WillByDefault(Return(_eventData));

    ON_CALL(transactionProvider, getPatientData())
            .WillByDefault(Return(_patientData));

    transactionProvider._fake._getTransactionDetailsCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ("05 MAY 2019 09:02:44", controller.getEventDateTime());
}

TEST_F(ReviewControllerTests, GetSensorCalibrationReturnsDataRetrievedFromTransactionDetailsCallback)
{

    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    ReviewController controller(transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(reviewDataReady()));

    ON_CALL(transactionProvider, getEventData())
            .WillByDefault(Return(_eventData));

    ON_CALL(transactionProvider, getPatientData())
            .WillByDefault(Return(_patientData));

    transactionProvider._fake._getTransactionDetailsCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    SensorCalibration *calibration = controller.getSensorCalibration();
    ASSERT_EQ("05 MAY 2019", calibration->date());
    ASSERT_EQ("09:03:23", calibration->time());
    ASSERT_EQ(33, calibration->systolic());
    ASSERT_EQ(22, calibration->diastolic());
    ASSERT_EQ(25, calibration->mean());
    ASSERT_EQ(30, calibration->reference());
    ASSERT_EQ(70, calibration->heartRate());
    ASSERT_EQ(91, calibration->sensorStrength());
    ASSERT_EQ("/tmp/valid_file.xml", calibration->waveFormFile());

}

TEST_F(ReviewControllerTests, GetCardiacOutputReturnsDataRetrievedFromTransactionDetailsCallback)
{

    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    ReviewController controller(transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(reviewDataReady()));

    ON_CALL(transactionProvider, getEventData())
            .WillByDefault(Return(_eventData));

    ON_CALL(transactionProvider, getPatientData())
            .WillByDefault(Return(_patientData));

    transactionProvider._fake._getTransactionDetailsCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    CardiacOutput *co = controller.getCardiacOutput();
    ASSERT_EQ("05 MAY 2019", co->date());
    ASSERT_EQ("09:04:23", co->time());
    ASSERT_EQ(3.3F, co->cardiacOutput());
    ASSERT_EQ(71, co->heartRate());
    ASSERT_EQ(92, co->sensorStrength());
    ASSERT_EQ("/tmp/valid_file2.xml", co->waveFormFile());

}

TEST_F(ReviewControllerTests, GetRhcValuesReturnsDataRetrievedFromTransactionDetailsCallback)
{

    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    ReviewController controller(transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(reviewDataReady()));

    ON_CALL(transactionProvider, getEventData())
            .WillByDefault(Return(_eventData));

    ON_CALL(transactionProvider, getPatientData())
            .WillByDefault(Return(_patientData));

    transactionProvider._fake._getTransactionDetailsCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    RhcValues *rhc = controller.getRhcValues();

    ASSERT_EQ("05 MAY 2019", rhc->date());
    ASSERT_EQ("09:05:23", rhc->time());
    ASSERT_EQ(30, rhc->raValue());
    ASSERT_EQ(31, rhc->rvSystolic());
    ASSERT_EQ(21, rhc->rvDiastolic());
    ASSERT_EQ(32, rhc->paSystolic());
    ASSERT_EQ(22, rhc->paDiastolic());
    ASSERT_EQ(25, rhc->paMean());
    ASSERT_EQ(11, rhc->pcwpValue());
    ASSERT_EQ(72, rhc->heartRate());
    ASSERT_EQ(93, rhc->sensorStrength());
    ASSERT_EQ("/tmp/valid_file3.xml", rhc->waveFormFile());
}

TEST_F(ReviewControllerTests, GetPatientInfoReturnsDataRetrievedFromTransactionDetailsCallback)
{

    NiceMock<MockTransactionProvider> transactionProvider;
    transactionProvider.DelegateToFake();
    ReviewController controller(transactionProvider);

    QSignalSpy spy(&controller, SIGNAL(reviewDataReady()));

    ON_CALL(transactionProvider, getEventData())
            .WillByDefault(Return(_eventData));

    ON_CALL(transactionProvider, getPatientData())
            .WillByDefault(Return(_patientData));

    transactionProvider._fake._getTransactionDetailsCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(456, controller.getPatientInfo()->patientId());
    ASSERT_EQ("Jane", controller.getPatientInfo()->firstName());
    ASSERT_EQ("Doe", controller.getPatientInfo()->lastName());
    ASSERT_EQ("09 MAR 1945", controller.getPatientInfo()->dob());
    ASSERT_EQ("03 MAR 2018", controller.getPatientInfo()->implantDate());

}
