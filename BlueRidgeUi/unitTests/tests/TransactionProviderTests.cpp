#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmem_command.h>

#include "unitTests/mocks/MockMessageParser.h"
#include "providers/TransactionProvider.h"

using namespace ::testing;

class TransactionProviderTests : public Test
{
public:
    TransactionProviderTests()
    {}

    ~TransactionProviderTests() override;

    void SetUp() override
    {
        _transactionResponse = "{\n\t\"TransactionId\" : \""
                               "1553195112 "
                               "\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

        _transactionDetailsResponse = "{\n\t\"Patient\" : \n{"
                                          "\n\t\"PatientId\": \"733141\","
                                          "\n\t\"PatientKey\": \"\","
                                          "\n\t\"FirstName\": \"John\","
                                          "\n\t\"MiddleInitial\": \"Q\","
                                          "\n\t\"LastName\": \"Doe\","
                                          "\n\t\"PhoneNumber\": \"555-555-5555\","
                                          "\n\t\"Clinician\": \" Dollie Goodwin, MD \","
                                          "\n\t\"Clinic\": \" Fairview MN Heart \","
                                          "\n\t\"DOB\": \"01 Jan 1970\","
                                          "\n\t\"ImplantDate\": \"01 Jan 2018\","
                                          "\n\t\"SerialNumber\": \"HJPSUZ\","
                                          "\n\t\"ImplantDoctor\": \"Dollie Goodwin, MD\","
                                          "\n\t\"CalCode\": \"\","
                                          "\n\t\"BaselineCode\": \"\","
                                          "\n\t\"ImplantLocation\": \"LEFT\","
                                          "\n\t\"UploadPending\": \"YES\" \n},"

                                      "\"Event\" : \n {"
                                          "\n\t\"EventId\": \"1558028197\","
                                          "\n\t\"PatientId\": \"733141\","
                                          "\n\t\"Procedure\": \"Implant\","
                                          "\n\t\"Date\": \"16 May 2019\","
                                          "\n\t\"Time\": \"01:36:37\","
                                          "\n\t\"PAMean\": \"25\","
                                          "\n\t\"Clinic\": \"\","
                                          "\n\t\"Clinician\": \"\","
                                          "\n\t\"Recordings\": [\n {"
                                             "\n\t\"RecordingId\": \"1558028204\","
                                              "\n\t\"UploadPending\": \"YES\","
                                              "\n\t\"Date\": \"16 May 2019\","
                                              "\n\t\"Time\": \"01:36:44\","
                                              "\n\t\"Systolic\": \"44\","
                                              "\n\t\"Diastolic\": \"32\","
                                              "\n\t\"Mean\": \"36\","
                                              "\n\t\"ReferenceSystolic\": \"0\","
                                              "\n\t\"ReferenceDiastolic\": \"0\","
                                              "\n\t\"ReferenceMean\": \"0\","
                                              "\n\t\"AverageHeartRate\": \"70\","
                                              "\n\t\"AverageSignalStrength\": \"90\","
                                              "\n\t\"PatientPosition\": \"Horizontal\","
                                              "\n\t\"Notes\": \"\","
                                              "\n\t\"WaveformData\": \"/tmp/valid_file.xml\""
                                          "\n}\n]\n},"
                                          "\n\"Status\": \"Success\"\n}";

        _uploadResponse = "{\n\t\"TransactionId\" : \""
                          "1553195112 "
                          "\",\n\t\"Count\": \"1\","
                          "\n\t\"Status\" : \"SUCCESS\"\n}\n";

    }

    std::string _transactionResponse;
    std::string _transactionDetailsResponse;
    std::string _uploadResponse;
    std::condition_variable _condition;
};

TransactionProviderTests::~TransactionProviderTests() {}

TEST_F(TransactionProviderTests, ConstructorCallsMessageParserRegisterCallback)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();

    EXPECT_CALL(parser, registerCallback(_))
            .Times(1);

    TransactionProvider provider(parser);
}

TEST_F(TransactionProviderTests, TransactionReadyCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count = 0;
    provider.registerTransactionReadyCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCREATE_TRANSACTION_ID, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(TransactionProviderTests, AllTransactionReadyCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerTransactionReadyCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerTransactionReadyCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCREATE_TRANSACTION_ID, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, AllTransactionReadyCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerTransactionReadyCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerTransactionReadyCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterTransactionReadyCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCREATE_TRANSACTION_ID, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, TransactionAbandonedCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count = 0;
    provider.registerTransactionAbandonedCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eABANDON_TRANSACTION, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(TransactionProviderTests, AllTransactionAbandonedCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerTransactionAbandonedCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerTransactionAbandonedCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eABANDON_TRANSACTION, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, AllTransactionAbandonedCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerTransactionAbandonedCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerTransactionAbandonedCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterTransactionAbandonedCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eABANDON_TRANSACTION, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, CommitTransactionCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count = 0;
    provider.registerCommitTransactionCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCOMMIT_TRANSACTION, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(TransactionProviderTests, AllCommitTransactionCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerCommitTransactionCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerCommitTransactionCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCOMMIT_TRANSACTION, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, AllCommitTransactionCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerCommitTransactionCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerCommitTransactionCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterCommitTransactionCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCOMMIT_TRANSACTION, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, GetTransactionDetailsCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count = 0;
    provider.registerGetTransactionDetailsCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionDetailsResponse));
    parser._fake._func(eGET_TRANSACTION_DETAILS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(TransactionProviderTests, AllGetTransactionDetailsCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerGetTransactionDetailsCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerGetTransactionDetailsCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionDetailsResponse));
    parser._fake._func(eGET_TRANSACTION_DETAILS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, AllGetTransactionDetailsCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerGetTransactionDetailsCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerGetTransactionDetailsCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterGetTransactionDetailsCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionDetailsResponse));
    parser._fake._func(eGET_TRANSACTION_DETAILS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, UploadToMerlinCallbackRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count = 0;
    provider.registerUploadToMerlinCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_uploadResponse));
    parser._fake._func(eUPLOAD_TO_MERLIN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}


TEST_F(TransactionProviderTests, AllUploadToMerlinCallbacksRegisteredWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    provider.registerUploadToMerlinCallback([&]{ count1++;});

    unsigned long count2 = 1;
    provider.registerUploadToMerlinCallback([&]{ count2++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_uploadResponse));
    parser._fake._func(eUPLOAD_TO_MERLIN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, AllUploadToMerlinCallbacksUnregisteredWithProviderIsNotExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count1 = 0;
    unsigned long cmdId1 = provider.registerUploadToMerlinCallback([&]{ count1++;});

    unsigned long count2 = 1;
    unsigned long cmdId2 = provider.registerUploadToMerlinCallback([&]{ count2++; _condition.notify_one(); });

    provider.unregisterUploadToMerlinCallback(cmdId1);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_uploadResponse));
    parser._fake._func(eUPLOAD_TO_MERLIN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count1);
    ASSERT_EQ(2, count2);
}

TEST_F(TransactionProviderTests, RequestTransactionCreationCallsParserSendCommandWithCreateTransactionCommandAndPatientId)
{
    NiceMock<MockMessageParser> parser;
    TransactionProvider provider(parser);

    int patientId = 12345;
    std::string eventType = "NewImplant";
    std::string payload = "{\n\t\"EventType\" : \"" +
                            eventType +
                            "\",\n\t\"PatientId\" : \"" +
                            std::to_string(patientId) +
                            "\"\n}\n";

    EXPECT_CALL(parser, sendCommand(eCREATE_TRANSACTION_ID, payload))
            .Times(1);

    provider.requestTransactionCreation(patientId, eventType);
}

TEST_F(TransactionProviderTests, AbandonTransactionCallsParserSendCommandWithAbandonTransactionCommandAndTransactionId)
{
    NiceMock<MockMessageParser> parser;
    TransactionProvider provider(parser);

    int transactionId = 98765;
    std::string payload = "{\n\t\"TransactionId\" : \"" +
                          std::to_string(transactionId) +
                          "\"\n}\n";

    EXPECT_CALL(parser, sendCommand(eABANDON_TRANSACTION, payload))
            .Times(1);

    provider.abandonTransaction(transactionId);
}

TEST_F(TransactionProviderTests, CommitTransactionCallsParserSendCommandWithCommitTransactionCommandAndTransactionId)
{
    NiceMock<MockMessageParser> parser;
    TransactionProvider provider(parser);

    int transactionId = 98765;
    int patientId = 12345;
    std::string payload = "{\n\t\"PatientId\" : \"" +
                          std::to_string(patientId) +
                          "\",\n\t\"TransactionId\" : \"" +
                          std::to_string(transactionId) +
                          "\"\n}\n";

    EXPECT_CALL(parser, sendCommand(eCOMMIT_TRANSACTION, payload))
            .Times(1);

    provider.commitTransaction(transactionId, patientId);
}

TEST_F(TransactionProviderTests, GetTransactionIdReturnsTheSavedTransactionId)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCREATE_TRANSACTION_ID, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(500));
    }

    ASSERT_EQ(1553195112, provider.getTransactionId());
}

TEST_F(TransactionProviderTests, GetTransactionIdResponseWithNoTransactionIdResetsTransactionId)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    _transactionResponse = "{\n\t\"Status\" : \"SUCCESS\"\n}\n";
    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCREATE_TRANSACTION_ID, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(0, provider.getTransactionId());
}

TEST_F(TransactionProviderTests, AbandonTransactionResponseClearsTransactionId)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eABANDON_TRANSACTION, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(0, provider.getTransactionId());
}

TEST_F(TransactionProviderTests, CommitTransactionResponseClearsTransactionId)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count = 0;
    provider.registerCommitTransactionCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCOMMIT_TRANSACTION, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(200));
    }

    ASSERT_EQ(0, provider.getTransactionId());
}

TEST_F(TransactionProviderTests, HaveValidTransactionReturnsFalseWhenTransactionIdIsZero)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    ASSERT_FALSE(provider.haveValidTransactionId());
}

TEST_F(TransactionProviderTests, HaveValidTransactionReturnsTrueWhenTransactionIdIsNotZero)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count = 0;
    provider.registerTransactionReadyCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionResponse));
    parser._fake._func(eCREATE_TRANSACTION_ID, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.haveValidTransactionId());
}

TEST_F(TransactionProviderTests, GetTransactionDetailsResponseSetsEventDataAndPatientData)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TransactionProvider provider(parser);

    unsigned long count = 0;
    provider.registerGetTransactionDetailsCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_transactionDetailsResponse));
    parser._fake._func(eGET_TRANSACTION_DETAILS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1558028197, provider.getEventData().eventId);
    ASSERT_EQ(1, provider.getEventData().recordings.size());
    ASSERT_EQ(733141, provider.getPatientData().patientId);
}

TEST_F(TransactionProviderTests, RequestTransactionDetailsCallsParserSendComandWithTransactionAndPatientId)
{
    NiceMock<MockMessageParser> parser;
    TransactionProvider provider(parser);

    int transactionId = 98765;
    int patientId = 12345;
    std::string payload = "{\n\t\"PatientId\" : \"" +
                          std::to_string(patientId) +
                          "\",\n\t\"TransactionId\" : \"" +
                          std::to_string(transactionId) +
                          "\"\n}\n";

    EXPECT_CALL(parser, sendCommand(eGET_TRANSACTION_DETAILS, payload))
            .Times(1);

    provider.requestTransactionDetails(transactionId, patientId);
}

TEST_F(TransactionProviderTests, UploadToMerlinCallsParserSendComandWithTransactionAndPatientId)
{
    NiceMock<MockMessageParser> parser;
    TransactionProvider provider(parser);

    int transactionId = 98765;
    int patientId = 12345;
    std::string payload = "{\n\t\"PatientId\" : \"" +
                          std::to_string(patientId) +
                          "\",\n\t\"TransactionId\" : \"" +
                          std::to_string(transactionId) +
                          "\"\n}\n";

    EXPECT_CALL(parser, sendCommand(eUPLOAD_TO_MERLIN, payload))
            .Times(1);

    provider.uploadToMerlin(transactionId, patientId);
}
