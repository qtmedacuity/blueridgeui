#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmem_command.h>

#include "unitTests/mocks/MockTimer.h"
#include "unitTests/mocks/MockMessageParser.h"
#include "providers/WifiStrengthProvider.h"

using namespace ::testing;

class WifiStrengthProviderTests : public Test
{
public:
    WifiStrengthProviderTests()
    {
        _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                         "Up"
                         "\",\n\t\"State\" : \""
                         "Enabled"
                         "\",\n\t\"NetworkName\" : \""
                         "A Network Name"
                         "\",\n\t\"SignalStrength\" : \""
                         "90"
                         "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";
    }

    ~WifiStrengthProviderTests() override;

    std::string _wifiStatusStr;
    std::condition_variable _condition;
};

WifiStrengthProviderTests::~WifiStrengthProviderTests() {}

TEST_F(WifiStrengthProviderTests, ConstructorCallsMessageParserRegisterCallback)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();

    EXPECT_CALL(parser, registerCallback(_))
            .Times(1);

    WifiStrengthProvider provider(timer, parser);
}

TEST_F(WifiStrengthProviderTests, GetStrengthReturnsZeroIfSignalStrengthNotSetInWifiStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"NetworkName\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, provider.getStrength());
}

TEST_F(WifiStrengthProviderTests, GetStrengthReturnsASignalStrengthFromTheWifiStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(90, provider.getStrength());
}

TEST_F(WifiStrengthProviderTests, GetStrengthReturnsADifferentSignalStrengthFromTheWifiStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"NetworkName\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     "5"
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));

    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(5, provider.getStrength());
}

TEST_F(WifiStrengthProviderTests, GetNetworkNameReturnsAnEmptyStringIfNetworkNameNotSetInWifiStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"NetworkName\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("", provider.getNetworkName());
}

TEST_F(WifiStrengthProviderTests, GetNetworkNameReturnsANetworkNameFromTheWifiStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("A Network Name", provider.getNetworkName());
}

TEST_F(WifiStrengthProviderTests, GetNetworkNameReturnsADifferentNetworkNameFromTheWifiStatus)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"NetworkName\" : \""
                     "Another Network"
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ("Another Network", provider.getNetworkName());
}

TEST_F(WifiStrengthProviderTests, IsConnectedReturnsTrueIfWifiStatusIsSetToUp)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.isConnected());
}

TEST_F(WifiStrengthProviderTests, IsConnectedReturnsFalseIfWifiStatusIsSetToDown)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                     "DOWN"
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"NetworkName\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));

    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isConnected());
}

TEST_F(WifiStrengthProviderTests, IsConnectedReturnsFalseIfWifiStatusIsNotSet)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"NetworkName\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));

    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isConnected());
}

TEST_F(WifiStrengthProviderTests, IsEnabledReturnsTrueIfStateIsSetToEnabled)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.isEnabled());
}

TEST_F(WifiStrengthProviderTests, IsEnabledReturnsFalseIfStateIsSetToDisabled)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     "DISABLED"
                     "\",\n\t\"NetworkName\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));

    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isEnabled());
}

TEST_F(WifiStrengthProviderTests, IsEnabledReturnsFalseIfStateIsNotSet)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"NetworkName\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));

    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isEnabled());
}

TEST_F(WifiStrengthProviderTests, IsSetReturnsTrueIfWifiStatusValuesAreSet)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.isSet());
}

TEST_F(WifiStrengthProviderTests, IsSetReturnsFalseIfWifiStatusValuesAreNotSet)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    provider.registerWifiStrengthCallback([&]{ _condition.notify_one(); });

    _wifiStatusStr = "{\n\t\"WiFiStatus\" : \""
                     ""
                     "\",\n\t\"State\" : \""
                     ""
                     "\",\n\t\"NetworkName\" : \""
                     ""
                     "\",\n\t\"SignalStrength\" : \""
                     ""
                     "%\",\n\t\"Status\" : \"SUCCESS\"\n}\n";

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.isSet());
}

TEST_F(WifiStrengthProviderTests, CallbackRegisterWithProviderIsExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    unsigned long count = 0;
    provider.registerWifiStrengthCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
}

TEST_F(WifiStrengthProviderTests, CallbacksRegisteredWithProviderAreExecutedWhenSignalUpdateIsExecuted)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    unsigned long count = 0;
    provider.registerWifiStrengthCallback([&]{ count++; });

    std::string value = "";
    provider.registerWifiStrengthCallback([&]{ value = "a string"; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, count);
    ASSERT_EQ("a string", value);
}

TEST_F(WifiStrengthProviderTests, UnregisteredCallbacksAreNotExecutedByTheProvider)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    unsigned long count = 0;
    unsigned long cmdId = provider.registerWifiStrengthCallback([&]{ count++; });

    std::string value = "";
    provider.registerWifiStrengthCallback([&]{ value = "a string"; _condition.notify_one(); });

    provider.unregisterWifiStrengthCallback(cmdId);

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(_wifiStatusStr));
    parser._fake._func(eGET_WIFI_STATUS, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, count);
    ASSERT_EQ("a string", value);
}

TEST_F(WifiStrengthProviderTests, CallbacksFromParserOfDifferentCommandTypeAreNotHandledByProvider)
{
    NiceMock<MockTimer> timer;
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    WifiStrengthProvider provider(timer, parser);

    unsigned long count = 0;
    provider.registerWifiStrengthCallback([&]{ count++; _condition.notify_one(); });

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("unkown string"));
    parser._fake._func(eUNKNOWN, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        _condition.wait_for(lock, std::chrono::milliseconds(100));
    }

    ASSERT_EQ(0, count);
}

