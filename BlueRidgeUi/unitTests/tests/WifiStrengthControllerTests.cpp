#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "unitTests/mocks/MockWifiStrengthProvider.h"
#include "presentation/WifiStrengthController.h"

using namespace ::testing;

class WifiStrengthControllerTests : public Test
{
public:
    WifiStrengthControllerTests()
        : controller(provider, {{"twoBars",40},{"threeBars",60},{"oneBar",10},{"fourBars",80},{"zeroBars",0},{"fault",-1}})
    {}
    ~WifiStrengthControllerTests() override;

    void SetUp() override
    {
        ON_CALL(provider, isEnabled())
                .WillByDefault(Return(true));
        ON_CALL(provider, isConnected())
                .WillByDefault(Return(true));
        ON_CALL(provider, isSet())
                .WillByDefault(Return(true));

        ON_CALL(provider, getStrength())
                .WillByDefault(Return(40));
    }

    NiceMock<MockWifiStrengthProvider> provider;
    WifiStrengthController controller;
};


WifiStrengthControllerTests::~WifiStrengthControllerTests()
{}

TEST_F(WifiStrengthControllerTests, ConstructorRegistersCallbackWithProvider)
{
    NiceMock<MockWifiStrengthProvider> _provider;

    EXPECT_CALL(_provider, registerWifiStrengthCallback(_))
            .Times(1);

    WifiStrengthController _controller(_provider);
}

TEST_F(WifiStrengthControllerTests, ControllerDefaultsStrengthToZero)
{
    NiceMock<MockWifiStrengthProvider> _provider;
    WifiStrengthController _controller(_provider);

    int expectedStrength = 0;
    ASSERT_EQ(expectedStrength, _controller.strength());
}

TEST_F(WifiStrengthControllerTests, ControllerDefaultsStatusToEmptyString)
{
    NiceMock<MockWifiStrengthProvider> _provider;
    WifiStrengthController _controller(_provider);

    QString expectedStatus("");

    ASSERT_EQ(expectedStatus, _controller.status());
}

TEST_F(WifiStrengthControllerTests, ControllerSetsStrengthAndStatusWhenProviderEmitsWifiStatusReadySignal)
{
    emit controller.wifiDataUpdated();

    ASSERT_EQ(40, controller.strength());
    ASSERT_EQ("twoBars", controller.status());
}

TEST_F(WifiStrengthControllerTests, NetworkNameSetToInvalidIfProviderStatusIsNotSet)
{
    ON_CALL(provider, isSet())
            .WillByDefault(Return(false));

    emit controller.wifiDataUpdated();

    ASSERT_EQ(controller.networkName(), "Invalid");
}

TEST_F(WifiStrengthControllerTests, NetworkNameSetToDisabledIfProviderStatusIsSetAndNotEnabled)
{
    ON_CALL(provider, isSet())
            .WillByDefault(Return(true));
    ON_CALL(provider, isEnabled())
            .WillByDefault(Return(false));

    emit controller.wifiDataUpdated();

    ASSERT_EQ(controller.networkName(), "Disabled");
}

TEST_F(WifiStrengthControllerTests, NetworkNameSetToNoConnectionIfProviderStatusIsSetEnabledAndNotConnected)
{
    ON_CALL(provider, isSet())
            .WillByDefault(Return(true));
    ON_CALL(provider, isEnabled())
            .WillByDefault(Return(true));
    ON_CALL(provider, isConnected())
            .WillByDefault(Return(false));

    emit controller.wifiDataUpdated();

    ASSERT_EQ(controller.networkName(), "No Connection");
}

TEST_F(WifiStrengthControllerTests, setNetworkNameToNetworkNameIfProviderStatusIsSetEnabledAndConnected)
{
    QString expectedName = "Network Name";

    controller.setNetworkName(expectedName);

    ASSERT_EQ(expectedName, controller.networkName());
}

TEST_F(WifiStrengthControllerTests, setNetworkNameEmitsNetworkNameChangedSignalWhenNetworkNameChanged)
{
    QSignalSpy spy(&controller, SIGNAL(networkNameChanged(QString)));

    controller.setNetworkName("Network");

    ASSERT_EQ(1, spy.count());
}

TEST_F(WifiStrengthControllerTests, setStrengthToZeroIfProviderStatusIsNotSet)
{
    int expectedStrength = 0;

    ON_CALL(provider, isSet())
            .WillByDefault(Return(false));

    emit controller.wifiDataUpdated();

    ASSERT_EQ(expectedStrength, controller.strength());
}

TEST_F(WifiStrengthControllerTests, setStrengthSetsStatusToFaultIfProviderStatusIsNotSet)
{
    QString expectedStatus = "fault";

    ON_CALL(provider, isSet())
            .WillByDefault(Return(false));

    emit controller.wifiDataUpdated();

    EXPECT_EQ(expectedStatus, controller.status());
}

TEST_F(WifiStrengthControllerTests, setStrengthSetsStrengthIfProviderStatusIsSetEnabledAndConnected)
{
    int expectedStrength = 60;

    ON_CALL(provider, getStrength())
            .WillByDefault(Return(expectedStrength));

    emit controller.wifiDataUpdated();

    ASSERT_EQ(expectedStrength, controller.strength());
}

TEST_F(WifiStrengthControllerTests, setStrengthEmitsSgnalStrengthChangedIfProviderStatusIsSetEnabledAndConnected)
{
    QSignalSpy spy(&controller, SIGNAL(strengthChanged(int)));

    ON_CALL(provider, getStrength())
            .WillByDefault(Return(60));

    emit controller.wifiDataUpdated();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WifiStrengthControllerTests, setStrengthSetsStatusToCorrespondingStringIfProviderStatusIsSetEnabledAndConnected)
{
    QString expectedStatus = "fourBars";

    ON_CALL(provider, getStrength())
            .WillByDefault(Return(80));

    emit controller.wifiDataUpdated();

    ASSERT_EQ(expectedStatus, controller.status());
}

TEST_F(WifiStrengthControllerTests, setStatusEmitsStatusChangedIfProviderStatusIsSetEnabledAndConnected)
{
    QSignalSpy spy(&controller, SIGNAL(statusChanged(QString)));

    ON_CALL(provider, getStrength())
            .WillByDefault(Return(60));

    emit controller.wifiDataUpdated();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(WifiStrengthControllerTests, strengthPropertySetsTheStrength)
{
    controller.setProperty("strength", 80);

    ASSERT_EQ(controller.strength(), 80);
}

TEST_F(WifiStrengthControllerTests, strengthPropertyReturnsTheStrength)
{
    int expectedStrength = 12;
    controller.setStrength(expectedStrength);
    int actualStrength = controller.property("strength").toInt();

    ASSERT_EQ(actualStrength, expectedStrength);
}

TEST_F(WifiStrengthControllerTests, strengthPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("strength");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("strengthChanged", property.notifySignal().name().data());
}

TEST_F(WifiStrengthControllerTests, statusPropertySetsTheStatus)
{
    controller.setProperty("status", "zeroBars");

    ASSERT_EQ(controller.status(), "zeroBars");
}

TEST_F(WifiStrengthControllerTests, statusPropertyReturnsTheSetStatus)
{
    QString expectedStatus = "oneBar";
    controller.setStatus(expectedStatus);
    QString actualStatus = controller.property("status").toString();

    ASSERT_EQ(actualStatus, expectedStatus);
}

TEST_F(WifiStrengthControllerTests, statusPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("status");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("statusChanged", property.notifySignal().name().data());
}

TEST_F(WifiStrengthControllerTests, networkNamePropertyReturnsTheSetNetworkName)
{
    QString expectedNetwork = "Network";
    controller.setNetworkName(expectedNetwork);
    QString actualNetwork = controller.property("networkName").toString();

    ASSERT_EQ(actualNetwork, expectedNetwork);
}

TEST_F(WifiStrengthControllerTests, networkNamePropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("networkName");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("networkNameChanged", property.notifySignal().name().data());
}


TEST_F(WifiStrengthControllerTests, wifiDataUpdatedSignalEmittedWhenProviderCallbackExcuted)
{
    NiceMock<MockWifiStrengthProvider> _provider;
    _provider.DelegateToFake();
    WifiStrengthController _controller(_provider);

    QSignalSpy spy(&_controller, SIGNAL(wifiDataUpdated()));

    _provider._fake._wifiStrengthCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}
