#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "unitTests/mocks/MockMessageParser.h"
#include "providers/MessageDataProvider.h"

#include <mutex>
#include <condition_variable>

using namespace ::testing;

class TestableMessageDataProvider : public MessageDataProvider
{
public:
    TestableMessageDataProvider(IMessageParser& parser, std::vector<unsigned int> const& validCommandIds = {})
        : MessageDataProvider(parser, validCommandIds),
          _cmdId(static_cast<unsigned int>(-1)),
          _payload("unset")
    {}

    ~TestableMessageDataProvider();

    void handleMessage(unsigned int cmdId, std::string const& payload)
    {
        _cmdId = cmdId;
        _payload = payload;
        _condition.notify_one();
    }

    std::map<unsigned int, std::vector<IMessageDataProvider::Callback>> getCallbacksMap()
    {
        return MessageDataProvider::m_callbacks;
    }

    void sendSignalUpdate(unsigned int cmdId)
    {
        MessageDataProvider::signalUpdate(cmdId);
    }

    unsigned int _cmdId;
    std::string _payload;
    std::condition_variable _condition;
};

TestableMessageDataProvider::~TestableMessageDataProvider() {}

class MessageDataProviderTests : public Test
{
public:
    MessageDataProviderTests(){}
    ~MessageDataProviderTests();
};

MessageDataProviderTests::~MessageDataProviderTests() {}

TEST_F(MessageDataProviderTests, ConstructorCallsMessageParserRegisterCallback)
{
    NiceMock<MockMessageParser> parser;

    EXPECT_CALL(parser, registerCallback(_))
            .Times(1);

    TestableMessageDataProvider provider(parser);
}

TEST_F(MessageDataProviderTests, RegisterCallbackReturnsNegativeOneOnNullCallback)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result = provider.registerCallback(0, nullptr);

    ASSERT_EQ(-1, result);
}

TEST_F(MessageDataProviderTests, RegisterCallbackReturnsFunctionIdForCallback)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result = provider.registerCallback(0, []{});

    ASSERT_EQ(0, result);
}

TEST_F(MessageDataProviderTests, RegisterCallbackReturnsTwoFunctionIdsForCallbacksOfSameType)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result = provider.registerCallback(0, []{});

    ASSERT_EQ(0, result);

    result = provider.registerCallback(0, []{});

    ASSERT_EQ(1, result);
}

TEST_F(MessageDataProviderTests, RegisterCallbackReturnsTwoFunctionIdsForCallbacksOfDifferentType)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result = provider.registerCallback(0, []{});

    ASSERT_EQ(0, result);

    result = provider.registerCallback(1, []{});

    ASSERT_EQ(0, result);
}

TEST_F(MessageDataProviderTests, UnregisterCallbackSetsCallbackToNullPointer)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result = provider.registerCallback(0, []{});

    provider.unregisterCallback(0, result);

    std::map<unsigned int, std::vector<IMessageDataProvider::Callback>> cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 1);
    ASSERT_TRUE(cbMap.at(0).size() == 1);
    ASSERT_EQ(nullptr, cbMap.at(0).at(0));
}

TEST_F(MessageDataProviderTests, UnregisterCallbackSetsCallbackToNullPointerForCallbackOfSameType)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result1 = provider.registerCallback(0, []{});
    unsigned long result2 = provider.registerCallback(0, []{});

    provider.unregisterCallback(0, result1);

    std::map<unsigned int, std::vector<IMessageDataProvider::Callback>> cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 1);
    ASSERT_TRUE(cbMap.at(0).size() == 2);
    ASSERT_EQ(nullptr, cbMap.at(0).at(0));
    ASSERT_NE(nullptr, cbMap.at(0).at(1));
}

TEST_F(MessageDataProviderTests, UnregisterSameCallbackTwiceIsHandledCorrectly)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result1 = provider.registerCallback(0, []{});
    unsigned long result2 = provider.registerCallback(0, []{});

    provider.unregisterCallback(0, result1);

    std::map<unsigned int, std::vector<IMessageDataProvider::Callback>> cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 1);
    ASSERT_TRUE(cbMap.at(0).size() == 2);
    ASSERT_EQ(nullptr, cbMap.at(0).at(0));
    ASSERT_NE(nullptr, cbMap.at(0).at(1));

    provider.unregisterCallback(0, result1);

    cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 1);
    ASSERT_TRUE(cbMap.at(0).size() == 2);
    ASSERT_EQ(nullptr, cbMap.at(0).at(0));
    ASSERT_NE(nullptr, cbMap.at(0).at(1));
}

TEST_F(MessageDataProviderTests, UnregisterAllCallbacksOfSameTypeIsHandledCorrectly)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result1 = provider.registerCallback(0, []{});
    unsigned long result2 = provider.registerCallback(0, []{});

    provider.unregisterCallback(0, result1);

    std::map<unsigned int, std::vector<IMessageDataProvider::Callback>> cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 1);
    ASSERT_TRUE(cbMap.at(0).size() == 2);
    ASSERT_EQ(nullptr, cbMap.at(0).at(0));
    ASSERT_NE(nullptr, cbMap.at(0).at(1));

    provider.unregisterCallback(0, result1);

    cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 1);
    ASSERT_TRUE(cbMap.at(0).size() == 2);
    ASSERT_EQ(nullptr, cbMap.at(0).at(0));
    ASSERT_NE(nullptr, cbMap.at(0).at(1));

    provider.unregisterCallback(0, result2);

    cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 1);
    ASSERT_TRUE(cbMap.at(0).size() == 2);
    ASSERT_EQ(nullptr, cbMap.at(0).at(0));
    ASSERT_EQ(nullptr, cbMap.at(0).at(1));
}

TEST_F(MessageDataProviderTests, UnregisterCallbacksOfDifferentTypeFromTypeStoredInMapIsHandledCorrectly)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result1 = provider.registerCallback(0, []{});
    unsigned long result2 = provider.registerCallback(0, []{});

    provider.unregisterCallback(1, result1);

    std::map<unsigned int, std::vector<IMessageDataProvider::Callback>> cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 1);
    ASSERT_TRUE(cbMap.at(0).size() == 2);
    ASSERT_NE(nullptr, cbMap.at(0).at(0));
    ASSERT_NE(nullptr, cbMap.at(0).at(1));
}

TEST_F(MessageDataProviderTests, UnregisterFirstCallbackOfDifferentTypesIsHandledCorrectly)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result1 = provider.registerCallback(0, []{});
    unsigned long result2 = provider.registerCallback(1, []{});

    provider.unregisterCallback(0, result1);

    std::map<unsigned int, std::vector<IMessageDataProvider::Callback>> cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 2);
    ASSERT_TRUE(cbMap.at(0).size() == 1);
    ASSERT_TRUE(cbMap.at(1).size() == 1);
    ASSERT_EQ(nullptr, cbMap.at(0).at(0));
    ASSERT_NE(nullptr, cbMap.at(1).at(0));
}

TEST_F(MessageDataProviderTests, UnregisterSecondCallbackOfDifferentTypesIsHandledCorrectly)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result1 = provider.registerCallback(0, []{});
    unsigned long result2 = provider.registerCallback(1, []{});

    provider.unregisterCallback(1, result2);

    std::map<unsigned int, std::vector<IMessageDataProvider::Callback>> cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 2);
    ASSERT_TRUE(cbMap.at(0).size() == 1);
    ASSERT_TRUE(cbMap.at(1).size() == 1);
    ASSERT_NE(nullptr, cbMap.at(0).at(0));
    ASSERT_EQ(nullptr, cbMap.at(1).at(0));
}

TEST_F(MessageDataProviderTests, UnregisterAllCallbacksOfDifferentTypesIsHandledCorrectly)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long result1 = provider.registerCallback(0, []{});
    unsigned long result2 = provider.registerCallback(1, []{});

    provider.unregisterCallback(1, result2);
    provider.unregisterCallback(0, result1);

    std::map<unsigned int, std::vector<IMessageDataProvider::Callback>> cbMap = provider.getCallbacksMap();

    ASSERT_TRUE(cbMap.size() == 2);
    ASSERT_TRUE(cbMap.at(0).size() == 1);
    ASSERT_TRUE(cbMap.at(1).size() == 1);
    ASSERT_EQ(nullptr, cbMap.at(0).at(0));
    ASSERT_EQ(nullptr, cbMap.at(1).at(0));
}

TEST_F(MessageDataProviderTests, SignalUpdateSendsSignalToOneCallback)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long count = 0;
    provider.registerCallback(0, [&]{ count++; });

    provider.sendSignalUpdate(0);

    ASSERT_EQ(1, count);
}

TEST_F(MessageDataProviderTests, SignalUpdateSendsTwoSignalsToOneCallback)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long count = 0;
    provider.registerCallback(0, [&]{ count++; });

    provider.sendSignalUpdate(0);
    provider.sendSignalUpdate(0);

    ASSERT_EQ(2, count);
}

TEST_F(MessageDataProviderTests, SignalUpdateSendsTwoSignalsToTwoCallbacksOfSameType)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long count = 0;
    std::string value = "unsent";
    provider.registerCallback(0, [&]{ count++; });
    provider.registerCallback(0, [&]{ value = "signal sent"; });

    provider.sendSignalUpdate(0);

    ASSERT_EQ(1, count);
    ASSERT_EQ("signal sent", value);
}

TEST_F(MessageDataProviderTests, SignalUpdateSendsOneSignalOfOneTypeForTwoRegisteredCallbacks)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long count = 0;
    std::string value = "unsent";
    provider.registerCallback(0, [&]{ count++; });
    provider.registerCallback(1, [&]{ value = "signal sent"; });

    provider.sendSignalUpdate(0);

    ASSERT_EQ(1, count);
    ASSERT_EQ("unsent", value);
}

TEST_F(MessageDataProviderTests, SignalUpdateSendsOneSignalOfDifferentTypeForTwoRegisteredCallbacks)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long count = 0;
    std::string value = "unsent";
    provider.registerCallback(0, [&]{ count++; });
    provider.registerCallback(1, [&]{ value = "signal sent"; });

    provider.sendSignalUpdate(1);

    ASSERT_EQ(0, count);
    ASSERT_EQ("signal sent", value);
}

TEST_F(MessageDataProviderTests, SignalUpdateSendsOneSignalOfEachTypeForTwoRegisteredCallbacks)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long count = 0;
    std::string value = "unsent";
    provider.registerCallback(0, [&]{ count++; });
    provider.registerCallback(1, [&]{ value = "signal sent"; });

    provider.sendSignalUpdate(1);
    provider.sendSignalUpdate(0);

    ASSERT_EQ(1, count);
    ASSERT_EQ("signal sent", value);
}

TEST_F(MessageDataProviderTests, SignalUpdateDoesNotSendSignalToUnregisteredCallback)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long count = 0;
    std::string value = "unsent";
    unsigned long result1 = provider.registerCallback(0, [&]{ count++; });
    unsigned long result2 = provider.registerCallback(1, [&]{ value = "signal sent"; });

    provider.unregisterCallback(0, result1);

    provider.sendSignalUpdate(1);
    provider.sendSignalUpdate(0);

    ASSERT_EQ(0, count);
    ASSERT_EQ("signal sent", value);
}

TEST_F(MessageDataProviderTests, SignalUpdateDoesNotSendSignalToDifferentUnregisteredCallback)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long count = 0;
    std::string value = "unsent";
    unsigned long result1 = provider.registerCallback(0, [&]{ count++; });
    unsigned long result2 = provider.registerCallback(1, [&]{ value = "signal sent"; });

    provider.unregisterCallback(1, result2);

    provider.sendSignalUpdate(1);
    provider.sendSignalUpdate(0);

    ASSERT_EQ(1, count);
    ASSERT_EQ("unsent", value);
}

TEST_F(MessageDataProviderTests, SignalUpdateDoesNotSendUnregisteredSignalsToRegisteredCallbacks)
{
    NiceMock<MockMessageParser> parser;
    TestableMessageDataProvider provider(parser);

    unsigned long count = 0;
    std::string value = "unsent";
    unsigned long result1 = provider.registerCallback(0, [&]{ count++; });
    unsigned long result2 = provider.registerCallback(1, [&]{ value = "signal sent"; });

    provider.sendSignalUpdate(3);

    ASSERT_EQ(0, count);
    ASSERT_EQ("unsent", value);
}

TEST_F(MessageDataProviderTests, HandleMessageReceivesDataOfRegisteredTypeWithoutPayload)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TestableMessageDataProvider provider(parser, {3,});

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>(std::string()));
    parser._fake._func(3, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        provider._condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(3, provider._cmdId);
    ASSERT_EQ("", provider._payload);
}

TEST_F(MessageDataProviderTests, HandleMessageReceivesDataOfRegisteredTypeWithPayload)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TestableMessageDataProvider provider(parser, {4,});

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload string"));
    parser._fake._func(4, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        provider._condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(4, provider._cmdId);
    ASSERT_EQ("payload string", provider._payload);
}

TEST_F(MessageDataProviderTests, HandleMessageReceivesDataOfOneOfTheRegisteredTypesWithPayload)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TestableMessageDataProvider provider(parser, {0,1});

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("payload string"));
    parser._fake._func(0, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        provider._condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, provider._cmdId);
    ASSERT_EQ("payload string", provider._payload);
}

TEST_F(MessageDataProviderTests, HandleMessageReceivesDataOfOtherRegisteredTypeWithPayload)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TestableMessageDataProvider provider(parser, {0,1});

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("a string"));
    parser._fake._func(1, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        provider._condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, provider._cmdId);
    ASSERT_EQ("a string", provider._payload);
}

TEST_F(MessageDataProviderTests, HandleMessageReceivesDataOfBothRegisteredTypeWithPayload)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TestableMessageDataProvider provider(parser, {0,1});

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("first string"));
    parser._fake._func(0, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        provider._condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(0, provider._cmdId);
    ASSERT_EQ("first string", provider._payload);

    payload = std::make_shared<const std::string>("second string");
    parser._fake._func(1, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        provider._condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(1, provider._cmdId);
    ASSERT_EQ("second string", provider._payload);
}

TEST_F(MessageDataProviderTests, HandleMessageWillNotReceivesDataOfUnknownTypeWithPayload)
{
    NiceMock<MockMessageParser> parser;
    parser.DelegateToFake();
    TestableMessageDataProvider provider(parser, {0,1});

    std::shared_ptr<const std::string> payload(std::make_shared<const std::string>("first string"));
    parser._fake._func(5, payload);

    {
        std::mutex _mutex;
        std::unique_lock<std::mutex> lock(_mutex);
        provider._condition.wait_for(lock, std::chrono::milliseconds(250));
    }

    ASSERT_EQ(static_cast<unsigned int>(-1), provider._cmdId);
    ASSERT_EQ("unset", provider._payload);
}
