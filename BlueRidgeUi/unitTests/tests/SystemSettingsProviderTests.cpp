#include "unitTests/mocks/MockMessageParser.h"
#include "unitTests/mocks/MockSensorDataParser.h"
#include "providers/SystemSettingsProvider.h"

#include <condition_variable>
#include <memory>
#include <mutex>
#include <regex>
#include <string>
#include <sstream>

using namespace ::testing;

class SystemSettingsProviderTests : public Test
{
public:
    SystemSettingsProviderTests()
    {}

    void SetUp() override {
        ON_CALL(_msgParser, registerCallback(_))
                .WillByDefault(Invoke(&_msgParser._fake, &FakeMessageParser::registerCallback));
        ON_CALL(_msgParser, unregisterCallback(_))
                .WillByDefault(Invoke(&_msgParser._fake, &FakeMessageParser::unregisterCallback));
        ON_CALL(_sensorParser, registerErrorCallback(_))
                .WillByDefault(Invoke(&_sensorParser._fake, &FakeSensorDataParser::registerErrorCallback));
    }

    ~SystemSettingsProviderTests() override;

    std::string makeRFCommand(bool enable) const {
        return std::string("{\n\t\"RF_State\" : ") + (enable?"\"ON\"":"\"OFF\"") + "\n}\n";
    }

    std::string makeRFResponse(bool enable) const {
        std::string json = std::string("{\n\t\"RF_State\" : ") + (enable?"\"ON\"":"\"OFF\"") + ",\n\t\"Status\" : \"SUCCESS\"\n}\n";
        return json;
    }

    std::string makeConfigurationResponse(std::string const& d, std::string const& t, int pmin, int pmax, int spmin, int spmax, int spdef, int smin, int smax, int dmin, int dmax, int rmin, int rmax, std::string const& l) const {
        // TODO: Backlog Bug QM-182 JSON parsing of cmem_config is broken
        //       A fix to this bug may change the JSON structure that defines the configuration.
        //       Update these tests accordingly.
        std::stringstream ss;
        ss << "{" << std::endl
           << "\t\"Date\": \"" << d << "\"," << std::endl
           << "\t\"Time\": \"" << t << "\"," << std::endl
           << "\t\"PaMean\":{" << std::endl
           << "\t\t\"Min\": \"" << pmin << "\"," << std::endl
           << "\t\t\"Max\": \"" << pmax << "\"" << std::endl
           << "\t}," << std::endl
           << "\t\"SearchPressure\":{" << std::endl
           << "\t\t\"Min\": \"" << spmin << "\"," << std::endl
           << "\t\t\"Max\": \"" << spmax << "\"," << std::endl
           << "\t\t\"StartPressure\": \"" << spdef << "\"" << std::endl
           << "\t}," << std::endl
           << "\t\"SystolicMin\": \"" << smin << "\"," << std::endl
           << "\t\"SystolicMax\": \"" << smax << "\"," << std::endl
           << "\t\"DiastolicMin\": \"" << dmin << "\"," << std::endl
           << "\t\"DiastolicMax\": \"" << dmax << "\"," << std::endl
           << "\t\"ReferenceMeanMin\": \"" << rmin << "\"," << std::endl
           << "\t\"ReferenceMeanMax\": \"" << rmax << "\"," << std::endl
           << "\t\"Language\": \"" << l << "\"," << std::endl
           << "\t\"Status\" : \"SUCCESS\"" << std::endl
           << "}" << std::endl;
        return ss.str();
    }

    IMessageParser::Payload makePayload(std::string const& json) const {
        std::shared_ptr<std::string> payload(new std::string(json));
        return std::move(payload);
    }

    NiceMock<MockMessageParser> _msgParser;
    NiceMock<MockSensorDataParser> _sensorParser;
    std::condition_variable _condition;
};

SystemSettingsProviderTests::~SystemSettingsProviderTests() {}

TEST_F(SystemSettingsProviderTests, ConstructionRegistersCallbacksWithMessageParser)
{
    EXPECT_CALL(_msgParser, registerCallback(_))
            .Times(1);

    SystemSettingsProvider provider(_msgParser, _sensorParser);
}

TEST_F(SystemSettingsProviderTests, RegisterRFStateCallbackReturnsCorrectCallbackIds)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    unsigned long cbId1 = provider.registerRFStateCallback([&]{});
    unsigned long cbId2 = provider.registerRFStateCallback([&]{});

    ASSERT_EQ(0, cbId1);
    ASSERT_EQ(1, cbId2);
}

TEST_F(SystemSettingsProviderTests, RegisteredRFStateCallbacksGetCalledOnEvent)
{
    unsigned int count1(1);
    unsigned int count2(2);

    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerRFStateCallback([&]{ count1++; });
    provider.registerRFStateCallback([&]{ count2++; _condition.notify_all(); });

    _msgParser._fake._func(eGET_RF_STATE, makePayload(makeRFResponse(true)));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(3, count2);
}

TEST_F(SystemSettingsProviderTests, UnregisteredRFStateCallbacksDontGetCalledOnEvent)
{
    unsigned int count1(1);
    unsigned int count2(2);

    SystemSettingsProvider provider(_msgParser, _sensorParser);

    unsigned long cbId = provider.registerRFStateCallback([&]{ count1++; });
    provider.registerRFStateCallback([&]{ count2++; _condition.notify_all(); });

    _msgParser._fake._func(eGET_RF_STATE, makePayload(makeRFResponse(true)));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(3, count2);

    provider.unregisterRFStateCallback(cbId);

    _msgParser._fake._func(eGET_RF_STATE, makePayload(makeRFResponse(false)));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(4, count2);

}

TEST_F(SystemSettingsProviderTests, DefaultRFStateIsFalse)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    ASSERT_FALSE(provider.rfState());
}

TEST_F(SystemSettingsProviderTests, RequestRFStateSendsCorrectMessageToMessageParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_msgParser, sendCommand(eGET_RF_STATE))
            .Times(1);

    provider.requestRFState();
}

TEST_F(SystemSettingsProviderTests, SetRFStateSendsDisabledMessagesToMessageParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    std::string payload(makeRFCommand(false));
    EXPECT_CALL(_msgParser, sendCommand(eSET_RF_STATE,payload.c_str()))
            .Times(1);
    EXPECT_CALL(_msgParser, sendCommand(eGET_RF_STATE))
            .Times(1);

    provider.setRFState(false);
}

TEST_F(SystemSettingsProviderTests, SetRFStateSendsEnabledMessagesToMessageParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    std::string payload(makeRFCommand(true));
    EXPECT_CALL(_msgParser, sendCommand(eSET_RF_STATE,payload.c_str()))
            .Times(1);
    EXPECT_CALL(_msgParser, sendCommand(eGET_RF_STATE))
            .Times(1);

    provider.setRFState(true);
}

TEST_F(SystemSettingsProviderTests, ProviderReceivesRFEnabledMessageFromMessageParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerRFStateCallback([&]() { _condition.notify_all(); });

    ASSERT_FALSE(provider.rfState());

    _msgParser._fake._func(eGET_RF_STATE, makePayload(makeRFResponse(true)));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.rfState());
}

TEST_F(SystemSettingsProviderTests, ProviderReceivesRFDisabledMessageFromMessageParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerRFStateCallback([&]() { _condition.notify_all(); });

    ASSERT_FALSE(provider.rfState());

    _msgParser._fake._func(eGET_RF_STATE, makePayload(makeRFResponse(true)));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_TRUE(provider.rfState());

    _msgParser._fake._func(eGET_RF_STATE, makePayload(makeRFResponse(false)));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_FALSE(provider.rfState());
}

TEST_F(SystemSettingsProviderTests, ProviderDoesNotCallCallbackOnGarbagePayloadData)
{
    unsigned int count(0);
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerRFStateCallback([&]() { count++; _condition.notify_all(); });

    ASSERT_FALSE(provider.rfState());

    _msgParser._fake._func(eGET_RF_STATE, makePayload(std::string("Nerfberger and Spambumpkin")));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::milliseconds(25));
    }

    ASSERT_FALSE(provider.rfState());
    ASSERT_EQ(0, count);
}

TEST_F(SystemSettingsProviderTests, ProviderDoesNotCallCallbackIfJsonTagIsMissingFromPayload)
{
    unsigned int count(0);
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerRFStateCallback([&]() { count++; _condition.notify_all(); });

    ASSERT_FALSE(provider.rfState());

    std::string payloadString = makeRFResponse(true);
    payloadString = std::regex_replace(payloadString, std::regex(std::string("RF_State")), std::string("Yoink"));
    _msgParser._fake._func(eGET_RF_STATE, makePayload(payloadString));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::milliseconds(25));
    }

    ASSERT_FALSE(provider.rfState());
    ASSERT_EQ(0, count);
}

TEST_F(SystemSettingsProviderTests, ProviderDoesNotCallCallbackIfJsonTagFromPayloadFailsToParse)
{
    unsigned int count(0);
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerRFStateCallback([&]() { count++; _condition.notify_all(); });

    ASSERT_FALSE(provider.rfState());

    std::string payloadString = makeRFResponse(true);
    payloadString = std::regex_replace(payloadString, std::regex(std::string("ON")), std::string("Zip"));
    _msgParser._fake._func(eGET_RF_STATE, makePayload(payloadString));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::milliseconds(25));
    }

    ASSERT_FALSE(provider.rfState());
    ASSERT_EQ(0, count);
}

TEST_F(SystemSettingsProviderTests, RegisterSensorDataSocketErrorCallbackRegistersWithSensorDataParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_sensorParser, registerErrorCallback(_))
            .Times(1);

    provider.registerSensorDataSocketErrorCallback([&](DataSocketError){});
}

TEST_F(SystemSettingsProviderTests, RegisteredSensorDataSocketErrorCallbacksGetCalledOnEvent)
{
    unsigned int count(1);
    DataSocketError error(DataSocketError::NoError);

    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerSensorDataSocketErrorCallback([&](DataSocketError err){ error = err; count++; _condition.notify_all(); });

    _sensorParser._fake._errrorCb(DataSocketError::ConnectionFailed);

    ASSERT_EQ(2, count);
    ASSERT_EQ(error, DataSocketError::ConnectionFailed);

    provider.registerSensorDataSocketErrorCallback([&](DataSocketError err){ error = err; count++; _condition.notify_all(); });

    _sensorParser._fake._errrorCb(DataSocketError::ReadFailed);

    ASSERT_EQ(3, count);
    ASSERT_EQ(error, DataSocketError::ReadFailed);
}

TEST_F(SystemSettingsProviderTests, UnregisterSensorDataSocketErrorCallbackUnregistersFromSensorDataParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_sensorParser, unregisterErrorCallback(3))
            .Times(1);

    provider.unregisterSensorDataSocketErrorCallback(3);
}

TEST_F(SystemSettingsProviderTests, ConnectSensorDataSocketCallsSensorDataProvider)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_sensorParser, restart())
            .Times(1);

    provider.connectSensorDataSocket();
}

TEST_F(SystemSettingsProviderTests, DisconnectSensorDataSocketCallsSensorDataProvider)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_sensorParser, stop())
            .Times(1);

    provider.disconnectSensorDataSocket();
}

TEST_F(SystemSettingsProviderTests, EnableSensorDataSourceCallsSensorDataProvider)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_sensorParser, reading(true))
            .Times(1);

    provider.enableSensorDataSource();
}

TEST_F(SystemSettingsProviderTests, DisableSensorDataSourceCallsSensorDataProvider)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_sensorParser, reading(false))
            .Times(1);

    provider.disableSensorDataSource();
}

TEST_F(SystemSettingsProviderTests, EnableSensorDataStreamCallsSensorDataProvider)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_sensorParser, streaming(true))
            .Times(1);

    provider.enableSensorDataStream();
}

TEST_F(SystemSettingsProviderTests, DisableSensorDataStreamCallsSensorDataProvider)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_sensorParser, streaming(false))
            .Times(1);

    provider.disableSensorDataStream();
}
///////////////////////////////////////////////////////////////

TEST_F(SystemSettingsProviderTests, RegisterConfigurationCallbackReturnsCorrectCallbackIds)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    unsigned long cbId1 = provider.registerConfigurationCallback([&]{});
    unsigned long cbId2 = provider.registerConfigurationCallback([&]{});

    ASSERT_EQ(0, cbId1);
    ASSERT_EQ(1, cbId2);
}

TEST_F(SystemSettingsProviderTests, RegisteredConfigurationCallbacksGetCalledOnEvent)
{
    unsigned int count1(1);
    unsigned int count2(2);

    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerConfigurationCallback([&]{ count1++; });
    provider.registerConfigurationCallback([&]{ count2++; _condition.notify_all(); });

    _msgParser._fake._func(eGET_CONFIGURATION, makePayload(makeConfigurationResponse("29 Apr 2019","11:34:21",0,1,2,4,3,5,6,7,8,9,10,"klingon")));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(3, count2);
}

TEST_F(SystemSettingsProviderTests, UnregisteredConfigurationCallbacksDontGetCalledOnEvent)
{
    unsigned int count1(1);
    unsigned int count2(2);

    SystemSettingsProvider provider(_msgParser, _sensorParser);

    unsigned long cbId = provider.registerConfigurationCallback([&]{ count1++; });
    provider.registerConfigurationCallback([&]{ count2++; _condition.notify_all(); });

    _msgParser._fake._func(eGET_CONFIGURATION, makePayload(makeConfigurationResponse("29 Apr 2019","11:34:21",0,1,2,4,3,5,6,7,8,9,10,"klingon")));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(3, count2);

    provider.unregisterConfigurationCallback(cbId);

    _msgParser._fake._func(eGET_CONFIGURATION, makePayload(makeConfigurationResponse("29 Aug 1997","1:19:35",10,11,12,14,13,15,16,17,18,19,20,"borg")));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    ASSERT_EQ(2, count1);
    ASSERT_EQ(4, count2);
}

TEST_F(SystemSettingsProviderTests, DefaultConfigurationIsZeroedOut)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    Configuration cfg(provider.configuration());
    ASSERT_STREQ("", cfg._language.c_str());
    ASSERT_EQ(0, cfg._paMean._min);
    ASSERT_EQ(0, cfg._paMean._max);
    ASSERT_EQ(0, cfg._searchPressure._min);
    ASSERT_EQ(0, cfg._searchPressure._max);
    ASSERT_EQ(0, cfg._searchPressure._default);
    ASSERT_EQ(0, cfg._systolic._min);
    ASSERT_EQ(0, cfg._systolic._max);
    ASSERT_EQ(0, cfg._diastolic._min);
    ASSERT_EQ(0, cfg._diastolic._max);
    ASSERT_EQ(0, cfg._referenceMean._min);
    ASSERT_EQ(0, cfg._referenceMean._max);
    ASSERT_FALSE(cfg._hasDate);
    ASSERT_FALSE(cfg._hasTime);
}

TEST_F(SystemSettingsProviderTests, RequestConfigurationSendsCorrectMessageToMessageParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    EXPECT_CALL(_msgParser, sendCommand(eGET_CONFIGURATION))
            .Times(1);

    provider.requestConfiguration();
}

TEST_F(SystemSettingsProviderTests, ProviderReceivesACorrectConfigurationMessageFromMessageParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerConfigurationCallback([&]{ _condition.notify_all(); });

    _msgParser._fake._func(eGET_CONFIGURATION, makePayload(makeConfigurationResponse("29 Apr 2019","11:34:21",1,2,3,5,4,6,7,8,9,10,11,"klingon")));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    Configuration cfg(provider.configuration());
    ASSERT_STREQ("klingon", cfg._language.c_str());
    ASSERT_EQ(1, cfg._paMean._min);
    ASSERT_EQ(2, cfg._paMean._max);
    ASSERT_EQ(3, cfg._searchPressure._min);
    ASSERT_EQ(5, cfg._searchPressure._max);
    ASSERT_EQ(4, cfg._searchPressure._default);
    ASSERT_EQ(6, cfg._systolic._min);
    ASSERT_EQ(7, cfg._systolic._max);
    ASSERT_EQ(8, cfg._diastolic._min);
    ASSERT_EQ(9, cfg._diastolic._max);
    ASSERT_EQ(10, cfg._referenceMean._min);
    ASSERT_EQ(11, cfg._referenceMean._max);
    ASSERT_TRUE(cfg._hasDate);
    ASSERT_TRUE(cfg._hasTime);
}

TEST_F(SystemSettingsProviderTests, ProviderReceivesADifferentConfigurationMessageFromMessageParser)
{
    SystemSettingsProvider provider(_msgParser, _sensorParser);

    provider.registerConfigurationCallback([&]{ _condition.notify_all(); });

    _msgParser._fake._func(eGET_CONFIGURATION, makePayload(makeConfigurationResponse("29 Aug 1997","1:19:35",10,11,12,14,13,15,16,17,18,19,20,"borg")));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }

    Configuration cfg(provider.configuration());
    ASSERT_STREQ("borg", cfg._language.c_str());
    ASSERT_EQ(10, cfg._paMean._min);
    ASSERT_EQ(11, cfg._paMean._max);
    ASSERT_EQ(12, cfg._searchPressure._min);
    ASSERT_EQ(14, cfg._searchPressure._max);
    ASSERT_EQ(13, cfg._searchPressure._default);
    ASSERT_EQ(15, cfg._systolic._min);
    ASSERT_EQ(16, cfg._systolic._max);
    ASSERT_EQ(17, cfg._diastolic._min);
    ASSERT_EQ(18, cfg._diastolic._max);
    ASSERT_EQ(19, cfg._referenceMean._min);
    ASSERT_EQ(20, cfg._referenceMean._max);
    ASSERT_TRUE(cfg._hasDate);
    ASSERT_TRUE(cfg._hasTime);

    _msgParser._fake._func(eGET_CONFIGURATION, makePayload(makeConfigurationResponse("29 Aug 1997","1:19:35",10,11,12,14,13,15,16,17,18,19,20,"borg")));
    {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);
        _condition.wait_for(lock, std::chrono::seconds(5));
    }
}

