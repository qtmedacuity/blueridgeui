#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "unitTests/mocks/MockCellularStrengthProvider.h"
#include "presentation/CellularStrengthController.h"

using namespace ::testing;

class CellularStrengthControllerTests : public Test
{
public:
    CellularStrengthControllerTests()
        : controller(provider, {{"threeBars",50},{"twoBars",25},{"oneBar",5},{"fourBars",75},{"zeroBars",0}})
    {}
    ~CellularStrengthControllerTests() override;

    void SetUp() override
    {
        ON_CALL(provider, isEnabled())
                .WillByDefault(Return(true));
        ON_CALL(provider, isConnected())
                .WillByDefault(Return(true));
        ON_CALL(provider, isSet())
                .WillByDefault(Return(true));

        ON_CALL(provider, getStrength())
                .WillByDefault(Return(40));
    }

    NiceMock<MockCellularStrengthProvider> provider;
    CellularStrengthController controller;
};

CellularStrengthControllerTests::~CellularStrengthControllerTests()
{}

TEST_F(CellularStrengthControllerTests, ConstructorRegistersCallbackWithProvider)
{
    NiceMock<MockCellularStrengthProvider> _provider;

    EXPECT_CALL(_provider, registerCellularStrengthCallback(_))
            .Times(1);

    CellularStrengthController _controller(_provider);
}

TEST_F(CellularStrengthControllerTests, ControllerDefaultsStrengthToZero)
{
    NiceMock<MockCellularStrengthProvider> _provider;
    CellularStrengthController _controller(_provider);

    int expectedStrength = 0;
    ASSERT_EQ(expectedStrength, _controller.strength());
}

TEST_F(CellularStrengthControllerTests, ControllerDefaultsStatusToEmptyString)
{
    NiceMock<MockCellularStrengthProvider> _provider;
    CellularStrengthController _controller(_provider);

    QString expectedStatus("");

    ASSERT_EQ(expectedStatus, _controller.status());
}
TEST_F(CellularStrengthControllerTests, ControllerSetsStrengthAndStatusWhenProviderEmitsCellStatusReadySignal)
{
    emit controller.cellularDataUpdated();

    ASSERT_EQ(40, controller.strength());
    ASSERT_EQ("twoBars", controller.status());
}

TEST_F(CellularStrengthControllerTests, CarrierSetToNotInitializedIfProviderStatusIsNotSet)
{
    ON_CALL(provider, isSet())
            .WillByDefault(Return(false));

    emit controller.cellularDataUpdated();

    ASSERT_EQ(controller.carrier(), "Not Initialized");
}

TEST_F(CellularStrengthControllerTests, CarrierSetToDisabledIfProviderStatusIsSetAndNotEnabled)
{
    ON_CALL(provider, isSet())
            .WillByDefault(Return(true));
    ON_CALL(provider, isEnabled())
            .WillByDefault(Return(false));

    emit controller.cellularDataUpdated();

    ASSERT_EQ(controller.carrier(), "Disabled");
}

TEST_F(CellularStrengthControllerTests, CarrierSetToNoCarrierIfProviderStatusIsSetEnabledAndNotConnected)
{
    ON_CALL(provider, isSet())
            .WillByDefault(Return(true));
    ON_CALL(provider, isEnabled())
            .WillByDefault(Return(true));
    ON_CALL(provider, isConnected())
            .WillByDefault(Return(false));

    emit controller.cellularDataUpdated();

    ASSERT_EQ(controller.carrier(), "No Carrier");
}

TEST_F(CellularStrengthControllerTests, setCarrierToCarrierIfProviderStatusIsSetEnabledAndConnected)
{
    QString expectedName = "Network Name";

    controller.setCarrier(expectedName);

    ASSERT_EQ(expectedName, controller.carrier());
}

TEST_F(CellularStrengthControllerTests, setCarrierEmitsCarrierChangedSignalWhenCarrierChanged)
{
    QSignalSpy spy(&controller, SIGNAL(carrierChanged(QString)));

    controller.setCarrier("Network");

    ASSERT_EQ(1, spy.count());
}

TEST_F(CellularStrengthControllerTests, setStrengthToZeroIfProviderStatusIsNotSet)
{
    int expectedStrength = 0;

    ON_CALL(provider, isSet())
            .WillByDefault(Return(false));

    emit controller.cellularDataUpdated();

    ASSERT_EQ(expectedStrength, controller.strength());
}

TEST_F(CellularStrengthControllerTests, setStrengthSetsStatusToEmptyStringIfProviderStatusIsNotSet)
{
    QString expectedStatus = "";

    ON_CALL(provider, isSet())
            .WillByDefault(Return(false));

    emit controller.cellularDataUpdated();

    ASSERT_EQ(expectedStatus, controller.status());

}

TEST_F(CellularStrengthControllerTests, setStrengthSetsStrengthIfProviderStatusIsSetEnabledAndConnected)
{
    int expectedStrength = 60;

    ON_CALL(provider, getStrength())
            .WillByDefault(Return(expectedStrength));

    emit controller.cellularDataUpdated();

    ASSERT_EQ(expectedStrength, controller.strength());
}

TEST_F(CellularStrengthControllerTests, setStrengthEmitsSgnalStrengthChangedIfProviderStatusIsSetEnabledAndConnected)
{
    QSignalSpy spy(&controller, SIGNAL(strengthChanged(int)));

    ON_CALL(provider, getStrength())
            .WillByDefault(Return(60));

    emit controller.cellularDataUpdated();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(CellularStrengthControllerTests, setStrengthSetsStatusToCorrespondingStringIfProviderStatusIsSetEnabledAndConnected)
{
    QString expectedStatus = "fourBars";

    ON_CALL(provider, getStrength())
            .WillByDefault(Return(80));

    emit controller.cellularDataUpdated();

    ASSERT_EQ(expectedStatus, controller.status());
}

TEST_F(CellularStrengthControllerTests, setStatusEmitsStatusChangedIfProviderStatusIsSetEnabledAndConnected)
{
    QSignalSpy spy(&controller, SIGNAL(statusChanged(QString)));

    ON_CALL(provider, getStrength())
            .WillByDefault(Return(60));

    emit controller.cellularDataUpdated();

    ASSERT_EQ(spy.count(), 1);
}

TEST_F(CellularStrengthControllerTests, strengthPropertySetsTheStrength)
{
    controller.setProperty("strength", 80);

    ASSERT_EQ(controller.strength(), 80);
}

TEST_F(CellularStrengthControllerTests, strengthPropertyReturnsTheStrength)
{
    int expectedStrength = 12;
    controller.setStrength(expectedStrength);
    int actualStrength = controller.property("strength").toInt();

    ASSERT_EQ(actualStrength, expectedStrength);
}

TEST_F(CellularStrengthControllerTests, strengthPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("strength");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("strengthChanged", property.notifySignal().name().data());
}

TEST_F(CellularStrengthControllerTests, statusPropertySetsTheStatus)
{
    controller.setProperty("status", "zeroBars");

    ASSERT_EQ(controller.status(), "zeroBars");
}

TEST_F(CellularStrengthControllerTests, statusPropertyReturnsTheSetStatus)
{
    QString expectedStatus = "oneBar";
    controller.setStatus(expectedStatus);
    QString actualStatus = controller.property("status").toString();

    ASSERT_EQ(actualStatus, expectedStatus);
}

TEST_F(CellularStrengthControllerTests, statusPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("status");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("statusChanged", property.notifySignal().name().data());
}

TEST_F(CellularStrengthControllerTests, carrierPropertyReturnsTheSetcarrier)
{
    QString expectedCarrier = "Carrier";
    controller.setCarrier(expectedCarrier);
    QString actualCarrier = controller.property("carrier").toString();

    ASSERT_EQ(actualCarrier, expectedCarrier);
}

TEST_F(CellularStrengthControllerTests, carrierPropertyHasNotifySignal)
{
    int propertyIndex = controller.metaObject()->indexOfProperty("carrier");
    QMetaProperty property = controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("carrierChanged", property.notifySignal().name().data());
}

TEST_F(CellularStrengthControllerTests, cellularDataUpdatedSignalEmittedWhenProviderCallbackExcuted)
{
    NiceMock<MockCellularStrengthProvider> _provider;
    _provider.DelegateToFake();
    CellularStrengthController _controller(_provider);

    QSignalSpy spy(&_controller, SIGNAL(cellularDataUpdated()));

    _provider._fake._cellularStrengthCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(spy.count(), 1);
}
