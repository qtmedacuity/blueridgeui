#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "providers/PaMeanData.h"

using namespace ::testing;

class PaMeanDataTests : public Test
{

public:
    PaMeanDataTests() {}
    ~PaMeanDataTests() override;

    void SetUp() override
    {
        _jsonRoot["PAMean"] = "45";
        _jsonRoot["Offset"] = "10";
        _jsonRoot["Systolic"] = "38";
        _jsonRoot["Diastolic"] = "19";
        _jsonRoot["Mean"] = "24";
    }

    Json::Value _jsonRoot;
};

PaMeanDataTests::~PaMeanDataTests() {}


TEST_F(PaMeanDataTests, DefaultConstructorDefaultsValuesToZero)
{
    PaMeanData paMeanData;

    ASSERT_EQ(0, paMeanData.paMean);
    ASSERT_EQ(0, paMeanData.offset);
    ASSERT_EQ(0, paMeanData.systolic);
    ASSERT_EQ(0, paMeanData.diastolic);
    ASSERT_EQ(0, paMeanData.mean);
}

TEST_F(PaMeanDataTests, ConstructorSetsPatientDataValuesFromJsonResponseValues)
{

    PaMeanData paMeanData(&_jsonRoot);

    ASSERT_EQ(45, paMeanData.paMean);
    ASSERT_EQ(10, paMeanData.offset);
    ASSERT_EQ(38, paMeanData.systolic);
    ASSERT_EQ(19, paMeanData.diastolic);
    ASSERT_EQ(24, paMeanData.mean);
}

TEST_F(PaMeanDataTests, ConstructorUsesDefaultsIfDataMissingFromJsonResponse)
{
    Json::Value jsonRoot;
    PaMeanData paMeanData(&jsonRoot);

    ASSERT_EQ(0, paMeanData.paMean);
    ASSERT_EQ(0, paMeanData.offset);
    ASSERT_EQ(0, paMeanData.systolic);
    ASSERT_EQ(0, paMeanData.diastolic);
    ASSERT_EQ(0, paMeanData.mean);
}


