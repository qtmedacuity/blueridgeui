#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QSignalSpy>

#include "unitTests/mocks/MockSearchbarProvider.h"
#include "presentation/SearchbarController.h"

using namespace ::testing;

class SearchbarControllerTests : public Test
{
public:
    SearchbarControllerTests()
        : _controller(_searchProvider)
    {}

    ~SearchbarControllerTests();

    NiceMock<MockSearchbarProvider> _searchProvider;
    SearchbarController _controller;
};

SearchbarControllerTests::~SearchbarControllerTests() {}

TEST_F(SearchbarControllerTests, ConstructorRegistersCallbacksWithProviders)
{
    NiceMock<MockSearchbarProvider> searchProvider;

    EXPECT_CALL(searchProvider, registerSearchPressureCallback(_))
            .Times(1);

    EXPECT_CALL(searchProvider, registerAutoSearchingCallback(_))
            .Times(1);

    SearchbarController controller(searchProvider);
}

TEST_F(SearchbarControllerTests, StartAutoSearchingCallsSearchbarProviderSetAutoSearching)
{
   EXPECT_CALL(_searchProvider, setAutoSearching())
           .Times(1);

   _controller.startAutoSearching();
}

TEST_F(SearchbarControllerTests, SearchValueReturnsSearchPressureValueFromSearchbarProvider)
{
    unsigned int value = 30;

    ON_CALL(_searchProvider, searchPressure())
            .WillByDefault(Return(value));

    ASSERT_EQ(static_cast<int>(value), _controller.searchValue());
}

TEST_F(SearchbarControllerTests, SearchValueReturnsADifferentSearchPressureValueFromSearchbarProvider)
{
    unsigned int value = 150;

    ON_CALL(_searchProvider, searchPressure())
            .WillByDefault(Return(value));

    ASSERT_EQ(static_cast<int>(value), _controller.searchValue());
}

TEST_F(SearchbarControllerTests, SetSearchValueCallsSearchbarProviderSetSearchPressureWithCorrectValue)
{
    int value = 50;

    EXPECT_CALL(_searchProvider, setSearchPressure(static_cast<unsigned int>(value)))
            .Times(1);

    _controller.setSearchValue(value);
}

TEST_F(SearchbarControllerTests, SetSearchValueSetsSearchbarProviderSearchPressureToZeroIfValueLessThanZero)
{
    int value = -100;

    EXPECT_CALL(_searchProvider, setSearchPressure(0))
            .Times(1);

    _controller.setSearchValue(value);
}

TEST_F(SearchbarControllerTests, AutoSearchingReturnsFalseIfSearchbarProviderIsNotAutoSearching)
{
    ON_CALL(_searchProvider, autoSearching())
            .WillByDefault(Return(false));

    ASSERT_FALSE(_controller.autoSearching());
}

TEST_F(SearchbarControllerTests, AutoSearchingReturnsTrueeIfSearchbarProviderIsAutoSearching)
{
    ON_CALL(_searchProvider, autoSearching())
            .WillByDefault(Return(true));

    ASSERT_TRUE(_controller.autoSearching());
}

TEST_F(SearchbarControllerTests, SearchValueChangedSignalEmittedWhenProviderExecutesCallback)
{
    NiceMock<MockSearchbarProvider> searchProvider;
    searchProvider.DelegateToFake();
    SearchbarController controller(searchProvider);

    QSignalSpy spy(&controller, SIGNAL(searchValueChanged(int)));

    searchProvider._fake._searchPressureCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(SearchbarControllerTests, SearchbarProviderSearchPressureCalledWhenProviderExecutesCallback)
{
    NiceMock<MockSearchbarProvider> searchProvider;
    searchProvider.DelegateToFake();
    SearchbarController controller(searchProvider);

    EXPECT_CALL(searchProvider, searchPressure())
            .Times(1);

    searchProvider._fake._searchPressureCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
}

TEST_F(SearchbarControllerTests, AutoSearchingChangedSignalEmittedWhenProviderExecutesCallback)
{
    NiceMock<MockSearchbarProvider> searchProvider;
    searchProvider.DelegateToFake();
    SearchbarController controller(searchProvider);

    QSignalSpy spy(&controller, SIGNAL(autoSearchingChanged(bool)));

    searchProvider._fake._autoSearchingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);

    ASSERT_EQ(1, spy.count());
}

TEST_F(SearchbarControllerTests, SearchbarProviderAutoSearchingCalledWhenProviderExecutesCallback)
{
    NiceMock<MockSearchbarProvider> searchProvider;
    searchProvider.DelegateToFake();
    SearchbarController controller(searchProvider);

    EXPECT_CALL(searchProvider, autoSearching())
            .Times(1);

    searchProvider._fake._autoSearchingCallback();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents);
}

TEST_F(SearchbarControllerTests, SearchValuePropertyReturnsTheSearchValue)
{
    int expectedValue = 20;

    ON_CALL(_searchProvider, searchPressure())
            .WillByDefault(Return(static_cast<unsigned int>(expectedValue)));

    int actualValue = _controller.property("searchValue").toInt();

    ASSERT_EQ(expectedValue, actualValue);
}

TEST_F(SearchbarControllerTests, SearchValuePropertySetsTheSearchValueInTheProvider)
{
    int searchPressure = 80;

    EXPECT_CALL(_searchProvider, setSearchPressure(static_cast<unsigned int>(searchPressure)))
            .Times(1);

    _controller.setProperty("searchValue", searchPressure);
}

TEST_F(SearchbarControllerTests, SearchValuePropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("searchValue");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("searchValueChanged", property.notifySignal().name().data());
}

TEST_F(SearchbarControllerTests, AutoSearchingPropertyReturnsAutoSearchingValue)
{
    bool expectedValue = true;

    ON_CALL(_searchProvider, autoSearching())
            .WillByDefault(Return(expectedValue));

    bool value = _controller.property("autoSearching").toBool();

    ASSERT_EQ(value, expectedValue);
}

TEST_F(SearchbarControllerTests, AutoSearchingPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("autoSearching");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("autoSearchingChanged", property.notifySignal().name().data());
}

TEST_F(SearchbarControllerTests, UserEnabledReturnsFalseByDefault)
{
    ASSERT_FALSE(_controller.userEnabled());
}

TEST_F(SearchbarControllerTests, UserEnabledReturnsTrueWhenEnabled)
{
    _controller.setUserEnabled(true);
    ASSERT_TRUE(_controller.userEnabled());
}

TEST_F(SearchbarControllerTests, UserEnabledChangedSignalEmittedWhenUserEnabled)
{
    QSignalSpy spy(&_controller, SIGNAL(userEnabledChanged(bool)));

    _controller.setUserEnabled(true);

    ASSERT_EQ(1, spy.count());
}

TEST_F(SearchbarControllerTests, UserEnabledPropertyReturnsTheUserEnabledValue)
{
    bool value = _controller.property("userEnabled").toBool();
    ASSERT_FALSE(value);
}

TEST_F(SearchbarControllerTests, UserEnabledPropertySetsTheValueOfUserEnabled)
{
    _controller.setProperty("userEnabled", true);

    ASSERT_TRUE(_controller.userEnabled());
}

TEST_F(SearchbarControllerTests, UserEnabledPropertyHasNotifySignal)
{
    int propertyIndex = _controller.metaObject()->indexOfProperty("userEnabled");
    QMetaProperty property = _controller.metaObject()->property(propertyIndex);

    ASSERT_STREQ("userEnabledChanged", property.notifySignal().name().data());
}
