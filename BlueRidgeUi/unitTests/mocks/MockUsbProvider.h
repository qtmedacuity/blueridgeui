#pragma once
#include <gmock/gmock.h>
#include "providers/IUsbProvider.h"

using ::testing::_;
using ::testing::Invoke;

class FakeUsbProvider : public IUsbProvider
{
public:

    FakeUsbProvider()
        : _usbCallback(nullptr)
    {}

    ~FakeUsbProvider();

    unsigned long registerUsbCallback(std::function<void()> func)
    {
        _usbCallback = func;
        return 0;
    }
    void unregisterUsbCallback(unsigned long) {}
    void requestUsbStatus() {}
    bool isPresent() { return true; }
    bool isValid() { return true; }
    std::string getSerialNumber() { return ""; }
    std::string getCalCode() {return ""; }
    std::string getBaseline() {return ""; }

public:
    std::function<void()> _usbCallback;
};

FakeUsbProvider::~FakeUsbProvider() {}

class MockUsbProvider : public IUsbProvider
{
public:
    MOCK_METHOD1(registerUsbCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterUsbCallback, void(unsigned long));
    MOCK_METHOD0(requestUsbStatus, void());
    MOCK_METHOD0(isPresent, bool());
    MOCK_METHOD0(isValid, bool());
    MOCK_METHOD0(getSerialNumber, std::string());
    MOCK_METHOD0(getCalCode, std::string());
    MOCK_METHOD0(getBaseline, std::string());

    void DelegateToFake()
    {
        ON_CALL(*this, registerUsbCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeUsbProvider::registerUsbCallback));
    }

public:
    FakeUsbProvider _fake;
};
