#pragma once
#include <gmock/gmock.h>
#include "providers/ICellularStrengthProvider.h"


using ::testing::_;
using ::testing::Invoke;

class FakeCellularStrengthProvider : public ICellularStrengthProvider
{
public:

    FakeCellularStrengthProvider()
        : _cellularStrengthCallback(nullptr)
    {}

    ~FakeCellularStrengthProvider();

    unsigned long registerCellularStrengthCallback(std::function<void()> func)
    {
        _cellularStrengthCallback = func;
        return 0;
    }
    void unregisterCellularStrengthCallback(unsigned long) {}
    int getStrength() { return 10; }
    std::string getCarrier() { return "";}
    bool isConnected() { return true;}
    bool isEnabled() { return true;}
    bool isSet() {return true;}

    std::function<void()> _cellularStrengthCallback;
};

FakeCellularStrengthProvider::~FakeCellularStrengthProvider() {}

class MockCellularStrengthProvider : public ICellularStrengthProvider
{
public:
    MOCK_METHOD1(registerCellularStrengthCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterCellularStrengthCallback, void(unsigned long));
    MOCK_METHOD0(getStrength, int());
    MOCK_METHOD0(getCarrier, std::string());
    MOCK_METHOD0(isConnected, bool());
    MOCK_METHOD0(isEnabled, bool());
    MOCK_METHOD0(isSet, bool());

    void DelegateToFake()
    {
        ON_CALL(*this, registerCellularStrengthCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeCellularStrengthProvider::registerCellularStrengthCallback));
    }
public:
    FakeCellularStrengthProvider _fake;
};
