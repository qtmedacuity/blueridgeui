#pragma once
#include <gmock/gmock.h>
#include "providers/IMessageParser.h"

#include <cmem_command.h>

using ::testing::_;
using ::testing::Invoke;

class FakeMessageParser : public IMessageParser
{
public:

    FakeMessageParser()
        : _func(nullptr)
    {}

    unsigned long registerCallback(Callback func) override
    {
       _func = func;
       return 0;
    }

    void unregisterCallback(unsigned long) override
    {}

    unsigned long registerErrorCallback(ErrorCallback func) override
    {
        _errrorCb = func;
        return 0;
    }

    virtual void unregisterErrorCallback(unsigned long) override
    {}

    bool sendCommand(unsigned int cmd) override
    {
        _func(cmd, _data);
        return true;
    }

    bool sendCommand(unsigned int cmd, std::string const&) override
    {
        _func(cmd, _data);
        return true;
    }

    bool sendCommand(unsigned int cmd, Payload) override
    {
        _func(cmd, _data);
        return true;
    }

public:
    Callback _func;
    ErrorCallback _errrorCb;
    Payload _data;
};

class MockMessageParser : public IMessageParser
{
public:

    MOCK_METHOD1(registerCallback, unsigned long(Callback));
    MOCK_METHOD1(unregisterCallback, void(unsigned long));
    MOCK_METHOD1(registerErrorCallback, unsigned long(ErrorCallback));
    MOCK_METHOD1(unregisterErrorCallback, void(unsigned long));
    MOCK_METHOD1(sendCommand, bool(unsigned int));
    MOCK_METHOD2(sendCommand, bool(unsigned int, std::string const&));
    MOCK_METHOD2(sendCommand, bool(unsigned int, Payload));

    void DelegateToFake()
    {
        ON_CALL(*this, registerCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeMessageParser::registerCallback));

        ON_CALL(*this, unregisterCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeMessageParser::unregisterCallback));

        ON_CALL(*this, registerErrorCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeMessageParser::registerErrorCallback));

        ON_CALL(*this, unregisterErrorCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeMessageParser::unregisterErrorCallback));

        ON_CALL(*this, sendCommand(_))
                .WillByDefault(Invoke(&_fake, static_cast<bool(FakeMessageParser::*)(unsigned int)>(&FakeMessageParser::sendCommand)));

        ON_CALL(*this, sendCommand(_,""))
                .WillByDefault(Invoke(&_fake, static_cast<bool(FakeMessageParser::*)(unsigned int, std::string const&)>(&FakeMessageParser::sendCommand)));

        ON_CALL(*this, sendCommand(_,_fake._data))
                .WillByDefault(Invoke(&_fake, static_cast<bool(FakeMessageParser::*)(unsigned int, IMessageParser::Payload)>(&FakeMessageParser::sendCommand)));
   }

public:
    FakeMessageParser _fake;
};
