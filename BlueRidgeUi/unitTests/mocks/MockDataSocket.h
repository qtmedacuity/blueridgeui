#pragma once
#include <gmock/gmock.h>
#include "providers/IDataSocket.h"

class MockDataSocket : public IDataSocket
{
public:
    MOCK_METHOD4(connection, int(const char*, int, int&, std::string&));
    MOCK_METHOD0(disconnect, int());
    MOCK_METHOD2(reconnect, int(int&, std::string&));
    MOCK_METHOD4(readbytes, ssize_t(void*, size_t, int&, std::string&));
    MOCK_METHOD4(writebytes, ssize_t(void const*, size_t, int&, std::string&));
};
