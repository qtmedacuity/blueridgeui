#pragma once
#include <gmock/gmock.h>
#include "providers/IRecordingProvider.h"
#include "providers/PaMeanData.h"
#include "providers/CardiacOutputData.h"

using ::testing::_;
using ::testing::Invoke;

class FakeRecordingProvider : public IRecordingProvider
{

public:
    FakeRecordingProvider()
        : _recordingStatusCallback(nullptr),
          _readyToRecordCallback(nullptr),
          _takeReadingCallback(nullptr),
          _defaultPaMeanCallback(nullptr),
          _setPaMeanCallback(nullptr),
          _saveRecordingCallback(nullptr),
          _deleteRecordingCallback(nullptr),
          _setCardiacOutputCallback(nullptr),
          _setRhcValuesCallback(nullptr)
    {}

    unsigned long registerRecordingStatusCallback(std::function<void()> func)
    {
       _recordingStatusCallback = func;
       return 0;
    }

    unsigned long registerReadyToRecordCallback(std::function<void()> func)
    {
       _readyToRecordCallback = func;
       return 0;
    }
    unsigned long registerTakeReadingCallback(std::function<void()> func)
    {
       _takeReadingCallback = func;
       return 0;
    }
    unsigned long registerGetPaMeanCallback(std::function<void()> func)
    {
       _defaultPaMeanCallback = func;
       return 0;
    }
    unsigned long registerSetPaMeanCallback(std::function<void()> func)
    {
       _setPaMeanCallback = func;
       return 0;
    }
    unsigned long registerSaveRecordingCallback(std::function<void()> func)
    {
        _saveRecordingCallback = func;
        return 0;
    }

    unsigned long registerDeleteRecordingCallback(std::function<void()> func)
    {
        _deleteRecordingCallback = func;
        return 0;
    }

    unsigned long registerSetCardiacOutputCallback(std::function<void()> func)
    {
        _setCardiacOutputCallback = func;
        return 0;
    }

    unsigned long registerSetRhcValuesCallback(std::function<void()> func)
    {
        _setRhcValuesCallback = func;
        return 0;
    }

    void unregisterRecordingStatusCallback(unsigned long) {}
    void unregisterReadyToRecordCallback(unsigned long) {}
    void unregisterTakeReadingCallback(unsigned long) {}
    void unregisterGetPaMeanCallback(unsigned long) {}
    void unregisterSetPaMeanCallback(unsigned long) {}
    void unregisterSaveRecordingCallback(unsigned long) {}
    void unregisterDeleteRecordingCallback(unsigned long) {}
    void unregisterSetCardiacOutputCallback(unsigned long) {}
    void unregisterSetRhcValuesCallback(unsigned long) {}
    void requestRecordingStatus(std::string) {}
    void takeReading(int) {}
    void requestPaMean() {}
    void setPaMean(int, int) {}
    void saveRecording(int, RecordingData const&) {}
    void deleteRecording(int, int) {}
    void setCardiacOutput(int, CardiacOutputData) {}
    void setRhcValues(int, RhcData const&) {}

    bool isReadyToRecord() const {return true;}
    int getRecordingId()const {return 0;}
    std::string getWaveFormFileName() const {return {""}; }
    int getPaMean() const {return 30;}
    PaMeanData getPaMeanData() const
    {
        PaMeanData data;
        data.offset = 10;
        data.systolic = 38;
        data.diastolic = 19;
        data.mean = 24;
        return data;
    }
    RecordingData getSavedRecording() const
    {
        RecordingData recording;
        return recording;
    }
    int getDeletedRecordingId() const {return 0;}

    std::function<void()> _recordingStatusCallback;
    std::function<void()> _readyToRecordCallback;
    std::function<void()> _takeReadingCallback;
    std::function<void()> _defaultPaMeanCallback;
    std::function<void()> _setPaMeanCallback;
    std::function<void()> _saveRecordingCallback;
    std::function<void()> _deleteRecordingCallback;
    std::function<void()> _setCardiacOutputCallback;
    std::function<void()> _setRhcValuesCallback;
};

class MockRecordingProvider : public IRecordingProvider
{
public:
    MOCK_METHOD1(registerRecordingStatusCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterRecordingStatusCallback, void(unsigned long));
    MOCK_METHOD1(registerReadyToRecordCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterReadyToRecordCallback, void(unsigned long));
    MOCK_METHOD1(registerTakeReadingCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterTakeReadingCallback, void(unsigned long));
    MOCK_METHOD1(registerGetPaMeanCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterGetPaMeanCallback, void(unsigned long));
    MOCK_METHOD1(registerSetPaMeanCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterSetPaMeanCallback, void(unsigned long));
    MOCK_METHOD1(registerSaveRecordingCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterSaveRecordingCallback, void(unsigned long));
    MOCK_METHOD1(registerDeleteRecordingCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterDeleteRecordingCallback, void(unsigned long));
    MOCK_METHOD1(registerSetCardiacOutputCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterSetCardiacOutputCallback, void(unsigned long));
    MOCK_METHOD1(registerSetRhcValuesCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterSetRhcValuesCallback, void(unsigned long));

    MOCK_METHOD1(requestRecordingStatus, void(std::string));
    MOCK_METHOD1(takeReading, void(int));
    MOCK_METHOD0(requestPaMean, void());
    MOCK_METHOD2(setPaMean, void(int, int));
    MOCK_METHOD2(saveRecording, void(int, RecordingData const&));
    MOCK_METHOD2(deleteRecording, void(int, int));
    MOCK_METHOD2(setCardiacOutput, void(int, CardiacOutputData));
    MOCK_METHOD2(setRhcValues, void(int, RhcData const&));

    MOCK_CONST_METHOD0(isReadyToRecord, bool());
    MOCK_CONST_METHOD0(getRecordingId, int());
    MOCK_CONST_METHOD0(getWaveFormFileName, std::string());
    MOCK_CONST_METHOD0(getPaMean, int());
    MOCK_CONST_METHOD0(getPaMeanData, PaMeanData());
    MOCK_CONST_METHOD0(getSavedRecording, RecordingData());
    MOCK_CONST_METHOD0(getDeletedRecordingId, int());

    void DelegateToFake()
    {
        ON_CALL(*this, registerRecordingStatusCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeRecordingProvider::registerRecordingStatusCallback));

        ON_CALL(*this, registerReadyToRecordCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeRecordingProvider::registerReadyToRecordCallback));

        ON_CALL(*this, registerTakeReadingCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeRecordingProvider::registerTakeReadingCallback));

        ON_CALL(*this, registerGetPaMeanCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeRecordingProvider::registerGetPaMeanCallback));

        ON_CALL(*this, registerSetPaMeanCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeRecordingProvider::registerSetPaMeanCallback));

        ON_CALL(*this, registerSaveRecordingCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeRecordingProvider::registerSaveRecordingCallback));

        ON_CALL(*this, registerDeleteRecordingCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeRecordingProvider::registerDeleteRecordingCallback));

        ON_CALL(*this, registerSetCardiacOutputCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeRecordingProvider::registerSetCardiacOutputCallback));

        ON_CALL(*this, registerSetRhcValuesCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeRecordingProvider::registerSetRhcValuesCallback));
    }

public:
    FakeRecordingProvider _fake;
};
