#pragma once
#include <gmock/gmock.h>

#include "providers/ISearchbarProvider.h"

using ::testing::_;
using ::testing::Invoke;

class FakeSearchbarProvider : public ISearchbarProvider
{
public:
    FakeSearchbarProvider()
        : _searchPressureCallback(nullptr),
          _autoSearchingCallback(nullptr)
    {}
    ~FakeSearchbarProvider();

    unsigned long registerSearchPressureCallback(Callback func)
    {
        _searchPressureCallback = func;
        return 0;
    }

    void unregisterSearchPressureCallback(unsigned long) {}
    unsigned long registerAutoSearchingCallback(Callback func)
    {
        _autoSearchingCallback = func;
        return 0;
    }

    void unregisterAutoSearchingCallback(unsigned long) {}
    unsigned int searchPressure() const { return 0; }
    void setSearchPressure(unsigned int) {}
    bool autoSearching() const { return true; }
    void setAutoSearching() {}

public:
    Callback _searchPressureCallback;
    Callback _autoSearchingCallback;
};

FakeSearchbarProvider::~FakeSearchbarProvider() {}

class MockSearchbarProvider : public ISearchbarProvider
{
public:
    MOCK_METHOD1(registerSearchPressureCallback, unsigned long(Callback));
    MOCK_METHOD1(unregisterSearchPressureCallback, void(unsigned long));
    MOCK_METHOD1(registerAutoSearchingCallback, unsigned long(Callback));
    MOCK_METHOD1(unregisterAutoSearchingCallback, void(unsigned long));
    MOCK_CONST_METHOD0(searchPressure, unsigned int());
    MOCK_METHOD1(setSearchPressure, void(unsigned int));
    MOCK_CONST_METHOD0(autoSearching, bool());
    MOCK_METHOD0(setAutoSearching, void());

    void DelegateToFake()
    {
        ON_CALL(*this, registerSearchPressureCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSearchbarProvider::registerSearchPressureCallback));

        ON_CALL(*this, registerAutoSearchingCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSearchbarProvider::registerAutoSearchingCallback));
    }

public:
    FakeSearchbarProvider _fake;
};
