#pragma once
#include <gmock/gmock.h>
#include "providers/ITransactionProvider.h"
#include "providers/EventData.h"

using ::testing::_;
using ::testing::Invoke;

class FakeTransactionProvider : public ITransactionProvider
{
public:
    FakeTransactionProvider()
        : _transactionReadyCallback(nullptr),
          _transactionAbandonedCallback(nullptr),
          _commitTransactionCallback(nullptr)
    {}

    unsigned long registerTransactionReadyCallback(std::function<void()> func)
    {
        _transactionReadyCallback = func;
        return 0;
    }

    void unregisterTransactionReadyCallback(unsigned long) {}

    unsigned long registerTransactionAbandonedCallback(std::function<void()> func)
    {
        _transactionAbandonedCallback = func;
        return 0;
    }

    void unregisterTransactionAbandonedCallback(unsigned long) {}

    unsigned long registerCommitTransactionCallback(std::function<void()> func)
    {
        _commitTransactionCallback = func;
        return 0;
    }

    void unregisterCommitTransactionCallback(unsigned long) {}

    unsigned long registerGetTransactionDetailsCallback(std::function<void()> func)
    {
        _getTransactionDetailsCallback = func;
        return 0;
    }

    void unregisterGetTransactionDetailsCallback(unsigned long) {}

    unsigned long registerUploadToMerlinCallback(std::function<void()> func)
    {
        _uploadToMerlinCallback = func;
        return 0;
    }

    void unregisterUploadToMerlinCallback(unsigned long) {}

    void requestTransactionCreation(int, std::string) {}
    void abandonTransaction(int) {}
    void commitTransaction(int, int) {}
    void requestTransactionDetails(int, int) {}
    void uploadToMerlin(int, int) {}

    int getTransactionId() const { return 0; }
    bool haveValidTransactionId() const { return true; }
    EventData getEventData() const
    {
        EventData eventData;
        return eventData;
    }
    PatientData getPatientData() const
    {
        PatientData patientData;
        return patientData;
    }

public:
    std::function<void()> _transactionReadyCallback;
    std::function<void()> _transactionAbandonedCallback;
    std::function<void()> _commitTransactionCallback;
    std::function<void()> _getTransactionDetailsCallback;
    std::function<void()> _uploadToMerlinCallback;
};

class MockTransactionProvider : public ITransactionProvider
{
public:
    MOCK_METHOD1(registerTransactionReadyCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterTransactionReadyCallback, void(unsigned long));
    MOCK_METHOD1(registerTransactionAbandonedCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterTransactionAbandonedCallback, void(unsigned long));
    MOCK_METHOD1(registerCommitTransactionCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterCommitTransactionCallback, void(unsigned long));
    MOCK_METHOD1(registerGetTransactionDetailsCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterGetTransactionDetailsCallback, void(unsigned long));
    MOCK_METHOD1(registerUploadToMerlinCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterUploadToMerlinCallback, void(unsigned long));
    MOCK_METHOD2(requestTransactionCreation, void(int, std::string));
    MOCK_METHOD1(abandonTransaction, void(int));
    MOCK_METHOD2(commitTransaction, void(int, int));
    MOCK_METHOD2(requestTransactionDetails, void(int, int));
    MOCK_METHOD2(uploadToMerlin, void(int, int));
    MOCK_CONST_METHOD0(getTransactionId, int());
    MOCK_CONST_METHOD0(haveValidTransactionId, bool());
    MOCK_CONST_METHOD0(getEventData, EventData());
    MOCK_CONST_METHOD0(getPatientData, PatientData());

    void DelegateToFake()
    {
        ON_CALL(*this, registerTransactionReadyCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeTransactionProvider::registerTransactionReadyCallback));

        ON_CALL(*this, registerTransactionAbandonedCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeTransactionProvider::registerTransactionAbandonedCallback));

        ON_CALL(*this, registerCommitTransactionCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeTransactionProvider::registerCommitTransactionCallback));

        ON_CALL(*this, registerGetTransactionDetailsCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeTransactionProvider::registerGetTransactionDetailsCallback));

        ON_CALL(*this, registerUploadToMerlinCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeTransactionProvider::registerUploadToMerlinCallback));

    }

public:
    FakeTransactionProvider _fake;
};
