#pragma once
#include <gmock/gmock.h>

#include "providers/ISystemSettingsProvider.h"

using ::testing::_;
using ::testing::Invoke;

class FakeSystemSettingsProvider : public ISystemSettingsProvider
{
public:
    FakeSystemSettingsProvider()
        : _rfStateCallback(nullptr),
          _dataSocketErrorCallback(nullptr)
    {}

    unsigned long registerRFStateCallback(SettingsChangedCallback func) override
    {
        _rfStateCallback = func;
        return 0;
    }
    void unregisterRFStateCallback(unsigned long) override {}

    unsigned long registerSensorDataSocketErrorCallback(DataSocketErrorCallback func) override
    {
        _dataSocketErrorCallback = func;
        return 0;
    }
    void unregisterSensorDataSocketErrorCallback(unsigned long) override {}

    unsigned long registerConfigurationCallback(SettingsChangedCallback func) override
    {
        _configurationCallback = func;
        return 0;
    }
    void unregisterConfigurationCallback(unsigned long) override {}

    void requestRFState() override {}
    bool rfState() const override {return true;}
    void setRFState(bool) override {}
    void connectSensorDataSocket() override {}
    void disconnectSensorDataSocket() override {}
    void enableSensorDataStream() override {}
    void disableSensorDataStream() override {}
    void enableSensorDataSource() override {}
    void disableSensorDataSource() override {}
    void requestConfiguration() override {}
    Configuration const& configuration() const override
    {
        return m_config;
    }


public:
    SettingsChangedCallback _rfStateCallback;
    DataSocketErrorCallback _dataSocketErrorCallback;
    SettingsChangedCallback _configurationCallback;
    Configuration m_config;
};

class MockSystemSettingsProvider : public ISystemSettingsProvider
{
public:
    MOCK_METHOD1(registerRFStateCallback, unsigned long(SettingsChangedCallback));
    MOCK_METHOD1(unregisterRFStateCallback, void(unsigned long));
    MOCK_METHOD1(registerSensorDataSocketErrorCallback, unsigned long(DataSocketErrorCallback));
    MOCK_METHOD1(unregisterSensorDataSocketErrorCallback, void(unsigned long));
    MOCK_METHOD1(registerConfigurationCallback, unsigned long(SettingsChangedCallback));
    MOCK_METHOD1(unregisterConfigurationCallback, void(unsigned long));

    MOCK_METHOD0(requestRFState, void());
    MOCK_CONST_METHOD0(rfState, bool());
    MOCK_METHOD1(setRFState, void(bool));
    MOCK_METHOD0(connectSensorDataSocket, void());
    MOCK_METHOD0(disconnectSensorDataSocket, void());
    MOCK_METHOD0(enableSensorDataStream, void());
    MOCK_METHOD0(disableSensorDataStream, void());
    MOCK_METHOD0(enableSensorDataSource, void());
    MOCK_METHOD0(disableSensorDataSource, void());
    MOCK_METHOD0(requestConfiguration, void());
    MOCK_CONST_METHOD0(configuration, Configuration const&());


    void DelegateToFake()
    {
        ON_CALL(*this, registerRFStateCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::registerRFStateCallback));

        ON_CALL(*this, unregisterRFStateCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::unregisterRFStateCallback));

        ON_CALL(*this, registerSensorDataSocketErrorCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::registerSensorDataSocketErrorCallback));

        ON_CALL(*this, unregisterSensorDataSocketErrorCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::unregisterSensorDataSocketErrorCallback));

        ON_CALL(*this, registerConfigurationCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::registerConfigurationCallback));

        ON_CALL(*this, unregisterConfigurationCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::unregisterConfigurationCallback));

        ON_CALL(*this, requestRFState())
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::requestRFState));

        ON_CALL(*this, rfState())
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::rfState));

        ON_CALL(*this, setRFState(_))
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::setRFState));

        ON_CALL(*this, connectSensorDataSocket())
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::connectSensorDataSocket));

        ON_CALL(*this, disconnectSensorDataSocket())
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::disconnectSensorDataSocket));

        ON_CALL(*this, enableSensorDataStream())
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::enableSensorDataStream));

        ON_CALL(*this, disableSensorDataStream())
                .WillByDefault(Invoke(&_fake, &FakeSystemSettingsProvider::disableSensorDataStream));
    }

public:
    FakeSystemSettingsProvider _fake;
};
