#pragma once
#include <gmock/gmock.h>
#include "providers/ITimer.h"

using ::testing::_;
using ::testing::Invoke;

class FakeTimer : public ITimer
{
public:

    FakeTimer() : _func(nullptr) {}

    void start(std::chrono::microseconds const&, Callback) override {}
    void start(unsigned int, Callback func) override
    {
        _func = func;
        _func();
    }
    void start(std::chrono::microseconds const&) override {}
    void start(unsigned int) override {}
    void stop() override {}

public:
    Callback _func;
};

class MockTimer : public ITimer
{
public:
    MOCK_METHOD2(start, void(std::chrono::microseconds const&, Callback));
    MOCK_METHOD2(start, void(unsigned int, Callback));
    MOCK_METHOD1(start, void(std::chrono::microseconds const&));
    MOCK_METHOD1(start, void(unsigned int));
    MOCK_METHOD0(stop,  void());

    void DelegateToFake()
    {
        ON_CALL(*this, start(1,_))
                .WillByDefault(Invoke(&_fake, static_cast<void(FakeTimer::*)(unsigned int, Callback)>(&FakeTimer::start)));
   }

    FakeTimer _fake;
};
