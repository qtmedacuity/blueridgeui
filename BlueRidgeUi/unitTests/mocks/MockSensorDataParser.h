#pragma once
#include <gmock/gmock.h>

#include "providers/ISensorDataParser.h"

using ::testing::_;
using ::testing::Invoke;

class FakeSensorDataParser : public ISensorDataParser
{
public:
    FakeSensorDataParser()
        : _dataCb(nullptr),
          _dataRateCb(nullptr),
          _data(nullptr)
    {}

    ~FakeSensorDataParser() override {}

    unsigned long registerDataCallback(DataCallback func) override
    {
       _dataCb = func;
       return 0;
    }

    void unregisterDataCallback(unsigned long) override
    {}

    unsigned long registerDataRateCallback(DataRateCallback func) override
    {
       _dataRateCb = func;
       return 0;
    }

    void unregisterDataRateCallback(unsigned long) override
    {}

    unsigned long registerErrorCallback(DataSocketErrorCallback func) override
    {
        _errrorCb = func;
        return 0;
    }

    virtual void unregisterErrorCallback(unsigned long) override
    {}

    void start(unsigned int dataRateHz, unsigned int) override
    {
        _dataRateCb(dataRateHz);
        _dataCb(_data);
    }

    void restart() override
    {}

    void stop() override
    {}

    void streaming(bool) override
    {}

    void reading(bool) override
    {}

public:
    DataCallback _dataCb;
    DataRateCallback _dataRateCb;
    DataSocketErrorCallback _errrorCb;
    DataListPtr _data;
};

class MockSensorDataParser : public ISensorDataParser
{
public:
    MOCK_METHOD1(registerDataCallback, unsigned long(DataCallback));
    MOCK_METHOD1(unregisterDataCallback, void(unsigned long));
    MOCK_METHOD1(registerDataRateCallback, unsigned long(DataRateCallback));
    MOCK_METHOD1(unregisterDataRateCallback, void(unsigned long));
    MOCK_METHOD1(registerErrorCallback, unsigned long(DataSocketErrorCallback));
    MOCK_METHOD1(unregisterErrorCallback, void(unsigned long));
    MOCK_METHOD2(start, void(unsigned int, unsigned int));
    MOCK_METHOD0(restart, void());
    MOCK_METHOD0(stop, void());
    MOCK_METHOD1(streaming, void(bool));
    MOCK_METHOD1(reading, void(bool));

    void DelegateToFake()
    {
        ON_CALL(*this, registerDataCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataParser::registerDataCallback));

        ON_CALL(*this, registerDataRateCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataParser::registerDataRateCallback));

        ON_CALL(*this, registerErrorCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataParser::registerErrorCallback));

        ON_CALL(*this, start(_, _))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataParser::start));
   }

public:
    FakeSensorDataParser _fake;
};
