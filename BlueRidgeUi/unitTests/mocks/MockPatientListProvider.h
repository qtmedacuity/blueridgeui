#pragma once
#include <gmock/gmock.h>
#include "providers/IPatientListProvider.h"

using ::testing::_;
using ::testing::Invoke;

class FakePatientListProvider : public IPatientListProvider
{
public:
    FakePatientListProvider()
        : _patientListCallback(nullptr),
          _refreshCallback(nullptr)
    {}

    ~FakePatientListProvider();

    unsigned long registerPatientListCallback(std::function<void()> func)
    {
        _patientListCallback = func;
        return 0;
    }

    void unregisterPatientListCallback(unsigned long) {}

    unsigned long registerRefreshCallback(std::function<void()> func)
    {
        _refreshCallback = func;
        return 0;
    }

    void unregisterRefreshCallback(unsigned long) {}

    void requestPatientsWaitingForImplant() {}
    void requestPatientsWithImplant() {}
    void requestAllPatients() {}
    void requestRefresh() {}

    std::string refreshDate() { return ""; }
    std::string refreshTime() { return ""; }
    std::vector<PatientData> patientList() { return {PatientData()}; }

public:
    std::function<void()> _patientListCallback;
    std::function<void()> _refreshCallback;
};

FakePatientListProvider::~FakePatientListProvider() {}

class MockPatientListProvider : public IPatientListProvider
{
public:
    MOCK_METHOD1(registerPatientListCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterPatientListCallback, void(unsigned long));
    MOCK_METHOD1(registerRefreshCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterRefreshCallback, void(unsigned long));

    MOCK_METHOD0(requestPatientsWaitingForImplant, void());
    MOCK_METHOD0(requestPatientsWithImplant, void());
    MOCK_METHOD0(requestAllPatients, void());
    MOCK_METHOD0(requestRefresh, void());

    MOCK_METHOD0(patientList, std::vector<PatientData>());
    MOCK_METHOD0(refreshDate, std::string());
    MOCK_METHOD0(refreshTime, std::string());

    void DelegateToFake()
    {
        ON_CALL(*this, registerPatientListCallback(_))
                .WillByDefault(Invoke(&_fake, &FakePatientListProvider::registerPatientListCallback));

        ON_CALL(*this, registerRefreshCallback(_))
                .WillByDefault(Invoke(&_fake, &FakePatientListProvider::registerRefreshCallback));
    }

public:
    FakePatientListProvider _fake;
};
