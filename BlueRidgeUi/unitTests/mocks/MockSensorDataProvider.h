#pragma once
#include <gmock/gmock.h>

#include "providers/ISensorDataProvider.h"

using ::testing::_;
using ::testing::Invoke;

class FakeSensorDataProvider : public ISensorDataProvider
{
public:
    FakeSensorDataProvider()
        : _dataRateCallback(nullptr),
          _sensorDataPointCallback(nullptr),
          _signalStrengthCallback(nullptr),
          _meanValueCallback(nullptr),
          _pulsatility(false),
          _signalStrength(90)
    {}

    unsigned long registerDataRateCallback(UIntCallback func) override
    {
        _dataRateCallback = func;
        return 0;
    }

    void unregisterDataRateCallback(unsigned long) override {}

    unsigned long registerSensorDataPointCallback(ULongDoubleCallback func) override
    {
        _sensorDataPointCallback = func;
        return 0;
    }

    void unregisterSensorDataPointCallback(unsigned long) override {}

    unsigned long registerSignalStrengthCallback(UIntCallback func) override
    {
        _signalStrengthCallback = func;
        return 0;
    }

    void unregisterSignalStrengthCallback(unsigned long) override {}

    unsigned long registerMeanValueCallback(UIntCallback func) override
    {
        _meanValueCallback = func;
        return 0;
    }

    void unregisterMeanValueCallback(unsigned long) override {}

    unsigned long registerSystolicValuesCallback(UIntUIntCallback func) override
    {
        _systolicPressuresCallback = func;
        return 0;
    }

    void unregisterSystolicValuesCallback(unsigned long) override {}

    unsigned long registerHeartRateCallback(UIntCallback func) override
    {
        _heartRateCallback = func;
        return 0;
    }

    void unregisterHeartRateCallback(unsigned long) override {}

    unsigned long registerPulsatilityCallback(BoolCallback func) override
    {
        _pulsatilityCallback = func;
        return 0;
    }

    void unregisterPulsatilityCallback(unsigned long)override {}

    void sendSignalStrength() override
    {
        _signalStrengthCallback(_signalStrength);
    }

    void sendMean()override {}
    void sendSystolicDiastolic()override {}
    void sendHeartRate()override {}
    void sendPulsatility() override
    {
        _pulsatilityCallback(_pulsatility);
    }

public:
    UIntCallback _dataRateCallback;
    ULongDoubleCallback _sensorDataPointCallback;
    UIntCallback _signalStrengthCallback;
    UIntCallback _meanValueCallback;
    UIntUIntCallback _systolicPressuresCallback;
    UIntCallback _heartRateCallback;
    BoolCallback _pulsatilityCallback;
    bool _pulsatility;
    unsigned int _signalStrength;
};

class MockSensorDataProvider : public ISensorDataProvider
{
public:
    MOCK_METHOD1(registerDataRateCallback, unsigned long(UIntCallback));
    MOCK_METHOD1(unregisterDataRateCallback, void(unsigned long));
    MOCK_METHOD1(registerSensorDataPointCallback, unsigned long(ULongDoubleCallback));
    MOCK_METHOD1(unregisterSensorDataPointCallback, void(unsigned long));
    MOCK_METHOD1(registerSignalStrengthCallback, unsigned long(UIntCallback));
    MOCK_METHOD1(unregisterSignalStrengthCallback, void(unsigned long));
    MOCK_METHOD1(registerMeanValueCallback, unsigned long(UIntCallback));
    MOCK_METHOD1(unregisterMeanValueCallback, void(unsigned long));
    MOCK_METHOD1(registerSystolicValuesCallback, unsigned long(UIntUIntCallback));
    MOCK_METHOD1(unregisterSystolicValuesCallback, void(unsigned long));
    MOCK_METHOD1(registerHeartRateCallback, unsigned long(UIntCallback));
    MOCK_METHOD1(unregisterHeartRateCallback, void(unsigned long));
    MOCK_METHOD1(registerPulsatilityCallback, unsigned long(BoolCallback));
    MOCK_METHOD1(unregisterPulsatilityCallback, void(unsigned long));

    MOCK_METHOD0(sendSignalStrength, void());
    MOCK_METHOD0(sendMean, void());
    MOCK_METHOD0(sendSystolicDiastolic, void());
    MOCK_METHOD0(sendHeartRate, void());
    MOCK_METHOD0(sendPulsatility, void());

    void DelegateToFake()
    {
        ON_CALL(*this, registerDataRateCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataProvider::registerDataRateCallback));

        ON_CALL(*this, registerSensorDataPointCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataProvider::registerSensorDataPointCallback));

        ON_CALL(*this, registerSignalStrengthCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataProvider::registerSignalStrengthCallback));

        ON_CALL(*this, registerMeanValueCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataProvider::registerMeanValueCallback));

        ON_CALL(*this, registerSystolicValuesCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataProvider::registerSystolicValuesCallback));

        ON_CALL(*this, registerHeartRateCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataProvider::registerHeartRateCallback));

        ON_CALL(*this, registerPulsatilityCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeSensorDataProvider::registerPulsatilityCallback));

        ON_CALL(*this, sendSignalStrength())
                .WillByDefault(Invoke(&_fake, &FakeSensorDataProvider::sendSignalStrength));

        ON_CALL(*this, sendPulsatility())
                .WillByDefault(Invoke(&_fake, &FakeSensorDataProvider::sendPulsatility));
    }

public:
    FakeSensorDataProvider _fake;
};
