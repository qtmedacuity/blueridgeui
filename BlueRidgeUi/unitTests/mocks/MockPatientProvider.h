#pragma once
#include <gmock/gmock.h>
#include "providers/PatientData.h"
#include "providers/IPatientProvider.h"

using ::testing::_;
using ::testing::Invoke;

class FakePatientProvider : public IPatientProvider
{
public:
    FakePatientProvider()
        : _patientSavedCallback(nullptr),
          _listDataCallback(nullptr)
    {}

    ~FakePatientProvider();

    unsigned long registerPatientSavedCallback(std::function<void()> func)
    {
        _patientSavedCallback = func;
        return 0;
    }

    void unregisterPatientSavedCallback(unsigned long) {}

    unsigned long registerListDataCallback(std::function<void()> func)
    {
        _listDataCallback = func;
        return 0;
    }

    void unregisterListDataCallback(unsigned long) {}

    void savePatient(int, PatientData const&) {}
    void requestListData(std::string) {}
    int getPatientId() const { return 0; }
    std::vector<std::string> getListData(std::string) const { return {""}; }

public:
    std::function<void()> _patientSavedCallback;
    std::function<void()> _listDataCallback;
};

FakePatientProvider::~FakePatientProvider() {}

class MockPatientProvider : public IPatientProvider
{
public:
    MOCK_METHOD1(registerPatientSavedCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterPatientSavedCallback, void(unsigned long));
    MOCK_METHOD1(registerListDataCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterListDataCallback, void(unsigned long));
    MOCK_METHOD2(savePatient, void(int, PatientData const&));
    MOCK_METHOD1(requestListData, void(std::string));
    MOCK_CONST_METHOD0(getPatientId, int());
    MOCK_CONST_METHOD1(getListData, std::vector<std::string>(std::string));

    void DelegateToFake()
    {
        ON_CALL(*this, registerPatientSavedCallback(_))
                .WillByDefault(Invoke(&_fake, &FakePatientProvider::registerPatientSavedCallback));

        ON_CALL(*this, registerListDataCallback(_))
                .WillByDefault(Invoke(&_fake, &FakePatientProvider::registerListDataCallback));
    }

public:
    FakePatientProvider _fake;
};
