#pragma once
#include <gmock/gmock.h>
#include "providers/IWifiStrengthProvider.h"

using ::testing::_;
using ::testing::Invoke;

class FakeWifiStrengthProvider : public IWifiStrengthProvider
{
public:
    FakeWifiStrengthProvider()
        : _wifiStrengthCallback(nullptr)
    {}

    ~FakeWifiStrengthProvider();

    unsigned long registerWifiStrengthCallback(std::function<void()> func)
    {
        _wifiStrengthCallback = func;
        return 0;
    }
    void unregisterWifiStrengthCallback(unsigned long) {}
    int getStrength() { return 0; }
    std::string getNetworkName() { return ""; }
    bool isConnected() { return true; }
    bool isEnabled() { return true; }
    bool isSet() { return true; }

public:
    std::function<void()> _wifiStrengthCallback;
};

FakeWifiStrengthProvider::~FakeWifiStrengthProvider() {}

class MockWifiStrengthProvider : public IWifiStrengthProvider
{
public:
    MOCK_METHOD1(registerWifiStrengthCallback, unsigned long(std::function<void()>));
    MOCK_METHOD1(unregisterWifiStrengthCallback, void(unsigned long));
    MOCK_METHOD0(getStrength, int());
    MOCK_METHOD0(getNetworkName, std::string());
    MOCK_METHOD0(isConnected, bool());
    MOCK_METHOD0(isEnabled, bool());
    MOCK_METHOD0(isSet, bool());

    void DelegateToFake()
    {
        ON_CALL(*this, registerWifiStrengthCallback(_))
                .WillByDefault(Invoke(&_fake, &FakeWifiStrengthProvider::registerWifiStrengthCallback));
    }

public:
    FakeWifiStrengthProvider _fake;
};
