pragma Singleton

import QtQuick 2.12

QtObject
{
    readonly property color darkerGray: "#13161C"
    readonly property color darkerGray2: "#1A1E26"
    readonly property color darkGray: "#212731"
    readonly property color darkGrayAlt: "#222731"
    readonly property color mediumGray: "#63666A"
    readonly property color mediumGray2: "#4A4A4A"
    readonly property color mediumGray3: "#21262F"
    readonly property color lightGray: "#D8D8D8"
    readonly property color lightGrayAlt: "#C7C8C9"
    readonly property color lighterGray: "#A6A8AD"
    readonly property color gray: "#898989"
    readonly property color grayAlt: "#404B50"

    readonly property color darkBlue: "#005174"
    readonly property color mediumBlue: "#002A3A"
    readonly property color lightBlue: "#009CDE"
    readonly property color lighterBlue: "#48C9FF"
    readonly property color blue: "#004F71"

    readonly property color yellow: "#FFD100"
    readonly property color green: "#00B140"
    readonly property color lightGreen: "#7CCC6C"
    readonly property color orange: "#E77A2B"
    readonly property color greenBlue: "#002A3A"
    readonly property color red: "#E4002B"
    readonly property color white: "#FFFFFF"
    readonly property color black: "#000000"
    readonly property color transparent: "transparent"

    readonly property string openSans: "Open Sans"
    readonly property string brandon: "Brandon Grotesque"
}
