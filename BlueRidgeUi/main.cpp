/**
 * @file   main.cpp
 *
 * \brief This file contains the main method for the Blueridge application.
 *
 * Description:
 * Main method for the application.
 *
 * @section LICENSE
 * This software is property of Abbott
 * Copyright 2019 Abbott All rights reserved.
 **/

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFontDatabase>
#include <QString>
#include <QXmlStreamReader>
#include <QtCharts/QXYSeries>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>

#include <iomanip>
#include <sstream>

#include "presentation/CardiacOutput.h"
#include "presentation/CellularStrengthController.h"
#include "presentation/DateTimeController.h"
#include "presentation/PASignalStrengthController.h"
#include "presentation/PatientInfo.h"
#include "presentation/PatientListController.h"
#include "presentation/PatientListModel.h"
#include "presentation/PressureValueController.h"
#include "presentation/RecordingController.h"
#include "presentation/RecordingListModel.h"
#include "presentation/ReviewController.h"
#include "presentation/RhcValues.h"
#include "presentation/SearchbarController.h"
#include "presentation/SensorCalibration.h"
#include "presentation/SignalStrengthController.h"
#include "presentation/SystemSettingsController.h"
#include "presentation/WifiStrengthController.h"

#include "providers/CellularStrengthProvider.h"
#include "providers/MessageParser.h"
#include "providers/PatientListProvider.h"
#include "providers/PatientProvider.h"
#include "providers/RecordingProvider.h"
#include "providers/SearchbarProvider.h"
#include "providers/SystemSettingsProvider.h"
#include "providers/TransactionProvider.h"
#include "providers/WifiStrengthProvider.h"

#include "providers/DataSocket.h"
#include "providers/SensorDataParser.h"
#include "providers/SensorDataProvider.h"
#include "providers/Timer.h"

#include <cardiomems_types.h>
#include "presentation/PatientController.h"
#include "presentation/Recording.h"
#include "providers/UsbProvider.h"

#include "presentation/ActiveChartController.h"
#include "presentation/StaticChartController.h"
#include "presentation/StaticChartControllerMgr.h"
#include "widgets/WaveChart/WaveAxis.h"
#include "widgets/WaveChart/WaveChartWidget.h"

using namespace QtCharts;
Q_DECLARE_METATYPE(QXYSeries*)

static const unsigned int defaultDataRate(125);
static const unsigned int defaultFlushRate(60);
static const QString dataRatesFile("./dataRates.xml");

int main(int argc, char* argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
    QApplication app(argc, argv);
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QDir fontDirectory(":/resources/fonts");
    fontDirectory.setNameFilters(QStringList("*.ttf"));
    if (fontDirectory.exists()) {
        foreach (QString file, fontDirectory.entryList()) {
            int font_id = 0;
            font_id = QFontDatabase::addApplicationFont(":/resources/fonts/" + file);
            if (font_id == -1) qWarning() << "Unable to load font: " + file;
        }
    } else {
        qWarning() << "Font Directory not found";
    }

    app.setFont(QFont("Roboto"));

    qmlRegisterType<PatientInfo>("PatientInfo", 1, 0, "PatientInfo");
    qmlRegisterType<Recording>("Recording", 1, 0, "Recording");
    qRegisterMetaType<QXYSeries*>();
    qmlRegisterType<WaveAxis>("WaveChartWidget", 1, 0, "WaveAxis");
    qmlRegisterType<WaveChartWidget>("WaveChartWidget", 1, 0, "WaveChart");
    qmlRegisterUncreatableType<StaticChartController>("WaveChartWidget", 1, 0, "StaticChartController",
                                                      "can not instantiate StaticChartController in qml");
    qmlRegisterType<SensorCalibration>("SensorCalibration", 1, 0, "SensorCalibration");
    qmlRegisterType<CardiacOutput>("CardiacOutput", 1, 0, "CardiacOutput");
    qmlRegisterType<RhcValues>("RhcValues", 1, 0, "RhcValues");

    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:/resources/keyboardStyle");
    qmlRegisterSingletonType(QUrl(QStringLiteral("qrc:/resources/styles/BlueRidgeStyle.qml")), "Style", 1, 0, "Style");
    QQmlContext* ctx = engine.rootContext();

    DataSocket dataSocket(PRESSURE_PUMP_SOCK);
    Timer sensorDataFlushTimer;
    SensorDataParser sensorDataParser(sensorDataFlushTimer, dataSocket);
    SensorDataProvider sensorDataProvider(sensorDataParser);

    // create the message parser and pass it in to when creating providers
    DataSocket commandSocket(COMMAND_SOCK_FILE);
    MessageParser messageParser(commandSocket);
    Timer cellularRefreshTimer;
    CellularStrengthProvider cellProvider(cellularRefreshTimer, messageParser);
    Timer wifiRefreshTimer;
    SystemSettingsProvider systemSettingsProvider(messageParser, sensorDataParser);
    WifiStrengthProvider wifiProvider(wifiRefreshTimer, messageParser);
    PatientListProvider patientListProvider(messageParser);
    UsbProvider usbProvider(messageParser);
    PatientProvider patientProvider(messageParser);
    SearchbarProvider searchbarProvider(messageParser);
    TransactionProvider transactionProvider(messageParser);
    RecordingProvider recordingProvider(messageParser);

    SystemSettingsController systemSettingsController(systemSettingsProvider);
    ctx->setContextProperty("systemSettingsController", &systemSettingsController);

    ActiveChartController activeChartController(sensorDataProvider, ":/configuration/waveChartAxis");
    ctx->setContextProperty("activeChartController", &activeChartController);

    StaticChartControllerMgr staticChartControllerMgr(":/configuration/waveChartAxis");
    ctx->setContextProperty("staticChartManager", &staticChartControllerMgr);

    PASignalStrengthController pasignalStrengthController({{"weak", 41}, {"good", 71}}, "noPulse", sensorDataProvider);
    ctx->setContextProperty("pasignalStrengthController", &pasignalStrengthController);

    SearchbarController searchbarController(searchbarProvider);
    ctx->setContextProperty("searchbarController", &searchbarController);

    WifiStrengthController wifiStrengthController(
        wifiProvider, {{"oneBar", 10}, {"twoBars", 30}, {"threeBars", 70}, {"zeroBars", 0}, {"fault", -1}});
    ctx->setContextProperty("wifiStrengthController", &wifiStrengthController);

    CellularStrengthController cellularStrengthController(
        cellProvider,
        {{"threeBars", 50}, {"twoBars", 25}, {"oneBar", 5}, {"fourBars", 75}, {"zeroBars", 0}, {"fault", -1}});
    ctx->setContextProperty("cellularStrengthController", &cellularStrengthController);

    DateTimeController dateTimeController("dd MMM yyyy", "hh:mm");
    ctx->setContextProperty("dateTimeController", &dateTimeController);

    PatientListController patientListController(patientListProvider);
    ctx->setContextProperty("patientListController", &patientListController);

    PatientController patientController(patientProvider, usbProvider, transactionProvider);
    ctx->setContextProperty("patientController", &patientController);

    RecordingController recordingController(transactionProvider, recordingProvider);
    ctx->setContextProperty("recordingController", &recordingController);

    PressureValueController pressureValueController(sensorDataProvider);
    ctx->setContextProperty("pressureValueController", &pressureValueController);

    RecordingListModel recordingListModel(recordingProvider, transactionProvider);
    ctx->setContextProperty("recordingListModel", &recordingListModel);

    ReviewController reviewController(transactionProvider);
    ctx->setContextProperty("reviewController", &reviewController);

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty()) return -1;

    QObject::connect(&engine, &QQmlApplicationEngine::quit, &app, &QApplication::quit);

    unsigned int dataRate(defaultDataRate);
    unsigned int flushRate(defaultFlushRate);
    QFile xmlFile(dataRatesFile);
    if (xmlFile.open(QIODevice::ReadOnly)) {
        QXmlStreamReader xml(&xmlFile);
        xml.readNextStartElement();
        while (!xml.atEnd()) {
            bool ok(false);
            unsigned int rate(0);
            if (xml.name() == "FlushRate" && xml.isStartElement()) {
                rate = xml.readElementText().toUInt(&ok);
                if (ok) {
                    flushRate = rate;
                } else {
                    qWarning() << "Cannot read FlushRate from " << dataRatesFile;
                }
            }
            if (xml.name() == "DataRate" && xml.isStartElement()) {
                rate = xml.readElementText().toUInt(&ok);
                if (ok) {
                    dataRate = rate;
                } else {
                    qWarning() << "Cannot read DataRate from " << dataRatesFile;
                }
            }
            xml.readNextStartElement();
        }
    }
    sensorDataParser.start(dataRate, flushRate);

    return app.exec();
}
