import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls

ColumnLayout {
    id: activeWaveView
    objectName: "ActiveWaveView"
    spacing: 0

    property bool streamingEnabled: false

    RowLayout {
        id: controls
        Layout.fillWidth: true
        Layout.fillHeight: false
        Layout.alignment: Qt.AlignTop | Qt.AlignLeft
        Layout.preferredHeight: 30
        Layout.margins: 10
        spacing: 10
        Text {
            id: units
            objectName: "Units"
            color: Style.white
            text: qsTr("mmHg")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.alignment: Qt.AlignLeft
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.preferredWidth: 50
        }
        Controls.IconButton {
            id: zoomIn
            objectName: "ZoomIn"
            iconImage: "qrc:/images/plusIcon"
            defaultColor: Style.transparent
            pressedColor: Style.white
            Layout.alignment: Qt.AlignCenter
            Layout.fillHeight: false
            Layout.fillWidth: false
            Layout.preferredHeight: 28
            Layout.preferredWidth: Layout.preferredHeight
            enabled: activeWaveChart.zoomInVerticalEnabled
            onClicked: { activeWaveChart.zoomInVertical() }
        }
        Controls.IconButton {
            id: zoomOut
            objectName: "ZoomOut"
            iconImage: "qrc:/images/minusIcon"
            defaultColor: Style.transparent
            pressedColor: Style.white
            Layout.alignment: Qt.AlignCenter
            Layout.fillHeight: false
            Layout.fillWidth: false
            Layout.preferredHeight: 28
            Layout.preferredWidth: Layout.preferredHeight
            enabled: activeWaveChart.zoomOutVerticalEnabled
            onClicked: { activeWaveChart.zoomOutVertical() }
        }
        Item {
            id: spacer
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
    RowLayout {
        id: pressureChart
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignTop | Qt.AlignLeft
        spacing: 0
        Controls.PressureSearchBar {
            id: pressureSearchBar
            objectName: "PressureSearchBar"
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            Layout.topMargin: 10
            Layout.fillHeight: false
            Layout.fillWidth: false
            Layout.preferredHeight: activeWaveChart.height * 0.9
            Layout.preferredWidth: 30
            z:1
            flashlightSize: activeWaveChart.width + activeWaveChart.anchors.margins
        }
        Item {
            id: activeWaveChartSpace
            objectName: "ActiveWaveChartSpace"
            Layout.alignment: Qt.AlignRight
            Layout.fillHeight: true
            Layout.fillWidth: true
            Controls.ActiveWaveChart {
                id: activeWaveChart
                objectName: "ActiveWaveChart"
                anchors.fill: parent
                anchors.margins: 10
                streamingEnabled: activeWaveView.streamingEnabled
            }
        }
    }
}
