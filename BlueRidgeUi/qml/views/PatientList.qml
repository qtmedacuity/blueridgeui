import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import Style 1.0
import "../controls" as Controls
import "../controls/popupContent" as Popups

Controls.Page
{
    id: patientList
    objectName: "PatientList"

    property int numItemsSelected: 0
    property string listType : "all"
    property var selectedPatient: null

    showBackButton: true

    Controls.SearchBar {
        id: patientSearch
        objectName: "PatientSearch"
        width: 413
        height: 38
        anchors.right: parent.right
        anchors.rightMargin: 16
        anchors.top: parent.top
        anchors.topMargin: 15
        onSearchTextEntered : {
            patientListController.setFilterString(text)
        }
    }

    Text {
        id: patientListTitle
        objectName: "PatientListTitle_PatientList"
        anchors.left: parent.left
        anchors.leftMargin: 26
        anchors.top: parent.top
        anchors.topMargin: 60
        text: qsTr("Select Patient")
        color: Style.white
        font.pointSize: 20
    }

    RowLayout {
        id: lastUpdatedRow
        anchors.top: patientSearch.bottom
        anchors.topMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 26
        Text {
            id: lastUpdatedText
            text: "Last updated " + patientListController.lastUpdated
            font.italic: true
            font.pixelSize: 13
            color: Style.yellow
        }
        Controls.IconButton {
            id: refreshButton
            iconImage: "qrc:/images/refreshIcon"
            width: 16
            height: 16
            dynamicColors: false
            onClicked: {
                patientListController.refresh();
            }
        }
    }

    ListView {
        id: listview
        objectName:"Listview_PatientList"
        spacing: 2

        anchors.top: patientListTitle.bottom
        anchors.topMargin: 21
        anchors.left: parent.left
        anchors.leftMargin: 26
        anchors.right: parent.right
        anchors.rightMargin: 26
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 80
        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds
        clip: true

        headerPositioning: ListView.OverlayHeader

        header: Controls.PatientListHeaderRow {
            id: headerRow
            objectName: "HeaderRow_Listview_PatientList"
            z: 2
            rowWidth: listview.width
            rowHeight: 25
            rowColor: Style.darkGray
            onHeaderClicked: {
                patientListController.sortByColumn(column, ascending);
            }
            onSelectAllClicked: {
                numItemsSelected = patientListController.selectAllPatients(checked);
            }
        }

        delegate: Controls.PatientListRow {
            id: patientRow
            objectName: "PatientRow_Listview_PatientList"
            rowHeight: 40
            rowWidth: listview.width
            separatorColor: Style.black
            rowColor: Style.darkGray
            onRowSelected: {
                listview.currentIndex = index;
                selectedPatient = listview.model.get(index)
                console.log("selected item: " + selectedPatient.patientName);
                selectionPopup.open();
            }
            onItemChecked: {
                console.log("item " + index + " checked? " + checked);
                if (checked) {
                    numItemsSelected++;
                } else {
                    numItemsSelected--;
                    if (listview.headerItem.checkBoxChecked) {
                        listview.headerItem.unselectCheckBox();
                    }
                }

            }
        }
        onCountChanged: {
            if (count > 0) {
                uploadButton.enabled = patientListController.havePendingUploads();
            }
        }

        ScrollBar.vertical: ScrollBar {
            id: listscroll
            objectName: "ListScroll_Listview_PatientList"
        }
        model: patientListController
    }

    RowLayout {
        id: buttonRow
        objectName: "ButtonRow_PatientList"
        anchors.right: parent.right
        anchors.rightMargin: 23
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 23
        spacing: 10

        Controls.Button
        {
            id: uploadButton
            objectName: "UploadButton_ButtonRow_PatientList"
            width: 240
            height: 45
            buttonText: qsTr("Upload to Merlin.net")
            buttonTextSize: 14
            buttonColor: Style.blue
            buttonPressedColor: Style.transparent
            borderColor: Style.transparent
            enabled: false
            onClicked: {
                console.log("upload to merlin clicked");
                patientListController.uploadToMerlin();
            }
        }

        Controls.Button
        {
            id: exportButton
            objectName: "exportButton_ButtonRow_PatientList"
            width: 240
            height: 45
            buttonText: qsTr("Export to USB")
            buttonTextSize: 14
            buttonColor: Style.blue
            buttonPressedColor: Style.transparent
            borderColor: Style.transparent
            enabled: numItemsSelected > 0
            onClicked: {
                 console.log("export clicked");
                patientListController.exportToUsb();
            }
        }

        Controls.Button
        {
            id: addNewButton
            objectName: "AddNewButton_ButtonRow_PatientList"
            width: 240
            height: 45
            buttonText: qsTr("Add New Patient")
            buttonTextSize: 14
            buttonColor: Style.blue
            buttonPressedColor: Style.transparent
            borderColor: Style.lightBlue
            onClicked: {
                 console.log("Add new patient clicked");
            }
        }
    }

    Component.onCompleted: {
        getPatientList();
    }

    Connections {
        target: patientListController
        onRefreshComplete: {
            getPatientList();
        }
    }

    function getPatientList() {
        if (listType === "implant") {
            patientListController.getImplantPatients();
        } else {
            patientListController.getAllPatients();
        }
    }

    Controls.Popup {
        id: selectionPopup
        objectName: "SelectionPopup"
        popupWidth: 450
        popupHeight: 200
        showButtons: false

        Text {
            id: instruction
            objectName: "Instruction"
            text: qsTr("What would you like to do?")
            color: Style.white
            font.pixelSize: 24
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 30
        }
        Row {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 50

            Controls.Button {
                width: 140
                height: 45
                buttonText: qsTr("View History")
                buttonTextSize: 14
                buttonColor: Style.blue
                buttonPressedColor: Style.transparent
                borderColor: Style.lightBlue
                onClicked: {
                    selectionPopup.close();
                    mainStackView.showPage("History.qml", {pageTitle: qsTr("FOLLOW-UP")});
                }
            }
            Controls.Button {
                width: 140
                height: 45
                buttonText: qsTr("Follow-Up")
                buttonTextSize: 14
                buttonColor: Style.blue
                buttonPressedColor: Style.transparent
                borderColor: Style.lightBlue
                onClicked: {
                    selectionPopup.close();
                    mainStackView.showPage("PatientConfirmation.qml", {patient: selectedPatient, fromListType: listType, pageTitle: qsTr("FOLLOW-UP")});
                }
            }
        }
    }
}
