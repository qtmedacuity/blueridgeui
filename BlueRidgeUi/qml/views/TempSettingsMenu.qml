import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Popup
{
    id: settingsMenu
    objectName: "SettingsMenu"
    width: 300
    height: mainWindow.height
    background: Rectangle
    {
        color: Qt.lighter(Style.darkGray)
    }

    Row
    {
        id: row
        spacing: 10
        anchors.right: parent.right
        Text
        {
            id: title
            text: qsTr("TEMP SETTINGS MENU")
            color: Style.white
            font.pointSize: 18
        }
        Controls.IconButton
        {
            id: settings
            objectName: "Settings"
            iconImage: "qrc:/images/settingsIcon"
            dynamicColors: false
            width: 25
            height: 25
            onClicked: settingsMenu.close()
        }
    }
}
