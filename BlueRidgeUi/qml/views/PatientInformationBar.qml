import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.2
import Style 1.0
import "../controls" as Controls

Rectangle {

    id: patientInfoBar
    objectName: "PatientInfoBar"
    property string patientName: ""
    property string patdob: ""
    property string patsn: ""

    property string textColor: Style.white
    color: Style.greenBlue
    clip: true

    RowLayout {
        anchors.fill: parent

        Text {
            id: name
            objectName: "Name"
            Layout.preferredWidth: 320
            Layout.preferredHeight: patientInfoBar.height
            font.pointSize: 15
            color: textColor
            text: patientName
            Layout.alignment: Qt.AlignVCenter
            Layout.leftMargin: 10
            verticalAlignment: Text.AlignVCenter
        }

        RowLayout {
            Layout.preferredWidth: 300
            Layout.preferredHeight: patientInfoBar.height
            Layout.alignment: Qt.AlignVCenter
            spacing: 5
            Text {
                id: dobLabel
                objectName: "DobLabel"
                font.pointSize: 10
                text: qsTr("D.O.B:")
                color: textColor
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                verticalAlignment: Text.AlignVCenter
            }
            Text {
                id: dob
                objectName: "Dob"
                font.pointSize: 15
                text: patdob
                color: textColor
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                verticalAlignment: Text.AlignVCenter
            }
        }

        RowLayout {
            Layout.preferredWidth: 200
            Layout.preferredHeight: patientInfoBar.height
            Layout.alignment: Qt.AlignVCenter

            spacing: 5
            Text {
                id: snLabel
                objectName: "SnLabel"
                font.pointSize: 10
                text: qsTr("SN:")
                color: textColor
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
            Text {
                id: serialNumber
                objectName: "SerialNumber"
                font.pointSize: 15
                text: patsn
                color: textColor
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            }
        }

        Item {
            id: xbuttonSpace
            objectName: "XButtonSpace"
            Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
            Layout.rightMargin: 10
            Layout.preferredWidth: 20
            Layout.preferredHeight: patientInfoBar.height
            Controls.IconButton {
                id: xbutton
                objectName: "XButton"
                width: 18
                height: 18
                iconImage: "qrc:/images/closeIcon"
                anchors.centerIn: parent

                onClicked: {
                    exitPopup.open();
                }
            }
        }
    }

   Popup {
       id: exitPopup
       objectName: "ExitPopup"
       width: 500
       height: 180
       anchors.centerIn: Overlay.overlay
       modal: true
       closePolicy: Popup.CloseOnEscape
       focus: true
       background: Item {
           Rectangle {
               id: backRect
               color: Style.mediumGray
               anchors.fill: parent
           }
       }

       ColumnLayout {
           objectName: "ExitPopupContent"
           anchors.fill: parent
           anchors.centerIn: parent

           Row {
               Layout.alignment: Qt.AlignHCenter
               spacing: 5
               Image {
                   id: warningImg
                   objectName: "WarningImage"
                   source: "qrc:/images/redwarning"
                   width: 25
                   height: 25
               }
               Text {
                   id: title
                   objectName: "Title"
                   text: qsTr("Are you sure you want to exit?")
                   font.pointSize: 14
                   color: Style.white
               }
           }

           Text {
               id: instruction
               objectName: "Instruction"
               Layout.alignment: Qt.AlignHCenter
               text: qsTr("The existing readings will not be saved.")
               font.pointSize: 12
               color: Style.white
           }

           Row {
               Layout.alignment: Qt.AlignHCenter
               spacing: 10
               Controls.Button {
                   id: noButton
                   objectName: "NoButton"
                   width: 155
                   height: 30
                   buttonText: qsTr("No, return to page")
                   buttonTextColor: Style.lightBlue
                   borderColor: Style.lightBlue
                   buttonColor: Style.darkGray
                   buttonTextSize: 12
                   onClicked: {
                       exitPopup.close();
                   }
               }

               Controls.Button {
                   id: yesButton
                   objectName: "YesButton"
                   width: 250
                   height: 30
                   buttonText: qsTr("YES, discard reading and exit")
                   buttonTextColor: Style.lightBlue
                   borderColor: Style.lightBlue
                   buttonColor: Style.darkGray
                   buttonTextSize: 12
                   onClicked: {
                       exitPopup.close();
                       resetPatientBar();
                       // abandon transaction
                       patientController.abandonTransaction();
                       mainStackView.pop(null);
                   }
               }
           }
       }
   }
}
