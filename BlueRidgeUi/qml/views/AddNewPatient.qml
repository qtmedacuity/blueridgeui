import QtQuick 2.12
import Style 1.0
import PatientInfo 1.0
import "../controls" as Controls

Controls.Page
{
    id: addNewPatient
    objectName: "AddNewPatient"

    showBackButton: true
    property string fromListType : "all"

    property alias sectionTitle: sectionTitle.text

    Text
    {
        id: sectionTitle
        objectName: "SectionTitle"
        anchors.left: parent.left
        anchors.leftMargin: 67
        anchors.top: parent.top
        anchors.topMargin: 60
        text: qsTr("Add New Patient")
        color: Style.white
        font.pointSize: 20
    }

    Column
    {
        id: requiredFieldsMessage
        visible: false
        anchors.top: sectionTitle.bottom
        anchors.topMargin: 14
        anchors.left: parent.left
        anchors.leftMargin: 67
        spacing: 5

        Text {
            id: message
            objectName: "RequiredTextMessage"
            text: qsTr("Please complete all required fields (*).")
            font.pointSize: 14
            color: Style.yellow
        }
        Rectangle
        {
            id: requiredLineIndicator
            objectName: "RequiredTextLineIndicator"
            height: 2
            width: message.width
            color: Style.yellow
        }
    }

    Grid
    {
        id: fieldsGrid
        objectName: "fieldsGrid"
        anchors.top: requiredFieldsMessage.visible ? requiredFieldsMessage.bottom : sectionTitle.bottom
        anchors.topMargin: 25
        anchors.left: parent.left
        anchors.leftMargin: 67
        anchors.right: parent.right
        anchors.rightMargin: 67
        anchors.bottom: nextButton.top
        columns: 2
        columnSpacing: 90
        rowSpacing: 20

        Controls.LabeledTextField
        {
            id: serialNumberField
            objectName: "SerialNumber"
            textFieldWidth: 500
            textFieldLabel: qsTr("Sensor Serial Number")
            isRequired: true
            textFieldPlaceholder: qsTr("Insert USB or type SN")
            isEditable: false
        }

        Controls.LabeledComboBox
        {
            id: implantDoctorComboxBox
            objectName: "ImplantDoctor"
            comboBoxWidth: 290
            comboBoxLabel: qsTr("Implanting Doctor")
            isRequired: true
            comboBoxModel: ["-select-"]
        }

        Controls.DateComboBox
        {
            id: implantDateComboBox
            objectName: "ImplantDate"
            isRequired: true
            comboBoxLabel: qsTr("Implant Date")
            yearMin: new Date().getFullYear() - 4
            yearMax: new Date().getFullYear()
        }

        Controls.LabeledComboBox
        {
            id: followClinicianComboBox
            objectName: "FollowUpClinician"
            comboBoxWidth: 290
            comboBoxLabel: qsTr("Follow Up Clinician")
            isRequired: true
            comboBoxModel: ["-select-"]
        }

        Row
        {
            id: patientName
            spacing: 10

            Controls.LabeledTextField
            {
                id: firstNameField
                objectName: "FirstName"
                textFieldWidth: 182
                textFieldLabel: qsTr("First Name")
                isRequired: true
            }

            Controls.LabeledTextField
            {
                id: middleNameField
                objectName: "MiddleInitial"
                textFieldWidth: 85
                textFieldLabel: qsTr("Middle")
            }

            Controls.LabeledTextField
            {
                id: lastNameField
                objectName: "LastName"
                textFieldWidth: 210
                textFieldLabel: qsTr("Last Name")
                isRequired: true
            }
        }

        Controls.LabeledComboBox
        {
            id: followClinicComboBox
            objectName: "FollowUpClinic"
            comboBoxWidth: 290
            comboBoxLabel: qsTr("Follow Up Clinic")
            comboBoxModel: ["-select-"]
        }

        Row
        {
            id: dobPhoneRow
            spacing: 10

            Controls.DateComboBox
            {
                id: dobComboBox
                objectName: "DOB"
                comboBoxLabel: qsTr("DOB")
                isRequired: true
            }

            Controls.LabeledTextField
            {
                id: phoneTextField
                objectName: "Phone"
                textFieldWidth: 250
                textFieldLabel: qsTr("Phone")
                textFieldPlaceholder: qsTr("555-555-5555")
            }

        }

        Controls.LabeledTextField
        {
            id: patientIdField
            objectName: "PatientId"
            textFieldLabel: qsTr("MRN/Patient ID")
            textFieldWidth: 290
        }
    }

    Controls.Button
    {
        id: nextButton
        objectName: "Next"
        width: 240
        height: 45
        anchors.right: parent.right
        anchors.rightMargin: 23
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 23
        buttonText: qsTr("Next")
        buttonTextSize: 14
        buttonColor: Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        enabled: true //false
        onClicked:
        {
            console.log("Next clicked");
            if (allFieldsValid()) {
                addPatient();
            }
        }
    }

    Component.onCompleted: {
        // get serial number from usb
        patientController.getUsbData();

        // get data for the dropdowns
        patientController.requestListOfImplantDoctors();
        patientController.requestListOfClinicians();
        patientController.requestListOfClinics();
    }

    Connections {
        target: patientController
        onUsbData: {
            console.log("usb serial number : " + serialNumber);
            if (serialNumber !== "") {
                serialNumberField.textFieldText = serialNumber;
            }
        }
        onListOfImplantDoctorsReady: {
            var doctors = patientController.getListOfImplantDoctors();
            console.log("Doctors: ", JSON.stringify(doctors));
            implantDoctorComboxBox.comboBoxModel = implantDoctorComboxBox.comboBoxModel.concat(doctors);
        }
        onListOfCliniciansReady: {
            var clinicians = patientController.getListOfClinicians();
            console.log("clinicians: ", JSON.stringify(clinicians));
            var model = ["-select-"];
            followClinicianComboBox.comboBoxModel = followClinicianComboBox.comboBoxModel.concat(clinicians);
        }
        onListOfClinicsReady: {
            var clinics = patientController.getListOfClinics();
            console.log("clinics: ", JSON.stringify(clinics));
            var model = ["-select-"];
            followClinicComboBox.comboBoxModel = followClinicComboBox.comboBoxModel.concat(clinics);
        }
        onNewPatientSavedFinished : {
            console.log("patient saved.")
            var newPatient = patientController.currentPatient;
            mainStackView.showPage("PatientConfirmation.qml",
                                   {patient: newPatient,
                                    pageTitle: pageTitle,
                                    fromListType: fromListType,
                                    showBackButton: false})
        }
    }

    function allFieldsValid() {
        var serialNumberValid = serialNumberField.isValid();
        var firstNameValid = firstNameField.isValid();
        var lastNameValid = lastNameField.isValid();
        var implantDateValid = implantDateComboBox.isValid();
        var implantDoctorValid = implantDoctorComboxBox.isValid();
        var followupClinicianValid = followClinicianComboBox.isValid();
        var dobValid = dobComboBox.isValid();
        var valid = (serialNumberValid && firstNameValid && lastNameValid
                     && implantDateValid && implantDoctorValid
                     && followupClinicianValid && dobValid);
        console.log("add patient valid: ", valid);
        if (!valid) {
            requiredFieldsMessage.visible = true;
        }

        return valid;
    }

    function addPatient() {
        // required fields should be valid
        patientInfo.serialNumber = serialNumberField.textFieldText;
        patientInfo.firstName = firstNameField.textFieldText;
        patientInfo.lastName = lastNameField.textFieldText;
        patientInfo.dob = dobComboBox.getDateValue();
        patientInfo.implantDate = implantDateComboBox.getDateValue();
        patientInfo.implantDoctor = implantDoctorComboxBox.comboBoxText;

        patientInfo.middleInitial = middleNameField.textFieldText;
        patientInfo.phone = phoneTextField.textFieldText;
        // only save data if it was entered.
        patientInfo.patientId = patientIdField.textFieldText !== "" ? parseInt(patientIdField.textFieldText) : 0;
        patientInfo.clinician = followClinicianComboBox.comboBoxIndex > 0 ? followClinicianComboBox.comboBoxText : "";
        patientInfo.clinic = followClinicComboBox.comboBoxIndex > 0 ? followClinicComboBox.comboBoxText : "";

        console.log("patient:",JSON.stringify(patientInfo));

        // save patient.
        patientController.addNewPatient(patientInfo);
    }

    PatientInfo {
        id: patientInfo
    }
}
