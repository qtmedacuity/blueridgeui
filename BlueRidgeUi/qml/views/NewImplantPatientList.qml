import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls
import "../views" as Views

Controls.Page
{
    id: newImplantPatientList
    objectName: "NewImplantPatientList"
    pageTitle: qsTr("NEW IMPLANT")

    showBackButton: true

    Controls.SearchBar {
        id: patientSearch
        objectName: "PatientSearch"
        width: 413
        height: 38
        anchors.right: parent.right
        anchors.rightMargin: 16
        anchors.top: parent.top
        anchors.topMargin: 15
        onSearchTextEntered : {
            patientListController.setFilterString(text)
        }
    }

    Text {
        id: patientListTitle
        objectName: "PatientListTitle"
        anchors.left: parent.left
        anchors.leftMargin: 26
        anchors.top: parent.top
        anchors.topMargin: 60
        text: qsTr("Select Patient")
        color: Style.white
        font.pointSize: 20
    }

    RowLayout {
        id: lastUpdatedRow
        anchors.top: patientSearch.bottom
        anchors.topMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 26
        Text {
            id: lastUpdatedText
            objectName: "LastUpdated"
            text: qsTr("Last updated %1").arg(patientListController.lastUpdated)
            font.italic: true
            font.pixelSize: 13
            color: Style.yellow
        }
        Controls.IconButton {
            id: refreshButton
            objectName: "RefreshList"
            iconImage: "qrc:/images/refreshIcon"
            width: 16
            height: 16
            dynamicColors: false
            onClicked: {
                patientListController.refresh();
            }
        }
    }

    ListView {
        id: listview
        objectName:"PatientList"
        spacing: 2

        anchors.top: patientListTitle.bottom
        anchors.topMargin: 21
        anchors.left: parent.left
        anchors.leftMargin: 26
        anchors.right: parent.right
        anchors.rightMargin: 26
        anchors.bottom: addNewButton.top
        anchors.bottomMargin: 20
        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds
        clip: true

        headerPositioning: ListView.OverlayHeader

        header: Controls.NewImplantHeaderRow {
            objectName: "PatientListHeader"
            z: 2
            rowWidth: listview.width
            rowHeight: 25
            rowColor: Style.darkGray
            onHeaderClicked: {
                patientListController.sortByColumn(column, ascending);
            }
        }

        delegate: Controls.NewImplantListRow {
            objectName: "PatientRow" + index
            rowHeight: 40
            rowWidth: listview.width
            separatorColor: Style.black
            rowColor: Style.darkGray
            onRowSelected: {
                listview.currentIndex = index;
                var patient = listview.model.get(index)
                console.log("selected item: " + patient.patientName);
                mainStackView.showPage("PatientConfirmation.qml", {patient: patient, fromListType: "newImplant", pageTitle: qsTr("NEW IMPLANT")})
            }
        }

        ScrollBar.vertical: ScrollBar {
            id: listscroll
            objectName: "ScrollBar"
        }
        model: patientListController
    }

    Controls.Button
    {
        id: addNewButton
        objectName: "AddNewPatient"
        anchors.right: parent.right
        anchors.rightMargin: 23
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 23
        width: 240
        height: 45
        buttonText: qsTr("Add New Patient")
        buttonTextSize: 14
        buttonColor: Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        onClicked: {
             console.log("Add new patient clicked");
             mainStackView.showPage("AddNewPatient.qml", {pageTitle: qsTr("NEW IMPLANT"), fromListType: "newImplant"});
        }
    }

    Component.onCompleted:
    {
        patientListController.getWaitingImplantPatients();
    }

    Connections {
        target: patientListController
        onRefreshComplete: {
            patientListController.getWaitingImplantPatients();
        }
    }
}

