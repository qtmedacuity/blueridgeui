import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls
import "../controls/popupContent" as PopupContent

Controls.Page
{
    id: review
    objectName: "Review"
    pageTitle: qsTr("REVIEW")

    showBackButton: false
    color: Style.black

    property string sensorPosition: ""

    ScrollView {
        id: pageScrollView
        anchors.fill: parent
        contentWidth: parent.width
        clip: true
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

        Controls.Button
        {
            id: backButton
            objectName: "BackButton"
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 10
            width: 50
            height: 22
            buttonText: qsTr("< back")
            buttonTextSize: 10
            buttonColor: Style.transparent
            buttonPressedColor: Style.lightBlue
            borderColor: Style.lightBlue
            onClicked: {
                mainStackView.pop();
            }
        }

        Controls.Button
        {
            id: helpButton
            objectName: "HelpButton"
            anchors.right: parent.right
            anchors.rightMargin: 13
            anchors.top: parent.top
            anchors.topMargin: 10
            width: 50
            height: 22
            buttonText: qsTr("Help")
            buttonTextSize: 10
            buttonColor: Style.transparent
            buttonPressedColor: Style.lightBlue
            borderColor: Style.lightBlue
            onClicked: {
                console.log("help button pressed.")
            }
        }

        Image {
            id: sensorSelectionImage
            objectName: "SensorSelectionImage"
            anchors.top: sensorCalibration.top
            anchors.left: sensorCalibration.right
            anchors.right: parent.right
            height: 125
            width: 110
            source: sensorPosition === "left" ? "qrc:/images/bodyleft" : "qrc:/images/bodyright"
            fillMode: Image.PreserveAspectFit
            mipmap: true
        }

        Controls.RhcValuesCard {
            id: rhcvalues
            objectName: "RHCValuesCard"
            anchors.top: sensorSelectionImage.bottom
            anchors.left: sensorCalibration.right
            anchors.leftMargin: 12
            anchors.right: parent.right
            anchors.rightMargin: 12
            height: 260
            visible: false
        }

        Controls.SensorReviewCard {
            id: sensorCalibration
            objectName: "SensorReviewCard"
            anchors.top: backButton.bottom
            anchors.topMargin: 12
            anchors.left: parent.left
            anchors.leftMargin: 5
            visible: false
            onEditClicked: {
                console.log("sensor review card edit clicked");
            }
            onDeleteClicked: {
                console.log("sensor review card delete clicked")
            }
        }

        Controls.CardiacOutputReviewCard {
            id: cardiacOutput
            objectName:  "COReviewCard"
            anchors.top: sensorCalibration.bottom
            anchors.topMargin: 7
            anchors.left: parent.left
            anchors.leftMargin: 5
            visible: false
            onEditClicked: {
                console.log("cardiac review card edit clicked");
            }
            onDeleteClicked: {
                console.log("caridac review card delete clicked")
            }
        }

        Text {
            id: recordedPressuresText
            objectName: "RecordedPressuresLabel"
            anchors.top: cardiacOutput.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 5
            text: qsTr("RECORDED PRESSURES")
            color: Style.white
            visible: false
        }

        ListView {
            id: recordingListView
            objectName: "RecordingListView"
            anchors.top: recordedPressuresText.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 5
            spacing: 5
            visible: false
            model: recordingListModel
            delegate: Controls.RecordingReviewCard {
                objectName: "ReviewCard_" + index
                onEditClicked: {
                    console.log("(record review card edit clicked) index: ", index);
                    var selectedRecording = recordingListView.model.get(index);
                    if (selectedRecording) {
                        mainWindow.openRecConfirmationOverlay(selectedRecording, false);
                    } else {
                        console.log("selected recording is null");
                    }
                }
                onDeleteClicked: {
                   console.log("(record review card delete button clicked) index: ", index);
                   var selectedRecording = recordingListView.model.get(index);
                   deleteRecordingPopup.recording = selectedRecording
                   deleteRecordingPopup.open();
                }
            }
        }

        Item {
            id: buttonContainer
            anchors.right: parent.right
            anchors.rightMargin: 12
            anchors.bottom: parent.bottom

            states: [
                State {
                    name: "anchorToPage"
                    AnchorChanges {
                        target: buttonContainer
                        anchors.bottom: parent.bottom
                    }
                    PropertyChanges {
                        target: buttonContainer
                        anchors.bottomMargin: 17
                    }
                },
                State {
                    name: "anchorToListView"
                    AnchorChanges {
                        target: buttonContainer
                        anchors.top: recordingListView.bottom
                    }
                    PropertyChanges {
                        target: buttonContainer
                        anchors.topMargin: 10
                    }
                }
            ]

            Controls.Button
            {
                id: uploadButton
                objectName: "UploadButton"
                width: 232
                height: 38
                anchors.right: saveButton.left
                anchors.rightMargin: 9
                buttonText: qsTr("Upload to merlin.net")
                buttonTextSize: 14
                buttonColor: Style.blue
                buttonPressedColor: Style.transparent
                borderColor: Style.lightBlue
                enabled: (wifiStrengthController.status !== "fault" && cellularStrengthController.status !== "fault")
                onClicked: {
                    console.log("(review) upload clicked");
                    reviewController.commitAndUpload(patientController.getTransactionId(),
                                                    patientController.getPatientId());

                }
            }

            Controls.Button
            {
                id: saveButton
                objectName: "SaveButton"
                width: 220
                height: 38
                anchors.right: parent.right
                buttonText: qsTr("Save and End Case")
                buttonTextSize: 14
                buttonColor: Style.blue
                buttonPressedColor: Style.transparent
                borderColor: Style.lightBlue
                enabled: (wifiStrengthController.status === "fault" && cellularStrengthController.status === "fault")
                onClicked: {
                    console.log("(review) save clicked");
                    reviewController.commitTransaction(patientController.getTransactionId(),
                                                       patientController.getPatientId());
                }
            }
        }
    }

    Component.onCompleted: {
        reviewController.getReviewData(patientController.getTransactionId(),
                                       patientController.getPatientId());
    }

    Connections {
        target: recordingListModel
        onModelReset: {
            console.log("(review) on list model reset count", recordingListModel.rowCount())
            recordingListView.height = (130 + 5) * recordingListModel.rowCount();
            recordedPressuresText.visible = true;
            recordingListView.visible = true;
            adjustScrollHeight();
        }
    }

    Connections {
        target: recordingController
        onRecordingDeleteComplete: {
            console.log("recording delete complete");
            deleteRecordingPopup.close();
        }
    }

    Connections {
        target: reviewController
        onReviewDataReady: {
            console.log("(review) on review data ready");
            if (mainWindow.eventType === "NewImplant") {
                rhcvalues.rhcData = reviewController.getRhcValues();
                rhcvalues.visible = true;
            }
            if (reviewController.getSensorCalibration()) {
                sensorCalibration.sensorData = reviewController.getSensorCalibration();
                sensorCalibration.visible = true;
            } else {
                sensorCalibration.height = 0;
            }

            if (reviewController.getCardiacOutput()) {
                cardiacOutput.outputData = reviewController.getCardiacOutput();
                cardiacOutput.visible = true;
            } else {
                cardiacOutput.height = 0;
            }

            adjustScrollHeight();
        }
        onTransactionCommitted: {
            console.log("(review) transaction committed");
            showTransactionSavedPopup(qsTr("saved"));
        }
        onUploadCompleted: {
            console.log("(review) upload to Merlin completed");
            showTransactionSavedPopup(qsTr("sent"));
        }
    }

    function adjustScrollHeight() {
        var pageHeight = calculatePageHeight();
        console.log("page height " + pageHeight + " review height: " +review.height);
        if (pageHeight < review.height) {
            pageScrollView.contentHeight = review.height - 30;
            buttonContainer.state = "anchorToPage"
        } else {
            pageScrollView.contentHeight = pageHeight;
            buttonContainer.state = "anchorToListView"
        }
    }

    function calculatePageHeight () {
        var cardHeight = 130;
        var height = 42; // top buttons and margins
        if (sensorCalibration.visible === true) {
            height += sensorCalibration.height;
        }
        if (cardiacOutput.visible === true) {
            height += cardiacOutput.height + 7;
        }
        height += recordedPressuresText.height + 20;
        height += recordingListView.height;
        height += saveButton.height + 30;
        return height;
    }

    PopupContent.DeleteRecording {
        id: deleteRecordingPopup
        objectName: "DeleteRecordingPopup"
        onDeleteClicked: {
            recordingController.deleteRecording(recordingId);
        }
    }

    function showTransactionSavedPopup(sendOrSave) {
        transactionSavedPopup.sendOrSave = sendOrSave;
        transactionSavedPopup.dateTime = reviewController.getEventDateTime();
        var patient = reviewController.getPatientInfo();
        transactionSavedPopup.patientName = patient.patientName;
        transactionSavedPopup.dob = patient.dob;
        transactionSavedPopup.patientId = patient.patientId;
        transactionSavedPopup.serialNumber = patient.serialNumber;
        transactionSavedPopup.implantDate = patient.implantDate;
        transactionSavedPopup.open();
    }

    function finish() {
        resetPatientBar();
        mainStackView.pop(null);
    }

    Controls.EventSavedPopup {
        id: transactionSavedPopup
        objectName: "EventSavedPopup"
        onClosed: {
            finish();
        }

    }

}
