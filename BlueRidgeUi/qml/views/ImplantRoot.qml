import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Style 1.0
import Recording 1.0
import "../controls" as Controls
import "../views" as Views

Controls.Page
{
    id: implantRoot
    objectName: "Implant_" + state
    showBackButton: false

    property string sequenceType: "newImplant"

    property variant stateSequence

    property variant newImplantSequence: ["selectSensor",
                                          "acquireSignal",
                                          "calibrateSensor",
                                          "recordPressures"]

    property variant followUpSequence: ["acquireSignal",
                                        "recordPressures"]

    property variant checkSensorSequence: ["acquireSignal",
                                           "calibrateSensor",
                                           "recordPressures"]

    property string sensorPosition: ""
    property bool showLargerBackground: false

    property bool updatingPressures: false

    property bool takePaMeanReading: false

    Controls.RoundedBackground {
        id: topBackground
        objectName: "TopBackground"
        height: 415
        width: parent.width
        anchors.left: parent.left
        anchors.top: parent.top
        visible: false
    }

    Views.ActiveWaveView {
        id: waveChartLoader
        objectName: "WaveChartLoader"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 300
        streamingEnabled: true
        Connections {
            target: mainWindow
            onOverlayOpened: {
                waveChartLoader.streamingEnabled = false
            }
            onOverlayClosed: {
                waveChartLoader.streamingEnabled = true
            }
        }
    }

    Controls.Button
    {
        id: waveExamplesButton
        objectName: "WaveExamplesButton"
        anchors.right: parent.right
        anchors.rightMargin: 25
        anchors.top: parent.top
        anchors.topMargin: 20
        width: 168
        height: 22
        buttonText: qsTr("Waveform Examples")
        buttonTextSize: 12
        buttonColor: Style.transparent
        buttonPressedColor: Style.lightBlue
        borderColor: Style.lightBlue
        onClicked: {
            console.log("waveform examples clicked")
            visible = false;
            exampleWaveformsState.state = "displayExampleWaveforms";
        }
    }

    Controls.SignalStrengthIndicator {
        id: signalStrength
        objectName: "SignalStrengthIndicator"
        anchors.top: waveChartLoader.bottom
        anchors.left: parent.left
        anchors.leftMargin: 10
    }

    RowLayout {
        id: statsInfo
        objectName: "StatsInfo"
        anchors.left: signalStrength.right
        anchors.leftMargin: 10
        anchors.right: sensorSelectionImage.left
        anchors.rightMargin: 10
        anchors.top: waveChartLoader.bottom
        anchors.topMargin: -18
        height: 100
        visible: false

        Controls.SystolicDiastolicText {
            id: systolicDiastolicText
            objectName: "SystolicDiastolicText"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            Layout.preferredWidth: parent.width * 0.4
            systolicVal: pressureValueController.systolic
            diastolicVal: pressureValueController.diastolic
        }

        Controls.MeanText {
            id: meanText
            objectName: "MeanText"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            Layout.preferredWidth: parent.width * 0.4
            meanValue: pressureValueController.mean
        }

        Controls.HeartRateText {
            id: heartRateText
            objectName: "HeartRateText"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            Layout.preferredWidth: parent.width * 0.2
            heartRateVal: pressureValueController.heartRate
        }
    }

    Image {
        id: sensorSelectionImage
        objectName: "SensorSelectionImage"
        height: 115
        width: 110
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.top: waveChartLoader.bottom
        anchors.topMargin: -3
        source: sensorPosition === "left" ? "qrc:/images/bodyleft" : "qrc:/images/bodyright"
        visible: false
        fillMode: Image.PreserveAspectFit
        mipmap: true
    }

    Loader {
        id: loader
        objectName: "Loader"
        anchors.left: parent.left
        anchors.top: signalStrength.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }

    onShowLargerBackgroundChanged: {
        implantRoot.color = showLargerBackground ? Style.black : Style.darkGray;
        sensorSelectionImage.visible = showLargerBackground
        topBackground.visible = showLargerBackground
    }

    Component.onCompleted: {
        mainWindow.waveFormExamplesClosed.connect(examplesClosed);

        if (sequenceType !== "newImplant") {
            var patientBarInfo = JSON.parse(patientController.getPatientBarInfo());
            setPatientBar(patientBarInfo);
            patientController.createTransaction(mainWindow.eventType);
        }

        if (sequenceType === "followUp") {
            stateSequence = followUpSequence;
        } else if (sequenceType === "checkSensor") {
            stateSequence = checkSensorSequence;
        } else {
            stateSequence = newImplantSequence;
        }
        state = stateSequence[0];
    }

    Component.onDestruction: {
        if (updatingPressures && pressureValueController) {
            pressureValueController.getPressureValues("Stop");
        }
    }

    onStateChanged: {
        if (!updatingPressures && state === "recordPressures") {
            pressureValueController.getPressureValues("Start");
            updatingPressures = true;
        }
    }

    states: [
        State {
            name: "selectSensor"
            PropertyChanges {
                target: loader
                source: "../controls/SelectSensorPosition.qml"
            }
        },
        State {
            name: "acquireSignal"
            PropertyChanges {
                target: loader
                source: "../controls/AcquireSignal.qml"
            }
            PropertyChanges {
                target: searchbarController
                userEnabled: true
            }
        },
        State {
            name: "calibrateSensor"
            PropertyChanges {
                target: loader
                source: "../controls/CalibrateSensor.qml"
            }
            PropertyChanges {
                target: implantRoot
                showLargerBackground: true
            }
        },
        State {
            name: "recordPressures"
            PropertyChanges {
                target: loader
                source: "../controls/RecordPressures.qml"
            }
            PropertyChanges {
                target: implantRoot
                showLargerBackground: true
            }
            PropertyChanges {
                target: statsInfo
                visible: true
            }
        }

    ]

    Connections {
        target: recordingController
        onTakeReadingFinished: {
            console.log("take reading has finished");
            if (takePaMeanReading) {
                takePaMeanReading = false;
                mainWindow.openPaMeanOverlay(recordingController.getNewRecording());
            }
        }
    }

    Connections {
        target: mainWindow
        onPaMeanSet: {
            nextStep();
        }
    }

    function examplesClosed() {
        console.log("received wave form examples closed signal");
        waveExamplesButton.visible = true;
    }

    // these methods will determine the next/previous step in the stateSequence
    // based on the current state
    function nextStep() {
        var index = stateSequence.findIndex(findCurrentStep);
        if (index === -1) {
            console.log("ERROR: could not find step ", state);
            return;
        }
        if (index < stateSequence.length - 1) {
            var next = stateSequence[index+1];
            console.log("next step is: ", next);
            state = next;
        } else {
            console.log("ERROR: already at the last step");
        }
    }

    function previousStep() {
        var index = stateSequence.findIndex(findCurrentStep);
        if (index === -1) {
            console.log("ERROR: could not find step ", state);
            return;
        }
        if (index > 0) {
            var previous = stateSequence[index-1];
            console.log("previous step is: ", previous);
            state = previous;
        } else {
            console.log("ERROR: already at the first step");
        }
    }

    // comparator function for findIndex
    function findCurrentStep(step) {
        return state === step;
    }


    function sensorPositionSelected(position) {
        console.log("selected: ", position);
        sensorPosition = position;
        patientController.setSensorPosition(position);
    }

    function freezeToCalibrate() {
        console.log("take reading for freeze to calibrate");
        takePaMeanReading = true;
        recordingController.takeReading();
    }
}
