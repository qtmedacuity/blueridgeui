import QtQuick 2.12
import Style 1.0
import "../controls" as Controls

Controls.Page {
    id: history
    objectName: "History"

    showBackButton: true

    Text {
        id: tempMessage
        objectName: "TempMessage"
        anchors.centerIn: parent
        color: Style.white
        font.pointSize: 28
        text: qsTr("TEMPORARY HISTORY PAGE")
    }
}
