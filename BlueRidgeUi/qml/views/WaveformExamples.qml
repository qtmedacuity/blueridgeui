import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Rectangle {
    id: waveformExamples
    objectName: "WaveformExamples"

    color: Style.darkerGray
    clip: true

    signal waveformHidden()

    Text
    {

        id: waveFormTitle
        objectName: "WaveformTitle"
        text: qsTr("WAVEFORM EXAMPLES")
        color: Style.white
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        font.bold: true
        font.pointSize: 12

    }

    Controls.Button
    {
        id: hideButton
        objectName: "HideButton"
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: 6
        anchors.rightMargin: 9
        width: 50
        height: 20
        buttonText: qsTr("hide")
        buttonTextSize: 10
        buttonColor: Style.transparent
        buttonPressedColor: Style.lightBlue
        borderColor: Style.lightBlue
        onClicked: {
            waveformHidden();
        }
    }

    Image {
        id: examplesImage
        objectName: "ExamplesImage"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors { topMargin: 10; leftMargin: 10; rightMargin: 10; bottomMargin: 0.1}
        source: "qrc:/images/waveformExamples"

    }

    Column
    {
        id: examplesLabelCol1
        anchors.top: examplesImage.top
        anchors.left: examplesImage.left
        anchors.topMargin: 95
        anchors.leftMargin: 5
        spacing: 85

        Text {
            id: examplesLabel1
            objectName: "ExamplesLabel1"
            text: qsTr("Good")
            font.bold: true
            font.pixelSize: 12
            color: Style.lightGreen
        }

        Text {
            id: examplesLabel2
            objectName: "ExamplesLabel2"
            text: qsTr("Dampened")
            font.pixelSize: 12
            color: Style.lightGrayAlt
        }

        Text {
            id: examplesLabel3
            objectName: "ExamplesLabel3"
            text: qsTr("Excessive Respiratory Variation")
            font.pixelSize: 12
            color: Style.lightGrayAlt
        }
    }

    Text
    {

        id: waveFormSubtitle
        objectName: "WaveformSubtitle"
        text: qsTr("ELECTRICAL INTERFERENCE EXAMPLES")
        color: Style.white
        anchors.left: parent.left
        anchors.top: examplesLabelCol1.bottom
        anchors.topMargin: 15
        anchors.leftMargin: 10
        font.bold: true
        font.pointSize: 10

    }

    Column
    {
        id: examplesLabelCol2
        anchors.top: waveFormSubtitle.bottom
        anchors.left: examplesImage.left
        anchors.topMargin: 85
        anchors.leftMargin: 5
        spacing: 86

        Text {
            id: examplesLabel4
            objectName: "ExamplesLabel4"
            text: qsTr("Loss of Waveform")
            font.pixelSize: 12
            color: Style.lightGrayAlt
        }

        Text {
            id: examplesLabel5
            objectName: "ExamplesLabel5"
            text: qsTr("Insufficient Pressure Waveform")
            font.pixelSize: 12
            color: Style.lightGrayAlt
        }

        Text {
            id: examplesLabel6
            objectName: "ExamplesLabel6"
            text: qsTr("Sob-optimal Signal Strength and Inconsistent\nWaveform")
            font.pixelSize: 12
            color: Style.lightGrayAlt
        }

    }

}
