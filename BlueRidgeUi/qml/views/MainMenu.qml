import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls

Controls.Page
{
    id: mainMenu
    objectName: "MainMenu"
    pageTitle: qsTr("MAIN MENU")

    Column
    {
        id: buttons
        spacing: 15
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 180
        property string readingsUnsentCount: "1"

        readonly property int buttonWidth: 360
        readonly property int buttonHeight: 67

        Controls.Button
        {
            id: newImplantButton
            objectName: "NewImplant"
            width: buttons.buttonWidth
            height: buttons.buttonHeight
            radius: 6
            buttonText: qsTr("New Implant")
            buttonColor: Style.blue
            buttonPressedColor: Style.transparent
            borderColor: Style.lightBlue
            onClicked:
            {
                console.log("Show New Implant View");
                mainWindow.eventType = "NewImplant";
                mainStackView.showPage("NewImplantPatientList.qml")

            }
        }

        Controls.Button
        {
            id: followUpButton
            objectName: "FollowUp"
            width: buttons.buttonWidth
            height: buttons.buttonHeight
            radius: 6
            buttonText: qsTr("Follow-Up")
            buttonColor: Style.blue
            buttonPressedColor: Style.transparent
            borderColor: Style.lightBlue
            onClicked: {
                console.log("Show Follow up View");
                mainWindow.eventType = "FollowUp";
                mainStackView.showPage("PatientList.qml", {pageTitle: qsTr("FOLLOW-UP"), listType: "implant"})
            }
        }

        Controls.Button
        {
            id: viewPatientListButton
            objectName: "ViewPatientList"
            width: buttons.buttonWidth
            height: buttons.buttonHeight
            buttonText: qsTr("View Patient List")
            buttonColor: Style.transparent
            buttonPressedColor: Style.blue
            borderColor: Style.blue
            borderWidth: 3
            onClicked:
            {
                console.log("Show Patient List View")
                // TODO: commenting out until ready to deliver all patients list
                //mainStackView.showPage("PatientList.qml", {pageTitle: qsTr("PATIENT LIST")})
            }
        }

        Controls.Button
        {
            id: unsentReadingbutton
            objectName: "UnsentReading"
            width: buttons.buttonWidth
            height: buttons.buttonHeight
            buttonText: qsTr(("%1 Unsent %2").arg(buttons.readingsUnsentCount).arg((buttons.readingsUnsentCount === "1") ? qsTr("Reading") : qsTr("Readings")))
            buttonColor: Style.transparent
            buttonPressedColor: Style.blue
            borderColor: Style.blue
            borderWidth: 3
            buttonIconSource: "qrc:/images/warningIcon"
            buttonIconVisible: true
            buttonIconWidth: 35
            buttonIconHeight: 35
            onClicked: {
                console.log("Show Readingss View");
            }
        }
    }

}
