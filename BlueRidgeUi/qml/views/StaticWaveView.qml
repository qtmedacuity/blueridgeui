import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls

ColumnLayout {
    id: staticWaveView
    objectName: "StaticWaveView"
    property bool showPressureSearchBar: true
    property alias dataFile: staticWaveChart.dataFile
    property alias showYLabels: staticWaveChart.showYAxisLabels

    function setVerticalOffset(offset) {
        staticWaveChart.controller.setVerticalOffset(offset);
    }

    spacing: 0
    RowLayout {
        id: controls
        Layout.fillWidth: true
        Layout.fillHeight: false
        Layout.alignment: Qt.AlignTop | Qt.AlignLeft
        Layout.preferredHeight: 40
        Layout.margins: 10
        spacing: 10
        Text {
            id: units
            objectName: "Units"
            color: Style.white
            text: qsTr("mmHg")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.alignment: Qt.AlignLeft
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.preferredWidth: 50
        }
        Controls.IconButton {
            id: zoomIn
            objectName: "ZoomIn"
            iconImage: "qrc:/images/plusIcon"
            defaultColor: Style.transparent
            pressedColor: Style.white
            Layout.alignment: Qt.AlignCenter
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.preferredHeight: 28
            Layout.preferredWidth: Layout.preferredHeight
            enabled: staticWaveChart.zoomInVerticalEnabled
            onClicked: { staticWaveChart.zoomInVertical() }
        }
        Controls.IconButton {
            id: zoomOut
            objectName: "ZoomOut"
            iconImage: "qrc:/images/minusIcon"
            defaultColor: Style.transparent
            pressedColor: Style.white
            Layout.alignment: Qt.AlignCenter
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.preferredHeight: 28
            Layout.preferredWidth: Layout.preferredHeight
            enabled: staticWaveChart.zoomOutVerticalEnabled
            onClicked: { staticWaveChart.zoomOutVertical() }
        }
        Item {
            id: spacer
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
    RowLayout {
        id: pressureChart
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignTop | Qt.AlignLeft
        spacing: 0
        Controls.PressureSearchBar {
            id: pressureSearchBar
            objectName: "PressureSearchBar"
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            Layout.topMargin: 10
            Layout.fillHeight: false
            Layout.fillWidth: false
            Layout.preferredHeight: staticWaveChart.height * 0.9
            Layout.preferredWidth: 30
            visible: showPressureSearchBar
        }
        Item {
            id: staticWaveChartSpace
            objectName: "StaticWaveChartSpace"
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            Layout.fillHeight: true
            Layout.fillWidth: true
            Controls.StaticWaveChart {
                id: staticWaveChart
                objectName: "StaticWaveChart"
                anchors.fill: parent
                anchors.margins: 10
            }
        }
    }
}
