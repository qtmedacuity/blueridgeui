import QtQuick 2.12
import Style 1.0
import "../controls" as Controls

Controls.Page
{
    id: editPatient
    objectName: "EditPatient"

    showBackButton: true

    Text {
        id: tempText
        objectName: "TempText"
        text: "Temporary Edit Patient Screen"
        anchors.centerIn: parent
        color: Style.white
        font.pointSize: 20
    }
}
