import QtQuick 2.12
import QtQuick.Dialogs 1.2
import Style 1.0
import QtMultimedia 5.12
import "../controls" as Controls
import "../controls/popupContent" as PopupContent

Controls.Page
{
    id: sensorCheck
    objectName: "SensorCheck"
    color: Style.black

    showBackButton: false

    Controls.RoundedBackground {
        id: topBackground
        objectName: "TopBackground"
        height: 425
        width: parent.width
        anchors.left: parent.left
        anchors.top: parent.top

        AnimatedImage
        {
            id: sensorCheckAnimation
            objectName: "SensorCheckVideo"
            anchors.centerIn: parent
            source: "qrc:/animations/sensorCheck"
        }
    }

    Text {
        id: signalStrengthText
        objectName: "SignalStrengthText"
        anchors.bottom: signalStrength.top
        anchors.bottomMargin: 13
        anchors.left: parent.left
        anchors.leftMargin: 15
        text: qsTr("SIGNAL STRENGTH")
        color: Style.white
        font.family: Style.brandon
        font.pixelSize: 16
    }

    Controls.SignalStrengthIndicator {
        id: signalStrength
        objectName: "SignalStrengthIndicator"
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.bottom: topBackground.bottom
        anchors.bottomMargin: 10
        isStatic: waitForImplantPopup.opened
    }

    Column {
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.bottom: cancelButton.top
        anchors.bottomMargin: 35
        spacing: 20
        Text {
            id: instructionTitle
            objectName: "InstructionTitle"
            text: qsTr("Sensor Check")
            color: Style.white
            font.pointSize: 20
        }

        Text {
            id: instruction1
            objectName: "Instruction1"
            text: qsTr("Place sensor package on top of wand so that sensor is in the middle.")
            color: Style.white
            font.pointSize: 14
        }

        Text {
            id: instruction2
            objectName: "Instruction2"
            text: qsTr("Continue when signal strength is green and over 70.")
            color: Style.white
            font.pointSize: 14
        }
    }


    Controls.Button
    {
        id: cancelButton
        objectName: "CancelButton"
        width: 101
        height: 45
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        buttonText: qsTr("Cancel")
        buttonTextSize: 14
        buttonColor: Style.transparent
        buttonPressedColor: Style.blue
        borderColor: Style.blue
        borderWidth: 3

        onClicked: {
            console.log("cancel clicked");
            sensorCheckAnimation.paused = true
            resetPatientBar();
            mainStackView.pop();
        }
    }

    Controls.Button
    {
        id: continueButton
        objectName: "ContinueButton"
        width: 240
        height: 45
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        buttonText: qsTr("Continue")
        buttonTextSize: 14
        buttonColor: Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        onClicked: {
            console.log("continue clicked");
            waitForImplantPopup.open();
        }
    }

    Component.onCompleted: {
        var patientBarInfo = JSON.parse(patientController.getPatientBarInfo());
        setPatientBar(patientBarInfo);
        pasignalStrengthController.setPulsatilityOverride(true)
        systemSettingsController.rfState = true
        systemSettingsController.enableSensorDataStream()
    }
    Component.onDestruction: {
        if (systemSettingsController)
            systemSettingsController.disableSensorDataStream()
            systemSettingsController.rfState = false
    }
    onVisibleChanged: { pasignalStrengthController.setPulsatilityOverride(visible) }

    PopupContent.WaitForImplant {
        id: waitForImplantPopup
        objectName: "WaitForImplantPopup"
        onPopupButtonClicked: {
            if (buttonName === "right") {
                sensorCheckAnimation.paused = true
                patientController.createTransaction(mainWindow.eventType);
                mainStackView.showPage("ImplantRoot.qml", {pageTitle: qsTr("NEW IMPLANT")});
            }
        }
    }
}
