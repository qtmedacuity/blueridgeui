import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../controls" as Controls

import Style 1.0

Rectangle {
    id: titleBar
    objectName: "TitleBar"
    implicitHeight: 40
    color: Style.black

    property alias titleText: titleText.text

    signal settingsClicked()

    RowLayout {
        id: rowLayout
        anchors.fill: parent
        spacing: 10

        Text {
            id: titleText
            objectName: "TitleText"
            color: Style.white
            elide: Text.ElideRight
            font.bold: true
            font.pointSize: 15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            leftPadding: 15
            Layout.alignment: Layout.Right
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        Controls.CellularStrengthIndicator {
            id: cellular
            objectName: "CellularIndicator"
            Layout.alignment: Layout.Right
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.minimumWidth: cellular.implicitWidth
        }

        Controls.WifiStrengthBars {
            id: wifi
            objectName: "WifiIndicator"
            Layout.alignment: Layout.Right
            Layout.fillHeight: false
            Layout.fillWidth: false
            Layout.preferredWidth: height
        }

        Controls.DateTimeIndicator {
            id: dateTime
            objectName: "DateTimeIndicator"
            Layout.alignment: Layout.Right
            Layout.fillHeight: true
            Layout.fillWidth: false
        }
        Controls.IconButton {
            id: settings
            objectName: "Settings"
            iconImage: "qrc:/images/settingsIcon"
            dynamicColors: false
            Layout.alignment: Layout.Right
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.rightMargin: 15
            width: parent.height * 0.5
            height: parent.height * 0.5
            onClicked: titleBar.settingsClicked()
        }
    }
}
