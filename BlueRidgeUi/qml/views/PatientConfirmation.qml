import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls

Controls.Page {
    id: patientConfirmation
    objectName: "PatientConfirmation"

    property var patient: null
    property string fromListType : "all"
    property string sensorSerialNumber: ""

    showBackButton: true

    Text {
        id: patientConfirmationTitle
        objectName: "PatientConfirmationTitle"
        anchors.left: parent.left
        anchors.leftMargin: 67
        anchors.top: parent.top
        anchors.topMargin: 60
        text: qsTr("Confirm Patient Information")
        color: Style.white
        font.pointSize: 20
    }


    Controls.Button
    {
        id: editButton
        objectName: "Edit"
        width: 101
        height: 45
        anchors.left: parent.left
        anchors.leftMargin: 22
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 23
        buttonText: qsTr("Edit")
        buttonTextSize: 14
        buttonColor: Style.transparent
        buttonPressedColor: Style.blue
        borderColor: Style.blue
        borderWidth: 3

        onClicked: {
            console.log("edit clicked");
            mainStackView.showPage("EditPatient.qml", {pageTitle: qsTr("NEW IMPLANT")});
        }
    }

    Grid {
        id: patientGrid
        objectName: "PatientGrid"
        anchors.top: patientConfirmationTitle.bottom
        anchors.topMargin: 25
        anchors.left: parent.left
        anchors.leftMargin: 67
        anchors.right: parent.right
        anchors.rightMargin: 67
        anchors.bottom: editButton.top
        anchors.bottomMargin: 100
        columns: 2
        rowSpacing: 35

        Controls.PatientConfirmationText {
            objectName: "patientSerialNumber"
            width: patientGrid.width / 2
            titleText: qsTr("SENSOR SERIAL #")
            infoText: sensorSerialNumber
        }

        Controls.PatientConfirmationText {
            objectName: "patientImplantDoctor"
            width: patientGrid.width / 2
            titleText: qsTr("IMPLANTING DOCTOR")
            infoText: patient.implantDoctor
        }

        Controls.PatientConfirmationText {
            objectName: "patientImplantDate"
            width: patientGrid.width / 2
            titleText: qsTr("IMPLANT DATE")
            infoText: patient.implantDate
        }

        Controls.PatientConfirmationText {
            objectName: "patientClinician"
            width: patientGrid.width / 2
            titleText: qsTr("FOLLOW UP CLINICIAN")
            infoText: patient.clinician
        }

        Controls.PatientConfirmationText {
            objectName: "patientName"
            width: patientGrid.width / 2
            titleText: qsTr("NAME")
            infoText: patient.patientName
        }

        Controls.PatientConfirmationText {
            objectName: "patientClinic"
            width: patientGrid.width / 2
            titleText: qsTr("FOLLOW UP CLINIC")
            infoText: patient.clinic
        }

        Controls.PatientConfirmationText {
            objectName: "patientDob"
            width: patientGrid.width / 2
            titleText: qsTr("D.O.B")
            infoText: patient.dob
        }

        Controls.PatientConfirmationText {
            objectName: "patientId"
            width: patientGrid.width / 2
            titleText: qsTr("MRN/PATIENT ID")
            infoText: patient.patientId
        }

        Controls.PatientConfirmationText {
            objectName: "patientPhone"
            width: patientGrid.width / 2
            titleText: qsTr("PHONE")
            infoText: patient.phone
        }

        Column {
            width: patientGrid.width / 2
            visible: (fromListType === "implant")
            Text {
                id: sensorLocationTitle
                objectName: "SensorLocationTitle"
                text: qsTr("SENSOR IMPLANT LOCATION")
                color: Style.lighterGray
                font.family: Style.brandon
                font.pixelSize: 12
            }
            Image {
                id: sensorSelectionImage
                objectName: "SensorSelectionImage"
                height: 115
                width: 110
                source: (patient.implantLocation === "left")
                        ? "qrc:/images/bodyleft"
                        : (patient.implantLocation === "right")
                          ? "qrc:/images/bodyright" : ""
                fillMode: Image.PreserveAspectFit
                mipmap: true
            }
        }
    }

    Controls.Button
    {
        id: confirmButton
        objectName: "Confirm"
        width: 240
        height: 45
        anchors.right: parent.right
        anchors.rightMargin: 23
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 23
        buttonText: qsTr("Confirm")
        buttonTextSize: 14
        buttonColor: Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        onClicked: {
            console.log("confirm clicked");
            patientController.currentPatient = patient;
            if (fromListType === "newImplant") {
                mainStackView.showPage("SensorCheck.qml", {pageTitle: qsTr("NEW IMPLANT")});
            } else if (fromListType === "implant") {
                console.log("confirm from follow up.");
                confirmSelectionPopup.open();
            } else {
                console.log("confirm from all patients");
            }
        }
    }

    Component.onCompleted: {
        if (patient.serialNumber === "") {
            patientController.getUsbData();
        } else {
            sensorSerialNumber = patient.serialNumber;
        }
    }

    Connections {
        target: patientController
        onUsbData: {
            console.log("usb serial number : " + serialNumber);
            if (serialNumber !== "") {
                patient.serialNumber = serialNumber;
                sensorSerialNumber = serialNumber;
            }
        }
    }

    Controls.Popup {
        id: confirmSelectionPopup
        objectName: "ConfirmSelectionPopup"
        popupWidth: 450
        popupHeight: 400
        showButtons: false

        Text {
            id: instruction
            objectName: "Instruction"
            text: qsTr("What do you want to do?")
            color: Style.white
            font.pixelSize: 24
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 10
        }
        Column {
            anchors.centerIn: parent
            spacing: 20

            Controls.Button {
                width: 283
                height: 45
                buttonText: qsTr("Record Pressures")
                buttonTextSize: 14
                buttonColor: Style.blue
                buttonPressedColor: Style.transparent
                borderColor: Style.lightBlue
                onClicked: {
                    confirmSelectionPopup.close();
                    mainStackView.showPage("ImplantRoot.qml", {pageTitle: qsTr("FOLLOW-UP"), sequenceType: "followUp"});
                }
            }
            Controls.Button {
                width: 283
                height: 45
                buttonText: qsTr("Check Sensor Calibration")
                buttonTextSize: 14
                buttonColor: Style.blue
                buttonPressedColor: Style.transparent
                borderColor: Style.lightBlue
                onClicked: {
                    confirmSelectionPopup.close();
                    mainStackView.showPage("ImplantRoot.qml", {pageTitle: qsTr("FOLLOW-UP"), sequenceType: "checkSensor"});
                }
            }
        }
    }
}

