import QtQuick 2.12
import Style 1.0

Column {

    id: heartRateText
    objectName: "HeartRateText"
    property int textSize: 40
    property int labelTextSize: 12

    property alias heartRateVal: heartRate.text

    Text {
        id: heartRate
        objectName: heartRateText.objectName + "_HeartRate"
        font.pointSize: textSize
        color: Style.white
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: label
        objectName: heartRateText.objectName + "_Label"
        text: qsTr("HR")
        font.pointSize: labelTextSize
        color: Style.white
        anchors.horizontalCenter: parent.horizontalCenter
    }
}

