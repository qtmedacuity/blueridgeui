import QtQuick 2.12
import QtCharts 2.3
import WaveChartWidget 1.0
import Style 1.0

WaveChart {
    id: waveChart
    backgroundColor: Style.darkGray
    property var controller: null
    property int scrollType: Drag.YAxis
    property bool showYAxisLabels: true
    property int xAxisFontSize: 10
    property int yAxisFontSize: 7
    readonly property bool zoomInVerticalEnabled: { if (controller) controller.zoomInVerticalEnabled; else false }
    readonly property bool zoomOutVerticalEnabled: { if (controller) controller.zoomOutVerticalEnabled; else false }
    readonly property bool zoomInHorizontalEnabled: { if (controller) controller.zoomInHorizontalEnabled; else false }
    readonly property bool zoomOutHorizontalEnabled: { if (controller) controller.zoomOutHorizontalEnabled; else false }

    xAxis {
        axisColor: Style.white
        span: 30
        lineWeight: 1
        lineEnabled: true
        ticksEnabled: false
        gridEnabled: false
        font { pointSize: xAxisFontSize }
    }
    yAxis {
        axisColor: Style.white
        span: 30
        lineWeight: 1
        lineEnabled: false
        ticksEnabled: false
        gridEnabled: true
        labelsEnabled: showYAxisLabels
        font { pointSize: yAxisFontSize }
    }
    Item {
        id: scrollArea
        objectName: "ScrollArea"
        width: waveChart.width
        height: waveChart.height
        property real scrollHorizontalPercent: (1 - ((waveChart.width - scrollArea.x) / waveChart.width)) * 100
        property real scrollVerticalPercent: (1 - ((waveChart.height - scrollArea.y) / waveChart.height)) * 100
        onScrollHorizontalPercentChanged: {
            if (mouseArea.drag.active && controller)
                controller.scrollHorizontal(scrollHorizontalPercent, true)
        }
        onScrollVerticalPercentChanged: {
            if (mouseArea.drag.active && controller)
                controller.scrollVertical(scrollVerticalPercent, true)
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            drag.target: scrollArea
            drag.axis: waveChart.scrollType
            drag.minimumX: -waveChart.width
            drag.maximumX: waveChart.width
            drag.minimumY: -waveChart.height
            drag.maximumY: waveChart.height
            drag.onActiveChanged: {
                if (!drag.active) {
                    scrollArea.x = 0
                    scrollArea.y = 0
                }
            }
            onPressed: {
                if (controller) {
                    controller.scrollHorizontal(scrollArea.scrollHorizontalPercent, true)
                    controller.scrollVertical(scrollArea.scrollVerticalPercent, true)
                }
            }
            onReleased: {
                if (controller) {
                    controller.scrollHorizontal(scrollArea.scrollHorizontalPercent, true)
                    controller.scrollVertical(scrollArea.scrollVerticalPercent, false)
                }
            }
        }
    }

    function zoomInVertical() { if (controller) controller.zoomInVertical() }
    function zoomOutVertical() { if (controller) controller.zoomOutVertical() }
    function zoomInHorizontal() { if (controller) controller.zoomInHorizontal() }
    function zoomOutHorizontal() { if (controller) controller.zoomOutHorizontal() }
}
