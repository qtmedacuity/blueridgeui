import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0

Item
{

    id: textField
    objectName: "TextField"
    property bool isEntryRequired: false
    property alias textFieldWidth: entryField.width
    property alias textFieldHeight: entryField.height
    property alias textFieldLabel: fieldLabel.text
    property alias textFieldPlaceholder: entryField.placeholderText
    property alias maxCharacterLength: entryField.maximumLength
    property alias textFieldValidator: entryField.validator

    readonly property color warningColor: Style.yellow

    Row
    {
        spacing: 5
        id: labelRow
        Text {
            id: fieldLabel
            objectName: "Label_TextField"
            text: qsTr("Label")
            font.pointSize: 13
            color: Style.white
        }

        Text {
            id: requiredTextIndicator
            text: qsTr("*")
            visible: isEntryRequired
            color: Style.white
            font.pointSize: 13
        }
    }


    Column
    {
        id: fieldColumn
        spacing: 0
        anchors.top: labelRow.bottom
        anchors.margins: 5

        Rectangle
        {
            id: requiredLineIndicator
            objectName: "RequiredLineIndicator_TextField"
            height: 3.75
            width: fieldWidth
            color: warningColor
            visible: false
        }

        TextField
        {
            id: entryField
            objectName: "entryField_TextField"
            font.pointSize: 13

            background: Rectangle {
                border.color: "transparent"
            }
        }
    }

    states: [
        State
        {
            name: "requiredEntry"
            when: isEntryRequired && entryField/*&&  entryField.requiredTextEmpty*/
            PropertyChanges { target: fieldLabel; color: warningColor }
            PropertyChanges { target: requiredTextIndicator; color: warningColor }
            PropertyChanges { target: requiredLineIndicator; visible: true }

        }
    ]
}
