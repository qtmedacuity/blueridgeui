import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0

Item
{
    id: wifiStrengthBars
    objectName: "WifiStrengthBars"
    implicitWidth: 30
    implicitHeight: 30
    state: wifiStrengthController.status

    Image
    {
        id: wifiStrengthImage
        objectName: "WifiStrengthImage"
        source: "qrc:/images/wifi/wifiDisabled"
        fillMode: Image.PreserveAspectFit
        anchors.fill: parent
    }

    states: [
        State {
            name: "zeroBars"
            PropertyChanges { target: wifiStrengthImage; source: "qrc:/images/wifi/wifiBars0" }
        },
        State {
            name: "oneBar"
            PropertyChanges { target: wifiStrengthImage; source: "qrc:/images/wifi/wifiBars1" }
        },
        State {
            name: "twoBars"
            PropertyChanges { target: wifiStrengthImage; source: "qrc:/images/wifi/wifiBars2" }
        },
        State {
            name: "threeBars"
            PropertyChanges { target: wifiStrengthImage; source: "qrc:/images/wifi/wifiBars3" }
        },
        State {
            name: "fault"
            PropertyChanges { target: wifiStrengthImage; source: "qrc:/images/wifi/wifiFault" }
        }
    ]
}
