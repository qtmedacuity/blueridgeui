import QtQuick 2.12
import Style 1.0

Rectangle {
    id: selectableListContent
    objectName: "SelectableContent"

    property string contentText: ""
    signal itemSelected(int index)

    color: "transparent"
    MouseArea {
        id: clickArea
        anchors.fill: parent
        objectName: "SelectableArea"
        Text {
            id: listText
            objectName: "Content"
            text:  contentText
            font.pointSize: 14
            color: Style.lighterBlue
            anchors.verticalCenter: parent.verticalCenter
        }

        onClicked: {
            console.log("index: " + index);
            itemSelected(index)
        }
    }
}
