import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Column
{

    id: dateComboBox
    objectName: "DateComboBox"
    spacing: 2

    property bool isRequired: false

    property alias comboBoxLabel: label.text
    property int yearMin: 1900
    property int yearMax: new Date().getFullYear()

    property var days: ["-day-"]
    property var months: ["-mo-"]
    property var years: ["-yr-"]

    property color textColor: Style.white
    property color warningColor: Style.yellow
    property int fontSize: 16
    property int comboContentHeight: 400

    function getDateValue() {
        // only return a date value if each combo box has a value
        if (dayComboBox.currentIndex !== 0 &&
            monthComboBox.currentIndex !== 0 &&
            yearComboBox.currentIndex !== 0) {
            return dayComboBox.currentText + " " + monthComboBox.currentText + " " + yearComboBox.currentText;
        }
        return "";
    }

    function isValid() {
        if (!isRequired) {
            return true;
        }

        var valid = (dayComboBox.currentIndex !== 0 &&
                     monthComboBox.currentIndex !== 0 &&
                     yearComboBox.currentIndex !== 0);
        if (!valid) {
            state = "requiredEntry";
        }
        return valid;
    }

    Row
    {
        id: labelRow
        spacing: 5

        Text
        {
            id: label
            objectName: "Label"
            font.pointSize: fontSize
            color: textColor
        }

        Text
        {
            id: requiredEntryIndicator
            objectName: "RequiredEntryIndicator"
            text: qsTr("*")
            visible: isRequired
            color: textColor
            font.pointSize: fontSize
        }
    }

    Row
    {
        id: dateRow
        spacing: 7

        Column
        {
            id: dayCol
            spacing: 0

            Rectangle
            {
                id: dayRequiredIndicator
                objectName: "RequiredTextLineIndicator"
                height: 3.75
                width: dayComboBox.width
                color: Style.yellow
                visible: false
            }

            ComboBox
            {
                id: dayComboBox
                width: 80
                height: 35
                background: Rectangle
                {
                    border.color: Style.transparent
                    radius: 1.5
                }

                contentItem: Text {
                    text: dayComboBox.displayText
                    color: dayComboBox.currentIndex === 0 ? Style.mediumGray : Style.black
                    font.pointSize: fontSize
                    verticalAlignment: Text.AlignVCenter
                    leftPadding: 10
                }

                indicator: Canvas
                {
                    objectName: "ComboBoxIndicator"
                    x: dayComboBox.width - width - dayComboBox.rightPadding
                    y: dayComboBox.topPadding + (dayComboBox.availableHeight - height) / 2
                    width: 9
                    height: 8
                    contextType: "2d"

                    onPaint: {
                        context.reset();
                        context.moveTo(0, 0);
                        context.lineTo(width, 0);
                        context.lineTo(width / 2, height);
                        context.closePath();
                        context.fillStyle = Style.mediumGray2
                        context.fill();
                    }
                }

                popup: Popup
                {
                    y: dayComboBox.height - 1
                    width: dayComboBox.width
                    implicitHeight: contentItem.implicitHeight
                    padding: 1

                    contentItem: ListView {
                        clip: true
                        implicitHeight: comboContentHeight
                        model: dayComboBox.popup.visible ? dayComboBox.delegateModel : null
                        currentIndex: dayComboBox.highlightedIndex

                        ScrollIndicator.vertical: ScrollIndicator { }
                    }

                    background: Rectangle {
                        border.color: "#ffffff"
                        radius: 2
                    }
                }
            }
        }
        Column
        {
            id: monthCol
            spacing: 0

            Rectangle
            {
                id: monthRequiredIndicator
                objectName: "RequiredTextLineIndicator"
                height: 3.75
                width: monthComboBox.width
                color: Style.yellow
                visible: false
            }
            ComboBox
            {
                id: monthComboBox
                width: 80
                height: 35
                background: Rectangle
                {
                    border.color: Style.transparent
                    radius: 1.5
                }

                contentItem: Text {
                    text: monthComboBox.displayText
                    color: monthComboBox.currentIndex === 0 ? Style.mediumGray : Style.black
                    font.pointSize: fontSize
                    verticalAlignment: Text.AlignVCenter
                    leftPadding: 10
                }

                indicator: Canvas
                {
                    objectName: "ComboBoxIndicator"
                    x: monthComboBox.width - width - monthComboBox.rightPadding
                    y: monthComboBox.topPadding + (monthComboBox.availableHeight - height) / 2
                    width: 9
                    height: 8
                    contextType: "2d"

                    onPaint: {
                        context.reset();
                        context.moveTo(0, 0);
                        context.lineTo(width, 0);
                        context.lineTo(width / 2, height);
                        context.closePath();
                        context.fillStyle = Style.mediumGray2
                        context.fill();
                    }
                }

                popup: Popup
                {
                    y: monthComboBox.height - 1
                    width: monthComboBox.width
                    implicitHeight: contentItem.implicitHeight
                    padding: 1

                    contentItem: ListView {
                        clip: true
                        implicitHeight: comboContentHeight
                        model: monthComboBox.popup.visible ? monthComboBox.delegateModel : null
                        currentIndex: monthComboBox.highlightedIndex

                        ScrollIndicator.vertical: ScrollIndicator { }
                    }

                    background: Rectangle {
                        border.color: "#ffffff"
                        radius: 2
                    }
                }

            }
        }

        Column
        {
            id: yearCol
            spacing: 0

            Rectangle
            {
                id: yearRequiredIndicator
                objectName: "RequiredTextLineIndicator"
                height: 3.75
                width: yearComboBox.width
                color: Style.yellow
                visible: false
            }

            ComboBox
            {
                id: yearComboBox
                width: 80
                height: 35
                background: Rectangle
                {
                    border.color: Style.transparent
                    radius: 1.5
                }

                contentItem: Text {
                    text: yearComboBox.displayText
                    color: yearComboBox.currentIndex === 0 ? Style.mediumGray : Style.black
                    font.pointSize: fontSize
                    verticalAlignment: Text.AlignVCenter
                    leftPadding: 10
                }

                indicator: Canvas
                {
                    objectName: "ComboBoxIndicator"
                    x: yearComboBox.width - width - yearComboBox.rightPadding
                    y: yearComboBox.topPadding + (yearComboBox.availableHeight - height) / 2
                    width: 9
                    height: 8
                    contextType: "2d"

                    onPaint: {
                        context.reset();
                        context.moveTo(0, 0);
                        context.lineTo(width, 0);
                        context.lineTo(width / 2, height);
                        context.closePath();
                        context.fillStyle = Style.mediumGray2
                        context.fill();
                    }
                }

                popup: Popup
                {
                    y: yearComboBox.height - 1
                    width: yearComboBox.width
                    implicitHeight: contentItem.implicitHeight
                    padding: 1

                    contentItem: ListView {
                        clip: true
                        implicitHeight: contentHeight < comboContentHeight ? contentHeight : comboContentHeight
                        model: yearComboBox.popup.visible ? yearComboBox.delegateModel : null
                        currentIndex: yearComboBox.highlightedIndex

                        ScrollIndicator.vertical: ScrollIndicator { }
                    }

                    background: Rectangle {
                        border.color: "#ffffff"
                        radius: 2
                    }
                }

            }
        }
    }

    states: [
        State
        {
            name: "requiredEntry"
            PropertyChanges { target: label; color: warningColor }
            PropertyChanges { target: requiredEntryIndicator; color: warningColor }
            PropertyChanges { target: dayRequiredIndicator; visible: true }
            PropertyChanges { target: monthRequiredIndicator; visible: true }
            PropertyChanges { target: yearRequiredIndicator; visible: true }
        }
    ]

    Component.onCompleted:
    {

        var date = new Date(0,0,0,0,0,0)
        for(var d=1; d <32; ++d)
        {
            date = new Date(0,0,d,0,0,0)
            days.push(Qt.formatDate(date,"d"))
        }

        for(var m=1; m < 13; ++m)
        {
            date = new Date(0,m,0,0,0,0)
            months.push(Qt.formatDate(date, "MMM"))
        }

        for(var y= yearMax; y > yearMin; --y)
        {
            date = new Date(y,1,1,0,0,0)
            years.push(Qt.formatDate(date, "yyyy"))
        }

        dayComboBox.model = days
        monthComboBox.model = months
        yearComboBox.model = years
    }
}
