import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Popup {
    id: eventSavedPopup
    objectName: "EventSavedPopup"

    property string dateTime: ""
    property string patientName: ""
    property string dob: ""
    property int patientId: 0
    property string serialNumber: ""
    property string implantDate: ""
    property string sendOrSave: ""

    width: 916
    height: 255
    anchors.centerIn: Overlay.overlay
    modal: true
    closePolicy: Popup.CloseOnEscape

    background: Rectangle {
        color: Style.darkGrayAlt
        anchors.fill: parent
        border.width: 4
        border.color: Style.green
    }

    Text {
        id: titleText
        objectName: "Title"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 20
        text: qsTr("Reading successfully ") + sendOrSave + "."
        color: Style.white
        font.pixelSize: 30
    }
    Image {
        id: icon
        objectName: "Icon"
        anchors.left: titleText.right
        anchors.leftMargin: 5
        anchors.verticalCenter: titleText.verticalCenter
        source: "qrc:/images/checkmark_large"
    }

    Column {
        anchors.top: titleText.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: okButton.top
        anchors.margins: 20
        spacing: 10

        Row {
            id: titleRow
            property int textSize: 9

            Text {
                id: readingLabel
                objectName: "ReadingLabel"
                color: Style.white
                font.pointSize: titleRow.textSize
                text: qsTr("READING TAKEN")
                width: 180
                font.bold: true
            }
            Text {
                id: nameLabel
                objectName: "NameLabel"
                color: Style.white
                font.pointSize: titleRow.textSize
                text: qsTr("NAME")
                width: 180
                font.bold: true
            }
            Text {
                id: dobLabel
                objectName: "DobLabel"
                color: Style.white
                font.pointSize: titleRow.textSize
                text: qsTr("DOB")
                width: 130
                font.bold: true
            }
            Text {
                id: mrnLabel
                objectName: "MrnLabel"
                color: Style.white
                font.pointSize: titleRow.textSize
                text: qsTr("MRN/PATIENT ID")
                width: 130
                font.bold: true
            }
            Text {
                id: sensorSerialLabel
                objectName: "SensorSerialLabel"
                color: Style.white
                font.pointSize: titleRow.textSize
                text: qsTr("SENSOR SERIAL #")
                width: 130
                font.bold: true
            }
            Text {
                id: implantDateLabel
                objectName: "ImplantDateLabel"
                color: Style.white
                font.pointSize: titleRow.textSize
                text: qsTr("IMPLANT DATE")
                width: 130
                font.bold: true
            }
        }
        Row {
            Rectangle {
                width: 840
                height: 2
                color: Style.black
            }
        }

        Row {
            id: dataRow
            property int textSize: 12

            Text {
                id: dateTimeData
                objectName: "DateTime"
                color: Style.white
                font.pointSize: dataRow.textSize
                text: dateTime
                width: 180
            }
            Text {
                id: patientNameData
                objectName: "PatientName"
                color: Style.white
                font.pointSize: dataRow.textSize
                text: patientName
                width: 180
            }
            Text {
                id: dobData
                objectName: "DOB"
                color: Style.white
                font.pointSize: dataRow.textSize
                text: dob
                width: 130
            }
            Text {
                id: patientIdData
                objectName: "PatientId"
                color: Style.white
                font.pointSize: dataRow.textSize
                text: patientId
                width: 130
            }
            Text {
                id: serialNumberData
                objectName: "SerialNumber"
                color: Style.white
                font.pointSize: dataRow.textSize
                text: serialNumber
                width: 130
            }
            Text {
                id: implantDateData
                objectName: "ImplantDate"
                color: Style.white
                font.pointSize: dataRow.textSize
                text: implantDate
                width: 130
            }
        }
    }

    Controls.Button {
        id: okButton
        objectName: "OK"
        buttonText: qsTr("OK")
        width: 100
        height: 38
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        buttonColor: Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: {
            close();
            clearValues();
        }
    }

    function clearValues() {
        dateTime = "";
        patientName = "";
        dob = "";
        patientId = 0;
        serialNumber = "";
        implantDate = "";
    }
}
