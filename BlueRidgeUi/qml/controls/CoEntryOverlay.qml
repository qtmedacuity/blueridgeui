import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Style 1.0
import Recording 1.0
import "../controls" as Controls

Controls.StaticWaveOverlay {
    id: coEntryOverlay
    objectName: "CardiacOutputEntry"

    saveEnabled: false

    Text {
        id: timeText
        objectName: "TimeText"
        color: Style.white
        anchors.top: parent.top
        anchors.topMargin: 30
        anchors.left: parent.left
        anchors.leftMargin: 30
        text: (recording) ? recording.recordingTime : ""
    }

    Text {
        id: dateText
        objectName: "DateText"
        color: Style.white
        anchors.top: timeText.top
        anchors.left: timeText.right
        anchors.leftMargin: 10
        text: (recording) ? recording.recordingDate : ""
    }

    RowLayout {
        id: sensorDataRow
        anchors.top: parent.top
        anchors.topMargin: 340
        anchors.left: parent.left
        anchors.leftMargin: 200
        width: 670
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter
        height: 60
        spacing: 70

        Controls.SystolicDiastolicText {
            id: sensorSystolicDiastolicText
            objectName: "SensorSystolicDiastolic"
            Layout.preferredWidth: 240
            textSize: 55
            systolicVal: (recording) ? recording.systolic : ""
            diastolicVal: (recording) ? recording.diastolic : ""
        }

        Controls.MeanText {
            id: sensorMeanText
            objectName: "SensorMean"
            Layout.preferredWidth: 130
            textSize: 55
            meanValue: (recording) ? recording.mean : ""
        }

        Controls.HeartRateText {
            id: sensorHeartRate
            objectName: "SensorHeartRate"
            Layout.preferredWidth: 120
            textSize: 30
            heartRateVal: (recording) ? recording.heartRate : 0
        }
    }


    Text {
        id: coLabel
        objectName: "cardiacOutputLabel"
        text: qsTr("CARDIAC OUTPUT")
        color: Style.white
        font.pointSize: 14
        anchors.right: cardiacOutputText.left
        anchors.bottom: cardiacOutputText.bottom
        anchors.margins: 20
    }


    Controls.CardiacOutputTextField {
        id: cardiacOutputText
        objectName: "CardiacOutput"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: sensorDataRow.bottom
        anchors.topMargin: 75

        onValueEntered: {
            saveEnabled = (value) ? true : false;
        }
    }

    Text {
        id: unitsLabel
        objectName: "UnitsLabel"
        text: qsTr("L/min")
        color: Style.white
        font.pointSize: 14
        anchors.left: cardiacOutputText.right
        anchors.bottom: cardiacOutputText.bottom
        anchors.margins: 20
    }


    onOpened: {
        cardiacOutputText.inFocus = true;
    }

    onSave: {
        console.log("Save cardiac output: ", cardiacOutputText.value);
        if (cardiacOutputText.value !== null) {
            recordingController.saveCardiacOutput(cardiacOutputText.value);
        }
    }

    onDiscard: {
        recordingController.deleteRecording(recording.recordingId);
    }

    onDismissed: {
        recordingController.deleteRecording(recording.recordingId);
    }

    function reset() {
        cardiacOutputText.reset();
        saveEnabled = false;
    }

    Connections {
        target: recordingController
        onSetCardiacOutputComplete: {
            console.log("CoEntryOverlay - save complete");
            close();
        }
        onRecordingDeleteComplete : {
            close();
        }
    }
}
