import QtQuick 2.12
import Style 1.0

Item {

    property int checkboxWidth: 16
    property int checkboxHeight: 16
    property bool checked: false
    property color borderColor: Style.white

    signal boxChecked(bool checked);

    Rectangle {
        id: checkbox
        objectName: "Checkbox"
        width: checkboxWidth
        height: checkboxHeight
        anchors.centerIn: parent
        radius: 2
        border.color: borderColor
        color: Style.transparent
        Text {
            id: checkText
            objectName: "CheckText_CheckBox"
            anchors.centerIn: parent
            text: "X"
            color: Style.white
            visible: checked
        }
        MouseArea {
            id: checkMouseArea
            objectName: "CheckMouseArea_Checkbox"
            anchors.fill: parent
            onClicked: {
                checked = !checked;
                console.log("List check box checked:", checked);
                boxChecked(checked);
            }
        }
    }
}
