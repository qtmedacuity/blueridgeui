import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Controls.StaticWaveOverlay {
    id: paMeanEntryOverlay
    objectName: "paMeanEntryOverlay"

    property int minValue: 0
    property int maxValue: 150
    property bool discarding: false

    signal paMeanSaved()
    signal paMeanDiscarded()

    saveEnabled: false
    showYAxisLabels: false

    RowLayout {
        id: sensorDataRow
        objectName: "SensorDataRow"
        anchors.top: parent.top
        anchors.topMargin: 340
        anchors.left: parent.left
        anchors.leftMargin: 200
        width: 670
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter
        height: 60
        spacing: 70
        visible: false

        Controls.SystolicDiastolicText {
            id: sensorSystolicDiastolicText
            objectName: "SensorSystolicDiastolicText"
            Layout.preferredWidth: 240
            textSize: 55
        }

        Controls.MeanText {
            id: sensorMeanText
            objectName: "SensorMeanText"
            Layout.preferredWidth: 130
            textSize: 55
        }

        Controls.HeartRateText {
            id: sensorHeartRate
            objectName: "SensorHeartRate"
            Layout.preferredWidth: 120
            textSize: 30
            heartRateVal: (recording) ? recording.heartRate : 0
        }
    }

    RowLayout {
        id: paMeanRow
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 150
        anchors.left: parent.left
        anchors.leftMargin: 280
        spacing: 10

        Text {
            id: paMeanLabel
            objectName: "PaMeanLabel"
            text: qsTr("PA CATHETER MEAN")
            color: Style.white
            font.pointSize: 12
            Layout.alignment: Qt.AlignRight
        }

        Controls.NumericTextField {
            id: paMeanText
            objectName: "PaMeanText"

            font.pointSize: 52
            Layout.preferredWidth: 130
            maxLength: 3
            minValue: 0
            maxValue: 150

            onAcceptableInputChanged: {
                if (acceptableInput && validationMessage.visible) {
                    validationMessage.visible = false;
                }
            }
            onInputEntered: {
                if (acceptableInput) {
                    savePaMean(paMeanText.text);
                    saveFocus = true;
                    saveEnabled = false;
                } else {
                    validationMessage.visible = true;
                    saveEnabled = false;
                }
            }
        }
    }

    Text {
        id: validationMessage
        objectName: "ValidationMessage"
        color: Style.yellow
        text: qsTr("Enter a value between " + minValue + " and " + maxValue)
        visible: false
        anchors.top: paMeanRow.bottom
        anchors.bottomMargin: 10
        anchors.left: paMeanRow.left
        horizontalAlignment: Text.AlignHCenter
    }

    onOpened: {
        paMeanText.focus = true;
    }

    onSave: {
        close();
        paMeanSaved();
    }

    onDiscard: discardPaMean();

    onDismissed: discardPaMean();

    onClosed: {
        console.log("(onClosed) pa mean closed.");
        closeNumericKeyboard();
        reset();
    }

    function savePaMean(value) {
        sensorDataRow.visible = false;
        recordingController.paMean = value;
        recordingController.savePaMean();
    }

    function discardPaMean() {
        discarding = true;
        savePaMean(0);
        paMeanText.reset();
    }

    function reset() {
        paMeanText.reset();
        sensorDataRow.visible = false;
        validationMessage.visible = false;
        saveEnabled = false;
        paMeanText.enabled = true;
        paMeanText.focus = false;
        showYAxisLabels = false;
    }

    Connections {
        target: recordingController
        onPaMeanSaveComplete: {
            console.log("pa mean save completed");
            if (discarding) {
                paMeanDiscarded();
                discarding = false;
                close();
            } else {
                // set the sensor data from controller data
                sensorSystolicDiastolicText.systolicVal = recordingController.offsetSystolic();
                sensorSystolicDiastolicText.diastolicVal = recordingController.offsetDiastolic();
                sensorMeanText.meanValue = recordingController.offsetMean();
                setChartVerticalOffset(recordingController.paMeanOffset());
                sensorDataRow.visible = true;
                saveEnabled = true;
                showYAxisLabels = true;
            }
        }
    }
}
