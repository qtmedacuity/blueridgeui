import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0

Item
{
    id: cellularStrengthBars
    objectName: "CellularStrengthBars"
    implicitWidth: 25
    implicitHeight: 25
    state: cellularStrengthController.status

    Image
    {
        id: cellularStrengthImage
        objectName: "CellularStrengthImage"
        source: "qrc:/images/cellular/cellularDisabled"
        fillMode: Image.PreserveAspectFit
        anchors.fill: parent
    }

    states: [
        State {
            name: "zeroBars"
            PropertyChanges { target: cellularStrengthImage; source: "qrc:/images/cellular/cellularBars0" }
        },
        State {
            name: "oneBar"
            PropertyChanges { target: cellularStrengthImage; source: "qrc:/images/cellular/cellularBars1" }
        },
        State {
            name: "twoBars"
            PropertyChanges { target: cellularStrengthImage; source: "qrc:/images/cellular/cellularBars2" }
        },
        State {
            name: "threeBars"
            PropertyChanges { target: cellularStrengthImage; source: "qrc:/images/cellular/cellularBars3" }
        },
        State {
            name: "fourBars"
            PropertyChanges { target: cellularStrengthImage; source: "qrc:/images/cellular/cellularBars4" }
        },
        State {
            name: "fault"
            PropertyChanges { target: cellularStrengthImage; source: "qrc:/images/cellular/cellularFault" }
        }
    ]
}
