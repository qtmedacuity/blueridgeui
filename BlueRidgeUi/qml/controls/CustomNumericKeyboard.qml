import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Rectangle {

    id: customNumericKeyboard
    objectName: "NumericKeyboard"
    width: 278
    height: 300
    z: 99
    color: Style.black

    property int textSize: 25
    property color keyColor: Style.darkGray
    property color keyPressedColor: Style.gray
    property int keyHeight: 64
    property int keyWidth: 80

    signal enterKeyPressed()
    signal deleteKeyPressed()
    signal keyPressed(var key)

    Grid {
        columns: 3
        spacing: 5
        anchors.centerIn: parent
        Repeater {
            model: [qsTr("1"), qsTr("2"), qsTr("3"), qsTr("4"), qsTr("5"), qsTr("6"), qsTr("7"), qsTr("8"), qsTr("9")]
            Controls.Button {
                objectName: modelData + "_Key"
                width: keyWidth
                height: keyHeight
                buttonColor: keyColor
                buttonText: modelData
                buttonTextSize: textSize
                buttonPressedColor: keyPressedColor
                onClicked: {
                    keyPressed(buttonText);
                }
            }
        }

        Controls.Button {
            objectName: "Delete_Key"
            width: keyWidth
            height: keyHeight
            buttonColor: keyColor
            buttonIconSource: "qrc:/images/deleteKeyIcon"
            buttonIconVisible: true
            buttonPressedColor: keyPressedColor
            onClicked: deleteKeyPressed();
        }

        Controls.Button {
            objectName: buttonText + "_Key"
            width: keyWidth
            height: keyHeight
            buttonColor: keyColor
            buttonText: qsTr("0")
            buttonTextSize: textSize
            buttonPressedColor: keyPressedColor
            onClicked: keyPressed(buttonText);
        }

        Controls.Button {
            objectName: "Next_Key"
            width: keyWidth
            height: keyHeight
            buttonColor: Style.blue
            buttonIconSource: "qrc:/images/nextKeyIcon"
            buttonIconVisible: true
            buttonPressedColor: keyPressedColor
            onClicked: {
                customNumericKeyboard.visible = false;
                enterKeyPressed();
            }
        }
    }
}
