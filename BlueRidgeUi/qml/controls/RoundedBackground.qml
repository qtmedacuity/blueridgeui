import QtQuick 2.12
import Style 1.0

Rectangle {
    id: roundedBackground
    objectName: "RoundedBackground"

    property int backgroundRadius: 60
    property color backgroundColor: Style.darkGray

    radius: backgroundRadius
    color: backgroundColor

    Rectangle {
        width: roundedBackground.width
        height: backgroundRadius * 2
        anchors.left: roundedBackground.left
        anchors.top: roundedBackground.top
        color: backgroundColor
    }

    Rectangle {
        height: roundedBackground.height
        width: backgroundRadius * 2
        anchors.bottom: roundedBackground.bottom
        anchors.right: roundedBackground.right
        color: backgroundColor
    }
}
