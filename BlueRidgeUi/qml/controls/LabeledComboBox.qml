import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import QtGraphicalEffects 1.12

Column
{

    id: labeledComboBox
    objectName: "LabeledComboBox"
    spacing: 2

    property alias comboBoxWidth: selectionField.width
    property alias comboBoxHeight: selectionField.height
    property alias comboBoxLabel: label.text
    property alias comboBoxModel: selectionField.model
    property alias comboBoxIndex: selectionField.currentIndex
    property alias comboBoxText: selectionField.currentText
    property bool isRequired: false

    property bool touched: false

    function isValid() {
        var valid = isRequired && selectionField.currentIndex !== 0;
        if (!valid && state == "") {
            state = "requiredEntry"
        }
        return valid;
    }

    Row
    {
        id: labelRow
        spacing: 5

        Text
        {
            id: label
            objectName: "Label"
            font.pointSize: 16
            color: Style.white
        }

        Text
        {
            id: requiredEntryIndicator
            objectName: "RequiredEntryIndicator"
            text: qsTr("*")
            visible: isRequired
            color: Style.white
            font.pointSize: 16
        }
    }


    Column
    {
        id: comboBoxColumn
        spacing: 0

        Rectangle
        {
            id: requiredLineIndicator
            objectName: "RequiredTextLineIndicator"
            height: 3.75
            width: comboBoxWidth
            color: Style.yellow
            visible: false
        }

        ComboBox
        {
            id: selectionField
            objectName: "SelectionField"
            font.pointSize: 16
            height: 35

            contentItem: Text {
                text: selectionField.displayText
                color: selectionField.currentIndex === 0 ? Style.mediumGray : Style.black
                font.pointSize: selectionField.font.pointSize
                verticalAlignment: Text.AlignVCenter
                leftPadding: 10
            }

            background: Rectangle
            {
                border.color: Style.transparent
                radius: 1.5
            }


            indicator: Canvas
            {
                id: indicatorCanvas
                objectName: "ComboBoxIndicator"
                x: selectionField.width - width - selectionField.rightPadding
                y: selectionField.topPadding + (selectionField.availableHeight - height) / 2
                width: 12
                height: 8
                contextType: "2d"

                Connections {
                    target: selectionField
                    onPressedChanged: indicatorCanvas.requestPaint()
                }

                onPaint: {
                    context.reset();
                    context.moveTo(0, 0);
                    context.lineTo(width, 0);
                    context.lineTo(width / 2, height);
                    context.closePath();
                    context.fillStyle = Style.mediumGray2
                    context.fill();
                }
            }


            popup: Popup
            {
                y: selectionField.height - 1
                width: selectionField.width
                implicitHeight: contentItem.implicitHeight
                padding: 1

                contentItem: ListView {
                    clip: true
                    implicitHeight: contentHeight
                    model: selectionField.popup.visible ? selectionField.delegateModel : null
                    currentIndex: selectionField.highlightedIndex

                    ScrollIndicator.vertical: ScrollIndicator { }
                }

                background: Rectangle {
                    border.color: "#ffffff"
                    radius: 2
                }
            }

            onActivated: {
                console.log("combo box activated ", index);
                touched = true;
            }
        }
    }

    states: [
        State
        {
            name: "requiredEntry"
            when: touched && isRequired && selectionField.currentIndex === 0
            PropertyChanges { target: label; color: Style.yellow }
            PropertyChanges { target: requiredEntryIndicator; color: Style.yellow }
            PropertyChanges { target: requiredLineIndicator; visible: true }

        }
    ]
}

