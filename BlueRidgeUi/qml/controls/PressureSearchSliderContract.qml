import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0
import "../controls" as Controls

Controls.PressureSearchSliderComponent {
    id: pressureSearchSliderContract
    objectName: "PressureSearchSliderContract"
    topCapImage: "qrc:/images/pressureSearchBar/tickSmallTop"
    barImage: "qrc:/images/pressureSearchBar/bar"
    bottomCapImage: "qrc:/images/pressureSearchBar/tickSmallBottom"

    Controls.ColoredIcon {
        id: centerDot
        iconColor: contentColor
        iconImage: "qrc:/images/pressureSearchBar/circle"
        width: bar.width * 0.4
        height: width
        anchors.centerIn: bar
    }
}
