import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0

Item {
    id: signalStrengthIndicator
    objectName: "SignalStrengthIndicator"
    implicitWidth: 100
    implicitHeight: 100

    property bool isStatic: false
    property double staticSignalStrength: 0.0
    property bool pulsatilityOverride: false
    property double signalStrength: (!isStatic)
                                    ? pasignalStrengthController.strength : staticSignalStrength
    property color indicatorColor : Style.red
    readonly property color backgroundColor: Style.mediumGray
    state: (!isStatic) ? pasignalStrengthController.status :
                         pasignalStrengthController.getStatusAt(staticSignalStrength)

    Rectangle
    {
        id: rec
        objectName: "SignalStrengthIndicator_Background"
        anchors.fill: parent
        color: Style.transparent
        radius: width/2
        Canvas
        {
            id: indicator
            objectName: "SignalStrengthIndicator_Canvas"
            anchors.fill: parent
            anchors.margins: 2

            onPaint: {

                var ctx = getContext("2d")
                ctx.lineWidth = width/5
                var radius =  width/2 - ctx.lineWidth/2

                // Draw background fill arc
                ctx.beginPath()
                ctx.fillStyle = backgroundColor
                ctx.strokeStyle = backgroundColor
                ctx.arc(width/2, height/2, radius, 0, 2*Math.PI)
                ctx.stroke();
                ctx.closePath()

                // Draw strength indicator arc
                ctx.fillStyle = indicatorColor
                ctx.strokeStyle = indicatorColor

                var originX = width/2
                var originY = height/2

                var startAngle = ((Math.PI/180.0)*270.0)
                var endAngle = startAngle + (((Math.PI/180.0))*(signalStrength*360.0/100.0))
                ctx.beginPath()
                ctx.arc(width/2, height/2, radius, startAngle, endAngle, false)
                ctx.stroke()
                ctx.closePath()
            }
        }

        Text {
            id: label
            objectName: "SignalStrengthIndicator_Label"
            property int value: 0.0 <= signalStrength ? 100.0 >= signalStrength ? signalStrength : 100 : 0.0
            text: value.toString()
            anchors.centerIn: parent
            anchors.margins: 5
            font.pointSize: 26
            font.bold: true
            color: Style.white
        }
    }

    Component.onCompleted: indicator.requestPaint()
    onSignalStrengthChanged: indicator.requestPaint()
    onStateChanged: indicator.requestPaint()
    onPulsatilityOverrideChanged: { pasignalStrengthController.pulsatilityOverride = pulsatilityOverride }

    states: [
        State {
            name: "good"
            PropertyChanges {target: signalStrengthIndicator; indicatorColor: Style.green}
        },
        State {
            name: "weak"
            PropertyChanges {target: signalStrengthIndicator; indicatorColor: Style.orange}
        },
        State {
            name: "noPulse"
            PropertyChanges {target: signalStrengthIndicator; indicatorColor: Style.lightGray}
        }
    ]
}
