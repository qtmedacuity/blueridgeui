import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls
import "../controls/popupContent" as PopupContent

Item {
    anchors.fill: parent

    property int minValue: 0
    property int maxValue: 150

    property var initiatedTakeReading: ({})

    Controls.Button {
        id: helpButton
        objectName: "HelpButton"
        height: 23
        width: 41
        anchors.top: recalibrateButton.top
        anchors.right: recalibrateButton.left
        anchors.rightMargin: 10
        buttonText: qsTr("Help")
        buttonTextSize: 10
        buttonColor: Style.transparent
        buttonPressedColor: Style.blue
        borderColor: Style.blue
        onClicked: {
            console.log("help button clicked");
        }
    }

    Controls.Button {
        id: recalibrateButton
        objectName: "RecalibrateButton"
        height: 23
        width: 120
        anchors.top: parent.top
        anchors.topMargin: 25
        anchors.right: parent.right
        anchors.rightMargin: 20
        buttonText: qsTr("Recalibrate Sensor")
        buttonTextSize: 10
        buttonColor: Style.transparent
        buttonPressedColor: Style.blue
        borderColor: Style.blue
        visible: mainWindow.eventType === "NewImplant"
        onClicked: {
            console.log("recalibrate button clicked");
        }
    }

    Text {
        id: recordedPressuresLabel
        objectName: "RecordedPressuresLabel"
        anchors.left: recordButton.right
        anchors.leftMargin: 20
        anchors.top: recalibrateButton.top
        text: qsTr("RECORDED PRESSURES") + (recordedPressureList.count > 0 ? " (" + recordedPressureList.count +"):" : ":")
        color: recordedPressureList.count === 0 ? Style.darkGrayAlt : Style.white
        font.family: Style.brandon
        font.pointSize: 10
    }

    Controls.RecordPressureBlankCard {
        id: blankCard1
        objectName: "BlankCard1"
        anchors.top: recalibrateButton.bottom
        anchors.topMargin: 10
        anchors.left: recordButton.right
        anchors.leftMargin: 20
        visible: recordedPressureList.count < 1
    }

    Controls.RecordPressureBlankCard {
        id: blankCard2
        objectName: "BlankCard2"
        anchors.top: blankCard1.top
        anchors.left: blankCard1.right
        anchors.leftMargin: 20
        visible: recordedPressureList.count < 2
    }

    Controls.RecordPressureBlankCard {
        id: blankCard3
        objectName: "BlankCard3"
        anchors.top: blankCard1.top
        anchors.left: blankCard2.right
        anchors.leftMargin: 20
        visible: recordedPressureList.count < 3
    }

    ListView {
        id: recordedPressureList
        objectName: "RecordedPressuresList"
        anchors.top: recalibrateButton.bottom
        anchors.topMargin: 10
        anchors.left: recordButton.right
        anchors.leftMargin: 20
        anchors.bottom: enterCOButton.top
        anchors.right: parent.right
        anchors.rightMargin: 20
        orientation: ListView.Horizontal
        spacing: 20
        model: recordingListModel
        cacheBuffer: 240
        delegate: Controls.RecordPressureDelegate {
            objectName: "RecordedPressureCard_" + index
            onCardClicked: {
                console.log("(record pressures card clicked) index: ", index);
                var selectedRecording = recordedPressureList.model.get(index);
                if (selectedRecording) {
                    mainWindow.openRecConfirmationOverlay(selectedRecording, false);
                } else {
                    console.log("selected recording is null");
                }
            }
            onDeleteClicked: {
                 console.log("(record pressures delete button clicked) index: ", index);
                var selectedRecording = recordedPressureList.model.get(index);
                console.log("selected recording", selectedRecording.recordingId);
                deleteRecordingPopup.recording = selectedRecording
                deleteRecordingPopup.open();
            }
        }

        onCountChanged: {
            console.log("list count changed ", count);
            if (count >= 3) {
                recordButton.state = "finished"
            } else {
                recordButton.state = ""
            }
            positionViewAtEnd();
            currentIndex = count -1
        }

        ScrollBar.horizontal: ScrollBar {
            id: listscroll
            objectName: "ScrollBar"
        }
    }

    Controls.Button {
        id: enterCOButton
        objectName: "CalibrateCO"
        height: 38
        width: 148
        anchors.bottom: reviewButton.bottom
        anchors.right: enterRHCButton.left
        anchors.rightMargin: 10
        buttonText: qsTr("Calibrate CO")
        buttonTextSize: 14
        buttonColor: (!buttonIconVisible) ? Style.blue : Style.transparent
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        disabledBorderColor: (!buttonIconVisible) ? Style.gray : Style.blue
        disabledTextColor:  (!buttonIconVisible) ? Style.gray : Style.white
        buttonIconSource: "qrc:/images/checkmark"
        buttonIconHeight: 13
        buttonIconWidth: 15
        buttonIconVisible: false
        clip: true
        enabled: false
        visible: mainWindow.eventType === "NewImplant"
        onClicked: {
            if (!buttonIconVisible) {
                console.log("enter CO button clicked");
                initiatedTakeReading = enterCOButton
                recordingController.takeReading();
                enterCOButton.enabled = false
            }
        }
    }

    Controls.Button {
        id: enterRHCButton
        objectName: "EnterRHC"
        height: 38
        width: 120
        anchors.bottom: reviewButton.bottom
        anchors.right: reviewButton.left
        anchors.rightMargin: 10
        buttonText: qsTr("Enter RHC")
        buttonTextSize: 14
        buttonColor: (buttonIconVisible) ? Style.transparent : Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        disabledBorderColor:  (buttonIconVisible) ? Style.blue : Style.gray
        disabledTextColor: (buttonIconVisible) ? Style.white : Style.gray
        buttonIconSource: "qrc:/images/checkmark"
        buttonIconHeight: 13
        buttonIconWidth: 15
        buttonIconVisible: false
        clip: true
        enabled: false
        visible: mainWindow.eventType === "NewImplant"
        onClicked: {
            if (!buttonIconVisible) {
                console.log("enter RHC button clicked");
                initiatedTakeReading = enterRHCButton
                recordingController.takeReading();
                enterRHCButton.enabled = false
            }
        }
    }

    Controls.Button {
        id: reviewButton
        objectName: "ReviewButton"
        height: 38
        width: 120
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 20
        buttonText: qsTr("Review")
        buttonTextSize: 14
        buttonColor: Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        enabled: false
        onClicked: {
            console.log("review button clicked");
            var listCount = recordedPressureList.count;
            if (listCount < 3) {
                reviewWarningPopup.reviewMsg =  listCount === 0 ?
                            qsTr("You have not recorded pressures. Would you like to continue to the review screen?")
                          : qsTr("You have taken less than three recordings. Would you like to continue to the review screen?");
                reviewWarningPopup.open();
            } else {
                goToReview();
            }
        }
    }

    Controls.RecordButton{
        id: recordButton
        objectName: "RecordButton"
        width: 205
        height: 205
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 40
        onClicked: {
            console.log("record button clicked");
            recordingController.recordPressures();
            recordButton.state = "recording";
        }
    }

    Component.onCompleted: {
        recordButton.state = "waiting"
        recordingController.getRecordingStatus("Start");
    }

    Component.onDestruction: {
        console.log("clear the recording list model");
        if (recordingListModel) {
            recordingListModel.clearRecordings();
        }
    }

    Connections {
        target: recordingController
        onReadyToRecord: {
            console.log("record pressures ready to record");
            recordButton.state = ""
            recordingController.getRecordingStatus("Stop");
            enterCOButton.enabled = true;
            enterRHCButton.enabled = true;
            reviewButton.enabled = true;
        }
        onRecordingPressuresFinished: {
            var recording = recordingController.getNewRecording();
            console.log("recording pressures, recording: ", JSON.stringify(recording));
            mainWindow.openRecConfirmationOverlay(recording, true);
        }
        onRecordingSaveComplete: {
            console.log("recording save complete");
            mainWindow.closeRecConfirmationOverlay();
        }
        onRecordingDeleteComplete: {
            console.log("recording delete complete");
            if (mainWindow.isRecConfirmationOverlayOpen) {
                mainWindow.closeRecConfirmationOverlay();
            }
            if (deleteRecordingPopup.opened) {
                deleteRecordingPopup.close();
            }
        }
        onTakeReadingFinished: {
            if (initiatedTakeReading == enterCOButton) {
                mainWindow.openCoEntryOverlay(recordingController.getNewRecording());
                enterCOButton.enabled = true;
            }
            else if (initiatedTakeReading == enterRHCButton) {
                mainWindow.openRHCEntryOverlay(recordingController.getNewRecording());
                enterRHCButton.enabled = true
            }
        }
        onSetCardiacOutputComplete: {
            enterCOButton.enabled = false;
            enterCOButton.buttonIconVisible = true;
        }
        onSetRhcValuesComplete: {
            enterRHCButton.enabled = false;
            enterRHCButton.buttonIconVisible = true;
        }
    }

    Connections {
        target: mainWindow
        onRecordingSaved: {
            console.log("recording saved: ", JSON.stringify(recording));
            recordingController.saveRecording(recording);
        }
        onRecordingDiscarded: {
            console.log("recording discarded isNew= " + isNew + " " + JSON.stringify(recording));
            if (isNew) {
                recordingController.deleteRecording(recording.recordingId);
            } else {
                deleteRecordingPopup.recording = recording;
                deleteRecordingPopup.open()
            }

        }
        onRecConfirmationOverlayClosed: {
            recordButton.state = "";
        }
    }

    PopupContent.DeleteRecording {
        id: deleteRecordingPopup
        objectName: "DeleteRecordingPopup"
        onDeleteClicked: {
            recordingController.deleteRecording(recordingId);
        }
    }

    Controls.Popup {
        id: reviewWarningPopup
        objectName: "ReviewWarningPopup"
        width: 550
        height: 220
        buttonHeight: 45
        property alias reviewMsg: questionText.text

        Text {
            id: questionText
            objectName: "QuestionText"
            color: Style.white
            anchors.top: parent.top
            anchors.topMargin: 20
            anchors.left: parent.left
            width: parent.width - 20
            wrapMode: Text.WordWrap
            font.pixelSize: 24
        }

        onPopupButtonClicked: {
            if (buttonName === "right") {
                goToReview();
            }
        }
    }

    function goToReview() {
        mainStackView.showPage("Review.qml", {
                                   pageTitle: (mainWindow.eventType === "NewImplant")
                                              ? qsTr("NEW IMPLANT") : qsTr("FOLLOW-UP"),
                                   sensorPosition: implantRoot.sensorPosition});
    }
}
