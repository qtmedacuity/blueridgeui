import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0

Item {
    id: overlayContainer
    anchors.fill: mainWindow.contentItem
    z: 2

    Rectangle {
        id: rect1
        anchors.fill: parent
        color: Style.black
        opacity: 0.8
    }

    MouseArea {
        id: modalMouseArea
        anchors.fill: parent
    }
}
