import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls

Item {
    objectName: "ReviewDataValues"
    property alias eventTime: eventTime.text
    property alias sys: systolicDiastolicText.systolicVal
    property alias dia: systolicDiastolicText.diastolicVal
    property alias meanVal: meanText.meanValue
    property alias hRate: heartRateText.text
    property alias refSystolic: refSystolicDiastolicText.systolicVal
    property alias refDiastolic: refSystolicDiastolicText.diastolicVal
    property alias refMean: refMeanText.meanValue
    property alias signalStrength: signalStrength.sensorStrengthVal
    property bool showReferenceRow: true

    property int pressureTextSize: 20

    RowLayout {
        id: titleRow
        anchors.top: parent.top
        anchors.left: parent.left

        Text {
            id: eventTime
            objectName: "EventTime"
            color: Style.white
            font.pointSize: 8
            Layout.preferredWidth: 55
            Layout.alignment: Qt.AlignLeft
        }
        Text {
            objectName: "SystolicDiastolicLabel"
            text: qsTr("Systolic/Diastolic")
            color: Style.white
            font.pointSize: 8
            Layout.preferredWidth: 100
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            objectName: "MeanLabel"
            text: qsTr("Mean")
            color: Style.white
            font.pointSize: 8
            Layout.preferredWidth: 55
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            objectName: "HeartRateLabel"
            text: qsTr("HR")
            color: Style.white
            font.pointSize: 8
            Layout.preferredWidth: 45
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            objectName: "SignalLabel"
            text: qsTr("Signal")
            color: Style.white
            font.pointSize: 8
            Layout.preferredWidth: 45
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
    RowLayout {
        id: sensorRow
        anchors.top: titleRow.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        Text {
            id: sensorLabel
            objectName: "SensorLabel"
            text: qsTr("SENSOR")
            font.pointSize: 9
            color: Style.white
            Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
            Layout.bottomMargin: 2
            Layout.preferredWidth: 55
        }

        Controls.SystolicDiastolicText {
            id: systolicDiastolicText
            objectName: "SystolicDiastolicText"
            Layout.preferredWidth: 100
            Layout.alignment: Qt.AlignHCenter
            showLabel: false
            textSize: pressureTextSize
        }

        Controls.MeanText {
            id: meanText
            objectName: "MeanText"
            Layout.preferredWidth: 55
            Layout.alignment: Qt.AlignHCenter
            textSize: pressureTextSize
            showLabel: false
        }
        Text {
            id: heartRateText
            objectName: "HeartRate"
            color: Style.white
            font.pointSize: pressureTextSize
            Layout.preferredWidth: 45
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
        }
        Controls.SignalStrengthIcon {
            id: signalStrength
            objectName: "SignalStrength"
            Layout.leftMargin: 10
            Layout.alignment: Qt.AlignHCenter
            visible: sensorStrengthVal !== 0
        }
    }
    RowLayout {
        id: referenceRow
        anchors.top: sensorRow.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        Text {
            id: refLabel
            objectName: "RefLabel"
            text: qsTr("REF")
            font.pointSize: 9
            color: Style.white
            Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
            Layout.bottomMargin: 2
            Layout.preferredWidth: 55
            visible: showReferenceRow
        }

        Item {
            id: blankItem
            objectName: "RefBlankItem"
            Layout.preferredWidth: 100
            Layout.alignment: Qt.AlignHCenter
            visible: (showReferenceRow && refSystolic === "")
        }

        Controls.SystolicDiastolicText {
            id: refSystolicDiastolicText
            objectName: "RefSystolicDiastolicText"
            visible: (showReferenceRow && refSystolic !== "")
            Layout.preferredWidth: 100
            Layout.alignment: Qt.AlignHCenter
            showLabel: false
            textSize: pressureTextSize

        }

        Controls.MeanText {
            id: refMeanText
            objectName: "RefMeanText"
            Layout.preferredWidth: 55
            Layout.alignment: Qt.AlignHCenter
            textSize: pressureTextSize
            showLabel: false
            visible: showReferenceRow
        }
    }
}
