import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls

Rectangle {
    id: recordPressureCard
    objectName: "RecordPressureCard"

    color: Style.darkGrayAlt

    property int pressureTextSize: 20
    property bool showDelete: true

    property alias recordedTime: timeText.text
    property alias heartRate: heartRateText.text
    property alias systolicPressure: systolicDiastolicText.systolicVal
    property alias diastolicPressure: systolicDiastolicText.diastolicVal
    property alias mean: meanText.meanValue
    property alias referenceSystolic: refSystolicDiastolicText.systolicVal
    property alias referenceDiastolic: refSystolicDiastolicText.diastolicVal
    property alias referenceMean: refMeanText.meanValue
    property alias signalStrength: signalStrength.sensorStrengthVal
    property alias hasNotes: noteImg.visible
    property alias positionIndicator: positionImg.position

    signal deletePressure();

    Text {
        id: timeText
        objectName: "RecordedTime"
        color: Style.white
        font.pointSize: 10
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 15
    }

    Text {
        id: heartRateLabel
        objectName: "HeartRateLabel"
        color: Style.lighterGray
        font.pointSize: 8
        text: qsTr("HR")
        anchors.right: heartRateText.left
        anchors.rightMargin: 5
        anchors.bottom: heartRateText.bottom
    }

    Text {
        id: heartRateText
        objectName: "HeartRate"
        color: Style.white
        font.pointSize: 15
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: 10
        anchors.rightMargin: 10
    }

    RowLayout {
        id: sensorDataRow
        objectName: "SensorDataRow"
        anchors.top: timeText.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 10

        Text {
            id: sensorLabel
            objectName: "SensorLabel"
            text: qsTr("SENSOR")
            font.pointSize: 10
            color: Style.white
            Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
            Layout.bottomMargin: 2
            Layout.preferredWidth: 60
        }

        Controls.SystolicDiastolicText {
            id: systolicDiastolicText
            objectName: "SensorSystolicDiastolic"
            showLabel: false
            textSize: pressureTextSize
        }

        Controls.MeanText {
            id: meanText
            objectName: "SensorMean"
            textSize: pressureTextSize
            showLabel: false
        }

    }

    RowLayout {
        id: referenceDataRow
        objectName: "ReferenceDataRow"
        anchors.top: sensorDataRow.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 10
        visible: (referenceSystolic > 0 || referenceDiastolic > 0 || referenceMean > 0)

        Text {
            id: referenceLabel
            objectName: "ReferenceLabel"
            text: qsTr("REF")
            font.pointSize: 10
            color: Style.white
            Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
            Layout.bottomMargin: 2
            Layout.preferredWidth: 60
        }

        Controls.SystolicDiastolicText {
            id: refSystolicDiastolicText
            objectName: "RefSystolicDiastolic"
            showLabel: false
            textSize: pressureTextSize
        }

        Controls.MeanText {
            id: refMeanText
            objectName: "ReferenceMean"
            textSize: pressureTextSize
            showLabel: false
        }

    }

    Controls.SignalStrengthIcon {
        id: signalStrength
        objectName: "SignalStrength"
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
    }

    Image {
        id: positionImg
        objectName: position + "PositionIndicator"
        property string position

        anchors.left: signalStrength.right
        anchors.leftMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        width: 35
        height: 23
        onPositionChanged: {
            console.log("(record card) position: ", position);
            if (position === "30_Degrees") {
                source = "qrc:/images/30Degree";
            } else if (position === "45_Degrees") {
                source = "qrc:/images/45Degree";
            } else if (position === "90_Degrees") {
                source = "qrc:/images/90Degree"
            } else {
                source = "qrc:/images/0Degree";
            }
        }
    }

    Image {
        id: noteImg
        objectName: "NoteIndicator"
        anchors.left: positionImg.right
        anchors.leftMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        source: "qrc:/images/cardNoteIcon"
    }


    Controls.IconButton {
        id: deleteButton
        objectName: "DeleteButton"
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        dynamicColors: false
        width: 24
        height: 29
        iconImage: "qrc:/images/trashIcon"
        visible: showDelete
        onClicked: {
            console.log("(record card) delete button pressed")
            deletePressure();
        }
    }
}
