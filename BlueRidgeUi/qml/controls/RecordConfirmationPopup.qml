import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.VirtualKeyboard 2.4
import Style 1.0
import "../controls" as Controls
import "../views" as Views

Controls.StaticWaveOverlay {
    id: recordConfirmationPopup
    objectName: "RecordConfirmationPopup"

    property bool isNewRecording: false

    signal saveRecording()
    signal discardRecording()

    Text {
        id: timeText
        objectName: "TimeText"
        color: Style.white
        anchors.top: parent.top
        anchors.topMargin: 30
        anchors.left: parent.left
        anchors.leftMargin: 30
    }

    Text {
        id: dateText
        objectName: "DateText"
        color: Style.white
        anchors.top: timeText.top
        anchors.left: timeText.right
        anchors.leftMargin: 10
    }

    ColumnLayout {
        id: columnLayout
        objectName: "ColumnLayout"
        anchors.left: parent.left
        anchors.leftMargin: 160
        anchors.bottom: positionButtonRow.top
        anchors.bottomMargin: 20
        width: 800
        spacing: 10

        RowLayout {
            id: sensorDataRow
            objectName: "SensorDataRow"
            Layout.fillWidth: true
            height: 60

            Text {
                id: sensorLabel
                objectName: "SensorLabel"
                text: qsTr("SENSOR")
                font.pointSize: 15
                color: Style.white
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                Layout.bottomMargin: 10
                Layout.preferredWidth: 120
            }

            Controls.SystolicDiastolicText {
                id: sensorSystolicDiastolicText
                objectName: "SensorSystolicDiastolic"
                Layout.preferredWidth: 240
                showLabel: false
                textSize: 50
            }

            Controls.MeanText {
                id: sensorMeanText
                objectName: "SensorMean"
                Layout.preferredWidth: 130
                showLabel: false
                textSize: 50
            }

            Controls.HeartRateText {
                id: sensorHeartRate
                objectName: "SensorHeartRate"
                Layout.preferredWidth: 120
                textSize: 25
            }
        }

        RowLayout {
            id: referenceDataRow
            objectName: "ReferenceDataRow"
            Layout.fillWidth: true
            height: 60

            Text {
                id: referenceLabel
                objectName: "ReferenceLabel"
                text: qsTr("REFERENCE")
                color: Style.white
                font.pointSize: 15
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                Layout.preferredWidth: 120
                Layout.bottomMargin: 30
            }

            Column {
                id: refSysDiastolticGroup
                objectName: "RefSystolicDiastolicData"
                Layout.preferredWidth: 240
                property bool invalidateSysDiastolic: false
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    Controls.NumericTextField {
                        id: refSystolicTextBox
                        objectName: "ReferenceSystolic"
                        font.pointSize: 50
                        width: 90
                        maxLength: 2
                        minValue: 0
                        maxValue: 99
                        invalid: refSysDiastolticGroup.invalidateSysDiastolic
                        onFocusChanged: {
                            if (refSysDiastolticGroup.invalidateSysDiastolic) {
                                refSysDiastolticGroup.invalidateSysDiastolic = false;
                                validationMessage.visible = false;
                            }
                        }
                        onInputEntered: {
                            refDiastolicTextBox.focus = true;
                        }
                    }

                    Text {
                        id: separator
                        objectName: "ReferenceSeparator"
                        text: "/"
                        font.pointSize: 50
                        color: Style.lighterGray
                    }

                    Controls.NumericTextField {
                        id: refDiastolicTextBox
                        objectName: "ReferenceDiastolic"
                        font.pointSize: 50
                        width: 90
                        maxLength: 2
                        minValue: 0
                        maxValue: 99
                        invalid: refSysDiastolticGroup.invalidateSysDiastolic
                        onFocusChanged: {
                            if (refSysDiastolticGroup.invalidateSysDiastolic) {
                                refSysDiastolticGroup.invalidateSysDiastolic = false;
                                validationMessage.visible = false;
                            }
                        }
                        onInputEntered: {
                            refMean.textField.focus = true;
                        }
                    }
                }
                Text {
                    id: refSystolicDiastolicLabel
                    objectName: "RefSystolicDiastolicLabel"
                    text: qsTr("Systolic/Diastolic")
                    font.pointSize: 8
                    color: refSysDiastolticGroup.invalidateSysDiastolic ? Style.yellow : Style.white
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }
            Controls.ParenNumericTextField {
                id: refMean
                objectName:  "ReferenceMeanData"
                label: qsTr("mean")
                Connections {
                    target: refMean.textField
                    onFocusChanged: {
                        if (refMean.invalidate) {
                            refMean.invalidate = false;
                            validationMessage.visible = false;
                        }
                    }
                }
            }
            Text {
                id: validationMessage
                objectName: "ValidationMessage"
                font.pointSize: 14
                color: Style.yellow
                Layout.preferredWidth: 250
                Layout.alignment: Qt.AlignLeft
                Layout.leftMargin: 40
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
            }
        }

        RowLayout {
            id: notesRow
            objectName: "NotesRow"
            Layout.fillWidth: true
            height: 20
            spacing: 10

            Text {
                id: notesLabel
                objectName: "NotesLabel"
                text: qsTr("COMMENT")
                color: Style.white
                font.pointSize: 15
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.preferredWidth: 120
            }

            TextArea {
                id: notesTextArea
                objectName: "NotesEntry"
                property int maxNotesLength: 100
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.preferredWidth: 480
                implicitHeight: 100
                font.pointSize: 15
                color: Style.white
                wrapMode: TextEdit.WordWrap
                EnterKeyAction.actionId: EnterKeyAction.Done
                onFocusChanged: {
                    if (focus) {
                        mainWindow.closeNumericKeyboard();
                    }
                }

                background: Rectangle {
                    width: 480
                    height: parent.implicitHeight
                    color: Style.transparent
                    border.width: 1
                    border.color: Style.lightBlue
                }
                onTextChanged: {
                    if (length >= maxNotesLength) {
                        remove(maxNotesLength, length);
                    }
                }
                Keys.onReleased: {
                    if (event.key === Qt.Key_Return) {
                        saveFocus = true;
                        text = text.replace(/\n/g, '');
                    }
                }
                Connections {
                    target: mainWindow
                    onKeyboardDismissed: {
                        notesRow.state = "";
                    }
                    onKeyboardActive: {
                        if (notesTextArea.focus){
                            notesRow.state = "notesFocus";
                        }
                    }
                }
            }

            states: [
                State {
                    name: "notesFocus"
                    PropertyChanges {
                        target: columnLayout
                        anchors.bottomMargin: 300
                    }
                    PropertyChanges {
                        target: recordConfirmationPopup
                        state: "hideChart"
                    }
                }
            ]

        }
    }
    Row {
        id: positionButtonRow
        objectName: "PositionButtonRow"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: 30

        ButtonGroup {
            id: positionButtonGroup
            objectName: "PositionButtonGroup"
            buttons: positionButtonRow.children
            property string selectedPosition : "Horizontal"
        }

        RadioButton {
            id: zeroPosition
            objectName: "0PositionButton"
            checked: true
            indicator: Image {
                objectName: parent.objectName
                source: positionButtonGroup.selectedPosition === "Horizontal" ? "qrc:/images/0DegreeSelected" : "qrc:/images/0DegreeClear"
            }
            onClicked: {
                positionButtonGroup.selectedPosition = "Horizontal";
            }
        }
        RadioButton {
            id: thirtyPosition
            objectName: "30PositionButton"
            indicator: Image {
                objectName: parent.objectName
                source: positionButtonGroup.selectedPosition === "30_Degrees" ? "qrc:/images/30DegreeSelected" : "qrc:/images/30DegreeClear"
            }
            onClicked: {
                positionButtonGroup.selectedPosition = "30_Degrees";
            }
        }
        RadioButton {
            id: fortyfivePosition
            objectName: "45PositionButton"
            indicator: Image {
                objectName: parent.objectName
                source: positionButtonGroup.selectedPosition === "45_Degrees" ? "qrc:/images/45DegreeSelected" : "qrc:/images/45DegreeClear"
            }
            onClicked: {
                positionButtonGroup.selectedPosition = "45_Degrees";
            }
        }
        RadioButton {
            id: ninetyPosition
            objectName: "90PositionButton"
            indicator: Image {
                objectName: parent.objectName
                source: positionButtonGroup.selectedPosition === "90_Degrees" ? "qrc:/images/90DegreeSelected" : "qrc:/images/90DegreeClear"
            }
            onClicked: {
                positionButtonGroup.selectedPosition = "90_Degrees";
            }
        }
    }

    onRecordingChanged: {
        if (recording) {
            console.log("record confirm onRecordingChanged");
            timeText.text = recording.recordingTime;
            dateText.text = recording.recordingDate;
            sensorSystolicDiastolicText.systolicVal = recording.systolic;
            sensorSystolicDiastolicText.diastolicVal = recording.diastolic;
            sensorMeanText.meanValue = recording.mean;
            sensorHeartRate.heartRateVal = recording.heartRate;
            positionButtonGroup.selectedPosition = recording.position;
            if (!isNewRecording) {
                if (recording.referenceSystolic !== 0) {
                    refSystolicTextBox.setText(recording.referenceSystolic);
                }
                if (recording.referenceDiastolic !== 0) {
                    refDiastolicTextBox.setText(recording.referenceDiastolic);
                }
                if (recording.referenceMean !== 0) {
                    refMean.textField.setText(recording.referenceMean);
                }
                notesTextArea.text = recording.notes;
                positionButtonGroup.selectedPosition = recording.position;
            }
        }
    }

    onSave: {
        validationMessage.visible = false;
        if (validateEntries()) {
            recording.referenceSystolic = refSystolicTextBox.text
                    ? refSystolicTextBox.text : 0;
            recording.referenceDiastolic = refDiastolicTextBox.text
                    ? refDiastolicTextBox.text : 0;
            recording.referenceMean = refMean.textField.text ? refMean.textField.text : 0;
            recording.position = positionButtonGroup.selectedPosition;
            recording.notes = notesTextArea.text;
            saveRecording();
            setButtonsEnabled(false);
        }
    }

    onOpened: {
        refSystolicTextBox.focus = true;
    }

    function validateEntries() {
        var isValid = true;
        var systolic = refSystolicTextBox.text
                ? parseInt(refSystolicTextBox.text) : 0;
        var diastolic = refDiastolicTextBox.text
                ? parseInt(refDiastolicTextBox.text) : 0;
        var mean = refMean.textField.text ? parseInt(refMean.textField.text) : 0;
        console.log("validating entries: " + systolic + " / " + diastolic + " / " + mean);

        if (systolic === 0 && diastolic === 0 && mean === 0) {
            // values are optional, so if none have values, then valid
            return isValid;
        }

        // Ref Systolic must be > than Disatolic
        if (diastolic >= systolic) {
            validationMessage.text = qsTr("Diastolic should be less than systolic");
            refSysDiastolticGroup.invalidateSysDiastolic = true;
            validationMessage.visible = true;
            isValid = false;
        } else if (!(mean > diastolic && mean < systolic)) {
            // Ref mean must be between Systolic and Diastolic.
            validationMessage.text = qsTr("Mean must be between Systolic and Diastolic values");
            refMean.invalidate = true;
            validationMessage.visible = true;
            isValid = false;
        }
        return isValid;
    }

    onDiscard: {
        discardRecording();
        if (isNewRecording){
            setButtonsEnabled(false);
        }
    }

    onDismissed: {
        saveFocus = true;
        if (!isNewRecording) {
            close();
        } else {
            discardRecording();
            setButtonsEnabled(false);
        }
    }

    function setButtonsEnabled(enable) {
        saveEnabled = enable;
        discardEnabled = enable;
    }

    function reset() {
        // clear reference values.
        refSystolicTextBox.reset();
        refDiastolicTextBox.reset();
        refMean.reset();
        // clear notes
        notesTextArea.clear();
        recording = null;
        isNewRecording = false;
        saveEnabled = true;
        setButtonsEnabled(true);
    }
}
