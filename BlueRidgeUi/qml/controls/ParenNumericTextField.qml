import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Column {
    id: parenNumericTextField
    objectName:  "ParenNumericTextField"
    property int textSize: 50
    property alias labelSize: valueLabel.font.pointSize
    property alias value: valueTextField.text
    property alias valueWidth: valueTextField.width
    property alias label: valueLabel.text
    property alias showLabel: valueLabel.visible
    property alias textField: valueTextField
    property alias maxLength: valueTextField.maxLength
    property alias minValue: valueTextField.minValue
    property alias maxValue: valueTextField.maxValue
    property alias invalidate: valueTextField.invalidate
    readonly property alias invalid: valueTextField.invalid
    readonly property alias acceptable: valueTextField.acceptable

    signal inputEntered()

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        Text {
            id: rightParens
            objectName: parenNumericTextField.objectName + "_RightParen"
            font.pointSize: parenNumericTextField.textSize
            color: Style.lighterGray
            text: "("
        }

        Controls.NumericTextField {
            id: valueTextField
            objectName: parenNumericTextField.objectName + "_ValueTextField"
            font.pointSize: parenNumericTextField.textSize
            width: 90
            maxLength: 2
            minValue: 0
            maxValue: 99
            onInputEntered: {
                mainWindow.closeNumericKeyboard();
                parenNumericTextField.inputEntered()
            }
        }

        Text {
            id: leftParen
            objectName: parenNumericTextField.objectName + "_LeftParen"
            font.pointSize: parenNumericTextField.textSize
            color: Style.lighterGray
            text: ")"
        }
    }
    Text {
        id: valueLabel
        objectName: parenNumericTextField.objectName + "_ValueLabel"
        font.pointSize: 8
        color: invalid ? Style.yellow : Style.white
        anchors.horizontalCenter: parent.horizontalCenter
    }

    onFocusChanged: {
        if (focus) {
            valueTextField.focus = true
        }
    }

    function reset() { textField.reset() }
}
