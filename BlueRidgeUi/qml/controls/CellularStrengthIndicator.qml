import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0

RowLayout {
    id: cellularStrengthIndicator
    objectName: "CellularStrengthIndicator"
    spacing: 1
    implicitHeight: 30

    Item {
        id: cellularBarsSpace
        Layout.fillWidth: false
        Layout.preferredWidth: 100
        Layout.minimumWidth: 30
        Layout.maximumWidth: 30
        CellularStrengthBars {
            id: cellularStrengthBars
            objectName: "CellularStrengthBars"
            anchors.centerIn: parent
        }
    }
    Item {
        id: labelSpace
        Layout.fillWidth: false
        Layout.minimumWidth: 40
        Layout.maximumWidth: 100
        Layout.preferredWidth: cellularCarrierLabelMetrics.boundingRect.width
        TextMetrics {
            id: cellularCarrierLabelMetrics
            font.family: Style.openSans
            font.pointSize: 10
            elide: Text.ElideRight
            elideWidth: 100
            text: cellularStrengthController.carrier
        }
        Text {
            id: cellularCarrierLabel
            objectName: "CellularCarrierLabel"
            text: cellularCarrierLabelMetrics.elidedText
            color: Style.white
            font: cellularCarrierLabelMetrics.font
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
    Item {
        id: spacer
        Layout.fillWidth: true
        Layout.preferredWidth: 5
        Layout.minimumWidth: 5
        Layout.maximumWidth: 100
    }
}
