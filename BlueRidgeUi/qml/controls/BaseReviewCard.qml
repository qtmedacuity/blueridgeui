import QtQuick 2.12
import Style 1.0
import "../controls" as Controls

Rectangle {
    id: baseReviewCard
    objectName: "BaseReviewCard"
    width: 800
    height: 130
    color: Style.darkGray

    property string cardTitle: ""
    property string waveFile: ""
    property alias editButtonEnabled: editButton.enabled
    property alias deleteButtonEnabled: deleteButton.enabled

    signal editClicked();
    signal deleteClicked();

    Rectangle {
        width: 381
        height: 30
        color: Style.darkerGray2
        visible: cardTitle !== ""
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 2
        Text {
            objectName: "CardTitle"
            font.family: Style.brandon
            font.pointSize: 12
            color: Style.white
            text: cardTitle
            anchors.fill: parent
            anchors.leftMargin: 12
            verticalAlignment: Qt.AlignVCenter
        }
    }

    Rectangle {
        border.color: Style.blue
        border.width: 2
        color: Style.transparent
        width: 363
        anchors.right: editButton.left
        anchors.rightMargin: 10
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        Controls.StaticWaveChart {
            id: staticWaveChart
            objectName: "StaticWaveChart"
            anchors.fill: parent
            xAxisFontSize: 7
            dataFile: waveFile
            scrollType: 0
        }
    }

    Controls.IconButton {
        id: editButton
        objectName: "EditButton"
        anchors.right: parent.right
        anchors.rightMargin: 12
        anchors.top: parent.top
        anchors.topMargin: 12
        dynamicColors: true
        defaultColor: Style.blue
        width: 21
        height: 20
        iconImage: "qrc:/images/addnote"
        onClicked: {
            editClicked();
        }
    }

    Controls.IconButton {
        id: deleteButton
        objectName: "DeleteButton"
        anchors.right: parent.right
        anchors.rightMargin: 12
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 12
        dynamicColors: true
        defaultColor: Style.red
        width: 18
        height: 22
        iconImage: "qrc:/images/trashIcon"
        onClicked: {
            deleteClicked();
        }
    }
}
