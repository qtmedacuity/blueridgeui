import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0
import "../controls" as Controls

Item {
    id: pressureSearchSlider
    objectName: "PressureSearchSlider"
    property int status: 0
    property color statusColor: Style.red
    property int flashlightSize: 500
    property bool flashlightVisible: false

    Controls.PressureSearchSliderContract {
        id: sliderContract
        visible: true
        anchors.fill: parent
        barColor: statusColor
        flashlightSize: pressureSearchSlider.flashlightSize
        flashlightVisible: pressureSearchSlider.flashlightVisible
    }
    Controls.PressureSearchSliderExtended {
        id: sliderExtended
        visible: false
        anchors.fill: parent
        barColor: statusColor
        labelText: (99 >= status && 0 <= status) ? ("00" + status).slice(-2) : (100 == status ? "100" : "00")
        flashlightSize: pressureSearchSlider.flashlightSize
        flashlightVisible: pressureSearchSlider.flashlightVisible
    }
    states: State {
            name: "Expand"
            when: "00" !== sliderExtended.labelText
            PropertyChanges { target: sliderContract; visible: false }
            PropertyChanges { target: sliderExtended; visible: true }
    }

    StateGroup {
        id: signalStrengthStates
        state: pasignalStrengthController.status
        states: [
            State {
                name: "good"
                PropertyChanges {target: pressureSearchSlider; statusColor: Style.green}
            },
            State {
                name: "weak"
                PropertyChanges {target: pressureSearchSlider; statusColor: Style.orange}
            },
            State {
                name: "noPulse"
                PropertyChanges {target: pressureSearchSlider; statusColor: Style.lightGray}
            }
        ]
    }
}
