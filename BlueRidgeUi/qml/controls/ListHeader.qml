import QtQuick 2.12
import Style 1.0

Rectangle {
    property string headerText: ""

    color: Style.transparent

    Text {
        id: listHeader
        objectName: "ListHeader"
        text: headerText
        font.pointSize: 10
        font.bold: true
        color: Style.white
    }

}
