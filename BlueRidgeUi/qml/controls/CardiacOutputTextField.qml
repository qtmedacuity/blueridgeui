import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Item {

    property int fontSize: 55
    property alias inFocus: leftOfDecimalText.focus
    property real value

    signal valueEntered();

    width: 170
    height: 100

    Controls.NumericTextField {
        id: leftOfDecimalText
        objectName: "LeftOfDecimalEntry"

        width: 55
        font.pointSize: fontSize
        maxLength: 1
        minValue: 1
        maxValue: 9

        onTextChanged: {
            if (text !== "") {
                rightOfDecimalText.focus = true;
            }
        }

    }
    Text {
        id: decimalPointText
        objectName: "DecimalPoint"
        anchors.left: leftOfDecimalText.right
        anchors.bottom: leftOfDecimalText.bottom
        enabled: false
        text: Qt.locale().decimalPoint === "," ? "," : "."
        font.pointSize: 55
        color: Style.white
    }

    Controls.NumericTextField {
        id: rightOfDecimalText
        objectName: "RightOfDecimalEntry"
        anchors.left: decimalPointText.right
        anchors.bottom: decimalPointText.bottom
        width: 95
        font.pointSize: fontSize
        maxLength: 2
        minValue: 0
        maxValue: 99

        onInputEntered: {
            console.log("(co text field) input entered");
            determineValue();
            valueEntered();
        }
    }

    Rectangle {
        anchors.top: leftOfDecimalText.bottom
        anchors.topMargin: -13
        anchors.left: leftOfDecimalText.left
        height: 3
        color: (leftOfDecimalText.focus || rightOfDecimalText.focus) ? Style.lightBlue : Style.blue
        width: leftOfDecimalText.width + decimalPointText.width + rightOfDecimalText.width
    }

    function determineValue() {
        var stringValue = "";
        if (leftOfDecimalText.text !== "") {
            stringValue = leftOfDecimalText.text;
        }
        if (rightOfDecimalText.text !== "") {
            stringValue += "." + rightOfDecimalText.text;
        }
        if (stringValue !== "") {
            value = parseFloat(stringValue);
            console.log("co value is ", value);
        }
    }

    function reset() {
        console.log("(caridac output text field) reset");
        leftOfDecimalText.reset();
        rightOfDecimalText.reset();
        value = 0.0;
    }

    onValueChanged: {
        if (value && value !== 0.0) {
            console.log("value changed, ", value);
            var stringValue = value.toString();
            var numPieces = stringValue.split(".");
            leftOfDecimalText.text = numPieces[0];
            if (numPieces.length > 1) {
                rightOfDecimalText.text = numPieces[1];
            }
        }
    }
}
