import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Rectangle {
    id: patientListRow
    objectName: "PaitentListRow"

    property int rowWidth: 100
    property int rowHeight: 40
    property string rowColor: Style.transparent
    property string separatorColor: Style.black

    signal rowSelected(int index)
    signal itemChecked(int index, bool checked)

    width: rowWidth
    height: rowHeight
    color: rowColor

    Column {
        id: rowColumn
        objectName: "RowColumn_PaitentListRow"
        anchors.fill: parent
        Rectangle {
            id: lineSeparator
            objectName: "LineSeparator_RowColumn_PaitentListRow"
            color: separatorColor
            width: rowWidth
            height: 2
        }

        Row {
            id: rowContent
            objectName: "RowContent_RowColumn_PaitentListRow"

            Controls.SelectableListContent {
                contentText: fullName
                height: rowHeight
                width: uploadPending ? 265: 375
                onItemSelected: {
                    rowSelected(index);
                }
            }

            Controls.UnsentReadingLabel {
                width: uploadPending ? 110 : 0
                height: rowHeight
            }

            Controls.ListContent {
                contentText: dob
                height: rowHeight
                width: 125
                alignment: Text.AlignLeft
            }

            Controls.ListContent {
                contentText: patientId
                height: rowHeight
                width: 130
                alignment: Text.AlignRight
            }


            Controls.ListContent {
                contentText: serialNumber
                height: rowHeight
                width: 135
                alignment: Text.AlignHCenter
            }


            Controls.ListContent {
                contentText: implantDate
                height: rowHeight
                width: 140
                alignment: Text.AlignHCenter
            }

            Controls.ListCheckBox {
                width: 25
                height: rowHeight
                checkboxHeight: 21
                checkboxWidth: 21
                checked: selected
                onBoxChecked: {
                    console.log("row checkbox checked: ", checked);
                    selected = checked;
                    itemChecked(index, checked);
                }
            }
        }
    }
}
