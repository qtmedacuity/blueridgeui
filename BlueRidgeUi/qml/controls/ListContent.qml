import QtQuick 2.12
import Style 1.0

Text {
    id: listContent
    objectName: "Content"

    property string contentText: ""
    property alias alignment: listContent.horizontalAlignment

    text: contentText
    font.pointSize: 14
    color: Style.white
    verticalAlignment: Text.AlignVCenter
}
