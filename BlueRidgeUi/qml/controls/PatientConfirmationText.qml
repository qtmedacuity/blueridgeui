import QtQuick 2.12
import Style 1.0

Column {
    id: patientConfirmationText
    objectName: "PatientConfirmationText"

    property color textColor: Style.white
    property string titleText: ""
    property string infoText: ""

    height: 40

    Text {
        id: title
        objectName: "Title"
        text: titleText
        color: Style.lighterGray
        font.family: Style.brandon
        font.pixelSize: 12
    }
    Text {
        id: info
        objectName: "Info"
        text: infoText
        color: textColor
        font.pixelSize: 23
    }
}
