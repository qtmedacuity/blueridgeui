import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0

Rectangle {
    id: recordButton
    objectName: "RecordButton"

    signal clicked()

    color: Style.black
    border.color: Style.white
    border.width: 2
    radius: width*0.5

    Rectangle {
        id: innerButton
        objectName: "InnerButton"
        implicitHeight: recordButton.height-15
        implicitWidth: recordButton.width-15
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        color: Style.blue
        radius: width*0.5


        Image
        {
            id: checkIcon
            objectName: "checkIcon"
            source: "qrc:/images/checkmark"
            visible: false
            anchors.right: buttonText.left
            anchors.rightMargin: -5
            anchors.top: buttonText.top
            anchors.topMargin: 10
        }

        Text
        {
            id: buttonText
            objectName: "ButtonText"
            width: innerButton.width / 2
            text: qsTr("Record Pressures")
            color: Style.white
            font.pointSize: 18
            anchors.centerIn: innerButton
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap

        }

    }

    MouseArea
    {
        id: mouseArea
        objectName: "MouseArea"
        anchors.fill: parent
        onClicked: if(enabled) recordButton.clicked()
    }

    BusyIndicator {
        id: busyIndicator
        objectName: "BusyIndicator"
        running: false

        contentItem: Item {
            implicitWidth: recordButton.width-15
            implicitHeight: recordButton.height-15

            Item {
                id: item
                x: 0
                y: 0
                width: recordButton.width-15
                height: recordButton.height-15
                opacity: busyIndicator.running ? 1 : 0

                Behavior on opacity {
                    OpacityAnimator {
                        duration: 250
                    }
                }

                RotationAnimator {
                    target: item
                    running: busyIndicator.visible && busyIndicator.running
                    from: 0
                    to: 360
                    loops: Animation.Infinite
                    duration: 10000
                }

                Rectangle {
                    x: item.width / 2 - width / 2
                    y: item.height / 2 - height / 2
                    implicitWidth: 10
                    implicitHeight: 10
                    radius: 5
                    color: Style.white
                    transform: [
                        Translate {
                            y: -Math.min(item.width, item.height) * 0.5 + 5
                        },
                        Rotation {
                            origin.x: 5
                            origin.y: 5
                        }
                    ]
                }
            }
        }
    }
    states: [
        State {
            name: "waiting"
            PropertyChanges {
                target: innerButton
                color: Style.black
            }
            PropertyChanges {
                target: recordButton
                enabled: false
            }
            PropertyChanges {
                target: busyIndicator
                running: true
            }
            PropertyChanges {
                target: buttonText
                text: qsTr("Wait")
            }
        },
        State {
            name: "recording"
            PropertyChanges {
                target: innerButton
                color: Style.lightBlue
            }
            PropertyChanges {
                target: recordButton
                enabled: false
            }
            PropertyChanges {
                target: buttonText
                text: qsTr("Wait")
            }
        },

        State {
            name: "finished"
            PropertyChanges {
                target: innerButton
                color: Style.black
            }
            PropertyChanges {
                target: innerButton
                border.color: Style.lightBlue
            }
            PropertyChanges {
                target: innerButton
                border.width: 2
            }
            PropertyChanges {
                target: checkIcon
                visible: true
            }
        }
    ]
}

