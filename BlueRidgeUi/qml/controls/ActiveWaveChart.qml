import QtQuick 2.12
import QtCharts 2.3
import WaveChartWidget 1.0
import Style 1.0
import "../controls" as Controls

Controls.BaseWaveChart {
    id: waveChart
    controller: activeChartController

    property bool streamingEnabled: false

    LineSeries {
        id: seriesOne
        objectName: "SeriesOne"
        name: objectName
        color: Style.white
        width: 3
        capStyle: Qt.RoundCap
        pointsVisible : true
    }

    LineSeries {
        id: seriesTwo
        objectName: "SeriesTwo"
        name: objectName
        color: Style.white
        width: 3
        capStyle: Qt.RoundCap
        pointsVisible : true
    }

    Component.onCompleted: {
        waveChart.addSeries(seriesOne)
        waveChart.addSeries(seriesTwo)
        waveChart.controller.registerComponent(waveChart)
        waveChart.controller.registerSeries(seriesOne)
        waveChart.controller.registerSeries(seriesTwo)
        waveChart.controller.rangeInSeconds = 10.25
    }
    Component.onDestruction: {
        streamingEnabled = false
        waveChart.controller.unregisterSeries(seriesTwo)
        waveChart.controller.unregisterSeries(seriesOne)
        waveChart.controller.unregisterComponent(waveChart)
        waveChart.removeSeries(seriesTwo)
        waveChart.removeSeries(seriesOne)
    }

    onStreamingEnabledChanged:  {
        if (streamingEnabled) {
            controller.refresh()
            systemSettingsController.enableSensorDataStream()
        }
        else {
            systemSettingsController.disableSensorDataStream()
            controller.refresh()
        }
    }
}
