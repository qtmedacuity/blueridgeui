import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Column {
    id: systolicDiastolicTextField
    objectName: "SystolicDiastolicTextField"
    property int textSize: 50
    property int labelSize: 8
    property alias systolic: systolicTextField
    property alias diastolic: diastolicTextField
    property string systolicLabel: ""
    property string diastolicLabel: ""
    property int valueWidth: 90
    property int maxLength: 2
    property int minValue: 0
    property int maxValue: 99
    property bool showLabel: true
    readonly property bool hasFocus: systolic.focus || diastolic.focus
    readonly property bool hasEntries: isNumber(systolic.text) && isNumber(diastolic.text)
    readonly property bool invalid: !hasFocus && (hasEntries && !isGreaterThanOrEqualTo(systolic.text, diastolic.text) || systolic.invalid || diastolic.invalid)

    signal inputEntered()

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        Controls.NumericTextField {
            id: systolicTextField
            objectName: systolicDiastolicTextField.objectName + "_SystolicTextField"
            font.pointSize: systolicDiastolicTextField.textSize
            width: systolicDiastolicTextField.valueWidth
            maxLength: systolicDiastolicTextField.maxLength
            minValue: systolicDiastolicTextField.minValue
            maxValue: systolicDiastolicTextField.maxValue
            invalidate: !hasFocus && hasEntries && !isGreaterThanOrEqualTo(text, diastolicTextField.text)
            onInputEntered: {
                if (isGreaterThanOrEqualTo(systolic.text, diastolic.text))
                    systolicDiastolicTextField.inputEntered()
                else
                    diastolicTextField.focus = true
            }
        }

        Text {
            id: separator
            objectName: systolicDiastolicTextField.objectName + "_Separator"
            text: "/"
            font.pointSize: systolicDiastolicTextField.textSize
            color: Style.lighterGray
        }

        Controls.NumericTextField {
            id: diastolicTextField
            objectName: systolicDiastolicTextField.objectName + "_DiastolicTextField"
            font.pointSize: systolicDiastolicTextField.textSize
            width: systolicDiastolicTextField.valueWidth
            maxLength: systolicDiastolicTextField.maxLength
            minValue: systolicDiastolicTextField.minValue
            maxValue: systolicDiastolicTextField.maxValue
            invalidate: !hasFocus && hasEntries && !isGreaterThanOrEqualTo(systolicTextField.text, text)
            onInputEntered: {
                systolicDiastolicTextField.inputEntered()
            }
        }
    }
    Text {
        id: labelText
        objectName: systolicDiastolicTextField.objectName + "_Label"
        visible: true && systolicDiastolicTextField.showLabel
        text: systolicDiastolicTextField.systolicLabel + "/" + systolicDiastolicTextField.diastolicLabel
        font.pointSize: systolicDiastolicTextField.labelSize
        color: !systolicDiastolicTextField.hasFocus && systolicDiastolicTextField.invalid ? Style.yellow : Style.white
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Row {
        id: altLabels
        objectName: "AltLabels"
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        visible: false && systolicDiastolicTextField.showLabel
        Item {
            width: 25
            height: systolicLabelText.height
        }
        Text {
            id: systolicLabelText
            objectName: systolicDiastolicTextField.objectName + "_SystolicLabel"
            text: systolicDiastolicTextField.systolicLabel
            font.pointSize: systolicDiastolicTextField.labelSize
            color: !hasFocus && systolicTextField.invalid ? Style.yellow : Style.white
            width: systolicTextField.width
            horizontalAlignment: Text.AlignHCenter
        }
        Item {
            width: separator.width
            height: systolicLabelText.height
        }
        Text {
            id: diastolicLabelText
            objectName: systolicDiastolicTextField.objectName + "_DiastolicLabel"
            text: systolicDiastolicTextField.diastolicLabel
            font.pointSize: systolicDiastolicTextField.labelSize
            color: !hasFocus && diastolicTextField.invalid ? Style.yellow : Style.white
            width: diastolicTextField.width
            horizontalAlignment: Text.AlignHCenter
        }
    }

    onFocusChanged: {
        if (focus) {
            if (isNumber(systolic) && !isNumber(diastolic))
                diastolicTextField.focus = true
            else
                systolicTextField.focus = true
        }
    }

    states: [
        State {
            name: "AltLabels"
            PropertyChanges {
                target: labelText
                visible: false && systolicDiastolicTextField.showLabel
            }
            PropertyChanges {
                target: altLabels
                visible: true && systolicDiastolicTextField.showLabel
            }
        }
    ]

    function isNumber(value) {
        return ("" !== value && !isNaN(parseInt(value)))
    }

    function isGreaterThanOrEqualTo(upper, lower) {
        return isNumber(upper) && isNumber(lower) && (parseInt(upper) >= parseInt(lower))
    }

    function isInRange(upper, lower, middle) {
        return isGreaterThanOrEqualTo(upper, middle) && isGreaterThanOrEqualTo(middle, lower)
    }

    function reset() { systolicTextField.reset(); diastolicTextField.reset() }
}
