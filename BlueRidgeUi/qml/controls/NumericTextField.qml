import QtQuick 2.12
import Style 1.0
import "../controls" as Controls

Controls.UnderlineTextField {

    objectName: "NumericTextField"
    property int maxLength: 2
    property int minValue: 0
    property int maxValue: 99
    property string enteredValue: ""
    property bool invalidate: false
    readonly property bool acceptable: ("" === text) || focus || (!isNaN(parseInt(text)) && acceptableInput)

    signal inputEntered()

    function reset() {
        clear();
        focus = false;
        enteredValue = "";
    }

    function setText(value) {
        enteredValue = value;
        text = value;
    }

    invalid: invalidate || !acceptable
    isEditable: true
    inputMethodHints: Qt.ImhDigitsOnly
    maximumLength: maxLength
    validator: IntValidator {
        bottom: minValue
        top: maxValue
    }

    onFocusChanged: {
        if (focus) {
            mainWindow.showNumericKeyboard();
        }
    }

    Connections {
        target: mainWindow
        onNumericInputValue: {
            if (focus) {
                console.log("(numeric text) on input: ", value);
                if ((value+enteredValue).length <= maximumLength)
                {
                    enteredValue += value;
                    text = enteredValue;
                }
            }
        }
        onNumericInputDelete: {
            if (focus) {
                var length = enteredValue.length;
                if (length === 1) {
                    enteredValue = "";
                    clear();
                } else {
                    enteredValue = enteredValue.slice(0, length-1);
                    text = enteredValue;
                }
            }
        }

        onNumericInputEntered: {
            if (focus) {
                focus = false;
                inputEntered();
            }
        }
    }
}
