import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0
import "../controls" as Controls

Controls.PressureSearchSliderComponent {
    id: pressureSearchSliderExtended
    objectName: "PressureSearchSliderExtended"
    topCapImage: "qrc:/images/pressureSearchBar/tickLargeTop"
    barImage: "qrc:/images/pressureSearchBar/bar"
    bottomCapImage: "qrc:/images/pressureSearchBar/tickLargeBottom"
    property string labelText: "99"

    topCap.width: bar.width
    topCap.height: bar.width * (topCap.image.sourceSize.height / topCap.image.sourceSize.width)
    bottomCap.width: bar.width
    bottomCap.height: bar.width * (bottomCap.image.sourceSize.height / bottomCap.image.sourceSize.width)

    Controls.ColoredIcon {
        id: barTop
        iconColor: barColor
        iconImage: "qrc:/images/pressureSearchBar/tickSmallTop"
        width: topCap.width
        height: width * (image.sourceSize.height / image.sourceSize.width)
        anchors.right: topCap.right
        anchors.bottom: topCap.bottom
    }

    Text {
        id: centerLabel
        color: contentColor
        text: labelText
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        width: bar.width
        height: width
        anchors.verticalCenter: bar.verticalCenter
        anchors.left: bar.left
        anchors.right: bar.right
    }

    Controls.ColoredIcon {
        id: barBottom
        iconColor: barColor
        iconImage: "qrc:/images/pressureSearchBar/tickSmallBottom"
        width: bottomCap.width
        height: width * (image.sourceSize.height / image.sourceSize.width)
        anchors.right: bottomCap.right
        anchors.top: bottomCap.top
    }
}
