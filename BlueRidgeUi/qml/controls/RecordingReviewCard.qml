import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls

Controls.BaseReviewCard {

    objectName: "RecordingReviewCard"
    property int pressureTextSize: 20
    waveFile: waveFormFile

    Controls.ReviewDataValues {
        objectName: "RecordingReview_DataValues"
        anchors.left: parent.left
        anchors.leftMargin: 12
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        width: 300
        eventTime: recordingTime
        sys: systolic
        dia: diastolic
        meanVal: mean
        hRate: heartRate
        signalStrength: sensorStrength
        refSystolic: referenceSystolic
        refDiastolic: referenceDiastolic
        refMean: referenceMean
        showReferenceRow: !(referenceSystolic === 0 && referenceDiastolic === 0 && referenceMean === 0)
    }

}
