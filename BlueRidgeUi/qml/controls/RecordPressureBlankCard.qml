import QtQuick 2.12
import Style 1.0

Rectangle {

    property color fakeTextColor: Style.mediumGray3

    height: 160
    width: 240
    color: Style.darkGrayAlt
    opacity: 0.75
    radius: 4

    Rectangle {
        id: fakeBox1
        width: 23
        height: 23
        anchors.top: parent.top
        anchors.topMargin: 14
        anchors.left: parent.left
        anchors.leftMargin: 12
        color: fakeTextColor
        radius: 7
    }

    Rectangle {
        id: fakeBox2
        width: 80
        height: 14
        anchors.bottom: fakeBox1.bottom
        anchors.left: fakeBox1.right
        anchors.leftMargin: 16
        color: fakeTextColor
        radius: 7
    }

    Rectangle {
        id: fakeBox3
        width: 19
        height: 14
        anchors.bottom: fakeBox1.bottom
        anchors.left: fakeBox2.right
        anchors.leftMargin: 70
        color: fakeTextColor
        radius: 7
    }

    Rectangle {
        id: fakeBox4
        width: 79
        height: 23
        anchors.right: fakeBox5.left
        anchors.rightMargin: 32
        anchors.top: fakeBox5.top
        color: fakeTextColor
        radius: 7
    }

    Rectangle {
        id: fakeBox5
        width: 29
        height: 23
        anchors.right: parent.right
        anchors.rightMargin: 18
        anchors.top: fakeBox3.bottom
        anchors.topMargin: 20
        color: fakeTextColor
        radius: 7
    }

    Rectangle {
        id: fakeBox6
        width: 79
        height: 23
        anchors.left: fakeBox4.left
        anchors.top: fakeBox4.bottom
        anchors.topMargin: 16
        color: fakeTextColor
        radius: 7
    }

    Rectangle {
        id: fakeBox7
        width: 29
        height: 23
        anchors.left: fakeBox5.left
        anchors.top: fakeBox6.top
        color: fakeTextColor
        radius: 7
    }

}
