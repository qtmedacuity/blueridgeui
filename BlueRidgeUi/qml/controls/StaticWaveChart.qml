import QtQuick 2.12
import QtCharts 2.3
import WaveChartWidget 1.0
import Style 1.0
import "../controls" as Controls

Controls.BaseWaveChart {
    id: waveChart
    property string dataFile: ""

    LineSeries {
        id: waveSeries
        objectName: "WaveSeries"
        name: objectName
        color: Style.white
        width: 3
        capStyle: Qt.RoundCap
        pointsVisible : true
    }

    onDataFileChanged: {
        if (null != waveChart.controller) {
            waveChart.controller.unregisterSeries(waveSeries)
            waveChart.controller.unregisterComponent(waveChart)
        }

        waveChart.controller = staticChartManager.controller(dataFile)

        if (null != waveChart.controller) {
            waveChart.controller.registerComponent(waveChart)
            waveChart.controller.registerSeries(waveSeries)
        }
    }

    Component.onCompleted: { waveChart.addSeries(waveSeries) }
    Component.onDestruction: {
        if (null != waveChart.controller) {
            waveChart.controller.unregisterSeries(waveSeries)
            waveChart.controller.unregisterComponent(waveChart)
        }
        waveChart.removeSeries(waveSeries)
    }
}
