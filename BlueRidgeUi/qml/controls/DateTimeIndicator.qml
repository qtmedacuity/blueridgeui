import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0

RowLayout {
    id: dateTimeIndicator
    objectName: "DateTimeIndicator"

    property alias dateVisible: dateSpace.visible
    property alias timeVisible: timeSpace.visible

    spacing: 1
    implicitHeight: 30

    Item {
        id: dateSpace
        Layout.fillWidth: false
        Layout.minimumWidth: 40
        Layout.maximumWidth: 100
        Layout.preferredWidth: dateLabelMetrics.boundingRect.width
        TextMetrics {
            id: dateLabelMetrics
            font.family: Style.openSans
            font.pointSize: 10
            elide: Text.ElideRight
            elideWidth: 100
            text: dateTimeController.dateNow
        }
        Text {
            id: dateLabel
            objectName: "DateLabel"
            text: dateLabelMetrics.elidedText
            color: Style.white
            font: dateLabelMetrics.font
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
    Item {
        id: timeSpace
        Layout.fillWidth: false
        Layout.minimumWidth: 40
        Layout.maximumWidth: 100
        Layout.preferredWidth: timeLabelMetrics.boundingRect.width
        TextMetrics {
            id: timeLabelMetrics
            font.family: Style.openSans
            font.pointSize: 10
            elide: Text.ElideRight
            elideWidth: 100
            text: dateTimeController.timeNow
        }
        Text {
            id: timeLabel
            objectName: "TimeLabel"
            text: timeLabelMetrics.elidedText
            color: Style.white
            font: timeLabelMetrics.font
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
    Item {
        id: spacer
        Layout.fillWidth: true
        Layout.preferredWidth: 5
        Layout.minimumWidth: 5
        Layout.maximumWidth: 100
    }
}
