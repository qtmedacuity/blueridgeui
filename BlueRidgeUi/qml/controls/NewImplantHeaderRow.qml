import QtQuick 2.12
import Style 1.0
import "../controls" as Controls

Rectangle {

    property int rowWidth: 100
    property int rowHeight: 30
    property string rowColor: Style.blue
    property int selectedColumn : 0

    signal headerClicked(int column, bool ascending)

    id: newImplantHeaderRow
    width: rowWidth
    height: rowHeight
    color: rowColor

    Row {
        id: rowLayout
        anchors.fill: parent


        Controls.SortableListHeader {
            id: patientNameColumn
            objectName: "PatientNameColumn"
            width: 400
            height: rowHeight
            headerText: qsTr("NAME")
            state: "ascending"
            onSortChanged: {
                console.log("column 0, sort ascending: ", ascending)
                setSelectedColumn(0);
                headerClicked(0, ascending);
            }
        }

        Controls.SortableListHeader {
            id: dobColumn
            objectName: "DOBColumn"
            width: 130
            height: rowHeight
            headerText: qsTr("D.O.B")
            alignment: "left"
            onSortChanged: {
                console.log("column 1, sort ascending: ", ascending)
                setSelectedColumn(1);
                headerClicked(1, ascending);
            }
        }

        Controls.SortableListHeader {
            id: patientIdColumn
            objectName: "PatientIdColumn"
            width: 145
            height: rowHeight
            headerText: qsTr("MRN/PATIENT ID")
            alignment: "right"
            onSortChanged: {
                console.log("column 2, sort ascending: ", ascending)
                setSelectedColumn(2);
                headerClicked(2, ascending);
            }
        }


    }

    function setSelectedColumn(column) {
        if (selectedColumn !== column) {
            //clear selected column
            if (selectedColumn === 0) {
                patientNameColumn.state = "default";
            }
            if (selectedColumn === 1) {
                dobColumn.state = "default";
            }
            if (selectedColumn === 2) {
                patientIdColumn.state = "default";
            }
            selectedColumn = column;
        }
    }

}

