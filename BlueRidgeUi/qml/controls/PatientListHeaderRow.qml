import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Rectangle {

    property int rowWidth: 100
    property int rowHeight: 30
    property string rowColor: Style.blue
    property int selectedColumn : 0
    property bool checkBoxChecked: false

    signal headerClicked(int column, bool ascending)
    signal selectAllClicked(bool checked)

    function unselectCheckBox() { headerCheckbox.checked = false; }

    id: patientListHeaderRow
    objectName: "PatientListHeaderRow"
    width: rowWidth
    height: rowHeight
    color: rowColor

    Row {
        id: rowLayout
        objectName: "RowLayout_PatientListHeaderRow"
        anchors.fill: parent
        Controls.SortableListHeader {
            id: patientNameColumn
            objectName: "PatientNameColumn"
            width: 375
            height: rowHeight
            headerText: qsTr("NAME")
            state: "ascending"
            onSortChanged: {
                console.log("column 0, sort ascending: ", ascending)
                setSelectedColumn(0);
                headerClicked(0, ascending);
            }
        }

        Controls.SortableListHeader {
            id: dobColumn
            objectName: "DOBColumn"
            width: 125
            height: rowHeight
            headerText: qsTr("D.O.B")
            onSortChanged: {
                console.log("column 1, sort ascending: ", ascending)
                setSelectedColumn(1);
                headerClicked(1, ascending);
            }
        }

        Controls.SortableListHeader {
            id: patientIdColumn
            objectName: "PatientIdColumn"
            height: rowHeight
            width: 130
            headerText: qsTr("MRN/PATIENT ID")
            alignment: "right"
            onSortChanged: {
                console.log("column 2, sort ascending: ", ascending)
                setSelectedColumn(2);
                headerClicked(2, ascending);
            }
        }

        Controls.SortableListHeader {
            id: sensorNumColumn
            objectName: "SensorNumColumn"
            height: rowHeight
            width: 135
            headerText: qsTr("SENSOR SERIAL #")
            alignment: "right"
            onSortChanged: {
                console.log("column 3, sort ascending: ", ascending)
                setSelectedColumn(3);
                headerClicked(3, ascending);
            }
        }

        Controls.SortableListHeader {
            id: implantDateColumn
            objectName: "ImplantDateColumn"
            width: 140
            height: rowHeight
            headerText: qsTr("IMPLANT DATE")
            alignment: "center"
            onSortChanged: {
                console.log("column 4, sort ascending: ", ascending)
                setSelectedColumn(4);
                headerClicked(4, ascending);
            }
        }

        Controls.ListCheckBox {
            id: headerCheckbox
            width: 25
            height: rowHeight
            onBoxChecked: {
                console.log("header checkbox checked: ", checked);
                checkBoxChecked = checked;
                selectAllClicked(checked);
            }
        }

    }
    function setSelectedColumn(column) {
        if (selectedColumn !== column) {
            //clear selected column
            if (selectedColumn === 0) {
                patientNameColumn.state = "default";
            }
            if (selectedColumn === 1) {
                dobColumn.state = "default";
            }
            if (selectedColumn === 2) {
                patientIdColumn.state = "default";
            }
            if (selectedColumn === 3) {
                sensorNumColumn.state = "default";
            }
            if (selectedColumn === 4) {
                implantDateColumn.state = "default";
            }
            selectedColumn = column;
        }
    }

}

