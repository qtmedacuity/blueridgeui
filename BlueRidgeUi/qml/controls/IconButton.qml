import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0
import "../controls" as Controls

Controls.ColoredIcon {
    id: iconButton
    objectName: "IconButton"

    property bool dynamicColors: true
    property color defaultColor: Style.white
    property color pressedColor: Style.lightBlue
    property color disableColor: Style.gray
    property alias enabled: mouseArea.enabled

    signal clicked()

    iconColor: mouseArea.enabled ? mouseArea.pressed ? pressedColor : defaultColor : disableColor
    colorOverlay.visible: dynamicColors

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: { iconButton.clicked() }
    }
}
