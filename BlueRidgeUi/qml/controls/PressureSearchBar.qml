import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0
import "../controls" as Controls

Item {
    id: pressureSearchBar
    objectName: "PressureSearchBar"
    property color backgroundColor: Style.mediumGray
    property alias statusColor: slider.statusColor
    property double heightFactor: 1.0
    readonly property int sliderValue: ((flicker.contentY / (flicker.height - slider.height)) * 100)
    property int flashlightSize: 500
    property alias mean: slider.status
    clip: false
    Controls.ColoredIcon {
        id: background
        objectName: "Background_PressureSearchBar"
        iconColor: pressureSearchBar.backgroundColor
        iconImage: "qrc:/images/pressureSearchBar/bar"
        image.fillMode: Image.Stretch
        anchors.bottom: pressureSearchBar.bottom
        anchors.right: pressureSearchBar.right
        width: pressureSearchBar.width * 0.5
        height: pressureSearchBar.height
    }
    Flickable {
        id: flicker
        objectName: "Flicker_PressureSearchBar"
        anchors.bottom: pressureSearchBar.bottom
        anchors.right: pressureSearchBar.right
        width: pressureSearchBar.width * 0.5
        height: pressureSearchBar.height
        contentWidth: width
        contentHeight: (pressureSearchBar.height * 2) - slider.height
        flickableDirection: Flickable.VerticalFlick
        boundsMovement: Flickable.StopAtBounds
        boundsBehavior: Flickable.StopAtBounds
        interactive: searchbarController.userEnabled && !searchbarController.autoSearching
        clip: false

        Controls.PressureSearchSlider {
            id: slider
            objectName: "Slider_Flicker_PressureSearchBar"
            width: flicker.width
            height: flicker.height * 0.2 * pressureSearchBar.heightFactor
            x: 0
            y: flicker.height - height
            flashlightSize: pressureSearchBar.flashlightSize
            flashlightVisible: searchbarController.autoSearching
        }

        onMovementEnded: {
            if (interactive)
                searchbarController.searchValue = pressureSearchBar.sliderValue
        }
    }

    states: State {
            name: "Expand"
            when: slider.state === "Expand"
            PropertyChanges { target: flicker; width: pressureSearchBar.width }
    }
    transitions: Transition {
        to: "Expand"
        reversible: true
        PropertyAnimation { target: flicker; property: "width"; duration: 250 }
    }

    Connections {
        target: searchbarController
        onSearchValueChanged: {
            flicker.contentY = (searchbarController.searchValue / 100.0) * (flicker.height - slider.height)
        }
    }
}
