import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.VirtualKeyboard 2.4
import Style 1.0
import RhcValues 1.0
import "../controls" as Controls
import "../views" as Views

Controls.StaticWaveOverlay {
    id: rhcEntryOverlay
    objectName: "RhcEntryOverlay"

    state: "hideChartAndStrength"

    saveEnabled: validateEntries()

    RhcValues {
        id: rhc
    }

    property int maxLength: 2
    property int minValue: 1
    property int maxValue: 99

    Text {
        id: rhcEntryLabel
        objectName: "RHCEntryLabel"
        anchors.left: parent.left
        anchors.leftMargin: 60
        anchors.top: parent.top
        anchors.topMargin: 125
        text: qsTr("Enter RHC Values")
        color: Style.white
        font.family: Style.brandon
        font.pointSize: 20
    }
    GridLayout {
        id: grid
        columns: 3
        rows: 4
        columnSpacing: 0
        anchors.left: rhcEntryLabel.right
        anchors.leftMargin: 40
        anchors.top: rhcEntryLabel.bottom
        anchors.topMargin: 20
        width: 400
        height: 300
        property int meanTextFieldWidth: 55
        property int sysdiaTextFieldWidth: 55
        property int textSize: 25
        property int labelTextSize: 12
        property int labelOffset: 15

        Item { id: raLabelSpace; objectName: "RALabelSpace"; height: grid.height / grid.rows; width: grid.width * 0.25;
            Text {
                id: raLabel
                objectName: "RALabel"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: grid.labelOffset
                text: qsTr("RA")
                color: Style.white
                font.family: Style.brandon
                font.pointSize: grid.labelTextSize
                width: pcwpLabel.width
            }
        }
        Item { id: raSpace; objectName: "RASpace"; height: grid.height / grid.rows; width: grid.width * 0.5 }
        Item { id: raMeanSpace; objectName: "RAMeanSpace"; height: grid.height / grid.rows; width: grid.width * 0.25;
            Controls.ParenNumericTextField {
                id: raMeanTextField
                objectName: "RAMean"
                anchors.fill: parent
                showLabel: true
                maxLength: rhcEntryOverlay.maxLength
                minValue: rhcEntryOverlay.minValue
                maxValue: rhcEntryOverlay.maxValue
                textSize: grid.textSize
                label: qsTr("mean")
                valueWidth: grid.meanTextFieldWidth
                onInputEntered: {
                    if (!rvSysDiaTextField.hasEntries || rvSysDiaTextField.invalid) {
                        rvSysDiaTextField.focus = true;
                    } else if (!paSysDiaTextField.hasEntries || paSysDiaTextField.invalid) {
                        paSysDiaTextField.focus = true;
                    } else if (!isNumber(paMeanTextField.value) || paMeanTextField.invalid) {
                        paMeanTextField.textField.focus = true;
                    } else if (!isNumber(pcwpMeanTextField.value) || pcwpMeanTextField.invalid) {
                        pcwpMeanTextField.textField.focus = true;
                    }
                }
            }
        }

        Item { id: rvLabelSpace; objectName: "RVLabelSpace"; height: grid.height / grid.rows; width: grid.width * 0.25;
            Text {
                id: rvLabel
                objectName: "RVLabel"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: grid.labelOffset
                text: qsTr("RV")
                color: Style.white
                font.family: Style.brandon
                font.pointSize: grid.labelTextSize
                width: pcwpLabel.width
            }
        }
        Item { id: rvSysDiaSpace; objectName: "RVSysDiaSpace"; height: grid.height / grid.rows; width: grid.width * 0.5
            Controls.SystolicDiastolicTextField {
                id: rvSysDiaTextField
                objectName: "RVSysDia"
                anchors.fill: parent
                showLabel: true
                maxLength: rhcEntryOverlay.maxLength
                minValue: rhcEntryOverlay.minValue
                maxValue: rhcEntryOverlay.maxValue
                textSize: grid.textSize
                systolicLabel: qsTr("sys")
                diastolicLabel: qsTr("dia")
                valueWidth: grid.sysdiaTextFieldWidth
                state: "AltLabels"
                onInputEntered: {
                    if (!paSysDiaTextField.hasEntries || paSysDiaTextField.invalid) {
                        paSysDiaTextField.focus = true;
                    } else if (!isNumber(paMeanTextField.value) || paMeanTextField.invalid) {
                        paMeanTextField.textField.focus = true;
                    } else if (!isNumber(pcwpMeanTextField.value) || pcwpMeanTextField.invalid) {
                        pcwpMeanTextField.textField.focus = true;
                    }
                }
            }
        }
        Item { id: rvSpace; objectName: "RVSpace"; height: grid.height / grid.rows; width: grid.width * 0.25 }

        Item { id: paLabelSpace; objectName: "PALabelSpace"; height: grid.height / grid.rows; width: grid.width * 0.25;
            Text {
                id: paLabel
                objectName: "PALabel"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: grid.labelOffset
                text: qsTr("PA")
                color: Style.white
                font.family: Style.brandon
                font.pointSize: grid.labelTextSize
                width: pcwpLabel.width
            }
        }
        Item { id: paSysDiaSpace; objectName: "PASysDiaSpace"; height: grid.height / grid.rows; width: grid.width * 0.5
            Controls.SystolicDiastolicTextField {
                id: paSysDiaTextField
                objectName: "PASysDia"
                anchors.fill: parent
                showLabel: true
                maxLength: rhcEntryOverlay.maxLength
                minValue: rhcEntryOverlay.minValue
                maxValue: rhcEntryOverlay.maxValue
                textSize: grid.textSize
                systolicLabel: qsTr("sys")
                diastolicLabel: qsTr("dia")
                valueWidth: grid.sysdiaTextFieldWidth
                state: "AltLabels"
                onInputEntered: {
                    if (!isNumber(paMeanTextField.value) || paMeanTextField.invalid) {
                        paMeanTextField.textField.focus = true;
                    } else if (!isNumber(pcwpMeanTextField.value) || pcwpMeanTextField.invalid) {
                        pcwpMeanTextField.textField.focus = true;
                    }
                }
            }
        }
        Item { id: paMeanSpace; objectName: "PAMeanSpace"; height: grid.height / grid.rows; width: grid.width * 0.25;
            Controls.ParenNumericTextField {
                id: paMeanTextField
                objectName: "PAMean"
                anchors.fill: parent
                showLabel: true
                maxLength: rhcEntryOverlay.maxLength
                minValue: rhcEntryOverlay.minValue
                maxValue: rhcEntryOverlay.maxValue
                textSize: grid.textSize
                label: qsTr("mean")
                valueWidth: grid.meanTextFieldWidth
                invalidate: paSysDiaTextField.hasEntries && !paSysDiaTextField.hasFocus && !paSysDiaTextField.invalid && isNumber(value) && !textField.focus && !isInRange(paSysDiaTextField.systolic.text, paSysDiaTextField.diastolic.text, value)
                onInputEntered: {
                    if (!isNumber(pcwpMeanTextField.value) || pcwpMeanTextField.invalid) {
                        pcwpMeanTextField.textField.focus = true;
                    }
                }
            }
        }

        Item { id: pcwpLabelSpace; objectName: "PCWPLabelSpace"; height: grid.height / grid.rows; width: grid.width * 0.25;
            Text {
                id: pcwpLabel
                objectName: "PCWPLabel"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: grid.labelOffset
                text: qsTr("PCWP")
                color: Style.white
                font.family: Style.brandon
                font.pointSize: grid.labelTextSize
            }
        }
        Item { id: pcwpSpace; objectName: "PCWPSpace"; height: grid.height / grid.rows; width: grid.width * 0.5 }
        Item { id: pcwpMeanSpace; objectName: "PCWPMeanSpace"; height: grid.height / grid.rows; width: grid.width * 0.25;
            Controls.ParenNumericTextField {
                id: pcwpMeanTextField
                objectName: "PCWPMean"
                anchors.fill: parent
                showLabel: true
                maxLength: rhcEntryOverlay.maxLength
                minValue: rhcEntryOverlay.minValue
                maxValue: rhcEntryOverlay.maxValue
                textSize: grid.textSize
                label: qsTr("mean")
                valueWidth: grid.meanTextFieldWidth
                invalidate: okToTest() && !lessThanPADiastolic()
                onInputEntered: {
                }

                function okToTest() {
                    return paSysDiaTextField.hasEntries &&
                            !paSysDiaTextField.hasFocus &&
                            !paSysDiaTextField.invalid &&
                            !textField.focus &&
                            isNumber(value)
                }

                function lessThanPADiastolic() {
                    return okToTest() && isLessThanOrEqualTo(value, paSysDiaTextField.diastolic.text)
                }

                function isAcceptableRange() {
                    if (!okToTest())
                        return false;
                    var pcwp = parseInt(pcwpMeanTextField.value)
                    var pad = parseInt(paSysDiaTextField.diastolic.text)
                    return (pad + 2 >= pcwp) && (pcwp >= pad - 5)
                }
            }
        }
    }

    Text {
        id: rhcErrorMessage
        objectName: "RHCErrorMessage"
        anchors.left: parent.left
        anchors.leftMargin: 60
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 125
        text: qsTr("")
        color: Style.yellow
        font.family: Style.brandon
        font.pointSize: 16
        visible: false

        state: errorState()

        states: [
            State {
                name: "PressureOutOfRange"
                PropertyChanges {
                    target: rhcErrorMessage
                    text: qsTr("Reference pressure is out of range.")
                    visible: true
                }
            },
            State {
                name: "PressureIllogical"
                PropertyChanges {
                    target: rhcErrorMessage
                    text: qsTr("Reference pressures are illogical.")
                    visible: true
                }
            },
            State {
                name: "PCWPGreaterThanPAD"
                PropertyChanges {
                    target: rhcErrorMessage
                    text: qsTr("PCWP should be less than or equal to the PAD.")
                    visible: true
                }
            },
            State {
                name: "DiastolicPulmonaryGrdient"
                PropertyChanges {
                    target: rhcErrorMessage
                    text: qsTr("Diastolic pulmonary gradient is > 5 mmHg.")
                    visible: true
                }
            }
        ]

    }

    onOpened: { raMeanTextField.textField.focus = true }
    onSave: {
        rhc.raValue = parseInt(raMeanTextField.value)
        rhc.rvSystolic = parseInt(rvSysDiaTextField.systolic.text)
        rhc.rvDiastolic = parseInt(rvSysDiaTextField.diastolic.text)
        rhc.paSystolic = parseInt(paSysDiaTextField.systolic.text)
        rhc.paDiastolic = parseInt(paSysDiaTextField.diastolic.text)
        rhc.paMean = parseInt(paMeanTextField.value)
        rhc.pcwpValue = parseInt(pcwpMeanTextField.value)
        recordingController.saveRhcValues(rhc)
    }
    onDiscard: {
        recordingController.deleteRecording(recording.recordingId);
    }
    onDismissed: {
        recordingController.deleteRecording(recording.recordingId);
    }

    function isNumber(value) {
        return ("" !== value && !isNaN(parseInt(value)))
    }

    function isGreaterThanOrEqualTo(upper, lower) {
        return isNumber(upper) && isNumber(lower) && (parseInt(upper) >= parseInt(lower))
    }

    function isLessThanOrEqualTo(lower, upper) {
        return isNumber(upper) && isNumber(lower) && (parseInt(lower) <= parseInt(upper))
    }

    function isInRange(upper, lower, middle) {
        return isGreaterThanOrEqualTo(upper, middle) && isGreaterThanOrEqualTo(middle, lower)
    }

    function validateEntries() {
        return !(raMeanTextField.invalid || paMeanTextField.invalid ||
                 rvSysDiaTextField.systolic.invalid || rvSysDiaTextField.diastolic.invalid ||
                 paSysDiaTextField.systolic.invalid || paSysDiaTextField.diastolic.invalid) &&
                (!pcwpMeanTextField.invalid || (pcwpMeanTextField.invalidate && pcwpMeanTextField.isAcceptableRange())) &&
                isNumber(raMeanTextField.value) &&
                isGreaterThanOrEqualTo(rvSysDiaTextField.systolic.text, rvSysDiaTextField.diastolic.text) &&
                isInRange(paSysDiaTextField.systolic.text, paSysDiaTextField.diastolic.text, paMeanTextField.value) &&
                isNumber(pcwpMeanTextField.value)
    }

    function errorState() {

        if (!raMeanTextField.acceptable) {
            return "PressureOutOfRange"
        }

        if (!(rvSysDiaTextField.systolic.acceptable && rvSysDiaTextField.diastolic.acceptable)) {
            return "PressureOutOfRange"
        }
        if (rvSysDiaTextField.hasEntries && !rvSysDiaTextField.hasFocus && rvSysDiaTextField.invalid) {
            return "PressureIllogical"
        }

        if (!(paSysDiaTextField.systolic.acceptable && paSysDiaTextField.diastolic.acceptable && paMeanTextField.acceptable)) {
            return "PressureOutOfRange"
        }
        if (paSysDiaTextField.hasEntries && !paSysDiaTextField.hasFocus && paSysDiaTextField.invalid) {
            return "PressureIllogical"
        }
        if (paSysDiaTextField.hasEntries && !paSysDiaTextField.hasFocus && isNumber(paMeanTextField.value) && !paMeanTextField.textField.focus && !isInRange(paSysDiaTextField.systolic.text, paSysDiaTextField.diastolic.text, paMeanTextField.value)) {
            return "PressureIllogical"
        }

        if (!pcwpMeanTextField.acceptable) {
            return "PressureOutOfRange"
        }
        if (pcwpMeanTextField.invalid && isNumber(pcwpMeanTextField.value) && !pcwpMeanTextField.textField.focus && !pcwpMeanTextField.lessThanPADiastolic()) {
            return "PCWPGreaterThanPAD"
        }
        if (!pcwpMeanTextField.invalid && isNumber(pcwpMeanTextField.value) && !pcwpMeanTextField.textField.focus && !pcwpMeanTextField.isAcceptableRange()) {
            return "DiastolicPulmonaryGrdient"
        }

        return ""
    }

    function reset() {
        raMeanTextField.reset();
        paMeanTextField.reset();
        pcwpMeanTextField.reset();
        rvSysDiaTextField.reset();
        paSysDiaTextField.reset();
    }

    Connections {
        target: recordingController
        onSetRhcValuesComplete: {
            console.log("RhcEntryOverlay - save complete")
            close()
        }
        onRecordingDeleteComplete : {
            console.log("RhcEntryOverlay - discarded")
            close();
        }
    }
}
