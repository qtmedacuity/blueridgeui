import QtQuick 2.12
import Style 1.0
import "../controls" as Controls

Item {
    anchors.fill: parent

    Text {
        id: instructionTitle
        objectName: "InstructionTitle"
        text: qsTr("Calibrate Sensor")
        color: Style.white
        font.pointSize: 20
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 30
        anchors.leftMargin: 20
    }

    Text {
        id: instructionText
        objectName: "InstructionText"
        text: qsTr("Freeze CardioMems and PA catheter waveforms when both are valid.")
        color: Style.white
        font.pointSize: 14
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: instructionTitle.bottom
        anchors.topMargin: 20
        wrapMode: Text.WordWrap
    }


    Controls.Button {
        id: calibrateButton
        objectName: "CalibrateButton"
        height: 45
        width: 240
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20
        buttonText: qsTr("Freeze to Calibrate")
        buttonTextSize: 14
        buttonColor: Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        enabled: false
        onClicked: {
            implantRoot.freezeToCalibrate();
            enabled = false;
        }
    }

    Controls.Button {
        id: cancelButton
        objectName: "CancelButton"
        height: 45
        width: 110
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        buttonText: qsTr("Cancel")
        buttonTextSize: 14
        buttonColor: Style.transparent
        buttonPressedColor: Style.blue
        borderColor: Style.blue
        onClicked: {
            implantRoot.previousStep();
        }
    }

    Component.onCompleted: {
        recordingController.getRecordingStatus("Start");
    }

    Connections {
        target: recordingController
        onReadyToRecord: {
            console.log("ready to record");
            calibrateButton.enabled = true;
            recordingController.getRecordingStatus("Stop");
        }
    }

    Connections {
        target: mainWindow
        onPaMeanOverlayDismissed: {
            recordingController.getRecordingStatus("Start");
        }
    }

}
