import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls

Column {

    id: systolicDiastolicText
    objectName: "SystolicDiastolicText"
    property int textSize: 65
    property int labelTextSize: 12
    property alias systolicVal: systolicText.text
    property alias diastolicVal: diastolicText.text
    property bool showLabel: true
    property bool showAltLabel: false

    spacing: 0
    Row {
        anchors.horizontalCenter: parent.horizontalCenter

        ColumnLayout {
            spacing: 0
            Text {
                id: systolicText
                objectName: systolicDiastolicText.objectName + "_Systolic"
                font.pointSize: textSize
                color: Style.white
            }
            Text {
                id: sysLabel
                objectName: systolicDiastolicText.objectName + "_SysLabel"
                text: qsTr("sys")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                color: Style.lighterGray
                font.pointSize: labelTextSize
                visible: showAltLabel
            }
        }

        Text {
            id: separator
            objectName: systolicDiastolicText.objectName + "_Separator"
            text: "/"
            font.pointSize: textSize
            color: Style.lighterGray
        }

        ColumnLayout {
            spacing: 0
            Text {
                id: diastolicText
                objectName: systolicDiastolicText.objectName + "_Diastolic"
                font.pointSize: textSize
                color: Style.white
            }
            Text {
                id: diaLabel
                objectName: systolicDiastolicText.objectName + "_DiaLabel"
                text: qsTr("dia")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                color: Style.lighterGray
                font.pointSize: labelTextSize
                visible: showAltLabel
            }
        }
    }

    Text {
        id: systolicDiastolicLabel
        objectName: systolicDiastolicText.objectName + "_Label"
        text: qsTr("Systolic/Diastolic")
        font.pointSize: labelTextSize
        color: Style.white
        anchors.horizontalCenter: parent.horizontalCenter
        visible: showLabel && !showAltLabel
    }

}

