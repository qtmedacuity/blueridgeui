import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls
import "../views" as Views

Row {
    anchors.fill: parent

    property color selectedButtonColor: Style.lightBlue
    property color unselectedButtonColor: Style.lightGray

    Text {
        id: instructionText
        objectName: "InstructionText"
        text: qsTr("Select Implanted Sensor Position:")
        color: Style.white
        font.pointSize: 20
        width: parent.width * 0.6
        leftPadding: 20
        topPadding: 20
    }

    Rectangle {
        width: parent.width * 0.4
        height: parent.height
        color: Style.transparent

        Image {
            id: bodyImg
            objectName: "BodyImg"
            source: "qrc:/images/body"
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.rightMargin: 55
            width: sourceSize.width
            height: sourceSize.height
            fillMode: Image.PreserveAspectFit
        }
        Row {
            anchors.left: bodyImg.left
            anchors.leftMargin: 105
            anchors.bottom: bodyImg.bottom
            anchors.bottomMargin: 30
            spacing: 18

            Image {
                id: rightButton
                objectName: "RightButton"
                source: "qrc:/images/rightPosition"
                fillMode: Image.PreserveAspectFit

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: {
                        rightButton.source = "qrc:/images/rightPositionSelected"
                        leftButton.source = "qrc:/images/leftPosition"
                    }

                    onReleased: positionSelected("right");

                    onClicked:
                    {
                        rightButton.source = "qrc:/images/rightPositionSelected"
                        leftButton.source = "qrc:/images/leftPosition"
                        positionSelected("right");
                    }
                }
            }

            Image {
                id: leftButton
                objectName: "LeftButton"
                source: "qrc:/images/leftPosition"
                fillMode: Image.PreserveAspectFit

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: {
                        rightButton.source = "qrc:/images/rightPosition"
                        leftButton.source = "qrc:/images/leftPositionSelected"
                    }

                    onReleased: positionSelected("left");

                    onClicked:
                    {
                        rightButton.source = "qrc:/images/rightPosition"
                        leftButton.source = "qrc:/images/leftPositionSelected"
                        positionSelected("left");
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        if (implantRoot.sensorPosition !== "") {
            if (implantRoot.sensorPosition === "left") {
               leftButton.source = "qrc:/images/leftPositionSelected"
            } else {
                rightButton.source = "qrc:/images/rightPositionSelected"
            }
        }
    }

    function positionSelected(position) {
        implantRoot.sensorPositionSelected(position);
        implantRoot.nextStep();
    }
}

