import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Column {
    id: meanText
    objectName: "MeanText"
    property int textSize: 65
    property int labelTextSize: 12
    property alias meanValue: mean.text
    property color parenthesisColor: Style.lighterGray
    property bool showLabel: true
    property bool showAltLabel: false

    spacing: 0
    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        Text {
            id: rightParen
            objectName: meanText.objectName + "_RightParen"
            font.pointSize: textSize
            color: parenthesisColor
            text: "("
        }

        Text {
            id: mean
            objectName: meanText.objectName + "_Mean"
            font.pointSize: textSize
            color: Style.white
        }

        Text {
            id: leftParen
            objectName: meanText.objectName + "_LeftParen"
            font.pointSize: textSize
            color: parenthesisColor
            text: ")"
        }
    }

    Text {
        id: meanLabel
        objectName: meanText.objectName + "_Label"
        text: showAltLabel ? qsTr("mean") : qsTr("Mean")
        font.pointSize: labelTextSize
        color: showAltLabel ? Style.lighterGray : Style.white
        anchors.horizontalCenter: parent.horizontalCenter
        visible: showLabel || showAltLabel
    }

}

