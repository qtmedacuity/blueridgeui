import QtQuick 2.12
import Style 1.0

Rectangle {
    id: sortableHeaderCell

    property alias headerText: headerText.text
    property string alignment: "left"

    state: "default"

    signal sortChanged(bool ascending)

    color: Style.transparent
    MouseArea {
        id: sortbutton
        objectName: "SortButton"
        width: parent.width
        height: parent.height
        property bool ascending: true

        Row {
            id: rowItem
            anchors.verticalCenter: parent.verticalCenter

            Column {
                width: 15
                height: 15
                spacing: 2

                Canvas
                {
                    id: sortUpIndicator
                    objectName: "SortUpIndicator"
                    property color sortUpColor : Style.transparent
                    width: 13
                    height: 6
                    contextType: "2d"

                    onPaint: {
                        context.reset();
                        context.moveTo(0, height);
                        context.lineTo(width, height);
                        context.lineTo(width / 2, 0);
                        context.closePath();
                        context.fillStyle = sortUpColor
                        context.fill();
                    }
                }

                Canvas {
                    id: sortDownIndicator
                    objectName: "sortDownIndicator"
                    width: 13
                    height: 6
                    property color sortDownColor : Style.lighterGray
                    contextType: "2d"

                    onPaint: {
                        context.reset();
                        context.moveTo(0, 0);
                        context.lineTo(width, 0);
                        context.lineTo(width / 2, height);
                        context.closePath();
                        context.fillStyle = sortDownColor
                        context.fill();
                    }
                }
            }

            Text {
                id: headerText
                objectName: "ColumnName"
                font.pointSize: 10
                font.bold: true
                color: Style.white
            }
        }

        onClicked: {
            if (sortableHeaderCell.state != "default") {
                ascending = !ascending;
            }
            sortableHeaderCell.state = ascending ? "ascending" : "descending";
            sortChanged(ascending);
        }
    }

    onStateChanged: {
        paintIndicators();
    }

    function paintIndicators() {
        sortUpIndicator.requestPaint();
        sortDownIndicator.requestPaint();
    }

    states: [
        State {
            name: "default"
            PropertyChanges {target: sortUpIndicator; sortUpColor: Style.transparent}
            PropertyChanges {target: sortDownIndicator; sortDownColor: Style.lighterGray}
            PropertyChanges {target: sortbutton; ascending: true }
        },
        State {
            name: "ascending"
            PropertyChanges {target: sortUpIndicator; sortUpColor: Style.transparent}
            PropertyChanges {target: sortDownIndicator; sortDownColor: Style.white}
        },
        State {
            name: "descending"
            PropertyChanges {target: sortUpIndicator; sortUpColor: Style.white}
            PropertyChanges {target: sortDownIndicator; sortDownColor: Style.transparent}
        }
    ]

    Component.onCompleted: {
        if (alignment === "center") {
            rowItem.anchors.centerIn = sortbutton;
        } else if (alignment === "right") {
            rowItem.anchors.right = sortbutton.right
        }
    }


}
