import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0

Item {
    id: coloredIcon
    objectName: "ColoredIcon"
    property alias iconImage: image.source
    property alias iconColor: colorOverlay.color
    property alias image: image
    property alias colorOverlay: colorOverlay
    Image {
        id: image
        objectName: "Image_ColoredIcon"
        anchors.fill: parent
        source: ""
        fillMode: Image.PreserveAspectFit
        mipmap: true
        smooth: true
    }
    ColorOverlay {
        id: colorOverlay
        objectName: "ColorOverlay_ColoredIcon"
        anchors.fill: parent
        source: image
        color: Style.white
    }
}
