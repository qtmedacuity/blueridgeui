import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.12
import Style 1.0
import "../../controls" as Controls

Controls.Popup {

    showRightButton: false

    property var recording: null
    signal deleteClicked(int recordingId)

    onRecordingChanged: {
        console.log("delete recording on recording changed");
        if (recording) {
            console.log("=> on recording changed recording", recording.recordingId);
            pressureCard.recordedTime = recording.recordingTime;
            pressureCard.heartRate = recording.heartRate;
            pressureCard.systolicPressure = recording.systolic;
            pressureCard.diastolicPressure = recording.diastolic;
            pressureCard.mean = recording.mean;
            pressureCard.referenceSystolic = recording.referenceSystolic;
            pressureCard.referenceDiastolic = recording.referenceDiastolic;
            pressureCard.referenceMean = recording.referenceMean;
            pressureCard.signalStrength = recording.sensorStrength;
            pressureCard.hasNotes = recording.notes !== "";
            pressureCard.positionIndicator = recording.position;
            deleteButton.enabled = true
        }
    }

    Text {
        id: title
        objectName: "Title"
        text: qsTr("Are you sure you want to delete?")
        color: Style.white
        font.pixelSize: 36
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 30
    }

    Text {
        objectName: "Instruction"
        text: qsTr("This will delete the following recorded pressure:")
        color: Style.white
        font.pixelSize: 24
        anchors.left: parent.left
        anchors.top: title.bottom
        anchors.margins: 30
    }

    Controls.RecordPressureCard {
        id: pressureCard
        objectName: "PressureCard"
        width: 240
        height: 159
        anchors.centerIn: parent
        showDelete: false
    }

    DropShadow {
        anchors.fill: pressureCard
        horizontalOffset: 5
        verticalOffset: 5
        radius: 12.0
        samples: 17
        color: Style.black
        source: pressureCard
    }

    Controls.Button {
        id: deleteButton
        objectName: "DeleteButton"
        width: 283
        height: 68
        buttonText: qsTr("Delete Recording")
        buttonIconSource: "qrc:/images/whiteTrashIcon"
        buttonIconHeight: 22
        buttonIconWidth: 19
        buttonIconVisible: true
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        buttonTextSize: 14
        buttonColor: Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        onClicked: {
            deleteClicked(recording.recordingId);
            enabled = false;
        }
    }
}
