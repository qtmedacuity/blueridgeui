import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../../controls" as Controls

Controls.Popup {

    rightButtonText: qsTr("Start Calibration")

    Column {
        anchors.top: parent.top
        anchors.left: parent.left
        spacing: 26
        objectName: "WaitForImplantContent"
        Text {
            objectName: "Popup_Title"
            text: qsTr("Wait For Implant")
            color: Style.white
            font.pixelSize: 36
        }

        Text {
            objectName: "Popup_Instruction"
            text: qsTr("The next step will be to calibrate the sensor with data from\nthe PA catheter.\n\nWait until:\n1. CardioMEMS has been implanted, and\n2. The PA catheter is in position")
            color: Style.white
            font.pixelSize: 24
        }
    }
}
