import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.12
import Style 1.0
import "../controls" as Controls

Popup {
    id: genericPopup
    objectName: "GenericPopup"

    property int popupWidth: 756
    property int popupHeight: 593
    property string popupContent
    property bool showButtons: true
    property bool showRightButton: true
    property bool showLeftButton: true
    property int buttonHeight: 68
    property string rightButtonText: qsTr("Continue")
    property string leftButtonText: qsTr("Cancel")

    signal popupButtonClicked(string buttonName)

    width: popupWidth
    height: popupHeight
    anchors.centerIn: Overlay.overlay
    modal: true
    closePolicy: Popup.CloseOnEscape
    focus: true
    topPadding: 15
    rightPadding: 15
    leftPadding: 25
    bottomPadding: 25
    background: Item {
        Rectangle {
            id: backRect
            objectName: "BackRect"
            color: Style.darkGrayAlt
            anchors.fill: parent
        }
        DropShadow {
            anchors.fill: backRect
            horizontalOffset: 5
            verticalOffset: 5
            radius: 12.0
            samples: 17
            color: Style.black
            source: backRect
        }
    }

    Overlay.modal: FastBlur {
        source: mainWindow.contentItem
        radius: 32
    }

   ColumnLayout {
       anchors.fill: parent
        Controls.IconButton {
            id: closeButton
            objectName: "CloseButton"
            iconImage: "qrc:/images/closeIcon"
            width: 21
            height: 21
            dynamicColors: false
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            onClicked: {
                popupButtonClicked("close");
                close();
            }
        }

        Rectangle {
            id: buttonContainer
            objectName: "ButtonContainer"
            visible: showButtons
            Layout.alignment: Qt.AlignBottom
            width: parent.width
            height: buttonHeight + 2
            color: Style.transparent
            Controls.Button {
                id: leftButton
                objectName: "LeftButton"
                visible: showLeftButton
                width: 155
                height: buttonHeight
                buttonText: leftButtonText
                anchors.left: parent.left
                buttonTextSize: 14
                buttonColor: Style.transparent
                buttonPressedColor: Style.blue
                borderColor: Style.blue
                onClicked: {
                    popupButtonClicked("left");
                    close();
                }
            }
            Controls.Button {
                id: rightButton
                objectName: "RightButton"
                visible: showRightButton
                width: 283
                height: buttonHeight
                buttonText: rightButtonText
                anchors.right: parent.right
                anchors.rightMargin: 15
                buttonTextSize: 14
                buttonColor: Style.blue
                buttonPressedColor: Style.transparent
                borderColor: Style.lightBlue
                onClicked: {
                    popupButtonClicked("right");
                    close();
                }
            }
        }
    }
}
