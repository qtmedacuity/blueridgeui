import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import "../controls" as Controls

Rectangle {
    id: recordingCard
    objectName: "RecordPressureDelegate"
    height: 159
    width: 240
    color: Style.darkGrayAlt

    signal cardClicked(int index);
    signal deleteClicked(int index);

    MouseArea {
        id: cardMouseArea
        objectName: "RecordedPressure" + index
        anchors.fill: parent
        onClicked: {
            console.log("Card clicked");
            cardClicked(index);
        }
    }
    Controls.RecordPressureCard {
        id: pressureCard
        objectName: "RecordedPressure_" + index
        width: parent.width
        height: parent.height
        recordedTime: model.recordingTime
        heartRate: model.heartRate
        systolicPressure: model.systolic
        diastolicPressure: model.diastolic
        mean: model.mean
        referenceSystolic: model.referenceSystolic
        referenceDiastolic: model.referenceDiastolic
        referenceMean: model.referenceMean
        signalStrength: model.sensorStrength
        hasNotes: model.haveNotes
        positionIndicator: model.position
        onDeletePressure: {
            console.log("pressure delegate, delete clicked", index);
            deleteClicked(index);
        }
    }

}
