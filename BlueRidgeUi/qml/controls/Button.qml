import QtQuick 2.12
import Style 1.0

Rectangle
{
    id: button
    objectName: "Button"

    property color buttonColor
    property color buttonPressedColor
    property color borderColor
    property color disabledBorderColor: Style.gray
    property color disabledTextColor: Style.gray
    property alias buttonText: text.text
    property color buttonTextColor: Style.white
    property alias buttonIconSource: icon.source
    property alias buttonIconVisible: icon.visible
    property alias buttonTextSize: text.font.pointSize
    property alias buttonIconWidth: icon.sourceSize.width
    property alias buttonIconHeight: icon.sourceSize.height
    property int borderWidth: 1

    signal clicked()

    border.width: enabled ? borderWidth : 1
    border.color: enabled ? borderColor : disabledBorderColor
    opacity: (buttonColor != "") ? 0.9 : 1.0
    color: enabled ? (mouseArea.pressed ? buttonPressedColor : buttonColor) : Style.transparent
    radius: 4

    Row
    {
        spacing: 10
        anchors.centerIn: button
        Image
        {
            id: icon
            width: (visible) ? sourceSize.width : 0
            height: (visible) ? sourceSize.height : 0
            visible: false
            anchors.verticalCenter: parent.verticalCenter
            fillMode: Image.PreserveAspectFit
        }

        Text
        {
            id: text
            color: enabled ? buttonTextColor : disabledTextColor
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 18
        }
    }

    MouseArea
    {
        id: mouseArea
        anchors.fill: parent
        onClicked: if(enabled) button.clicked()
    }
}

