import QtQuick 2.12
import Style 1.0

Item {
    id: unsentReadingLabel
    objectName: "UnsentReadingLabel"
    Rectangle {
        id: labelRect
        objectName: "LabelRect"
        color: Style.transparent
        border.width: 1
        border.color: Style.yellow
        width: 110
        height: 18
        radius: 5
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.verticalCenter: parent.verticalCenter

        Text {
            id: lableText
            objectName: "LabelText"
            text: qsTr("UNSENT READING")
            font.family: Style.brandon
            font.pixelSize: 10
            color: Style.yellow
            anchors.centerIn: parent
        }
    }
}
