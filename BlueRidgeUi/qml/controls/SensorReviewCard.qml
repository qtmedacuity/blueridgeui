import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import SensorCalibration 1.0
import "../controls" as Controls

Controls.BaseReviewCard {

    objectName: "SensorReviewCard"
    property SensorCalibration sensorData: null
    property int pressureTextSize: 20

    cardTitle: qsTr("SENSOR CALIBRATION")
    waveFile: sensorData ? sensorData.waveFormFile : ""
    editButtonEnabled: sensorData !== null
    deleteButtonEnabled: sensorData !== null

    Controls.ReviewDataValues {
        objectName: "SensorReview_DataValues"
        anchors.left: parent.left
        anchors.leftMargin: 12
        anchors.top: parent.top
        anchors.topMargin: 35
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        width: 300
        eventTime: sensorData ? sensorData.time : ""
        sys: sensorData ? sensorData.systolic : ""
        dia: sensorData ? sensorData.diastolic : ""
        meanVal: sensorData ? sensorData.mean : " "
        hRate: sensorData ? sensorData.heartRate : ""
        signalStrength: sensorData ? sensorData.sensorStrength : 0
        refSystolic: ""
        refDiastolic: ""
        refMean: sensorData ? sensorData.reference : " "
    }
}
