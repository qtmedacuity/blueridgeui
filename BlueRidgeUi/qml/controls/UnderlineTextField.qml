import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0


TextField {

    property bool isEditable: false
    property bool invalid: false

    id: textField
    objectName: "UnderlineTextField"
    color: Style.white
    readOnly: !isEditable
    background: Rectangle {
        color: Style.transparent
        Rectangle {
            id: textBackground
            objectName: textField.objectName + "_Underline"
            color: textField.focus ? Style.lightBlue : Style.blue
            width: parent.width
            height: textField.focus ? 3 : 2
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            visible: isEditable
        }
    }

    states: [
        State {
            name: "infocus"
            when: !invalid && focus
            PropertyChanges {
                target: textBackground
                color: Style.lightBlue
            }
        },
        State {
            name: "invalid"
            when: invalid
            PropertyChanges {
                target: textBackground
                color: Style.yellow
            }
        }
    ]
}
