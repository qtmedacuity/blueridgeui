import QtQuick 2.12
import QtQuick.Layouts 1.12
import Style 1.0
import CardiacOutput 1.0
import "../controls" as Controls

Controls.BaseReviewCard {
    objectName: "CardiaOutputCard"
    property int pressureTextSize: 20
    property CardiacOutput outputData: null
    cardTitle: qsTr("CARDIAC OUTPUT CALIBRATION")
    waveFile: outputData ? outputData.waveFormFile : ""
    editButtonEnabled: outputData !== null
    deleteButtonEnabled: outputData !== null

    RowLayout {
        id: titleRow
        anchors.left: parent.left
        anchors.leftMargin: 12
        anchors.top: parent.top
        anchors.topMargin: 35

        Text {
            id: eventTime
            objectName: "EventTime"
            color: Style.white
            font.pointSize: 8
            Layout.preferredWidth: 55
            Layout.alignment: Qt.AlignLeft
            text: outputData ? outputData.time : ""
        }
        Text {
            objectName: "Label"
            text: qsTr("Cardiac Output")
            color: Style.white
            font.pointSize: 8
            Layout.preferredWidth: 100
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
        }
        Item {
            Layout.preferredWidth: 55
        }

        Text {
            objectName: "HeartRateLabel"
            text: qsTr("HR")
            color: Style.white
            font.pointSize: 8
            Layout.preferredWidth: 45
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            objectName: "SignalLabel"
            text: qsTr("Signal")
            color: Style.white
            font.pointSize: 8
            Layout.preferredWidth: 45
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
    RowLayout {
        anchors.left: parent.left
        anchors.leftMargin: 12
        anchors.top: titleRow.top
        anchors.topMargin: 15
        Item {
            id: placeholder1
            Layout.preferredWidth: 55
        }

        Text {
            id: cardiacOutput
            objectName: "CardiacOutput"
            Layout.preferredWidth: 100
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
            text: outputData ? Number(outputData.cardiacOutput).toLocaleString() : ""
            color: Style.white
            font.pointSize: pressureTextSize
        }

        Item {
            Layout.preferredWidth: 55
        }
        Text {
            id: heartRateText
            objectName: "HeartRate"
            color: Style.white
            font.pointSize: pressureTextSize
            Layout.preferredWidth: 45
            Layout.alignment: Qt.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
            text: outputData ? outputData.heartRate : ""
        }
        Controls.SignalStrengthIcon {
            id: signalStrength
            objectName: "SignalStrength"
            Layout.leftMargin: 10
            Layout.alignment: Qt.AlignHCenter
            sensorStrengthVal: outputData ? outputData.sensorStrength : 0
            visible: outputData !== null
        }
    }

}
