import QtQuick 2.12
import Style 1.0
import "../controls" as Controls

Rectangle
{
    id: page
    objectName: "Page"
    color: Style.darkGray
    property string pageTitle
    property bool showBackButton: false

    Controls.Button
    {
        id: backButton
        objectName: "Back"
        visible: showBackButton
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 21
        width: 50
        height: 22
        buttonText: qsTr("< back")
        buttonTextSize: 10
        buttonColor: Style.transparent
        buttonPressedColor: Style.lightBlue
        borderColor: Style.lightBlue
        onClicked: {
             mainStackView.pop();
        }
    }

}
