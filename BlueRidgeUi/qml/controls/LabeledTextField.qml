import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0

Column
{
    id: labeledTextField
    objectName: "LabeledTextField"
    spacing: 2

    property alias textFieldWidth: entryField.width
    property alias textFieldHeight: entryField.height
    property alias textFieldLabel: label.text
    property alias textFieldPlaceholder: entryField.placeholderText
    property alias textFieldText: entryField.text
    property alias maxCharacterLength: entryField.maximumLength
    property alias textFieldValidator: entryField.validator
    property bool isRequired: false
    property bool isEditable: true

    property bool touched: false;

    function isValid() {
        var valid = isRequired && entryField.acceptableInput;
        if (!valid && state == "") {
            state = "requiredEntry"
        }
        return valid;
    }

    Row
    {
        spacing: 5
        id: labelRow

        Text
        {
            id: label
            objectName: "Label"
            text: qsTr("Label")
            font.pointSize: 16
            color: Style.white
        }

        Text
        {
            id: requiredTextIndicator
            objectName: "RequiredTextIndicator"
            text: qsTr("*")
            visible: isRequired
            color: Style.white
            font.pointSize: 16
        }
    }


    Column
    {
        id: fieldColumn
        spacing: 0

        Rectangle
        {
            id: requiredLineIndicator
            objectName: "RequiredTextLineIndicator"
            height: 2
            width: textFieldWidth
            color: Style.yellow
            visible: false
        }

        TextField
        {
            id: entryField
            objectName: "EntryField"
            font.pointSize: 16
            height: 35
            placeholderTextColor: Style.mediumGray
            selectByMouse: true
            color: Style.black
            background: Rectangle {
                border.color: Style.transparent
                radius: 1.5
            }
            readOnly: !isEditable
            validator: RegExpValidator {
                regExp: isRequired ? /[A-Za-z]+/ : /.*/
            }
            onTextChanged: touched = true
        }
    }

    states: [
        State
        {
            name: "requiredEntry"
            when: touched && isRequired && !entryField.acceptableInput
            PropertyChanges { target: label; color: Style.yellow }
            PropertyChanges { target: requiredTextIndicator; color: Style.yellow }
            PropertyChanges { target: requiredLineIndicator; visible: true }

        }
    ]


}

