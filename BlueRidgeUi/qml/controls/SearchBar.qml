import QtQuick 2.12
import QtQuick.Controls 2.5
import Style 1.0
import "../controls" as Controls

Rectangle {
    id: searchBar
    objectName: "SearchBar"

    signal searchTextEntered(string text)

    radius: 3

    TextField {
        id: filterText
        objectName: "SearchField"
        placeholderText: "search"
        placeholderTextColor: Style.gray
        color: Style.gray
        anchors.fill: parent
        background: Rectangle {
            anchors.fill: parent
            color: Style.darkGray
            border.color: Style.lightBlue
            border.width: 1
            radius: 4
        }
        onAccepted: {
            searchTextEntered(filterText.text)
        }
    }

    Item {
        id: searchSpace
        anchors.right: filterText.right
        anchors.top: filterText.top
        anchors.verticalCenter: filterText.verticalCenter
        width: 30
        height: 30
        Controls.IconButton {
            id: searchButton
            objectName: "SearchButton"
            iconImage: "qrc:/images/searchIcon"
            anchors.centerIn: parent
            defaultColor: Style.transparent
            pressedColor: Style.lightBlue
            height: 16
            width: 16
            onClicked: {
                searchTextEntered(filterText.text)
            }
        }
    }

}
