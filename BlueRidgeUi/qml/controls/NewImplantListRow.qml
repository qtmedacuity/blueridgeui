import QtQuick 2.12
import Style 1.0
import "../controls" as Controls

Rectangle {
    id: newImplantRow
    objectName: "PatientDelegate"

    property int rowWidth: 100
    property int rowHeight: 40
    property color rowColor: Style.transparent
    property color separatorColor: Style.black

    signal rowSelected(int index)

    width: rowWidth
    height: rowHeight
    color: rowColor

    Column {
        id: rowColumn
        anchors.fill: parent
        Rectangle {
            id: lineSeparator
            color: separatorColor
            width: rowWidth
            height: 2
        }

        Row {
            id: rowContent
            Controls.SelectableListContent {
                id: patientName
                objectName: "PatientName_Delegate" + index
                contentText: fullName
                height: rowHeight
                width: 400
                onItemSelected: {
                    rowSelected(index);
                }
            }

            Controls.ListContent {
                id: patientDob
                objectName: "PatientDOB_Delegate" + index
                contentText: dob
                height: rowHeight
                width: 130
                alignment: Text.AlignLeft
            }

            Controls.ListContent {
                id: patientIdentifier
                objectName: "PatientId_Delegate" + index
                contentText: patientId
                height: rowHeight
                width: 145
                alignment: Text.AlignRight
            }

        }
    }
}
