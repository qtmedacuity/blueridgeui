import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.12
import Style 1.0
import Recording 1.0
import "../controls" as Controls
import "../views" as Views

Controls.OverlayContainer {

    id: staticWaveOverlay
    objectName: "StaticWaveOverlay"

    property var recording: null
    property alias saveEnabled: saveButton.enabled
    property alias saveFocus: saveButton.focus
    property alias discardEnabled: discardButton.enabled
    property alias showYAxisLabels: staticWaveView.showYLabels

    signal opened()
    signal closed()

    function open() {
        visible = true;
        opened();
    }

    function close() {
        visible = false;
        closed();
    }

    function setChartVerticalOffset(offset) {
        staticWaveView.setVerticalOffset(offset);
    }

    signal save()
    signal discard()
    signal dismissed()

    Item {
        width: 984
        height: 723
        anchors.centerIn: parent

        Rectangle {
            id: background
            anchors.fill: parent
            anchors.centerIn: parent
            color: Style.darkGrayAlt
        }
        DropShadow {
            anchors.fill: background
            horizontalOffset: 5
            verticalOffset: 5
            radius: 12.0
            samples: 17
            color: "black"
            source: background
        }

        Controls.IconButton {
            id: closeButton
            objectName: "CloseButton"
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: 15
            iconImage: "qrc:/images/closeIcon"
            width: 21
            height: 21
            dynamicColors: false
            onClicked: {
                dismissed();
                close();
            }
        }

        Views.StaticWaveView {
            id: staticWaveView
            objectName: "StaticWaveView"
            height: 300
            anchors.top: parent.top
            anchors.topMargin: 15
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 5
            anchors.rightMargin: 5
            showPressureSearchBar: false
        }

        Controls.SignalStrengthIndicator {
            id: staticSignalStrength
            objectName: "StaticSignalStrength"
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.top: staticWaveView.bottom
            anchors.topMargin: 10
            isStatic: true
        }


        Controls.Button {
            id: discardButton
            objectName: "DiscardButton"
            width: 126
            height: 38
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.margins: 20
            buttonText: qsTr("Discard")
            buttonIconSource: "qrc:/images/trashIcon"
            buttonIconHeight: 20
            buttonIconWidth: 15
            buttonIconVisible: true
            buttonTextSize: 14
            buttonColor: Style.transparent
            buttonPressedColor: Style.blue
            borderColor: Style.blue
            onClicked: {
                focus = true;
                discard();
            }
        }
        Controls.Button {
            id: saveButton
            objectName: "SaveButton"
            width: 126
            height: 38
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 20
            buttonText: qsTr("Save")
            buttonTextSize: 14
            buttonColor: Style.blue
            buttonPressedColor: Style.transparent
            borderColor: Style.lightBlue
            onClicked: {
                focus = true;
                save();
            }
        }
    }
    onRecordingChanged: {
        if (recording) {
            staticSignalStrength.staticSignalStrength = recording.sensorStrength;
            staticWaveView.dataFile = recording.waveFormFile
        }
    }

    states: [
        State {
            name: "hideChart"
            PropertyChanges {
                target: staticWaveView
                height: 0
            }
            PropertyChanges {
                target: staticWaveView
                visible: false
            }
        },
        State {
            name: "hideStrength"
            PropertyChanges {
                target: staticSignalStrength
                height: 0
            }
            PropertyChanges {
                target: staticSignalStrength
                visible: false
            }
        },
        State {
            name: "hideChartAndStrength"
            PropertyChanges {
                target: staticWaveView
                height: 0
            }
            PropertyChanges {
                target: staticWaveView
                visible: false
            }
            PropertyChanges {
                target: staticSignalStrength
                height: 0
            }
            PropertyChanges {
                target: staticSignalStrength
                visible: false
            }
        }
    ]
}
