import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import Style 1.0
import RhcValues 1.0
import "../controls" as Controls

Rectangle {

    objectName: "RhcValuesCard"
    property RhcValues rhcData: null
    property int pressureTextSize: 18
    property int pressureLabelSize: 7

    color: Style.darkerGray2
    Text {
        objectName: "Title"
        text: qsTr("RHC Values")
        color: Style.white
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 9
    }

    Controls.Button
    {
        id: editButton
        objectName: "EditButton"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 9
        width: 40
        height: 22
        buttonText: qsTr("Edit")
        buttonTextSize: 10
        buttonColor: Style.transparent
        buttonPressedColor: Style.lightBlue
        borderColor: Style.lightBlue
        enabled: rhcData !== null
        onClicked: {
            console.log("rhc values edit button pressed.")
        }
    }

    ColumnLayout {
        anchors.top: editButton.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 9

        RowLayout {

            Text {
                id: raLabel
                objectName: "RALabel"
                text: qsTr("RA")
                font.pointSize: 9
                color: Style.white
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                Layout.bottomMargin: 15
                Layout.preferredWidth: 35
            }

            Item {
                id: raPlaceholder
                Layout.preferredWidth: 80
            }

            Controls.MeanText {
                id: raMeanText
                objectName: "RaMeanText"
                Layout.preferredWidth: 55
                Layout.alignment: Qt.AlignHCenter
                textSize: pressureTextSize
                showAltLabel: true
                meanValue: rhcData ? rhcData.raValue : " "
                labelTextSize: pressureLabelSize
            }

        }
        RowLayout {

            Text {
                id: rvLabel
                objectName: "RVLabel"
                text: qsTr("RV")
                font.pointSize: 9
                color: Style.white
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                Layout.bottomMargin: 15
                Layout.preferredWidth: 35
            }

            Controls.SystolicDiastolicText {
                id: rvSystolicDiastolicText
                objectName: "RvSystolicDiastolicText"
                Layout.preferredWidth: 80
                Layout.alignment: Qt.AlignHCenter
                showAltLabel: true
                labelTextSize: pressureLabelSize
                textSize: pressureTextSize
                systolicVal: rhcData ? rhcData.rvSystolic : " "
                diastolicVal: rhcData ? rhcData.rvDiastolic : " "
            }

        }
        RowLayout {

            Text {
                id: pcwpLabel
                objectName: "PcwpLabel"
                text: qsTr("PCWP")
                font.pointSize: 9
                color: Style.white
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                Layout.bottomMargin: 15
                Layout.preferredWidth: 35
            }

            Item {
                id: pcwpPlaceholder
                Layout.preferredWidth: 80
            }


            Controls.MeanText {
                id: pcwpMeanText
                objectName: "PcwpMeanText"
                Layout.preferredWidth: 55
                Layout.alignment: Qt.AlignHCenter
                textSize: pressureTextSize
                showAltLabel: true
                meanValue: rhcData ? rhcData.pcwpValue : " "
                labelTextSize: pressureLabelSize
            }

        }
        RowLayout {

            Text {
                id: paLabel
                objectName: "PALabel"
                text: qsTr("PA")
                font.pointSize: 9
                color: Style.white
                Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
                Layout.bottomMargin: 15
                Layout.preferredWidth: 35
            }

            Controls.SystolicDiastolicText {
                id: paSystolicDiastolicText
                objectName: "PaSystolicDiastolicText"
                Layout.preferredWidth: 80
                Layout.alignment: Qt.AlignHCenter
                showAltLabel: true
                labelTextSize: pressureLabelSize
                textSize: pressureTextSize
                systolicVal: rhcData ? rhcData.paSystolic : " "
                diastolicVal: rhcData ? rhcData.paDiastolic : " "
            }

            Controls.MeanText {
                id: paMeanText
                objectName: "PaMeanText"
                Layout.preferredWidth: 55
                Layout.alignment: Qt.AlignHCenter
                textSize: pressureTextSize
                showAltLabel: true
                meanValue: rhcData ? rhcData.paMean : " "
                labelTextSize: pressureLabelSize
            }

        }
    }
}
