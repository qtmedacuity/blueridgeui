import QtQuick 2.12
import Style 1.0
import QtMultimedia 5.12
import "../controls" as Controls

Rectangle
{
    id: acquireSignal
    objectName: "AcquireSignal"
    color: Style.transparent
    anchors.fill: parent

    Text {
        id: instructionTitle
        objectName: "InstructionTitle"
        text: qsTr("Acquire Signal")
        color: Style.white
        font.pointSize: 20
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 20
    }

    Text {
        id: instructionText
        objectName: "InstructionText"
        text: qsTr("Place wand under sensor. Adjust wand slightly until signal strength is green and above 70.")
        color: Style.white
        font.pointSize: 14
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: instructionTitle.bottom
        anchors.topMargin: 10
        width: parent.width * 0.5
        wrapMode: Text.WordWrap
    }

    Controls.Button {
        id: continueButton
        objectName: "ContinueButton"
        height: 45
        width: 155
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.right: acquireSignalAnimation.left
        anchors.rightMargin: 30
        buttonText: qsTr("Continue")
        buttonTextSize: 14
        buttonColor: Style.blue
        buttonPressedColor: Style.transparent
        borderColor: Style.lightBlue
        onClicked: {
            implantRoot.nextStep();
        }
    }

    Controls.Button
    {
        id: autoSearchPressureButton
        objectName: "AutoSearchPressureButton"
        anchors.right: continueButton.left
        anchors.rightMargin: 15
        anchors.top: continueButton.top
        width: 200
        height: continueButton.height
        buttonText: qsTr("Adjust Search Pressure")
        buttonTextSize: 12
        buttonColor: Style.transparent
        buttonPressedColor: Style.lightBlue
        borderColor: Style.lightBlue
        visible: pasignalStrengthController.strength < 50.0
        enabled: !searchbarController.autoSearching
        onClicked: {
            searchbarController.startAutoSearching();
        }
    }

    Controls.Button {
        id: cancelButton
        objectName: "CancelButton"
        height: 45
        width: 110
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        buttonText: qsTr("Cancel")
        buttonTextSize: 14
        buttonColor: Style.transparent
        buttonPressedColor: Style.blue
        borderColor: Style.blue
        onClicked: {
            implantRoot.previousStep();
        }
    }

    AnimatedImage
    {
        id: acquireSignalAnimation
        objectName: "AcquireSignalVideo"
        anchors.right: parent.right
        anchors.rightMargin: 70
        anchors.bottom: parent.bottom
    }

    Component.onCompleted: {
        var src = (implantRoot.sensorPosition === "left") ? "qrc:/animations/acquireSignalLeft" : "qrc:/animations/acquireSignalRight";
        acquireSignalAnimation.source = src;
    }
}
