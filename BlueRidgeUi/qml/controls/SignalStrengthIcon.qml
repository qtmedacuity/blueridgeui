import QtQuick 2.12
import Style 1.0

Rectangle {
    id: signalStrengthIcon
    objectName: "SignalStrengthIcon"

    property int sensorStrengthVal

    state: pasignalStrengthController.getStatusAt(sensorStrengthVal);

    width: 25
    height: 25
    color: Style.transparent
    border.width: 2
    border.color: Style.red

    radius: width*0.5

    Text {
        id: signalStrengthText
        objectName: "SignalStrengthValue"
        anchors.centerIn: parent
        color: Style.white
        font.pointSize: 10
        text: sensorStrengthVal
    }

    states: [
        State {
            name: "good"
            PropertyChanges {target: signalStrengthIcon; border.color: Style.green}
        },
        State {
            name: "weak"
            PropertyChanges {target: signalStrengthIcon; border.color: Style.orange}
        }
    ]

}
