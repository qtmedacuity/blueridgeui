import QtQuick 2.12
import QtGraphicalEffects 1.12
import Style 1.0
import "../controls" as Controls

Item {
    id: sliderComponent
    objectName: "PressureSearchSliderComponent"
    property color barColor: Style.lightGray
    property color capColor: Style.white
    property color contentColor: Style.white
    property int flashlightSize: 500
    property bool flashlightVisible: true
    property string topCapImage: ""
    property string barImage: ""
    property string bottomCapImage: ""
    property alias topCap: topCapIcon
    property alias bar: barIcon
    property alias bottomCap: bottomCapIcon
    antialiasing: true

    Controls.ColoredIcon {
        id: topCapIcon
        iconColor: capColor
        iconImage: sliderComponent.topCapImage
        width: barIcon.width
        height: width * (image.sourceSize.height / image.sourceSize.width)
        anchors.right: barIcon.right
        anchors.bottom: barIcon.top
    }
    Controls.ColoredIcon {
        id: barIcon
        iconColor: barColor
        iconImage: sliderComponent.barImage
        image.fillMode: Image.Stretch
        width: parent.width
        height: parent.height
    }
    Item {
        id: flashlight
        objectName: "Flashlight"
        width: sliderComponent.flashlightSize
        height: barIcon.height
        anchors.left: barIcon.right
        anchors.top: barIcon.top
        visible: sliderComponent.flashlightVisible
        z:1
        opacity: 0.6
        LinearGradient {
            anchors.fill: parent
            start: Qt.point(0, 0)
            end: Qt.point(sliderComponent.flashlightSize, 0)
            gradient: Gradient {
                GradientStop { position: 0.0; color: Style.white }
                GradientStop { position: 1.0; color: Style.transparent }
            }
        }
    }
    Controls.ColoredIcon {
        id: bottomCapIcon
        iconColor: capColor
        iconImage: sliderComponent.bottomCapImage
        width: barIcon.width
        height: width * (image.sourceSize.height / image.sourceSize.width)
        anchors.right: barIcon.right
        anchors.top: barIcon.bottom
    }
}
