import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.VirtualKeyboard 2.4
import QtQuick.VirtualKeyboard.Settings 2.2
import "views" as Views
import "controls" as Controls
import Recording 1.0

import Style 1.0

ApplicationWindow {
    id: mainWindow
    objectName: "MainWindow"
    //    flags: Qt.FramelessWindowHint // Commnent in for frameless window
    visible: true
    width: 1024
    height: 768
    title: qsTr("Blue Ridge")

    property string eventType: ""

    signal waveFormExamplesClosed();

    Views.TitleBar
    {
        id: titleBar
        objectName: "TitleBar"
        anchors.top: parent.top
        anchors.left: parent.left
        width: mainWindow.width
        height: 45
        titleText: mainStackView.currentItem.pageTitle

        Views.TempSettingsMenu
        {
            id: tempSettingsMenu
            objectName: "SettingsMenu"
            x: 720
        }
    }

    Views.PatientInformationBar
    {
        id: patientInfoBar
        objectName: "PatientInfoBar"
        anchors.top: titleBar.bottom
        anchors.left: parent.left
        width: mainWindow.width
        visible: false
        height: 0
    }

    RowLayout
    {
        id: mainRowLayout
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: mainWindow.height - titleBar.height
        spacing: 0

        StackView {
            id: mainStackView
            objectName: "MainStackView"
            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
            Layout.fillHeight: true
            Layout.fillWidth: true
            initialItem: Views.MainMenu
            {
                id: mainContent
            }

            function showPage(page, attributes)
            {
                var pageComp = Qt.createComponent("qrc:qml/views/" + page);
                if (pageComp.status === Component.Ready)
                 {
                    if (attributes)
                        mainStackView.push(pageComp, attributes)
                    else
                        mainStackView.push(pageComp)
                }
            }
        }

        Views.WaveformExamples {
            id: waveExamples
            objectName: "WaveExamples"
            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
            Layout.fillHeight: true
            Layout.fillWidth: false
            implicitWidth: 0
            visible: false
            onWaveformHidden: {
                exampleWaveformsState.state = ""
                console.log("sending waveform examples closed signal");
                waveFormExamplesClosed();
            }
        }
    }


    Connections {
        target: titleBar
        onSettingsClicked: {
            console.log("Settings Button clicked on the Title Bar")
            tempSettingsMenu.open()
        }
    }

    StateGroup {
        id: exampleWaveformsState
        states: State {
            name: "displayExampleWaveforms"
            PropertyChanges {
                target: waveExamples
                visible: true
            }
            PropertyChanges {
                target: waveExamples
                implicitWidth: 300
            }
        }
        transitions: Transition {
            from: ""
            to: "displayExampleWaveforms"
            reversible: true
            SequentialAnimation {
                PropertyAnimation {
                    target: waveExamples
                    property: "visible"
                    easing.type: Easing.InOutQuad
                    duration: 0
                }
                NumberAnimation {
                    target: waveExamples
                    property: "implicitWidth"
                    easing.type: Easing.InOutQuad
                    duration: 500
                }
            }
        }
    }

    StateGroup {
        id: patientInfoBarState
        states: State {
            name: "displayInfoBar"
            PropertyChanges {
                target: patientInfoBar
                visible: true
            }
            PropertyChanges {
                target: patientInfoBar
                height: 38
            }
            PropertyChanges {
                target: mainRowLayout
                height: mainWindow.height - titleBar.height - 38
            }
        }
        transitions: Transition {
            from: ""
            to: "displayInfoBar"
            reversible: true
            SequentialAnimation {
                PropertyAnimation {
                    target: patientInfoBar
                    property: "visible"
                    easing.type: Easing.InOutQuad
                    duration: 0
                }
                NumberAnimation {
                    target: patientInfoBar
                    property: "height"
                    easing.type: Easing.InOutQuad
                    duration: 250
                }
            }
        }
    }

    function setPatientBar(patient) {
        patientInfoBar.patientName = patient.patientName
        patientInfoBar.patdob = patient.dob
        patientInfoBar.patsn = patient.serialNumber
        patientInfoBarState.state = "displayInfoBar"
    }

    function resetPatientBar() {
        patientInfoBar.patientName = ""
        patientInfoBar.patdob = ""
        patientInfoBar.patsn = ""
        patientInfoBarState.state = ""
        eventType = ""
    }

    //----------------------------------------
    // OVERLAYS:
    //----------------------------------------

    signal overlayOpened()
    signal overlayClosed()

    //----------------------------------------
    // PA mean overlay
    //----------------------------------------
    function openPaMeanOverlay(recording) {
        paMeanOverlay.recording = recording;
        paMeanOverlay.open();
    }

    signal paMeanOverlayDismissed()
    signal paMeanSet()

    Controls.PaMeanEntryOverlay {
        id: paMeanOverlay
        objectName: "PaMeanOverlay"
        anchors.centerIn: parent
        visible: false

        onPaMeanSaved: paMeanSet();
        onPaMeanDiscarded: paMeanOverlayDismissed()
        onOpened: overlayOpened()
        onClosed: overlayClosed()
    }

    //----------------------------------------
    // Record confirmation overlay
    //----------------------------------------
    function openRecConfirmationOverlay(recording, isNew) {
        recordingConfirmation.recording = recording;
        recordingConfirmation.isNewRecording = isNew;
        recordingConfirmation.open();
    }

    function closeRecConfirmationOverlay() {
        recordingConfirmation.close();
    }

    function isRecConfirmationOverlayOpen() {
        return recordingConfirmation.visible;
    }

    signal recordingSaved(Recording recording)
    signal recordingDiscarded(Recording recording, bool isNew)
    signal recConfirmationOverlayClosed()

    Controls.RecordConfirmationPopup {
        id: recordingConfirmation
        objectName: "RecordingConfirmation"
        anchors.centerIn: parent
        visible: false

        onSaveRecording: {
            recordingSaved(recordingConfirmation.recording)
        }
        onDiscardRecording: {
            recordingDiscarded(recordingConfirmation.recording, recordingConfirmation.isNewRecording)
        }

        onOpened: overlayOpened()

        onClosed: {
            closeNumericKeyboard();
            recordingConfirmation.reset();
            recConfirmationOverlayClosed();
            overlayClosed()
        }
    }

    //----------------------------------------
    // RHC Entry overlay
    //----------------------------------------
    function openRHCEntryOverlay(recording) {
        rhcEntry.recording = recording;
        rhcEntry.open();
    }

    Controls.RhcEntryOverlay {
        id: rhcEntry
        objectName: "RHCEntry"
        anchors.centerIn: parent
        visible: false

        onClosed: {
            closeNumericKeyboard();
            rhcEntry.reset();
            overlayClosed();
        }
    }

    //----------------------------------------
    // Cardiac Output overlay
    //----------------------------------------

    function openCoEntryOverlay(recording) {
        coEntryOverlay.recording = recording;
        coEntryOverlay.open();
    }

    Controls.CoEntryOverlay {
        id: coEntryOverlay
        objectName: "CoEntryOverlay"
        anchors.centerIn: parent
        visible: false

        onClosed: {
            closeNumericKeyboard();
            coEntryOverlay.reset();
            overlayClosed()
        }
    }

    //----------------------------------------
    // KEYBOARDS:
    //----------------------------------------
    // Numeric keyboard
    //----------------------------------------
    function showNumericKeyboard() {
        numericKeyboard.visible = true;
    }

    function closeNumericKeyboard() {
        numericKeyboard.visible = false;
    }

    signal numericInputValue(var value);
    signal numericInputEntered();
    signal numericInputDelete();

    // Custom numeric keyboard for numeric only text boxes
    Controls.CustomNumericKeyboard {
        id: numericKeyboard
        visible: false
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        onKeyPressed: numericInputValue(key)
        onEnterKeyPressed: numericInputEntered()
        onDeleteKeyPressed: numericInputDelete()
    }

    //----------------------------------------
    // Qt keyboard
    //----------------------------------------

    Component.onCompleted: {
        // TODO: this is the method to set the language once that's wired up
        // VirtualKeyboardSettings.locale = "es_ES";
        VirtualKeyboardSettings.styleName = "blueridge";
    }

    property var keyboardLayout: inputPanel.keyboard.layout

    function removeKeys(parent, objectText) {
        var obj = null
        if (parent === null)
            return null
        var children = parent.children
        for (var i = 0; i < children.length; i++) {
            obj = children[i]
            if (obj.toString().substring(0, 17) === "ChangeLanguageKey" ||
                    (obj.text === objectText && obj.toString().substring(0, 7) === "BaseKey")) {
                obj.visible = false
            }
            obj = removeKeys(obj, objectText)
            if (obj)
                break
        }
        return obj
    }

    onKeyboardLayoutChanged: {
        if (keyboardLayout !== "") {
            // remove the language key and the smiley face key
            removeKeys(inputPanel.keyboard, ":-)");
        }
    }

    signal keyboardActive();
    signal keyboardDismissed();

    InputPanel {
        id: inputPanel
        objectName: "Keyboard_InputPanel"
        z: 99
        x: 0
        y: mainWindow.height
        width: mainWindow.width

        states: State {
            name: "visible"
            when: inputPanel.active && !numericKeyboard.visible
            PropertyChanges {
                target: inputPanel
                y: mainWindow.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
        onStateChanged: {
            if (state == "") {
                keyboardDismissed();
            } else {
                keyboardActive();
            }
        }
    }
}
